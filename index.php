<!DOCTYPE html>
<html lang="en">
<head>
<title>ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"> 
<style>
	html {background-color: #418EDD;}
	.pics {width: 20%;}
	#desktopmenubutton {
		position: absolute;
		z-index: 4;
		width: 100%;
		text-align: center;
		display: flex;
		color: white;
		top: 0px;
		height: 50px;
		font-family: Arial, Helvetica, sans-serif;
	}
	#mainmenumobiletitle {
		text-align: center;
		margin-left: 10px;
		margin-top: 8px;
		font-size: 1.8em;
		font-family: Arial, Helvetica, sans-serif;
		top: -4px;
		position: relative;
		margin-right: 20px;
	}
	.contentbox {
    background-color: #ffffff;
  	float:left;
  	margin-top:5%;
  	margin-left:5%;
  	margin-right:5%;
  	margin-bottom:5%;
		box-sizing: content-box;
		border-radius: 25px;
		padding: 20px;
		display: inline;
		overflow: hidden;

	}
	.rowdiv {
		margin-bottom: 5px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 20px;
	}
	.headerbutton_a {
		text-decoration: none;
	}
	#footer_bar {
		background-color: #ffffff;
	 	bottom: 0;
		position: fixed;
		width: 100%;
		text-align: center;
	}
	body {
		padding: 0px;
		margin: 0px;
	}
	#left-contentbox {
		float: left;
		min-height: 400px;
	}
	#right-contentbox {
		height: 40%;
	}
	#searchbar_form {
		display: inline-flex;
		width: 100%;
		margin-right: 30px;
	}
	
	#searchbutton {
		width: 20%;
	}
	#searchbar {
		width: 90%;
		margin-right: 50px;
	}
	#search_input {
		width: 93%;
	}
</style>
<?php
if (substr($_SERVER['SERVER_NAME'], 0,20)=="atlas-expert-system."){?>
<!-- Matomo -->
<script>
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://webanalytics.web.cern.ch/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '282']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
<?php } ?>
</head>

<body>
        <noscript><p><img src="http://piwik.web.cern.ch/piwik.php?idsite=3883" style="border:0" alt="" /></p></noscript>
	<div id="desktopmenubutton">
		<img src="images/cernlogo1.jpg" style="height:100%;" alt="CERN logo">
		<div id="mainmenumobiletitle" style=""><span>ATLAS Expert System (Simulator)</span></div>
		<img src="images/atlaslogo1.png" style="height:100%;" alt="ATLAS experiment logo">
	</div>

<div class="contentbox">
	<div id="left-contentbox">
		<div>
	  		<form id="searchbar_form" method="GET" action="login/search.php" style="margin-bottom: 20px;">
				<input id="search_button" type="submit" value="Search" style="float: right;">
				<div  id=searchbar style="overflow: hidden;"><input id="search_input" name="query" type="text"></div>
			</form>
		</div>
  	<div class="rowdiv"><a class="headerbutton_a" href="login/about.php">User's guide</a></div>
  	<div class="rowdiv"><a class="headerbutton_a" href="login/alarmHelper.php">Alarm helper</a></div>
  	<div class="rowdiv"><a class="headerbutton_a" href="login/inhibitRequest.php">Inhibit requests</a></div>
  	<div class="rowdiv"><a class="headerbutton_a" href="login/simulator_evaluator.php">Annual interventions</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=safety">Safety</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=racksUSA15lvl0">Racks</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=electricity">Electricity</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=cryogenicsDistribution">Cryogenics</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=magnetSummary">Magnets</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=gasSummary">Gas</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=detectorStatus">Sub-detectors</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=ventilation">Ventilation</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=detectorCooling">Cooling</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/simulator.php?page=waterDistribution">Water</a></div>
		<div class="rowdiv"><a class="headerbutton_a" href="login/dssalarms.php">DSS alarms</a></div>
  </div>
  <div id="right-contentbox">
	<div class="nav-right" style="text-align: left;">
	  
	  <a class="tooltip" 
	  title="__________ Electricity Page __________
	  
	  A general overview of the electricity
	  systems of ATLAS both on the surface
	  and in the cavern. " 
	  href="login/simulator.php?page=electricity"><img class="pics" src="images/electricityi.png" ></a> 
	  
	  <a class="tooltip" 
	  title="___________ Magnets Page ___________
	  
	  A general overview of the magnet system
	  of ATLAS, as well as the auxilliary 
	  systems that enable them to run. Includes,
	  but is not limited to, the following: 
	  Toroid Magnets
	  Solenoid Magnet" 
	  href="login/simulator.php?page=magnetSummary"><img class="pics" src="images/magnetsi.png" ></a> 
	  
	  <a class="tooltip" 
	  title="___________ Cryogenics Page ___________"
	  
	   
	  href="login/simulator.php?page=cryogenicsDistribution"><img class="pics" src="images/cryogenics2.png" ></a>
	  
	   <br>
	  
	  <a class="tooltip" 
	  title="____________ Gas Page ______________
	  
	  A main page for gas, among several, 
	  including an overview of gas distributions
	  in ATLAS, including the following: 
	  CSC, TRT,
	  TGC, MDT,
	  TFC" 
	  href="login/simulator.php?page=gasSummary"><img class="pics" src="images/gasi.png" ></a>
	  
	  <a  class="tooltip" 
	  title="______________ Sub-detectors Page ______________
	  A general description of ATLAS sub-detectors"
	  href="login/simulator.php?page=detectorStatus"><img class="pics" src="images/detectorStatus.png" ></a>

	  
	  <a  class="tooltip" 
	  title="____________Ventilation Page____________
	  
	  A detailed map of interconnected buildings
	  serving to ventilate the ATLAS Systems. The
	  buildings included in the scheme are: 
	  
	  SE1, SF1, SX1,
	  SUX1, SDX1, SH1,
	  SGX1, Cavern"
	  href="login/simulator.php?page=ventilation"><img class="pics" src="images/ventilationi.png" ></a><br>

	  
	  <a  class="tooltip" 
	  title="____________ Cooling Page ____________
	  
	  Overview of the sub-detector cooling station in ATLAS.
	  Including TRT, ID evaporative cooling, 
	  IBL CO2 cooling, Tile, LAr, Muons and 
	  Cable cooling." 
	  href="login/simulator.php?page=detectorCooling"><img class="pics" src="images/coolingi.png" ></a>
	  
	  <a  class="tooltip" 
	  title="____________ Water Page ______________
	  
	  A description of the water circulation within
	  ATLAS from the ATLAS Pump in SF1 to all sites:
	  SUX1, SH1, SDX1, SGX1, SU1, SR1, SX1,
	  USA15, UX15, US15."
	  href="login/simulator.php?page=waterDistribution"><img class="pics" src="images/wateri.png" ></a>
	  
	  <a  class="tooltip" 
	  title="____________ DSS Page _______________
	  
	  A collection of alarms, which, when
	  triggered, can cause unexpected
	  shutdowns to different systems: 
	  
	  AL_MAG_Toroid_FastDump_atHighCurrent,
	  AL_AL3_FG_UX15_BigWheel_A,
	  AL_AL3_FG_UX15_BigWheel_C"
	  href="login/dssalarms.php"><img class="pics" src="images/alarmsi.png" ></a>

	</div>	
 



</div>
</div>

<div id="footer_bar">
	<a href="mailto:atlas-opm-expert-system@cern.ch">ATLAS Technical Coordination Expert System</a>
</div>
</body>
</html>
