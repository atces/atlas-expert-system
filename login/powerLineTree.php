<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("search");
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$pagetitle;?> - ATLAS Expert System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
	<?php include ("favicon.php");?>
	<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src = "node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
	<script src="JS/db.js"></script>
	<script src="JS/ui.js"  id="ui"></script>
	<?php $styler->includeStyle(); ?>
	<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
	<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
	<script src="JS/searchTools.js"></script>
	<script src="JS/history.js"></script>
	<script src="JS/checkUrl.js"></script>
	<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
	<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
	<link href="css/theme.bootstrap.css" rel="stylesheet">
	<!--<script src="node_modules/clipboard/dist/clipboard.min.js"></script>-->
	<!--<script id="tableFunctions_search" class="tableFunctions" src="JS/tableFunctions.js" table="search"></script>-->
	<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>
	<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
	<script src="JS/simulatorParser.js"></script>
	<script src="data/codification.js"></script>
  <?php $scripter->includeScripts(); ?>

  <?php include("scripts/treeincludes.php");?>
	

</head>
<body>
	<?php include("header.php"); ?>	
	<div class="chart" id="drawTree" ></div>
	<?php
	$raw_uid=$_GET["uid"];
	$uid = htmlentities($raw_uid, ENT_QUOTES, "UTF-8");
	?>
	<script>
		drawObjectPowerlineTree("<?=$uid?>");
	</script>
	<div class="footer">
		<?php include("footer.php"); ?>
	</div>
</body>
</html>
