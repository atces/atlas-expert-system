<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("Alarm Statistics");
?>



<!DOCTYPE html>
<html lang="en">
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<link href="css/theme.bootstrap.css" rel="stylesheet">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js"  id="ui"></script>
<script src="JS/searchTools.js"></script>
<script src="JS/history.js"></script>
<script src="JS/checkUrl.js"></script>
<script src="JS/simulatorParser.js"></script>
<script src="data/codification.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>
<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
<script src="node_modules/tableexport/dist/js/tableexport.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php $styler->includeStyle(); ?>
<?php $scripter->includeScripts(); ?>
<script> GetTokenId(); </script>
<script src="JS/alarmStats.js"></script>
<script src="JS/alarmHelper.js"></script>
</head>

<!--  body -->
<body>
<?php $pagetitle="Alarm Statistics"; include("header.php"); ?>
<div class="CONTENT" style="font-size: 12px;"> 


<header>
    <!-- <h1 style="margin-top: 70px;">Alarm Statistics</h1> -->
    <p style="font-size: 20px; margin-top: 70px;">Statistics are based on alarms that are stored in the database.</p>
    <p style="font-size: 20px;">Particularly, Last month might not <i>yet</i> be up to date.</p>
</header>


<div>
    <input type="text" id="alarm-to-be-searched" onkeyup="PseudoSearchTable()" placeholder="Filter alarms to select for statistics ..." class="alarmStats-search-bar"></input>
    <div style="max-height: 200px; overflow-y: auto; max-width: 700px; margin:5px; border: 1px solid #ddd;">
    <div><span id="table_alarms_to_select"></span></div>  
</div>

<div>
    <h1>Selected alarms
    <button type="button" class="button-sort-alarmHelper" onclick="buttonSortSelected()"><img src="../images/alarmHelper/sort.png" title="Sort"></button>
    </h1>
    <span id="table_alarms_selected"></span>

    <div>
        <p style="font-size: 20px;">Statistics are provided with respect to the selected date.</p>
        <label for="stats_date"><input type="date" id="stats_date" name="stats_date" style="font-size: 20px"></label>
        <button type="button" class="button-update-alarmHelper" onclick="buttonChartEventTypeDistribution()" style="margin-left: 20px; margin-right: 5px;">
            <img src="../images/alarmHelper/update.png" alt="update" title="Reload"></button>
    </div>
</div>
<div id="alarmStats-reply" style="font-size:20px; margin:5px"></div>



<style>
    .chart-container {
        position: relative;
        width: 700px;
        height: 200px;
        margin-bottom: 10px;
        margin-top: 20px;
        /* border: 2px solid black; */
        z-index: -1;
    }
    .chart-container canvas {
        position: absolute;
        top: 50%;
        left: 5%;
        transform: translate(-10%, -50%);
        width: 95%;
        height: 100%;
        /* border: 1px solid #df2d2d; */
        z-index: -1;
    }
</style>

<!-- make a legend -->
<div id="alarmStats-legend"></div>



<div style="font-size: 20px; margin-top:20px;">Last month</div>
<div id="div-last-month" style="z-index: -1">
    <canvas id="chart-last-month" style="z-index: -1"></canvas>
</div>

<div style="font-size: 20px;">Last year</div>
<div id="div-last-year" style="z-index: -1">
    <canvas id="chart-last-year" style="z-index: -1"></canvas>
</div>

<div style="font-size: 20px;">Inclusive</div>
<div id="div-inclusive" style="z-index: -1">
    <canvas id="chart-inclusive" style="z-index:-1;"></canvas>
</div>



<div>
    <h1>Related Events
    <button class="button-collapseAll-alarmHelper" type="button" onclick="CollapseAllEvents()"><img id="image-collapseAll-alarmHelper" src="../images/alarmHelper/collapseAll.png" alt="collapseAll_button" title="Collapse events"></button>
    </h1>
    <div id="display-events"></div>
</div>



<div class="footer"> <?php include("footer.php"); ?> </div>


</div>
</body>
</html>