<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("dssalarms");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js" id="ui"></script>
<?php $styler->includeStyle(); ?>

<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="JS/searchTools.js" table="alarm" id="searchTools"></script>
<script src="JS/history.js"></script>
<script src="JS/checkUrl.js"></script>
<script src="node_modules/clipboard/dist/clipboard.min.js"></script>
<script src="JS/tableFunctions.js" class="tableFunctions" table="alarm"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>

<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
<script src="JS/simulatorParser.js"></script>
<script src="data/codification.js"></script>
<?php $scripter->includeScripts(); ?>

<script>
$(document).ready(function(){
	loading("start");
	<?if ($tokenid==-1){?>
	  setTimeout(function(){FindObject("AL&type=Alarm",parseSearch);}, 1000);
	<?}else{?>
	  FindObject("AL&type=Alarm",parseSearch);
	<?}?>
});
</script>


</head>
<body>	
<?php include("header.php");?>	
<?php include("searchbar.php")?>
<div class="CONTENT notopmargin">
	<div id="result"></div>
	<div id="alarms_center">
		<div id="alarmpage_div">
			<span id="alarm_list">
				<input hidden class="getsearch" type="search" data-column="1">
				<table id="alarm_table" class="tablesorter">
					<thead>
						<th data-placeholder="Search...">Item</th>
						<th class="first-name filter-select" data-placeholder>Type</th>
					</thead>
					<tbody id="alarm_tbody"></tbody>
				</table>
			</span>
			<div id="page_wrapper">
				<span id="page2">
					
				</span>
				<span id="page3"></span>
			</div>
			
		</div>
	</div>
	<div id="ad_search_full_table"></div>
	<div id="debug"></div>
</div>
<div class="footer">
<?php include("footer.php"); ?>
</div>

</body>

<?php /*
<body>
<?php include("header.php"); ?>
<?php include("searchbar.php")?>
	<div class="CONTENT notopmargin">
		<div id="alarms_center">
			<div id="alarmpage_div">
				<span id="alarm_list">
					<table id="alarm_table" class="tablesorter" style="width:auto">
						<thead>
							<th>Alarm</th>
							<th class="triggered">Triggered</th>
						</thead>
						<tbody id="alarm_tbody"></tbody>
					</table>
				</span>
		
		
				<div id="page_wrapper">
					<span id="page2">
						
					</span>
					<span id="page3"></span>
				</div>
							
				</span>
				<span id="page4">
							
				</span>
			</div>
		</div>
	</div>
	<!-- <iframe id="txtArea1" style="display:none"></iframe> -->
	<div class="footer">
			<?php include("footer.php"); ?>
	</div>
</body>
*/?>

</html>
