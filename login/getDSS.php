<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("Current DSS Status");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<link rel="stylesheet" type="text/css" href="node_modules/jquery-ui-dist/jquery-ui.css">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/searchTools.js"></script>
<script src="JS/simulatorParser.js"></script>
<script src="JS/getDSS.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>


<script src="JS/inhibitRequest.js"></script>
<?php $scripter->includeScripts(); ?> 
<?php $styler->includeStyle(); ?>

<style type="text/css">
  
td {
    white-space: nowrap;
}
</style>
</head>
<body>
	<?php $pagetitle="DSS status"; include("header.php"); ?>
	<div class="CONTENT" style="font-size: 12px;"> 
		<br>
    <p><b>This tool has no effect on ATLAS Detector Safety System (DSS). It is only a simulation. </b>It shows the actual current status of ATLAS DSS. It can be used to set the ATLAS Expert System in the same state as ATLAS DSS.</p>
    

<h2>Active alarms
<input type="button" onclick="trigger_all_DSS_alarms();" value="Trigger all">
<input type="button" onclick="export_tablesorter('DSS_alarms_table');" value="CSV export">
</h2>
<div id="alarms"></div>

<h2>Active inputs
<input type="button" onclick="trigger_all_DSS_inputs();" value="Trigger all">
<input type="button" onclick="export_tablesorter('DSS_inputs_table');" value="CSV export">
</h2>
<div id="inputs"></div>

<h2>Active actions
<input type="button" onclick="trigger_all_DSS_actions();" value="Trigger all">
<input type="button" onclick="export_tablesorter('DSS_actions_table');" value="CSV export">
</h2>
<div id="actions"></div>

<script>

  var triggeredDIs=new Array();
  var dssdata="";
  $(document).ready(function(){

  	

  $.ajax({
	  url: 'dss/dbfunctions.php',
    data: {cmd:"get_alarm_active"},
    type: 'get',
    success: function(reply) {
      dssdata=JSON.parse(reply);
      update_DSS_tables();
    }
  });
 });

</script> 
	</div>
	<div class="footer">
			<?php include("footer.php"); ?>
	</div>
</body>
</html>
