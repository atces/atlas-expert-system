<?php 
/*
 * Template for DSU
 * */
include_once("../functions.php");
$DSU=$_POST["dsu"];
$DIs= $_POST["DIs"];
$actions= $_POST["actions"];

function printStyle($data){
	foreach($data as $k=>$v){
		if (@isset($v["alarm"])==true && $v["alarm"]==true){
		}else{
			#printf("td#image_td>div.%s { position: absolute; top:%dpx; left:%dpx; }\n",str_replace(".","\.",$k),$v["top"],$v["left"]);
		    printf("td#image_td>div[class=\"%s\"] { position: absolute; top:%dpx; left:%dpx; }\n",str_replace(".","\.",$k),$v["top"],$v["left"]);
		}
		
	}
}

function printElements($data){
	//printf('<a  style="text-decoration: none; color: blue;" href="https://atlas-expert-system-dev.web.cern.ch/atlas-expert-system-dev/login/pageElementTable.php?page='.$GLOBALS['pageid'].'"><p>All page elements</p></a>');
	$alarms_str="";
	foreach($data as $k=>$v){
		if (@isset($v["pin"])==true && $v["pin"]==true){
			printf('<div class="%s" title="%s" style="transform:rotate(90deg) translateX(10px) translateY(18px);">',$k,$k);
		}else{
			printf('<div class="%s">',$k);
		}
		printf('<svg viewBox="0 3 52 28" width="52" height="22" role="img" ><g id="%ssw" class="cursor element" onclick="window.parent.ChangeSwitch(%s)" transform="scale(0.8),translate(0 5)" ><path d="m 2,13 c 0,-3 3,-6 6,-6 3,0 6,3 6,6 l 0,11 c 0,3 -3,6 -6,6 -3,0 -6,-3 -6,-6 z" fill="#14b828" stroke="#000000" stroke-width="1"></path><circle cx="8" cy="14" r="6.4" fill="#ffffff" stroke="#a9a9a9"></circle></g><rect id="%sst" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect><g onclick="window.parent.displayHistory(%s)" class="cursor" id="%shi"><circle cx="45" cy="21" r="7" fill="#4287f5"></circle><text fill="#ffffff" x="43" y="27" transform="rotate(-90,45,21)">i</text></g></svg>',$k,"'".$k."'",$k,"'".$k."'",$k);
		printf('</div>'."\n");
	}
}

function printPageElements($data){
	$keys=array_keys($data);
	printf("['");
	printf(join("','",$keys));
	printf("']");
}
//var_dump($actions);


$channels=32;
$data=array();

$firstrow=58;
$row=125;
$rightcolumn=706;
$rightminicolumn=320;
$top["upper7"]=$firstrow+$row*0;
$top["upper9"]=$firstrow+$row*1;
$top["upper6"]=$firstrow+$row*1;
$top["upper5"]=$firstrow+$row*2;
$top["upper10"]=$firstrow+$row*2;
$top["upper8"]=$firstrow+$row*3;
$top["upper4"]=$firstrow+$row*3;
$top["lower7"]=$firstrow+$row*4;
$top["lower9"]=$firstrow+$row*5;
$top["lower6"]=$firstrow+$row*5;
$top["lower5"]=$firstrow+$row*6;

$top["lower8"]=$firstrow+$row*7;
$top["lower4"]=$firstrow+$row*7;
$top["upper12"]=1658;
$top["lower12"]=1658;
$top["upper3"]=1658;
$top["lower3"]=1658;




for ($i = 0; $i <= 33	; $i++) {
    $left[strval($i)]=10+($i*20);
}
switch($DSU){
	case "DSU_1":
		$top["lower10"]=$firstrow+$row*6;
		$top["upper10"]=$firstrow;
		$top["lower5"]=$firstrow+$row*6;
		break;
	case "DSU_5":
		$top["lower10"]=$firstrow+$row*3;
		break;
}
$error=array();


$items=array($DIs, $actions);
foreach ($items as &$item) {
    $error=array();
	$error_counter=0;
	foreach ($item as $name => $values){
		//array_push($data, $name=>array("digitalInput"=>True, "top"=> 1, "left"=> 2));
		$slot	=	$values["slot"];
		$crate 	=	$values["crate"];
		$crateslot=$crate.$slot;

		$channel=$values["channel"];
		#echo "Channel".$channel;

		if (array_key_exists($crateslot, $top)){
			$t=$top[$crateslot];
			$l=$left[$channel];
			

			if (($crateslot=="upper5" and $DSU =="DSU_2")  
				or ($crateslot == "lower10" and $DSU=="DSU_1") 
				or ($crateslot == "upper4" and $DSU=="DSU_5")
				){
				$l+=$rightminicolumn;
			}
			if (($crateslot == "upper10" and $DSU=="DSU_1")){
				$l+=$rightcolumn;	
			}
			if (intval($slot)=="8" or intval($slot=="9")){
				
				if (intval($channel>=17)){
					$l+=$rightcolumn-(16*20);
					$t-=$row;
				}else{
					$l+=$rightcolumn;
				}
			}
			

		}else{
			$error_counter+=20;
			$t=1100;
			$l=10+$error_counter;
			echo "<br>DI ERROR:-".$data[$name]."- - ->".$name." : ".$crateslot."-- Channel [".$channel."].<br>";
		}
		if (in_array(substr( $name, 0, 3 ), array("DI_","AI_","PT_"))){$t+=50;}#make actions in top inner row
		$data[$name]=array("pin"=>True, "top"=> $t, "left"=> $l);
	   
	}
}

printElements($data);	

?>
<style>
<?php 
	printStyle($data);	
?>
</style>
<script>
    //page elements
    pageElements=<?php printPageElements($data); ?>;
    if(pageElements.length>0){
			loading("start");  	
			console.warn(pageElements);
			elementsjson=JSON.stringify(pageElements);
			console.warn(elementsjson);
			GetCurrentState(elementsjson, parseJSONCurrentState);
		}
</script>
