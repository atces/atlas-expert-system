<?php
$json=$_POST["json"];
$tablename=$_POST["tablename"];


//var_dump($json);
?>
<!-- table_general -->
<table  id="inhibit_table" class="inhibit_table tablesorter" style="width:auto">
    <thead>
        <th colspan='3' id="actions_table_caption" data-placeholder="Search..."><?= $tablename ?> <button class="cursor" onclick="export_tablesorter('inhibit_table');">CSV</button>
            <button class="cursor" onclick="inhibit_add_full_filtered_table('inhibit_table');">Add all filtered</button>
            <button class="cursor" onclick="inhibit_del_full_filtered_table('inhibit_table');">Remove all filtered</button>
        </th>
    </thead>
    <tbody id="inhibit_tbody">
    <?php

    foreach( $json["Result"] as $name => $value  ){
        ?>  
        <tr>
            <td>
                <a>
                    <span><?= $name ?></span>
                </a>
                <div style="display:none" id="<?= $name ?>"></div>
                <span class="inhibit_buttons"></span>
                <span style="display:none" id="del_<?= $name ?>" class="inhibit_del cursor" onclick="inhibit_del('<?= $name ?>')"></span>
                <span id="add_<?= $name ?>" class="inhibit_add cursor" onclick="inhibit_add('<?= $name ?>')"></span>
            </td>
        </tr>
        <?php
     }
     ?>
     </tbody>
</table>
    