<?php
$json=$_POST["json"];
//var_dump($json);
?>
	<?php
	
		?>
		<table  id="dashboard_minimax_table" class="white_background dashboard minitable2" style="margin-left:2px">
			<thead>
				<th colspan='3' id="minimax_table_caption" data-placeholder="Search..."></th>
			</thead>
			<tbody id="dashboard_minimax_tbody">
			<?php
			$counter=0;
			foreach( $json as $name => $emptystring ){
				$counter++;
				?>	
				<tr><td><?= $name ?></td></tr>
				<?php
			 }
			 ?>
			 </tbody>
		</table>
	<script>
		var tablecaption='<span class=\"red\">Deployed minimax </span>(<?= $counter ?>) <button onclick=\"export_tablesorter(\'dashboard_minimax_table\');\">CSV</button>';
		console.log(tablecaption);
		$('#minimax_table_caption').html(tablecaption);
		toogleTableRows("dashboard_minimax_table", "hide");
		tables_with_hidden_rows["dss"].push('dashboard_minimax_table');//adding table to list for button Show all
		process_table('dashboard_minimax',5);
		toogleTableRows("dashboard_minimax", "hide");
		add_caption_if_long_table("dashboard_minimax_table");
	</script>