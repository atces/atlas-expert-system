<?php 
include ('../functions.php');

include ('../isAdmin.php');
$isAdminClass= new isAdmin;
	
$filename='../data/requestsDB.json';
//var_dump($_POST["archived"]);
$archived=($_POST["archived"]=="true"?"true":"false");
//var_dump($archived);
if ($isAdminClass->isDevpage()){
	$dev="-dev";
}else{$dev="";}

$contents=file_get_contents($filename);
$list= json_decode($contents, true);

//var_dump($list);
if (count($list) > 0 ){
	if ($archived=="false"){
		?>	

		<button class="apply_button" onclick="requests_applyJSON();">Apply all inhibits to Expert System</button>
		<button class="apply_button" onclick="requests_triggerJSON();">Simulate trigger all inhibits on Expert System</button>

		<?php
		if($isAdminClass->isAdmin()) { 
			?>
			<button class="apply_button" onclick="hide_table_buttons();">Hide buttons</button>
		<?php }?>
		<button id="button_show_archived" class="apply_button" onclick="loadArchivedTable();">Show archived</button>
		
		<button id="button_show_archived" class="apply_button" onclick="checkRowPassedDateRemove();">Remove highlight on expired</button>
		<button id="button_hide_archived" class="apply_button" style="display:none" onclick="hideArchivedTable();">Hide archived</button>
	<?php 	} ?>
		
	<span>
		<div id="row_counter" class="<?=($archived=="false"?"inhibited":"archived") ?>_table"></div>
		<table id="<?=($archived=="false"?"inhibited":"archived") ?>_table" style="width:100%" class="tablesorter <?= ($archived=="false"?"inhibited":"archived") ?>_table">
			<thead>
			<tr>
				<th>Alarms</th>
				<th>Requestor</th>
				<th>Mail</th>
				<th>Beginning</th>
				<th>Expiration</th>
				<th>Reason</th>
				<th>Requested on</th>
				<th>Status</th>
				
			</tr>
			</thead>
				<tbody id="inhibited_tbody">
			<?php
			foreach($list as $position => $row){
				echo "<tr id='td_".$row['id']."'>";
				if ($row["status"]=="archived" xor $archived=="false"){
					foreach($row as $element => $description){
						if($element!="0"){
							
							switch ($element){
								case "id":
								case "inhibited":
									//cases not to be displayed in table
									/*if($isAdminClass->isAdmin()) { 
										echo "<td><button class='delete_inhibit'   onclick=\"deleteInhibited('".$description."')\">Delete</button></td>";
									}else{
										echo "<td></td>";
									}*/
									break;
								case "requestedon":
									echo "<td class=\"requestedon\">$description</td>";
									break;
								
								case "status":


									$deleteButton="";
									$editButton="";
									$approvebutton="";
									$declineButton="";
									$archiveButton="";
									$inhibitedButton="";
									$requestRemoveButton="";
									//$linkButton="<button class=\"nonprintable disableonedit cursor inhibit_link inhibit_button\" onclick='window.open(\"https://atlas-expert-system".$dev.".web.cern.ch/atlas-expert-system".$dev."/login/templates/viewInhibited.php?uid=".$row["id"]."\")';>Link</button>";
									$linkButton="<button class=\"nonprintable disableonedit cursor inhibit_link inhibit_button\" onclick='window.open(\"viewInhibited.php?uid=".$row["id"]."\")';>Link</button>";
									$requestRemoveButton="<button class=\"nonprintable disableonedit cursor inhibit_link inhibit_button\" onclick=\"button_requestRemove('".$row["id"]."')\">Request removal</button>";
									if($isAdminClass->isAdmin()) {
										$editButton="<button class=\"nonprintable disableonedit cursor inhibit_link inhibit_button\" onclick=\"button_editRequest('".$row["id"]."')\">Edit</button>";
										$deleteButton="<button class='nonprintable disableonedit cursor delete_inhibit inhibit_button'   onclick=\"button_deleteInhibited('".$row["id"]."')\">Delete</button>";
										$declineButton="<button class=\"nonprintable disableonedit cursor inhibit_button\" onclick=\"button_declineInhibited('".$row["id"]."')\">Decline</button>";
										$archiveButton="<button class=\"nonprintable disableonedit cursor inhibit_button\" onclick=\"button_archiveInhibited('".$row["id"]."')\">Archive</button>";
										$approvebutton="<button class=\"nonprintable disableonedit cursor inhibit_button\" onclick=\"button_approveInhibited('".$row["id"]."')\">Approve</button>";
										$elisaButton="<button class=\"nonprintable disableonedit cursor inhibit_button apply_button\" style=\"display:none\" onclick=\"button_writeELISA('".$row["id"]."')\">Send to ELISA</button>";//TODO elisa
										$applyButton="<button class=\"nonprintable disableonedit cursor inhibit_button\" onclick=\"requests_applyJSON('".$row["id"]."')\">Apply to Expert System</button>";
										$triggerButton="<button class=\"nonprintable disableonedit cursor inhibit_button\" onclick=\"requests_triggerJSON('".$row["id"]."')\">Simulate Trigger</button>";
										if($row["inhibited"]=="no"){
											$inhibitedButton="<button class=\"nonprintable disableonedit cursor inhibit_button\"  onclick=\"button_setInhibit('".$row["id"]."')\">Set as DSS inhibited</button>";
										}else{
											$inhibitedButton="<button class=\"nonprintable disableonedit cursor inhibit_button\"  onclick=\"button_set_NOTinhibit('".$row["id"]."')\">Set as DSS not inhibited</button>";
										}
										
									}
									
									if ($description =="pending") {
										echo "<td><div class=\"orange centered\">Pending...</div> "
											.$linkButton
											.$approvebutton
											.$editButton
											.$declineButton
											.$archiveButton
											.$deleteButton
											.$applyButton
											.$triggerButton
											."</td>";
									}else if ($description =="approved"){
										if($row["inhibited"]=="no"){
											echo "<td><div class='greenb centered'>Approved<div class='orange centered'>Currently NOT inhibited</div></div>";
										}else{echo "<td><div class='greenb centered'>Approved</div><div class='greenb centered'>Currently inhibited</div>";}
											echo $linkButton
											.$editButton
											.$archiveButton
											.$declineButton
											.$deleteButton
											.$inhibitedButton
											.$applyButton
											.$triggerButton
											.$requestRemoveButton
											."</td>";
										
										
									}else if ($description =="declined"){
										$approvebutton="";
										if($isAdminClass->isAdmin()) $approvebutton= "<button class=\"nonprintable inhibit_button\" onclick=\"button_approveInhibited('".$row["id"]."')\">Approve</button>".$deleteButton;
										echo "<td><div class='redb centered'>Declined</div>"
										.$editButton
										.$archiveButton
										.$approvebutton
										.$applyButton
										.$triggerButton
										."</td>";
										
									}else if ($description =="archived"){
										$approvebutton="";
										if($isAdminClass->isAdmin()) $approvebutton= "<button class=\"nonprintable inhibit_button\" onclick=\"button_restoreInhibited('".$row["id"]."')\">Restore</button>".$deleteButton;
										echo "<td><div class='greyb centered'>Archived</div>".$linkButton.$editButton.$approvebutton."</td>";
									} 
									
									break;
								default:
									if(!is_array($description)){
										echo "<td class='norray $element'>".$description."</td>";
									}else{
										echo "<td>";
										foreach($description as $alarm){
											echo "<div>".$alarm."</div>";
										}
										echo "</td>";
									}
									break;
							}
						}
					}
				echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</span>
	<?php 
}else{
	echo "0 entries found";
}



?>



<script>
	
<?php
if ($archived=="false"){
	//var_dump($list);	
	?>
	js_mem_inhibited=new Array();

	<?php
	foreach($list as $position => $row){
		if($row["status"]!="archived"){
			?>
			js_mem_inhibited["<?= $row["id"]?>"]=new Array();
			<?php
			
			foreach($row as $element => $description){
				//echo $element;
				//echo $description;
				if ($description !="archived" ){
					foreach($description as $alarm){
						//echo "<div>".$alarm."</div>";

						?>
						js_mem_inhibited["<?= $row["id"]?>"].push("<?= $alarm ?>");//
						<?php
					}
				}
			}
		}//endif
	}
}
?>


process_table("inhibited", 0,0,false,{
            3: { sorter: 'date1' },4: { sorter: 'date1' },6: { sorter: 'date2' }
        });

</script>

