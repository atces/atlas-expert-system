<!-- dashboard_computation_all -->
<?php
$computation=$_POST["json"]["Computation"];
$rows_limit=10000;
//var_dump($computation);
?>
<div><button id="CSV_computation_table" onclick="export_tablesorter('dashboard_computation_table');">CSV</button></div>
<div>Currently limited to <?= $rows_limit ?></div>
<table id="dashboard_computation_table" class="white_background dashboard minitable" style="margin-left:2px">
    <thead>
        <th style="width:15px;">Cmd</th>
        <th>Object</th>
        <th>Resolution</th>
        <th>Reason</th>
    </thead>
    <tbody id="dashboard_computation_tbody" >
<?php
$counter=0;
foreach ($computation as $command){
    $counter+=1;
    if($counter>$rows_limit){
        break;
    }
    $spl_command=explode(',', $command);
    $cmd_number=intval(substr($spl_command[0],1));
    $cmd_number=$cmd_number+1;
    ?>
    <tr>
    <td><?= $cmd_number ?></td>
    <td><?= $spl_command[1] ?></td>
    <td><?= $spl_command[2] ?></td>
    <td><?= substr($spl_command[3],0,-1)?></td>
    </tr>
	<?php
}
?>
    </tbody>
</table>
<script>
  process_table('dashboard_computation');
  loading("stop");
</script>
<!-- end dashboard_computation_all -->