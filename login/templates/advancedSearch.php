<?php
/*
 * Template for advanced search tool: full table

* May 2017
* */

$json= $_POST["content"];
$keys=array_keys($json["Result"]);
$table_header=array_keys($json["Result"][$keys[0]]);
$counter=0;
//following attributes are links
$link=array("actions",
			"powers", 
			"poweredBy",
			"requiresPowerFrom","receivesPowerFrom",
			"requiresCoolingFrom","receivesCoolingFrom",
			"waterTo", 
			"requiresWaterFrom","receivesWaterFrom", 
			"requiresWaterFrom","receivesWaterFrom", 
			"gasTo", 
			"receivesGasFrom", 
			"requiresWaterFrom", 
			"groupedBy",
			"groups",
			"contains",
			"containedIn",
			"delayedactions",
			"interlocks",
			"DSU", "digitalInput", "controlledBy", "controls"
		);
?>
<script>
	function showExtraContent(obj) {
		elems = $('.as_'+obj+'_extra')
		console.log(elems)
		if(elems.first().css('display') == 'none') {
			elems.css('display','inline')
		}
		else {
			elems.css('display','none')
		}
	}
</script>
<div id="advanced_search_full_div">
	<button onclick="export_tablesorter('advanced_search_full_table');">CSV</button> Export <span class="advanced_search_full_table_row_counter"></span> <button id="reset-link" onclick="reset_table('advanced_search_full')" >Reset filters:</button>
	</div>
	<table id="advanced_search_full_table" class="tablesorter" style="width: 99% !important; margin-right:5px;font-size:smaller;">
		<?php
		echo "<thead><th data-placeholder=\"Search...\">UID</th>";
		foreach($table_header as $position =>$name){
			?>
				<th data-placeholder="Search..."><?= $name ?></th>
			<?php
		}
		echo "</thead><tbody>";
		$buffer="";
		foreach($json as $state =>$response){
			//echo "State: "+$state+"</br>";
			//echo $state;
			//echo count($state);

			foreach($response as $object => $attributes){
				//$buffer=$buffer+$object;
				//echo $object;
				$counter+=1;
				?>
				<tr>
					<td><span><?=$object?></span><a href="search.php?query=<?= $object ?>" target="_blank"><img title="Open in new tab" class="cursor newtabicon" src="../images/newtab.png" ></img></a></td>
					<?php 
					foreach($attributes as $name => $content){		

						if (!is_array($content)){
							if(in_array($name, $link)){
								?>
									<td><?= $content ?><a href="search.php?query=<?= $content ?>" target="_blank"><img title="Open in new tab" class="cursor newtabicon" src="../images/newtab.png" ></img></a></td>
								<?php 
							}else{
								?>
									<td><?= $content ?></td>
								<?php 
							}
						}else{
							?>
							<td>
							<?php 
							
							$count = 0;
							foreach($content as $contentname){
								if (strlen($contentname)==0){continue;}
								
								if(in_array($name, $link)){
									?> 
									<?= $contentname ?><a href="search.php?query=" target="_blank"><img title="Open in new tab" class="cursor newtabicon" src="../images/newtab.png" ></img></a></div>
									<?php 
								}else{
									?>
											<div><?= $contentname ?></div>
										<?php 

								}
								$count++;
							}
								
							?>
							</td>
							<?php 
						}
					}
					?>
				</tr>
				<?php
			}
		}
		?>	
		</tbody>
	</table>
	
</div>
<?php echo $counter; ?>