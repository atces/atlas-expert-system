<?php 
/*
 * Template for page 2 Racks
 * require JS/searchTools.js
 * require JS/ui.js
 *	test
 * IMPORTANT:
 * Switches and states are rendered after loading this page by javascript
 */ 

$content= $_POST["content"];
$system_classes=$_POST["system_classes"];
$page = $_POST["content"]["page"];
$pageprefix=str_replace("#","",$page)."_";
$type = $_POST["content"]["type"];
$uid = $content["uid"];
$lang = $_POST["lang"];

$dir=@scandir("../photos/".$uid);
//var_dump($dir);
$displayIcons=True;
$elements=array();


$icons=array();
$icons["switchSVG"]='<path d="m 2,13 c 0,-3 3,-6 6,-6 3,0 6,3 6,6 l 0,11 c 0,3 -3,6 -6,6 -3,0 -6,-3 -6,-6 z" fill="#14b828" stroke="#000000" stroke-width="1"></path><circle cx="8" cy="13" r="6.4" fill="#ffffff" stroke="#a9a9a9"></circle>';
$icons["switch"]="../images/switchon.png";
$icons["state"]="../images/stateon.png";
$icons["history"]="../images/history.png";
$icons["repoweredBy_switch_true"]="../images/repow_switchon.png";
$icons["repoweredBy_state_true"]="../images/repow_stateon.png";
$icons["repoweredBy_switch_false"]="../images/repow_switchoff.png";
$icons["repoweredBy_state_false"]="../images/repow_stateoff.png";
$icons["inhibit_false"]="../images/inhibit_false.png";
$icons["inhibit_true"]="../images/inhibit_true.png";

$network_classes=array("Board","Camera","Computer","Crate","NetworkRouter","NetworkSwitch","NetworkSubnet","Oscilloscope","Plcmodule","Plc","Powerstrip","Powersupply","Singleboardcomputerblade","Virtualmachine");//types_to_update_object_on_changeswitch

if ($type != "Rack" && (in_array($type, $system_classes)==1 || $type == "Alarm")){
	$system=true;
}else{
	$system=false;
}

$update_object_on_changeswitch="";
if (in_array($type,$network_classes)==1){$update_object_on_changeswitch=";update_page2and3objects();";}

echo "<!-- ";
echo "type:";
echo $type;
echo "";
echo "system:";
if ($system==true){
	echo "True";
}else{
	echo "False";
};
echo "-->";

//Classes with no need for Finder
$nofinder=array("Alarm","Action","DelayedAction","Session", "DigitalInput");
$displayfinder=False;
if(!in_array($type, $nofinder, false)){
	$displayfinder=True;
}

if(@$contest["type"]=="Action"){
?>
	<div id="exitButton"><button onClick="closePage4();">Exit</button></div>
<?php } ?>

	
	<?php 
		if ($page=="#page3"){
			?>
			<div id="page3backbutton_wrapper">
				<button id="page3slider" class='noselect cursor' onclick="hidePanel3();"><b>>></b></button> 
				<button id="page3_prev" class='page3backbutton noselect' onclick="navPage3('prev')">Back</button>
				<button id="page3_next" class='page3backbutton noselect' onclick="navPage3('next')">Next</button>
			</div>
			
			<script>
				showPanel3();
				$(document).ready(function(){
					if (position==hist.length+1) $('button#page3_prev').prop("disabled", true);
					if (position<3) $('button#page3_next').prop("disabled", true);
					
					});
					
			</script>
			<?php
		}
	?>


<div class='level1 capital'>
	<span class='object_type noselect'><?= $content["type"] ?></span><span id="description_title>"><?= $uid ?></span> 
		<svg viewBox="0 3 52 28" width="52" height="22" role="img">
			<g id="<?= $pageprefix.$uid?>sw" class="cursor element element_pointer" onclick="window.parent.ChangeSwitch('<?= $uid ?>')<?=$update_object_on_changeswitch?>"  <?php if(!$system){ echo 'visibility="hidden"'; ?><?php }?>>
				<?= $icons["switchSVG"]?>
			</g>
			<rect id="<?= $pageprefix.$uid?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
		</svg>
		
	
</div>



<!-- end of caption -->

<!-- ELEMENT DESCRIPTION INJECTED BY AJAX -->
<?php 
	if($system){ 
?>
	<div>
		<?php if($displayfinder==True){ ?>
			<figure class="caption_div">
				<a  class="nounderscore" href="#finder">
				<img src="img/search.png">
				<figcaption>
					Page finder
				</figcaption>
				</a>
			</figure>
		<?php } ?>
		<figure class="caption_div">
			<a class="nounderscore" href="probability.php?uid=<?= $uid ?>&line=parents" target="_blank">
				<img src="img/tree_parents.png">
				<figcaption>
					Parents tree
				</figcaption>
			</a>
		</figure>
		<figure class="caption_div">
			<a class="nounderscore"  href="probability.php?uid=<?= $uid ?>&line=children" target="_blank">
				<img src="img/tree_children.png">
				<figcaption>
					Children tree
				</figcaption>
			</a>
		</figure>
		<figure class="caption_div">	
			<button onclick="AddToMPCList('<?= $uid ?>')" type="submit" id="button_<?= $uid ?>">Add to List</button> 
			<figcaption>
				<a class="nounderscore"  href="getMPC.php" target="_blank">MPC</a>
			</figcaption>
		</figure>
		
		<?php if($type == "DSU") {   ?>
			<figure class="caption_div">
				<img src="img/dsu.png">
				<figcaption>
				<a  class="nounderscore" href="DSU.php?uid=<?= $uid ?>" target="_blank">DSU page</a>
				</figcaption>
			</figure>
		<?php } ?>
		</div>

	<?php } ?>
	
<div class="level2 capital" id="description_<?=$pageprefix.$uid ?>"></div>
<hr>



<!-- ELEMENT INFORMATION FROM DB-->
<?php
foreach( $content as $name => $value  ){
	$displayIcons=True;

	switch($name){
		case "uid":// all - do not show
		case "Reply":
		case "trigger_information":
		case "type":
		case "page":
		case "photo":
		case "Class":
		//case "logger":
		case "dt":
		case "Trace_exception":
		case "clientSelectionCriteria":
			break;
		
		//case "DigitalInput":
		case "commands":
		case "computation":
		case "DSU":

		//case "belongsTo":
		//case "alarms":
		
					
			?>
			<div class='level2 capital'><b><?= $name ?>:</b></div>
			<?php
				if (!(is_string($value)) and count($value)>2){ ?>
				<table  id='<?= $name ?>_table'class='minitable'>
					<?php
						$counter=1;
						foreach($value as $subname => $subvalue){
							if($subvalue !="") {
							?>
							<tr><td class="grey noselect"></td><td><span class='cursor link' onclick="search('<?= $subvalue ?>', '#page3');"> <?= $subvalue ?></span></td></tr>
						<?php 
							}
						}
					?>
				</table>
				<?php 
				}else{ 
				?>
				<table  id='<?= $name ?>_table'class='minitable'>
					<tr><td class="grey noselect"></td>
						<td><span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span></td>
						<td>
							<svg viewBox="0 3 52 28" width="52" height="22" role="img">
								<g id="<?= $pageprefix.$value?>sw" class="cursor element element_pointer" onclick="window.parent.ChangeSwitch('<?= $value ?>')<?=$update_object_on_changeswitch?>"  <?php if(!$displayIcons==True){ echo 'visibility="hidden"'; ?><?php }?>>
									<?= $icons["switchSVG"]?>
								</g>
								<rect id="<?= $pageprefix.$value?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
							</svg>
						</td>
					</tr>
				</table><?php 
				}
				?>
			<?php 
			break;
		case "affects":
			?>
					<div class='level2 capital'><b><?= $name ?>:</b></div>
					<table  id='<?= $name ?>_table'class='minitable'>
						<?php
							$counter=1;
							foreach($value as $subname => $subvalue){
								?>
								<tr><td class="grey noselect"></td><td><span class='cursor link' onclick="search('<?= $subvalue ?>', '#page3');"> <?= $subvalue ?></span></td></tr>
							<?php 
							}
						?>
					</table>
					<?php 
					break;
		
		case "link":
		case "Link":
		case "landbLink":
		case "acesLink":
		case "documentation":
			
			?>
			<div class='level2 capital'><b><?= $name ?>: </b><a class='cursor link newtab' target="_blank" href="<?= $value ?>"> <?= $value ?></a></div>
			<?php
			break;

		case "otherIds":
			?>
			<div class='level2 capital'><b>OtherIds:</b></div>
					<table id='<?= $name ?>_table'class='minitable'>
						<?php
							foreach($value as $subname => $subvalue){
								?>
								<tr><td class="grey noselect"></td><td><span><?= $subvalue ?></span></td></tr>
							<?php 
							}
						?>
					</table>

			<?php
			break;		
		
		//switch and state inverted logic: alarms, actions
		case "alarms":
		case "action":
		case "actions":
			if ($type==="Alarm"){
				//$displayIcons=False;	
			}
			if (is_array($value)){
				if(count($value)>1 ){
				?>
					<div class='level2 capital'><b><?= $name ?> <?=count($value)>5?"(".(count($value)-1).")":""?>: </b></div>
					<table  id='<?= $name ?>_table'class='minitable'>
						<?php
							$counter=1;
							foreach($value as $subname => $subvalue){
								
								if($subvalue!==""){
									array_push($elements, $subvalue);
								?>
								<tr>
									<td class="grey noselect"></td>
									<td><span class='cursor link' onclick="search('<?= $subvalue ?>', '#page3');"> <?= $subvalue ?></span></td>
									<td>
									<svg viewBox="0 3 52 28" width="52" height="22" role="img">
										<g id="<?= $pageprefix.$subvalue?>sw" class="cursor element element_pointer" onclick="window.parent.ChangeSwitch('<?= $subvalue ?>')<?=$update_object_on_changeswitch?>"  <?php if(!$displayIcons==True){ echo 'visibility="hidden"'; ?><?php }?>>
											<?= $icons["switchSVG"]?>
										</g>
										<rect id="<?= $pageprefix.$subvalue?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
									</svg>
								</tr>
							<?php 
								}
							}
						?>
					</table>
					<?php
			}elseif ($type=="Alarm" ) {
				?>
					<div class='level2 capital'><b>Actions: </b>None</div>
				<?php
			}
					
			}else{
				?>
					<div class="level2 capital"><b> <?= $name ?>: </b>
						<table  id='<?= $name ?>_table'class='minitable'>
							<tr>
								<td>
									<span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span>
									<svg viewBox="0 3 52 28" width="52" height="22" role="img">
										<g id="<?= $pageprefix.$value?>sw" class="cursor element element_pointer" onclick="window.parent.ChangeSwitch('<?= $value ?>')<?=$update_object_on_changeswitch?>"  <?php if(!$displayIcons==True){ echo 'visibility="hidden"'; ?><?php }?>>
											<?= $icons["switchSVG"]?>
										</g>
										<rect id="<?= $pageprefix.$value?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
									</svg>

								</td>
							</tr>
						</table>
					</div>
					
					<?php
					break;
			}
			break;
		case "repowered":
		case "reSuppliedAir":
			if ($value =="true"){
				$sw="repoweredBy_switch_true";
			}else{
				$sw="repoweredBy_switch_false";
			}

			?>
			<div class="level2 capital"><b> <?= $name ?>:</b>
			
				<img  repowered="<?=$value?>" <?= ($displayIcons==True)?"":"hidden "?> id="<?= $pageprefix.$value ?>sw" onclick="ChangeRealim('<?= $uid ?>','<?= $name ?>')" src="<?= $icons[$sw]?>" class="cursor element element_pointer repowered_switch cursor"> 
			</div>
			<?php
			break;
		case "deployed":
			if ($value =="yes"){
				$str_class="red";
			}else{
				$str_class="green";
			}
			
			?>
			<div class="level2 capital"><b> <?= $name ?></b>:<b><span class="<?=$str_class?>"> <?= $value ?></b></span>
			
				
			</div>
			<?php
			break;
		case "inhibit":
			if ($value =="true"){
				$sw="inhibit_true";
			}else{
				$sw="inhibit_false";
			}
			?>
			<div class="level2 capital"><b> <?= $name ?>:</b>
				<img  repowered="<?=$value?>" <?= ($displayIcons==True)?"":"hidden "?> id="<?= $pageprefix.$value ?>sw" onclick="ChangeInhibit('<?= $uid ?>','<?= $name ?>')" src="<?= $icons[$sw]?>" class="cursor element element_pointer inhibit_switch cursor"> 
			</div>
			<?php
			break;
		case "event":
				?>
				<div class='level2 capital'><b><?= $name ?></b>:
				<td><span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span></td>
				</div>
				<?php
				break;
		case "alarm":
				?>
				<div class='level2 capital'><b><?= $name ?></b>:
				<td><span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span></td>
				</div>
				<?php
				break;
		case "dsslog":
				?>
				<div class='level2 capital'><b><?= $name ?></b>:
				<td><span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span></td>
				</div>
				<?php
				break;
		case "dsslogaction":
			?>
			<div class='level2 capital'><b><?= $name ?></b>:
			<td><span class='cursor link' onclick="search('<?= $value ?>', '#page3');"> <?= $value ?></span></td>
			</div>
			<?php
			break;
		
		default://all
			if($name=="mailTo" || $name=="SMSto"){
				//$value=str_replace("  ","",$value);
				$value=trim($value," \n\r\t\v\x00");
				$value=str_replace("    ",", ",$value);
				$value=str_replace(",,","",$value);
				$value=str_replace(" , ",", ",$value);
				//$value=str_replace(" ",",",$value);
			}
			if (is_array($value)){
				if(count($value)>1){
				?>
					<div class='level2 capital'><b><?= $name ?> <?=count($value)>5?"(".(count($value)-1).")":""?>: </b></div>
					<table  id='<?= $name ?>_table'class='minitable'>
						<?php
							$counter=1;
							foreach($value as $subname => $subvalue){
								if($subvalue!==""){
									array_push($elements, $subvalue);
								?>
								<tr>
									<td class="grey noselect"></td>
									<td><span class='cursor link' onclick="search('<?= $subvalue ?>', '#page3');"> <?= $subvalue ?></span></td>
									<td>
										<svg viewBox="0 3 52 28" width="52" height="22" role="img">
										<g id="<?= $pageprefix.$subvalue?>sw" class="cursor element element_pointer" onclick="window.parent.ChangeSwitch('<?= $subvalue ?>')<?=$update_object_on_changeswitch?>"  <?php if(!$displayIcons==True){ echo 'visibility="hidden"'; ?><?php }?>>
											<?= $icons["switchSVG"]?>
										</g>
										<rect id="<?= $pageprefix.$subvalue?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
									</svg>
								</tr>
							<?php 
								}
							}
						?>
					</table>
					<?php
			}else{ // speciall cases when 0 occurrences but want to display something instead of not displaying the relationship
				if (in_array($type, $network_classes) and $name=="isReachableIn"){
				?>
					<div class="level2 capital"><b style="color:red">Not reachable</b></div>
					<?php

				}
			}
					break;
					
			}else{
				?>
					<div class="level2 capital"><b> <?= $name ?> : </b><span> <?php echo $value;?></span></div>
					<?php
					break;
			}
	}
}
if($dir!=false){
	echo "<div class='level2 capital'><b>Photos:</b></div>";
	echo "<div class='photos_wrapper'>";
	$count=0;

	$dir_list = array();


	foreach($dir as $photo) {
		if(substr($photo, 0, 10)==="thumbnail_") {
			array_push($dir_list, "photos/".$uid."/".$photo);
		}
	}
	?>
	<script type="text/javascript">var encoded_dir = <?php echo json_encode($dir_list); ?>;</script>
	<?php
	foreach($dir as $photo){
		if(substr($photo, 0, 10)==="thumbnail_"){
	?>	
		<span><a target="_blank" onclick="showImage('<?="photos/".$uid."/".$photo?>', encoded_dir)" ><img class="photo_item" height="75px" src="<?= "photos/".$uid?>/<?= $photo ?>" /></a></span>

	<?php
		}
	}
	?>

	<div id="img-modal">
		<span id="close-modal">&times;</span>
		<span id="open-full-image"><a target="_blank" href="">&#x21b3;</a></span>
		<span id="previous-image">&#60;</span>
		<span id="next-image">&#62;</span>  
		<div id="img-slot" data-scale="3.0"></div>	<!-- The zoom can be changed with the data-scale attribute -->
	</div>

	<?php
	echo "</div>";
}
	
?>
<!-- FIND ELEMENT -->
<?php
	//excluding types with no need for Finder
	if($displayfinder==True){
		?>
		<div class="level2 capital"><b>Find this element in:</b><div id="finder" class="<?= $uid?>"></div>
		<?php 
	} ?>


<hr class="blueline">


<!-- OBJECT DATA-->
<?php
if ($page == "#page3" && $type == "DelayedAction"){
	?>
	<div id="action_description"></div>
	<?php
} ?>
<div>
	<button class='get_object_schema cursor' id='get_object_schema_<?=$pageprefix.$uid?>' onClick="getVerboseSchema('<?=$pageprefix.$uid?>')">Display schema</button>
	<button class='get_object_data cursor' id='get_object_data_<?=$pageprefix.$uid?>' onClick="getVerboseData('<?=$pageprefix.$uid?>')">Display data</button>
</div>
<pre class="code_font" id="verbose_schema_<?=$pageprefix.$uid?>" ></pre>
<pre class="code_font" id="verbose_data_<?=$pageprefix.$uid?>" hidden>
	<?php
		var_dump($content);
	?>
	
</pre>

<script>
	$(document).ready(function(){
		//getSystemProperties(uid, tag, template) to get naming codification information
		getSystemProperties('<?= $uid ?>', "description_<?=$uid ?>", "properties_description");
		var type="<?= $type ?>";
		if (!(type== "Alarm" || type== "action")) GetDDVhistory('<?=$uid?>', parseDDVhistory);
		
		//adjust page to window size
		$("<?= $page ?>").height($( window ).height());
		<?php
		if ($page=="#page2"){
			echo "obj_in_page2=\"$uid\";";
		}else{
			echo "obj_in_page3=\"$uid\";";
		}
		//excluding types with no need for Finder
		if(!in_array($type, $nofinder, false)){
			?>
				finder("<?=$uid;?>", "#finder", "search", "<?=$lang;?>");
			<?php 
		} ?>
		MPC_disable_added_items("<?= $uid ?>");
	});
</script>




