
<!-- dashboard_panel_alarms -->
<?php
$json=$_POST["json"]["Result"];
 $JStag="dashboard_properties_content";
 $JStemplate="properties_dashboard";
?>
<div></div>
<table  id="dashboard_alarms_table" class="white_background dashboard minitable" style="margin-left:2px">
	<thead>
		<th colspan='3' id="alarms_table_caption" data-placeholder="Search..."></th>
	</thead>
	<?php 
	//echo "<thead><th colspan='3' class='dashboard_minitable_header' id='dashboard_minitable_$key'>Alarms</th></thead>";
	echo "<tbody id=\"body_dashboard_alarms_table\">";
	#echo "<tr><th colspan='2' class='dashboard_minitable_header'>Alarms</th></tr>";
	$counter=0;
	foreach( $json as $name => $value  ){
		$counter++;			
			?>	
		<tr>
			<td>
					<span id ="<?= $name ?>" class="cursor" onclick="getSystemProperties('<?= $name ?>', '<?= $JStag?>','<?= $JStemplate?>')" ><?= $name ?></span>
				</td>
				<td>
					<img class="cursor" src="../images/newtab.png" onclick="displayHistory('<?= $name ?>')"/>
				</td>
			</tr>
		<?php
		 }
	if(count($json)===0) echo "<tr><td>No triggered alarms</td></tr>";
	 ?>
	</tbody>
</table>

<script>
	var tableCSVButton="<button onclick=\"export_tablesorter('dashboard_alarms_table');\">CSV</button>";
	var tablecaption='Alarms <span class=\"dashboard_alarms_table_row_counter\" value=\"<?= $counter ?>\" id=\"Alarms\">(<?= $counter ?>)> </span>  ';
	var tableallcaption=tablecaption+tableCSVButton;
	tables_with_hidden_rows["dss"].push('dashboard_alarms_table');//adding table to list for button Show 
	$('#alarms_table_caption').html(tableallcaption);
	process_table('dashboard_alarms',5);
	toogleTableRows("dashboard_alarms_table", "hide");
	add_caption_if_long_table("dashboard_alarms_table");
	
</script>

<!-- end dashboard_panel_alarms -->
