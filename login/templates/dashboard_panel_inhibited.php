<!-- dashboard_panel_inhibited -->
<?php
if (array_key_exists("dashboarddata",$_POST)){
	$json=$_POST["json"]["Result"];
}
else
{
	$json=array();
}
 $JStag="dashboard_properties_content";
 $JStemplate="properties_dashboard";
?>
<div></div>

<?php
if (count($json)>1){
	?>
	<table  id="dashboard_inhibited_table" class="white_background dashboard minitable" style="margin-left:2px">
		<thead>
			<th colspan='3' id="inhibited_table_caption" data-placeholder="Search..."></th>
		</thead>
		<?php 
		//echo "<thead><th colspan='3' class='dashboard_minitable_header' id='dashboard_minitable_$key'>inhibited</th></thead>";
		echo "<tbody id=\"body_dashboard_inhibited_table\">";
		#echo "<tr><th colspan='2' class='dashboard_minitable_header'>inhibited</th></tr>";
		$counter=0;
		foreach( $json as $name => $value  ){
			$counter++;			
				?>	
			<tr>			<td>
				<td>
						<span id ="<?= $name ?>" class="cursor" onclick="getSystemProperties('<?= $name ?>', '<?= $JStag?>','<?= $JStemplate?>')" ><?= $name ?></span>
					</td>
					<td>
						<img class="cursor" src="../images/newtab.png" onclick="displayHistory('<?= $name ?>')"></img>
					</td>
				</tr>
			<?php
			 }
		if(count($json)===0) echo "<tr><td>No DSS elements inhibited</td></tr>";
		 ?>
		</tbody>
	</table>
<?php } ?>

<script>
	var tableCSVButton="<button onclick=\"export_tablesorter('dashboard_inhibited_table');\">CSV</button>";
	<?php
	if (count($json)>1){
		?>
		var tablecaption='DSS inhibits  <span class=\"dashboard_inhibited_table_row_counter\" value=\"<?= $counter ?>\" id=\"inhibited\">(<?= $counter ?>)> </span>  ';
	<?php } ?>
	var tableallcaption=tablecaption+tableCSVButton;
	tables_with_hidden_rows["dss"].push('dashboard_inhibited_table');//adding table to list for button Show 
	$('#inhibited_table_caption').html(tableallcaption);
	process_table('dashboard_inhibited',5);
	toogleTableRows("dashboard_inhibited_table", "hide");
	add_caption_if_long_table("dashboard_inhibited_table");
	
</script>

<!-- end dashboard_panel_inhibited -->
