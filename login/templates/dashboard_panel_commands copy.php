<!-- dashboard_panel_commands -->
<?php
	$json=$_POST["json"];
	$counter=0;
	//print_r($_POST["json"]);
	$actions=array();
	foreach($json as $time => $action){
		$string=str_replace("[", "", $action);
		$string=str_replace("]", "", $string);
		$string=str_replace("\"", "", $string);
		$string=str_replace("\\", "", $string);
		$commands[$time]=explode(",",$string);
	}
	 $JStag="dashboard_properties_content";
	 $JStemplate="properties_dashboard";
?>
<table  id="dashboard_commands_table" class="white_background dashboard minitable" style="margin-left:2px">
	   <thead><tr><th colspan="3">Commands</th></tr></thead>
	   <tbody id="dashboard_commads_tbody">
	<?php 
	$counter=0;
	foreach( $commands as $time => $command  ){
		if($command[0]!==""){
				$counter++;	
			?>	
		<tr <?=  ($counter>3)?"hidden":""?>>
			<td><?=$counter ?>- <span class="cursor" onclick="getSystemProperties('<?= $command[0]; ?>', '<?= $JStag?>','<?= $JStemplate?>')"><?= $command[0] ?></span></td>
			<td><?=$command[1];?></td>
		</tr>
		<?php
		}	
	}
	if(count($json)<2) {
		//var_dump($json);
		echo "<tr><td>No commands yet</td></tr>";	
	}
	 ?>
	 </tbody>
</table>
<script> 
	$('#dashboard_commands>tbody>tr>th').text($('#dashboard_commands>tbody>tr>th').text()+" ("+<?=  $counter ?>+")");
	process_table("dashboard_commands");	
</script>
<!-- end dashboard_panel_commands -->