<!-- table_saved_simulations -->
<?php 
/**
 * This file requires simulator_evaluator.js
 **/
require_once("../scripts/jsonGetter.php");
$jsonGetter = new JsonGetter;
$simulations = $jsonGetter->getSimulations();

foreach ($simulations as $simulation){
  $content=$jsonGetter->getSimulation($simulation);
  $name=(property_exists($content, "name")?$content->name:"Unknown");
  $date=(property_exists($content, "date")?$content->date:"Unknown");
  ?>
  <tr>
    <td><?=$name;?></td>
    <td><?=$date;?></td>
    <td>
      <button class="cursor" onclick='RunSimulationFile("<?= $simulation; ?>")'>Run</button> 
      <button class="cursor" onclick='DownloadSimulationFile("<?= $simulation; ?>")'>Download</button>
      <button class="cursor" onclick='DeleteSimulationFile("<?= $simulation; ?>")'>Delete</button>
    </td>
  </tr>
  <?php
}
?>
<!-- end of table_saved_simulations -->