<!-- panel_reliability_results-->
<?php
$uid=$_GET["uid"];
?>
<div><h1>Reliability analysis:</h1></div>
<br>
<table class="reliability_results_table">
    <tr>
        <th class='regform-done-caption'><span><b>Probability of failure:</b></span></th>
        <td class='regform-done-title'><span id="PoF_value_<?= $uid ?>"></span> = <span id="PoF_formula_<?= $uid ?>"></span></td>
    </tr>
    <tr>
        <th class='regform-done-caption'><b>Reliability commponents</b></span></td>
        <td class='regform-done-title'><span id="tree_formula_uids_<?= $uid ?>"></span></th>
    </tr>
    <tr>
        <th class='regform-done-caption'><span><b>Probability of survival:</b></span></th>
        <td class='regform-done-title'><span id="PoS_value_<?= $uid ?>"></span> = <span id="PoS_formula_<?= $uid ?>"></span></td>
    </tr>
    <tr>
        <th class='regform-done-caption'><span><b>PCA Analysis:</b></span></th>
        <td class='regform-done-title'><p class="left_margin"><span  id="PCA_all_<?= $uid ?>"></span></p></td>
    </tr>
    <tr>
        <th class='regform-done-caption'><span><b>PCA:</b></span></th>
        <td class='regform-done-title'><p class="left_margin"><b><span id="PCA_name_<?= $uid ?>"></span></b><span id="PCA_result_<?= $uid ?>"></span> = <span id="PCA_formula_<?= $uid ?>"></span></p></td>
    </tr>
</table>
<!-- end panel_reliability_results-->
