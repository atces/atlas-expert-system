<?php 

$group=$_POST["json"];

$groupid=$_POST["uid"];
$script="";//initializing string var for finder calls
$counter=count($group["groupTo"]);
switch($counter){
		case ($counter>50): $rows=8;break;
		case ($counter>40): $rows=4;break;
		case ($counter>20): $rows=2;break;
		default: $rows=1;break;	
}

?>
<div id="drag_<?= $groupid;?>" class='group_main_wrapper wrapper_<?= $groupid;?>'>
	
	
	<b><?= $groupid;?> </b><svg height="22" width="14" role="img"><g onclick="window.parent.displayHistory('<?= $groupid?>')" class="cursor" id="<?= $groupid?>hi"><circle cx="7" cy="7" r="7" fill="#4287f5"></circle><text fill="#ffffff" x="5" y="12">i</text></g></svg>
	<div id="header_drag_<?= $groupid;?>" class="group_dragger" >
		<!--<div id="header_drag_<?= $groupid;?>" style="background-image: url('../images/move_icon.png'); width:12px">-->
	</div>
	<button class="cursor float_right group_close" onClick="closeGroup('<?= $groupid;?>')">X</button>
	
		<?php 
		
		foreach( $group as $name => $value  ){
			switch($name){
				case "description": ?><p class='level2 capital'><?= $value ?></p><?php break;
				case "groupTo":  
					
					
					?>
					<p class='level2 capital'><b>Belonging elements (<?= count($value)-1 ?>):</b><span class="grouploading"></span></p>
					<div class="group_elements_list" style="column-count:<?= $rows ?>">
						<?php
							
							foreach($value as $subname => $subvalue){
								if(strcmp($subvalue,"")!==0){
									$counter+=1;
									?>
								<div class="group_elements_list_row">
									<span class='cursor link' onclick="displayHistory('<?= $subvalue ?>', '#page3');"> <?= $subvalue ?></span>
									<div class="<?= $subvalue ?>"  style="display:inline">
										<svg viewBox="0 3 52 28" width="52" height="22" role="img">
											<g id="<?= $subvalue?>sw" class="cursor element" onclick="window.parent.ChangeSwitch('<?= $subvalue?>')"><path d="m 2,13 c 0,-3 3,-6 6,-6 3,0 6,3 6,6 l 0,11 c 0,3 -3,6 -6,6 -3,0 -6,-3 -6,-6 z" fill="#14b828" stroke="#000000" stroke-width="1"></path><circle cx="8" cy="13" r="6.4" fill="#ffffff" stroke="#a9a9a9"></circle></g>
											<rect id="<?= $subvalue?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
											<g onclick="window.parent.displayHistory('<?= $subvalue?>')" class="cursor" id="<?= $subvalue?>hi"><circle cx="45" cy="21" r="7" fill="#4287f5"></circle><text fill="#ffffff" x="43" y="27">i</text></g>
										</svg>
									</div>
								</div>
								
								
								
							<?php
								
								}
							}
						?>
					</div>
					<?php 
					
					//Dissabled
					//echo "<script>".$script."LoadJSONCurrentState();</script>";
					

					
					break;
				
						
				
				
			}
			
		}
/*
switch($counter){
	case ($counter>20):
		echo ""
}*/
		?>
	
</div>


