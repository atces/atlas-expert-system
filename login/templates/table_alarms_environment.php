<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);
$alarms=array();
if(isset($_POST["json"])){$alarms=$_POST["json"];}
if(isset($_GET["json"])){$alarms=$_GET["json"];}
$counter=0;
?>
<!-- table_alarms_environment -->
<table id="alarms_environment_table" class="white_background dashboard minitable" style="margin-left:2px">
	<thead>
	  <tr>
	   	<th id="alarms_environment_caption" data-placeholder="Search..." >Alarm</th>
	 	<th>Status</th>
	  </tr>
	</thead>
	<tbody id="alarms_environment_tbody">
	<?php 
	$counter=0;
	foreach($alarms as $value ){
		$counter++;	
		?>	
		<tr>
			<td><?=$value;?></td>
			<td>
				<svg viewBox="0 3 52 28" width="52" height="22" role="img">
					<g id="<?= $value?>sw" class="cursor element" onclick="window.parent.ChangeSwitch('<?= $value?>')"><path d="m 2,13 c 0,-3 3,-6 6,-6 3,0 6,3 6,6 l 0,11 c 0,3 -3,6 -6,6 -3,0 -6,-3 -6,-6 z" fill="#14b828" stroke="#000000" stroke-width="1"></path><circle cx="8" cy="13" r="6.4" fill="#ffffff" stroke="#a9a9a9"></circle></g>
					<rect id="<?= $value?>st" class="element state" x="20" y="14" width="14" height="14" fill="#14b828" stroke="#000000" stroke-width="1"></rect>
					<g onclick="window.parent.displayHistory('<?= $value?>')" class="cursor" id="<?= $value?>hi"><circle cx="45" cy="21" r="7" fill="#4287f5"></circle><text fill="#ffffff" x="43" y="27">i</text></g>
				</svg>
			</td>
		</tr>
		<?php
	}
	if($counter==0) {
		echo "<tr><td colspan='2'>No alarms found</td></tr>";	
	}
	?>
	</tbody>
</table>
<script>
	var tableCSVButton="<button onclick=\"export_tablesorter('alarms_environment_table');\">CSV</button>";
	var tablecaption='Alarms <span class=\"dashboard_data\" value=\"<?= $counter ?>\" id=\"alarms_environment\">(<?= $counter ?>)<span>';
	var tableallcaption=tablecaption+tableCSVButton;
	//JS/ui.js process_table
	process_table('alarms_environment');	
	$('#alarms_environment_caption').html(tableallcaption);
</script>

<!-- end table_alarms_environment -->
