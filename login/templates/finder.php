<?php
/**
 * Ignacio.Asensi@cern.ch
 * This code gets from POST var one uid
 * calls function findInPages from functions.php pasing the uid as argument
 * prints a table with results
 * table is name of page (from menuTree.json through functions.php) and button to search page pointing uid
 *
 * Send love and other messages to 
 * Carlos.Solans@cern.ch
 **/

$uid='';//$_POST["uid_finder"];
$lang='en';
$showLang=false;
if(@$_GET["uid_finder"]){$uid=$_GET["uid_finder"];}
if(@$_GET["uid"]){$uid=$_GET["uid"];}
if(@$_POST["uid"]){$uid=$_POST["uid"];}
if(@$_POST["lang"]){$lang=$_POST["lang"];$showLang=true;}
if(@$_GET["lang"]){$lang=$_GET["lang"];$showLang=true;}

//include_once("../functions.php");
include_once("../scripts/querystring.php");
include_once("../scripts/StringLoader.php");
$isgroup=array();
	
function findInPages($needle){
	global $isgroup;
	include_once ("../data/PHPdatagrouped.php");
	$results=array();
	foreach($data as $page=>$content){
		//var_dump($content);
		//echo "Page".$page."\n";
		foreach($content as $selected => $gr){
		  if(strcmp($needle,$selected)==0){
		          //array_push($results,$page);
			//echo $gr;
			$results[$page]=$gr;
			if(strcmp($selected,$gr)!=0){
				$isgroup[$page]="(in group)";
			}else{
				$isgroup[$page]="";}
		  }
		}
	}
	return $results;
}

$strings=new StringLoader();
$strings->set_lang('en','../data/strings/strings_en.xml');
$strings->set_lang('fr','../data/strings/strings_fr.xml');
$strings->set_lang('ru','../data/strings/strings_ru.xml');
$strings->load_lang($lang);

$data=findInPages($uid);
//var_dump($data);
//$menuTree=json_decode(file_get_contents('../data/menuTree.json'), true);
?>
<table class='minitable'>
<?php 
	foreach($data as $page => $gr){
		//$parsedLocationName=($menuTree[$page]!==NULL)?$menuTree[$page]["description"]:"<span class='red'>Undocumented page</span>";
		$parsedLocationName=$strings->get($page);
		$qs=new querystring();
		$qs->set("page",$page);
		$qs->set("uid",$gr);
		if($showLang){$qs->set("lang",$lang);}
		?>
		<tr>
			<td><?=$parsedLocationName?> <?=$isgroup[$page]?></td>
			<td><button style="margin-left: 40px;" onclick='window.open("simulator.php<?=$qs->toFullString();?>","_blank")';>Go</button></td>
		</tr>
		<?php 
	}
	if (count($data)==0){
		?>
		<tr>
			<td>Element not found in any page</td>
		</tr>
		<?php 	
	}
?>
</table>
