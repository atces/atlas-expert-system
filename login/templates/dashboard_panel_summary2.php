<?php
if (array_key_exists("dashboarddata",$_POST)){
	$json=$_POST["dashboarddata"];
}
else{
	$json=array();
}
$GroupsAffected=$_POST["GroupsAffected"]["Result"];
$nodisplay=array("commands");
?>

	
	<?php
	if (count($GroupsAffected)>1){
		?>
		<table  id="dashboard_important_elements_table" class="white_background dashboard minitable2" style="margin-left:2px">
			<thead>
				<th colspan='3' id="actions_table_caption" data-placeholder="Search...">Important elements affected <button onclick="export_tablesorter('dashboard_important_elements_table');">CSV</button></th>
			</thead>
			<tbody id="dashboard_important_elements_tbody">
			<?php

			foreach( $GroupsAffected as $name => $value  ){
				?>	
				<tr><td><?= $name ?></td></tr>
				<?php
			 }
			 ?>
			 </tbody>
		</table>
		<?php } ?>

	<?php
	if (count($json)>1){
		?>

			<table  id="dashboard_summary_elements_table" class="white_background dashboard minitable2" style="margin-left:2px">
			<thead>
				<th  id="actions_table_caption" data-placeholder="Search...">Summary <button onclick="export_tablesorter('dashboard_summary_elements_table');">CSV</button></th>
				<th></th>
				
			</thead>
			<tbody id="dashboard_summary_elements_tbody">
		<?php
			foreach( $json as $name => $value  ){
				if(!in_array($name, $nodisplay )){
			?>	
				<tr><td><?= $name ?></td><td><?= $value ?></td></tr>
				<?php
			 }
			}
			 ?>
			 </tbody>
	</table>
	<?php } ?>
	<script>
		toogleTableRows("dashboard_important_elements_table", "hide");
		toogleTableRows("dashboard_summary_elements_table", "hide");
		tables_with_hidden_rows["summary"].push('dashboard_important_elements_table');//adding table to list for button Show all
		tables_with_hidden_rows["summary"].push('dashboard_summary_elements_table');//adding table to list for button Show 
		add_caption_if_long_table("dashboard_important_elements_table");
		add_caption_if_long_table("dashboard_summary_elements_table");

	</script>
		
