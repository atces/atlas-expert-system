<?php
$json=$_POST["json"];


if(count($json) <1){
    ?>
    <div>No objects were found. Simulation seems to be correct.</div>
    <?php
}else{
?>
<!-- table_general -->
<table  id="consistency_check_table" class="tablesorter" style="width:auto">
    <thead>
        <th id="consistency_check_caption" data-placeholder="Search...">Objects <button class="cursor" onclick="export_tablesorter('consistency_check_table');">CSV</button>
        </th>
        <th>Class</th>
    </thead>
    <tbody id="consistency_check_tbody">
    <?php

    foreach( $json as $name => $value  ){
        ?>  
        <tr>
            <td><?= $name ?><img title="Full description in new tab" class="cursor newtabicon" src="../images/newtab.png" onclick="displayHistory('<?= $name ?>')"></img></td>
            <td><?= $value ?></td>
        </tr>
        <?php
     }
     ?>
     </tbody>
</table>
<?php } ?>