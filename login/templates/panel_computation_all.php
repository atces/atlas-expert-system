<!-- panel_computation_all -->
<?php
$commands=$_POST["commands"];
$rows_limit=1000;
//var_dump($commands);
?>
<div><button id="CSV_computation_table" onclick="export_tablesorter('dashboard_computation_table');">CSV</button></div>


<table id="dashboard_computation_table" >
    <thead>
        <th style="width:15px;">Cmd</th>
        <th>Object</th>
        <th>Resolution</th>
        <th>Reason</th>
    </thead>
    <tbody id="computation_tbody" >
<?php
$counter=0;
foreach ($commands as $command){
    $counter+=1;
    if($counter>$rows_limit){
        break;
    }
    $spl_command=explode(',', $command);
    $cmd_number=intval(substr($spl_command[0],1));
    $cmd_number=$cmd_number+1;
    ?>
    <tr>
    <td><?= $cmd_number ?></td>
    <td><?= $spl_command[1] ?></td>
    <td><?= $spl_command[2] ?></td>
    <td><?= substr($spl_command[3],0,-1)?></td>
    </tr>
	<?php
}
?>
    </tbody>
</table>
<div>Currently limited to <?= $rows_limit ?></div>
<script>
<?php 
//if no rows remove CSV export button
if ($counter==0){
    ?>
    $("#CSV_computation_table").hide();
    <?php 
}
?>
</script>
<!-- end panel_computation_all -->