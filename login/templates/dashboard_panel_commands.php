<!-- dashboard_panel_commands -->
<?php
	$json=$_POST["json"];
	$counter=0;
	//print_r($_POST["json"]);
	$actions=array();
	foreach($json as $time => $action){
		$string=str_replace("[", "", $action);
		$string=str_replace("]", "", $string);
		$string=str_replace("\"", "", $string);
		$string=str_replace("\\", "", $string);
		$commands[$time]=explode(",",$string);
	}
	 $JStag="dashboard_properties_content";
	 $JStemplate="properties_dashboard";
?>
<table  id="dashboard_commands_table" class="white_background dashboard minitable" style="margin-left:2px">
	   <thead>
	   	<th id="commands_table_caption" data-placeholder="Search..." colspan="3"></th>
	   </thead>
	   <tbody id="dashboard_commads_tbody">
	<?php 
	$counter=0;
	foreach( $commands as $time => $command  ){
		if($command[0]!==""){
				$counter++;	
			?>	
		<tr>
			<td><?=$counter ?>- <span class="cursor" onclick="getSystemProperties('<?= $command[0]; ?>', '<?= $JStag?>','<?= $JStemplate?>')"><?= $command[0] ?></span></td>
			<td><?=$command[1];?></td>
		</tr>
		<?php
		}	
	}
	if(count($json)<2) {
		//var_dump($json);
		echo "<tr><td>No commands yet</td></tr>";	
	}
	 ?>
	 </tbody>
</table>

<script>
	var tableCSVButton="<button onclick=\"export_tablesorter('dashboard_commands_table');\">CSV</button>";
	var tablecaption='Commands  <span class=\"dashboard_commands_table_row_counter\" value=\"<?= $counter ?>\" id=\"commands\">(<?= $counter ?>) <span>  ';
	var tableallcaption=tablecaption+tableCSVButton;
	tables_with_hidden_rows["summary"].push('dashboard_commands_table');//adding table to list for button Show 
	$('#commands_table_caption').html(tableallcaption);
	process_table('dashboard_commands',5);
	toogleTableRows("dashboard_commands_table", "hide");
	add_caption_if_long_table("dashboard_commands_table");
	
	
</script>


<!-- end dashboard_panel_commands -->