
<!-- dashboard_panel_alarms -->
<?php
$json=$_POST["json"];
//var_dump($json);
?>

<table  id="calculations_table" class="white_background dashboard minitable" style="margin-left:2px">
	<thead>
		<th  id="calculations_table_caption" data-placeholder="Search..."></th>
		<th   data-placeholder="Search...">Status</th>
		<th   data-placeholder="Search...">Link</th>
	</thead>
	<?php 
	//echo "<thead><th colspan='3' class='dashboard_minitable_header' id='dashboard_minitable_$key'>Alarms</th></thead>";
	echo "<tbody id=\"calculations_table\">";
	#echo "<tr><th colspan='2' class='dashboard_minitable_header'>Alarms</th></tr>";
	$counter=0;
	foreach( $json as $name => $value  ){
		$counter++;			
			?>	
		<tr>
			<td>
					<span id ="<?= $name ?>" class="cursor"><?= $name ?></span>
				</td>
				<td>
					<?= $value ?><!--<img class="cursor" src="../images/newtab.png" onclick="displayHistory('<?= $name ?>')"/>-->
				</td>
				<td><button type="submit" onclick="window.location='view_calculation.php?uid=<?= $name ?>'">Details</button></td>
			</tr>
		<?php
		 }
	if(count($json)===0) echo "<tr><td colspan='3'>No MPC calculations found</td></tr>";
	 ?>
	</tbody>
</table>

<script>
	var tablecaption='Calculations <span class=\"calculations_table_row_counter\" value=\"<?= $counter ?>\" id=\"calculations\">(<?= $counter ?>) </span>  ';
	var tableallcaption=tablecaption;
	tables_with_hidden_rows["dss"].push('calculations_table');//adding table to list for button Show 
	$('#calculations_table_caption').html(tableallcaption);
	process_table('calculations',5);
	toogleTableRows("calculations_table", "hide");
	add_caption_if_long_table("calculations_table");
</script>
<!-- end table_calculations-->
