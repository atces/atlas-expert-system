<!-- dashboard_panel_actions -->
<?php
$json=$_POST["json"]["Result"];
$actions=array();
/*
foreach($json as $time => $action){
	$string=str_replace("[", "", $action);
	$string=str_replace("]", "", $string);
	$string=str_replace("\"", "", $string);
	$string=str_replace("\\", "", $string);
	$actions[$time]=explode(",",$string);
}
//print_r($actions);
*/
 $JStag="dashboard_properties_content";
 $JStemplate="properties_dashboard";
?>
<table  id="dashboard_actions_table" class="white_background dashboard minitable" style="margin-left:2px">
	<thead>
		<th colspan='3' id="actions_table_caption" data-placeholder="Search...">Action</th>
	</thead>
	<tbody id="dashboard_actions_tbody">
	<?php 
	#echo "<tr><th class='dashboard_minitable_header'>Actions</th></tr>";
	$counter=0;
	foreach( $json as $action => $attr){
			$counter=$counter+1;		
			?>	
		<tr>
			<td><span class="cursor" onclick="getSystemProperties('<?= $action?>', '<?= $JStag?>','<?= $JStemplate?>')"><?= $action ?></span></td>
			<td>
				<img class="cursor" src="../images/newtab.png" onclick="displayHistory('<?= $action ?>')"></img>
			</td>
			
		</tr>
		<?php
		
		 }
	if(count($json)===0) {
		echo "<tr><td>No actions</td></tr>";	
	}
	?>
	 </tbody>


</table>
<script>
	var tableCSVButton="<button onclick=\"export_tablesorter('dashboard_actions_table');\">CSV</button>";
	var tablecaption='Actions  <span class=\"dashboard_actions_table_row_counter\" value=\"<?= $counter ?>\" id=\"Actions\">(<?= $counter ?>)> </span> ';
	var tableallcaption=tablecaption+tableCSVButton;
	tables_with_hidden_rows["dss"].push('dashboard_actions_table');//adding table to list for button Show 
	$('#actions_table_caption').html(tableallcaption);
	process_table('dashboard_actions',5);
	toogleTableRows("dashboard_actions_table", "hide");
	add_caption_if_long_table("dashboard_actions_table");

</script>
<!-- end dashboard_panel_actions -->