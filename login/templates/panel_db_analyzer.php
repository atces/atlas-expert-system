<span id="tools_db_analysis_wrapper"></span>
<div class="dashboard_level">
	<span id="tools_db_analysis">Database - Simulator compare:</span>
	<div>Check how many times do database objects are present in simulator pages. This process can take several minutes depending of the number of objects in the class</div>
	<table>
		<tr>
			<td>Select a class</td><td><select id="tools_db_analysis_classname" class="ad_search_type" style="width:120px"></select></td>
		</tr>
		<tr>
			<td>Number of occurences. Empty for all</td><td style="text-align: right"><input  size="3" id="tools_db_analysis_number"></td>
		</tr>
	</table>
	<button onclick="tools_DbAnalizer();">Go</button>
	<div id="reading_db_flag"></div>

	<div id="db_progress" hidden>
	  <div id="db_Bar"></div>
	</div>
	<table  id="tools_db_analysis_table" hidden>
		<thead>
			<tr><td>Object</td><td>Pages</td></tr>
		</thead>
		<tbody id="db_analysis_tbody">
			
		</tbody>
	</table>
</div>