<?php 
$s=$_POST["json"];
//var_dump($s);
?>
<div class="CONTENT grid"> 
<!-- Dashboard options-->
			<div class="dashboard_level">
				<fieldset> <!-- this fieldset groups the whole page in one form -->
					<legend>
						<div class="level1 capital">
							<b>Calculation Started on <?= $s["datetime"] ?></b>
						</div>
					</legend>
					<div>
						<p>
						<b>Elapsed time since the failure:  </b> <?= $s["elapsedTime"] ?> min <br>
						</p>
					</div>
					<div style="display: grid; grid-template-columns: repeat(3, 1fr);">
						<div style="grid-column: 1;">
						<fieldset> 
						<legend>	
							<div class="level2 capital">
								<b>Input: Affected Systems (List <?= $s["type"] ?>)</b>
							</div>
						</legend>
							<div>
							<table  id="affected_systems_table" class="white_background dashboard minitable" style="margin-left:2px">
								<thead>
									<th   data-placeholder="Search...">Type</th>
									<th   data-placeholder="Search...">UID</th>
								</thead>
								<?php 
									echo "<tbody id=\"affected_systems_table\">";
									$counter_affected=0;
									$affected_dict=json_decode(str_replace("'","\"",$s["affected"][1]),true);
									foreach ($affected_dict as $key => $value) {
										$counter_affected++;
										if($value!=""){
		  								?>	
										<tr>
											<td>
												<span id ="<?= $key ?>" class="cursor"><?= $value ?></span>
											</td>
											<td>
												<?= $key ?>
											</td>
										</tr>
								<?php
									}
		 							}
	 							?>
									
							</table>
							</div>
						</fieldset>
						<fieldset> 
						<legend>	
							<div class="level2 capital">
								<b>Input: Inhibited Objects</b>
							</div>
						</legend>
							<div>
							<table  id="inhibited_table" class="white_background dashboard minitable" style="margin-left:2px">
								<thead>
									<th   data-placeholder="Search...">Type</th>
									<th   data-placeholder="Search...">UID</th>
								</thead>
								<?php 
									echo "<tbody id=\"inhibited_table\">";
									$counter_inhibited=0;
									$data=json_decode(str_replace("None",'"None"',str_replace("'",'"',$s["inhibitedUIDs"][1])),true);
									foreach ($data as $value => $key) {
										$counter_inhibited++;
										if($value!=""){
		  								?>	
										<tr>
											<td>
												<span id ="<?= $key ?>" class="cursor"><?= $key ?></span>
											</td>
											<td>
												<?= $value ?>
											</td>
										</tr>
								<?php
									}
		 							}
	 							?>
									
							</table>
							</div>
						</fieldset>
						</div>
						<div style="grid-column: 2; text-align: center">
							<div style="display:grid; grid-template-rows: repeat(4, auto) ">
								<span style="margin:20px;" id="status" ><b><?= $s["status"] ?></b></span>
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="164px" height="117px" viewBox="-0.5 -0.5 164 117" style="background-color: rgb(255, 255, 255);margin:auto;"><defs><linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="mx-gradient-f8cecc-1-ea6b66-1-s-0"><stop offset="0%" style="stop-color: rgb(248, 206, 204); stop-opacity: 1;"/><stop offset="100%" style="stop-color: rgb(234, 107, 102); stop-opacity: 1;"/></linearGradient></defs><rect fill="#ffffff" width="100%" height="100%" x="0" y="0"/><g><g data-cell-id="0"><g data-cell-id="1"><g data-cell-id="bI81M3q493k3-jCTVfht-109"/><g data-cell-id="bI81M3q493k3-jCTVfht-24"/><g data-cell-id="bI81M3q493k3-jCTVfht-1"><g data-cell-id="bI81M3q493k3-jCTVfht-2"><g data-cell-id="bI81M3q493k3-jCTVfht-3"/><g data-cell-id="bI81M3q493k3-jCTVfht-4"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-6"><g data-cell-id="bI81M3q493k3-jCTVfht-7"/><g data-cell-id="bI81M3q493k3-jCTVfht-8"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-10"><g data-cell-id="bI81M3q493k3-jCTVfht-11"/><g data-cell-id="bI81M3q493k3-jCTVfht-12"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-14"><g data-cell-id="bI81M3q493k3-jCTVfht-15"/><g data-cell-id="bI81M3q493k3-jCTVfht-16"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-18"><g data-cell-id="bI81M3q493k3-jCTVfht-19"/><g data-cell-id="bI81M3q493k3-jCTVfht-20"/></g></g><g data-cell-id="bI81M3q493k3-jCTVfht-22"/><g data-cell-id="bI81M3q493k3-jCTVfht-28"/><g data-cell-id="bI81M3q493k3-jCTVfht-36"/><g data-cell-id="bI81M3q493k3-jCTVfht-37"><g data-cell-id="bI81M3q493k3-jCTVfht-38"><g data-cell-id="bI81M3q493k3-jCTVfht-39"/><g data-cell-id="bI81M3q493k3-jCTVfht-40"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-41"><g data-cell-id="bI81M3q493k3-jCTVfht-42"/><g data-cell-id="bI81M3q493k3-jCTVfht-43"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-44"><g data-cell-id="bI81M3q493k3-jCTVfht-45"/><g data-cell-id="bI81M3q493k3-jCTVfht-46"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-47"><g data-cell-id="bI81M3q493k3-jCTVfht-48"/><g data-cell-id="bI81M3q493k3-jCTVfht-49"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-50"><g data-cell-id="bI81M3q493k3-jCTVfht-51"/><g data-cell-id="bI81M3q493k3-jCTVfht-52"/></g></g><g data-cell-id="bI81M3q493k3-jCTVfht-53"/><g data-cell-id="bI81M3q493k3-jCTVfht-55"/><g data-cell-id="bI81M3q493k3-jCTVfht-56"><g data-cell-id="bI81M3q493k3-jCTVfht-57"><g data-cell-id="bI81M3q493k3-jCTVfht-58"/><g data-cell-id="bI81M3q493k3-jCTVfht-59"/><g data-cell-id="bI81M3q493k3-jCTVfht-74"/><g data-cell-id="bI81M3q493k3-jCTVfht-79"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-60"><g data-cell-id="bI81M3q493k3-jCTVfht-61"/><g data-cell-id="bI81M3q493k3-jCTVfht-62"/><g data-cell-id="bI81M3q493k3-jCTVfht-75"/><g data-cell-id="bI81M3q493k3-jCTVfht-80"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-63"><g data-cell-id="bI81M3q493k3-jCTVfht-64"/><g data-cell-id="bI81M3q493k3-jCTVfht-65"/><g data-cell-id="bI81M3q493k3-jCTVfht-76"/><g data-cell-id="bI81M3q493k3-jCTVfht-81"/></g></g><g data-cell-id="bI81M3q493k3-jCTVfht-72"/><g data-cell-id="bI81M3q493k3-jCTVfht-91"/><g data-cell-id="bI81M3q493k3-jCTVfht-92"><g data-cell-id="bI81M3q493k3-jCTVfht-93"><g data-cell-id="bI81M3q493k3-jCTVfht-94"/><g data-cell-id="bI81M3q493k3-jCTVfht-95"/></g><g data-cell-id="bI81M3q493k3-jCTVfht-96"><g data-cell-id="bI81M3q493k3-jCTVfht-97"/><g data-cell-id="bI81M3q493k3-jCTVfht-98"/></g></g><g data-cell-id="bI81M3q493k3-jCTVfht-108"/><g data-cell-id="bI81M3q493k3-jCTVfht-110"/><g data-cell-id="bI81M3q493k3-jCTVfht-111"/><g data-cell-id="JuJogOcSAYNlG12OztVo-9"><g id="cell-JuJogOcSAYNlG12OztVo-9"><g><path d="M 21 50.18 Q 21 37 38 37" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 21 55.43 L 17.5 48.43 L 21 50.18 L 24.5 48.43 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-10"><g id="cell-JuJogOcSAYNlG12OztVo-10"><g><path d="M 31 82.75 Q 31 95.6 52.39 95.61" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 31 77.5 L 34.5 84.5 L 31 82.75 L 27.5 84.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-2"><g id="cell-JuJogOcSAYNlG12OztVo-2"><g><ellipse cx="17" cy="71" rx="15" ry="15" fill="url(#mx-gradient-f8cecc-1-ea6b66-1-s-0)" stroke="#b85450" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-5"><g id="cell-JuJogOcSAYNlG12OztVo-5"><g><ellipse cx="53" cy="37" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-6"><g id="cell-JuJogOcSAYNlG12OztVo-6"><g><ellipse cx="63" cy="85" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-11"><g id="cell-JuJogOcSAYNlG12OztVo-11"><g><path d="M 67 64.18 Q 67 51 84 51" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 67 69.43 L 63.5 62.43 L 67 64.18 L 70.5 62.43 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-12"><g id="cell-JuJogOcSAYNlG12OztVo-12"><g><path d="M 77 96.75 Q 77 109.6 98.39 109.61" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 77 91.5 L 80.5 98.5 L 77 96.75 L 73.5 98.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-13"><g id="cell-JuJogOcSAYNlG12OztVo-13"><g><ellipse cx="63" cy="85" rx="15" ry="15" fill="none" stroke="#b85450" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-14"><g id="cell-JuJogOcSAYNlG12OztVo-14"><g><ellipse cx="99" cy="51" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-15"><g id="cell-JuJogOcSAYNlG12OztVo-15"><g><ellipse cx="109" cy="99" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-16"><g id="cell-JuJogOcSAYNlG12OztVo-16"><g><path d="M 103 30.18 Q 103 17 120 17" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 103 35.43 L 99.5 28.43 L 103 30.18 L 106.5 28.43 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-17"><g id="cell-JuJogOcSAYNlG12OztVo-17"><g><path d="M 113 62.75 Q 113 75.6 134.39 75.61" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 113 57.5 L 116.5 64.5 L 113 62.75 L 109.5 64.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-18"><g id="cell-JuJogOcSAYNlG12OztVo-18"><g><ellipse cx="99" cy="51" rx="15" ry="15" fill="url(#mx-gradient-f8cecc-1-ea6b66-1-s-0)" stroke="#b85450" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-19"><g id="cell-JuJogOcSAYNlG12OztVo-19"><g><ellipse cx="135" cy="17" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g><g data-cell-id="JuJogOcSAYNlG12OztVo-20"><g id="cell-JuJogOcSAYNlG12OztVo-20"><g><ellipse cx="145" cy="65" rx="15" ry="15" fill="none" stroke="#82b366" stroke-width="5" pointer-events="all"/></g></g></g></g></g></g><rect id="progress_bar" width="100%" height="100%" fill="white" opacity="0.85" transform="translate(162, 0),scale(-1,1)"></rect></svg>
								<button id="stopMPC" type="submit" style="display:block;width:100px;margin: 0 auto;" onclick="callStopMPC('<?= $s["name"] ?>')">Stop</button>
							</div>
						</div>
						
						<div style="grid-column: 3">
						<fieldset> 
						<legend>	
							<div class="level2 capital">
								<b>Output: Most Probable Cause (MPC) - Single Point Failure - <?= $s["progress"] ?>% </b>
							</div>
						</legend>
							<div>
							<table  id="mpc_table" class="white_background dashboard minitable" style="margin-left:2px">
								<thead>
									<th   data-placeholder="Search...">Rank</th>
									<th   data-placeholder="Search...">Type</th>
									<th   data-placeholder="Search...">UID</th>
								</thead>
								<?php 
									echo "<tbody id=\"mpc_table\">";
									$counter_mpc=0;
									$rank=0;
									$data=json_decode(str_replace("'",'"',$s["result"][1]),true);
									foreach ($data as $value => $key) {
										$counter_mpc++;
										$rank++;
										if($value!=""){
		  								?>	
										<tr>
											<td>
												<?= $rank ?>
											</td>
											<td>
												<span id ="<?= $key ?>" class="cursor"><?= $key ?></span>
											</td>
											<td>
												<?= $value ?>
											</td>
										</tr>
								<?php
									}
		 							}
	 							?>
									
							</table>
							</div>
						</fieldset>
						<fieldset> 
						<legend>	
							<div class="level2 capital">
								<b>Output: Most Probable Cause (MPC) - Dual Point Failure - <?= $s["progress2"] ?>% </b>
							</div>
						</legend>
							<div>
							<table  id="mpc2_table" class="white_background dashboard minitable" style="margin-left:2px">
								<thead>
									<th   data-placeholder="Search...">Rank</th>
									<th   data-placeholder="Search...">Type</th>
									<th   data-placeholder="Search...">UID</th>
								</thead>
								<?php 
									echo "<tbody id=\"mpc2_table\">";
									$counter_mpc=0;
									$rank=0;
									$data=json_decode($s["result2"][0],true);
									foreach ($data as $value => $key) {
										$counter_mpc++;
										$rank=ceil($counter_mpc/2);
										if($value!=""){
		  								?>	
										<tr>
											<td>
												<?= $rank ?>
											</td>
											<td>
												<span id ="<?= $key ?>" class="cursor"><?= $key ?></span>
											</td>
											<td>
												<?= $value ?>
											</td>
										</tr>
								<?php
									}
		 							}
	 							?>
									
							</table>
							</div>
						</fieldset>
						</div>
					</div>		
				
			</div>
</div>

<script>
	var tablecaption='Affected Systems <span class=\"affected_systems_table_row_counter\" value=\"<?= $counter_affected ?>\" id=\"Affected\">(<?= $counter_affected ?>) </span>  ';
	var tableallcaption=tablecaption;
	$('#affected_systems_table_caption').html(tableallcaption);
	process_table('affected_systems',5);
	toogleTableRows("affected_systems_table", "hide");
	add_caption_if_long_table("affected_systems_table");
	var tablecaption='Inhibited <span class=\"inhibited_table_row_counter\" value=\"<?= $counter_inhibited ?>\" id=\"Inhibited\">(<?= $counter_inhibited ?>) </span>  ';
	var tableallcaption=tablecaption;
	$('#inhibited_table_caption').html(tableallcaption);
	process_table('inhibited',5);
	toogleTableRows("inhibited_table", "hide");
	add_caption_if_long_table("inhibited_table");
	var tablecaption='Inhibited <span class=\"mpc_table_row_counter\" value=\"<?= $counter_mpc ?>\" id=\"Inhibited\">(<?= $counter_mpc ?>) </span>  ';
	var tableallcaption=tablecaption;
	$('#mpc_caption').html(tableallcaption);
	process_table('mpc',5);
	process_table('mpc2',5);
	toogleTableRows("mpc_table", "hide");
	toogleTableRows("mpc2_table", "hide");
	add_caption_if_long_table("mpc_table");
	add_caption_if_long_table("mpc2_table");
	total_progress=(<?= $s["progress"] ?>+<?= $s["progress2"] ?>)/2
	$("#progress_bar").css('width', 162-1.62*total_progress);
	if ($('#status').text() == "Stopped" || $('#status').text() == "Terminated"  ){
		$('#stopMPC').prop('disabled',true);
	}
</script>		

