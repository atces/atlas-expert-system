<?php
$json=$_POST["json"]["Result"];
$JStag="dashboard_properties_content";
$JStemplate="properties_dashboard";

$classes= array();
//var_dump($json);
foreach( $json as $name => $value  ){
	if (!in_array($value, $classes)) $classes[$value]=$value; 
}

?>
<!-- dashboard_panel_impact 1 -->

<?php 
foreach ($classes as $key){
	$counter=0;
	
	?>
	<div></div>
	<table  id="<?= $key;?>_dashboard_impacted_table" class="white_background dashboard minitable" style="margin-left:2px">
		<?php 
		echo "<thead><th colspan='3' class='dashboard_minitable_header' id='dashboard_minitable_$key'></th></thead>";
		echo "<tbody id=\"$key"."_dashboard_impacted_table\">";
		foreach( $json as $name => $value  ){
			if(strcmp($value, $key)===0){
				$counter++;
				if($counter>5){$hidden=" class='impact_hidden'";}else{$hidden="";}
				?>	
			<tr>
				<td>
					<span id ="<?= $name ?>" class="cursor" onclick="getComputation('<?= $name ?>')" ><?= $name ?></span>
					<a href="probability.php?explanation=<?= $name ?>" target="_blank"><img title="Reliability analysis" class="cursor newtabicon" src="img/icon_probability.png" ></img></a>
					<img title="Full description in new tab" class="cursor newtabicon" src="../images/newtab.png" onclick="displayHistory('<?= $name ?>')"></img>
				</td>
				
			</tr>
			<?php
			}
		}

		echo "</tbody>";
		?> 
		
	</table>

	<script>
		//Fill each table caption with Title, counter and CSV button
		var tableCSVButton="<button onclick=\"export_tablesorter('<?=$key?>_dashboard_impacted_table');\">CSV</button>";
		var tablecaption='<?= $key ?> <span class=\"<?= $key ?>_dashboard_impacted_table_row_counter\" value=\"<?= $counter ?>\" id=\"<?= $key ?>\">(<?= $counter ?>) </span>  ';
		var tableallcaption=tablecaption+tableCSVButton;
		//console.log(tableallcaption);
		$('#dashboard_minitable_<?= $key ?>').html(tableallcaption);
		$("table#<?= $key;?>_dashboard_impacted_table > tbody > tr").hide().slice(0, 3).show();
		tables_with_hidden_rows["impact"].push('<?= $key ?>_dashboard_impacted_table');//adding table to list for button Show all
		process_table('<?= $key ?>_dashboard_impacted',5,1,true);
		add_caption_if_long_table("<?= $key ?>_dashboard_impacted_table");
	</script>
			<?php
}
if(count($json)===0 || $json===NULL) {
	?>
	<table  id="dashboard_impacted_table" class="white_background dashboard minitable" style="margin-left:2px">
		<thead><tr><th class='dashboard_minitable_header'>Systems</th></tr></thead>
		<tbody><tr><td>No systems affected</td></tr></tbody>
	</table>
	<script>
		process_table('dashboard_impacted',5);
		$("dashboard_show_impact").hide();
	</script>

	<?php
}
?> 
<!-- END OF dashboard_panel_impact -->
