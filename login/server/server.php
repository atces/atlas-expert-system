<?
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("../functions.php");
include ("../scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css","../");
include ("../scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("server"); 
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("../favicon.php");?>
<script src="../node_modules/jquery/dist/jquery.min.js"></script>
<script src="../node_modules/jquery-ui-dist/jquery-ui.min.js"></script>

<!-- PLEASE DO NOT DELETE THE FOLLOWING NODE!!!!!!! -->
<script class="db" src="../JS/db.js?<?=strftime("%Y%m%d%H%M%S"); ?>" folder="server"></script>
<!-- THANK YOU FOR NOT DELETING IT -->

<script src="../JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<?php $scripter->includeScripts(); ?>
<?php $styler->includeStyle(); ?>
<script>
	function GetCmd(cmd,callback){
		doRequest("../socketlogic.php?cmd="+cmd,callback);
	}

	function serverPing(){
		function parsePing(response){
			console.log(response);
			$('#server_output').stop(true,true).text("").fadeIn("fast");
            if (response["Reply"]==="Pong"){
				$('#server_output').text("Server is up").delay(3000).fadeOut("fast");	
			}else{
				$('#server_output').text("Server is down").delay(3000).fadeOut("fast");	
			}
		}
		$('#server_output').stop().text("Ping...").show();
		GetCmd("Ping",parsePing);
	}

	function serverStart(){
		function parseStart(response){
			console.log(response);
			$('#server_output').stop(true,true).text("").fadeIn("fast");
            if (response["Reply"]==="OK"){
				$('#server_output').text("Server started").delay(3000).fadeOut("fast");		
			}else{
				$('#server_output').text("Error starting server").delay(3000).fadeOut("fast");	
			}
		}
		$('#server_output').stop().text("Starting server...").show();
		GetCmd("Start",parseStart);
	}

	function serverStop(){
		function parseStop(response){
			console.log(response);
			$('#server_output').stop(true,true).text("").fadeIn("fast");
            if (response["Reply"]==="OK"){
				$('#server_output').text("Server stopped").delay(3000).fadeOut("fast");		
			}else{
				$('#server_output').text("Error stopping server").delay(3000).fadeOut("fast");	
			}
		}
		$('#server_output').stop().text("Stopping server...").show();
		GetCmd("Stop",parseStop);
	}

	function serverInit(){
		function parseInit(response){
			console.log(response);
			$('#server_output').stop(true,true).text("").fadeIn("fast");
            if (response["Reply"]==="OK"){
				$('#server_output').text("Server initialized").delay(3000).fadeOut("fast");		
			}else{
				$('#server_output').text("Error initializing server").delay(3000).fadeOut("fast");	
			}
		}
		$('#server_output').stop().text("Initializing server...").show();
		GetCmd("Init",parseInit);
	}

	function serverRestart(){
		function parseRestart(response){
			console.log(response);
			$('#server_output').stop(true,true).text("").fadeIn("fast");
            if (response["Reply"]==="OK"){
				$('#server_output').text("Server restarted").delay(3000).fadeOut("fast");		
			}else{
				$('#server_output').text("Error restarting server").delay(3000).fadeOut("fast");	
			}
		}
		$('#server_output').stop().text("Restarting server...").show();
		GetCmd("Restart",parseRestart);
	}	

	function serverGetLogs(){
	  function parseGetLogs(response){
	    console.log(response);
		$('#server_output').stop(true,true).text("").fadeIn("fast");
	    if (response["Reply"]==="OK"){
	      $('#server_output').text("Log updated").delay(3000).fadeOut("fast");
	    }else{
	      $('#server_output').text("Error updating log").delay(3000).fadeOut("fast");
	    }
	  }
	  $('#server_output').stop().text("Getting log...").show();
	  GetCmd("GetLogs",parseGetLogs);
	}

	function serverGetRLog(){
	  function parseGetRLog(response){
	    console.log(response);
		$('#server_output').stop(true,true).text("").fadeIn("fast");
	    if (response["Reply"]==="OK"){
	      $('#server_output').text("Log updated").delay(3000).fadeOut("fast");
	    }else{
	      $('#server_output').text("Error updating log").delay(3000).fadeOut("fast");
	    }
	  }
	  $('#server_output').stop().text("Getting log...").show();
	  GetCmd("GetRLog",parseGetRLog);
	}
	
	function serverGetStatus(){
	  function parseGetStatus(response){
	    console.log(response);
		$('#server_output').stop(true,true).text("").fadeIn("fast");
		$('#server_output').text(response["Reply"]).delay(10000).fadeOut("fast");		
	  }
	  $('#server_output').stop().text("Getting status...").show();
	  GetCmd("GetStatus",parseGetStatus);
	}
	


</script>
</head>
<body>

<?php include("../header.php"); ?>

	<div class="CONTENT">
		<div id="pagecontent" class="administration">
			<div id="adminpage_div">
			<br>
				<p>Hello <?php  echo $_SERVER["OIDC_CLAIM_given_name"]; ?></p>
		    <hr>
		    <div id="server_control">
			  <h3>Server control</h3>
				<button class="cursor" id="ping_server" type="button" onClick="serverPing()">Ping</button>
				<button class="cursor" id="start_server" type="button" onClick="serverStart()">Start</button>
				<button class="cursor" id="stop_server" type="button" onClick="serverStop()">Stop</button>
				<button class="cursor" id="init_server" type="button" onClick="serverInit()">Init</button>
				<button class="cursor" id="restart_server" type="button" onClick="serverRestart()">Restart</button>
				<button class="cursor" id="getstatus_server" type="button" onClick="serverGetStatus()">GetStatus</button>
				<button class="cursor" id="getlogs_server" type="button" onClick="serverGetLogs()">GetLogs</button>
				<button class="cursor" id="getrlog_server" type="button" onClick="serverGetRLog()">GetRLog</button>
				<a href="../../logs/server.log" target="_blank">
					<button  class="cursor" id="log_server" type="button" >Log <img height="12" src="../img/newtab.png"></button>
				</a>
        </div>
			  <br>
			<hr>
			<div id="server_output" style="height:50px;"></div>
			<br>
      <br>
			<hr>
			</div>
		</div>
	</div>

<?php include("../footer.php"); ?>
	
</body>
</html>
