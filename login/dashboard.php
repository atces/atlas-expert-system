<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("dashboard");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		
		<?php include ("favicon.php");?>
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
        <script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
		<script src="JS/ui.js?<?=strftime("%Y%m%d%H%M%S");?>" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<!--
    <script src="JS/explanation.js"></script>
    -->
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>

		<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
		<script id="tableFunctions_search" class="tableFunctions" src="JS/tableFunctions.js" table="computation"></script>
		
	</head>
	<body class="grey_background">  
		<?php include("header.php"); ?>
		
 				
		<div class="CONTENT grid"> 
			
			<!-- Commands  -->
			<span id="dashboard_actions_commands_wrapper"></span>
			<div class="dashboard_level">
				<span id="dashboard_alarms_caption" class="dashboard_panel_title" >Simulation</span>
				<div><button class="dashboard_button" id="dashboard_show_summary" onclick="toogleTablesDashboardMultiple('summary')">Show all</button></div>
				<span id="dashboard_commands_wrapper"></span>
				<span id="dashboard_summary_wrapper"></span>
			</div>
			
			<!-- Alarms, inhibits, actions -->
			<span id="dashboard_dss_alarms_wrapper"></span>
			<div class="dashboard_level">
				<span id="dashboard_alarms_caption" class="dashboard_panel_title" >DSS status</span>
				<div><button class="dashboard_button" id="dashboard_show_dss" onclick="toogleTablesDashboardMultiple('dss')">Show all</button></div>
				<span id="dashboard_alarms_wrapper"></span>
				<span id="dashboard_inhibited_wrapper"></span>
				<span id="dashboard_actions_wrapper"></span>
				<span id="dashboard_minimax_wrapper"></span>
			</div>
      
			<!-- Impacted elements: no show all in case they are many -->
			<span id="dashboard_actions_history_wrapper"></span>
			<div class="dashboard_level" >
				<span id="dashboard_impact_caption" class="dashboard_panel_title" >Impacted elements</span>
				<div  id="dashboard_impact_wrapper"></div>
			</div>
      
			<!-- Computation -->
			<span id="dashboard_actions_computation_wrapper"></span>
			<div class="dashboard_level">
				<span id="dashboard_computation_caption">Server computation:</span>
				<div><button class="dashboard_button" id="dashboard_show_computation" onclick="toogleTablesDashboardComputation()">Load</button></div>
				<div id="dashboard_computation_wrapper"></div>
			</div>
			
			
		</div>

		<script>
			$(document).ready(function(){
				loadDashboard();
        updateBarIcons();
			});
		</script>		
		<div class="footer">
		<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
