<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("Probability");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<?php include ("favicon.php");?>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<script src="node_modules/jspdf/dist/jspdf.min.js"></script>
		<script src="node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.min.js"></script>
		<script src="JS/exporttablepdf.js"></script>
		
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>

    <?php include("scripts/treeincludes.php");?>

	</head>
	<body class="grey_background">  
		<? include("header.php"); ?>
		<div class="CONTENT grid"> 
			<div class="dashboard_level">
				<fieldset id="dashboard_properties_right">
					<!--
					<span id="dashboard_search_uid"></span>
					<span id="dashboard_search_content"></span>
					-->
					<div>
						<span class="float_block">
							<input id="search_explanation" onkeydown = "if (event.keyCode == 13) document.getElementById('probability_btn_search').click()">
							<button id="probability_btn_search" onclick="call_draw_tree();">Search</button>
						</span>
						<span class="float_block">
							<span>Depth:
								<select id="depth" onchange="draw_tree()">
									<option value="0">1</option>
									<option value="1">2</option>
									<option value="2">3</option>
									<option value="3">4</option>
									<option value="4">5</option>
									<option value="5">6</option>
									<option value="6">7</option>
									<option value="7">8</option>
									<option value="8">9</option>
									<option value="9">10</option>
									<option value="10">11</option>
									<option value="11">12</option>
									<option value="12">13</option>
									<option value="13">14</option>
								</select>
							</span>
						</span>
						<span class="float_block">
							<span>Hierarchy:
								<select id="line" onchange="draw_tree()">
									<option value="parents">Parents</option>
									<option value="children">Children</option>
								</select>
							</span>
						</span>
						<span class="float_block">
							<span>Arrow:
								<select id="arrow" onchange="draw_tree()">
									<option value="to_parents">To parents</option>
									<option value="to_children">To children</option>
								</select>
							</span>
						</span>
						<span class="float_block">
							<span>Update tree:
								<input type="checkbox" id="update_tree"/>
							</span>
						</span>
						<div style="padding: 5px;">
							<table id="attr_select" class="pad_basic">
								<thead>
									<tr>
										<th><b>Select branches</b></th>
										<th><b>Options</b></th>
										<th><b>Style</b></th>
									</tr>
								</thead>
								<tbody>
									<tr id="tree_info">
										<td>
											<input id="attr_tree_all" type="radio" name="attr_tree" onchange="draw_tree()"	value="all" checked="checked" > <label>Full tree</label>
										</td>
										<td>
											<input id="opt_tree_show_desc" type="radio" name="info_tree" onchange="draw_tree()" value="no" checked="checked" > <label>Hide description</label><br>
										</td>
										<td><label>Orientation</label>
											<select id="orientation" onchange="draw_tree()">
												<option value="vertical">Vertical</option>
												<option value="horizontal">Horizontal</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<input id="attr_tree_only_power" type="radio" name="attr_tree" onchange="draw_tree()" value="power"> <label>Only power (direct supply)</label>
										</td>
										<td>
											<input id="opt_tree_show_desc" type="radio" name="info_tree" onchange="draw_tree()" value="yes"> <label>Show description</label>
										</td>
										<td>
											<label>UID font size</label>
											<select id="font" onchange="tree_style('font')">
												<option value="large">Large</option>
												<option value="small">Small</option>
												<option value="smaller">Smaller</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<input id="attr_tree_only_water" type="radio" name="attr_tree" onchange="draw_tree()" value="water"> <label>Only water (direct supply)</label>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<span id="dashboard_properties_uid" ></span>
					<span id="dashboard_properties_content" ></span>
					<div class="chart" id="OrganiseChart6" ></div>
					<div id="tree_draw"></div>
				</fieldset>
				<fieldset>
					<div id="tree_warning_response"></div>
					<div>
						<b>Description: </b>
						<span id="alarm_description"></span>
					</div>
					<div id="tree_json" hidden></div>
					<hr>
					<h1><b>Probability tools</b></h1>
					<button id="toggle_probability_tools" onclick="toggle_probability_tools()">Show</button>
					<div id="parameters_wrapper" hidden>
					<?php
						include_once("scripts/probability.php");
						include("templates/panel_reliability_results.php"); 
					?>
					</div>
				</fieldset>
			</div>
		</div>
		<script> 
			function draw_tree() {
				if ($('#update_tree').prop('checked')==false){return 0;}
					loading("start");
					console.log("Drawing tree");
					var depth = $("#depth").val();
          var line=$("#line").val();
					var arrow = $("#arrow").val();
					var attr = $("input[name='attr_tree']:checked").val();
					var info = $("input[name='info_tree']:checked").val();
					var font = $("#font").val();
          var opts = Array();
          opts.orientation =$("#orientation option:selected").val();
          if (line=="parents"){
						if (opts.orientation=="horizontal") opts.orientation="EAST";
						if (opts.orientation=="vertical") opts.orientation="SOUTH";
						arrow_style={
		                "stroke-width": 2,
		                "stroke": "#ccc",
		                'arrow-start': 'classic-wide-long'//'block-wide-long'
		            }
					}else if (line=="children"){
						if (opts.orientation=="horizontal") opts.orientation="WEST";
						if (opts.orientation=="vertical") opts.orientation="NORTH";
						arrow_style={
			                "stroke-width": 2,
			                "stroke": "#ccc",
			                'arrow-end': 'classic-wide-long'//'block-wide-long'
			            }
					}
          if(arrow=="to_parents"){
						arrow_style={
		                "stroke-width": 2,
		                "stroke": "#ccc",
		                'arrow-end': 'classic-wide-long',
		                'arrow-end': 'classic-wide-long'
		            }
          }
          console.log("uid: "+uid+", "
                      +"depth: "+depth+", "
                      +"attr: "+attr+", "
                      +"info: "+info+", "
                      +"opts: "+opts+", "
                      +"line: "+line+", " 
                      )
					drawObjectAttrTree(uid, depth, attr, info, opts, line, arrow_style);
          console.log("font-size: "+font);
          setTimeout(function(){$("p.node-name").css("font-size",font);},400);       
			}
      
			function toggle_probability_tools(){
				if ($("#toggle_probability_tools").text()=="Show"){
					$("#toggle_probability_tools").text("Hide");
					$("#parameters_wrapper").show();
				}else{
					$("#toggle_probability_tools").text("Show");
					$("#parameters_wrapper").hide();
				}
				getFormulaPoF($("#search_explanation").val());
			}
			
      function call_draw_tree(){
      		if ($('#update_tree').prop('checked')==false){return 0;}
				uid=$("#search_explanation").val();
				level=$("#depth").val();
				line=$("#line").val();
				draw_tree();
			}
			

      function tree_style(item){
      	if (item==="font"){
      		$("p.node-name").css("font-size",$("#font option:selected").val());
      	}
      }
      
      $(document).ready(function(){ 
				var qs = new URLSearchParams(window.location.search);
				uid="";
				line="parents";
				depth="0";
				arrow="to_children";
				orientation="vertical";
        font="smaller";
        if(line=="parents"){arrow="to_parents";}
        if(qs.has('explanation')){uid=qs.get('explanation');}
				if(qs.has('uid')){uid=qs.get('uid');}
				if(qs.has('depth')){depth=qs.get('depth')-1;}
				if(qs.has('level')){depth=qs.get('level')-1;}
				if(qs.has('hierarchy')){line=qs.get('hierarchy');}
				if(qs.has('line')){line=qs.get('line');}
				if(qs.has('arrow')){arrow=qs.get('arrow');}
				if(qs.has('font')){font=qs.get('font');}
				if(qs.has('orientation')){orientation=qs.get('orientation');}
        if(qs.has('only_power')){
				    $("input[name='attr_tree'][value='power']").prop('checked',true);
				}
				else if(qs.has('only_water')){
				    $("input[name='attr_tree'][value='water']").prop('checked',true);
				}
				if(uid=="") return;
				$("#search_explanation").val(uid);
				$("#depth").val(depth);
				$("#line").val(line);
				$("#arrow").val(arrow);
        $("#orientation").val(orientation);
				$("#font").val(font);
				draw_tree();
			});
		</script>
		<span hidden id="hidden_uid_tree"></span>
		<div class="footer">
			<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
