<?php
include_once("functions.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Server test - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"> 
<link rel="shortcut icon" href="../images/favicon.png" type="image/png">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="JS/functions.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  
<? $pagetitle="Server test"; include("header.php"); ?>

<div class="CONTENT">
<script>
  function req(){
    cmd=document.getElementById("cmd").value;
    if(cmd.indexOf("socketlogic.php")==-1){
      cmd="socketlogic.php?"+cmd+"&TokenId="+tokenid;
    }
    console.log("request cmd:"+cmd)
    doRequest(cmd, rep);
  }
  function rep(dict){
  	var s=JSON.stringify(dict);
    console.log("response: "+s);
    document.getElementById("output").value=s;
  }
</script>

<div>Type in a command and press request to get a reply</div>
<div><label>Command</label><input type="text" style="width:400px;" id="cmd"><button onclick="req()">Request</button></div>
<div><label>Reply</label><textarea id="output" style="height:200px;width:500px;"></textarea></div>

<?php include("footer.php"); ?>
</div>
</body>
</html>
