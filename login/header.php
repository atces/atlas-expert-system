<?php
include_once("functions.php");
?>
<script>
GetTokenId();
var urlpath="<?=$stage;?>/";
var lang="<?=$lang;?>";
</script>
<!-- Piwik DEPRECATED
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://piwik.web.cern.ch/" : "http://piwik.web.cern.ch/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 3883);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://piwik.web.cern.ch/piwik.php?idsite=3883" style="border:0" alt="" /></p></noscript>
End Piwik Tracking Code --> 

<?php
if (substr($_SERVER['SERVER_NAME'], 0,20)=="atlas-expert-system."){?>
<!-- Matomo -->
<script>
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://webanalytics.web.cern.ch/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '282']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->

<?php } ?>

<div id="desktopmenubutton" style="display:none">
  <div id="menupngdesktop">
    <img id="menubuttonCLOSE" src="<?php echo $stage;?>/login/img/menu.png" class="cursor" >
    <img id="menubuttonOPEN" src="<?php echo $stage;?>/login/img/menu-hide.png" class="cursor" style="display:none;">
  </div> 
  <div id="mainmenumobiletitle"><span id="main_AES">ATLAS Expert System - </span><span id="main_page_title"><?=$pagetitle;?></span></div>
  <div id="mobilecontrol" class="mobile" style="display:none">X</div>
  <div id="verbosecontrol" class="mobile" style="display:none">?</div>	
  
  <div id="menu_dashboard_wrapper">
  	<a title="Affected Alarms and Systems" id="menu_dashboard_wrapper_link" href="<?echo $stage;?>/login/dashboard.php">
    <table id="bar_icons">
      <tr><td>Alarms:</td><td id="menu_dashboard_wrapper_alarms_icon" class="red float_right margin_left_10"></td></tr>
      <tr><td>Systems:</td><td id="menu_dashboard_wrapper_sys_icon" class="red float_right margin_left_10"></td></tr>
    </table>
  	</a>
  </div>
  

  <div id="critical_error_wrapper" hidden>
  	<div id="critical_error_msg"></div>
  	<div id="critical_error_acknowledge">
  		<span>Acknowledge: </span>
  		<input id="critical_error_checkbox" type="checkbox" onclick="critical_error_acknowledge();"/>
  	</div>
  </div>
<!-- table moved to line 116
  <table>
      <tr>
        <td class="language-table"></td>
        <td>
          <select id="language_select"  class="lang-switcher" >
            <option>EN</option>
            <option>FR</option>
            <option>RU</option>
          </select>
        </td>
      </tr>
    </table> -->

  <div style="text-align:center"> 
    	     
  </div>
  <div id="reset_tokenid_bar"><button id="reset_tokenid_button" class="cursor mobilehide" onclick="resetTokenId();">Reset Session <span class="tokenid"></span></button></div>
  <!--DEPRECATED <span hidden id="coordinates_input_wrapper">Show coords<input type="checkbox" id="input_coordinates"></span>-->
  
  	<form method="GET" action="<?=$stage;?>/login/search.php" id="search_form_upperbar">
        <!-- <input id="search_header" type="submit" value="?" style="float: right;">-->
        <div style="overflow: hidden; padding-right: 1em;">
          <input id="upperbar_searchbox" name="query" type="text" size="7" value="Search..."/>
          <i class="fa fa-search cursor" aria-hidden="true"></i>
          <!--<input class="fa fa-search" type="submit" id="searchsubmit" value="">-->
        </div>
   </form>
  <div id="time_bar" class="mobilehide">
    Time (min.): 
    <select id="time_select" onchange="saveTimeOnMem()">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="10">10</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="30">30</option>
      <option value="60">60</option>
      <option value="120">120</option>
      <option value="0" selected="selected">Max</option>
    </select>	
  </div>
  <a id="diaeditor" href="https://atlas-expert-system-dev.web.cern.ch/login/data/diaeditor/?p=svgexporter_es&stealth=1#Uhttps://atlas-expert-system-dev.web.cern.ch/login/<?=$pagexml;?>" target="_blank"><button class="cursor" style="display: none;" >Edit</button></a>
  <a id="diaeditorcernbox" href="https://cernbox.cern.ch/draw-io/eos/user/a/atopes/www/atlas-expert-system-dev/login/<?=$pagexml;?>?contextRouteName=files-spaces-personal&contextRouteParams.item=eos/user/a/atopes/www/atlas-expert-system-dev/login/data/xmlgraphs" target="_blank"><button class="cursor" >Edit (cernbox)</button></a>
  <a id="reset_zoom"><button class="cursor" id="reset_zoom_button" onclick="panzoom.reset();" style="display: none;">Reset Zoom </button></a>
  <table>
      <tr>
        <td class="language-table"></td>
        <td>
          <select id="language_select"  class="lang-switcher" >
            <option>EN</option>
            <option>FR</option>
            <option>RU</option>
          </select>
        </td>
      </tr>
    </table>
</div>

<div id="mainmenudesktop" style="display:none">
  <div id="innermainmenudesktop">    
    <div class="CELL TITLE">Menu
    </div>
    <hr>
    <!-- SEARCH LOCATION INPUT DO NOT DELETE-->
    <div id="menu_string_search">
      <span id="menu_search_label">Search location</span><input type="text" size="5" onkeyup="showLocationResult(this.value)">
      <br/>
      <span id="menu_search_label">Search panel</span><input type="text" size="5" onkeyup="showPanelResult(this.value)">
    </div>
    
    
    
    <div class="ROW">
    	<div class="headerbutton_div" id="mobile_search" class="mobile"></div>
    	
    	<hr>
      Tree type: <span id="menu_switcher" class="cursor" onclick="switch_menu(this)">Advanced</span>
      <br>
		<?php
		include("menuTree.php");
		?>
	    <hr>
      
      <!--<div class="desktop" id="tokenid">Tokenid: <span class="tokenid"> </span><button id="resetTokenId" onclick="resetTokenId();">Reset</button></div>-->
    </div>
  </div>
</div>
<div id="related-pages-links" style="display:none"></div>
<span id="right_bar" style="display:none"></span>
<!-- pointing line for highlight element in simulator pages -->
<svg id="pointing_svg" height="2000" width="2000" style="display:none">
  <line id="pointing_lineY" x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(240,0,0);stroke-width:20; stroke-opacity:0.5;"></line>
  <line id="pointing_lineX" x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(240,0,0);stroke-width:20; stroke-opacity:0.5;"></line>
</svg>

<!-- page name in JS -->

<script>
    var pagetitle="<?= $pagetitle;?>";
    var menuTree={}; 
    var responses=[];
    if (pagetitle==="Server Control"){
      console.log("Server page. No loading stuff");
    }else{
      $.getJSON("data/menuTree.json", function(data){ 
        menuTree=data; 
        if (typeof fillRelatedPages === "function") {fillRelatedPages();}
	if (typeof setLabelsPreference === "function") {setLabelsPreference();}
      });

    }
      
      
  </script>
