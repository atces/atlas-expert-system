<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
$page=(isset($_GET["page"])?$_GET["page"]:"electricity");
definePage($page);
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/simulatorParser.js"></script>
 
<?php $scripter->includeScripts(); ?>
<?php $styler->includeStyle(); ?>
<?php include ("scripts/jsgraphrenderhelper.php");?>
</head>

<body onload="renderGraph('<?=$pagexml;?>')">   
<?php include("header.php"); ?>
<div style="position: fixed;left: 10px; z-index: 3;background-color:white" id="mousetracker"></div>
<div class="CONTENT">
	<div class="container" style="width:2000px">
		<table id="maintable">
			<tr>
				<td id="image_td">
					<!-- EDITOR ANCHOR  -->
					<?php require_once("DiagramEditor/main.html"); ?>
				</td>
				<td id="verbose_td">
					<div id="verbose"> </div>	
				</td>
			</tr>
			<tr>
				<td>
					<!-- EDITOR ANCHOR  -->
					<div id="distribution" style="float:left;" class="element">
						<!-- <div id="graph"></div> -->
					 </div>
					 <p><a style="text-decoration: none; color: blue;" href="pageElementTable.php?page=<?=$pageid;?>">All page elements</a></p>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<div style="height:15px;" id="blank_space"></div>
				</td>
				<td></td>
			</tr>
		</table>
	</div>

	<!-- EDITOR ANCHOR  -->
</div>

<div class="footer">
	<?php include("footer.php"); ?>
</div>
</body>	
</html>
