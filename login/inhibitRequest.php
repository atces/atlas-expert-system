<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("Requests to DSS");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<link rel="stylesheet" type="text/css" href="node_modules/jquery-ui-dist/jquery-ui.css">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/searchTools.js"></script>
<script src="JS/simulatorParser.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>


<script src="JS/inhibitRequest.js"></script>
<?php $scripter->includeScripts(); ?> 
<?php $styler->includeStyle(); ?>

<script>


$( function() {
	$( "#datepicker_beginning" ).datepicker({ dateFormat: 'dd-mm-yy', firstDay: 1 });
  $( "#datepicker_expiration" ).datepicker({ dateFormat: 'dd-mm-yy', firstDay: 1 });
});
	
$(document).ready(function(){
	loading("start");
	<?if ($tokenid==-1){?>
	  	loadInhibitTargetTable("Alarm", 1000);
	  
	<?}else{?>
	
	  loadInhibitTargetTable("Alarm", 1);
	<?}
	
	?>
		loadInhibitedTable();
    updateBarIcons();
  	
  	var edit = new URL(window.location.href).searchParams.get("edit");	
  	if(edit!==null){
  		button_editRequest(edit);
  	}
  	if (development===true)alert("This is the development version. Request should be sent to https://atlas-expert-system.web.cern.ch")
});

$.tablesorter.addParser({
    id: "date1",
    is: function (s) {
        return false;
    },
    format: function (s, table) {
        return getDate(s,1) || '';
    },
    type: "numeric"
});
$.tablesorter.addParser({
    id: "date2",
    is: function (s) {
        return false;
    },
    format: function (s, table) {
        return getDate(s,2) || '';
    },
    type: "numeric"
});

</script>
</head>
<body>
	<?php $pagetitle="Requests to DSS"; include("header.php"); ?>
	<div class="CONTENT" style="font-size: 12px;"> 
		<div id="pagecontent" class="inhibit">
			<div id="inhibit_div">
				<div> <!--style="text-size:large;font-family:arial;">-->
				<table>
					<tr>
						<td>
				<br>
				<h1>Requests to DSS</h1>
				<h2>Hello <?php  echo $_SERVER["OIDC_CLAIM_given_name"]; ?>, to send an request to DSS, please follow the following instructions: </h2>
				<ol>
					<li>Use the type select to choose the type of item</li>
					<li>Use the plus and minus signs to add / remove items</li>
					<li>Write down a reason that we can understand</li>
					<li>Click on send to the request</li>
				</ol>
				<p>We will handle your request as soon as possible. The request will only be active when displayed in green status</p>
					</td>
					<td>
						<img id="request_states" src="img/request_states.png">
					</td>
					</tr>
				</table>
			</div>
				<hr>
				
				<div id="inhibit_center">
					<div id="inhibitpage_div">
						<span id="inhibit_list">
							<b>Select type of item: </b>
							<select id="inhibit_type" style="width:120px" onchange="loadInhibitTargetTable(this.value, 0);">
								<option value="Alarm" selected>Alarms</option>
								<option value="Action">Actions</option>
								<option value="DigitalInput">Digital Inputs</option>
							</select>
								<span id="table_toinhibit_wrapper"></span>
								
						</span>
						
					</div>
				</div>	
				
				</span>
				
				<span id="inhibit_span">
					<form id="inhibit_form" >
					<textarea style="display:none" id="hidden_uid"></textarea>
						<table id="inhibit_table_form">
							<tr>
								<td>Selected</td>
								<td>	
									<textarea id="to_inhibit_textarea" rows="8" cols="50" readonly></textarea>
								</td>
							</tr>
							<tr>
								<td>Reason</td>
								<td>
								<textarea required id="to_inhibit_reason" rows="3" cols="50"></textarea>
								</td>
							</tr>
							<tr id="email_row_0">
								<td>Email (only one per box)</td>
								<td>
								<textarea onblur="checkOnlyEmail(this.value)" required class="to_inhibit_email" style="resize: none;"rows="1" cols="40"></textarea>
								<div id="email_errors"></div>
								<!--<input class="to_inhibit_email" style="resize: none;" rows="1" cols="40"/>-->
							  </td>
								<td>
								<button type="button" onclick="addNewEmailField('')">+</button>
							  </td>
							</tr>
						<tr>
								<td>Beginning date</td>
								<td>
									<input required type="text" id="datepicker_beginning">
									<select id="beginning_inhibit_hours">
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
									</select>:
									<select id="beginning_inhibit_minutes">
										<option value="00">00</option>
										<option value="15">15</option>
										<option value="30">30</option>
										<option value="45">45</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Expiration date</td>
								<td>
									<input required type="text" id="datepicker_expiration">
									<select id="expiration_inhibit_hours">
										
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
									</select>:
									<select id="expiration_inhibit_minutes">
										<option value="00">00</option>
										<option value="15">15</option>
										<option value="30">30</option>
										<option value="45">45</option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
								<button type="submit" id="inhibit_send">Send</button>
								<button type="reset" id="inhibit_reset">Reset</button>   
								</td>
							</tr>
							<tr><td></td><td><div id="confirmation_message"></div></td></tr>
						</table>
					</form>
				</span>
				<hr style="clear:both">
				<h1>Active requests:</h1>
				<span id="inhibited_list"></span>
				<br>
				<hr style="clear:both">
				<h1>Archived requests:</h1>
				<span id="archived_list"></span>
				
			</div>
			
		</div>
	</div>
	<div class="footer">
			<?php include("footer.php"); ?>
	</div>
</body>
</html>
