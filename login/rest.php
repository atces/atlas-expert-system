<?php
/***************************
 * REST interface for ACES
 * Carlos.Solans@cern.ch
 ***************************/
error_reporting(E_ALL); 
ini_set('display_errors', 1); 

//include_once("json.php");
if(isset($_POST["cmd"])){$_GET=array_merge($_GET,$_POST);}
$debug = (isset($_GET["debug"])?$_GET["debug"]:false);
	
//Parse the actions

$host="atlas-expert-system-2.cern.ch";
if (substr($_SERVER['REQUEST_URI'], 1,25)=="atlas-expert-system-dev-2"){
  $host="atlas-expert-system-cc7-2.cern.ch";
}
elseif (substr($_SERVER['SERVER_NAME'], 0,23)=="atlas-expert-system-dev"){
  $host="atlas-expert-system-dev-cc7.cern.ch";
}

/*
$host="atlas-expert-system-cc7.cern.ch";
if (substr($_SERVER['REQUEST_URI'], 1,23)=="atlas-expert-system-dev"){
  $host="atlas-expert-system-dev-cc7.cern.ch";
}
*/

$port=9998;

//$url="http://".$_SERVER['SERVER_NAME'].dirname(dirname($_SERVER['REQUEST_URI']));
$url="https://".$_SERVER['SERVER_NAME'];

function request($host, $port, $dreq){
	
	// communicate with server
	$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	socket_set_nonblock($sock);
	$time = time();
	$drep = array();
	// Connect
	while (! @socket_connect($sock, $host, $port)) {
    	$err = socket_last_error($sock);
    	// timeout
    	if ($err == 114 || $err == 115) {
        	if (time() - $time > 2) {
            	$drep["error"]="Cannot connect to server";
				return $drep;
			}
    	}
    	continue;
	}
	// block the socket
	socket_set_block($sock);
	// encode request
	//$sreq = "{" . json_encode($dreq) . "}";
  $sreq = json_encode($dreq);
	// Send the request to the socket
	if (socket_send($sock, $sreq, strlen($sreq), 0) === false) {
    	$drep["error"]="Error writing to server";
		return $drep;
	}
  $srep = "";
  // start: Read the msg size
  $nrep = intval(socket_read($sock, 8),16);
  // end: Read the msg size
  while(strlen($srep)<$nrep){
      $resp = socket_read($sock, 10000);
      $srep .= $resp;
  }

	// Decode the reply: needed in case we need to add more to the reply from the socket
	$arr=json_decode($srep,TRUE);
	$drep = array_merge($drep,$arr);
	
	// close the socket
	socket_close($sock);

	return $drep;
}

// Check Token 0
$tid="1";
$req=array("cmd"=>"CheckToken","TokenId"=>$tid);
$rep=request($host,$port,$req);
if($debug){print_r($req);print_r($rep);}

if ($rep["Reply"]=="Error"){
	// Get Token
	$req=array("cmd"=>"GetNewToken");
	$rep=request($host,$port,$req);
	$tid=$rep["TokenId"];
	if($debug){print_r($req);print_r($rep);}
}

// Send request "GetRackInfo"
$req=array("cmd"=>"GetRackInfo","uid"=>$_GET["uid"],"TokenId"=>$tid);
$rep=request($host,$port,$req);
if($debug){print_r($req);print_r($rep);}

if ($rep["Reply"]=="Error"){
	echo json_encode($rep);
	exit;
}

// Append photos
$rep["photos"]=array();
$photos = scandir("../photos/".$_GET["uid"]);
foreach ($photos as $photo){
	if($photo==".") continue;
	if($photo=="..") continue;
	if(strpos(".sys",$photo)!==false) continue;
	if(strpos($photo, 'thumbnail')!==false) continue;
	$rep["photos"][]=$url."/photos/".$_GET["uid"]."/".$photo;
}

// Reply 
if($debug){print_r($rep);}
echo json_encode($rep);

?>
