<!--
page to get the most probable scenario for a failure
get here by pressing the button Simulations->GetMPC in the search menu
after adding some (or one) item to a list
show the selected items 
-->

<!-- include other scripts -->
<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("Most Probable Cause Tool");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
$filename='data/requestsDB.json';
$contents=file_get_contents($filename);
$list= json_decode($contents, true);
?>

<!-- beginn the HTML document -->
<!DOCTYPE html>
<html lang="en">
	<!-- head 
	run several scripts
	-->
	<head>
		<title>Get MPC - ATLAS Expert System</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		
		<?php include ("favicon.php");?>
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>
    
		
		<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
		<script id="tableFunctions_search" class="tableFunctions" src="JS/tableFunctions.js" table="computation"></script>
		<script src="JS/report.js"></script>
		<style type="text/css">
			.tablesorter-blue{width: auto!important;}
		</style>	
	<!-- set the style -->

	</head>

	<!-- body -->
	<body class="white_background">  
		<? include("header.php"); ?>
		
 				
		<div class="CONTENT grid"> 
			
			<!-- Dashboard options-->
			<div class="dashboard_level">
				<fieldset> <!-- this fieldset groups the whole page in one form -->
					<legend>
						<div class="level1 capital">
							<b>Calculate the Most Probable Cause (MPC) for a failure</b>
						</div>
					</legend>
					<div>
						<p>This tool calculates the most probable cause for the failure of the listed elements.<br>
						The output of this tool is a list of faulty systems that can cause a state off in the list of items added below on this page. 
						Currently, the tool evaluates the case of one single system failure or the combination of two systems failing.
						</p>
						<p>Only one calculation per user is allowed.</p>
						<p> The calculation takes some minutes. In 5 minutes the tool delivers at least one result in 97%.   
						</p>
						<h4>Parameters</h4>
						<label for="elapsed_time">Elapsed time since the failure:</label>
						<select id="elapsed_time">
							<option value="0">>120 min</option>
							<option value="120">120 min</option>
							<option value="60">60 min</option>
							<option value="40">40 min</option>
							<option value="30">30 min</option>
							<option value="20">20 min</option>
							<option value="15">15 min</option>
							<option value="10">10 min</option>
							<option value="5">5 min</option>
							<option value="4">4 min</option>
							<option value="3">3 min</option>
							<option value="2">2 min</option>
							<option value="1">1 min</option>
						</select>
						<div><input type="checkbox" id="exhaustive_checkbox" name="exhaustive" value="exhaustive" checked><label for="exhaustive">The added items below are all the affected objects in the selected types (exhaustive list)</label></div>
						<div><input type="checkbox" id="apply_inhibits" name="apply_inhibits" value="apply_inhibits" checked>Apply all current inhibited objects to the calculation</button></div>
						<br>
						<table>
						<tr>
							<td><button id="buttonGetMPC" type="submit" onclick="callFindMPC($('#exhaustive_checkbox').is(':checked'),$('#elapsed_time').val(),$('#apply_inhibits').is(':checked'))">Start calculation...</button></td>
							<td></td>
						</tr>
						<!--
						<tr>
							<td><button id="buttonDeleteList" type="submit" onclick="DeleteListMPC('outputStatusMPC')">DeleteList</button></td><td></td>
						</tr>
						-->
					</table>
					</div>
				</fieldset>
				<fieldset>
					<legend>
						<div class="level2 capital">
							<b>Calculations</b>
						</div>
					</legend>
					<div id="calculations_table_wrapper">
						
					</div>
					<br>
					
					<p>
						
					</p>
					<p>
				
				</fieldset>
				<fieldset>
					<legend>
						<div class="level2 capital">
							<b>List of affected systems, actions, and/or alarms:</b>
						</div>
					</legend>	
					<div id="inhibitpage_div">
							<span id="inhibit_list">
								<label>Select the type:<label>
								<select id="inhibit_type" style="width:120px" onchange="loadMPCtoAddItemsTable(this.value, 0, '#table_MPC_toadd_wrapper','inhibit');">
									<!--<option value="Action">Actions</option>
									<option value="Alarm" >Alarms</option>
									<option value="DigitalInput" disabled>Digital Inputs</option>-->
								</select>
								<button id="buttonGetMPC" type="submit" onclick="addMPCItemsFromClipboard()">Add from clipboard</button>
							<td></td>
								<div style="display: inline-flex;">
									<span id="table_MPC_toadd_wrapper"></span>
									<span id="table_MPC_added_wrapper"></span>
								</div>
							</span>
					</div>
				</fieldset>
				<p id="outputChildrenLevel"></p>					
				<p id="outputParentsLevel"></p>

			</div>
		<script>
			fillClassesSelect("#inhibit_type");
			updateCalculationsTable();
			loadMPCTable("#table_MPC_added_wrapper","MPC_added");
			inhibited_objects_uids=new Array();
			<?php
				foreach($list as $row){
					if ($row["status"] === "archived" OR $row["inhibited"]==="no"){ continue;}
					foreach($row as $description){
						foreach($description as $alarm){?>
							inhibited_objects_uids.push("<?= $alarm?>");
						<?php
							}
					}
				}
			?>
		</script>
		<div class="footer">
		<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
