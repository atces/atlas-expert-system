<?php
function isAuthorised(){
  $authorised=isset($_SERVER['Shib-Session-ID']);
  if($authorised){
    $authorised=false;
    foreach( explode(";",$_SERVER['ADFS_GROUP']) as $group){
      if(in_array($group, array("atlas-gen","en-dep-cv","ep-dep-dt","hse-dep-fb"))){
        $authorised=true;
      }
    }
  }
  return $authorised;
}

function getrelpath($needle){
	$cur = dirname($_SERVER["PHP_SELF"]);
	$chk = explode("/",$cur);
	$rel = "";
  //echo "SERVER_NAME:".$_SERVER["PHP_SELF"]."\n";
  //echo "dirname:".$cur."\n";
	for($i=count($chk)-1;$i>=1;$i--){
		if($chk[$i]==$needle) break;
		$rel .= "../";
	}
  //echo "rel:".$rel."\n";
	return $rel;
}

function gobase(){
	$current = dirname($_SERVER["SERVER_NAME"]);
	$gobase="";
	for($i=1;$i<=(substr_count( $current,"/"));$i++){
	 	$gobase .= "../";
	}
	return $gobase;
}

function getstage(){
	$ret="";
	$bar=explode("/",$_SERVER['SERVER_NAME']);
	if(count($bar)>=1){$ret="/".$bar[1];}
	//return $ret;
	return $_SERVER['SERVER_NAME'];
}

function getlang(){
	$lang = 'en';
	$langs = array('en','fr','ru');
	if (isset($_GET['lang']) && in_array(strtolower($_GET['lang']),$langs)){$lang=strtolower($_GET['lang']);}
	return $lang;	
}

//!define debug
$debug=(isset($_GET["debug"])?true:false);

//!define token id
$tokenid=(isset($_GET["TokenId"])?$_GET["TokenId"]:-1);

//!define server url
$baseurl=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'];
$url = $baseurl;

//!define stage
$stage="";//getstage();

//!define 
$gologin=getrelpath("login");
$pathlogin="login";
$urllogin=$pathlogin;

//!select language
$lang=getlang();

//!load locale
$socketlogic = $pathlogin."/socketlogic.php";
include_once($gologin."scripts/StringLoader.php");
$strings = new StringLoader($gologin."data/strings/strings_".$lang.".xml");
	
$pageid="";
$pagetitle="";
$pagexml="";

function definePage($id){
  global $strings;
  global $pageid;
  global $pagetitle;
  global $pagexml;
  global $pathlogin;
  $pageid = $id;
  $pagetitle = $strings->get($pageid);
  $pagexml = "data/xmlgraphs/".$id.".xml";
  $pagexml2 ="data/xmlgraphs/".$id.".drawio";
  if(file_exists("".$pagexml2)){
    $pagexml = $pagexml2;
  }
}


