<?php
/**
 * ignacio.asensi@cern.ch
 * 
 * 
 */
class isAdmin{
	function isAdmin(){
		$administrators=json_decode(file_get_contents("../data/admin/administrators.json"),true); //administrators list
		if($administrators==null) $administrators=json_decode(file_get_contents("../../data/admin/administrators.json"),true); 
		if(isset($administrators[$_SERVER['OIDC_CLAIM_preferred_username']])) return true;
		return false;
	}
	
	function isDevpage(){
		if (strpos($_SERVER["SERVER_NAME"],"-dev") !== false){
			return true;
		}
		return false;
		
	}
} 
?>



