<?php

class Probability{
    
    public $jsonFilename='data/parameters_probability.json';
    
    public $default=array(
        'Computer'=>0.9977,
        'CoolingLoop'=>0.9918,
        'CoolingStation'=>0.9975,
        'DSU'=> 0.9997,
        'GasSystem'=>0.9834,
        'Heater'=>0.9659,
        'Magnet'=>0.958,
        'PowerSupply'=>0.9983,
        'Rack'=>0.9962,
        'SubDetector'=>0.9987,
        'sys'=>0.9943,
        'VacuumPump'=>0.9927,
        'VentilationSystem'=>0.9943,
        'WaterSystem'=>0.9932,
    );
   
    public $default1=array(
        'Computer'=>1,
        'CoolingLoop'=>1,
        'CoolingStation'=>1,
        'DSU'=> 1,
        'GasSystem'=>1,
        'Heater'=>1,
        'Magnet'=>0.9,
        'PowerSupply'=>1,
        'Rack'=>1,
        'SubDetector'=>1,
        'sys'=>1,
        'VacuumPump'=>1,
        'VentilationSystem'=>1,
        'WaterSystem'=>1,
    );
   
    public function saveDefaults(){
       $encoded=json_encode($this->default);
       $jsonFile=fopen('../data/parameters_probability.json', 'w+');
       fwrite($jsonFile, $encoded);
       fclose($jsonFile);
       return "Defaults restored";
       //return $encoded;
    }
    public function saveParameters($parameters){
        $encoded=json_encode($parameters);
        $jsonFile=fopen('../data/parameters_probability.json', 'w+');
        $so=fwrite($jsonFile, $encoded);
        fclose($jsonFile);
        return $jsonFile;//"New values saved";
        //return $parameters;
    }
    public function getProbParameters($jsonFilename){
        $file_s=file_get_contents($this->jsonFilename);
        $file_j=json_decode($file_s);
        return $file_j;
    }
    
    
    //
    //$content=readJSONFile($jsonFilename);
    
}
//$prob = new Probability();
//$prob->saveToFile();
?>