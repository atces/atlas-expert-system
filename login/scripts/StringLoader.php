<?php 

libxml_use_internal_errors(true);

class StringLoader {
  
  private $xml=NULL;
  private $url=NULL;
  private $urls=array();
  
  function __construct($url=NULL){
    if($url==NULL) return;
    $this->open($url);
  }

  function set_lang($lang,$url){
    if(!is_array($this->urls)){$this->ulrs=array();}
    $this->urls[$lang]=$url;
  }
  
  function get_url(){
    return $this->url;
  }
  
  function load_lang($lang){
    if(!array_key_exists($lang,$this->urls)) return;
    $this->open($this->urls[$lang]);
  }
  
  function open($url){
  
    $this->url=$url;
    $this->xml=simplexml_load_file($url);
  
    if ($this->xml === false) {
      echo "Failed loading XML: ";
      foreach(libxml_get_errors() as $error) {
        echo "<br>", $error->message;
      }
    }
  }
  
  function get($str){
    $ret=$str;
    if(property_exists($this->xml,$str)){$ret = $this->xml->$str;}
    $ret = htmlentities($ret,ENT_IGNORE,"UTF-8");
    return $ret;
  }
    
}

?>