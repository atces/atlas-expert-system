<?php

class jsonGetter{
	
	private $dir="../data/simulations/";
		
   public function getSimulations(){
        $files_array=array();
        
        $files = scandir($this->dir);
        foreach ($files as $file){
          if (substr($file, 0, 1) == ".") continue; // don't list hidden files
          if (substr($file, 0, 7) == "panels") continue; // don't list panels folder
          array_push($files_array,$file);
        }
        return $files_array;
    }
    public function getSimulation($name){
        $file_s=file_get_contents($this->dir.$name);
        $file_j=json_decode($file_s);
        return $file_j;
    }

    public function saveToJSON($content, $filename, $path, $type){
        $encoded=json_encode($content);
        $file=$path.$filename.$type;
        $jsonFile=fopen($file, 'w+');
        $so=fwrite($jsonFile, $encoded);
        fclose($jsonFile);
        return $file;//"New values saved";
        //return $content;
    }
}
?>