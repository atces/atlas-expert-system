<?php 

/*
 * StyleHelper
 * Wrapper for Mobile_Detect class 
 * Helps to reduce the number of lines of code
 * Author: Carlos.Solans@cern.ch
 * July 2017
 */

include_once("Mobile_Detect.php");

class StyleHelper{
    
    private $mobile_detect;
    private $desktop_css;
    private $mobile_css;
    
    /**
     * Constructor
     * @param desktop_css path to the desktop css
     * @param mobile_css path to the mobile css
     */
    public function __construct($desktop_css, $mobile_css,$basedir=""){
        $this->mobile_detect = new Mobile_Detect();
        $this->mobile_css = $mobile_css;
        $this->desktop_css = $desktop_css;
        $this->basedir = $basedir;
    }
    
    /**
     * includeStyle
     * prints the html code necessary to include the mobile or desktop css
     */
    public function includeStyle(){
        $str = '<link rel="stylesheet" type="text/css" href="'.$this->basedir;
        if ($this->mobile_detect->isMobile()){ $str.=$this->mobile_css; }
        else {$str.=$this->desktop_css; }
        $str.='">'."\n";
		    $str.="<link rel=\"stylesheet\" href=\"".$this->basedir."css/font-awesome-4.7.0/css/font-awesome.min.css\">\n";
        print $str;
    }
    
}
?>
