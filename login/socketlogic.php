<?php
header("Access-Control-Allow-Origin: *");
include_once("json.php");
// merge POST into GET to access all parameters uniformly - is this a good idea? // Flo
if(isset($_POST["cmd"])){$_GET=array_merge($_GET,$_POST);}
$debug = (isset($_GET["debug"])?$_GET["debug"]:false);

//Parse the actions
$backend_host="atlas-expert-system-el9.cern.ch";
$frontend_host=explode(".",$_SERVER['SERVER_NAME'])[0];

if($debug){ echo "Connect: ".$frontend_host."<br>\n";}

// Set the backend host based on the frontend host
if ($frontend_host=="atlas-expert-system-dev"){
  $backend_host="atlas-expert-system-dev-el9.cern.ch";
}
elseif ($frontend_host=="atlas-expert-system-dev2"){
  $backend_host="atlas-expert-system-dev2-el9.cern.ch";
}

// check if the cmd paramter is in _GET
if(isset($_GET["cmd"])){ 
	$dreq=array();
	$dreq["cmd"]="";
	$dreq["TokenId"]=0;
    // copy all key-value pairs from GET to dreq
	foreach ($_GET as $key => $value) {
	  $dreq[$key]=$value;
	}
    $cmds=array(
        "RunSimulationFromDB",
        "GetSimulations",
        "SaveSimulation",
        "GetElementsSubsystems",
        "GetMPCCalculationDetails",
        "GetMPCCalculationAndStatus",
        "FindMPC",//Used in the MPC calculations context
        "StopMPC",//Used in the MPC calculations context
        "UpdateSimulationDB",
        "FindCauses",
        "CheckStateAllObjects",
        "GetAttrObjectParentsTree",
        "GetMinimaxDeployed",
        "GetInhibitedItems",
        "triggerSniffers",
        "GetSummaryGroupsAffected",
        "ChangeAttribute",
        "ChangeBoolAttribute",
        "ChangeBoolJSONAttribute",
        "CheckSimulationJSON",
        "GetSimulationJSON",
        "GetSimulationJSON2",
        "GetIntervenedSystemsWithLocation",
        "GetAffectedSystemsWithLocation",
        "GetFullTableSearch",
        "GetObjectParentsTree",
        "GetObjectChildrenTree",
        "GetAlarmTree",
        "GetSysClasses",
        "GetReliabilityOfClass",
        "GetFormulaPoF",
        "TestingCMD",
        "GetAttributeCounts",
        "GetComputationAll",
        "GetComputation",
        "GetAlarmsAffectingPage",
        "InhibitAllActions",
        "isSystem",
        "ToggleAlarm",
        "UpdateMenuTree",
        "GetJSONObjectsClassAndLocation",
        "GetCommandsHistory",
        "GetActiveActions",
        "GetActiveInputs",
        "GetCommandsHistoryCleared",
        "GetAllObjectsOfAClass",
        "GetElementsDescription",
        "GetNoClassObjectDescription",
        "GetAffectedSystems",
        "GetDDVTree",
        "GetDDVhistory",
        "GetClassSchema",
      	"GetFullDSU",
        "AdvancedSearchObjectFullTable",
        "AdvancedSearchObject",
        "GetNoClassObject",
        "GetPoF",
        "Ping",
        "CheckToken",
        "GetAllObjects",
        "GetClasses",
        "RemoveDelayedAction",
        "GetTriggeredAlarms",
        "GetNumberOfAffectedSystems",
        "UnTriggerAlarm",
        "TriggerAlarm",
        "SearchObject",
        "GetClass",
        "GetSchema",
        "GetNewToken",
        "GetCurrentState",
        "GetCurrentStateFull",
        "SaveCurrentState",
        "GetStateList",
        "GetSavedState",
        "GetSummaryOfObjects",
        "ChangeState",
        "ChangeSwitch",
        "isAValidUID",
        "FindObject",
        "InvokeAlarm",
        "Shutdown",
	    "SetSwitchNoDB",
        "GetLog",
        "GetLogJSON", 
        "GetObjsClasses", 
        "ReadDisplayEventsLogs", # for alarm helper"
        "WriteDisplayEventsLogs", # for alarm helper
        "GetAlarmStats", # for alarm helper
        "MatchLogsLogactions",
        "CreateSlides"
	);
                
    $server_cmds=array("Start",
                "Stop",
                "Init",
                "Restart",
                "GetLogs",
                "GetRLog",
                "GetStats",
                "GetStatus");
  
    if (in_array($_GET["cmd"], $cmds)) {
        
        // communicate with server
        $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_nonblock($sock);
        $time = time();
        // Connect
        if($debug){ echo "Connect: ".$backend_host.":9998";}
        while (! @socket_connect($sock, $backend_host, 9998)) {
            $err = socket_last_error($sock);
            // timeout
            if ($err == 114 || $err == 115) {
                if (time() - $time > 2) {
                    echo "{\"error\":\"Connection time out\"}";
                    exit();
                }
            }
            continue;
        }
        // block the socket
        socket_set_block($sock);
        // encode request
        $sreq = json_encode($dreq);
        // debug output
        if ($debug)
            echo $sreq;
        // Send the request to the socket
        if (socket_send($sock, $sreq, strlen($sreq), 0) === false) {
            echo "{\"error\":\"Read time out\"}";
            exit();
        }
        // Read the reply from the socket
        $srep = "";
        // start: Read the msg size
        $nrep = intval(socket_read($sock, 8),16);
        // end: Read the msg size
        /*
        while ($resp = socket_read($sock, 10000)) {
            $srep .= $resp;
            if (substr($resp, - 1) == "}") {
                break;
            }
        }
        */
        while(strlen($srep)<$nrep){
            $resp = socket_read($sock, 10000);
            $srep .= $resp;
        }
        // debug output
        if ($debug){ echo "Total size: ".strlen($srep); }
        // This is needed in case we need to add more to the reply from the socket
        // decode the reply
        // $drep = jdecode($srep);
        // close the socket
        socket_close($sock);
        if (strlen($srep) == 0) {
            echo '{"Reply":"Server connection lost"}';
        } else {
            echo $srep;
        }
} else if (in_array($_GET["cmd"], $server_cmds)) {
        
        // create new socket
        $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_nonblock($sock);
        $time = time();
        // Connect
        if($debug){ echo "Connect: ".$backend_host.":9999";}
        while (! @socket_connect($sock, $backend_host, 9999)) { // it's another port
            $err = socket_last_error($sock);
            // timeout
            if ($err == 114 || $err == 115) {
                if (time() - $time > 2) {
                    echo "{\"error\":\"Connection time out\"}";
                    exit();
                }
            }
            continue;
        }
        // block the socket
        socket_set_block($sock);
        // encode request
        $sreq = strtolower($_GET["cmd"]);
        // debug output
        if ($debug)
            echo $sreq . "<br>\n";
        // Send the request to the socket
        if (socket_send($sock, $sreq, strlen($sreq), 0) === false) {
            echo "{\"error\":\"Read time out\"}";
            exit();
        }
        // Get the message size
        $srep = socket_read($sock, 8);
        $esz = intval($srep, 16);
        if ($debug){echo "Msg Size: " . $esz . "<br>\n";}
        // Read the reply from the socket
        // $srep = socket_read($sock,1000000);
        $srep = "";
        $i = 0;
        $timeout = 5;
        $start = time();
        $msz = 0;
        $readlog = false;
		if ($sreq == "getlogs" || $sreq == "getrlog"){$readlog=true;}
        if ($readlog){$srep="OK";$fw = fopen('../logs/server.log', 'w');if(!$fw){$srep="Error opening log file";}}
        while ($msz < $esz) {
            if (time() - $start >= $timeout) {
                $srep="Log updated with timeout";
                if ($readlog){
                    fwrite($fw, "timeout reading...");
                }
                break;
            }
            $buf = "";
            $nb = socket_recv($sock, $buf, 4, 0);
            if ($nb <= 0 || $nb === false) {
                if ($debug) {
                    echo "break reading:" . socket_strerror(socket_last_error());
                }
                break;
            }
            if ($debug){echo $i ++ . ": " . $buf . "<br>\n";}
            
            $msz += strlen($buf);
            if ($readlog){
                fwrite($fw, $buf);
            }else{
                $srep .= $buf;
            }
        }
        if ($debug){echo "msz=" . $msz . ";esz=" . $esz . "<br>\n";}
        if ($readlog){fclose($fw);}
        // debug output
        if ($debug){echo $srep;}
        // close the socket
        socket_close($sock);
        if (strlen($srep) == 0) {
            echo '{"Reply":"Server connection lost"}';
        } else if ($sreq == "getstatus") {
            echo '{"Reply":"' . $srep . '"}';
        } else {
            echo '{"Reply":"OK"}';
        }
    }
    
    exit();
}

?>
