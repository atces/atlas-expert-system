<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("search");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<link href="css/theme.bootstrap.css" rel="stylesheet">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js"  id="ui"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="JS/searchTools.js"></script>
<script src="JS/history.js"></script>
<script src="JS/checkUrl.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-scroller.min.js"></script>
<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
<script src="JS/simulatorParser.js"></script>
<script src="data/codification.js"></script>
<?php $styler->includeStyle(); ?>
<?php $scripter->includeScripts(); ?>
<script>
	function initSearch(uid){
    if (uid==undefined){uid="";}
	    loading("start");
        SearchObject(uid,parseSearchPage);
        //GetFullTableSearch(parseSearchPage);
        return false;
    }
	
	function searchOnEnter(event){
	  if (event.keyCode == 13){ initSearch();}
	}
	
  function parseSearchPage(json){
    var counter=0;
	 	var type="";
	 	var element="";
    if (json.hasOwnProperty("error")){
      GetToken(initSearch);
      return;
    }
    var text="";
    likehoods=Object.keys(json["Result"]);//.sort()
    	for(l in json["Result"]){
			for (k in json["Result"][l]){
	            if(json["Result"][l][k]){
					element=k;
					type=json["Result"][l][k];
					text+='<tr><td>'+l+'</td><td class="cursor" onclick="search(\''+k+'\', \'#page2\')"><a>'+element+'</a><div id="'+element+'"></div></td><td>'+type+'</td></tr>';
					counter++;	
				}
			}	    
		}
		if (counter===1){
			search(element,type,'#page2')			
		}
		console.log("parseSearchPage");
		document.getElementById("search_tbody").innerHTML=text;
		//$("#search_table").trigger("update").trigger("applyWidgets");//.trigger("appendCache")
		process_table("search");
		process_smalltable("search",$( window ).height());
		if ($("#search_tbody > tr > td.cursor").length ===1){
			$("#search_tbody > tr > td.cursor").click();
		}
		loading("stop");
		//fitTable();
	 }
</script>
</head>
<body onload="AdvancedSearchDefault();">
<?php include("header.php"); ?>	
<?php include("searchbar.php"); ?>
<div class="CONTENT notopmargin">
	<div id="result"></div>
	<div id="search_center">
		<div id="searchpage_div">
			<span id="search_list">
				<input  hidden class="getsearch" type="search" data-column="1">
				
				<table id="search_table" class="tablesorter">
					<thead>
						<th class="first-name filter-select" ><span title="Index of similarity">I</span></th>
						<th data-placeholder="Search...">Items <span class="search_table_row_counter"></span><button onclick="export_tablesorter('search_table');">CSV</button></th>
						<th class="first-name filter-select" data-placeholder>Type</th>
					</thead>
					<tbody id="search_tbody"></tbody>
				</table>
			</span>
			<div id="page_wrapper">
				<span id="page2"></span>
				<span id="page3"></span>
			</div>
		</div>
	</div>
	<div id="ad_search_full_wrapper"></div>
	<div id="debug"></div>
</div>
<script>
<?if (@isset($_GET["query"])){?>
	initSearch("<?=$_GET["query"];?>");
	$(document).ready(function(){	
		$('#search_input').val("<?=$_GET["query"];?>");
	});
<?}else{?>
	initSearch();
<?}?>
  updateBarIcons();
</script>
	
<div class="footer">
<?php include("footer.php"); ?>
</div>
</body>
</html>
