<?php include_once("functions.php");?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $gologin;?>apple-touch-icon.png?v=QEGA6WQdyP">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $gologin;?>favicon-32x32.png?v=QEGA6WQdyP">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $gologin;?>favicon-16x16.png?v=QEGA6WQdyP">
<link rel="manifest" href="<?php echo $gologin;?>site.webmanifest">
<link rel="mask-icon" href="<?php echo $gologin;?>safari-pinned-tab.svg?v=QEGA6WQdyP" color="#3a81c7">
<link rel="shortcut icon" href="<?php echo $gologin;?>favicon.ico?v=QEGA6WQdyP">
<meta name="apple-mobile-web-app-title" content="ATLAS Expert System">
<meta name="application-name" content="ATLAS Expert System">
<meta name="msapplication-TileColor" content="#3a81c7">
<meta name="msapplication-TileImage" content="<?php echo $gologin;?>mstile-144x144.png?v=QEGA6WQdyP">
<meta name="theme-color" content="#3a81c7">
<meta name="msapplication-config" content="<?php echo $gologin;?>browserconfig.xml">

