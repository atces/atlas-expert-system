//import puppeteer from 'puppeteer';
const puppeteer = require('puppeteer');
const socket_logic = require('./socket_logic');
const http = require('http');
const url = require('url');
const exec = require('child_process').exec;
const path = require('path');


(async () => {

	const xmlFilename=process.argv[2];
	const svgFilename=xmlFilename.replace("drawio","svg").replace(".xml",".svg");

	const minimal_args = [
		'--disable-gpu',
		'--autoplay-policy=user-gesture-required',
		'--disable-background-networking',
		'--disable-background-timer-throttling',
		'--disable-backgrounding-occluded-windows',
		'--disable-breakpad',
		'--disable-client-side-phishing-detection',
		'--disable-component-update',
		'--disable-default-apps',
		'--disable-dev-shm-usage',
		'--disable-domain-reliability',
		'--disable-web-security',
		'--disable-extensions',
		'--disable-features=AudioServiceOutOfProcess',
		'--disable-hang-monitor',
		'--disable-ipc-flooding-protection',
		'--disable-notifications',
		'--disable-offer-store-unmasked-wallet-cards',
		'--disable-popup-blocking',
		'--disable-print-preview',
		'--disable-prompt-on-repost',
		'--disable-renderer-backgrounding',
		'--disable-setuid-sandbox',
		'--disable-speech-api',
		'--disable-sync',
		'--hide-scrollbars',
		'--ignore-gpu-blacklist',
		'--metrics-recording-only',
		'--mute-audio',
		'--no-default-browser-check',
		'--no-first-run',
		'--no-pings',
		'--no-sandbox',
		'--no-zygote',
		'--password-store=basic',
		'--use-gl=swiftshader',
		'--use-mock-keychain',
	];

  // Launch the browser and open a new blank page
	const browser = await puppeteer.launch({headless: true,
							args: minimal_args,
							userDataDir: './puppeteer_user_data'});

	const page = await browser.newPage();


	const hostname = '127.0.0.1'; // Localhost (replace with your actual host if needed)
	const port = 3000; // Port number
	const server = http.createServer(async (req, res) => {
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/plain');
	var parsedURL=url.parse(req.url,true);
	const dataToSend = JSON.parse(JSON.stringify(parsedURL.query));
	var response= await socket_logic.connectAndSend(dataToSend);
	res.end(JSON.stringify(response));
	});

	server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
	});

	//Print the message from the browser console and send an alive to the pluggin
	page.on('console', async msg => {
					console.log('PAGE LOG:', msg.text()); 
					if (msg.text().includes("isActiveBrowserDriver")){
							await page.evaluate(() => { 
											window.browserDriverResponse=true;
										});
					}
	});

	async function waitForDownload(page, fileName = '') {
	  return new Promise((resolve, reject) => {
	    page._client().on('Page.downloadProgress', (e) => {
	      if (e.state === 'completed') {
		resolve(fileName); // Optionally, you can pass the downloaded file name
	      } else if (e.state === 'canceled') {
		reject(new Error('Download canceled'));
	      }
	    });
	  });
	}

	const client = await page.target().createCDPSession();
	await client.send('Page.setDownloadBehavior', {
	 behavior: 'allow',
	 downloadPath: './'
	});

	// Navigate the page to a URL
	await page.goto(`file://${__dirname}/index.html?stealth=1#Ufile://${path.dirname(__dirname)}/xmlgraphs/`+xmlFilename);
	//await page.goto('https://atlas-expert-system-dev.web.cern.ch/login/data/diaeditor/?p=svgexporter_es&stealth=1#Uhttps://atlas-expert-system-dev.web.cern.ch/login/data/xmlgraphs/racksSR1.drawio');
	await page.evaluate(() => console.log(`url is ${location.href}`)); 

	// Set screen size
	await page.setViewport({width: 1080, height: 1024});
	await page.waitForSelector('.geDiagramContainer');

	var menu=await page.waitForSelector('.geItem >>> ::-p-text(Extras)');
	await menu.click();
	menu=await page.waitForSelector('.mxPopupMenuItem >>> ::-p-text(Plugins)');
	await menu.click();
	try{
	var element=page.waitForSelection('span ::-p-text(svgexporter_es.js)');
	console.log("Expert System SVG export plugging already loaded");
	I }catch(error){
	console.log("Loading expert System SVG export plugging");
	var element=await page.waitForSelector('button.geBtn ::-p-text(Add)');
	await element.click();
	var element=await page.waitForSelector('button.geBtn ::-p-text(Custom)');
	await element.click();
	var element=await page.waitForSelector('td ::-p-text(URL)');
	await element.type(`file://${__dirname}/plugins/svgexporter_es.js`);
	element=await page.waitForSelector('button.gePrimaryBtn ::-p-text(Add)');
	await element.click();
	element=await page.waitForSelector('button.gePrimaryBtn ::-p-text(Apply)');
	await element.click();
	element=await page.waitForSelector('button.geBtn ::-p-text(OK)');
	await element.click();
	await page.evaluate(() => location.reload())
	}
	menu=await page.waitForSelector('.geItem >>> ::-p-text(File)');
	await menu.click();
	menu=await page.waitForSelector('.mxPopupMenuItem >>> ::-p-text(Export as)');
	await menu.click();
	menu=await page.waitForSelector('.mxPopupMenuItem >>> ::-p-text(SVG)');
	await menu.click();
	var element=await page.waitForSelector('input[type=checkbox] ~ label ::-p-text(Include a copy)');
	await element.click();
	element=await page.waitForSelector('button.gePrimaryBtn');//Export
	await element.click();
	element=await page.waitForSelector('button.geBtn ::-p-text(Download)',{ timeout: 60000});
	await element.click();


	//await page.screenshot({
	//	path:'exported.png',
	//});
	await waitForDownload(page);

	await browser.close();

	exec(`mv ${svgFilename} ${path.dirname(__dirname)}/svggraphs/`,(error, stdout, stderr) => { console.log("Moved.."+xmlFilename.replace("drawio","svg")+stdout+stderr); });
	server.close();

})();
