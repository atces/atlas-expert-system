const net = require('net');

const backendHost = 'atlas-expert-system-dev-el9.cern.ch'; 
const port = 9998;
const debug = false; // Set to true for debugging output
const timeout = 10000; // Connection timeout in milliseconds

module.exports = {
	
	connectAndSend:	function (data) {
	  return new Promise((resolve, reject) => {
	    const startTime = Date.now();
	    const client = net.createConnection(port, backendHost);

	    client.setNoDelay(true); // Set non-blocking mode (similar to socket_set_nonblock)

	    client.on('connect', () => {
	      if (debug) {
		console.log(`Connected to ${backendHost}:${port}`);
	      }

	      const encodedData = JSON.stringify(data);
	      if (debug) {
		console.log('Sending request:', encodedData);
	      }

	      client.write(encodedData);
	    });

	    client.on('error', (err) => {
	      if (err.code === 'ECONNREFUSED' || err.code === 'ETIMEDOUT') {
		const elapsedTime = Date.now() - startTime;
		if (elapsedTime > timeout) {
		  console.error('Connection timed out');
		  reject(new Error('Connection timed out'));
		} else {
		  console.warn('Connection error:', err.message);
		  client.destroy(); // Close the socket on error
		}
	      } else {
		console.error('Socket error:', err.message);
		reject(err);
	      }
	    });

	    var response="";
	    client.on('data', (chunk) => {
	      response += chunk.toString();
	      if (debug) {
		console.log('Received data:', response);
	      }
	      // Accumulate received data until it reaches the expected size
	      if (response.length-8 >= parseInt(response.slice(0, 8), 16)) {
		      // Parse the received data (assuming it's JSON)
		      try {
			const decodedResponse = JSON.parse(response.slice(8));
			resolve(decodedResponse);
		      } catch (error) {
			console.error('Error parsing response:', error.message);
			reject(error);
		      } finally {
			client.destroy(); // Close the socket after receiving the response
		      }
	      }

	    });

	    client.on('close', () => {
	      if (debug) {
		console.log('Socket closed');
	      }
	    });
	  });
	}
};

