/**
 * Expert system SVG exporter plugin.
 */
Draw.loadPlugin(async function(ui) {

	/**
	 * Overrides SVG export to add metadata for eac.
	 */
	var graphCreateSvgImageExport = Graph.prototype.createSvgImageExport;

	console.log("Plugin loaded");
	console.log("isActiveBrowserDriver?");
	let counter=[];
	let checkBrowserDriverResponse = (resolve, reject,max_counter) => {
						counter=0;
						let intervalId = setInterval(() => {
							counter+=1;
    							if (window.browserDriverResponse !== undefined) {
      								clearInterval(intervalId);
								counter=0;
      								resolve(window.browserDriverResponse);
    							}
							if(counter>max_counter){
								resolve(false);
      								clearInterval(intervalId);
								counter=0;
							}
  						},10);
					}
 
	let promise = new Promise((resolve,reject) => checkBrowserDriverResponse(resolve,reject,10));
	isUsingBrowserDriver=await promise;
	console.log(isUsingBrowserDriver);

	let backendRequest = (cmd,uid,tokenId,fx) => {
		let backEndURL;
		if(isUsingBrowserDriver){
			backEndURL="http://localhost:3000/?cmd="+cmd+"&TokenId="+tokenId+"&uid="+uid;
		}
		else{
			backEndURL="../../socketlogic.php?cmd="+cmd+"&TokenId="+tokenId+"&uid="+uid;
		}
		request=new XMLHttpRequest();
		request.open("GET",backEndURL,false);
		request.send(null);
		if (request.readyState == 4 && request.status == 200){
			if(request.responseText.length>0 && !(request.responseText.match('Reply[^a-zA-Z]+Error'))){
				//console.log(request.responseText)
				obj=JSON.parse(request.responseText);		
				fx(obj);
			}						
		}
	}

	Graph.prototype.createSvgImageExport = function()
	{
		var exp = graphCreateSvgImageExport.apply(this, arguments);
		
		// Overrides rendering to add metadata
		var expDrawCellState = exp.drawCellState;

		exp.drawCellState = function(state, canvas)
		{
				
			var svgDoc = canvas.root.ownerDocument;
			var g = (svgDoc.createElementNS != null) ?
					svgDoc.createElementNS(mxConstants.NS_SVG, 'g') : svgDoc.createElement('g');
			g.setAttribute('id', 'cell-' + state.cell.id);

			//Root element
			if (state.parent==null){
				if (state.cell.value != undefined && state.cell.value.attributes != undefined && ("alarms" in state.cell.value.attributes || "actions" in state.cell.value.attributes)){
					var alarms=("alarms" in state.cell.value.attributes) ? state.cell.value.attributes['alarms'].value : "";
					var actions=("actions" in state.cell.value.attributes) ? state.cell.value.attributes['actions'].value : "";
					var metadata=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG, 'metadata') : svgDoc.createElement('metadata');
					metadata.textContent=JSON.stringify({alarms:alarms.split("\n"),actions:actions.split("\n")});
					svgDoc.children[0].insertBefore(metadata,svgDoc.children[0].firstChild);
				}
			}

			// Temporary replaces root for content rendering
			var prev = canvas.root;
			prev.appendChild(g);
			canvas.root = g;
			
			expDrawCellState.apply(this, arguments);
			if (state.cell.isEdge()) //Edges/Relations
			{
				
				var sourceNode= state.cell.source;
				var targetNode= state.cell.target;
				var uid=null;
				if (sourceNode != null && sourceNode.value.attributes != null && ('UID' in sourceNode.value.attributes || 'uid' in sourceNode.value.attributes))
				{
					uid=('UID' in sourceNode.value.attributes ) ? sourceNode.value.attributes['UID'].value : sourceNode.value.attributes['uid'].value;
				} else if (targetNode != null && targetNode.value.attributes != null && ('UID' in targetNode.value.attributes || 'uid' in targetNode.value.attributes))
				{
					uid=('UID' in targetNode.value.attributes) ? targetNode.value.attributes['UID'].value : targetNode.value.attributes['uid'].value;
				}
				if (uid!=null)
				{
					backendRequest("GetNoClassObject",uid,"1",(obj) => {
							var system=obj['subsystem'];
							var clazz=obj['Class'];
							g.setAttribute("class",system+" relation "+clazz);
							if (clazz=="Switchboard" && (uid.startsWith("EO") || uid.startsWith("ES")))
							{ 
								g.setAttribute('class',g.getAttribute("class")+' ups');
							}
						});
				}
				if (mxUtils.isNode(state.cell.value) && 'class' in state.cell.value.attributes )
				{
					g.setAttribute("class", g.getAttribute("class")+" "+state.cell.value.attributes['class'].value);
				}
				
			}
			else //Vertexes/Systems
			{	
				// Adds metadata if group is not empty
				if (g.firstChild == null)
				{
					g.parentNode.removeChild(g);
				}
				else if (mxUtils.isNode(state.cell.value))
				{
					g.setAttribute('content', mxUtils.getXml(state.cell.value));
					var rotation="";
					g.childNodes.forEach(function(child){
						if (child.getAttribute("transform")!=null)
						{
							if(child.tagName=="rect"){
								rotation=child.getAttribute("transform");
							}
							//child.setAttribute("transform",child.getAttribute("transform").replace(/rotate\([^)]*\)/,""));
						}
					})
					if ('class' in state.cell.value.attributes )
					{
						g.setAttribute("class",state.cell.value.attributes['class'].value);
					}
					
					for (var i = 0; i < state.cell.value.attributes.length; i++)
					{
						var attrib = state.cell.value.attributes[i];
						g.setAttribute('data-' + attrib.name, attrib.value);
					}
					if ('UID' in state.cell.value.attributes || 'uid' in state.cell.value.attributes ){
						var uid=('UID' in state.cell.value.attributes ) ? state.cell.value.attributes['UID'].value : state.cell.value.attributes['uid'].value;
						var interactiveElementsPosition= ('position' in state.cell.value.attributes ) ? state.cell.value.attributes['position'].value : "center-bottom";
						//Getting the data from the server
						backendRequest("GetNoClassObject",uid,"1", (obj) => {
									var system=obj['subsystem'];
									system=(system)?system:"";
									var clazz=obj['Class'];
									g.setAttribute('class',g.getAttribute("class")+' '+system+' system '+clazz);
									if (clazz=="Switchboard" && (uid.startsWith("EO") || uid.startsWith("ES")))
									{ 
										g.setAttribute('class',g.getAttribute("class")+' ups');
									}
									var g_title=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'title') : svgDoc.createElement('title');
									g_title.textContent=uid+": "+obj["description"];
									g.insertBefore(g_title,g.firstChild);
									if (state.cell.geometry.width>54 && state.cell.geometry.height>40)
									{
										//Drawing system label
										var g_system=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'g') : svgDoc.createElement('g');
										var system_node=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'text') : svgDoc.createElement('text');
										system_node.setAttribute("id",uid+"system");
										system_node.setAttribute("class",clazz+" label system "+system+" "+g.getAttribute("class"));
										var font_size=8;
										// Add ... when the subsystem can not be display into the box
										text_factor=system.toUpperCase()==system ? 1.5:2.5
										system_node.textContent=(state.cell.geometry.height<(system.length+1)*font_size/text_factor) ? system.substring(0,state.cell.geometry.height/font_size*text_factor-4)+"...":system;
										system_node.setAttribute("fill","#000000");
										system_node.setAttribute("font-size",font_size+"px");
										var x=state.cell.geometry.x+font_size-(state.view.graphBounds.x-state.view.translate.x);//Diagrams have different translations in the geometrics
										var y=state.cell.geometry.y+state.cell.geometry.height/2+system_node.textContent.length/2*font_size/text_factor-(state.view.graphBounds.y-state.view.translate.y);
										system_node.setAttribute("x",x);
										system_node.setAttribute("y",y);
										system_node.setAttribute("transform","rotate(270 "+x+" "+y+")");
										g_system.appendChild(system_node);
										g_system.setAttribute("transform",rotation);
										g.appendChild(g_system);
									}
									
									//Drawing element label
									if (state.cell.value.attributes['label'].value=="")
									{
										var label_node=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'text') : svgDoc.createElement('text');
										label_node.setAttribute("id",uid+"label");
										label_node.setAttribute("class",clazz+" label system "+system);
										var nice_uid=(clazz == "Switchboard" || clazz == "UPS") ? uid.replace('_','/') : uid.replace('_',' ');
										label_node.textContent=(state.cell.value.attributes['altLabel']!= null) ? state.cell.value.attributes['altLabel'].value : nice_uid;
										label_node.setAttribute("fill","#000000");
										text_factor=uid.toUpperCase()==uid ? 1.5:2.5
										var font_size=(state.cell.geometry.width > (uid.length-1-(uid.match(/ /g) || []).length)*12/text_factor || state.cell.geometry.height>40) ? 12 : 8;
										label_node.setAttribute("font-size",font_size+"px");
										label_node.setAttribute("font-weight","bold");
										label_node.setAttribute("font-family","Helvetica");
										label_node.setAttribute("x",state.cell.geometry.x+state.cell.geometry.width/2-(uid.length/2)*font_size/text_factor-(state.view.graphBounds.x-state.view.translate.x));
										label_node.setAttribute("y",state.cell.geometry.y+font_size+2-(state.view.graphBounds.y-state.view.translate.y));
										g.appendChild(label_node);
									}else {
										var label_text=state.cell.value.attributes['label'].value;
										label_text=label_text.startsWith('<')? label_text.match(">([^<>]+)<")[1]: label_text;
										label_text=label_text.replaceAll("&nbsp;","\u00A0");
										label_text=label_text.includes('<') ? label_text.match("([^<>]+)")[1] : label_text;
										var xpathResult=svgDoc.evaluate("//*[text()='"+label_text+"']", svgDoc, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
										var node;
										for (let i = 0; i < xpathResult.snapshotLength; i++) {
											label_node=xpathResult.snapshotItem(i);								         
											label_node.removeAttribute("color");
											while (label_node.firstElementChild != null) {
												if (label_node.firstElementChild.tagName == "div")
												{
													label_node=label_node.firstElementChild;
												}
												else
												{
													break;
												}
											}
											label_node.setAttribute("id",uid+"label");
											label_node.setAttribute("class",clazz+" label system "+system+" "+g.getAttribute("class"));
											text_factor=label_text.toUpperCase()==label_text ? 1.5:2.5
											var font_size=(state.cell.geometry.width > (label_text.length-1-(label_text.match(/ /g) || []).length)*12/text_factor || state.cell.geometry.height>40) ? 12 : 8;
											label_node.setAttribute("style","font-size: "+font_size+"px; font-weight: bold; font-family: Helvetica");
											label_node.setAttribute("fill","#000000");
											label_node.setAttribute("color","#000000");
										}
									}
									//Drawing switch
									var center_x=state.cell.geometry.x+state.cell.geometry.width/2-23-(state.view.graphBounds.x-state.view.translate.x);
									var center_y=state.cell.geometry.y+state.cell.geometry.height-20-(state.view.graphBounds.y-state.view.translate.y);
									var scale_factor=(state.cell.geometry.width>54 && state.cell.geometry.height>40)? 1 : 0.5 ;
									if (interactiveElementsPosition.includes("right-")) { center_x=center_x+state.cell.geometry.width/2-50*scale_factor; }
									if (interactiveElementsPosition.includes("-center")) { center_y=center_y-state.cell.geometry.height/2+20*scale_factor; }
									if (interactiveElementsPosition.includes("-top")) { center_y=center_y-state.cell.geometry.height+20+20*scale_factor; }
									if (clazz!="Group" && clazz!="Rack"){
										var switch_img=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'g') : svgDoc.createElement('g');
										switch_img.setAttribute("id",uid+"sw");
										switch_img.setAttribute("class","cursor element");
										switch_img.setAttribute("onclick","window.parent.ChangeSwitch('"+uid+"')");
										g.appendChild(switch_img);
										var path=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'path') : svgDoc.createElement('path');
										m=3*scale_factor;//Min trace length
										path.setAttribute("d","m "+(center_x+20*(1-scale_factor))+","+(center_y+12-12*scale_factor)+" c 0,-"+m+" "+m+",-"+2*m+" "+2*m+",-"+2*m+" "+m+",0 "+2*m+","+m+" "+2*m+","+2*m+" l 0,"+4*m+" c 0,"+m+" -"+m+","+2*m+" -"+2*m+","+2*m+" -"+m+",0 -"+2*m+",-"+m+" -"+2*m+",-"+2*m+" z");
										path.setAttribute("fill","#14b828");
										path.setAttribute("stroke","#000000");
										path.setAttribute("stroke-width","1");
										switch_img.appendChild(path);
										var circle1=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'circle') : svgDoc.createElement('circle');
										circle1.setAttribute("cx",center_x+20-14*scale_factor);
										circle1.setAttribute("cy",center_y+1+12-12*scale_factor);
										circle1.setAttribute("r","6.4"*scale_factor);
										circle1.setAttribute("fill","#ffffff");
										circle1.setAttribute("stroke","#a9a9a9");
										switch_img.appendChild(circle1);
										switch_img.setAttribute("transform",rotation);
									} else {
										var g_expand=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'g') : svgDoc.createElement('g');
										g_expand.setAttribute("onclick","window.parent.Get"+clazz+"('"+uid+"')");
										g_expand.setAttribute("class","cursor");
										var back_rect=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'rect') : svgDoc.createElement('rect');
										back_rect.setAttribute("x",center_x+8-5*scale_factor);
										back_rect.setAttribute("y",center_y+18-18*scale_factor);
										back_rect.setAttribute("width",10*scale_factor);
										back_rect.setAttribute("height",18*scale_factor);
										back_rect.setAttribute("fill","#FFFFFF");
										back_rect.setAttribute("fill-opacity","0.0001");
										back_rect.setAttribute("transform",rotation);
										g_expand.appendChild(back_rect);
										var path_expand=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'path') : svgDoc.createElement('path');	
										path_expand.setAttribute("style","fill:none;stroke:#000000;stroke-width:10.765;stroke-linecap:round;stroke-linejoin:miter");
										x0=27
										y0=78
										dx=16*scale_factor
										dy=30*scale_factor
										path_expand.setAttribute("d","M "+x0+","+y0+" "+(x0+dx)+","+(y0-dy)+" M "+(x0+2*dx)+","+y0+" "+(x0+dx)+","+(y0-dy)+" M "+x0+","+(y0+dy-dx/2)+" "+(x0+dx)+","+(y0+2*dy-dx/2)+" M "+(x0+2*dx)+","+(y0+dy-dx/2)+" "+(x0+dx)+","+(y0+2*dy-dx/2));
										path_expand.setAttribute("transform","translate("+(center_x+x0/2*(1-scale_factor))+","+(center_y-10+dy/2*(1-scale_factor))+") scale(0.2) "+rotation);
										g_expand.appendChild(path_expand);
										g.appendChild(g_expand);
									}
									var center_x=state.cell.geometry.x+state.cell.geometry.width/2-(state.view.graphBounds.x-state.view.translate.x);
									var center_y=state.cell.geometry.y+state.cell.geometry.height-20-(state.view.graphBounds.y-state.view.translate.y);
									if (interactiveElementsPosition.includes("right-")) { center_x=center_x+state.cell.geometry.width/2-50*scale_factor; }
									if (interactiveElementsPosition.includes("-center")) { center_y=center_y-state.cell.geometry.height/2+20*scale_factor; }
									if (interactiveElementsPosition.includes("-top")) { center_y=center_y-state.cell.geometry.height+20+20*scale_factor; }

									//Draw state rect	
									var state_img=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'rect') : svgDoc.createElement('rect');
									state_img.setAttribute("id",uid+"st");
									state_img.setAttribute("class","element state");
									state_img.setAttribute("x",center_x-7*scale_factor);
									state_img.setAttribute("y",center_y+14-14*scale_factor);
									state_img.setAttribute("width",14*scale_factor);
									state_img.setAttribute("height",14*scale_factor);
									state_img.setAttribute("fill","#14b828");
									state_img.setAttribute("stroke","#000000");
									state_img.setAttribute("stroke-width","1");
									state_img.setAttribute("transform",rotation);
									g.appendChild(state_img);
									//Draw info image
									var info_g=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'g') : svgDoc.createElement('g');
									info_g.setAttribute("onclick","window.parent.displayHistory('"+uid+"')");
									info_g.setAttribute("class","cursor");
									info_g.setAttribute("id",uid+"hi");
									var info_img=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'circle') : svgDoc.createElement('circle');
									info_img.setAttribute("cx",center_x+17*scale_factor);
									info_img.setAttribute("cy",center_y+7*(2-scale_factor));
									info_img.setAttribute("r","7"*scale_factor);
									info_img.setAttribute("fill","#4287f5");
									info_g.appendChild(info_img);
									var i_text=(svgDoc.createElementNS != null) ? svgDoc.createElementNS(mxConstants.NS_SVG,'text') : svgDoc.createElement('text');
									var font_size=(state.cell.geometry.width>54 && state.cell.geometry.height>40) ? 16 : 8;
									i_text.textContent="i";
									i_text.setAttribute("fill","#ffffff");
									i_text.setAttribute("x",center_x+15*scale_factor);
									i_text.setAttribute("y",center_y+14-2*scale_factor);
									i_text.setAttribute("font-size",font_size+"px");
									info_g.appendChild(i_text);
									info_g.setAttribute("transform",rotation);
									g.appendChild(info_g);
						});
					}
				}
			}
			// Restores previous root
			canvas.root = prev;
		};

		return exp;
	};
	
});
