var codification=new Array();
codification["E"]=new Array();
codification["E"]["reseau"]=new Array();
codification["E"]["type"]=new Array();



var groups=new Array();
groups.E="Electrical system";
groups.UA="Air handling unit";
groups.UI="Control cubicle";

codification["E"]["reseau"]["BT"]=["A", "B", "C", "D", "G", "I", "J", "L", "N", "O", "P", "Q", "R", "S", "U", "V", "W", "X", "Z"];
codification["E"]["reseau"]["HT"]=["E", "F", "H", "K", "M", "T", "Y"];

codification["E"]["reseau"]["A"]="BT ASSURE   400 V AC";
codification["E"]["reseau"]["B"]="BT NORMAL 400 V AC";
codification["E"]["reseau"]["C"]="TBT COURANT CONTINU < 50 V DC";
codification["E"]["reseau"]["D"]="SERVICES MACHINE MIXTES ";
codification["E"]["reseau"]["G"]="TBT COURANT CONTINU 50<= U <120 V DC";
codification["E"]["reseau"]["I"]="RESERVE EQUIPEMENTS NON EL";
codification["E"]["reseau"]["J"]="BT COURANT CONTINU  >= 120 V DC";
codification["E"]["reseau"]["L"]="Interdit (Demander autorisation)";
codification["E"]["reseau"]["N"]="CAPTEURS";
codification["E"]["reseau"]["O"]="BT ONDULE 230/400 V AC";
codification["E"]["reseau"]["P"]="ECLAIRAGE ANTIPANIQUE";
codification["E"]["reseau"]["Q"]="BT CRYOGENIE  400 V AC";
codification["E"]["reseau"]["R"]="BT CONVERTISSEUR MACHINE  400 V AC";
codification["E"]["reseau"]["S"]="BT SECOURU  400 V AC";
codification["E"]["reseau"]["U"]="ARRET D'URGENCE";
codification["E"]["reseau"]["V"]="BT VACUUM 400 V AC";
codification["E"]["reseau"]["W"]="BT COOLING VENTILATION  400 V AC";
codification["E"]["reseau"]["X"]="BT EXPERIENCE 400 V AC";
codification["E"]["reseau"]["Z"]="BT RF 400 V AC";


codification["E"]["reseau"]["E"]="HTA EDF (MT)";
codification["E"]["reseau"]["F"]="HTA SECOURU (MT)";   
codification["E"]["reseau"]["H"]="HTB NORMAL";
codification["E"]["reseau"]["K"]="HTA 3.3 kV (MT)";  
codification["E"]["reseau"]["M"]="HTA 18 kV (MT)"; 
codification["E"]["reseau"]["T"]="Voir onglet codification Supervision";
codification["E"]["reseau"]["Y"]="Voir onglet codification RACKS";




codification["E"]["type"]["BT"]=new Array();
codification["E"]["type"]["BT"]["A"]="BLOC INVERSEUR SOURCE";
codification["E"]["type"]["BT"]["B"]="COMMANDE (BOUTON CHAINE)";
codification["E"]["type"]["BT"]["C"]="CHASSIS CONTRÔLE";
codification["E"]["type"]["BT"]["D"]="DISTRIBUTION (TABLEAU DEPART)";
codification["E"]["type"]["BT"]["E"]="MESURE ANALOGIQUE";
codification["E"]["type"]["BT"]["F"]="Interdit (Demander autorisation)";
codification["E"]["type"]["BT"]["G"]="GENERATEUR (DIESEL - BATTERIE)";
codification["E"]["type"]["BT"]["H"]="PROTECTION";
codification["E"]["type"]["BT"]["I"]="CHASSIS INTERCONNEXION-SYNCHRO";
codification["E"]["type"]["BT"]["J"]="REPARTITEUR (COFFRET CHASSIS)";
codification["E"]["type"]["BT"]["K"]="TRANCHE CANALISATION PREFABR.";
codification["E"]["type"]["BT"]["L"]="TRANCHE ECLAIRAGE, AU LOCAL";
codification["E"]["type"]["BT"]["M"]="MOTEURS ELECTRIQUES";
codification["E"]["type"]["BT"]["N"]="Interdit (Demander autorisation)";
codification["E"]["type"]["BT"]["O"]="Interdit (Demander autorisation)";
codification["E"]["type"]["BT"]["P"]="TRANCHE FORCE (COFFRET PRISES)";
codification["E"]["type"]["BT"]["Q"]="COMPENSATION";
codification["E"]["type"]["BT"]["R"]="TRANCHE CHAUFFAGE SANITAIRE";
codification["E"]["type"]["BT"]["S"]="UPS ALIMENTATION SANS COUPURE";
codification["E"]["type"]["BT"]["T"]="TRANSFORMATEUR";
codification["E"]["type"]["BT"]["U"]="CHARGEUR REDRESSEUR ONDULEUR";
codification["E"]["type"]["BT"]["V"]="TRANCHE VENTILATION Extraction";
codification["E"]["type"]["BT"]["W"]="Interdit (Demander autorisation)";
codification["E"]["type"]["BT"]["X"]="DERIVATION - PROLONGATION";
codification["E"]["type"]["BT"]["Y"]="Interdit (Demander autorisation)";
codification["E"]["type"]["BT"]["Z"]="Interdit (Demander autorisation)";



codification["E"]["type"]["HT"]=new Array();
codification["E"]["type"]["HT"]["A"]="TC (TRANSFORMATEUR DE COURANT)";
codification["E"]["type"]["HT"]["B"]="JEU DE BARRES";
codification["E"]["type"]["HT"]["C"]="CHASSIS CONTRÔLE/COMMANDE ";
codification["E"]["type"]["HT"]["D"]="DISTRIBUTION (CELLULES)";
codification["E"]["type"]["HT"]["E"]="CHASSIS/SEPAM MESURE TENSION";
codification["E"]["type"]["HT"]["F"]="FILTRE";
codification["E"]["type"]["HT"]["G"]="GENERATEUR";
codification["E"]["type"]["HT"]["H"]="PROTECTION (SEPAM - FIR  )";
codification["E"]["type"]["HT"]["I"]="COFFRET TRANSFO";
codification["E"]["type"]["HT"]["J"]="CHASSIS REPARTITEUR MESURE TENS";
codification["E"]["type"]["HT"]["K"]="JONCTION TRONCONS DE CABLE";
codification["E"]["type"]["HT"]["M"]="MOTEURS ELECTRIQUES";
codification["E"]["type"]["HT"]["N"]="Interdit (Demander autorisation)";
codification["E"]["type"]["HT"]["O"]="SPECIFIQUE (Demander autorisation)";
codification["E"]["type"]["HT"]["P"]="Interdit (Demander autorisation)";
codification["E"]["type"]["HT"]["Q"]="REACTANCE";
codification["E"]["type"]["HT"]["R"]="RESISTANCE";
codification["E"]["type"]["HT"]["S"]="SECTIONNEUR DE LIGNE/TRANCHE";
codification["E"]["type"]["HT"]["T"]="TRANSFORMATEUR";
codification["E"]["type"]["HT"]["U"]="EXCITATION";
codification["E"]["type"]["HT"]["V"]="VENTIL TRANSFO/COMPRES GENERAT.";
codification["E"]["type"]["HT"]["W"]="TP (TRANSFORMATEUR DE POTENTIEL)";
codification["E"]["type"]["HT"]["X"]="SECTIONNEUR DE TERRE";
codification["E"]["type"]["HT"]["Y"]="Interdit (Demander autorisation)";
codification["E"]["type"]["HT"]["Z"]="PARAFOUDRE";





// AHU Air Handling Units
codification["UA"]=new Array();
codification.UA.UAEB=["Extraction from racks", "60"];
codification.UA.UAED=["Smoke extraction (mechanical)", "25"];
codification.UA.UAEG=["Gas extraction (option)", "15"];
codification.UA.UAEK=["Gas extraction (option)", "45"];
codification.UA.UAEP=["Extraction from restricted area", "5"];
codification.UA.UAEW=["Gas extraction (option)", "10"];
codification.UA.UAEZ=["Extraction from sanitary facilities", "0.5"];
codification.UA.UAIE=["Extraction from experiment", "90"];
codification.UA.UAT1=["Smoke vent (gravity)", ""];
codification.UA.UAT2=["Smoke vent (mechanical)","3"];


// UI control cubicle
codification["UI"]=new Array();
codification.UI.UIAC=["Electric power cubicle 'Normal Power'"];
codification.UI.UIAE=["Electric power cublicle 'Emergency Power'"];
codification.UI.UIAO=["Electric control cubicle 'Normal Power'"];
codification.UI.UIAN=["Electric control cucicle 'Emergency Power'"];
codification.UI.UICN=["Firemen control cubicle"];






/*LOCATIONS*/

var batiments=new Array();

batiments["80"]=["2828","HALL D'EXPERIENCE","UX85"];
batiments["85"]=["2829","CENTRALE DE REFROIDISSEMENT","UW85"];
batiments["8Z"]=["2830","PUITS D'ACCES PERSONNEL (D=5.1M P=104M)","PZ85"];
batiments["85"]=["2835","TUNNEL SECTION FAIBLE BETA DIAMETRE 4.40M","RB86"];
batiments["85"]=["2837","CHAMBRE DE JONCTION","UJ86"];
batiments["87"]=["2838","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA87"];
batiments["88"]=["2847","TUNNEL MACHINE DIAMETRE 3.76M","R88"];
batiments["88"]=["2848","ALVEOLE ELECTRIQUE","RE88"];
batiments["88"]=["2849","TUNNEL MACHINE DIAMETRE 3.76M","R89"];
batiments["P18"]=["2145","PUITS DESCENTE AIMANTS (D=14.10M P=81M)","PM18"];
batiments["3Z"]=["2316","PUITS DACCES PERSONNEL (D=5.1M P=98M)","PZ33"];
batiments["4Z"]=["2430","PUITS D'ACCES PERSONNEL (D=5.1M P=143M)","PZ45"];
batiments["8X"]=["2882","BATIMENT ANNEXE DE VENTIL. ZONE EXPERIMENT.","SUX8"];
batiments["8U"]=["2880","BATIMENT VENTILATION PRINCIPALE","SU8"];
batiments["8E"]=["2860","SALLE DE SECURITE DE S/STATION","SES8"];
batiments["8E"]=["2860","S/STATION ELECTRIQUE","SE8"];
batiments["8F"]=["2865","TOURS DE REFROIDISS. ET S/STATION DE POMPAGE","SF8"];
batiments["8G"]=["2870","BATIMENT GAZ","SG8"];
batiments["8R"]=["2875","BATIMENT DES REDRESSEURS","SR8"];
batiments["8UH"]=["2881","BATIMENT COMPRESSEUR HELIUM (ANNEXE A SU)","SUH8"];
batiments["6Z"]=["2695","BATIMENT D'ACCES PERSONNEL A ZONE EXPERIMENt.","SZ6"];
batiments["8X"]=["2885","BATIMENT EXPERIMENTAL","SX8"];
batiments["8Z"]=["2895","BATIMENT D'ACCES PERSONNEL A ZONE EXPERIMENT.","SZ8"];
batiments["7D"]=["2755","BATIMENT DE DECHARGEMENT","SD7"];
batiments["7E"]=["2760","SALLE DE SECURITE DE S/STATION","SES7"];
batiments["6X"]=["2685","BATIMENT EXPERIMENTAL","SX6"];
batiments["6Y"]=["2690","BATIMENT CONTROLE ACCES AU SITE","SY6"];
batiments["7E"]=["2758","POSTE AUXILIAIRE EDF (20KV)","SEE7"];
batiments["8D"]=["2855","BATIMENT DE DECHARGEMENT","SD8"];
batiments["7E"]=["2760","LOCAL DISTRIBUTION MOYENNE TENSION","SEM7"];
batiments["7E"]=["2760","S/STATION ELECTRIQUE","SE7"];
batiments["7R"]=["2775","BATIMENT DES REDRESSEURS","SR7"];
batiments["7U"]=["2780","BATIMENT VENTILATION PRINCIPALE","SU7"];
batiments["6E"]=["2660","S/STATION ELECTRIQUE","SE6"];
batiments["6E"]=["2660","ENCLOS NIVEAU HAUTE TENSION","SEH6"];
batiments["8E"]=["2858","POSTE AUXILIAIRE EDF (20KV)","SEE8"];
batiments["8E"]=["2860","LOCAL DISTRIBUTION MOYENNE TENSION","SEM8"];
batiments["6A"]=["2650","BATIMENT DE CONDITIONNEMENT CAVITES ACCELER.","SA6"];
batiments["6E"]=["2654","LOCAL POUR COMPENSATEURS","SEQ6"];
batiments["6D"]=["2655","BATIMENT DE DECHARGEMENT","SD6"];
batiments["6E"]=["2658","POSTE AUXILIAIRE EDF (20KV)","SEE6"];
batiments["6U"]=["2680","BATIMENT VENTILATION PRINCIPALE","SU6"];
batiments["6E"]=["2660","LOCAL DISTRIBUTION MOYENNE TENSION","SEM6"];
batiments["4U"]=["2480","BATIMENT VENTILATION PRINCIPALE","SU4"];
batiments["6E"]=["2660","SALLE DE SECURITE DE S/STATION","SES6"];
batiments["6F"]=["2665","TOURS DE REFROIDISSEMENT","SF6"];
batiments["6G"]=["2670","BATIMENT GAZ","SG6"];
batiments["6R"]=["2675","BATIMENT DES REDRESSEURS","SR6"];
batiments["4Y"]=["2490","BATIMENT CONTROLE ACCES AU SITE","SY4"];
batiments["6UH"]=["2681","BATIMENT COMPRESSEUR HELIUM (ANNEXE A SU)","SUH6"];
batiments["4UH"]=["2481","BATIMENT COMPRESSEUR HELIUM (ANNEXE A SU)","SUH4"];
batiments["4X"]=["2485","BATIMENT EXPERIMENTAL","SX4"];
batiments["4P"]=["2489","BATIMENT DES PHYSICIENS HORS SITE P4","SXY4"];
batiments["5R"]=["2575","BATIMENT DES REDRESSEURS","SR5"];
batiments["4Z"]=["2495","BATIMENT D'ACCES PERSONNEL A ZONE EXPERIMENT.","SZ4"];
batiments["5D"]=["2555","BATIMENT DE DECHARGEMENT","SD5"];
batiments["5E"]=["2558","POSTE AUXILIAIRE EDF (20KV)","SEE5"];
batiments["5E"]=["2560","S/STATION ELECTRIQUE","SE5"];
batiments["5E"]=["2560","SALLE DE SECURITE DE S/STATION","SES5"];
batiments["5E"]=["2560","LOCAL DISTRIBUTION MOYENNE TENSION","SEM5"];
batiments["4F"]=["2465","TOURS DE REFROIDISSEMENT","SF4"];
batiments["3U"]=["2380","BATIMENT VENTILATION PRINCIPALE","SU3"];
batiments["3Z"]=["2395","BATIMENT D'ACCES DU PERSONNEL (CHALET)","SZ33"];
batiments["4G"]=["2470","BATIMENT GAZ","SG4"];
batiments["4D"]=["2455","BATIMENT DE DECHARGEMENT","SD4"];
batiments["48"]=["2448","ALVEOLE ELECTRIQUE","RE48"];
batiments["48"]=["2449","TUNNEL MACHINE DIAMETRE 3.76M","R49"];
batiments["52"]=["2501","TUNNEL MACHINE DIAMETRE 3.76M","R51"];
batiments["52"]=["2502","ALVEOLE ELECTRIQUE","RE52"];
batiments["52"]=["2503","TUNNEL MACHINE DIAMETRE 3.76M","R52"];
batiments["56"]=["2512","TUNNEL MACHINE DIAMETRE 3.76M","R53"];
batiments["56"]=["2525","TUNNEL MACHINE DIAMETRE 3.76M","R54"];
batiments["56"]=["2527","ZONE DE SERVICE","US56"];
batiments["65"]=["2619","CHAMBRE DE JONCTION","UJ64"];
batiments["56"]=["2537","CHAMBRE DE JONCTION","UJ56"];
batiments["56"]=["2541","TUNNEL MACHINE DIAMETRE 3.76M","R57"];
batiments["58"]=["2547","TUNNEL MACHINE DIAMETRE 3.76M","R58"];
batiments["58"]=["2548","ALVEOLE ELECTRIQUE","RE58"];
batiments["58"]=["2549","TUNNEL MACHINE DIAMETRE 3.76M","R59"];
batiments["62"]=["2601","TUNNEL MACHINE DIAMETRE 3.76M","R61"];
batiments["62"]=["2602","ALVEOLE ELECTRIQUE","RE62"];
batiments["62"]=["2603","TUNNEL MACHINE DIAMETRE 3.76M","R62"];
batiments["65"]=["2613","CHAMBRE DE JONCTION","UJ63"];
batiments["63"]=["2617","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA63"];
batiments["63"]=["2618","TUNNEL KLYSTRONS","UA63"];
batiments["65"]=["2637","CHAMBRE DE JONCTION","UJ66"];
batiments["65"]=["2620","GALERIE DE LIAISON","UL64"];
batiments["65"]=["2621","TUNNEL SECTION FAIBLE BETA (D=4.40M","RB64"];
batiments["65"]=["2627","ZONE DE SERVICE","US65"];
batiments["60"]=["2628","HALL D'EXPERIENCE","UX65"];
batiments["65"]=["2629","CENTRALE DE REFROIDISSEMENT","UW65"];
batiments["6Z"]=["2630","PUITS D'ACCES PERSONNEL (D=5.1M P=100M)","PZ65"];
batiments["67"]=["2638","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA67"];
batiments["67"]=["2639","TUNNEL KLYSTRONS","UA67"];
batiments["65"]=["2640","CHAMBRE DE JONCTION","UJ67"];
batiments["68"]=["2647","TUNNEL MACHINE DIAMETRE 3.76M","R68"];
batiments["68"]=["2649","TUNNEL MACHINE DIAMETRE 3.76M","R69"];
batiments["72"]=["2701","TUNNEL MACHINE DIAMETRE 3.76M","R71"];
batiments["72"]=["2703","TUNNEL MACHINE DIAMETRE 3.76M","R72"];
batiments["76"]=["2712","TUNNEL MACHINE DIAMETRE 3.76M","R73"];
batiments["82"]=["2803","TUNNEL MACHINE DIAMETRE 3.76M","R82"];
batiments["76"]=["2734","TUNNEL MACHINE DIAMETRE 3.76M","R76"];
batiments["76"]=["2737","CHAMBRE DE JONCTION","UJ76"];
batiments["76"]=["2741","TUNNEL MACHINE DIAMETRE 3.76M","R77"];
batiments["78"]=["2747","TUNNEL MACHINE DIAMETRE 3.76M","R78"];
batiments["78"]=["2748","ALVEOLE ELECTRIQUE","RE78"];
batiments["78"]=["2749","TUNNEL MACHINE DIAMETRE 3.76M","R79"];
batiments["82"]=["2801","TUNNEL MACHINE DIAMETRE 3.76M","R81"];
batiments["83"]=["2817","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA83"];
batiments["85"]=["2819","CHAMBRE DE JONCTION","UJ84"];
batiments["4E"]=["2458","POSTE AUXILIAIRE EDF (20KV)","SEE4"];
batiments["4E"]=["2460","S/STATION ELECTRIQUE","SE4"];
batiments["4E"]=["2460","LOCAL DISTRIBUTION MOYENNE TENSION","SEM4"];
batiments["4E"]=["2460","SALLE DE SECURITE DE S/STATION","SES4"];
batiments["2X"]=["2282","BATIMENT ANNEXE DE VENTIL. ZONE EXPERIMENT.","SUX2"];
batiments["2X"]=["2285","BATIMENT EXPERIMENTAL","SX2"];
batiments["3D"]=["2355","BATIMENT DE DECHARGEMENT","SD3"];
batiments["4R"]=["2475","BATIMENT DES REDRESSEURS","SR4"];
batiments["2R"]=["2275","BATIMENT DES REDRESSEURS","SR2"];
batiments["2U"]=["2280","BATIMENT VENTILATION PRINCIPALE","SU2"];
batiments["2UH"]=["2281","BATIMENT COMPRESSEUR HELIUM (ANNEXE A SU)","SUH2"];
batiments["3E"]=["2360","SALLE DE SECURITE DE S/STATION","SES3"];
batiments["2Y"]=["2290","BATIMENT CONTROLE ACCES AU SITE","SY2"];
batiments["3E"]=["2358","POSTE AUXILIAIRE EDF (20KV)","SEE3"];
batiments["2A"]=["2250","BATIMENT DE CONDITIONNEMENT CAVITES ACCELER.","SA2"];
batiments["3E"]=["2360","LOCAL DISTRIBUTION MOYENNE TENSION","SEM3"];
batiments["2E"]=["2254","LOCAL POUR COMPENSATEURS","SEQ2"];
batiments["3E"]=["2360","S/STATION ELECTRIQUE","SE3"];
batiments["3R"]=["2375","BATIMENT DES REDRESSEURS","SR3"];
batiments["2E"]=["2260","S/STATION ELECTRIQUE","SE2"];
batiments["2S"]=["2252","HALL D'ASSEMBLAGE EXPERIENCE L3","SXL2"];
batiments["2G"]=["2270","BATIMENT GAZ","SG2"];
batiments["2E"]=["2260","LOCAL DISTRIBUTION MOYENNE TENSION","SEM2"];
batiments["2D"]=["2255","BATIMENT DE DECHARGEMENT","SD2"];
batiments["2E"]=["2258","POSTE AUXILIAIRE EDF (20KV)","SEE2"];
batiments["2E"]=["2260","ENCLOS NIVEAU HAUTE TENSION","SEH2"];
batiments["2E"]=["2260","SALLE DE SECURITE DE S/STATION","SES2"];
batiments["2F"]=["2265","TOURS DE REFROIDISSEMENT","SF2"];
batiments["1R"]=["2175","BATIMENT DES REDRESSEURS","SR1"];
batiments["1E"]=["2160","S/STATION ELECTRIQUE","SE1"];
batiments["1E"]=["2160","LOCAL DE DISTRIBUTION MT 1RE PARTIE","SEM11"];
batiments["1E"]=["2160","LOCAL DE DISTRIBUTION MT 2EME PARTIE","SEM12"];
batiments["1E"]=["2160","SALLE DE SECURITE DE S/STATION","SES1"];
batiments["M18"]=["2173","HALL D'ASSEMBLAGE DES AIMANTS","SM18"];
batiments["12"]=["2101","TUNNEL MACHINE DIAMETRE 3.76M","R11"];
batiments["12"]=["2102","ALVEOLE ELECTRIQUE","RE12"];
batiments["12"]=["2103","TUNNEL MACHINE DIAMETRE 3.76M","R12"];
batiments["12"]=["2104","TUNNEL ELARGI AU DIAMETRE 5.50M","RT12"];
batiments["15"]=["2110","TUNNEL ELARGI AU DIAMETRE 4.40M","RI12"];
batiments["15"]=["2111","TUNNEL ELARGI AU DIAM. 4.40M FAISC. 800/UJ14","RI13"];
batiments["15"]=["2119","CHAMBRE DE JONCTION","UJ14"];
batiments["18"]=["2146","TUNNEL ELARGI AU DIAMETRE 5.50M","RT18"];
batiments["15"]=["2121","TUNNEL SECTION FAIBLE BETA (D=4.40M","RB14"];
batiments["15"]=["2127","ZONE DE SERVICE","US15"];
batiments["15"]=["2135","TUNNEL ELARGI AU DIAMETRE 4.40M","RB16"];
batiments["15"]=["2137","CHAMBRE DE JONCTION","UJ16"];
batiments["15"]=["2138","TUNNEL ELARGI AU DIAMETRE 4.40M FAISCEAU 800","RI17"];
batiments["15"]=["2142","TUNNEL ELARGI AU DIAMETRE 4.40M","RI18"];
batiments["16"]=["2144","CHAMBRE DE JONCTION TUNNEL INJECTION","UJ18"];
batiments["18"]=["2147","TUNNEL MACHINE DIAMETRE 3.76M","R18"];
batiments["18"]=["2149","TUNNEL MACHINE DIAMETRE 3.76M","R19"];
batiments["22"]=["2201","TUNNEL MACHINE DIAMETRE 3.76M","R21"];
batiments["22"]=["2202","ALVEOLE ELECTRIQUE","RE22"];
batiments["22"]=["2203","TUNNEL MACHINE DIAMETRE 3.76M","R22"];
batiments["25"]=["2213","CHAMBRE DE JONCTION","UJ23"];
batiments["23"]=["2217","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA23"];
batiments["23"]=["2218","TUNNEL KLYSTRONS","UA23"];
batiments["25"]=["2219","CHAMBRE DE JONCTION","UJ24"];
batiments["25"]=["2220","GALERIE DE LIAISON","UL24"];
batiments["28"]=["2249","TUNNEL MACHINE DIAMETRE 3.76M","R29"];
batiments["25"]=["2227","ZONE DE SERVICE","US25"];
batiments["29"]=["2228","HALL D'EXPERIENCE","UX25"];
batiments["25"]=["2229","CENTRALE DE REFROIDISSEMENT","UW25"];
batiments["25"]=["2236","GALERIE DE LIAISON","UL26"];
batiments["25"]=["2237","CHAMBRE DE JONCTION","UJ26"];
batiments["27"]=["2238","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA27"];
batiments["27"]=["2239","TUNNEL KLYSTRONS","UA27"];
batiments["25"]=["2240","CHAMBRE DE JONCTION","UJ27"];
batiments["28"]=["2247","TUNNEL MACHINE DIAMETRE 3.76M","R28"];
batiments["33"]=["2325","TUNNEL MACHINE DIAMETRE 3.76M","R34"];
batiments["32"]=["2301","TUNNEL MACHINE DIAMETRE 3.76M","R31"];
batiments["32"]=["2302","ALVEOLE ELECTRIQUE (DANS UJ32)","RE32"];
batiments["32"]=["2303","TUNNEL MACHINE DIAMETRE 3.76M","R32"];
batiments["32"]=["2305","CHAMBRE DE JONCTION","UJ32"];
batiments["32"]=["2312","TUNNEL MACHINE ELARGI AU DIAMETRE 4.20M","R33"];
batiments["33"]=["2313","CHAMBRE DE JONCTION","UJ33"];
batiments["45"]=["2420","GALERIE DE LIAISON","UL44"];
batiments["33"]=["2334","TUNNEL MACHINE DIAMETRE 3.76M","R36"];
batiments["38"]=["2347","TUNNEL MACHINE DIAMETRE 3.76M","R38"];
batiments["42"]=["2401","TUNNEL MACHINE DIAMETRE 3.76M","R41"];
batiments["42"]=["2402","ALVEOLE ELECTRIQUE","RE42"];
batiments["43"]=["2417","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA43"];
batiments["45"]=["2419","CHAMBRE DE JONCTION","UJ44"];
batiments["47"]=["2438","TUNNEL CAVITES ACCELERATRICES (D=4.40M)","RA47"];
batiments["45"]=["2421","TUNNEL SECTION FAIBLE BETA (D=4.40M","RB44"];
batiments["45"]=["2427","ZONE DE SERVICE","US45"];
batiments["40"]=["2428","HALL D'EXPERIENCE","UX45"];
batiments["45"]=["2429","CENTRALE DE REFROIDISSEMENT","UW45"];
batiments["45"]=["2436","GALERIE DE LIAISON","UL46"];
batiments["45"]=["2437","CHAMBRE DE JONCTION","UJ46"];
batiments["56"]=["2534","TUNNEL MACHINE DIAMETRE 3.76M","R56"];
batiments["48"]=["2447","TUNNEL MACHINE DIAMETRE 3.76M","R48"];
batiments["1E"]=["2161","GALERIE DE LIAISON SEM/SES","SL11"];
batiments["2E"]=["2261","GALERIE DE LIAISON SU/SUH/SH/SD","SL21"];
batiments["38"]=["2348","ALVEOLE ELECTRIQUE","RE38"];
batiments["68"]=["2648","ALVEOLE ELECTRIQUE","RE68"];
batiments["T62"]=["3616","CHAMBRE DE JONCTION VERS PT5 (8x40)","UJ62"];
batiments["T62"]=["3614","TUNNEL BEAM DUMP VERS PT5 (3x324)","TD62"];
batiments["T62"]=["3612","CAVERNE BEAM DUMP VERS PT5 (9x25)","UD62"];
batiments["T62"]=["3610","GALERIE LIAISON TUNNEL R62/CAV.UD62 (2.2x33)","UP62"];
batiments["T68"]=["3634","CHAMBRE DE JONCTION VERS PT7 (8x45)","UJ68"];
batiments["T68"]=["3636","TUNNEL BEAM DUMP VERS PT7 (3x345)","TD68"];
batiments["72"]=["2702","ALVEOLE ELECTRIQUE","RE72"];
batiments["T68"]=["3640","GALERIE LIAISON TUNNEL R68/CAV.UD68(2.2x33)","UP68"];
batiments["82"]=["2802","ALVEOLE ELECTRIQUE","RE82"];
batiments["B18"]=["3177","BATIMENT COMPRESSEUR HELIUM PT 1.8","SHM18"];
batiments["1E"]=["3160","PLATEFORME ELECTRIQUE  66KV","SEH1"];
batiments["5HB"]=["3551","BATIMENT BALLONS HELIUM","SHB5"];
batiments["T68"]=["3638","CAVERNE BEAM DUMP VERS PT7 (9x25)","UD68"];
batiments["W18"]=["3183","BATIMENT EAU DEMINERALISEE,CRYO PT1.8","SW18"];
batiments["B18"]=["3177","BATIMENT VENTILATION LATERAL SHM18","SUX18"];
batiments["A18"]=["3173","HALL DE MONTAGE PT 1.8","SMA18"];
batiments["A18"]=["3173","PASSAGE COUVERT SM18","SMB18"];
batiments["3E"]=["2361","GALERIE DE LIAISON SEM/SES/SR/SD","SL31"];
batiments["4E"]=["2461","GALERIE DE LIAISON SU/SUH/SD","SL41"];
batiments["5E"]=["2561","GALERIE DE LIAISON SEM/SES/SR/SD","SL51"];
batiments["7E"]=["2761","GALERIE DE LIAISON SEM/SES/SR/SD","SL71"];
batiments["4D"]=["2474","BATIMENT BALLONS HELIUM","SHBB4"];
batiments["6H"]=["2674","BATIMENT BALLONS HELIUM","SHBB6"];
batiments["2H"]=["2284","BATIMENT LIQUEFACTEUR HELIUM","SH2"];
batiments["4H"]=["2484","BATIMENT LIQUEFACTEUR HELIUM","SH4"];
batiments["6H"]=["2684","BATIMENT LIQUEFACTEUR HELIUM","SH6"];
batiments["8H"]=["2884","BATIMENT LIQUEFACTEUR HELIUM","SH8"];
batiments["H18"]=["2184","BATIMENT LIQUEFACTEUR HELIUM","SH18"];
batiments["32"]=["2312","SECTION ELARGIE DU TUNNEL","RZ33"];
batiments["83"]=["2818","TUNNEL KLYSTRONS","UA83"];
batiments["85"]=["2840","CHAMBRE DE JONCTION","UJ87"];
batiments["87"]=["2839","TUNNEL KLYSTRONS","UA87"];
batiments["85"]=["2813","CHAMBRE DE JONCTION","UJ83"];
batiments["2HB"]=["2251","BATIMENT BALLONS HELIUM","SHB2"];
batiments["8HB"]=["2851","BATIMENT BALLONS HELIUM","SHB8"];
batiments["F18"]=["2165","STATION DE POMPAGE","STP18"];
batiments["6HB"]=["2651","BATIMENT BALLONS HELIUM","SHB6"];
batiments["15A"]=["3125","CAVERNE TECHNIQUE ATLAS(20x62)","USA15"];
batiments["43"]=["2418","TUNNEL KLYSTRONS","UA43"];
batiments["47"]=["2439","TUNNEL KLYSTRONS","UA47"];
batiments["2DH"]=["2257","BATIMENT DECHARGEMENT HELIUM","SDH2"];
batiments["8E"]=["2854","LOCAL POUR COMPENSATEURS","SEQ8"];
batiments["4E"]=["2454","LOCAL POUR COMPENSATEURS","SEQ4"];
batiments["4HB"]=["2451","BATIMENT BALLONS HELIUM","SHB4"];
batiments["6L"]=["2661","GALERIE DE LIAISON SU/SD/SGH/SL64/SL62","SL61"];
batiments["1UX"]=["3182","BATIMENT VENTILATION ANNEXE","SUX1"];
batiments["1H"]=["3184","BATIMENT LIQUEFACTEUR HELIUM","SH1"];
batiments["1F"]=["3165","TOURS DE REFROIDISSEMENT + LOCAL POMPES SOUT.","SF1"];
batiments["1X"]=["3185","BATIMENT TETE DE PUITS PX14 ET PX16","SX1"];
batiments["55X"]=["3525","CAVERNE EXPERIENCE CMS (26.5x53)","UXC55"];
batiments["5U"]=["3580","BATIMENT VENTILATION MACHINE","SU5"];
batiments["5X"]=["3585","BAT MONTAGE EXPERIENCE","SX5"];
batiments["5UX"]=["3582","BATIMENT  VENTILATION EXPERIENCE","SUX5"];
batiments["5GX"]=["3570","BATIMENT MELANGE GAZ EXPERIENCE","SGX5"];
batiments["5Y"]=["3590","BATIMENT CONTROLE ACCES AU SITE","SY5"];
batiments["5F"]=["3565","TOURS DE REFROIDISSEMENT + LOCAL POMPES SOUT.","SF5"];
batiments["2I"]=["3209","TUNNEL INJECTION TI2 DE TT60 VERS PT2(3x2660)","TI2"];
batiments["8I"]=["3809","TUNNEL INJECTION TI8 DE LSS4 VERS PT8(3x2334)","TI8"];
batiments["1CX"]=["3162","BATIMENT CONTROLE EXPERIENCE","SCX1"];
batiments["2DX"]=["3278","BATIMENT TETE DE PUITS PGC2 MACHINE","SDX2"];
batiments["4L"]=["3461","GALERIES TECHNIQUES","SL4"];
batiments["1DX"]=["3178","BATIMENT TETE DE PUITS PX15","SDX1"];
batiments["1GX"]=["3170","BATIMENT","SGX1"];
batiments["4DH"]=["3457","BATIMENT DECHARGEMENT HELIUM","SDH4"];
batiments["4HX"]=["3468","BATIMENT COMPRESSEUR HELIUM EXPERIENCE","SHX4"];
batiments["5CX"]=["3562","BATIMENT CONTROLE EXPERIENCE","SCX5"];
batiments["5DX"]=["3578","BATIMENT TETE DE PUITS PM54","SDX5"];
batiments["5H"]=["3584","BATIMENT LIQUEFACTEUR HELIUM","SH5"];
batiments["45"]=["2435","TUNNEL SECTION FAIBLE BETA DIAMETRE 4.40M","RB46"];
batiments["6DH"]=["3657","BATIMENT DE DECHARGEMENT HELIUM UCB2","SDH6"];
batiments["8HM"]=["3877","BATIMENT COMPRESSEUR HELIUM MACHINE","SHM8"];
batiments["65"]=["2635","TUNNEL SECTION FAIBLE BETA DIAMETRE 4.40M","RB66"];
batiments["8DH"]=["3857","BATIMENT DE DECHARGEMENT HELIUM UCB2","SDH8"];
batiments["8HX"]=["3868","BATIMENT COMPRESSEUR HELIUM EXPERIENCE","SHX8"];
batiments["15X"]=["3126","CAVERNE EXPERIENCE ATLAS(53x30x34.9h)","UX15"];
batiments["85"]=["2821","TUNNEL SECTION FAIBLE BETA (D=4.40M)","RB84"];
batiments["18"]=["2148","ALVEOLE ELECTRIQUE","RE18"];
batiments["55"]=["3524","CAVERNE CONTROLE CMS (18x85)","USC55"];
batiments["28"]=["2248","ALVEOLE ELECTRIQUE","RE28"];
batiments["85"]=["2827","ZONE DE SERVICE","US85"];
batiments["6HM"]=["3677","BATIMENT COMPRESSEUR HELIUM MACHINE","SHM6"];
batiments["4HM"]=["3477","BATIMENT COMPRESSEUR HELIUM MACHINE","SHM4"];
batiments["13"]=["3110","CAVERNE REDRESSEUR VERS PT8","RR13"];
batiments["17"]=["3141","CAVERNE REDRESSEUR VERS PT2","RR17"];
batiments["53"]=["3508","CAVERNE REDRESEUR VERS PT4","RR53"];
batiments["57"]=["3541","CAVERNE REDRESSEUR VERS PT6","RR57"];
batiments["73"]=["3718","CAVERNE ELECTRIQUE FEEDBOX VERS PT6","RR73"];
batiments["77"]=["3732","CAVERNE ELECTRIQUE FEEDBOX VERS PT8","RR77"];
batiments["38"]=["2341","TUNNEL MACHINE DIAMETRE 3.76M","R37"];
batiments["38"]=["2349","TUNNEL MACHINE DIAMETRE 3.76M","R39"];
batiments["42"]=["2403","TUNNEL MACHINE DIAMETRE 3.76M","R42"];
batiments["1U"]=["2180","BATIMENT VENTILATION PRINCIPALE","SU1"];
batiments["8Y"]=["2890","BATIMENT CONTROLE ACCES AU SITE","SY8"];
batiments["40"]=["2428","HALL D'EXPERIENCE NIVEAU 4.2/5.2","UX451"];
batiments["60"]=["2628","HALL D'EXPERIENCE NIVEAU 0","UX650"];
batiments["60"]=["2628","HALL D'EXPERIENCE NIVEAU 5.2/5.7","UX651"];
batiments["L18"]=["2198","POINT 1.8 LHC/LEP SURFACE","L18S"];
batiments["D18"]=["2156","BATIMENT DETRUIT","SD18"];
batiments["76"]=["2725","TUNNEL MACHINE DIAMETRE 3.76M","R74"];
batiments["80"]=["2828","HALL D'EXPERIENCE NIVEAU 4.35/5.2","UX851"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE A1","UXA451"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE A1","UXA851"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE A2","UXA852"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE B1","UXB451"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE B1","UXB851"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE B2","UXB852"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE B3","UXB853"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE C1","UXC451"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE C2","UXC452"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE C3","UXC453"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE C1","UXC851"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE C2","UXC852"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE D1","UXD451"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE D2","UXD452"];
batiments["40"]=["2428","SALLE ELECTRONIQUE ALEPH. SALLE D3","UXD453"];
batiments["80"]=["2828","SALLE ELECTRONIQUE DELPHI. SALLE D1","UXD851"];
batiments["56"]=["2537","CHAMBRE DE JONCTION UJ56 NIVEAU 0","UJ56-0"];
batiments["56"]=["2537","CHAMBRE DE JONCTION UJ561 NIVEAU 1","UJ56-1"];
batiments["76"]=["2737","CHAMBRE DE JONCTION UJ760","UJ760"];
batiments["76"]=["2737","CHAMBRE DE JONCTION UJ761","UJ761"];
batiments["22"]=["2213","CHAMBRE DE JONCTION UJ23 PARTIE MACHINE","RJ23"];
batiments["28"]=["2240","CHAMBRE DE JONCTION UJ27 PARTIE MACHINE","RJ27"];
batiments["56"]=["2537","CHAMBRE DE JONCTION UJ56 PARTIE MACHINE","RJ56"];
batiments["62"]=["2613","CHAMBRE DE JONCTION UJ63 PARTIE MACHINE","RJ63"];
batiments["2X"]=["2285","BATIMENT EXPERIMENTAL","SX20"];
batiments["EL"]=["3999","ENSEMBLE DU SITE LHC/LEP","EL"];
batiments["68"]=["2640","CHAMBRE DE JONCTION UJ67 PARTIE MACHINE","RJ67"];
batiments["76"]=["2737","CHAMBRE DE JONCTION UJ76 PARTIE MACHINE","RJ76"];
batiments["15"]=["2127","ZONE DE SERVICE NIVEAU 0","US150"];
batiments["15"]=["2127","ZONE DE SERVICE NIVEAU 1","US151"];
batiments["15"]=["2127","ZONE DE SERVICE NIVEAU 2","US152"];
batiments["15"]=["2127","ZONE DE SERVICE NIVEAU 3","US153"];
batiments["25"]=["2227","ZONE DE SERVICE NIVEAU 0","US250"];
batiments["25"]=["2227","ZONE DE SERVICE NIVEAU 1","US251"];
batiments["25"]=["2227","ZONE DE SERVICE NIVEAU 2","US252"];
batiments["25"]=["2227","ZONE DE SERVICE NIVEAU 3","US253"];
batiments["45"]=["2427","ZONE DE SERVICE NIVEAU 0","US450"];
batiments["45"]=["2427","ZONE DE SERVICE NIVEAU 1","US451"];
batiments["45"]=["2427","ZONE DE SERVICE NIVEAU 2","US452"];
batiments["45"]=["2427","ZONE DE SERVICE NIVEAU 3","US453"];
batiments["65"]=["2627","ZONE DE SERVICE NIVEAU 0","US650"];
batiments["65"]=["2627","ZONE DE SERVICE NIVEAU 1","US651"];
batiments["65"]=["2627","ZONE DE SERVICE NIVEAU 2","US652"];
batiments["65"]=["2627","ZONE DE SERVICE NIVEAU 3","US653"];
batiments["85"]=["2827","ZONE DE SERVICE NIVEAU 0","US850"];
batiments["85"]=["2827","ZONE DE SERVICE NIVEAU 1","US851"];
batiments["85"]=["2827","ZONE DE SERVICE NIVEAU 2","US852"];
batiments["85"]=["2827","ZONE DE SERVICE NIVEAU 3","US853"];
batiments["2X"]=["2285","SOUS-STATION ELECTRIQUE","SX21"];
batiments["25"]=["2221","TUNNEL FAIBLE BETA COTE UX","RX24"];
batiments["25"]=["2235","TUNNEL FAIBLE BETA COTE UX","RX26"];
batiments["45"]=["2421","TUNNEL FAIBLE BETA COTE UX","RX44"];
batiments["45"]=["2435","TUNNEL FAIBLE BETA COTE UX","RX46"];
batiments["65"]=["2621","TUNNEL FAIBLE BETA COTE UX","RX64"];
batiments["65"]=["2635","TUNNEL FAIBLE BETA COTE UX","RX66"];
batiments["85"]=["2821","TUNNEL FAIBLE BETA COTE UX","RX84"];
batiments["85"]=["2835","TUNNEL FAIBLE BETA COTE UX","RX86"];
batiments["2F"]=["2265","TOURS DE REFROID. ET S/STATION DE POMPAGE","SF22"];
batiments["4F"]=["2465","TOURS DE REFROID. ET S/STATION DE POMPAGE","SF40"];
batiments["4F"]=["2465","TOURS DE REFROID. ET S/STATION DE POMPAGE","SF42"];
batiments["6F"]=["2665","TOURS DE REFROID. ET S/STATION DE POMPAGE","SF61"];
batiments["20"]=["2222","SALLE ELECTRONIQUE L3 PX24 NIVEAU 1","PXA241"];
batiments["20"]=["2222","SALLE ELECTRONIQUE L3 PX24 NIVEAU 2","PXA242"];
batiments["20"]=["2222","SALLE ELECTRONIQUE L3 PX24 NIVEAU 3","PXA243"];
batiments["20"]=["2222","SALLE ELECTRONIQUE L3 PX24 TERRASSE NIVEAU 5","PXA245"];
batiments["1D"]=["2155","BATIMENT DE DECHARGEMENT","SD1"];
batiments["1U"]=["2180","BATIMENT VENTILATION PRINCIPALE - REZ DE CH.","SU10"];
batiments["1U"]=["2180","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU11"];
batiments["2U"]=["2280","BATIMENT VENTILATION PRINCIPALE - REZ DE CH.","SU20"];
batiments["2U"]=["2280","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU21"];
batiments["3U"]=["2380","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU31"];
batiments["4U"]=["2480","BATIMENT VENTILATION PRINCIPALE - REZ DE CH.","SU40"];
batiments["5U"]=["3580","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU51"];
batiments["6U"]=["2680","BATIMENT VENTILATION PRINCIPALE - REZ DE CH.","SU60"];
batiments["6U"]=["2680","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU61"];
batiments["7U"]=["2780","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU71"];
batiments["8U"]=["2880","BATIMENT VENTILATION PRINCIPALE - REZ DE CH.","SU80"];
batiments["8U"]=["2880","BATIMENT VENTILATION PRINCIPALE - ETAGE","SU81"];
batiments["2X"]=["2282","BAT. ANNEXE DE VENTIL. (SUX) NIVEAU 0","SUX20"];
batiments["2X"]=["2282","BAT. ANNEXE DE VENTIL. (SUX) NIVEAU 1","SUX21"];
batiments["2X"]=["2282","BAT. ANNEXE DE VENTIL. (SUX) NIVEAU 2","SUX22"];
batiments["8X"]=["2882","BAT. ANNEXE DE VENTIL. (SUX) NIVEAU 0","SUX80"];
batiments["8X"]=["2882","BAT. ANNEXE DE VENTIL. (SUX) NIVEAU 1","SUX81"];
batiments["3Z"]=["2395","SOUS STATION ELECTRIQUE (CHALET SZ33)","SZS33"];
batiments["3Z"]=["2395","POSTE AUXILIAIRE EDF (20KV)","SZE33"];
batiments["L1"]=["2199","POINT 1 (SURFACE + SOUTERRAIN) EMPRISE CERN","L1"];
batiments["L2"]=["2299","POINT 2 (SURFACE + SOUTERRAIN) EMPRISE CERN","L2"];
batiments["L3"]=["2399","POINT 3 (SURFACE + SOUTERRAIN) EMPRISE CERN","L3"];
batiments["L4"]=["2499","POINT 4 (SURFACE + SOUTERRAIN) EMPRISE CERN","L4"];
batiments["L5"]=["2599","POINT 5 (SURFACE + SOUTERRAIN) EMPRISE CERN","L5"];
batiments["L6"]=["2699","POINT 6 (SURFACE + SOUTERRAIN) EMPRISE CERN","L6"];
batiments["L7"]=["2799","POINT 7 (SURFACE + SOUTERRAIN) EMPRISE CERN","L7"];
batiments["L8"]=["2899","POINT 8 (SURFACE + SOUTERRAIN) EMPRISE CERN","L8"];
batiments["4E"]=["2460","ENCLOS NIVEAU HAUTE TENSION","SEH4"];
batiments["8E"]=["2860","ENCLOS NIVEAU HAUTE TENSION","SEH8"];
batiments["E18"]=["2173","SOUS STATION ELECTRIQUE  SM18","SE18"];
batiments["ELU"]=["3999","ENSEMBLE DU SITE LHC/LEP SOUTERRAIN","ELU"];
batiments["ELS"]=["3999","ENSEMBLE DU SITE LHC/LEP SURFACE","ELS"];
batiments["L18"]=["2198","POINT 1.8 LHC/LEP","L18"];
batiments["L18"]=["2198","POINT 1.8 LHC/LEP SOUTERRAIN","L18U"];
batiments["68"]=["2647","TUNNEL MACHINE DIAMETRE 3.76M","R681"];
batiments["68"]=["2647","TUNNEL MACHINE DIAMETRE 3.76M","R682"];
batiments["68"]=["2647","TUNNEL MACHINE DIAMETRE 3.76M","R683"];
batiments["13"]=["3110","CAVERNE REDRESSEUR VERS PT8 NIV 0","RR130"];
batiments["13"]=["3110","CAVERNE REDRESSEUR VERS PT8 NIV 1","RR131"];
batiments["17"]=["3141","CAVERNE REDRESSEUR VERS PT2 NIV 0","RR170"];
batiments["17"]=["3141","CAVERNE REDRESSEUR VERS PT2 NIV1","RR171"];
batiments["53"]=["3508","CAVERNE REDRESEUR VERS PT4 NIV 0","RR530"];
batiments["53"]=["3508","CAVERNE REDRESEUR VERS PT4 NIV 1","RR531"];
batiments["57"]=["3541","CAVERNE REDRESSEUR VERS PT6 NIV 0","RR570"];
batiments["57"]=["3541","CAVERNE REDRESSEUR VERS PT6 NIV 1","RR571"];
batiments["73"]=["3718","CAVERNE ELECTRIQUE FEEDBOX VERS PT6 NIV 0","RR730"];
batiments["73"]=["3718","CAVERNE ELECTRIQUE FEEDBOX VERS PT6 NIV 1","RR731"];
batiments["77"]=["3732","CAVERNE ELECTRIQUE FEEDBOX VERS PT8 NIV 0","RR770"];
batiments["77"]=["3732","CAVERNE ELECTRIQUE FEEDBOX VERS PT8 NIV 1","RR771"];
batiments["65"]=["2613","CHAMBRE DE JONCTION - NIV 0","UJ630"];
batiments["65"]=["2613","CHAMBRE DE JONCTION - NIV 1","UJ631"];
batiments["65"]=["2640","CHAMBRE DE JONCTION - NIV 0","UJ6701"];
batiments["65"]=["2640","CHAMBRE DE JONCTION - NIV 1","UJ671"];
batiments["5XC"]=["6593","BARAQUE","SXC5"];
batiments["C18"]=["3153","HANGAR DE STOCKAGE","3153"];
batiments["2A"]=["3288","BATIMENT DE STOCKAGE","SXS2"];
batiments["M18"]=["2173","HALL D'ASSEMBLAGE DES AIMANTS (1er etage)","SM181"];
batiments["B18"]=["3174","SALLE DE CONTROLE CRYO","SHC18"];
batiments["25"]=["2229","CENTRALE DE REFROIDISSEMENT (RdeCH)","UW250"];
batiments["25"]=["2229","CENTRALE DE REFROIDISSEMENT (1er Niveau))","UW251"];
batiments["C18"]=["3152","BATIMENT BUREAUX","3152"];
batiments["15"]=["2111","TUNNEL ELARGI AU DIAM. 4.40M FAISC. 800/UJ14","RI131"];
batiments["15"]=["2111","TUNNEL ELARGI AU DIAM. 4.40M FAISC. 800/UJ14","RI132"];
batiments["15"]=["2138","TUNNEL ELARGI AU DIAMETRE 4.40M FAISCEAU 800","RI171"];
batiments["15"]=["2138","TUNNEL ELARGI AU DIAMETRE 4.40M FAISCEAU 800","RI172"];
batiments["56"]=["2525","TUNNEL MACHINE DIAMETRE 3.76M","R541"];
batiments["56"]=["2525","TUNNEL MACHINE DIAMETRE 3.76M","R542"];
batiments["56"]=["2534","TUNNEL MACHINE DIAMETRE 3.76M","R561"];
batiments["56"]=["2534","TUNNEL MACHINE DIAMETRE 3.76M","R562"];
batiments["7D"]=["6271","BARAQUE RENDUE","6271"];
batiments["1F"]=["3055","ABRI POUR APAREIL DE MESURE","MPA905"];
batiments["1F"]=["3057","ABRI POUR APAREIL DE MESURE","PAS957"];
batiments["1F"]=["3073","ABRI POUR APAREIL DE MESURE","PAS973"];
batiments["1F"]=["3010","ABRI POUR APAREIL DE MESURE","MSG910"];
batiments["1F"]=["3072","ABRI POUR APAREIL DE MESURE","MSG972"];
batiments["1F"]=["3010","ABRI POUR APAREIL DE MESURE","MSN910"];
batiments["1F"]=["3072","ABRI POUR APAREIL DE MESURE","MSN972"];
batiments["1F"]=["1220","ABRI POUR APAREIL DE MESURE","MSG120"];
batiments["1F"]=["1220","ABRI POUR APAREIL DE MESURE","MSN120"];
batiments["1F"]=["3176","CONTROLE DES EAUX","PMW910"];
batiments["1F"]=["3276","CONTROLE DES EAUX","PMW920"];
batiments["1F"]=["3376","CONTROLE DES EAUX","PMW930"];
batiments["1F"]=["3476","CONTROLE DES EAUX","PMW940"];
batiments["1F"]=["3576","CONTROLE DES EAUX","PMW950"];
batiments["1F"]=["3776","CONTROLE DES EAUX","PMW970"];
batiments["1F"]=["3876","CONTROLE DES EAUX","PMW980"];
batiments["1F"]=["3676","CONTROLE DES EAUX","PMW960"];
batiments["5X"]=["9353","ZONE RESISTANCES DECHARGES LHC PT5","SXR5"];
batiments["2A"]=["3289","BATIMENT DE STOCKAGE RENDU:NE PAS UTILISER","SXSA2"];
batiments["8H"]=["6257","BARAQUE","6257"];
batiments["1EG"]=["3158","ABRI POUR GROUPE ELECTROGENE","SEG11"];
batiments["1GL"]=["80","GLOBE DE L'INNOVATION","80"];
batiments["1F"]=["3176","CONTROLE DES EAUX","SMW1"];
batiments["1F"]=["3276","CONTROLE DES EAUX","SMW2"];
batiments["1F"]=["3376","CONTROLE DES EAUX","SMW3"];
batiments["1F"]=["3476","CONTROLE DES EAUX","SMW4"];
batiments["1F"]=["3576","CONTROLE DES EAUX","SMW5"];
batiments["1F"]=["3676","CONTROLE DES EAUX","SMW6"];
batiments["1F"]=["3776","CONTROLE DES EAUX","SMW7"];
batiments["1F"]=["3876","CONTROLE DES EAUX","SMW8"];
batiments["C18"]=["3199","ZONE DE STOCKAGE","ZONE19"];
batiments["1F"]=["1176","CONTROLE RADIATION","PMV802"];
batiments["1F"]=["3176","CONTROLE DES EAUX","PSW910"];
batiments["1F"]=["3014","ABRI POUR APAREIL DE MESURE","PSA914"];
batiments["1F"]=["3054","ABRI POUR APAREIL DE MESURE","PSA954"];
