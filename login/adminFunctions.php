<?php
//vars:
/*
$authorised=isset($_SERVER['Shib-Session-ID']);
if($authorised){
  $authorised=false;
  foreach( explode(";",$_SERVER['ADFS_GROUP']) as $group){
    if(in_array($group, array("atlas-gen","en-dep-cv","ep-dep-dt"))){
      $authorised=true;
    }
  }
  //$other_users=array("");
  //if(in_array($_SERVER["ADFS_LOGIN"],$other_users)){$authorised=true;}


}

*/
header('Content-Type: application/json');
$isadmin=false;

$administrators=json_decode(file_get_contents("../data/admin/administrators.json"),true); //administrators list

function writeToFile($administrators){
	$administrators['solans']="Carlos Solans Sanchez";
        file_put_contents('../data/admin/administrators.json',json_encode( $administrators));
}

if(isset($administrators[$_SERVER['ADFS_LOGIN']])){
	$isadmin=true; //current user is admin
	if($administrators[$_SERVER['ADFS_LOGIN']]=="undefined"){
		$administrators[$_SERVER['ADFS_LOGIN']]=$_SERVER['ADFS_FULLNAME'];
		writeToFile($administrators);
	}
}

//Next calls from javascript:

//returns the array with administrators list
if (isset($_GET["adminlist"]) ){ // && $isadmin -> to restrict admin list to admins
	echo json_encode($administrators);
}

//adds a new administrator to the json file
if (isset($_GET["addAdmin"]) && $isadmin){
	$newAdminName="";
	$newAdminLogin=$_GET["addAdmin"];
	$newAdminName=$_GET["name"];
	if(!isset($administrators[$newAdminLogin])){
		//array_push($administrators, $newAdmin);
		$administrators[$newAdminLogin]=$newAdminName;
		file_put_contents('../data/admin/administrators.json',json_encode( $administrators));
	}
}


//deletes administrator
if(isset($_GET['delAdminLogin']) && $isadmin){
	$delAdminLogin=$_GET["delAdminLogin"];

	if(isset($administrators[$delAdminLogin])){
		unset($administrators[$delAdminLogin]);
	}
	writeToFile($administrators);
}

function isAdmin(){
	$administrators=json_decode(file_get_contents("../data/admin/administrators.json"),true); //administrators list
	return in_array($_SERVER['ADFS_LOGIN'], $administrators);
}

?>



