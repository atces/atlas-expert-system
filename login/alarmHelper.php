<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("alarmHelper");
?>



<!DOCTYPE html>
<html lang="en">
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<link href="css/theme.bootstrap.css" rel="stylesheet">
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js"  id="ui"></script>
<script src="JS/searchTools.js"></script>
<script src="JS/history.js"></script>
<script src="JS/checkUrl.js"></script>
<script src="data/codification.js"></script>
<?php $styler->includeStyle(); ?>
<?php $scripter->includeScripts(); ?>
<script> GetTokenId(); </script>
<script src="JS/alarmHelper.js"></script>
</head>

<!--  body -->
<body>
<?php $pagetitle="Alarm Helper"; include("header.php"); ?>
<div class="CONTENT" style="font-size: 12px;"> 


<header>
    <h1 style="margin-top: 70px;">Date range</h1>
</header>

<a>
<div id='alarm_date_selector' style="display:flex">
    <form action="" style="margin-left: 5px; margin-right: 5px;">
    <label for="alarm_start_date"><input type="date" id="start_date" name="start_date" style="font-size: 20px"></label>
    </form> 
    <span style="font-size: 20px">to</span>
    <form action="" style="margin-left: 5px; margin-right: 5px;">
    <label for="end_date"><input type="date" id="end_date" name="end_date" style="font-size: 20px"></label>
    </form>
    <button class="button-defaultDate-alarmHelper" type="button" onclick="buttonDefaultDates()"><img src="../images/alarmHelper/defaultDate.png" alt="defaultDate" title="Today"></button>
    <button class="button-update-alarmHelper" type="button" onclick="buttonReadSqlLogs()" style="margin-left: 20px; margin-right: 5px;"><img src="../images/alarmHelper/update.png" alt="update" title="Reload"></button>
    <button class="button-upload-alarmHelper" type="button" onclick="buttonWriteDisplayEventsLogs()"><img src="../images/alarmHelper/upload.png" alt="upload" title="Save to server"></button>
    <button class="button-download-alarmHelper" type="button" onclick="buttonDownloadJSON()"><img src="../images/alarmHelper/download.png" alt="download" title="Downlaod JSON"></button>
    <button class="button-download-alarmHelper" type="button" onclick="buttonDownloadSlides()"><img src="../images/alarmHelper/download.png" alt="download" title="Downlaod Slides"></button>
</div>
</a>

<a>
    <h1>Uncategorised alarms
        <button class="button-collapseActions-alarmHelper" type="button" onclick="CollapseActions()"><img id="image-collapseActions-alarmHelper" src="../images/alarmHelper/collapseAll.png" alt="collapseActions_button" title="Collapse actions"></button>
    </h1>
    <section style="border:0px; margin:1px; padding:5px; min-width: 600px; min-height: 10px;">
    <div id="display-notGrouped"></div>
</section>
</a>


<div id="display-editEvent"></div>


<a>
<h1>Categorised events 
    <button class="button-newEdit-alarmHelper" type="button" onclick="CreateNewEvent()"><img src="../images/alarmHelper/new.png" alt="new_button" title="New Event"></button>
    <button class="button-collapseAll-alarmHelper" type="button" onclick="CollapseAll()"><img id="image-collapseAll-alarmHelper" src="../images/alarmHelper/collapseAll.png" alt="collapseAll_button" title="Collapse events"></button>
</h1>
<section style="border:0px; margin:1px; padding:5px; min-width: 300px; min-height: 100px;">
<div>
    <div id="display-events"></div>
</div>
</section>
</a>



<div class="footer"> <?php include("footer.php"); ?> </div>

</div>
</body>
</html>