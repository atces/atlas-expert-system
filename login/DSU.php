<?
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("DSU");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src = "node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/simulatorParser.js"></script>

 



<?php $scripter->includeScripts(); ?> 
<?php $styler->includeStyle(); ?>

<style id="elements_style">
  td#image_td>div{display:grid;}
  /*#simulator_tables_wrapper{display:block!important;}*/
  #simulator_tables_wrapper{display:none!important;}
</style>
</head>
  
<body>  
<? include("header.php"); ?>
<div style="position: fixed;left: 10px; z-index: 3;background-color:white"id="mousetracker"></div>
<div  class="CONTENT">
  <div class="centered" id="DSU_toolbar_wrapper">
  	<span id="DSU_toolbar">
  		<select id="select_DSU" onchange="init_LoadDSU('select');">
  		  <option value="None">Select a DSU</option>	
  		  <option value="DSU_1">DSU 1</option>
  		  <option value="DSU_2">DSU 2</option>
  		  <option value="DSU_3">DSU 3</option>
  		  <option value="DSU_4">DSU 4</option>
  		  <option value="DSU_5">DSU 5</option>
  		  <option value="DSU_6">DSU 6</option>
  		  <option value="DSU_7">DSU 7</option>
  		  <option value="DSU_8">DSU 8</option>
  		</select>
  		<button id="DSU_details"class="cursor">DSU Details</button>
  	</span>
  </div>
	<div class="container" style="width:1350px">
		<table id="maintable">
			<tr>
				<td id="image_td">
					<img id="distribution" src="img/dsu/DSU.png" style="float:left;" class="element"> <!-- width:1015px-->
				</td>
				<td id="verbose_td">
					<div id="verbose"></div>
				</td>
			</tr>
			<tr><td><div style="height:15px;" id="blank_space"><!--  for the footer bar--></div></td><td></td></tr>
		</table>
	</div>
</div>

<script>
<?if (@isset($_GET["uid"])){?>
	$(document).ready(function(){
		$("#select_DSU").val("<?=$_GET["uid"];?>");
		init_LoadDSU("<?=$_GET["uid"];?>");
		});
	
<?} ?>
</script>

<div class="footer">
	<?php include("footer.php"); ?>
</div>
</body> 
</html>
