<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("pageElements");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<?php include ("favicon.php");?>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		 
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>
		<?php include_once "data/PHPdatagrouped.php"; ?>
	</head>
	<body class="grey_background" onload="process_smalltable('smolboi');">  
		<?include("header.php"); ?>
		<?
		$pageid=$_GET["page"];
		$pagetitle=$strings->get($_GET['page']);
		$pageurl=$pageid.".php";
		?>
		<div style="margin:70px;">
		<a style="text-decoration: none; color: blue;" href="<?php echo $pageurl;?>"><h1><?php echo $pagetitle;?></h1></a>
		<table id="search_table">
		<thead>
		</thead>
		<tbody>
		<?php
		$elements = $data[$pageid];	
		foreach($elements as $key => $val) {
			echo "<tr>";
			echo "<td>$val</td>";
			echo "<td><img onclick=\"displayHistory('$val')\" src='../images/history.png' class='cursor'></td>";
			echo "</tr>";
		}
		?>
		</tbody>
		</table>
		</div>



		<?include("footer.php"); ?>
	</body>	
</html>
