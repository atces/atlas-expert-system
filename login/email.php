<?php

include_once("functions.php");
$jsonFilename='data/requestsDB.json';

$cmd=$_POST["cmd"];
$phpverbose=$debug;

function saveToFile($list){
	
	$encoded=json_encode($list);
	global $jsonFilename;
	$jsonFile=fopen($jsonFilename, 'w+');
	fwrite($jsonFile, $encoded);
	fclose($jsonFile);
		
}


function process($type, $entry, $comment){
	if (substr($_SERVER['SERVER_NAME'], 0,23)=="atlas-expert-system-dev"){
		$servername="atlas-expert-system-dev";
	}else $servername="atlas-expert-system";
	
	include_once('scripts/email.php');
	$email = new email();
	$email->setSender("no-reply", "no-reply@cern.ch");
	$email->setServer("cernmx.cern.ch",25,"","");
	$email->setSmtp(true);
	$email->setSubject("Alarm inhibit request - Expert System");
	
	
	if (substr($_SERVER['SERVER_NAME'], 0,23)=="atlas-expert-system-dev"){
		$email->addTo("carlos.solans@cern.ch");
		$email->addTo("ignacio.asensi@cern.ch");	
	}else{
		$email->addTo($entry["mail"]);
		$email->addTo("carlos.solans@cern.ch");
		$email->addTo("andre.rummler@cern.ch");
		$email->addTo("florian.haslbeck@cern.ch");
		$email->addTo("ryan.mckenzie@cern.ch");
	}
	
	$message .= "Do not reply to this message \n";
	switch($type){
		case "requestRemove":
			$message .= "The removal of an inhibit has been requested \n";
			$message .= "Requested by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "add":
			$message .= "A request to inhibit DSS alarms or actions have been created. \n";
			$message .= "Requested by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "decline":
			$message .= "Request to inhibit alarms of the DSS has been DECLINED"."\n";
			$message .= "Declined by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "approve":
			$message .= "Request to inhibit alarms of the DSS has been APPROVED"."\n";
			$message .= "Approved by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "edit":
			$message .= "Request to inhibit alarms of the DSS has been MODIFIED"."\n";
			$message .= "Mofified by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "set_as_inhibit":
			$message .= "The following request has been applied to DSS"."\n";
			$message .= "Applied by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
		case "set_as_NOTinhibit":
			$message .= "The following request has been removed from DSS"."\n";
			$message .= "Removed by: ".$_SERVER["OIDC_CLAIM_name"]."\n";
			break;
	}

	if (strcmp($comment,"")!==0){
		$message .= "Comments: ".$comment;
		}
	$message .= "\n";
	$message .= "\n";
	$message .= "Request:----------------------------------------------\n";
	$message .= "Provided email: ".$entry["mail"]."\n";
	$message .= "Requested  by: ".$entry["requestor"]."\n";
	$message .= "Beginning datetime: ".$entry["beginning"]."\n";
	$message .= "Expiration date: ".$entry["expiration"]."\n";
	$message .= "\n";
	$message .= "Reason: ".$entry["reason"]."\n";
	$message .= "\n";
	$message .= "Inhibited items: ";
	if (is_array($entry["alarms"])){
		$message.="\n";
		foreach($entry["alarms"] as $value){
			$message.=$value."\n";
		}
	}else $message.=$entry["alarms"]."\n";//FIXME It seems that $selected is always and array, the condition doesn't seem to be necessary. Check again
	$message .= "\n";
	$message .= "Link: \n";
	//$message .= "https://atlas-expert-system.web.cern.ch/atlas-expert-system/login/inhibitRequest.php";
	$message .= "https://".$servername.".web.cern.ch/login/templates/viewInhibited.php?uid=".$entry["id"];
	$message .= "\n";
	$message .= "------------------------------------------------------\n";
	$email->setMessage($message);
	$email->send();



}

function approve(){


}


switch($cmd){
	case 'requestRemove':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to save
		//echo "comment:".$comment;
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["status"]="approved";
				process("requestRemove",$existinglist[$position],$comment);
			}
		}
		break;
		//var_dump($existinglist);
		//saveToFile($existinglist);
	case 'add':
		if($phpverbose) echo "-----------ADD----------\n";
		$selected=$_POST["selected"];
		$reason=$_POST["reason"];
		$mail=$_POST["requestor"];
		$expiration=$_POST["expiration"];
		$beginning=$_POST["beginning"];
		$requestor=$_SERVER["OIDC_CLAIM_name"];
		$inhibited="no";
		$new_uid=uniqid();
		$requestedon = date("d/m/y H:m");
		$entry=array(
				"alarms"=>$selected,
				"requestor"=>$requestor,
				"mail"=> $mail,
				"beginning"=>$beginning,
				"expiration"=>$expiration,
				"reason"=>$reason,
				"id" => $new_uid,
				"requestedon"=>$requestedon,
				"status" => "pending",
				"inhibited" => $inhibited,
		);
			
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);
		if (empty($existinglist)){
			if($phpverbose)echo "File empty. Creatin new list\n";			
			$existinglist["0"]=$entry;
		}else{
			if($phpverbose)echo "Adding".$entry["id"]."\n";
			array_push($existinglist,$entry);
		}
		process("add",$entry,$comment);
		if($phpverbose)echo "after".count($existinglist)."\n";
		saveToFile($existinglist);
		break;
	case 'del':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
			
			
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);
			
		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				unset($existinglist[$position]);
					
			}
		}
		//var_dump($existinglist);
		saveToFile($existinglist);
		break;
	case 'approve':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to save
		//echo "comment:".$comment;
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["status"]="approved";
				process("approve",$existinglist[$position],$comment);
			}
		}
		//var_dump($existinglist);
		saveToFile($existinglist);
			
		break;
	case 'decline':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to sav
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["status"]="declined";
				process("decline",$existinglist[$position],$comment);
			}
		}
		var_dump($existinglist);
		saveToFile($existinglist);
		break;
	case 'archive':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to sav
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["status"]="archived";
			}
		}
		//var_dump($existinglist);
		saveToFile($existinglist);
		break;
	case 'restore':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to sav
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["status"]="pending";
			}
		}
		saveToFile($existinglist);
		break;
	case 'edit_get'://retrieve one request
		$uid=$_POST["uid"];
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);
		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				echo json_encode($existinglist[$position]);
			}
		}
		
		break;
	case 'edit':
		$selected=$_POST["selected"];
		$reason=$_POST["reason"];
		$mail=$_POST["requestor"];
		$expiration=$_POST["expiration"];
		$beginning=$_POST["beginning"];
		$requestor=$_SERVER["OIDC_CLAIM_name"];
		$uid=$_POST["uid"];
			
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);
		$requestedon = date("d/m/y H:m");
		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["alarms"]=$selected;
				$existinglist[$position]["requestor"]=$requestor;
				$existinglist[$position]["mail"]=$mail;
				$existinglist[$position]["beginning"]=$beginning;
				$existinglist[$position]["expiration"]=$expiration;
				$existinglist[$position]["reason"]=$reason;
				$existinglist[$position]["requestedon"]=$requestedon;
				process("edit",$existinglist[$position],$comment);
				
			}
		}
		/*
		$file=fopen($filename, 'w+');
		fwrite ($file, serialize($existinglist));
		fclose($file);
		 */
		saveToFile($existinglist);
		break;
	case 'set_as_inhibit':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to sav
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["inhibited"]="yes";
				process("set_as_inhibit",$existinglist[$position],$comment);
			}
		}
		saveToFile($existinglist);
		break;
	case 'set_as_NOTinhibit':
		$entry=$_POST["cmd"];
		$uid=$_POST["uid"];
		$comment=$_POST["comment"];//only for mail, not to sav
		$existing=file_get_contents($jsonFilename);
		$existinglist=json_decode($existing, true);

		foreach ($existinglist as $position=> $value){
			if($value["id"]==$uid){
				$existinglist[$position]["inhibited"]="no";
				process("set_as_NOTinhibit",$existinglist[$position],$comment);
			}
		}
		saveToFile($existinglist);
		break;
	case 'migrate':
		/*Special case used to migrate from phpserialization to json*/
		$existing=file_get_contents($filename);
		$existinglist=unserialize($existing);
		foreach ($existinglist as $position=> $value){
			
				$existinglist[$position]["inhibited"]="no";
			
		}
		saveToFile($existinglist);
		break;
}
?>
