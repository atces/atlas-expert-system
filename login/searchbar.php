<div id="toolsbar">
	<div id="search_div">
	  	<!-- 
	  	<input id="search_input" type="text" />
	  	<input id="search_button" type="button" name="action" value="Search" onclick="initSearch();"/>
	  	 -->
	  	<form id="searchbar_form" method="GET" action="<?echo $stage;?>/login/search.php">
	        <input id="search_button" type="submit" value="Search" style="float: right;">
	        <div style="overflow: hidden; padding-right: 1em;">
	          <input id="search_input" name="query" type="text" style="width: 100%;">
	        </div>
        </form>
        
	  	<label>Advanced search</label> <input type="checkbox" id="ad_search_toggle" onclick="AdvancedSearchToggle();"/>
	  	<input id="history_button" type="button" value="History" onclick="displayHistoryBar();"/>
  	</div>
</div>
<div id="toolsbar2" style="display:none">
	<div id="advanced_search_div">
		<table id="advanced_search_table">
			<!--<tr>
			<td>Search mode</td>
				<td>
					<select id="ad_search_mode" style="width:120px" onchange="searchMode();">
						<option value="classic" selected>Classic</option>
						<option value="table">Detailed Table</option>
					</select>
				</td>
				<td class="verbose">Detailed Table works only with one type</td>
			</tr>-->	
			<tr>
				<td>Type of object</td>
				<td>
					<select id="ad_search_type" style="width:120px" onchange="searchInputToggle()">
						<option value="All">Select...</option>
					</select>
				</td>
				<td class="verbose"></td>
			</tr>
		</table>
  	</div>
</div>
<script>
	function fillClassesSearchBar(){
		function parseClasses(json){ 
			for(each in json["Classes"]){
	 			text+="<option value=\""+json["Classes"][each]+"\">"+json["Classes"][each]+"</option>";
			}
			$('#ad_search_type').append(text);
		}
		var text="";
		GetClasses(parseClasses);
	}
	fillClassesSearchBar();
</script>
