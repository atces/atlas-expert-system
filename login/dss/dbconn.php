<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$con=oci_connect("atlas_dss_writer","WORK4DSS9","ATONR_ADG");
if (!$con) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
$sti=oci_parse($con,"SELECT * FROM DSS_ALARM_CONDITION ORDER BY SERIAL ASC");
oci_execute($sti);
while (($row = oci_fetch_assoc($sti))!= false) {
    print_r($row);
}
oci_close($con);
  
?>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1>ATLAS DSS quick links</h1>
<ul>
  <li><a href="http://atlas-expert-system.web.cern.ch">ATLAS Technical Coordination Expert System</a></li>
  <li><a href="https://atlasop.cern.ch/dcs/dcs/process.php?page=ATL_SAF::SAF_DSS&subd=IS">DSS status online</a></li>
  <li><a href="https://atglance.web.cern.ch/atglance/DSS/">Glance DSS interface</a></li>
  <li><a href="http://atlas-dss.web.cern.ch">Ancient DSS page</a></li>
</ul>
</body>  
</html>
