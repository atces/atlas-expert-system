<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<title>ATLAS DSS Input</title>
<link rel="stylesheet" href="style.css">

</head>
<body>
<h1>ATLAS DSS Input</h1>

<h2>Input</h2>
<div id="input"></div>

<h2>Alarms</h2>
<div id="alarms"></div>

<h2>History</h2>
<div id="history"></div>

<script>
  
  function fillInput(data){
    src="<h3><ul>";
    src+="<li>Name: "+data['NAME']+"</li>";
    src+="<li>Status: "+data['STATUS']+"</li>";
    src+="<li>Inhibit: "+data['INHIBIT']+"</li>";    
    src+="</ul></h3>";
    $('#input').html(src);
  }
  
  function fillAlarms(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Delay[s]</th>";
    src+="<th>Status</th>";
    src+="<th>Inhibit</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td><a href='alarm.php?alarm_name="+ele['name']+"'>"+ele['name']+"</a></td>";
      src+="<td>"+ele['delay']+"</td>";
      src+="<td></td>";
      src+="<td></td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#alarms').html(src);
  }
  
  function fillHistory(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Type</th>";
    src+="<th>Class</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['TYPE']+"</td>";
      src+="<td>"+ele['CLASS']+"</td>";
      src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#history').html(src);
  }
  
  $(document).ready(function(){
  qs = new URLSearchParams(window.location.search);
  if(!qs.has('input_name')) return;
  if(qs.get('input_name')=="") return;
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"get_input",name:qs.get('input_name')},
    type: 'get',
    success: function(reply) {
      data=JSON.parse(reply);
      console.log(data);
      if(data['input']=="false")return;        
      fillInput(data['input']);
      fillAlarms(data['alarms']);
      fillHistory(data['history']);
    }
  });
 });

</script>  
</body>  
</html>
