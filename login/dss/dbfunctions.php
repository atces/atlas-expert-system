<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$d=$_GET;
$r=array();

$con=oci_connect("atlas_dss_writer","WORK4DSS9","ATONR_ADG");
if (!$con) {
  $e = oci_error();
  trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if(@$d["cmd"]=="get_alarm_details"){
  $r["alarm"] = array("OP2"=>"","DP2"=>"","NAME"=>$d["name"],"STATUS"=>"Alarm does not exist");
  $r["input"] = array();
  $r["output"] = array();
  $r["history"] = array(); 
    
  $sti=oci_parse($con,"SELECT * FROM DSS_ALARM_CONDITION WHERE NAME='".$d["name"]."' ORDER BY SERIAL DESC FETCH NEXT 1 ROWS ONLY");
  $sti=oci_parse($con,"SELECT * FROM DSS_ALARM_CONDITION INNER JOIN DSS_ITEM_USER_STATUS ON DSS_ALARM_CONDITION.NAME=DSS_ITEM_USER_STATUS.NAME WHERE DSS_ALARM_CONDITION.NAME='".$d["name"]."' ORDER BY SERIAL DESC FETCH NEXT 1 ROWS ONLY");
  oci_execute($sti);
  $desc = oci_fetch_assoc($sti);
  if($desc===false){}
  else{$r["alarm"]=$desc;}
  
  $inputs=explode(",",$r["alarm"]["DP2"]);
  $types=explode(",",$r["alarm"]["OP2"]);
  for($i = 0; $i < count($inputs); $i++){
    if($inputs[$i]==""){continue;}
    if($types[$i]=="D"){
      $sti=oci_parse($con,"SELECT * FROM DSS_INPUT_D INNER JOIN DSS_ITEM_USER_STATUS ON DSS_INPUT_D.NAME=DSS_ITEM_USER_STATUS.NAME WHERE DSS_INPUT_D.NAME='".$inputs[$i]."' ORDER BY SERIAL DESC FETCH NEXT 1 ROWS ONLY");
      oci_execute($sti);
      $r["input"][] = oci_fetch_assoc($sti);
    }else if($types[$i]=="H"){
      $sti=oci_parse($con,"SELECT * FROM DSS_INPUT_A INNER JOIN DSS_ITEM_USER_STATUS ON DSS_INPUT_A.NAME=DSS_ITEM_USER_STATUS.NAME WHERE DSS_INPUT_A.NAME='".$inputs[$i]."' ORDER BY SERIAL DESC FETCH NEXT 1 ROWS ONLY");
      oci_execute($sti);
      $r["input"][] = oci_fetch_assoc($sti);  
    }
  }  
  
  $sti=oci_parse($con,"SELECT * FROM DSS_ALARM_ACTION INNER JOIN DSS_ITEM_USER_STATUS ON DSS_ALARM_ACTION.ACTION=DSS_ITEM_USER_STATUS.NAME WHERE DSS_ALARM_ACTION.ALARM='".$d["name"]."' ORDER BY DSS_ALARM_ACTION.NAME ASC, SERIAL DESC FETCH NEXT 1000 ROWS ONLY");
  oci_execute($sti);
  $aam=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array($row["NAME"],$aam)){continue;}
    $aam[]=$row["NAME"];
    $r["output"][]=$row;
  }
  
  $sti=oci_parse($con,"SELECT * FROM DSS_LOGBOOK WHERE NAME='".$d["name"]."' ORDER BY SERIAL DESC");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    $r["history"][]=$row;
  }
  
}else if(@$d["cmd"]=="get_alarm_active"){
  $r["actions"] = array();
  $r["alarms"] = array();
  $r["inputs"] = array();
    
  //deleted
  $sti=oci_parse($con,"SELECT * FROM DSS_OUTPUT_D WHERE OPCODE='DELETED' ORDER BY SERIAL DESC");
  oci_execute($sti);
  $deleted=array();
  $deleted[]="O_INF_CRY_Power_Y0721A2";
  while(($row = oci_fetch_assoc($sti))!=false){
    $deleted[]=$row["NAME"];
  }
  
  //actions
  $sti=oci_parse($con,"SELECT * FROM DSS_ITEM_USER_STATUS WHERE NAME LIKE 'O_%'");
  oci_execute($sti);
  $actions=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array(trim($row["NAME"]),$deleted)){continue;}
    $actions[]=$row["NAME"];
  }
  
  //get actions that went
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,NAME FROM DSS_LOGBOOK WHERE TYPE='ACTION_WENT' GROUP BY NAME");
  oci_execute($sti);
  $went=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(!in_array($row["NAME"],$actions)){continue;}
    $went[$row["NAME"]]=$row["SERIAL"];
  }
  
  //get actions that came
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,NAME FROM DSS_LOGBOOK WHERE TYPE='ACTION_CAME' GROUP BY NAME");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    if(!in_array($row["NAME"],$actions)){continue;}
    if($row["SERIAL"]>$went[$row["NAME"]]){
      $r["actions"][]=array("name"=>$row["NAME"],"came"=>$row["SERIAL"],"went"=>$went[$row["NAME"]]);
    }
  }
  
  //list of alarms
  $sti=oci_parse($con,"SELECT trim(NAME) AS NAME FROM DSS_ITEM_USER_STATUS WHERE NAME LIKE 'AL_%'");
  oci_execute($sti);
  $alarms=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array($row["NAME"],$deleted)){continue;}
    $alarms[$row["NAME"]]=array("came"=>0,"went"=>0);
  }
  
  //alarms came
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,trim(NAME) AS NAME  FROM DSS_LOGBOOK WHERE TYPE='ALARM_CAME' GROUP BY NAME");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    //print_r($row);
    if(!array_key_exists($row["NAME"],$alarms)){continue;}
    $alarms[$row["NAME"]]["came"]=$row["SERIAL"];
  }
  //print_r($alarms);
  
  //alarms went
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,trim(NAME) AS NAME  FROM DSS_LOGBOOK WHERE TYPE='ALARM_WENT' GROUP BY NAME");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    if(!array_key_exists($row["NAME"],$alarms)){continue;}
    $alarms[$row["NAME"]]["went"]=$row["SERIAL"];
  }
  $alarms["AL_INF_Power_USA15_EOD1_UPSFailure"]["went"]=1553067106.203;
  $alarms["AL_INF_Power_SDX1_EOD1_UPSFailure"]["went"]=1553067722.403;
  
  //print_r($alarms);
  
  //alarms return
  foreach($alarms as $name => $values){
    if($values["came"] > $values["went"]){
      $r["alarms"][]=array("name"=>$name,"came"=>$values["came"],"went"=>$values["went"]);
    }
  
  }
  
  //list of digital inputs
  $sti=oci_parse($con,"SELECT * FROM DSS_ITEM_USER_STATUS WHERE NAME LIKE 'DI_%'");
  oci_execute($sti);
  $inputs=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array(trim($row["NAME"]),$deleted)){continue;}
    $inputs[]=trim($row["NAME"]);
  }
  
  //digital inputs went
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,trim(NAME) AS NAME  FROM DSS_LOGBOOK WHERE TYPE='DIGITAL_TRUE_WENT' OR TYPE='INPUT_INHIBIT' GROUP BY NAME");
  oci_execute($sti);
  $input_went=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(!in_array($row["NAME"],$inputs)){continue;}
    $input_went[$row["NAME"]]=$row["SERIAL"];
  }
  $input_went["DI_Emergency_ATLAS_OFF"]=1515423600.000;
  
  //digital inputs came
  $sti=oci_parse($con,"SELECT MAX(PVSSTIME) AS SERIAL,trim(NAME) AS NAME FROM DSS_LOGBOOK WHERE TYPE='DIGITAL_TRUE_CAME' GROUP BY NAME");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    if(!in_array($row["NAME"],$inputs)){continue;}
    if($row["SERIAL"]>$input_went[$row["NAME"]]){
      $r["inputs"][]=array("name"=>$row["NAME"],"came"=>$row["SERIAL"],"went"=>$input_went[$row["NAME"]]);
    }
  }
  
  
}else if(@$d["cmd"]=="get_action"){
  
  $sti=oci_parse($con,"SELECT * FROM DSS_OUTPUT_D WHERE name='".$d["name"]."'");
  oci_execute($sti);
  $r["action"] = oci_fetch_assoc($sti);
  $r["alarms"] = array();
  $r["history"] = array();
  
  $sti=oci_parse($con,"SELECT * FROM DSS_ALARM_ACTION WHERE action='".$d["name"]."' ORDER BY ALARM, SERIAL DESC");
  oci_execute($sti);
  $alarms=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array($row["ALARM"],$alarms)){continue;}
    $alarms[]=$row["ALARM"];
    if(strpos("DELETED",$row["OPCODE"])!==false){continue;}
    $r["alarms"][]=array("name"=>$row["ALARM"],"delay"=>$row["DELAY"]);
  }
    
  $sti=oci_parse($con,"SELECT * FROM DSS_LOGBOOK WHERE NAME='".$d["name"]."' ORDER BY SERIAL DESC");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    $r["history"][]=$row;
  }
}else if(@$d["cmd"]=="get_input"){
  
  $sti=oci_parse($con,"SELECT * FROM DSS_INPUT_D WHERE name='".$d["name"]."'");
  oci_execute($sti);
  $r["input"] = oci_fetch_assoc($sti);
  $r["alarms"] = array();
  $r["history"] = array();
  
  $sti=oci_parse($con,"SELECT * FROM DSS_ALARM_CONDITION WHERE DP2 LIKE '%".$d["name"]."%' ORDER BY NAME, SERIAL DESC");
  oci_execute($sti);
  $alarms=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    if(in_array($row["NAME"],$alarms)){continue;}
    $alarms[]=$row["NAME"];
    if(strpos("DELETED",$row["OPCODE"])!==false){continue;}
    $r["alarms"][]=array("name"=>$row["NAME"]);
  }
    
  $sti=oci_parse($con,"SELECT * FROM DSS_LOGBOOK WHERE NAME='".$d["name"]."' ORDER BY SERIAL DESC");
  oci_execute($sti);
  while(($row = oci_fetch_assoc($sti))!=false){
    $r["history"][]=$row;
  }
}else if(@$d["cmd"]=="get_report"){
  $ts=strtotime("-7 days");
  if(isset($d["days"])){$ts=strtotime("-".$d["days"]." days");}
  $sql="SELECT * FROM DSS_LOGBOOK WHERE ( ( TYPE='ALARM_CAME' AND NAME LIKE 'AL_%%' ) OR ( TYPE='ACTION_CAME' AND NAME LIKE 'O_%%' ) ) AND PVSSTIME>".$ts." ORDER BY PVSSTIME ASC";
  $sti=oci_parse($con,$sql);
  oci_execute($sti);
  $r["report"] = array();
  //$r["raw"]=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    //$r["raw"][]=$row;
    $r["report"][]=array("serial"=>$row["SERIAL"],"came"=>$row["SERIAL"],"time"=>date("Y-m-d H:i:s",$row["PVSSTIME"]),"name"=>$row["NAME"]);
  }
}else if(@$d["cmd"]=="get_alarms_since"){
  $ts=strtotime("1 January 2018");
  if(isset($d["date"])){$ts=strtotime($d["date"]);}
  $sql="SELECT * FROM DSS_LOGBOOK WHERE  TYPE='ALARM_CAME' AND NAME LIKE 'AL_%%'  AND PVSSTIME>".$ts." ORDER BY PVSSTIME ASC";
  $sti=oci_parse($con,$sql);
  oci_execute($sti);
  $r["report"] = array();
  $r["raw"]=array();
  while(($row = oci_fetch_assoc($sti))!=false){
    //$r["raw"][]=$row;
    $r["report"][]=array("serial"=>$row["SERIAL"],"came"=>$row["SERIAL"],"time"=>date("Y-m-d H:i:s",$row["PVSSTIME"]),"name"=>$row["NAME"]);
  }
}else if(@$d["cmd"]=="get_alarms_range"){
  // Get the Alarms in a range of dates, returns "report" array with the alarms
  $ts_since=strtotime("1 January 2018");
  $ts_until=strtotime("1 January 2050");
  if(isset($d["since"])){$ts_since=strtotime($d["since"]);}
  if(isset($d["until"])){$ts_until=strtotime($d["until"]);}
  
  // Add 24 hours to ts_until to include the entire last day
  $ts_until += 86400;

  // if ts_since is after or equal to ts_until, set ts_until to ts_since + 1 day
  if($ts_since >= $ts_until){ $ts_until = $ts_since + 86400; }
  //echo "range: [".$ts_since.", ".$ts_until."]<br/>\n";
  $sql="SELECT * FROM DSS_LOGBOOK WHERE  TYPE='ALARM_CAME' AND NAME LIKE 'AL_%%'  AND PVSSTIME>".$ts_since." AND PVSSTIME<".$ts_until." ORDER BY PVSSTIME ASC";
  //echo $sql;
  $sti=oci_parse($con,$sql);
  oci_execute($sti);

  $r["report"] = array();
  while(($row = oci_fetch_assoc($sti))!=false){
    $r["report"][]=array("serial"=>$row["SERIAL"],"came"=>$row["SERIAL"],"date"=>date("Y-m-d",$row["PVSSTIME"]),"time"=>date("H:i:s",$row["PVSSTIME"]),"name"=>trim($row["NAME"]));
  }
} else if(@$d["cmd"]=="get_alarms_and_actions_range"){
  //  Get the Alarms and Actions in a range of dates
  $ts_since=strtotime("1 January 2018");
  $ts_until=strtotime("1 January 2050");

  // Set the 'since' and 'until' timestamps if provided
  if(isset($d["since"])){$ts_since=strtotime($d["since"]);}
  if(isset($d["until"])){$ts_until=strtotime($d["until"]);}

  // Add 24 hours to ts_until to include the entire last day
  $ts_until += 86400;

  // If ts_since is after or equal to ts_until, set ts_until to ts_since + 1 day
  if($ts_since >= $ts_until){ $ts_until = $ts_since + 86400; }

  // Initialize the report structure
  $r["alarms"] = array();
  $r["actions"] = array();

  // SQL query to get alarms in the given time range
  $sql_alarms = "SELECT * FROM DSS_LOGBOOK WHERE TYPE='ALARM_CAME' AND NAME LIKE 'AL_%%' AND PVSSTIME > $ts_since AND PVSSTIME < $ts_until ORDER BY PVSSTIME ASC";
  $sti_alarms = oci_parse($con, $sql_alarms);
  oci_execute($sti_alarms);

  // Fetch and format alarms
  while(($row = oci_fetch_assoc($sti_alarms)) != false){
    $r["alarms"][] = array(
      "serial" => $row["SERIAL"],
      "came" => $row["SERIAL"],
      "date" => date("Y-m-d", $row["PVSSTIME"]),
      "time" => date("H:i:s", $row["PVSSTIME"]),
      "name" => trim($row["NAME"])
    );
  }

  // SQL query to get actions in the given time range
  $sql_actions = "SELECT * FROM DSS_LOGBOOK WHERE TYPE='ACTION_CAME' AND NAME LIKE 'O_%%' AND PVSSTIME > $ts_since AND PVSSTIME < $ts_until ORDER BY PVSSTIME ASC";
  $sti_actions = oci_parse($con, $sql_actions);
  oci_execute($sti_actions);

  // Fetch and format actions
  while(($row = oci_fetch_assoc($sti_actions)) != false){
    $r["actions"][] = array(
      "serial" => $row["SERIAL"],
      "came" => $row["SERIAL"],
      "date" => date("Y-m-d", $row["PVSSTIME"]),
      "time" => date("H:i:s", $row["PVSSTIME"]),
      "name" => trim($row["NAME"])
    );
  }
}

oci_close($con);

echo json_encode($r);
?>
