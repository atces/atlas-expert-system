<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<title>ATLAS DSS Alarms</title>
<style type="text/css">
	h2{
	  color: #019999;
	}
  .zebra tr:nth-child(odd){ 
		background: #f0f0f0;
	}
	.zebra tr:nth-child(even){
		background: #aaaaaa;
	}
	.zebra tr:first-child{ 
		background: #019999;
	}
</style>

</head>
<body>
<h1>ATLAS DSS Active Alarms and Actions</h1>

<h2>Active Alarms</h2>
<div id="alarms"></div>

<h2>Active Sensors</h2>
<div id="inputs">Not implemented yet</div>

<h2>Active Actions</h2>
<div id="actions"></div>

<script>

  function fillAlarms(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td><a href='alarm.php?alarm_name="+ele['name']+"'>"+ele['name']+"</a></td>";
      src+="<td>"+new Date(parseFloat(ele['came'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#alarms').html(src);
  }
  
  function fillActions(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['name']+"</td>";
      src+="<td>"+new Date(parseFloat(ele['came'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#actions').html(src);
  }
  
  function fillHistory(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Type</th>";
    src+="<th>Class</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['TYPE']+"</td>";
      src+="<td>"+ele['CLASS']+"</td>";
      //src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toUTCString()+"</td>";
      src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#history').html(src);
  }
  
  $(document).ready(function(){
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"get_alarm_active"},
    type: 'get',
    success: function(reply) {
      data=JSON.parse(reply);
      console.log(data);
      fillAlarms(data['alarms'].sort((a,b)=>(a.came>b.came?1:-1)));
      fillActions(data['actions'].sort((a,b)=>(a.came>b.came?1:-1)));
    }
  });
 });

</script>  
</body>  
</html>
