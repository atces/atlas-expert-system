<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<title>ATLAS DSS Alarms</title>
<link rel="stylesheet" href="style.css">

</head>
<body>
<h1>ATLAS DSS Alarm</h1>

<h2>Alarm</h2>
<div id="alarm"></div>

<h2>Input Sensors</h2>
<div id="inputs"></div>

<h2>Output Actions</h2>
<div id="outputs"></div>

<h2>Alarm History</h2>
<div id="history"></div>

<script>
  
  function fillAlarm(data){
    src="<h3><ul>";
    src+="<li>Name: "+data['NAME']+"</li>";
    src+="<li>Status: "+data['STATUS']+"</li>";
    src+="<li>Inhibit: "+data['INHIBIT']+"</li>";    
    src+="</ul></h3>";
    $('#alarm').html(src);
  }
  
  function fillInputs(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Persistency&nbsp;[s]</th>";
    src+="<th>Status</th>";
    src+="<th>Inhibit</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['NAME']+"</td>";
      src+="<td>"+ele['LATENCY']+"</td>";
      src+="<td>"+ele['STATUS']+"</td>";
      src+="<td>"+ele['INHIBIT']+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#inputs').html(src);
  }
  
  function fillOutputs(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Delay[s]</th>";
    src+="<th>Status</th>";
    src+="<th>Inhibit</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['ACTION']+"</td>";
      src+="<td>"+ele['DELAY']+"</td>";
      src+="<td>"+ele['STATUS']+"</td>";
      src+="<td>"+ele['INHIBIT']+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#outputs').html(src);
  }
  
  function fillHistory(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Type</th>";
    src+="<th>Class</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['TYPE']+"</td>";
      src+="<td>"+ele['CLASS']+"</td>";
      //src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toUTCString()+"</td>";
      src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#history').html(src);
  }
  
  $(document).ready(function(){
  qs = new URLSearchParams(window.location.search);
  if(!qs.has('alarm_name')) return;
  if(qs.get('alarm_name')=="") return;
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"get_alarm_details",name:qs.get('alarm_name')},
    type: 'get',
    success: function(reply) {
      data=JSON.parse(reply);
      console.log(data);
      if(data['alarm']=="false")return;        
      fillAlarm(data['alarm']);
      fillInputs(data['input']);
      fillOutputs(data['output']);
      fillHistory(data['history']);
    }
  });
 });

</script>  
</body>  
</html>
