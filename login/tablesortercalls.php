<!-- This file is not longer needed in the simulator pages -->
<!-- These includes are now included in scripts/jsgraphrenderhelper.php -->
<!-- Carlos January 2020 -->

<!-- Needed for alarms tables -->
<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
<script src="JS/dashboard.js"></script>
<!-- End of  Needed for alarms tables -->
