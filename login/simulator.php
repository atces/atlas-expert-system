<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
$page=(isset($_GET["page"])?$_GET["page"]:"electricity");
definePage($page);
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>

<!-- Tables includes -->
<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.min.js"></script>
<script type="text/javascript" src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
<script type="text/javascript" src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">

<script src="node_modules/@panzoom/panzoom/dist/panzoom.js"></script>

<script src="JS/db.js"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/simulatorParser.js"></script>
 
<?php $scripter->includeScripts(); ?>
<?php $styler->includeStyle(); ?>

</head>

<body>   

<?php include("header.php"); ?>
<div style="position: fixed;left: 10px; z-index: 3;background-color:white" id="mousetracker"></div>
<div class="CONTENT">
	<div class="container" style="width:2000px">
		<table id="maintable">
			<tr>
				<td id="image_td">
					<div id="distribution" style="float:left;" >
						<div id="graph" class="element">
              <?php echo file_get_contents("data/svggraphs/{$page}.svg");?>
            </div>
					 </div>
					<div id="table2">
					 <p><a style="text-decoration: none; color: blue;" href="pageElementTable.php?page=<?=$pageid;?>">All page elements</a></p>
					</div>
				</td>
				<td id="verbose_td">
					<div id="verbose"> </div>	
				</td>
			</tr>
			<tr><td><div style="height:15px;" id="blank_space"><!--  for the footer bar--></div></td><td></td></tr>
		</table>
	</div>
</div>
<div class="footer">
	<?php include("footer.php"); ?>
</div>
<script>
const element = document.getElementById('graph');

const panzoom = Panzoom(element, {excludeClass: 'rect' });
element.addEventListener('wheel', 
function (event) {
  if (!event.shiftKey) return
  panzoom.zoomWithWheel(event)
});

</script>

</body>	
</html>
