<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("admin");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src = "node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
<script src="JS/ui.js?<?=strftime("%Y%m%d%H%M%S");?>" retractableDetailsTable="true" id="ui"></script>
<script src="JS/simulatorParser.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
<script src="JS/adminFunctions.js"></script>
<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
<script src="JS/dashboard.js"></script>
<script class="tableFunctions" src="JS/tableFunctions.js" table="documentation"></script>
<script class="tableFunctions" src="JS/tableFunctions.js" table="administrators"></script>
<script class="tableFunctions" src="JS/tableFunctions.js" table="panels"></script>
<script class="tableFunctions" src="JS/tableFunctions.js" table="agenda"></script>
<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
 
<?php $scripter->includeScripts(); ?> 
<?php $styler->includeStyle(); ?>
<script>
$(document).ready(function(){
	refreshAdminList();
});

function load_photos(){
	loading("start");
	$.ajax({ url: 'templates/photos.php',	         
        type: 'post',
        success: function(output) {
                    $('#photos_container').html(output);
                    loading("stop");
                 }
	});	
}



  	  	function stateSave() {
    	    function parseStateSave(response){
            console.log(response);
            $('#state_output').text("").show();
            $('#state_output').text(response["Reply"]);//.fadeOut(10000); 
          }
    		  SaveCurrentState(document.getElementById("statename").value.replace(/[^\w\s]/gi, ''),parseStateSave);
      	}
      	
      	function parseSavedState(response){
      			console.log(response);
      			console.log("Tokenid received from GetSavedState = "+response["Tokenid"])
      			t = response["Tokenid"]
      			document.getElementById('state_output').innerHTML = "State loaded";
      			GetCurrentState();
      		}
      	
      	function stateList() {
      		
          function parseStateList(response){
          	var stateTable= "<table><tr><td style='width: 900px; color: blue;'>Click on state to load</td></tr>";
            console.log(response);
            statelist=response["States"];
            console.log(statelist);
            for (var i in statelist) {
            	a = statelist[i];
            	var index = Object.keys(statelist).indexOf(i);
            	console.log("i="+index);
            	console.log(i);
            	stateTable+="<tr><td style='width: 500px;'>";
            	stateTable+="<button id=\""+index+"\" onclick=\"GetSavedState('"+a+"',parseSavedState)\">" + a + "</button>";
            	stateTable+="</td></tr>";
            	//document.getElementById(index).onclick = function(){GetSavedState(a,parseSavedState);};
            }
            stateTable+="</table>";
          	document.getElementById('state_output').innerHTML = stateTable;
          	}
          
          	GetStateList(parseStateList);
        	}
</script>
</head>
<body> <br>
	<?php $pagetitle="Administration"; include("header.php"); ?>
	<div class="CONTENT">
		<div id="pagecontent" class="administration">
			<div id="adminpage_div">
				<p>Hello <?php  echo $_SERVER["ADFS_FIRSTNAME"]; ?>, this is the administration page.</p>
				<ul>
					<li><a href="#add_admin">Administrators list</a></li>
					<li><a href="#documentation">Documentation</a></li>
					<li><a href="#panels">Simulator pages layouts</a></li>
					<li><a href="#photos">Photo Gallery</a></li>
				</ul>
				<?php
					include 'isAdmin.php';
					$isAdminClass= new isAdmin;
		        ?>
				<hr>
				
				
			    <div id="state_control">
			    	<h3>Session</h3>
			    	To save this session current state: (name) <input id="statename" type="text">
			    	<button id="scs" onclick="stateSave()">Save</button>
			    	<br>
			    	
				    <br>
				    <button id="gsl" onclick="stateList()">List saved states</button><br>
				    <div id="state_output"></div>
				    <div class="desktop" id="tokenid">Tokenid: <span class="tokenid"> </span><button id="resetTokenId" onclick="resetTokenId();">Reset</button>
				</div>  
				<hr>
				
				
				
				<?php if($isAdminClass->isAdmin()){ ?>
				<div id="add_admin">
			        <h3>Administrators</h3>
			        <p>Too add a new administrator insert a valid CERN user login. Name will be added at first login</p>
			        <br/>
					<div id="addAdmin_div">        
						<label>Cern login: </label><input type="text" size="6" id="newAdminLogin"/>
						<!--<label>Name: </label><input type="text" size="15" id="newAdminName"/><br/>-->
						<button id="addAdmin_button" type="button" onClick="addAdmin(this.value)">Add</button>
					</div>
				</div>
				<?php } else{ ?>
				<p>You are not administrator. To ask for administrators rights you can contact one of the following</p>
				<?php }	?>
				
			    <div id="administrators_center">
			      <div id="administratorspage_div">
						   <span id="administrators_list">
				         <table id="administrators_table" class="tablesorter" >
				           <thead>
				        	 <th>Login</th>
				        	 <th>Full Name</th>
				        	 <th ta-sorter="false" data-filter="false">Action</th>
				        	 </thead>
				        	 <tbody id="administrators_tbody"></tbody>
				         </table>
				       </span>
				     </div>
			    </div>
				<hr>
				<!-- 
				<h3 id="agenda">DSS Agenda</h3>
				<div id="agenda_center">
					<div id="agendapage_div">
						<span id="agenda_list">
							<table id="agenda_table" class="tablesorter">
								<thead>
									<th>Filename</th>
									<th>Size (KB)</th>
									<th>Last modified</th>
								</thead>
							  	<tbody id="agenda_tbody">
									<?
									$dir="../docs/agenda/";
									$files = scandir($dir);
									foreach ($files as $file){
									  if (substr($file, 0, 1) == ".") continue; // don't list hidden files
									  if (substr($file, 0, 7) == "panels") continue; // don't list panels folder
									  ?>
									  <tr>
									  <td>
									    <?
									    $ext=pathinfo($file, PATHINFO_EXTENSION);
									    if(in_array($ext,array('png','jpg'))){
									      ?><a href="<?=$_SERVER['PHP_SELF']."?display=".$dir.$file;?>"><?=$file;?></a><?
									    }if(is_dir($dir.$file)){
									      $link=$dir.$file;
									      if($file==".."){$link=substr($dir,0,strrpos($dir, "/",-2));}
									      ?><a href="<?=$_SERVER['PHP_SELF']."?dir=".$link;?>"><?=$file;?></a><?
									    }else{
									      ?><a href="<?=$dir.$file;?>"><?=$file;?></a><?
									    }
									    ?>
									  </td>
									  <td>
									  <?=round(filesize($dir.$file)/1024);?>
									  </td>
									  <td>
									  <?=date("l, dS F, Y @ h:ia", filemtime($dir.$file));?>
									  </td>
									  </tr>
									  <?php } ?>
								</tbody>
							</table>
						</span>
					</div>
				</div>
				
				 -->
				<h3 id="documentation">Documentation</h3>
				<div id="documentation_center">
					<div id="documentationpage_div">
						<span id="documentation_list">
							<table id="documentation_table" class="tablesorter">
								<thead>
									<th>Filename</th>
									<th>Size (KB)</th>
									<th>Last modified</th>
								</thead>
							  	<tbody id="documentation_tbody">
									<?
									$dir="../docs/";
									$files = scandir($dir);
									foreach ($files as $file){
									  if (substr($file, 0, 1) == ".") continue; // don't list hidden files
									  if (substr($file, 0, 7) == "panels") continue; // don't list panels folder
									  ?>
									  <tr>
									  <td>
									    <?
									    $ext=pathinfo($file, PATHINFO_EXTENSION);
									    if(in_array($ext,array('png','jpg'))){
									      ?><a href="<?=$_SERVER['PHP_SELF']."?display=".$dir.$file;?>"><?=$file;?></a><?
									    }if(is_dir($dir.$file)){
									      $link=$dir.$file;
									      if($file==".."){$link=substr($dir,0,strrpos($dir, "/",-2));}
									      ?><a href="<?=$_SERVER['PHP_SELF']."?dir=".$link;?>"><?=$file;?></a><?
									    }else{
									      ?><a href="<?=$dir.$file;?>"><?=$file;?></a><?
									    }
									    ?>
									  </td>
									  <td>
									  <?=round(filesize($dir.$file)/1024);?>
									  </td>
									  <td>
									  <?=date("l, dS F, Y @ h:ia", filemtime($dir.$file));?>
									  </td>
									  </tr>
									  <?php } ?>
								</tbody>
							</table>
						</span>
					</div>
				</div>
				<hr>
				<?php if($isAdminClass->isAdmin()){ //Panels avaiable only for administrators  ?>
				<h3 id="panels">Panels</h3>
				<ul>
					<li>ONLY PPT files</li>
					<li>PPT file and description image file must have the same name</li>
				</ul>
				
				<div id="panels_center">
					<div id="panelspage_div">
						<span id="panels_list">
							<table id="panels_table" class="tablesorter">
								<thead>
									<th>Filename</th>
									<th>Size (KB)</th>
									<th>Last modified</th>
								</thead>
							  	<tbody id="panels_tbody">
									<?
									$dir="../docs/panels/";
									$files = scandir($dir);
									// loop through the array of files and print them all
									foreach ($files as $file){
									  //if($file==".."&$dir!="./"){/*good to go*/}
									  //else 
									  if (substr($file, 0, 1) == ".") continue; // don't list hidden files
									  ?>
									  <tr>
									  <td>
									    <?
									    $ext=pathinfo($file, PATHINFO_EXTENSION);
									    if(in_array($ext,array('png','jpg'))){
									      ?>
									      <a href="<?=$_SERVER['PHP_SELF']."?display=".$dir.$file;?>">
									      <?=$file;?>
									      </a>
									      <?
									    }if(is_dir($dir.$file)){
									      $link=$dir.$file;
									      if($file==".."){$link=substr($dir,0,strrpos($dir, "/",-2));}
									      ?>
									      <a href="<?=$_SERVER['PHP_SELF']."?dir=".$link;?>">
									      <?=$file;?>
									      </a>
									      <?
									    }else{
									      ?>
									      <a href="<?=$dir.$file;?>"><?=$file;?></a>
									      <?
									    }
									    ?>
									  </td>
									  <td>
									  <?=round(filesize($dir.$file)/1024);?>
									  </td>
									  <td>
									  <?=date("l, dS F, Y @ h:ia", filemtime($dir.$file));?>
									  </td>
									  </tr>
									  <?php } ?>
								</tbody>
							</table>
						</span>
					</div>
				</div>
				<?php }	?>
				<hr>
				<div id="photos_wrapper">
					<h3 id="photos">Photo Gallery</h3>
					<button id="load_photos_button" onClick="load_photos()">Load</button> 
					<div id="photos_container"></div>
				</div>
				<hr>
				
				
			</div>
		</div>
	</div>
	<div class="footer">
		<?php include("footer.php"); ?>
	</div>
</body>
</html>
