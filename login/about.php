<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("About");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$pagetitle;?> - ATLAS Expert System</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 

<?php include ("favicon.php");?>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/simulatorParser.js"></script>
<?php $scripter->includeScripts(); ?> 
<?php $styler->includeStyle(); ?>

</head>
<body>
	<?php include("header.php"); ?>
	<div class="CONTENT about">
		<h2>A quick guide to the ATLAS TC Expert System</h2>
		<div id="index">
			<ul>
				<li><a href="#introduction">Introduction</a></li>
				<li><a href="#parts">Types of pages</a></li>
				<li><a href="#user_interface">User interface</a></li>
				<li><a href="#simulator_pages">Simulator pages</a></li>
				<li><a href="#search_page">Search page</a></li>
				<li><a href="#simulations">How to do a simulation</a></li>
				<li><a href="#thedashboard">Dashboard</a></li>
				<li><a href="#request">How to send a requests to DSS</a></li>
				<li><a href="#thealarmhelper">How to use the Alarm Helper</a></li>
				<li><a href="#links">Useful links</a></li>
			</ul>
		</div>
	 	<div >
	 		<h3 id="intro">Introduction</h3>
		 	The ATLAS TC Expert System is a diagnostic tool to increase the knowledge base of the experiment
		 	<ul>
				<li>Includes gas systems, cooling and ventilation, electricity distribution and Detector Safety System elements</li>
				<li>Document the behavior and interaction of different components</li>
				<li>Allow easier turn over of knowledge between experts</li>
			</ul>
		</div>
		
		<div>
			<h3 id="parts">Types of pages</h3>
			There are three types of pages
			<ul>
				<li>Simulator: Interact with systems to evaluate consequences</li>
				<li>Search: Find a detailed description of every object in the database </li>
				<li>Overview: Dashboard shows an overview of all systems</li>
				<li>Administration:Documentation, DSS Agenda, Photo gallery</li>
			</ul>
		</div>
		
		<div>
			<h3 id="user_interface">User interface tips</h3>
			<ul>
				<li>
					<div>Upper bar</div>
					<div>It is the first element to work with the expert system. It has the following tools from right to left:</div>
					<ul>
						<li><strong>Menu</strong> button, to unfold the menu on the left</li>
						<li><strong>Page</strong> name</li>
						<li><strong>Dashboard:</strong> Notification numbers of the simulation: Triggered alarms and affected systems.</li>
						<li><strong>Language:</strong> Avaliable languages: English (default), French and Russian</li>
						<li><strong>Reset:</strong> Reset session and go back to initial state in all systems.</li>
						<li><strong>Search box:</strong> Find a system in a new tab</li>
					</ul>
					
				</li>
				
				<li>Menu
					<ul>
						<li><strong>Advanced: </strong>Tree menu ordered by most relevant categories</li>
						<li><strong>List: </strong>List of all pages</li>
					</ul>
				</li>
			</ul>
		</div>
		<div>
			<h3 id="simulator_pages">Simulator pages</h3>
			<ul>
				<li><div>Boxes</div>
					<div>Simulator pages are composed mainly of boxes (systems) and lines (relations). 
					There are many types of objects: electrical switchboards, gas systems, power supplies, water systems...</div> 
					<div>Box and border colors give information about the type of system and how it is powered. 
					Orange border indicates it is UPS powered. Black border means normal power.</div> 
					<div class="about container">
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/obj_sys.png" width="120px" />
					            <figcaption>Switchboard under UPS</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/obj_rack.png" width="120px" />
					            <figcaption>Rack under UPS</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/obj_water.png" width="120px" />
					            <figcaption>Water system</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/obj_gas.png" width="120px" />
					            <figcaption>Gas sytem</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/obj_pompier.png" width="120px" />
					            <figcaption>Coffret pompiers</figcaption>
		        			</figure>
						</span>
					</div>
					A brief description of the system (if any) is displayed when the cursor is placed over the object
				</li>
				<li>
					<div>Icons</div>
					Most of the systems have tree buttons, usually a switch, state and info. Although other types of objects have differents like groups.  
					The easiest way to simulate events is using the switch button of systems, interlocks and alarms. 
					<div class="about container">
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/system_buttons.png" width="120px" />
					            <figcaption>System</figcaption>
		        			</figure>
						</span>
						<a class="about">
							<figure class="about">
					            <img class="small" src="img/about/group_buttons.png" width="120px" />
					            <figcaption>Group</figcaption>
		        			</figure>
						</span>
					</div>
					<ul>
						<li><strong>Switch</strong>: Can be switched OFF/ON by the user.[Red/Green]</li>
						<li><strong>State</strong>: Represents the state of the system in the simulation.[Red/Green]</li>
						<li><strong>Info</strong>: Opens object information in new tab</li>
					</ul>
				</li>
				<li><div>Simulation</div>
					Consequences of the simulation can be followed without leaving the current page in three areas 
					<div class="about container">
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/sim_dashboard_upperbar.png" width="120px" />
					            <figcaption>Dashboard link and notifications</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/sim_verbose_table.png" width="120px" />
					            <figcaption>Table on the right</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/sim_system.png" width="120px" />
					            <figcaption>Each object</figcaption>
		        			</figure>
						</span>
					</div>
				</li>
			</ul>
		</div>
		<div>
			<h3 id="search_page">Search page</h3>
			<ul>
				<li><div>Get to the Search page</div>
					<div>Search page is useful to find detailed descriptions of every object of the Expert System.</div>
					<div>Click the server link in the menu or search for an empty string in the upper bar to search for any object</div>
					<div>Info icon in the boxes will only find the clicked object.</div>
					<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/search_upper_bar.png" width="120px" />
					            <figcaption>Search box in bar</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/search_menu_link.png" width="120px" />
					            <figcaption>Search link in menu</figcaption>
		        			</figure>
						</span>
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/search_info_icon.png" width="120px" />
					            <figcaption>Info icon in box</figcaption>
		        			</figure>
						</span>
				</li>
				<li>
					<div>Search page panels</div>
					<div>There are two main parts: objects table [left] and two description panels[middle and right]</div>
					<div>The description panel on the right can be displayed or collapsed</div>
					<div>Object descriptions display information and relations with other objects. They can be navigated using the two description panels</div>
					<div>Description of objects also shows all the simulator pages where the object is present</div>
					<div class="about container">
						<span href="#" class="about">
							<figure class="about">
					            <img class="small" src="img/about/search_navigation.png" width="720px" />
					            <figcaption>Object navigation</figcaption>
		        			</figure>
						</span>
					</div>
				</li>
			</ul>
		</div>
		<div>
			<h3 id="simulations">How to do a simulation</h3>
			<ul>
				<li>
					<div>Simulation of a DSS alarm</div>
					<div>Search for the alarm in the search page. The table can filter by object type</div>
					<div>Once the alarm is found you can click on it to see the description including the button to trigger it. </div>
					<div class="about container">
						<span href="#" class="about">
						  <figure class="about">
						    <img class="small" src="img/about/simulation_before.png" width="720px" />
						    <figcaption>Rack cooling alarm in USA15 description and trigger button</figcaption>
			     		</figure>
						</span>
					</div>
					<div class="about container">
						<span href="#" class="about">
							<figure class="about">
						    <img class="small" src="img/about/simulation_after.png" width="720px" />
						    <figcaption>Alarm triggered</figcaption>
			        </figure>
						</span>
					</div>
					<div>Differences can be seen between before [up] and after [bottom] triggering the alarm:</div>
					<ul>
						<li>Description panel in the middle: state icons of the actions and the alarm have turned red. They have been triggered.</li>
						<li>Description panel in the right: this panel is describing the first action listed in the alarm. It is related to two alarms. It can be seen that only on of them is triggered</li>
						<li>Upper bar: Notification numbers have change for alarms and actions</li>
					</ul>
					<div>Dashboard page gives detailed report of everything affected. </div>
				</li>
				<li>
					<div>Simulation of switching a system off</div>
					<div>Simulator pages display many objects with their relations. It is possible to search for one object in the "Search page" and then see in the description all the simulator pages that contain the object.</div>
					<div>Once located the object it can be switched OFF/ON from the description panels in the search page and from the simulator pages</div>
					
					<div class="about container">
						<span href="#" class="about">
						  <figure class="about">
						     <img class="small" src="img/about/simulation_sys2.png" width="720px" />
						  <figcaption>EOD2/1DX switched OFF in a simulator page</figcaption>
			        </figure>
						</span>
					</div>
					<div>In this simulator page it seems that only one system is affected but in the uppar bar the notifications icon show that 5 systems are affected. Clicking the notifications go to Dashboard where the full report can be seen. </div>
				</li>
			</ul>
		</div>
		
		<div>
			<h3 id="thedashboard">Dashboard</h3>
			<div>Dashboard provides a detailed report of the simulations:</div>
			<ul>
				<li><strong>Commands:</strong> List of all the commands taken by the user.</li>
				<li><strong>Alarms:</strong> Triggered DSS alarms</li>
				<li><strong>Actions:</strong> Active DSS actuators</li>
				<li><strong>Impact:</strong> List of all impacted elements, grouped by type of system. </li>
			    <div class="about container">
			      <span href="#" class="about">
					    <figure class="about">
			          <img class="small" src="img/about/dashboard_1.png" width="520px" />
			          <figcaption>Dashboard report of AL_INF_Smoke_USA15L2_GasRoom</figcaption>
        		  </figure>
				    </span>
			    </div>
			    <div>The simulation above shows how one command of triggering one alarm (AL_INF_Smoke_USA15L2_GasRoom) has triggered a second alarm and one actuator as consequence. The impact panels shows also 22 impacted elements.</div>
			</ul>
		</div>
		<div>
			<h3 id="request">Requests to DSS</h3>
			<div>This is a tool to formally request inhibit alarms or actions in DSS. </div>
			<div>It is very important to provide the CERN email. Requests are sent by email to requestor and DSS experts when they are first requested, modified, approved, applied/removed in DSS and rejected. </div>
			<div>All fields are mandatory:</div>
			<ul>
				<li>Alarms to be requested: Using the table and the add/remove icon</li>
				<li>Reason: Please be clear in the description about the reasons and intervention</li>
				<li>Dates</li>
			</ul>
			<div>The normal workflow of a request is the following:</div>
			<ol>
				<li>The request is filled and sent. Status: "Pending"</li>
				<li>The request is revised by DSS team and approved. Status: "Approved. Currently not inhibited"</li>
				<li>If the request is not approved DSS team contacts requestor and explain reasons. Status: Declined</li>
				<li>Approved request are applied in due time, changing status to: "Approved. Currently inhibited"</li>
			</ol>
			
			<div class="about container">
				<span href="#" class="about">
					<figure class="about">
			      <img class="small" src="img/about/inhibit_1.png" width="520px" />
			      <figcaption>Example of a request</figcaption>
        	</figure>
				</span>
			</div>
		</div>
    
		<div>
			<h3 id="thealarmhelper">How to use the Alarm Helper</h3>
			<div>The alarm helper is the graphical interface to a database listing all DSS alarms and their cause - grouped into an <i>event</i> after 2018. Each event is categorised in either an <i>Error</i>, <i>Intervention</i> or <i>Unknown</i>. Any number of alarms can be be grouped to a common cause. In addition to a title and a short description, a contact person and links to further information, notably Elisa can be added. Furthermore, the tool can show the occurrences  for any selected combinations of alarms and their causes in a dedicated statistics page.</div>
		
			<div><b>Display alarms and events:</b></div>
			<ol>
				<li>Select the start and end date for which the events shall be displayed. Load by pressing the <i>Reload</i> button on the right.</li>
				<li>Any alarm for which no event has been defined will appear at <i>Uncategorised alarms</i>; all other will be displayed grouped into events under <i>Categorised events</i></li>
			</ol>

			<div class="about container">
				<span href="#" class="about">
					<figure class="about">
						<img class="small" src="img/about/alarm_helper_view.png" width="520px" />
						<figcaption>Display alarms and events</figcaption>
					</figure>
				</span>
			</div>


		<div><b>Creating a new event:</b></div>
		<ol>
			<li>Press the big green plus symbol. A new field will appear where one can edit the title, description etc. Add new alarms from the </i>Uncategorised alarms</i> field by clicking on the small plus next to the alarm.</li>
			<li>Press the checkmark on the right side of the editing window to confirm. Then save your changes to the server by pressing the save icon on the top right of the page.</li>
			<li>To cancel, press the cancel <i>x</i> on the left side of the editing window.</li>			
		</ol>

		<div><b>Editing an existing event</b></div>
		<ol>
			<li>Click on the pencil icon on the right of the event, this will open the event in the edit window</li>
			<li>Alarms can be unlinked by pressing the red minus button on the right of an alarm.</li>
			<li>Press the checkmark on the right side of the editing window to confirm. Then save your changes to the server by pressing the save icon on the top right of the page.</li>
			<li>To cancel, press the cancel <i>x</i> on the left side of the editing window.</li>
		</ol>

		<div class="about container">
			<span href="#" class="about">
				<figure class="about">
					<img class="small" src="img/about/alarm_helper_create.png" width="520px" />
					<figcaption>Creating or editing a new event</figcaption>
				</figure>
			</span>
		</div>
		<div class="about container">
			<span href="#" class="about">
				<figure class="about">
					<img class="small" src="img/about/alarm_helper_create2.png" width="520px" />
					<figcaption>Editing information of an event event</figcaption>
				</figure>
			</span>
		</div>
		<div class="about container">
			<span href="#" class="about">
				<figure class="about">
					<img class="small" src="img/about/alarm_helper_create3.png" width="520px" />
					<figcaption>Adding and removing alarms from an event</figcaption>
				</figure>
			</span>
		</div>
		<div class="about container">
			<span href="#" class="about">
				<figure class="about">
					<img class="small" src="img/about/alarm_helper_save.png" width="520px" />
					<figcaption>Saving an event to the server</figcaption>
				</figure>
			</span>
		</div>

		</div>
    
		<div>
			<h3 id="links">Useful links</h3>
      <ul>
			  <li>Developers e-group: <a href="mailto:atlas-opm-expert-system@cern.ch">atlas-opm-expert-system</a></li>
				<li>JIRA project: <a href="https://its.cern.ch/jira/projects/ATLASTCES">ATCES</a></li>
			  <li>GitLab repository: <a href="https://gitlab.cern.ch/atces/atlas-expert-system">atlas-expert-system</a></li>
		 	</ul>
	 	</div>
	</div>
	<div class="footer">
	<?php include("footer.php"); ?>
	</div>
</body>
</html>
