/* Eva Lott 06/2019
 * A set of general functions for parsing an xml with vector element location 
*/

// Parses the graph xml for vertices and edges, then renders them
// Capible of writing new x,y coordinates to the graph


function grapher (graphName, container)
{
	// Set up graph background from inputted container
	mxEvent.disableContextMenu(container);
	var graph = new mxGraph(container);
	new mxRubberband(graph);
	var parent = graph.getDefaultParent();

	// Load the xml for the given graph
	loadGraph(graphName, graph);

}

function loadGraph (graphName, graph)
{

	// Open the file
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			var vertexStringList = [];
			var edgeList = [];

			// Get the XML
			var xmlDoc = xmlhttp.responseXML;
			var graphXML = xmlDoc.getElementsByTagName(graphName)[0];

			// Populate the vertexStringList with the extracted xml
			var node;
			for(var i = 0; i < graphXML.childNodes.length; i++) {
				node = graphXML.childNodes[i];
				if(node.nodeType !== Node.TEXT_NODE) {
					vertexStringList.push([
						node.getElementsByTagName('systemname')[0].textContent,
						node.getElementsByTagName('xpos')[0].textContent,
						node.getElementsByTagName('ypos')[0].textContent,
						node.getElementsByTagName('xsize')[0].textContent,
						node.getElementsByTagName('ysize')[0].textContent,
					]);
					edgeList.push(node.getElementsByTagName('edge'));
				}
			}

			// Render the graph
			renderGraph(graph, vertexStringList, edgeList);
		}
	};
	
	// ========================= NOTE: ===========================================================
	// Math.random() call is in use for testing just so the xml will be looked up when it's changed. Without it'll just use the cached unchanged one.
	xmlhttp.open('GET', 'data/tempmxgraphs.xml?id="+Math.random(),true', false);
	xmlhttp.setRequestHeader('Content-Type', 'text/xml');
	xmlhttp.send();	
}

function renderGraph(graph, vertexStringList, edgeList)
{
	var vertexList = [];

	// Update the graph
	graph.getModel().beginUpdate();
	try {
		// Loop through every vertex and construct the mxGraph element with the data
		for(let i = 0; i < vertexStringList.length; i++) {
			vertexList.push(
				graph.insertVertex(graph.getDefaultParent(), null,
					vertexStringList[i][0], // systemname
					vertexStringList[i][1], // xpos
					vertexStringList[i][2], // ypos
					vertexStringList[i][3], // xsize
					vertexStringList[i][4]  // ysize
				)
			);
		}

		// Loop through every vertex and construct the mxGraph element with the data
		// Needs to be done after all vertices are present
		for(let i = 0; i < vertexStringList.length; i++) {
			// If the vertex is connected to an edge
			if (edgeList[i][0] != undefined) {
				// Loop through the given edges to render
				for (let j = 0; j < edgeList[i].length; j++) {
					for (let k = 0; k < vertexStringList.length; k++) {
						if (edgeList[i][j].textContent == vertexStringList[k][0]) {
							graph.insertEdge(graph.getDefaultParent(), null, '', vertexList[i], vertexList[k], 'edgeStyle=elbowEdgeStyle;elbow=horizontal;');
						}
					}
				}
			}
		}
	}
	
	finally {
		graph.getModel().endUpdate();
	};


	// The graph isn't editable on render
	graph.setCellsLocked(true);
	graph.setVertexLabelsMovable(false);
	graph.setEdgeLabelsMovable(false);
}
