/*****************************************
 * ATLAS EXPERT SYSTEM Dashboard functions
 * Requires ui.js
 *
 * ignacio.asensi@cern.ch
 * Oct 2017 
 *****************************************/

function loadDashboard(){
	loading("start");
	dashboard_get_commands();
	dashboard_get_impact();
	dashboard_get_alarms();
	dashboard_get_actions();
	dashboard_get_inhibited();
	//dashboard_get_MinimaxDeployed();
  setTimeout(loadDashboardSummary,10000);
}
var dashboarddata={};

/*
it summarizes on the top of the page the simulation taking each panel number of rows
to be triggered after last panel is loaded
*/
function loadDashboardSummary(){
	if (jsdebug.functions == true){console.log("called loadDashboardSummary");}
	function callpanel(GroupsAffected){
		console.log("returned loadDashboardSummary");
		$.ajax({
		url: 'templates/dashboard_panel_summary2.php',
        data: {dashboarddata:dashboarddata, GroupsAffected:GroupsAffected},
        type: 'post',
        success: function(data) {
	       	 $("#dashboard_summary_wrapper").html(data);
	       	 process_table("dashboard_important_elements");
	       	 process_table("dashboard_summary_elements");
	       	 //$("table#dashboard_commands > tbody > tr").hide().slice(0, 3).show();
	        }
		});	
	}
	$( ".dashboard_data" ).each(function( index ) {
	  		if(this.getAttribute("value")!=="0"){
	  			dashboarddata[this.id]=this.getAttribute("value");	
	  		}
	  		
		});
	GetSummaryGroupsAffected(callpanel);

	/*
	if (tables_with_hidden_rows["summary"].length===0){//prevent the function to be called twice. Settimeout bug
		var cont=false;
		$( ".dashboard_data" ).each(function( index ) {
	  		if(this.getAttribute("value")!=="0"){
	  			cont=true;
	  			//console.log(this.id+ ": " + $( this ));
	  			dashboarddata[this.id]=this.getAttribute("value");	
	  		}
	  		
		});
		
		if (cont){
			GetSummaryGroupsAffected(callpanel);
		}else{
			$("#dashboard_level_summary").hide();
			$("#dashboard_summary_caption").hide();
			$("#dashboard_summary_wrapper").hide();
			
		}
	}
	*/
	
}


function dashboard_get_commands(){
	if (jsdebug.functions == true){console.log("called dashboard_get_commands");}
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_commands(json){
		if (json.Reply==="Error"){
			console.log("dashboard_get_commands():Error. Server response error");
			$("#dashboard_commands_wrapper").html("Server error! Commands history could not be retrieved.");
		}else{
			$.ajax({
				url: 'templates/dashboard_panel_commands.php',
		        data: {json:json['commands']},
		        type: 'post',
		        success: function(data) {
			       	 $("#dashboard_commands_wrapper").html(data);
			       	 $("table#dashboard_commands_table > tbody > tr").hide().slice(0, 3).show();
			       	 //$("table#dashboard_commands > tbody > tr").hide().slice(0, 3).show();
			        }
			});
		}
	}
	GetCommandsHistory(parse_dashboard_commands);
}


function dashboard_get_computation(){
  if (jsdebug.functions == true){console.log("called dashboard_get_computation");}
	/**
	 *  parse_dashboard_computation parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_computation(json){
		$.ajax({
		url: 'templates/dashboard_panel_computation_all.php',
		    data: {json:json},
		    type: 'post',
		    success: function(data) {
		      	 $("#dashboard_computation_wrapper").html(data);
		    }
		});
	}
	GetComputationAll(parse_dashboard_computation);
	
}

function dashboard_get_impact(){
	if (jsdebug.functions == true){console.log("called dashboard_get_impact");}
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_affeted_systems(json){
		$.ajax({
		url: 'templates/dashboard_panel_impact.php',
        data: {json:json},
        type: 'post',
        success: function(data) {
	       	 $("#dashboard_impact_wrapper").html(data);
	       	 //setTimeout(loadDashboardSummary,1000);
	       	 //setTimeout(loadDashboardSummary,10000);
	        }
		});
	}
	GetAffectedSystems(parse_dashboard_affeted_systems);
	
}

function dashboard_get_inhibited(){
	if (jsdebug.functions == true){console.log("called dashboard_get_inhibited");}
	/**
	 *  parse_dashboard_affeted_systems parses affected alarms using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_inhibited(json){
		$.ajax({
		url: 'templates/dashboard_panel_inhibited.php',
        data: {json:json},
        type: 'post',
        success: function(data) {
	       	 $("#dashboard_inhibited_wrapper").html(data);
	       	 $("table#dashboard_inhibited > tbody > tr").hide().slice(0, 3).show();
	        }
		});
	}
	GetInhibitedItems(parse_dashboard_inhibited);
	
}

function dashboard_get_MinimaxDeployed(){
	if (jsdebug.functions == true){console.log("called dashboard_get_MinimaxDeployed");}
	/**
	 *  parse_dashboard_affeted_systems parses affected alarms using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_MinimaxDeployed(json){
		if(Object.keys(json["Result"]).length>0){
			$.ajax({
				url: 'templates/dashboard_panel_minimax.php',
		        data: {json:json["Result"]},
		        type: 'post',
		        success: function(data) {
			       	 $("#dashboard_minimax_wrapper").html(data);
			       	 process_table('dashboard_minimax',5);
			       	 $("table#dashboard_minimax > tbody > tr").hide().slice(0, 3).show();
			        }
				});
		}
		
	}
	GetMinimaxDeployed(parse_dashboard_MinimaxDeployed);
	
}




function dashboard_get_alarms(){
	if (jsdebug.functions == true){console.log("called dashboard_get_alarms");}
	/**
	 *  parse_dashboard_affeted_systems parses affected alarms using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_affeted_alarms(json){
		$.ajax({
		url: 'templates/dashboard_panel_alarms.php',
        data: {json:json},
        type: 'post',
        success: function(data) {
	       	 $("#dashboard_alarms_wrapper").html(data);
	       	 $("table#dashboard_alarms > tbody > tr").hide().slice(0, 3).show();
	        }
		});
	}
	GetTriggeredAlarms(parse_dashboard_affeted_alarms);
	
}


function dashboard_get_actions(){
	if (jsdebug.functions == true){console.log("called dashboard_get_actions");}
	function parseActions(json){
		$.ajax({
			url: 'templates/dashboard_panel_actions.php',
	        
			data: {json:json}, //test
	        type: 'post',
	        success: function(data) {
		       	$("#dashboard_actions_wrapper").html(data);
		       	$("table#dashboard_actions > tbody > tr").hide().slice(0, 2).show();
		       },
			});	
	}
	
	GetActiveActions(parseActions);
}

function resetCommandsHistory(){
	function parse(json){
		//console.log(json);
	};
	GetCommandsHistoryCleared(parse);
}


function toogleTablesDashboardMultiple(fieldsetname){
	loading("start");
	if ($('#dashboard_show_'+fieldsetname).text()==="Show all"){
		for(var i = 0; i < tables_with_hidden_rows[fieldsetname].length; i++) {
			toogleTableRows(tables_with_hidden_rows[fieldsetname][i], "show");
		}
		$('#dashboard_show_'+fieldsetname).text("Show less");
	}else{
		for(var i = 0; i < tables_with_hidden_rows[fieldsetname].length; i++) {
			toogleTableRows(tables_with_hidden_rows[fieldsetname][i], "hide");
		}
		$('#dashboard_show_'+fieldsetname).text("Show all");
	}
}



function toogleTablesDashboardAlarms(){
	loading("start");
	if ($('#dashboard_show_alarms').text()==="Show all"){
		toogleTableRows("dashboard_alarms_table", "show");
		$('#dashboard_show_alarms').text("Show less");
	}else{
		toogleTableRows("dashboard_alarms_table", "hide");
		$('#dashboard_show_alarms').text("Show all");
	}
}

function toogleTablesDashboardActions(){
	loading("start");
	if ($('#dashboard_show_actions').text()==="Show all"){
		toogleTableRows("dashboard_actions_table", "show");
		$('#dashboard_show_actions').text("Show less");
	}else{
		toogleTableRows("dashboard_actions_table", "hide");
		$('#dashboard_show_actions').text("Show all");
	}
}

function toogleTablesDashboardComputation(){
  console.log("toggleTablesDashboardComputation")
	if ($('#dashboard_show_computation').text()==="Load"){
  	loading("start"); //stop is called by the template
    dashboard_get_computation();
		$('#dashboard_show_computation').text("Hide");
  }else if ($('#dashboard_show_computation').text()==="Hide"){
    $("#dashboard_computation_table").hide();
		$('#dashboard_show_computation').text("Show");
  }else if ($('#dashboard_show_computation').text()==="Show"){
    $("#dashboard_computation_table").show();
		$('#dashboard_show_computation').text("Hide");
	}
	
}



