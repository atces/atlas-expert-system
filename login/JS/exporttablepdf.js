/* Eva Lott 06/2019
 * Uses the jspdf and jspdf-autotable libraries to export intervened and affected tables to pdf */

function exporttablepdf (table1, table2) {
	var copy1 = document.getElementById(table1).cloneNode(true);
	var copy2 = document.getElementById(table2).cloneNode(true);

	copy1.deleteRow(1);
	copy2.deleteRow(1);

	window.jsPDF = window.jspdf.jsPDF;

	var pdf = new window.jsPDF();

	pdf.text(7,10,"Intervened Systems");

	pdf.autoTable({
		startY: 15,
		styles: {
		},
		html: copy1,
	});

	pdf.text("Affected Systems",7,pdf.autoTable.previous.finalY + 10);

	pdf.autoTable({
		startY: pdf.autoTable.previous.finalY + 15,
		styles: {
		},
		html: copy2,
	});

	pdf.save('pdfoutput.pdf');
}
