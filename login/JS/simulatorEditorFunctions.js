/**
 * Ignacio.Asensi@cern.ch 
 * October 2016
 * Functions for the UI simulator editor
 */




var box=new Object();
var boxes=new Object();



/*menu buttons*/
function menuAdd(){
	paintToolbox();
	paintPreview();
	box.power=new Object();
	box.links=new Object();
	$('.edit_box_button').hide();//only edit one at a time
	
}


function menuRemoveAll(){
	box=new Object();
	boxes=new Object();
	$('.box').each(function(){
		$(this).remove();
	});
	
}

function menuSave(){
	var array_json = JSON.stringify(boxes);
	$.ajax({
	    type: 'POST',
	    url: 'simulatorFunctions.php',
	    data: {cmd:'save',json: array_json},
	    success:function(response){
	    	console.log(response);
	    },
	    error:function(request, status, error){
	    	console.log(request.responseText);
	    }
	});
}

function menuLoad(){
	$.ajax({
	    type: 'POST',
	    url: 'simulatorFunctions.php',
	    data: {cmd:'load'},
	    success:function(json_object){
	    	boxes=JSON.parse(json_object);
	    	for(item in boxes){
	    		createBoxFromObject(boxes[item]);
	    	}
	    },
	    error:function(request, status, error){
	    	console.log(request.responseText);
	    }
	});
}



function menuLoadOthers(){
	//cargar un frame con la lista de objectos
	$.ajax({
	    type: 'POST',
	    url: 'simulatorFunctions.php',
	    data: {cmd:'getOthers'},
	    success:function(json_object){
	    	$('#loadOthers_wrapper').show();
	    	var others=JSON.parse(json_object);
	    	var text="";
			$.each(others, function(i,obj){
				var name=obj.split("-");
				text+="<tr><td>"+name[0]+"</td></tr>";
				
			});
			$('#loadOthers_tbody').html(text);
	    },
	    error:function(request, status, error){
	    	console.log(request.responseText);
	    }
	});
	
	
}




/*toolbox actions*/
function toolboxSave(){
	function removeEditingTools(){
		$('#toolbox_div').hide();
		//$('#current_previewbox_div').hide();//probando
	}

	if(typeof boxes[box.name]==='undefined'){
		updatePreview();
		boxes[box.name]=box;//add to array
		createBoxFromObject(box);//create ui element from object
		
		$('.edit_box_button').show();//display edit buttons
		//$('#current_previewbox_div').remove();
		resetPreview();
		removeEditingTools();//remove editing tools
		menuDraw();//call to canvas
		box=null;
		box=new Object();
	}else{
		$('#error_messages').text("The object already exists. Change name").fadeOut(10000);	
	}
}

function toolboxDelete(){
	//TODO
}




/*Get templates functions*/
function paintToolbox(){
	//types
	function parseTypes(classes_types){
		var text="";
		text+="<option name=\"\"></option>";
		$.each(classes_types["Classes"], function(i,obj){
			text+="<option name=\""+obj+"\">"+obj+"</option>";
		});
		$('#toolbox_type_select').html(text);
	}
	
	function parseLinks(){
		var text="";
		$.each(boxes, function(i){
			text+="<tr id=\"LINK-ITEM-"+i+"\"><td>"+i+"</td><td><button onClick=addLink(\""+i+"\")>+</button></td></tr>";
		});
		$('#toolbox_links_tbody').html(text);
	}
	//links
	
	
	
	GetClasses(parseTypes);
	parseLinks();
	$('#name_toolbox').val("");
	$('#power_toolbox').val("");
	$('#toolbox_div').show();
}



/*add en remove powers*/

function typeChange(){
	console.log("Getting types");
	function clearPowers(){
		
		$('#toolbox_powers').hide();		
	}
	function parsePowers(objects){
		$('#toolbox_powers').show();
		actionshtml="";
		$.each(objects["Result"], function(i,obj){
			actionshtml+="<tr id=\"POWER-ITEM-"+obj+"\"><td>"+obj+"</td><td><button onClick=addPower(\""+obj+"\")>+</button></td></tr>";
			
		});
		$('#toolbox_delayedactions_tbody').html(actionshtml);
		selected_items_helper();
	}
	function callObjectsFromType(type_selected){
		var call="";
		switch(type_selected){
			case "sys": call="sys";break;
			case "alarm": call="DelayedAction";break;
			case "DelayedAction": call="Action";break;
			case "Action": call="Action";break;
			default: break;
		}
		
		GetAllObjects(call,parsePowers);
	}
	console.log("Getting types");
	var text=$('#toolbox_type_select option:selected').text();
	(text=="")?clearPowers():callObjectsFromType(text);// get powers table
}

//powers
function addPower(item){
	if(!box.power.hasOwnProperty(item)){
		$('#toolbox_selected_powers_tbody').append("<tr id=\"POWER-SELECTED-"+item+"\" ><td>"+item+"</td><td><button onclick=removePower(\""+item+"\")>-</button>");
		selected_items_helper();
		updatePreview();
	}else{
		$('#error_messages').text("This item is already selected").fadeIn(200);
		$('#error_messages').text("This item is already selected").fadeOut(2000);
		
	}
}


function removePower(item){
	$('#POWER-SELECTED-'+item).remove();
	$('#POWER-ITEM-'+item).show();
	updatePreview();
}



//links
function addLink(item){
	$('#toolbox_selected_links_tbody').append("<tr id=\"LINK-SELECTED-"+item+"\" ><td>"+item+"</td><td><button onclick=removeLink(\""+item+"\")>-</button>");
	selected_items_helper();
	updatePreview();
}

function removeLink(item){
	$('#LINK-SELECTED-'+item).remove();
	$('#LINK-ITEM-'+item).show();
	updatePreview();
}


/*
 * selected_items_helper
 * removes selected items form the bellow table
 * */
function selected_items_helper(){
	$('#toolbox_powers_selected > tbody > tr').each(
		function(){
			$this=$(this);
			$('#POWER-ITEM-'+$this.context.id.slice(15)).hide();
		}
		
	) 
	$('#toolbox_links_selected > tbody > tr').each(
		function(){
			$this=$(this);
			$('#LINK-ITEM-'+$this.context.id.slice(14)).hide();
		}
		
	) 
}




function toogle_size(){
	if($('#toogle_size').text()===">"){
		$('#toolbox_div').addClass('big');
		$('#toolbox_selected_powers_wrapper').show();
		$('#toolbox_list_powers_wrapper').show();
		$('#toolbox_list_links_wrapper').show();
		$('#toolbox_number_powers').val($('#toolbox_selected_powers_wrapper tr').length);
		$('#toolbox_number_links').val($('#toolbox_selected_links_wrapper tr').length);
		$('#toolbox_number_powers').hide();
		$('#toolbox_number_links').hide();
		$('#toogle_size').text("<");
	}else{
		$('#toolbox_div').removeClass('big');
		$('#toolbox_selected_powers_wrapper').hide();
		$('#toolbox_selected_links_wrapper').hide();
		$('#toolbox_list_powers_wrapper').hide();
		$('#toolbox_list_links_wrapper').hide();
		$('#toolbox_number_powers').val($('#toolbox_selected_powers_wrapper tr').length);
		$('#toolbox_number_links').val($('#toolbox_selected_links_wrapper tr').length);
		$('#toolbox_number_powers').show();
		$('#toolbox_number_links').show();
		$('#toogle_size').text(">");
	}
}
function paintPreview(){
	if ( $( "#current_previewbox_div" ).length ) {
		var div=$( "#current_previewbox_div" );
		div.css({
		top:box.top, 
		left: box.left, 
		height:box.height,
		width:box.width,
		'background-color':'white'}
		).show();
		clearPreview();
		
	}else{
		$.ajax({ url: 'templates/previewbox.php',
	         data: {},
	         type: 'post',
	         success: function(output) {
	                      $('#simulator_div').append(output);
	                      addListenersPreview();//calling drawable functions after loading html code
	                  		var div=$( "#current_previewbox_div" );
							div.css({
							top:82, 
							left: 7, 
							height:65,
							width:90,
							'background-color':'white'}
							).show();
							clearPreview();
	                  }
		});
	}
}
function resetPreview(){
	var div=$( "#current_previewbox_div" );
							div.css({
							top:82, 
							left: 7, 
							height:65,
							width:90,
							'background-color':'white'}
							).hide();
							clearPreview();
}



/*Generate preview functions*/

function updatePreview(){
	box.name=$('#name_toolbox').val();
	box.type=$('#toolbox_type_select option:selected').text();
	box.color=$('#color_toolbox').val().substring(0,1)=="#"?$('#color_toolbox').val():"#"+$('#color_toolbox').val();
	
	box.power=new Object();
	box.links=new Object();
	$('#toolbox_powers_selected tr').each(function(){
		$this = $(this);
		if(!(box.power.hasOwnProperty($this.context.id.slice(15)))){
			box.power[$this.context.id.slice(15)]=$this.context.id.slice(15);
		}
		
	});
	$('#toolbox_links_selected tr').each(function(){
		$this = $(this);
		if(!(box.links.hasOwnProperty($this.context.id.slice(14)))){
			box.links[$this.context.id.slice(14)]=$this.context.id.slice(14);
		}
		
	});
	
	fillPreview(box);
	updatePreviewSizePosition();
	
}
function fillPreview(info){
	$('#current_preview_div1').text(info.name);
	var text="";
	$.each(box.power, function(key, value){
		text+=value+"<br>";
	});
	$('#current_preview_div2').html(text);
	$('#current_previewbox_div').css({'background-color':info.color});
}

function clearPreview(){
	$('#current_preview_div1').text("");
	$('#current_preview_div2').text("");
	$('#current_previewbox_div').css({'background-color':'white'});
}

function updatePreviewSizePosition(){
	var pb=$('#current_previewbox_div');
	box.height=pb.height();
	box.width = pb.width();
	box.top = pb.position().top;
	box.left = pb.position().left;
	$('#current_preview_position').text(JSON.stringify(box));
	
}
/*
 * top, left, height, width, type, name, power
 * */
function createBoxFromObject(pobject){
	var text="<div id=\""+pobject.name+"\" class=\"previewbox_div\">";
	text+="<p>"+pobject.type+"</p>";
	text+="<div class=\"drag_bar\">Move me</div>";
	//text+="<div class=\"edit_box_button_div\"><input type=\"image\" src=\"img/edit.png\" onClick='editBox(\""+pobject.name+"\")' class=\"edit_box_button\" /></div>";
	text+="<div class=\"edit_box_button_div\"><img src=\"img/edit.png\" onClick='editBox(\""+pobject.name+"\")' class=\"edit_box_button\" /></div>";
	text+="<p><b>"+pobject.name +"</b></p>";
	
	$.each(pobject.power, function(key, value){
		text+="<p>"+value+"</p>";
	});
	text+="</div>";
	$('#simulator_div').append(text);
	$('#'+pobject.name).css({
		top: pobject.top, 
		left: pobject.left, 
		position:'absolute', 
		height:pobject.height,
		width:pobject.width,
		'background-color':pobject.color}
		).attr('class', 'box');
}

function createBoxFromDBObject(pobject){
	var text="<div id=\""+pobject.name+"\" class=\"box\" style=\"top:"+pobject.top+"px;left: "+pobject.left+"px;position: absolute;height: 50px; width: "+pobject.width+"px; background-color:"+pobject.color+"\">";
	text+="<p>"+pobject.type+"</p>";
	
	//text+="<div class=\"edit_box_button_div\"><input type=\"image\" src=\"img/edit.png\" onClick='editBox(\""+pobject.name+"\")' class=\"edit_box_button\" /></div>";
	text+="<div class=\"edit_box_button_div\"><img src=\"img/edit.png\" onClick='editBox(\""+pobject.name+"\")' class=\"edit_box_button\" /></div>";
	text+="<p>"+pobject.name +"</p>";
	/*
	if(pobject.hasOwnProperty("powers")){
		$.each(pobject.powers, function(key, value){
		text+="<p>"+value+"</p>";
		//get this other sys (from db, this is just the name) and paint it linked to this
		});
	}
	*/
	text+="<table class=\"leds\">";
	text+="<tr>";
	text+="<td><div class=\"switch "+pobject.sysswitch+"\"></div></td>";
	text+="<td><div class=\"state "+pobject.sysstate+"\"></div></td>";
	text+="</tr>";
	text+="</table>";
	text+="</div>";
	$('#simulator_div').append(text);
	
}


function updatePowersCounter(){
	$('#toolbox_selected_powers_wrapper tr').length;
}

function editBox(item){
	
	$('.edit_box_button').hide();
	box=null;
	
	box=boxes[item];
	delete boxes[item];
	paintPreview();
	//show toolbox and fill it
	
	$('#toolbox_div').show();
	$('#name_toolbox').val(box.name);
	
	$('#toolbox_type_select option:eq('+ box.type +')').prop('selected', true);
	typeChange();//get type
	
	$('#color_toolbox').val(box.color);



	$('#current_previewbox_div').show();//probando
	
	$('#'+box.name).remove();
	fillPreview(box);	
	updatePreviewSizePosition();
	
	
	
}







