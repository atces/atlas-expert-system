var display_notGrouped = new Array();
var display_events = new Object();

var edit_event = new Object();
var edit_uid = "";  

var trash = new Array();

var displayedSuggestedEvents = 3;
var similar_events = new Array();

// * * * * * 
// upon load
// * * * * *
$().ready(function(){
  var [startDate, endDate] = getDefaultDates();
  $('#start_date').val(startDate);
  $('#end_date').val(endDate);

  updateBarIcons();
  ReadSqlLogs();
});



// * * * * * 
// Displays are defined here
// * * * * *

function _addEntryToEditEvent(entry) {
  console.log("addEntryToEditEvent");
  // Add the entry of the table to edit_event

  // add entry['log'] (string) to edit_event['logs'] (array) if it is not already there
  if (!(edit_event['logs'].includes(entry['log']))) {
    edit_event['logs'].push(entry['log']);
    // add a copy of entry without ['log'] to edit_event['alarms']
    edit_event['alarms'][entry['log']] = JSON.parse(JSON.stringify(entry)); 

    // insert logaction, logaction_names, logaction_times, not_triggered to edit_event with key
    edit_event['logactions'][entry['log']]       = entry['logactions'];
    edit_event['logactions_names'][entry['log']] = entry['logactions_names'];
    edit_event['logactions_times'][entry['log']] = entry['logactions_times'];
    edit_event['not_triggered'][entry['log']]    = entry['not_triggered'];

    // remove entry from display_notGrouped
    display_notGrouped = display_notGrouped.filter(item => item.log != entry['log']);
  }

  // sort logs by date and time
  edit_event['logs'].sort((a, b) => { return new Date(edit_event['alarms'][a].date + ' ' + edit_event['alarms'][a].time) - new Date(edit_event['alarms'][b].date + ' ' + edit_event['alarms'][b].time); });

  // update date and time of the event
  if (edit_event['logs'].length > 0) {
    edit_event['date'] = edit_event['alarms'][edit_event['logs'][0]].date;
    edit_event['time'] = edit_event['alarms'][edit_event['logs'][0]].time;
    edit_event['uid'] = "event_" + convertToUnixTimestamp(edit_event['date'], edit_event['time']);
  } else {
    edit_event['date'] = "None";
    edit_event['time'] = "None";
    edit_event['uid'] = "log_None_" + edit_event['description'];
  }
  // Update the display of edit_event
  RefreshEditEvent(); // this calls DisplayEditEvent
  DisplayNotGrouped(true);

  SuggestEvent();
}

function _removeEntryFromEditEvent(entry) {
  console.log("removeEntryFromEditEvent");
  // remove entry['log'] (string) from edit_event['logs'] (array)
  
  log = {
    "log"        : entry,
    "name"       : edit_event['alarms'][entry].name,
    "date"       : edit_event['alarms'][entry].date,
    "time"       : edit_event['alarms'][entry].time,
    "came"       : edit_event['alarms'][entry].came,
    "serial"     : edit_event['alarms'][entry].serial,
    "logactions"       : edit_event['logactions'][entry],
    "logactions_names" : edit_event['logactions_names'][entry],
    "logactions_times" : edit_event['logactions_times'][entry],
    "not_triggered"    : edit_event['not_triggered'][entry],
    "not_triggered_times" : edit_event['not_triggered_times'][entry],
  };
  display_notGrouped.push(log);

  // remove entry from edit_event['logs'] and edit_event['alarms']
  edit_event['logs'] = edit_event['logs'].filter(item => item != entry);
  delete edit_event['alarms'][entry];

  // remove logaction, logaction_names, logaction_times, not_triggered from edit_event
  delete edit_event['logactions'][entry];
  delete edit_event['logactions_names'][entry];
  delete edit_event['logactions_times'][entry];
  delete edit_event['not_triggered'][entry];
  delete edit_event['not_triggered_times'][entry];

  // sort logs by date and time is not necessary as the order is preserved
  // update date and time of the event
  if (edit_event['logs'].length > 0) {
    edit_event['date'] = edit_event['alarms'][edit_event['logs'][0]].date;
    edit_event['time'] = edit_event['alarms'][edit_event['logs'][0]].time;
    edit_event['uid'] = String("event_" + convertToUnixTimestamp(edit_event['date'], edit_event['time']));
  } else {
    edit_event['date'] = "None";
    edit_event['time'] = "None";
    edit_event['uid'] = String("log_None_" + edit_event['description']);
  }

  console.log("removeEntryFromEditEvent: updated uid: edit_event uid", edit_event['uid'], "edit_uid", edit_uid, edit_event['uid'] === edit_uid);

  // Update the display of edit_event
  RefreshEditEvent(); // this calls DisplayEditEvent
  DisplayNotGrouped();
  SuggestEvent();
}

function DisplayNotGrouped(displayButtons = false, show_actions = true) {
  // Display display_notGrouped as a table
  console.log("DisplayNotGrouped");

  // reverse sort display_notGrouped by date and time and then alphabetically
  display_notGrouped.sort((a, b) => {
    // Compare by date and time first
    const dateTimeA = new Date(a.date + ' ' + a.time);
    const dateTimeB = new Date(b.date + ' ' + b.time);
  
    if (dateTimeA - dateTimeB !== 0) {
      return dateTimeA - dateTimeB;  // If dates and times are different, sort by them
    }
    
    // If date and time are the same, compare alphabetically by name
    return a.name.localeCompare(b.name);
  });
  
  display_notGrouped.reverse();

  // Only display buttons if a single event has 'isEditable' set to true
  if (!displayButtons) { displayButtons = (Object.keys(edit_event).length !== 0) }

  let html_string = '<table style="font-size:20px;">';

if (display_notGrouped.length === 0) {
    html_string += '<span style="font-size:20px; font-weight:bold;">No unprocessed alarms for selected time range.</span>';
} else {
    let currentDay = "";
    for (let i = 0; i < display_notGrouped.length; i++) {
        const alarm = display_notGrouped[i];
        if (alarm.date !== currentDay) {
            // Display new row for weekday and date
            html_string += '<tr style="background-color:DarkGray; height:25px;"><td colspan="4"><strong>' + GetWeekday(alarm.date) + '</strong> - ' + alarm.date + '</td></tr>';
            currentDay = alarm.date;
        }

        // Display each alarm
        html_string += '<tr>';
        if (displayButtons) {
            html_string += '<td><button class="button-plus-alarmHelper" onclick="_addEntryToEditEvent(' + JSON.stringify(alarm).replace(/"/g, '&quot;') + ')"><img src="../images/alarmHelper/plus.png" alt="add_button"></button></td>';
        } else {
            html_string += '<td></td>';
        }
        html_string += '<td>' + alarm.time + ' - ' + alarm.name + '</td>'


        // html_string += '<div style="flex: 0 0 20px; text-align: right;">'; // Set width to 20px and align to right
        //       html_string += '<button class="button-expandEvent-alarmHelper" onclick="ToggleEventVisibility(\'' + event['uid'] + '\')">';
        //       html_string += '<img id="eventToggleImage-' + event['uid'] +  '" src="../images/alarmHelper/collapse.png" alt="collapse" style="width:20;"></button>';
        //     html_string += '</div>';


        // button to hide the actions
        // html_string += '<td><button class="button-expandEvent-alarmHelper" onclick="ToggleActionsVisibility(\'' + alarm.log + '\')">Hide</button></td>';

        // button linking to stats page
        html_string += '<td><a href="alarmStats.php?alarm='+encodeURIComponent(alarm.name)+'&date='+encodeURIComponent(alarm.date);
        html_string += '" target="_blank" class="link-stats-alarmHelper"><img src="../images/alarmHelper/stats.png" style="width:8%; height:8%"></a></td>';
        html_string += '</tr>';


        // Display actions belonging to the alarm below in a table
        if (show_actions) { 
          html_string += '<tr><td colspan="4">';
          html_string += '<table style="width: 100%; font-size:20px; display: table; border-left-style:solid; border-color:DarkGray; left-margin:2px" id="' + alarm.log + '">';
          for (let j = 0; j < alarm.logactions_names.length; j++) {
              html_string += '<tr style="color: DarkGray;">';
              html_string += '<td></td>';
              html_string += '<td>' + alarm.logactions_times[j] + " - " + alarm.logactions_names[j] + '</td>';
              html_string += '</tr>';
          }
          html_string += '</table>';
          html_string += '</td></tr>';
        }
    } 
}
html_string += '</table>';


  $('#display-notGrouped').html(html_string);
}



function CreateNewEvent(){
  // create a new event and open it in edit mode
  console.log("CreateNewEvent");

  // alert user if there is an open event and ask if they want to save it
  if (Object.keys(edit_event).length !== 0) {
    var r = confirm("Before you create a new event - do you want to save the current event?");
    if (r == true) { SaveEditEvent(); } 
    else { CloseEditEvent(); }
  }

  // set isEditable to false for all events
  for (let key in display_events) { display_events[key]['isEditable'] = false; }

  DisplayEvents();

  // fill edit_event with an example
  // uid, date and time is created from log
  let new_edit_event = {};

  new_edit_event['description'] = "New Event Title";
  new_edit_event['short_description'] = "None";
  new_edit_event['event_type'] = "Unknown";
  new_edit_event['date'] = "None";
  new_edit_event['time'] = "None";
  new_edit_event['logs'] = [];
  new_edit_event['alarms'] = {};
  new_edit_event['elisa'] = "No Elisa log."
  new_edit_event["person"] = "Unknown"
  new_edit_event['isEditable'] = true;
  new_edit_event['uid'] = "";

  // actions grouped by log
  new_edit_event['logactions']       = {};
  new_edit_event['logactions_names'] = {}
  new_edit_event['logactions_times'] = {}
  new_edit_event['not_triggered']    = {}
  new_edit_event['not_triggered_times'] = {}

  // assign to edit_event
  edit_event = new_edit_event;
  edit_uid = new_edit_event['uid'];

  DisplayEditEvent();  
}


function DeleteEditEvent(){
  // delete the edit_event from display_events and 
  // ungroup the logs from the edit_event and add them to display_notGrouped
  console.log("DeleteEditEvent");

  // skip if edit_event is empty
  if (Object.keys(edit_event).length === 0) { return; }

  // add a copy to trash
  var timestamp =  Math.floor( new Date().getTime() / 1000);
  var trash_key = String(edit_event['uid'] + "_" + timestamp);
  
  trash.push({trash_key : JSON.parse(JSON.stringify(edit_event))});
  $('#display-trash').html(JSON.stringify(trash));

  // ungroup the logs from the edit_event and add them to display_notGrouped
  for (let log of edit_event['logs']) { display_notGrouped.push(edit_event['alarms'][log]); }

  // delete the edit_event from display_events
  delete display_events[edit_uid];

  // change 'isEditable' to true for all events
  for (let key in display_events) { display_events[key]['isEditable'] = true; }

  // clear edit_event
  edit_event = {};

  DisplayEditEvent();
  DisplayEvents();
  DisplayNotGrouped();
}

function CloseEditEvent(){
  // close the edit_event without saving
  console.log("CloseEditEvent");
  if (Object.keys(edit_event).length === 0) { return; }

  RefreshEditEvent();

  // revert the changes to display_events, display_notGrouped and edit_event that were made when the event was opened
  if (edit_uid in display_events) { 
    // revert the changes to display_events
    var original_event = JSON.parse(JSON.stringify(display_events[edit_uid]));
    display_events[edit_uid] = original_event;

    // ungroup the logs from the edit_event and add them to display_notGrouped
    for (let log of edit_event['logs']) { 
      // see if log is in the original event
      if (!original_event['logs'].includes(log)) {
        // if log is not already in display_notGrouped 
        if (!display_notGrouped.includes(edit_event['alarms'][log])) { display_notGrouped.push(edit_event['alarms'][log]); }
      }
    }
    // remove logs from display_notGrouped that are "still" in the original event
    for (let log of original_event['logs']) { display_notGrouped = display_notGrouped.filter(item => item.log != log);}
  } else if (edit_uid === "") {
    // if edit_uid is not in display_events, add the logs to display_notGrouped from edit_event
    for (let log of edit_event['logs']) { display_notGrouped.push(edit_event['alarms'][log]); }
  }
  // change 'isEditable' to true for all events
  for (let key in display_events) { display_events[key]['isEditable'] = true; }

  // clear edit_event
  edit_event = {};

  similar_events = new Array();

  DisplayNotGrouped();
  DisplayEvents();
  DisplayEditEvent();
}

function RefreshEditEvent(){
  // update all but logs of the edit_event
  // this does not touch the edit_uid or edit_event['uid']
  console.log("RefreshEditEvent");

  var title       = $('#display-editEvent-title').val().trim() || removeNonASCII(edit_event['description']); // TODO: fix this
  var description = $('#display-editEvent-description').val().trim() || removeNonASCII(edit_event['short_description']);
  var eventType   = $('#select-editEvent-eventType').val() || edit_event['event_type'];
  var person      = $('#display-editEvent-person').val().trim() || removeNonASCII(edit_event['person']);
  var elisa       = $('#display-editEvent-elisa').val().trim() || removeNonASCII(edit_event['elisa']);

  edit_event['description'] = title ;
  edit_event['short_description'] = description;
  edit_event['event_type'] = eventType;
  edit_event['person'] = person;
  edit_event['elisa'] = elisa;

  DisplayEditEvent();
}

function SaveEditEvent(){
  // save the edit_event to display_events
  console.log("SaveEditEvent");

  if (Object.keys(edit_event).length === 0) { return; }

  RefreshEditEvent();
  
  // delete the old event from display_events
  delete display_events[edit_uid];

  // insert the event into display_events
  display_events[edit_event['uid']] = JSON.parse(JSON.stringify(edit_event));

  // temporary set edit_uid to the new uid so that saved changes are not reverted in CloseEditEvent
  edit_uid = edit_event['uid'];

  // change 'isEditable' to true for all events
  for (let key in display_events) { display_events[key]['isEditable'] = true; }
  CloseEditEvent();
}


function DisplayEditEvent(){
  console.log("DisplayEditEvent");
  // display edit_event as a table

  // skip if edit_event is empty
  if (!(Object.keys(edit_event).length === 0)) {

    DisplayNotGrouped(true);

    // sort logs by date and time
    edit_event['logs'].sort((a, b) => { return new Date(edit_event['alarms'][a].date + ' ' + edit_event['alarms'][a].time) - new Date(edit_event['alarms'][b].date + ' ' + edit_event['alarms'][b].time); });

    var eventMaxWidth = "600px";
    var eventMinWidth = "600px";

    var eventColor = "DarkGray"
    var eventIcon  = "../images/alarmHelper/unknown_icon.png"
    if (edit_event["event_type"] == "Error") { 
      eventColor = "DarkOrange"; 
      eventIcon  = "../images/alarmHelper/error_icon.png";
    }
    if (edit_event["event_type"] == "Intervention") { 
      eventColor = "DodgerBlue";
      eventIcon  = "../images/alarmHelper/intervention_icon.png"; 
    }

    var _eventTypeOptions = ["Unknown", "Intervention", "Error"];
 
    html_string = "<a>";  
    html_string += '<section style="border:2px solid grey; margin:2px; padding:5px; min-width: 630px; max-width: 630px; min-height: 100px;">';
    html_string += '<div><a><a style="font-size: 20px; font-weight: bold;">Edit</a>';
    html_string += '<button class="button-closeEdit-alarmHelper" type="button" onclick="CloseEditEvent()" title="Cancel"><img src="../images/alarmHelper/close.png" alt="close_button"></button></a>';

    // Statistics: 
    // if called, show Total = Error + Intervention + Unknown in corresponding colors
    html_string += '<div id="suggestedEventBox" style="display: none; flex-direction: column; background-color: lightgrey; padding: 10px; font-size: 20px; border-radius: 5px; gap: 10px;">';

    // a title
    html_string += '<div style="display: flex; justify-content: space-between; width: 100%;">';
      html_string += '<span style="font-weight: bold;">Similar events:</span>';
    html_string += '</div>';

    // Summary data and suggested contacts
    html_string += '<div style="display: flex; justify-content: space-between; width: 100%;">';
      html_string += '<span style="font-weight: bold;">Last year: </span>';
      html_string += '<span id="prevError" style="color: DarkOrange;">Error: <strong>NaN</strong></span>';
      html_string += '<span id="prevIntervention" style="color: DodgerBlue;">Intervention: <strong>NaN</strong></span>';
      html_string += '<span id="prevUnknown" style="color: DarkGray;">Unknown: <strong>NaN</strong></span>';
      html_string += '<span id="prevTotal" >Total: <strong>NaN</strong></span>';
    html_string += '</div>';
    html_string += '<div style="display: flex; width: 100%;"><span id="possibleContacts">Past events:</span></div>';
    html_string += '<div style="display: flex; width: 100%;"><span id="slimosHandbook">Slimos handbook:</span></div>';

    // Suggested events container
    html_string += '<div id="eventList" style="display: flex; flex-direction: column; width: 100%; gap: 5px;"></div>';
    // "Show More" button
    html_string += '<div><button id="showMoreBtn" onclick="ShowSimilarEvents()" style="font-size: 16px;" title="Load more similar events">...</button></div>';

    // Closing div
    html_string += '</div>';

    html_string += '<table style="max-width:' + eventMaxWidth + '; min-width:' + eventMinWidth + '; border-color:' + eventColor + ' ; border-left-style:solid; margin:10px; font-size:20px">';
      html_string += '<tr style="background-color:' + eventColor + '; color:white; padding:5px; height: 50px;">';
        html_string += '<td>';
        html_string += '<div style="display: flex; align-items: center;">'; // Use flexbox for layout

        // Left side (icon)
        html_string += '<div style="flex: 0 0 30px; padding: 0 5px;"><img src="' + eventIcon + '" width="35" alt="icon_button"></div>';

        // Center (title)
        // html_string += '<span id="display-editEvent-title" contenteditable="true" style="font-size: 25px; font-weight: bold ">' + edit_event["description"]+ '</span>';

        html_string += '<input id="display-editEvent-title" value="' + edit_event["description"] + '" ' +
               'onfocus="if(this.value==\'' + (edit_event["description"] || "None") + '\') this.value=\'\'" ' + 
               'onblur="if(this.value.trim() === \'\') this.value = \'' + edit_event["description"] + '\'" ' +  
               'style="border: none; background: transparent; width: auto; outline: none; font-size: 25px; font-weight: bold; color:white; display: inline;">';

        // Right side (icons)
        html_string += '<div style="flex-grow: 1;"></div>'; // Empty div to push buttons to the right
          html_string += '<div><button class="button-saveEdit-alarmHelper" type="button" onclick="SaveEditEvent()" title="Close edit event"><img src="../images/alarmHelper/save.png" alt="save_button"></button></div>';
          html_string += '<div><button class="button-deleteEdit-alarmHelper" type="button" onclick="DeleteEditEvent()" title="Delete event"><img src="../images/alarmHelper/delete.png" alt="delete_button"></button></div>';
        html_string += '</div>';
        
        html_string += '</td>';
      html_string += '</tr>';

      html_string += '<tr><td>';
        html_string += '<span class="custom-select" id="display-editEvent-eventType" display="inline-block">';
          html_string += '<select id="select-editEvent-eventType" onchange="RefreshEditEvent()">';
          for (let _eventType of _eventTypeOptions) {
            var isSelected = (_eventType === edit_event["event_type"]);
            // Add the option, with the 'selected' attribute if it matches the selected value
            html_string += '<option value="' + _eventType + '" ' + (isSelected ? 'selected' : '') + '>' + _eventType + '</option>';
        }
        html_string += '</select></span>';
      html_string += '</td></tr>';     

      html_string += '<tr><td><span>Description: </span>';
      html_string += '<input id="display-editEvent-description" value="' + edit_event["short_description"] + '" onfocus="if(this.value==\'' + edit_event["short_description"]  + '\') this.value=\'\'" ' +
                    'onblur="if(this.value.trim() === \'\') this.value = \'' + edit_event["short_description"] + '\'" ' +  
                    'style="border: none; background: transparent; width: auto; outline: none; display: inline; font-size: inherit;">';
      html_string += '</td></tr>';

      html_string += '<tr><td><span>Person: </span>';
      html_string += '<input id="display-editEvent-person" value="' + edit_event["person"] + '" onfocus="if(this.value==\'' + edit_event["person"] + '\') this.value=\'\'" ' +
                    'onblur="if(this.value.trim() === \'\') this.value = \'' + edit_event["person"] + '\'" ' + 
                    'style="border: none; background: transparent; width: auto; outline: none; display: inline; font-size: inherit;">';
      html_string += '</td></tr>';

      html_string += '<tr><td><span>Elisa: </span>';
      html_string += '<input id="display-editEvent-elisa" value="' + edit_event["elisa"] + '" onfocus="if(this.value==\'' + edit_event["elisa"] + '\') this.value=\'\'" ' +
                    'onblur="if(this.value.trim() === \'\') this.value = \'' + edit_event["elisa"] + '\'" ' +  
                    'style="border: none; background: transparent; width: auto; outline: none; display: inline; font-size: inherit;">';
      html_string += '</td></tr>';

      // Show data and time
      html_string += '<tr><td style="color:DarkGray">' + GetWeekday(edit_event['date']) + "   " + edit_event["time"] + "  " + edit_event["date"] + '</td></tr>';

      // add the alarms
      if (edit_event["logs"].length > 0) {
        html_string += '<tr><td>';
        
        html_string += '<table style="font-size:20px">';
    
        // html_string += '<tr><th></th><th>Time</th><th>Name</th></tr>';
        var associated_logs = edit_event["logs"];
          for (let _log of associated_logs) {
            let _alarm = edit_event['alarms'][_log];
            html_string += '<tr>';
            html_string += "<td><button class=\"button-minus-alarmHelper\" onclick=_removeEntryFromEditEvent('" ;
            html_string += _log + "')><img src='../images/alarmHelper/minus.png' alt='remove_button'></button></td>";
            html_string += '<td>' + _alarm["time"] + '</td><td>' + _alarm["name"] + '</td>';
            html_string += '<td><a href="alarmStats.php?alarm='+encodeURIComponent(_alarm["name"])+'&date='+encodeURIComponent(_alarm["date"]);
            html_string += '" target="_blank" class="link-stats-alarmHelper"><img src="../images/alarmHelper/stats.png" style="width:20%; height:20%"></a></td>';            
            html_string += '</tr>';

            // add the actions
            if (true) {
              html_string += '<tr><td colspan="4">';
              html_string += '<table style="width: 100%; font-size:20px; display: table; border-left-style:solid; border-color:DarkGray; left-margin:2px">';
              
              var action_times = edit_event['logactions_times'][_log];
              var action_names = edit_event['logactions_names'][_log];
              for (let j = 0; j < action_times.length; j++) {
                  html_string += '<tr style="color: DarkGray;">';
                  html_string += '<td></td>';
                  html_string += '<td>' + action_times[j] + " - " + action_names[j] + '</td>';
                  html_string += '</tr>';
              }
              html_string += '</table>';
              html_string += '</td></tr>';
            }
          }
        html_string += '</td></tr></table>';
        } else {
          html_string += '<tr><td style="color:DarkGray; font-style: italic;">Date and time are set from alarms.</td></tr>';
        }

    html_string += '</table>';
    html_string += '</div></section></a>';

    $('#display-editEvent').html(html_string);
    } else {
      $('#display-editEvent').html("");
    }
}

function SelectEditEvent(event_uid){
  // open the event at index eventIndex in edit mode
  console.log("SelectEditEvent");

  // keep the inital uid as this can change
  edit_uid = event_uid;
  
  // move the event to edit_event
  edit_event = JSON.parse(JSON.stringify(display_events[event_uid]));
  // delete the event from display_events

  // change 'isEditable' to false for all other events
  for (let key in display_events) {
    if (key != event_uid) { display_events[key]['isEditable'] = false; }
    else { display_events[key]['isEditable'] = true; }
  }

  DisplayNotGrouped(); 
  DisplayEditEvent();
  DisplayEvents();
}

function SuggestEvent(){
  // suggest an event based on the selected alarms
  console.log("SuggestEvent");

  // get the list of alarms from edit_event['alarms']['name']
  let alarm_list = edit_event['logs'].map(log => edit_event['alarms'][log]['name']);
  if (alarm_list.length === 0) { return; }
  let date = edit_event['date'];
  
  jsonDict = { "alarms" : alarm_list, "date" : date , "nevents" : 15};
  GetAlarmStats(JSON.stringify(jsonDict), function(response) {
    response = JSON.parse(response["Result"]);
    // get stats
    let stats = response["stats"];
    let sum_last_year = stats['last-year']['sum'];
    let error = sum_last_year['E'];
    let interv = sum_last_year['I'];
    let unkn = sum_last_year['U'];
    let tot = sum_last_year['T'];

    document.getElementById('prevError').innerHTML = `Error: <strong>${error}</strong>`;
    document.getElementById('prevIntervention').innerHTML = `Intervention: <strong>${interv}</strong>`;
    document.getElementById('prevUnknown').innerHTML = `Unknown: <strong>${unkn}</strong>`;
    document.getElementById('prevTotal').innerHTML = `Total: <strong>${tot}</strong>`;

    // responsibles
    let responsibles = response["responsibles"];
    document.getElementById('possibleContacts').innerHTML = `Possible contacts: ${responsibles.slice(0, 3).join(", ")}, ...`;    

    // events - take the description and date from response["events"]
    let suggested_events = response["events"];
    console.log("SuggestEvent: suggested_events", suggested_events);

    similar_events = suggested_events
      .sort((a, b) => b.uid.localeCompare(a.uid)) // Sort by 'uid' in descending order
      .map(event => [event['date'], event['description']]); // Create tuples

    const suggestedEventBox = document.getElementById('suggestedEventBox');
    const showMoreBtn = document.getElementById('showMoreBtn');

    // Reset displayedSuggestedEvents to its initial count
    displayedSuggestedEvents = 3;

    // Show the container and "Show More" button
    suggestedEventBox.style.display = 'flex';
    showMoreBtn.style.display = 'flex';

    ShowSimilarEvents();
  });
}



function ShowSimilarEvents() {
  const eventList = document.getElementById('eventList');
  const showMoreBtn = document.getElementById('showMoreBtn');

  // Clear current list to re-render the correct amount of events
  eventList.innerHTML = '';

  // Display the current count of events
  similar_events.slice(0, displayedSuggestedEvents).forEach(event => {
    const eventElement = document.createElement('span');
    eventElement.innerText = `${event[0]} - ${event[1]}`;
    eventList.appendChild(eventElement);
  });
  
  // Hide "Show More" button if all events are shown
  if (displayedSuggestedEvents >= similar_events.length) {
    showMoreBtn.style.display = 'none';
  } else {
    displayedSuggestedEvents += 3;
  }
}



function DisplayEvents(passed_events = null, no_edit = false){
  // passed_events : used in alarmStats.php to pass the events to the display
  // no_edit : used in alarmStats.php to disable the edit buttons

  // display display_events as a table
  console.log("DisplayEvents");

  if (passed_events !== null) { display_events = passed_events;  }

  // reverse sort display_events by uid
  let arrayOfEvents = Object.values(display_events);
  arrayOfEvents.sort((a, b) => parseInt(a.uid.split('_')[1]) - parseInt(b.uid.split('_')[1]));
  arrayOfEvents.reverse();

  let currentDay = "";

  html_string = "<table>";
  arrayOfEvents.forEach(event => {
    let uid = event['uid'];

    if (event['date'] !== currentDay) {
        // Display new row for weekday and date
        html_string += '<tr style="height: 30px; font-size: 20px; text-align: left;"><td><strong>' + GetWeekday(event['date']) + '</strong> - ' + event['date'] + '</td></tr>';
        currentDay = event['date'];
      }

    var eventMaxWidth = "600px";
    var eventMinWidth = "600px";

    var opacity = 1;
    if ( ! event["isEditable"]) { opacity = 0.5; }
    if (no_edit) { opacity = 1; }

    var eventColor = "DarkGray"
    var eventIcon  = "../images/alarmHelper/unknown_icon.png"
    if (event["event_type"] == "Error") { 
      eventColor = "DarkOrange"; 
      eventIcon  = "../images/alarmHelper/error_icon.png";
    }
    if (event["event_type"] == "Intervention") { 
      eventColor = "DodgerBlue";
      eventIcon  = "../images/alarmHelper/intervention_icon.png"; 
    }

    html_string += '<tr><td>';
      html_string += '<table style="font-size:20px; max-width:' + eventMaxWidth + '; min-width:' + eventMinWidth + '; border-color:' + eventColor + ' ; border-left-style:solid; margin:10px; opacity:' + opacity +'">';
      // html_string += '<table style="border-color:' + eventColor + ' ; border-left-style:solid; margin:10px; opacity:' + opacity +'">';
      
      html_string += '<tr style="background-color:' + eventColor + '; color:white; padding:5px; height: 50px;">'; 
        html_string += '<td>';
          html_string += '<div style="display: flex; align-items: center;">'; // Use flexbox for layout
      
            //  Left side (icon)
            html_string += '<div style="flex: 0 0 30px; padding: 0 5px;"><img src="' + eventIcon + '" width="35" alt="icon_button"></div>';
 
            // Center (title)
            html_string += '<span style="flex: 1; font-size: 25px; font-weight:bold">' + event["description"] + '</span>';
            
            // Right side (button)
            html_string += '<div style="flex: 0 0 20px; text-align: right;">'; // Set width to 20px and align to right
            if (!no_edit) {
              if (event["isEditable"]) {
                  html_string += '<button class="button-edit-alarmHelper" onclick="SelectEditEvent(\'' + uid + '\')"><img src="../images/alarmHelper/edit.png" alt="edit_button" title="Edit"></button>';
              } else {
                  html_string += '<button class="button-noEdit-alarmHelper" ><img src="../images/alarmHelper/noedit.png" alt="noEdit_button"></button>';
              }
            }
            html_string += '</div>';

            // Right side (another button)
            html_string += '<div style="flex: 0 0 20px; text-align: right;">'; // Set width to 20px and align to right
              html_string += '<button class="button-expandEvent-alarmHelper" onclick="ToggleEventVisibility(\'' + event['uid'] + '\')">';
              html_string += '<img id="eventToggleImage-' + event['uid'] +  '" src="../images/alarmHelper/collapse.png" alt="collapse" style="width:20;"></button>';
            html_string += '</div>';

          html_string += '</div>'; // Close flex container
        html_string += '</td>';
      html_string += '</tr>';
      
      if (!no_edit) {
        if (event["isEditable"]) {
          html_string += '<tr id="eventToggle-' + event['uid'] + '"><td><table style="display: table-row;">';
        } else {
          html_string += '<tr id="eventToggle-' + event['uid'] + '"><td><table style="display: none;">';
        }
      } else {
        html_string += '<tr id="eventToggle-' + event['uid'] + '"><td><table style="display: table-row;">';
      }
        
        html_string += '<tr><td>';
        html_string += '<div style="display: flex; justify-content: space-between;">'; // Use flexbox for alignment
          html_string += '<div style="text-align: left;"><span>' + event["event_type"] + '</span></div>';
          html_string += '<div style="text-align: center; flex-grow: 1;">'; // Align the second span to the center
            html_string += '<span><img src="../images/alarmHelper/person.png" width="20" height="20">' + event["person"] + '</span>';
          html_string += '</div>';
        html_string += '</div>';
        html_string += '</td></tr>';

        html_string += '<tr><td><span>' + event["short_description"] + '</span></td></tr>';
        html_string += '<tr><td><span><img src="../images/alarmHelper/logbook.png" width="20" height="20">';
        if (event["elisa"].startsWith("https://atlasop.cern.ch/elisa/")) {
          html_string += '<a href="' + event["elisa"] + '" target="_blank">' + event["elisa"] + '</a>';
        } else {
          html_string += event["elisa"];
        }
        html_string += '</span></td></tr>';
        html_string += '<tr><td style="color:DarkGray">' + GetWeekday(event['date']) + "  " + event["time"] + "  " + event["date"] + '</td></tr>';
        
        var associated_logs = event["logs"];
        if (associated_logs.length > 0) {
          // add the alarms
          html_string += '<tr>';
            html_string += '<td><table>';
            // html_string += '<tr><<th>Time</th><th>Name</th></tr>';
            for (let _log of associated_logs) {
                let _alarm = event['alarms'][_log];
                html_string += '<tr><td>' + _alarm["time"] + '</td><td>' + _alarm["name"] + '</td>';
                html_string += '<td><a href="alarmStats.php?alarm='+encodeURIComponent(_alarm["name"])+'&date='+encodeURIComponent(_alarm["date"]);
                html_string += '" target="_blank" class="link-stats-alarmHelper"><img src="../images/alarmHelper/stats.png" style="width:15%; height:15%"></a></td>';
                html_string += '</tr>';

                console.log("DisplayEvents: _log", _log, "event", event);

                // add the actions
                if ('logactions_times' in event) {
                  html_string += '<tr><td colspan="4">';
                  html_string += '<table style="width: 100%; font-size:20px; display: table; border-left-style:solid; border-color:DarkGray; left-margin:2px">';
                
                  var action_times = event['logactions_times'][_log];
                  var action_names = event['logactions_names'][_log];
                  for (let j = 0; j < action_times.length; j++) {
                      html_string += '<tr style="color: DarkGray;">';
                      html_string += '<td></td>';
                      html_string += '<td>' + action_times[j] + " - " + action_names[j] + '</td>';
                      html_string += '</tr>';
                  }
                  html_string += '</table>';
                  html_string += '</td></tr>';
                }
              }
            html_string += '</table></td>';
          html_string += '</tr>';
        }
      html_string += '</table></td></tr>';
      
      html_string += '</table>';
    html_string += '</tr></td>';
  });
  html_string += "</table>"
  $('#display-events').html(html_string);
}


function ToggleEventVisibility(elementId) {
  // toggle the visibility of the event with the given elementId apart from the title
  console.log("ToggleEventVisibility")
  
  var $element = $('#eventToggle-' + elementId);
  if ($element.length === 0) { return; }
  var displayState = $element.css('display');
  var $image = $('#eventToggleImage-' + elementId);  
  
  if (displayState === 'none') {
      $element.css('display', 'table-row');
      $image.attr('src', "../images/alarmHelper/collapse.png");
  } else {
      $element.css('display', 'none');
      $image.attr('src', "../images/alarmHelper/expand.png");
  }
}

function CollapseAll(){
  // collapse all events apart from their title bar
  console.log("CollapseAll");
  for (let key in display_events) { 
    console.log("CollapseAll: key", key);
    ToggleEventVisibility(key); 
  }

  // change the image of the button
  $buttonImage = $('#image-collapseAll-alarmHelper');
  if ($buttonImage.attr('src') === "../images/alarmHelper/collapseAll.png") {
    $buttonImage.attr('src', "../images/alarmHelper/expandAll.png");
  } else {
    $buttonImage.attr('src', "../images/alarmHelper/collapseAll.png");
  }
}

function CollapseActions(){
  // collapse all actions in DisplayNotGrouped
  console.log("CollapseActions");
  
  // change the image of the button
  // and toggle the visibility of the actions
  $buttonImage = $('#image-collapseActions-alarmHelper');
  if ($buttonImage.attr('src') === "../images/alarmHelper/collapseAll.png") {
    $buttonImage.attr('src', "../images/alarmHelper/expandAll.png");
    DisplayNotGrouped(false, false);
  } else {
    $buttonImage.attr('src', "../images/alarmHelper/collapseAll.png");
    DisplayNotGrouped(false, true);
  }
}

function ToggleActionsVisibility(id) {
  var table = document.getElementById(id);
  if (table.style.display === "none") {
      table.style.display = "table";
  } else {
      table.style.display = "none";
  }

}

// * * * * *
// DSS database
// * * * * *

function ReadSqlLogs() {
  // get the alarms from SQL within the given time window and assign to sql_alarms
  console.log("ReadSqlLogs");

  var startDate = $('#start_date').val();
  var endDate = $('#end_date').val();

  if (startDate > endDate) { 
    alert("Start date must be before end date. Reset to default values.");
    [startDate, endDate] = getDefaultDates();
    $(id_start).val(startDate);
    $(id_end).val(endDate);
  }

  $.getJSON('dss/dbfunctions.php?cmd=get_alarms_and_actions_range&since='+startDate+'&until='+endDate, function(data) {
    if (!data.hasOwnProperty('alarms') || !data.hasOwnProperty('actions')) { 
      alert('Failed to get DSS alarms. Invalid JSON content; "alarms" and/or "actions" keys are missing.'); 
      return;
    }
    // this a list of dicts with keys: came, name, time, serial
    sql_alarms  = data['alarms'];
    sql_actions = data['actions'];

    // convert sql_alarms to logs : log_came
    var logs_request = {};
    for (let alarm of sql_alarms) {
      logs_request[`log_${alarm.came}`] = {  ...alarm, log: `log_${alarm.came}` };
    }

    // convert sql_actions to actions : logaction_came
    var logactions_request = {};
    for (let action of sql_actions) {
      logactions_request["logaction_" + action['came']] = { ...action, logaction: `logaction_${action.came}` };
    }

    // First, match the logs and actions
    var jsonDict = { "logs" : logs_request, "logactions" : logactions_request };
    MatchLogsLogactions(JSON.stringify(jsonDict), function(response) {
      if (response['Reply'] !== "OK") {
        alert("Failed to match alarms and actions. Server might be down.");
        return;
      }
      matched_logs = JSON.parse(response['Result']);

      console.log("MatchLogsLogactions: response", response);

      // Second, get what's known already
      var jsonDict = { 'time_range' : [startDate + " 00:00", endDate + " 23:59"], 'logs': matched_logs} ;
      ReadDisplayEventsLogs(JSON.stringify(jsonDict), function(response) {
        if (response['Reply'] !== "OK") {
          alert("Failed to get display events. Server might be down.");
          return;
        }
        response = JSON.parse(response['Result']);

        console.log("ReadDisplayEventsLogs: response", response);

        var display_notGrouped_dict = response['display_notGrouped'];
        display_notGrouped = [];
        for (let key in display_notGrouped_dict) { display_notGrouped.push(display_notGrouped_dict[key]); }

        display_events = response['display_Events'];
        for (let key in display_events) { display_events[key]['isEditable'] = true; }

        DisplayNotGrouped();
        DisplayEvents();
      });
    });
  });
}

function buttonReadSqlLogs() {
  console.log("buttonReadSqlLogs");
  var r = confirm("Unsaved changes will be overwritten if not uploaded. Continue?");
  if (r == false) { return; }
  ReadSqlLogs();
} 
  

// * * * * *
// other useful functions
// * * * * *

function GetWeekday(datestring) {
  var date = new Date(datestring);
  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return days[date.getDay()];
}

function convertToUnixTimestamp(dateString, timeString) {
  // remove the seconds from the timeString
  timeString = timeString.split(":").slice(0, 2).join(":");

  var dateTimeString = dateString + " " + timeString;
  var dateTime = new Date(dateTimeString);
  var unixTimestamp = dateTime.getTime() / 1000; // Convert milliseconds to seconds
  return unixTimestamp.toString();
}

function formatDate(date) {
  // format a date object to a string in the format "YYYY-MM-DD"
  if (date instanceof Date && !isNaN(date)) {
      return date.toISOString().split('T')[0];
  } else {
      console.error("Invalid date object:", date);
      return "";
  }
}
  
function getDefaultDates() {
  // get the current date and the date 7 days ago
  var today = new Date();
  var lastMonday = new Date();
  //if today is sunday or monday, get previous monday
  if(today.getDay()==0 || today.getDay()==1){ 
    lastMonday.setDate(today.getDate()-7);
  }else{
    lastMonday.setDate(today.getDate()-today.getDay());
  }
  return [lastMonday.toISOString().split('T')[0],today.toISOString().split('T')[0]];
}

function buttonDefaultDates() {
  var [startDate, endDate] = getDefaultDates();
  $('#start_date').val(startDate);
  $('#end_date').val(endDate);
}

function buttonWriteDisplayEventsLogs() {
  //  convert display_NotGrouped to a a dict, and then parse it as a json
  var jsonDict = {};

  var display_notGrouped_dict = {};
  for (let log of display_notGrouped) { display_notGrouped_dict[log['log']] = log; }

  jsonDict['display_notGrouped'] = display_notGrouped_dict;
  jsonDict['display_events'] = JSON.parse(JSON.stringify(removeNonASCII(display_events)));
  jsonDict['time_range'] = [$('#start_date').val() + " 00:00" , $('#end_date').val() + " 23:59"];

  // use socketlogic
  WriteDisplayEventsLogs(JSON.stringify(jsonDict), function(response) {
    console.log("buttonWriteDisplayEventsLogs: ", response);
    if (response['Reply'] === "OK") { 
      alert("Successfully uploaded display events."); 
    } else {
      alert("Failed to upload display events. Server might be down.");
    }
  });
}

function buttonDownloadJSON() {
  // JSON data
  var jsonData = { 
    "display_notGrouped": display_notGrouped,
    "display_events": display_events,
    "time_range": [$('#start_date').val() + " 00:00", $('#end_date').val() + " 23:59"]
  };

  // Convert JSON to string
  var jsonString = JSON.stringify(jsonData);

  // Create a blob from the JSON string
  var blob = new Blob([jsonString], { type: "application/json" });

  // Create a temporary anchor element
  var a = document.createElement("a");
  a.style.display = "none";

  // Set the href attribute to the blob URL
  a.href = window.URL.createObjectURL(blob);

  // download file : date_start_date_end.json
  a.download = "date_" + $('#start_date').val() + "_" + $('#end_date').val() + ".json";

  // Append the anchor element to the body
  document.body.appendChild(a);
  // Simulate a click to trigger the download
  a.click();

  // Remove the anchor element
  document.body.removeChild(a);
}

function buttonDownloadSlides(){
  console.log("buttonDownloadSlides");

  var jsonDict = { "display_events": removeNonASCII(display_events),};
  
  CreateSlides(JSON.stringify(jsonDict), function(response) {
    console.log("CreateSlides in alarmHelper.js ", response);
    
    if (response['Reply'] === "OK") { 
        // Get the codimd text
        var coditxt = response["codi"];
        
        // Create a Blob for the TXT file
        var txtBlob = new Blob([coditxt], { type: "text/plain" });
        
        // Create a link element to download the TXT file
        var txtLink = document.createElement('a');
        txtLink.href = URL.createObjectURL(txtBlob);
        
        // Save name should be "DSS_YYYYMMDD.txt" for today
        txtLink.download = `DSS_${new Date().toISOString().slice(0, 10).replace(/-/g, "")}.txt`;
        
        // Trigger download for the TXT file
        document.body.appendChild(txtLink);
        txtLink.click();
        document.body.removeChild(txtLink);
        
        // Decode the Base64 data to binary for the PPTX file
        const base64Data = response['Result'];
        const binaryData = atob(base64Data);

        // Convert binary data to a byte array for PPTX file
        const byteArray = new Uint8Array(binaryData.length);
        for (let i = 0; i < binaryData.length; i++) {
            byteArray[i] = binaryData.charCodeAt(i);
        }

        // Create a Blob for the PPTX file
        const pptxBlob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' });

        // Create a link element to download the PPTX file
        const pptxLink = document.createElement('a');
        pptxLink.href = URL.createObjectURL(pptxBlob);
        
        // Save name should be "DSS_YYYYMMDD.pptx" for today
        pptxLink.download = `DSS_${new Date().toISOString().slice(0, 10).replace(/-/g, "")}.pptx`;

        // Trigger download for the PPTX file
        document.body.appendChild(pptxLink);
        pptxLink.click();
        document.body.removeChild(pptxLink);

    } else {
      alert("Failed to create slides. Server might be down.");
    }
  });
}

function removeNonASCII(obj) {
  // Helper function to strip non-ASCII characters from a string
  const stripNonASCII = str => str.replace(/[^\x00-\x7F]/g, "");

  for (const key in obj) {
    if (typeof obj[key] === "string") { obj[key] = stripNonASCII(obj[key]); }
  }
  return obj;
}