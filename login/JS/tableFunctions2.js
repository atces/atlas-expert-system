/*
 * Ignacio.Asensi@cern.ch
 * This script is for dynamic tables. It recieves one paramter: table
 * In the form of:
 * 
 * Parameter: table name string to be processed
 * <script id="tableFunctions" src="JS/tableFunctions.js" table="tableName"></script>
 * */
$(function() {
	var height=0;
	var scripts=document.getElementsByClassName('tableFunctions');
	
	
	if(jsdebug.tables) console.log("Loading table functions 2");
	
	for (var i=0; i<scripts.length; i++){
			var sortingOrder=[[0,0]];
			var table=scripts[i].getAttribute('table');
			table="."+table+"_table";
			if(jsdebug.tables) console.log("Generating table: "+table);
			 var header_a=["zebra", "filter", "resizable"];
			 var options=null;
			//(table!=="#impact_table")?null:{'.switch':true, '.state':true, '.alarm':true},
			switch(table){
				case ".impact_table": 
					options={'.switch':true, '.state':true, '.alarm':true};
					break;
				//case "#administrators_table": options={'.action':true,};break;
				case ".alarm_table": options={'.triggered':true};break;
				case ".inhibit_table": 
					if(jsdebug.tables) console.log("Adding scroller");
					header_a=["scroller","zebra", "filter", "resizable"]
					height=255;
					break;
				case ".inhibited_table": 
					height=555; 
					break;
				case ".archived_table":
					sortingOrder=[[6,0]];
					height=400;
					
					break;
				default:
					options=null;
			}
			if (window.mobilecheck()){
			 
			   updateScroller = function () {
				   
		            $('.tablesorter-scroller-table').css({
		                height: '',
		                'max-height': 810 + 'px',
		                
		            });
		            $('.tablesorter-header-inner').css({
		                fontSize: '2em!important',
		                
		            });
		            if(height!==0){
			        	
			        	$('.tablesorter-scroller-table').css({
		                height: '',
		                'max-height': height + 'px',
		                
			            });
			        };
		        
			   }
			  $(table).tablesorter({
				theme: 'blue',
				widthFixed : false,
				widgets: ["zebra", "filter", "scroller"],
				ignoreCase: false,
				sortList:sortingOrder,
				dateFormat : "dd/mm/yy hh:mm",
				widgetOptions : {
					filter_childRows : false,
					filter_childByColumn : false,
					filter_childWithSibs : true,
					filter_columnFilters : true,
					filter_columnAnyMatch: true,
					filter_cellFilter : '',
					filter_cssFilter : '', // or []
					filter_defaultFilter : {},
					filter_excludeFilter : {},
					filter_external : '',
					filter_filteredRow : 'filtered',
					filter_formatter : null,
					filter_functions : 
						(table!=="#impact_table")?null:{'.switch':true, '.state':true, '.alarm':true},
						
						/*options,*/
					filter_hideEmpty : true,
					filter_hideFilters : false,
					filter_ignoreCase : true,
					filter_liveSearch : true,
					filter_matchType : { 'input': 'exact', 'select': 'exact' },
					filter_onlyAvail : 'filter-onlyAvail',
					filter_placeholder : { search : '', select : '' },
					filter_reset : 'button.reset',
					filter_resetOnEsc : true,
					filter_saveFilters : false,
					filter_searchDelay : 200,
					filter_searchFiltered: true,
					filter_selectSource  : null,
					filter_serversideFiltering : false,
					filter_startsWith : false,
					filter_useParsedData : false,
					filter_defaultAttrib : 'data-value',
					filter_selectSourceSeparator : '|',
		        	initialized: function(){
		            	updateScroller();
		        }
				}
			  	});
			
				$('.resetsaved').click(function(){
					$(table).trigger('filterResetSaved');
			
					// show quick popup to indicate something happened
					var $message = $('<span class="results"> Reset</span>').insertAfter(this);
					setTimeout(function(){
						$message.remove();
						}, 500);
					return false;
				});
			
				$('button[data-filter-column]').click(function(){
					var filters = [],
					$t = $(this),
					col = $t.data('filter-column'), // zero-based index
					txt = $t.data('filter-text') || $t.text(); // text to add to filter
			
					filters[col] = txt;
					$.tablesorter.setFilters( $(table), filters, true ); // new v2.9
					return false;
				});
				updateScroller();
			}else{
				/*var scripts=document.getElementById('tableFunctions');
			  var table=scripts.getAttribute('table');
			  table="#"+table+"_table";
			  console.log("table: "+table);*/
				 //var $table = $('table'),
				
				
		        updateScroller = function () {
		        	
		            $('.tablesorter-scroller-table').css({
		                height: '',
		                'max-height': height + 'px',//'max-height': $(window).height()-130 + 'px',
		                
		            });
		            if(height!==0){$('.tablesorter-scroller-table').css({
			                height: '',
			                'max-height': height + 'px',
			                
			            });
		            }
		            $('.tablesorter-header-inner').css({
		                fontSize: '2em!important',
		                
		            });
		        };
		
		    $(table).tablesorter({
		        theme: 'blue',
		        widthFixed: false,
		        sortList:[[6,0]],
		        widgets: header_a,
		        dateFormat : "dd/mm/yy hh:mm",
		        widgetOptions: {
		        	scroller_upAfterSort: false,
			        scroller_jumpToHeader: false,
			        filter_childRows : false,
					filter_childByColumn : false,
					filter_childWithSibs : true,
					filter_columnFilters : true,
					filter_columnAnyMatch: true,
					filter_cellFilter : '',
					filter_cssFilter : '', // or []
					filter_defaultFilter : {},
					filter_excludeFilter : {},
					filter_external : '',
					filter_filteredRow : 'filtered',
					filter_formatter : null,
					filter_functions : 
						options,
						
					filter_hideEmpty : true,
					filter_hideFilters : false,
					filter_ignoreCase : true,
					filter_liveSearch : true,
					filter_matchType : { 'input': 'exact', 'select': 'exact' },
					filter_onlyAvail : 'filter-onlyAvail',
					filter_placeholder : { search : '', select : '' },
					filter_reset : 'button.reset',
					filter_resetOnEsc : true,
					filter_saveFilters : false,
					filter_searchDelay : 200,
					filter_searchFiltered: true,
					filter_selectSource  : null,
					filter_serversideFiltering : false,
					filter_startsWith : false,
					filter_useParsedData : false,
					filter_defaultAttrib : 'data-value',
					filter_selectSourceSeparator : '|'
		        },
		        initialized: function(){
		            //if (table===".inhibit_table")updateScroller( 810 );
		            
		            if (table!==".inhibit_table"){
		            	
		            }else{
		            	updateScroller( 810 );
		            	}
		            
		        }
		    });
		
		    $('.update').on('click', function () {
		        updateScroller( );
		    });
		    
		    $('.resetsaved').click(function(){
				$(table).trigger('filterResetSaved');
		
				// show quick popup to indicate something happened
				var $message = $('<span class="results"> Reset</span>').insertAfter(this);
				setTimeout(function(){
					$message.remove();
					}, 500);
				return false;
			});
		
			$('button[data-filter-column]').click(function(){
				var filters = [],
				$t = $(this),
				col = $t.data('filter-column'), // zero-based index
				txt = $t.data('filter-text') || $t.text(); // text to add to filter
		
				filters[col] = txt;
				$.tablesorter.setFilters( $(table), filters, true ); // new v2.9
				return false;
			});
			}
			if(height!==0){
				$('#page2').css('height',(height+55+"px"));
				$('#page3').css('height',(height+55+"px"));
			}else{
				$('#page2').css('height',($(window).height()-30)+'px');
				$('#page3').css('height',($(window).height()-30)+'px');
			}
			
			
	}
});