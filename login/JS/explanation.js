var test={};
var sorted=[];
/* Get Explanation system info*/

function dashboard_get_explanation_input(){
	var req=$('#search_explanation').val();
	if (req!=""){
		dashboard_get_explanation(req);
	}
}

function getComputation(uid){//dashboard_get_explanation
	loading("start");
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_explanation(json){
		//console.log("JS json:");
		//console.log(json["Occurrences"]);
		$.ajax({
		url: 'templates/panel_computation.php',
        data: {occurrences:json["Occurrences"], commands:json["Commands"], uid:uid},
        type: 'post',
        success: function(data) {
	       	 $("#explanation_wrapper").html(data);
	       	getFormulaPoF(uid);
	       	drawObjectTree(uid);
	       	 loading("stop");
	        }
		});
	}
	
	//get last computation and parse it
	GetComputation(uid,parse_dashboard_explanation);
	
}

function getComputationAll(){//dashboard_get_computation
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_dashboard_computation_all(json){
		console.log("JS json:");
		//console.log(json["Occurrences"]);
		console.log(json);
		$.ajax({
		url: 'templates/panel_computation_all.php',//dashboard_panel_computation.php
        data: {commands:json["Computation"]},
        type: 'post',
        success: function(data) {
			$("#dashboard_computation_wrapper").html(data);
			process_table('dashboard_computation',5);//need to be run twice! why??
			process_table('dashboard_computation',5);
			toogleTableRows("dashboard_computation_table", "hide");
			loadDashboardSummary();
			loading("stop");
	        }
		});
	}
	
	//get last computation and parse it
	GetComputationAll(parse_dashboard_computation_all);
}

function goDown(obj){
	
	name=obj[2];
	parent=obj[3];
	level=obj[0];
	console.log("name:" + name);
	
	// get all entries with level -1 and parent with the name of the current
	
	
	childs=[];
	if(level>0){
		for (x in sorted){
			
			console.log("Checking sorted["+x+"][2]:"+sorted[x][2]);
			if (sorted[x][0]===level-1 && level>-1){
				//childs[sorted[x][2]]=sorted[x];
				console.log("calling goDown("+sorted[x][0]);
				goDown(sorted[x]);
				
			} 
			
		}
		//console.log("--------End--------" + name);
	}
	
}
