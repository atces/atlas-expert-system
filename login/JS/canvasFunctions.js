/**
 * Ignacio.Asensi@cern.ch 
 * October 2016
 * Functions for the UI simulator editor canvas features
 */
var c = document.getElementById('myCanvas');
var ctx = c.getContext('2d');
function menuDraw(){


  
	

//boxlink(boxes["as"]);
//boxlinker(boxes["a"], boxes["b"], '#ff0000');

ctx.clearRect(0, 0, c.width, c.height);
$.each(boxes, function(i){
	if (Object.getOwnPropertyNames(boxes[i].links).length > 0){
		$.each(boxes[i].links, function(x){
			console.log("from: "+i+". To: "+x);
			boxlinker(boxes[i], boxes[x], '#ff0000');
		});
		
		//boxlinker(boxes[i], )
	}
});


}
//testing func
function boxlink(origin){
	//no
	ctx.beginPath();
	ctx.lineWidth = 1;
	var top=origin.top-134 + origin.height;
	var left=origin.left + (origin.width/2);
	ctx.moveTo(left, top);
	ctx.lineTo(200, 200);
	ctx.stroke();

}

/**
 * Links the boxes (elements)
 * param start - start coordinate
 * param end - end coordinate
 * param color - color of line
 */

function boxlinker(start, end, color){
	ctx.beginPath();
	ctx.lineWidth = 1;
	ctx.strokeStyle= color;
	ctx
	var stop=start.top-134 + start.height;
	var sleft=start.left + (start.width/2);
	ctx.moveTo(sleft, stop);
	var etop=end.top-134 + end.height;
	var eleft=end.left + (end.width/2);
	ctx.lineTo(eleft, etop);
	ctx.stroke();
}


function menuScreenshot(){
	/*$('#noprint').hide();//for html2canvas 0.4
	var html2obj = html2canvas($('#print'));
	var queue  = html2obj.parse();
	var canvas = html2obj.render(queue);
	var img = canvas.toDataURL();
	$('#noprint').show();
	window.open(img);*/
	$('.noprint').hide();
	var html2obj= document.getElementById('print');
	html2canvas(html2obj).then(function(canvas) {
            //document.body.appendChild(canvas);
            window.open(canvas.toDataURL());
        });
    $('.noprint').show();
	
}