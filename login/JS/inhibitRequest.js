/**
* Used in inhibitRequest.php
* Used in findcauses.php
*
*/


var text="";
var inhibited=new Array;
var ar_alarms=new Array;

Array.prototype.clean = function(deleteValue) {
	  for (var i = 0; i < this.length; i++) {
	    if (this[i] == deleteValue) {         
	      this.splice(i, 1);
	      i--;
	    }
	  }
	  return this;
	};



var currentEmailFields = 0;
function addNewEmailField (email)
{
	var elementToAppendTo = document.getElementById('email_row_0');
	currentEmailFields += 1;

	var newEmailRow = "<tr id='email_row_" + currentEmailFields.toString() + "'><td/><td><textarea required style='resize: none;' class='to_inhibit_email' rows='1' cols='40'></textarea><td/><button onclick='removeNewEmailField(" + currentEmailFields.toString() + ")'>-</button></tr>";
	
	elementToAppendTo.insertAdjacentHTML('afterend', newEmailRow);	
}   
function removeNewEmailField (fieldNum)
{
	element = document.getElementById('email_row_' + fieldNum.toString());
	element.parentNode.removeChild(element);
}


function refresh_inhibited_on_table(tablename){
	for (a=0; a<inhibited.length; a++){
		if (inhibited[a]!==undefined)inhibit_add(inhibited[a]);
	}
}
/*****Functions to load table of items to select ****/
function loadInhibitTargetTable(type, delay){
	//setTimeout(function(){getAllClassObjects(type,parseRequestTable);}, delay);
	loading("start");
	function parseRequestTableWithCaption(json){
		$.ajax({
		url: 'templates/table_general.php',
        data: {json:json, tablename:type},
        type: 'post',
        success: function(data) {
	       	 $("#table_toinhibit_wrapper").html(data);
	       	 process_smalltable("inhibit",300);
	       	 refresh_inhibited_on_table("inhibit");
	       	 loading("stop");
	       	 //$("#inhibit_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	       	 //process_table("inhibit");
	        }
		});
	}
	getAllClassObjects(type,parseRequestTableWithCaption)
}



function loadGeneralTable(tablename, json){
		$.ajax({
		url: 'templates/table_general.php',
        data: {json:json, tablename:tablename},
        type: 'post',
        success: function(data) {
	       	 $("#tablename").html(data);
	       	 //$("table#tablename > tbody > tr").hide().slice(0, 3).show();
	        }
		});
	}



function inhibit_add(item){
	$("#add_"+item).hide();
	$("#del_"+item).show();
	if(!inhibited.includes(item)) inhibited.push(item);
	inhibited.clean("");
	$('#to_inhibit_textarea').val(inhibited.join("\n"));
}

function inhibit_add_full_filtered_table(tablename){
	filtered_rows=select_nonhidden_rows(tablename);
	for (var fr=0; fr<= filtered_rows.length; fr++){
		inhibit_add(filtered_rows[fr]);
	}
}
function inhibit_del_full_filtered_table(tablename){
	filtered_rows=select_nonhidden_rows(tablename);
	for (var fr=0; fr<= filtered_rows.length; fr++){
		inhibit_del(filtered_rows[fr]);
	}
}
function inhibit_del(item){
	$("#del_"+item).hide();
	$("#add_"+item).show();
	Array.prototype.remove = function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	};
	
	inhibited.remove(item);		
	inhibited.clean("");
	$('#to_inhibit_textarea').val(inhibited.join("\n"));
}
function resetInhibited(){
	inhibited=new Array;
	$('.inhibit_del').hide();
	$('.inhibit_add').show();
	
}


function button_requestRemove(uid){
	var comment = prompt("Leave any comment to remove the request. Then press OK");
	if (comment!=null){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'requestRemove',
	        		uid:uid,
	        		comment:comment
	        		},
	        type: 'post',
	        success: function(data) {
	        	console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}

function button_setInhibit(uid){
	var comment = prompt("This request will be treated as applied to DSS. Write any comments if any.");
	if (comment!=null){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'set_as_inhibit',
	        		uid:uid,
	        		comment:comment
	        		},
	        type: 'post',
	        success: function(data) {
	        	console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}
function button_set_NOTinhibit(uid){
	var comment = prompt("This request will be treated as NOT applied to DSS. Write any comments if any.");
	if (comment!=null){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'set_as_NOTinhibit',
	        		uid:uid,
	        		comment:comment
	        		},
	        type: 'post',
	        success: function(data) {
	        	console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}

function button_deleteInhibited(uid){
	var conf = confirm("Deleting a request can not be undone. Do you want to proceed?");
	if (conf===true){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'del',
	        		uid:uid,
	        		},
	        type: 'post',
	        success: function(data) {
	        	console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
	
}
function button_archiveInhibited(uid){
	loading("start");
	$.ajax({ url: 'email.php',//'+output.replace('#','')+'
        data: {	cmd: 'archive',
        		uid:uid,
        		},
        type: 'post',
        success: function(data) {
       	 	loadInhibitedTable();
       	 	loading("stop");
        },
        error: function(err){
        	console.log("error sending request to PHP");
        	console.log(err);
    	 	loading("stop");
        }
	});
}
function button_restoreInhibited(uid){
	loading("start");
	$.ajax({ url: 'email.php',//'+output.replace('#','')+'
        data: {	cmd: 'restore',
        		uid:uid,
        		},
        type: 'post',
        success: function(data) {
       	 	loadInhibitedTable();
       	 	loading("stop");
        },
        error: function(err){
        	console.log("error sending request to PHP");
        	console.log(err);
    	 	loading("stop");
        }
	});
}
function button_approveInhibited(uid){
	var comment = prompt("Enter a comment for the request. (Can be empty).");
	if (comment!=null){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'approve',
	        		uid:uid,
	        		comment:comment,
	        		},
	        type: 'post',
	        success: function(data) {
	        	if (jsdebug.parse) console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}


function button_declineInhibited(uid){
	var comment = prompt("Enter a reason to decline the request. It will be sent to requestor");
	if (comment!=null){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: 'decline',
	        		uid:uid,
	        		comment:comment,
	        		},
	        type: 'post',
	        success: function(data) {
	        	if (jsdebug.parse) console.log(data);
	       	 	loadInhibitedTable();
	       	 	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}

function loadArchivedTable(){
  loading("start");
	$.ajax({ url: 'templates/inhibitedRequests.php',//'+output.replace('#','')+'
	        data: {archived: true},
	        type: 'post',
	        success: function(data) {
	        	if (jsdebug.parse) console.log(data);
	        	$('#archived_list').html(data);
	       	 	endOfLoadingTables();
	       	 	process_table("archived");
          	$('#button_show_archived').css('visibility', 'hidden');
          	$('#button_hide_archived').css('visibility', 'visible');
            loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	        	loading("stop");
	        }
		});
}

function hideArchivedTable(){
  loading("start");
	$('#archived_list').html('');
	$('#button_show_archived').css('visibility', 'visible');
	$('#button_hide_archived').css('visibility', 'hidden');
  loading("stop");
}

function loadInhibitedTable(){
	loading("start");
	$.ajax({ url: 'templates/inhibitedRequests.php',//'+output.replace('#','')+'
        data: {archived:false},
        type: 'POST',
        success: function(data) {
        	if (jsdebug.parse) console.log(data);
        	$('#inhibited_list').html(data);
       	 	loading("stop");
       	 	checkRowPassedDate(1,'inhibited', 'expiration');
        },
        error: function(err){
        	console.log("error sending request to PHP");
        	console.log(err);
    	 	loading("stop");
        }
	});
}

function button_writeELISA(uid){
	if(confirm("This action will write an entry in the ELISA logbook. This action can not be undone. Do you want to proceed?")){
		loading("start");
		$.ajax({ url: 'scripts/elisa.php',//'+output.replace('#','')+'
	        data: {},
	        type: 'POST',
	        success: function(data) {
	        	if (jsdebug.parse) console.log(data);
	        	loading("stop");
	       	 },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
}
function clearRequestPanel(){
	//clear panel
	$('#to_inhibit_reason').val("");
	//$('.').val(""); //FIXME why this?
	var emailBoxes = document.getElementsByClassName('to_inhibit_email');
  	for (var i=0; i < emailBoxes.length; i++)
		emailBoxes[i].value = "";
	$('#datepicker_expiration').val("");
	$('#expiration_inhibit_hours').val("08");
	$('#expiration_inhibit_minutes').val("00");
	$('#datepicker_beginning').val("");
	$('#beginning_inhibit_hours').val("08");
	$('#beginning_inhibit_minutes').val("00");
	$('#to_inhibit_textarea').val("");
	//clear memmory
	resetInhibited();
	
}
function button_editRequest(uid){
	loading("start");
	
	//clear request panel
	clearRequestPanel();
	
	//load request to edit
	$.ajax({ url: 'email.php',//'+output.replace('#','')+'
        data: {cmd:"edit_get", 
        		uid:uid},
        
        type: 'POST',
        success: function(response) {
        	var data=JSON.parse(response); 
        	$('#to_inhibit_reason').val(data["reason"]);
        	$('#to_inhibit_email').val(data["mail"]);

		var emailBoxes = document.getElementsByClassName('to_inhibit_email');
		var inputEmails = data["mail"].split('\n');
		number_of_mails=inputEmails.length-1;
		emailBoxes[0].value = inputEmails[0];
	  	for (var i = 1; i < number_of_mails; i++){
	  		addNewEmailField(inputEmails[i]);
	  		emailBoxes[i].value = inputEmails[i];
	  	}
			

        	$('#datepicker_expiration').val(data["expiration"].slice(0,10));
        	$('#expiration_inhibit_hours').val(data["expiration"].slice(11,13));
        	$('#expiration_inhibit_minutes').val(data["expiration"].slice(14,16));
        	$('#datepicker_beginning').val(data["beginning"].slice(0,10));
        	$('#beginning_inhibit_hours').val(data["beginning"].slice(11,13));
        	$('#beginning_inhibit_minutes').val(data["beginning"].slice(14,16));
        	resetInhibited();
        	
        	for (var i=0; i<= data["alarms"].length; i++){
        		inhibit_add(data["alarms"][i]);
        	}
        	$('#hidden_uid').val(data["id"]);
        	loading("stop");
        	$('#inhibit_span').effect("highlight", {color:"#fff2a8"}, 4000);
        	$('#td_'+uid+'>td').css('background-color', 'red');
        	$('.disableonedit').prop('disabled', true);
       	 },
        error: function(err){
        	console.log("error sending request to PHP");
        	console.log(err);
    	 	loading("stop");
        }
	});
	
	
}
function checkOnlyEmail(email) {
    function validateEmail(elementValue){      
   		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   		return emailPattern.test(elementValue); 
 	} 
    if (!validateEmail(email)) {
        //alert("There are invalid characters in the email");   
        $("textarea.to_inhibit_email").css('background-color', 'red');
        $("#email_errors").text("There are errors in email"); 
    }else{
    	$("textarea.to_inhibit_email").css('background-color', '');
    	$("#email_errors").text("");
    	console.log("OK");               
		}
    }
function sendForm(cmd){
	var form=new Array();
	form.selected = inhibited;
	form.reason= $('#to_inhibit_reason').val();

	var emailBoxes = document.getElementsByClassName('to_inhibit_email');

	form.email = "";
  	for (var i = 0; i < emailBoxes.length; i++)
		form.email += emailBoxes[i].value + '\n';

	form.expiration = $('#datepicker_expiration').val()+" "+$('#expiration_inhibit_hours').val()+":"+$('#expiration_inhibit_minutes').val();
	form.beginning = $('#datepicker_beginning').val()+" "+$('#beginning_inhibit_hours').val()+":"+$('#beginning_inhibit_minutes').val();
	form.uid=$('#hidden_uid').val();
	
	
	function processRequest(cmd){
		loading("start");
		$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: cmd,
	        		selected:form.selected,
	        		reason: form.reason,
	        		requestor: form.email,
	        		expiration: form.expiration,
	        		beginning: form.beginning,
	        		uid:form.uid,
	        		},
	        type: 'post',
	        success: function(data) {
	       	 	
	       	 	loading("stop");
	       	 	if(cmd==="edit"){
	       	 		$('#td_'+form.uid+'>td').css('background-color', '');
	       	 		$('.disableonedit').prop('disabled', true);
	       	 	}
	       	 	messageSent();
	       	 	loadInhibitedTable()
	       	 	//leave only one mail box
	       	 	var emailBoxes = document.getElementsByClassName('to_inhibit_email');
	       	 	
	       	 	left_boxes=emailBoxes.length-1;
	       	 	
	       	 	for (var i=0; i<left_boxes; i++) {
	       	 		removeNewEmailField(i);
	       	 	}

	       	 	
	       	 	
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	loading("stop");
	        }
		});
	}
	if(form.selected.length===0){
		alert("No alarm selected");
	}else if (form.uid===""){
		processRequest("add");
	} else processRequest("edit");
	clearRequestPanel();
	
}



function sendForm_findcauses(cmd){
	function parseCallback(json){
		console.log(json);
	}



	var form=new Array();
	form.selected = inhibited;
	function processRequest(cmd){
		loading("start");
		console.log(form.selected);
	    elementsjson=JSON.stringify(form.selected);
		FindCauses(elementsjson, "alarms", parseCallback);
	}
	if(form.selected.length===0){
		alert("No alarm selected");
	}else if (form.uid===""){
		processRequest("add");
	} else processRequest("edit");
	clearRequestPanel();
	
}


function migrate(){
	$.ajax({ url: 'email.php',//'+output.replace('#','')+'
	        data: {	cmd: "migrate"},
	        type: 'post',
	        success: function(data) {
	       	 	
	       console.log(data);	 	
	       	 	
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	
	        }
		});
}
function endOfLoadingTables(){
	console.log("endOfLoadingTables");
	//check for editing parameters in url,. Can be sent from viewInihibt
	var url_string =window.location.href
	var url = new URL(url_string);
	var edit = url.searchParams.get("edit");
	
	if(edit!==null){
		button_editRequest(edit);
	}
}


	

function requests_apply(uid=""){
	if (uid==""){
		for (var uid in js_mem_inhibited) {
		    var ar2 = js_mem_inhibited[uid].length;
			for (var i = 0; i < ar2; i++) {
			    ChangeBoolAttribute(js_mem_inhibited[uid][i], "inhibit", 'true', dummy);
			    console.log(js_mem_inhibited[uid][i]);
			}
		}

	}else{
		var arrayLength = js_mem_inhibited[uid].length;
		for (var i = 0; i < arrayLength; i++) {
		    ChangeBoolAttribute(js_mem_inhibited[uid][i], "inhibit", 'true', dummy);
		}
	}
}
function requests_applyJSON(uid=""){
	loading("start");
	if (uid==""){
		var temp_mem= new Array();
		for (var uid in js_mem_inhibited) {
		    var ar2 = js_mem_inhibited[uid].length;
			for (var i = 0; i < ar2; i++) {
			    temp_mem.push(js_mem_inhibited[uid][i]);
			}
		}
		elementsjson=JSON.stringify(temp_mem);
		ChangeBoolJSONAttribute(elementsjson, "inhibit", 'true', dummy);
	}else{
		elementsjson=JSON.stringify(js_mem_inhibited[uid]);
		ChangeBoolJSONAttribute(elementsjson, "inhibit", 'true', dummy);
		
	}
}

function requests_triggerJSON(uid=""){
	loading("start");
	if (uid==""){
		var temp_mem= new Array();
		for (var uid in js_mem_inhibited) {
		    var ar2 = js_mem_inhibited[uid].length;
			for (var i = 0; i < ar2; i++) {
			    temp_mem.push(js_mem_inhibited[uid][i]);
			}
		}
		elementsjson=JSON.stringify(temp_mem);
		ChangeBoolJSONAttribute(elementsjson, "switch", 'off', updateBarIconsWithLoadingStop);
	}else{
		elementsjson=JSON.stringify(js_mem_inhibited[uid]);
		ChangeBoolJSONAttribute(elementsjson, "switch", 'off', updateBarIconsWithLoadingStop);
		
	}
}


/*
select_nonhidden_rows
general function to return the string of every row that is visible from a given table
*/
function select_nonhidden_rows(tablename){
	visible_rows= new Array();
	rows=$("#"+tablename+" > tbody > tr:visible td");
	for (a in rows){
		if (rows[a].innerText!== undefined ){
			row_st=rows[a].innerText.replace(" ","").replace("\n","");
			console.log(row_st);
			if (row_st.length>0) visible_rows.push(row_st);
		};
	}
	return visible_rows;
}



/* hide_table_buttons
remove buttons from table for better printed version*/
function hide_table_buttons(){
	$(".nonprintable").toggle();
}

function callInhibitAllActions(){
	function parseInhibitAllActions(json){
		if (json["Reply"]==="OK"){
			$('#callInhibitAllActions_debug').text("Actions are inhibited");
		}else{$('#callInhibitAllActions_debug').text("Error! Actions may not be inhibited. ");}
	}
	InhibitAllActions(parseInhibitAllActions);
}



function callCheckStateAllObjects(){

	function parseCheckStateAllObjects(json){

		$.ajax({ url: 'templates/table_simulator_tools_consistency_check.php',//'+output.replace('#','')+'
	        data: {	json: json["Reply"]},
	        type: 'post',
	        success: function(data) {
	       	 	$("#callCheckStateAllObjects_debug").html(data);
		       	process_smalltable("consistency_check",300);
		       	//refresh_inhibited_on_table("consistency_check");
		       	loading("stop");
	        },
	        error: function(err){
	        	console.log("error sending request to PHP");
	        	console.log(err);
	    	 	
	        }
		});
	}
	loading("start");
	CheckStateAllObjects(parseCheckStateAllObjects);
}



$(document).ready(function() {
	//inhibitRequest page
	$('#inhibit_form').on('submit', function(e){
		e.preventDefault();
		if (development===true){
			if (confirm("THIS IS THE DEVELOPMENT VERSION!!\n Are you sure you want to continue?") == true){
			sendForm();	
			}	
		}else{ sendForm(); }
		
		
	});

	//Find causes page
	$('#findcauses_form').on('submit', function(e){
		e.preventDefault();
		sendForm_findcauses();
	});
});

$(document).ajaxStop(function() {
  console.log("ajaxstop");// place code to be executed on completion of last outstanding ajax call here
});
