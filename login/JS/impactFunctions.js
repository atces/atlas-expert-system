/**
 * Ignacio.Asensi@cern.ch 
 * October 2016
 * Functions for impact simulator
 */


/*
function parseImpact(json_impacts){
	//console.log(json_impacts);
	var text="";
	//text="<tr><th>Item</th><th>State</th><th>Switch</th></tr>";
	$.each(json_impacts, function(i,obj){
		text+="<tr><td>"+i+"</td>";
		$.each(obj, function(k,v){
			console.log("i: "+i+". k: "+k+". v:"+v);
			text+="<td>"+v+"</td>";//<td>"+k+"</td>
		});
		text+="</tr>";
	});
	$('#impact_tbody').html(text);
	$("#impact_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	console.log("end");
}
*/
var impacted_items=false;
function lastImpact(){
	
	$('#impact_tbody').html("<tr><td colspan=\"4\">No affected systems</td></tr>");
}
function parseAlImpact(json_impacts_al){
	console.log("parsing alarms");
	var text="";
	for (var i in json_impacts_al["Result"]){
		text+="<tr><td class=\"cursor\" onclick=\"search('"+i+"','#page2')\">"+i+" <a class=\"newtabicon\" href=\"dssalarms.php?uid="+i+"\" ><img height=\"16px\" src=\"img/link.png\" ></a></td><td>Alarm</td><td></td><td></td></tr>";//target=\"_blank\" 
		impacted_items=true;

	}
	$('#impact_tbody').append(text);
	if(!impacted_items) lastImpact();
	setTimeout(function(){$("table").trigger("update").trigger("appendCache").trigger("applyWidgets");},300);
	loading("stop");
	
}

function parseSysImpact(json_impacts){
	console.log("parsing sys");
	var text="";
	var counter=0;
	
	
	$.each(json_impacts, function(i,obj){
		if(obj["State"]==="off" || obj["Switch"]==="off"){
			text+="<tr><td class=\"cursor\" onclick=\"search('"+i+"','#page2')\">"+i+"</td><td>Sys</td><td>"+obj["State"]+"</td><td>"+obj["Switch"]+"</td></tr>";
			impacted_items=true;
		}
		
		
	});
	//if(counter===0)text="<tr><td colspan=\"4\">No affected systems</td></tr>";
	$('#impact_tbody').append(text);
	$("table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	setTimeout(function(){GetTriggeredAlarms(parseAlImpact)}, 500);//requires some delay!!
	
}



function getImpactL2(power, type){
	
	function parseImpactL2(json_impacts_l2){
		var text="";
		var counter=0;
		
		text+="<tr><td colspan=\"2\">Affected by <b>"+power+"</b></td></tr><tr><td colspan=\"2\"><hr></td></tr>";
		$.each(json_impacts_l2["powers"], function(i,obj){
			text+="<tr><td>"+(i+1)+"</td><td>"+obj+"</td></tr>";
			counter++;
		});
		if(counter===0)text+="<td>No affected systems</td>";
		if (window.mobilecheck()){
			showPage2();
		}
		$('#l2_tbody').html(text);
		$('#impact_center').fadeIn();
	}
	GetObject(power, parseImpactL2);
	
}


function l2hide(){
	$('#impact_center').hide();
}
