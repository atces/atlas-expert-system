/****************************************
 * Expert system communication tools
 * Carlos.Solans@cern.ch
 * Ignacio.Asensi@cern.ch
 * Gustavo.Uribe@cern.ch
 * July 2016
 ****************************************/
var folder = "";
var path = "";
var folders = document.getElementsByClassName('db');
for (var i = 0; i < folders.length; i++) {
	var folder = "";
	folder = folders[i].getAttribute('folder');
	console.log("Folder:" + folder);
	if (folder !== "")
		path = "../";
}
var tokenid;
var trace_exception_from_server=false;
var AES_triggered_alarms=new Array();
var AES_triggered_inputs=new Array();
var AES_triggered_actions=new Array();

/**
 * setCookie
 * sets cookie
 */

function setCookie(cname, cvalue) {
	var d = new Date();
	d.setTime(0);
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname + "=" + cvalue;
}

/**
 * readCookie
 * read cookie value
 */

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
		c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

/**
 * isFunction
 * brief check if paramter is a function
 * param functionToCheck
 * return true if parameter is a function
 */

function isFunction(functionToCheck) {
	var getType = {};
	return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

/**
 * Request
 * brief configurable XMLHttpRequest
 * param query string
 * param callback function
 * param optional limit maximum number of calls, default=1
 * param optional use an asynchronous call, default=true
 */


//doRequest(path + "socketlogic.php?cmd=ChangeSwitch&TokenId=" + tokenid + "&uid=" + uid + "&json=" + json, callback);	////////

function doRequest(query, callback, limit = 1, async = true) {
    if (jsdebug.dbcalls) {
        console.log("doRequest() Query: " + query.substring(0, 35) + ". Callback: " + callback.name);
    }

    $.ajax({
        url: query,
        type: 'GET',
        async: async,
        success: function(responseText) {
            try {
                if (jsdebug.responses) { console.log("request.responseText: " + responseText); }
				// Parse the response text to JSON.
                var json = JSON.parse(responseText);
                if (typeof callback === 'function') {
                    var trace_exception_from_server = json["Trace_exception"];
                    if (trace_exception_from_server === true) {
                        showServerException();
                    }
                    callback(json);
                } else {
                    console.log("!!!!!!!! callback is not a function");
                }
            } catch (err) {
                console.log("!!!!!!!! Error in Server response: " + err);
                if (limit > 0) {
                    console.log("Try... " + limit);
                    doRequest(query, callback, limit - 1, async);
                }
            }
        },
		// 
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Request failed: " + textStatus + ", " + errorThrown);
            if (limit > 0) {
                console.log("Try... " + limit);
                doRequest(query, callback, limit - 1, async);
            }
        }
    });
}



function doPostRequest(query, callback, limit = 1, async = true) {
	if (jsdebug.dbcalls) {
    	console.log("doPostRequest() Query: " + query.substring(0, 35) + ". Callback: " + callback.name);
	}

	// Extract params and url from the query string.
    const params = query.split("?")[1];
    const url = query.split("?")[0];

	// async ajax post request.
    $.ajax({
        url: url,
        type: 'POST',
        async: async,
        data: params,
        contentType: 'application/x-www-form-urlencoded', // json gives error
        success: function(responseText) {
            try {
                if (jsdebug.responses) { console.log("request.responseText: " + responseText); }
				// Parse the response text to JSON.
                var json = JSON.parse(responseText);
                if (typeof callback === 'function') {
                    var trace_exception_from_server = json["Trace_exception"];
                    console.log("trace_exception_from_server:" + trace_exception_from_server);
                    if (trace_exception_from_server === true) {
                        showServerException();
                    }
                    callback(json);
                } else {
                    console.log("!!!!!!!! callback is not a function");
                }
            } catch (err) {
                console.log("!!!!!!!! Error in Server response: " + err);
                if (limit > 0) {
                    doPostRequest(query, callback, limit - 1, async);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Request failed: " + textStatus + ", " + errorThrown);
            if (limit > 0) {
                doPostRequest(query, callback, limit - 1, async);
            }
        }
    });
}

/**
 * RunWithTokenId
 * brief wait until tokenid is received before executing command
 * param callback function
 */

function runWithTokenId(callback) {
	if ( typeof tokenid == 'undefined') {
		window.setTimeout(runWithTokenId, 200);
	} else {
		// if(isFunction(callback)){callback();}
		if (isFunction(callback)) {
			callback(json);
		}
	}
}

/**
 * GetTokenId
 * If tid is in query string, use it
 * If tid in cookie, use it
 * If tid is valid, use it
 * If tid is not valid, get new tid
 */

function GetTokenId() {
	var debug=true;
	if (jsdebug.session) debug=true;
	if(debug) console.log("Getting token id...");
	var tid = null;
	if ("TokenId" in QueryString) {
		tid = QueryString["TokenId"];
		if (debug) console.log("Found TokenId in QueryString:" + tid);
	} else if (document.cookie.indexOf(cname) >= 0) {
		tid = readCookie(cname);
		if (debug) console.log("Found TokenId in Cookie:" + tid);
	}
	if (tid != null) {
		if (debug) console.log("Check validity of TokenId");
		doRequest(path + "socketlogic.php?cmd=CheckToken&TokenId=" + tid, function(json) {
			if (json["Reply"] != "Valid") {
				if (debug) console.log("Server says TokenId is invalid");
				tid = null;
			}
		}, 1, false);
		if (debug) console.log("TokenId after check is: " + tid);
	}
	if (tid != null) {
		tokenid = tid;
	} else {
		if (debug) console.log("TokenId not found in QueryString nor in Cookie. Get a new TokenId from the server");
		doRequest(path + "socketlogic.php?cmd=GetNewToken&dev="+((development)?"True":"False"), function(json) {
			tokenid = json["TokenId"];
			if (debug){
				console.log("TokenID received is: " + tokenid);
				console.log("JSON:" + JSON.stringify(json));
			}
				
			setCookie(cname, tokenid);
		}, 1, false);
	}
	if (debug) console.log("GetTokenId end");
	return tokenid;
}

/**
 * getToken
 * brief get a new tokenid and perform action afterwards
 *
 */

function GetToken(callback) {
	doRequest(path + "socketlogic.php?cmd=GetNewToken&dev="+((development)?"True":"False"), function(json) {
		tokenid = json["TokenId"];
		if (jsdebug.session)
			console.log("TokenID received is: " + tokenid);
		setCookie(cname, tokenid);
		if (isFunction(callback)) {
			callback();
		} else {
			if (jsdebug.session)
				console.log("Callback is not a function:" + callback);
		}
	});
}

/**
 * getNewToken
 * brief get a new tokenid and store it in a global variable tokenid
 */
function GetNewToken() {
	if (jsdebug.session)
		console.log("Cookie index: " + document.cookie.indexOf(cname));
	if (document.cookie.indexOf(cname) > 0) {
		var cvalue = readCookie(cname);
		tokenid = cvalue;
		if (jsdebug.session)
			console.log("Cookie value: " + cvalue);
	} else {
		doRequest(path + "socketlogic.php?cmd=GetNewToken", function(json) {
			tokenid = json["TokenId"];
			if (jsdebug.session)
				console.log("TokenID received is: " + tokenid);
			setCookie(cname, tokenid);
		});
	}
}

/**
 * getTokenAndCurrentState
 * brief get token id and current state in one go
 */

function GetTokenAndCurrentState() {
	doRequest(path + "socketlogic.php?cmd=GetNewToken", function(json) {
		tokenid = json["TokenId"];
		if (jsdebug.session)
			console.log("TokenID received is: " + tokenid);
		if ( typeof tokenid == 'undefined')
			return;
		if (jsdebug.session)
			console.log("Request current state")
		GetCurrentState();
	});
}

 

function loopingStates(callback) {
	doRequest(path + "socketlogic.php?cmd=GetCurrentState&TokenId=" + tokenid, callback);
}

/**
 * FindObject
 * brief find objects matching uid
 * param uid string
 * param callback function to handle list of objects
 */

function FindObject(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=FindObject&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * isAValidUID
 * brief Validate if the uid is in the database
 * param uid string
 * param callback function to handle the response TRUE or FALSE
 */

function isAValidUID(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=isAValidUID&TokenId=" + tokenid + "&uid=" + uid, callback);
}

function GetNoClassObject(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetNoClassObject&TokenId=" + tokenid + "&uid=" + uid, callback);
}

function GetNumberOfAffectedSystems(callback) {
	doRequest(path + "socketlogic.php?cmd=GetNumberOfAffectedSystems&TokenId=" + tokenid, callback);
}

function GetAffectedSystems(callback) {
	doRequest(path + "socketlogic.php?cmd=GetAffectedSystems&TokenId=" + tokenid, callback);
}
function GetAffectedSystemsWithLocation(callback) {
	doRequest(path + "socketlogic.php?cmd=GetAffectedSystemsWithLocation&TokenId=" + tokenid, callback);
}
function GetIntervenedSystemsWithLocation(callback) {
	doRequest(path + "socketlogic.php?cmd=GetIntervenedSystemsWithLocation&TokenId=" + tokenid, callback);
}

function GetCurrentStateFull(callback) {
	doPostRequest(path + "socketlogic.php?cmd=GetCurrentStateFull&TokenId=" + tokenid, callback);
}

/**
 * SaveCurrentState
 * brief save the current state of the database
 * param folder name
 * param callback function to handle the reply
 */

function SaveCurrentState(fname, callback) {
	doRequest(path + "socketlogic.php?cmd=SaveCurrentState&TokenId=" + tokenid + "&fname=" + fname, callback);
}

/**
 * GetStateList
 * list of all the saved states/configurations
 * param callback function to handle reply
 */

function GetStateList(callback) {
	doRequest(path + "socketlogic.php?cmd=GetStateList&TokenId=" + tokenid, callback);
}

/**
 * GetSavedState
 * get chosen state and redirect to page1
 * param state name
 * param callback function to handle reply
 */

function GetSavedState(statename, callback) {
	doRequest(path + "socketlogic.php?cmd=GetSavedState&TokenId=" + tokenid + "&statename=" + statename, callback);
}

/**
 * ChangeState
 * brief flip the state of an object
 * param uid string
 * param callback function
 */

function ChangeState(uid, callback) {
	//ahist
	doRequest(path + "socketlogic.php?cmd=ChangeState&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * ChangeSwitch
 * brief flip the switch of an object
 * param uid string
 */

function ChangeSwitch(uid ) {
	console.log("!!!!!!!!! Call to db.js to ChangeSwitch ")
	loading("start");
	if (pageElements === undefined) {
		loadPageElements(dummy);//LoadJSONCurrentState();
	}
	var json = JSON.stringify(pageElements);
	ctime=$("#time_select").val();	
	doPostRequest(path + "socketlogic.php?cmd=ChangeSwitch&TokenId=" + tokenid + "&uid=" + uid + "&json=" + json + "&ctime=" + ctime, parseJSONCurrentState,0);
}

/**
 * ChangeSwitch
 * brief flip the switch of an object
 * param uid string
 * param callback function
 */

function ChangeSwitchAndReloadDSS(uid, callback) {
	loading("start");
	ctime=$("#time_select").val();	
	json="[]";
	doPostRequest(path + "socketlogic.php?cmd=ChangeSwitch&TokenId=" + tokenid + "&uid=" + uid + "&json=" + json + "&ctime=" + ctime, callback);
}

/**
 * ChangeSwitchWithJSON
 * brief flip the switch of an object
 * param uid string
 * param callback function
 */
function ChangeSwitchWithJSON(uid, obj_class, json, callback) {
	loading("start");
	ctime=$("#time_select").val();
	//ahist.push(uid);
	//sessionStorage.setItem(Math.floor(Date.now() / 1000), JSON.stringify([uid, ($('#' + uid.replace(/\./g, "\\\.") + "sw").attr("src").indexOf("off.png") !== -1) ? "on" : "off"]));
	if(obj_class==="action") {
		doRequest(path + "socketlogic.php?cmd=ChangeSwitchAction&TokenId=" + tokenid + "&uid=" + uid + "&json=" + json, callback);
	}else if(obj_class==="sys") {
		doRequest(path + "socketlogic.php?cmd=ChangeSwitch&TokenId=" + tokenid + "&uid=" + uid + "&json=" + json+ "&ctime=" + ctime, callback);
	}else{
		console.log("ERROR: CLASS NOT PROVIDED TO ChangeSwitchWithJSON IN ui.js")
	}
	
}


/*
 * get all the alarms that affect a page
 */
function GetAlarmsAffectingPage(json, callback){
	doRequest(path + "socketlogic.php?cmd=GetAlarmsAffectingPage&TokenId=" + tokenid + "&json=" + elementsjson, callback);
}

/**
 * GetSchema
 * param uid string
 * param reply callback function
 *
 */
function GetSchema(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetSchema&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * GetClassSchema
 * param class string
 * param reply callback function
 *
 */
function GetClassSchema(classname, callback) {
	doRequest(path + "socketlogic.php?cmd=GetClassSchema&TokenId=" + tokenid + "&class=" + classname, callback);
}

/**
 * GetClass
 * param uid string
 * param reply callback function
 *
 */
function GetClass(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetClass&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * AdvancedSearchObject
 * brief find objects matching passed attribute
 * param string search
 * param attribute name
 * param callback function to handle list of objects
 */

function AdvancedSearchObject(string, attribute, type, callback) {
	doRequest(path + "socketlogic.php?cmd=AdvancedSearchObject&TokenId=" + tokenid + "&string=" + string + "&attribute=" + attribute + "&type=" + type, callback);
}

function AdvancedSearchObjectFullTable(string, attribute, type, callback) {
	doRequest(path + "socketlogic.php?cmd=AdvancedSearchObjectFullTable&TokenId=" + tokenid + "&string=" + string + "&attribute=" + attribute + "&type=" + type, callback);
}

/**
 * SearchObject
 * brief find objects matching uid
 * param uid string
 * param callback function to handle list of objects
 */

function SearchObject(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=SearchObject&TokenId=" + tokenid + "&uid=" + uid, callback);
}


function GetFullTableSearch(callback) {
	doRequest(path + "socketlogic.php?cmd=GetFullTableSearch&TokenId=" + tokenid , callback);
}



/**
 * TriggerAlarm
 * sets triggered state to yes
 * param uid string
 * param callback function to handle list of objects
 */

function TriggerAlarm(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=TriggerAlarm&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * UnTriggerAlarm
 * sets triggered state to no
 * param uid string
 * param callback function to handle list of objects
 */

function UnTriggerAlarm(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=UnTriggerAlarm&TokenId=" + tokenid + "&uid=" + uid, callback);
}


/**
 * sets triggered state to no
 * param uid string
 * param callback function to handle list of objects
 */

function GetTriggeredAlarms(callback) {
	doRequest(path + "socketlogic.php?cmd=GetTriggeredAlarms&TokenId=" + tokenid, callback);
}


/**
 * GetInhibitedItems
 * get all inhibited items 
 * param uid string
 * param callback function to handle list of objects
 */

function GetInhibitedItems(callback) {
	doRequest(path + "socketlogic.php?cmd=GetInhibitedItems&TokenId=" + tokenid, callback);
}

/**
 * RemoveDelayedAction
 * sets triggered state to no
 * param uid string
 * param callback function to handle list of objects
 */

function RemoveDelayedAction(uid, dauid, callback) {
	doRequest(path + "socketlogic.php?cmd=RemoveDelayedAction&TokenId=" + tokenid + "&uid=" + uid + "&dauid=" + dauid, callback);
}

/**
 * GetClasses
 * return list of object classes
 * param callback function to handle list of objects
 */
function GetClasses(callback) {
	doRequest(path + "socketlogic.php?cmd=GetClasses&TokenId=" + tokenid, callback);
}

function GetSysClasses(callback) {
	doRequest(path + "socketlogic.php?cmd=GetSysClasses&TokenId=" + tokenid, callback);
}

/**
 * GetAllObjects
 * returns list of objects of type/class received as parameter
 * @param {Object} type of objects to return
 * @param {Object} callback function to handle list of objects
 */
function GetAllObjects(type, callback) {
	doRequest(path + "socketlogic.php?cmd=GetAllObjects&TokenId=" + tokenid + "&type=" + type, callback);
}

/**
 * GetPingResponse
 * returns response to ping
 * @param {Object} callback function to handle list of objects
 */

function GetPingResponse(callback) {
	doRequest(path + "socketlogic.php?cmd=Ping", callback);
}

/**
 * GetRestart
 * returns response to restart
 * @param {Object} callback function to handle list of objects
 */
function GetRestart(callback) {
	doRequest(path + "socketlogic.php?cmd=Restart", callback);
}

/*
 *  GetFullDSU 
 *  return fullDSU with DIs and Actions
 *  @param {Object} uid of alarm
 *  @param {Object} callback function to handle list of objects
 */
function GetFullDSU(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetFullDSU&TokenId=" + tokenid + "&uid=" + uid, callback);
}


/**
 * getDDVhistory
 * get DDV history of uid
 */
function GetDDVhistory(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetDDVhistory&TokenId=" + tokenid + "&uid=" + uid, callback);
}

function GetDDVTree(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetDDVTree&TokenId=" + tokenid + "&uid=" + uid, callback);
}

/**
 * GetNoClassObjectDescription
 * gets ONLY the description of an element. Has to work for all classes
 * @param {Object} uid
 * @param {Object} callback
 */
function GetDescription(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetNoClassObjectDescription&TokenId=" + tokenid + "&uid=" + uid, callback);
}

function GetElementsDescription(json, callback) {
	doRequest(path + "socketlogic.php?cmd=GetElementsDescription&TokenId=" + tokenid + "&json=" + json, callback);
}
function GetElementsSubsystems(json, callback) {
	doRequest(path + "socketlogic.php?cmd=GetElementsSubsystems&TokenId=" + tokenid + "&json=" + json, callback);
}
function GetCurrentState(json, callback) {
	if(json == null){console.log("NO JSON!!");}
	doPostRequest(path + "socketlogic.php?cmd=GetCurrentState&TokenId=" + tokenid + "&json=" + json, callback);
}

/**
 * GetPoF
 * Obtain Probability of Failure for an element
 * @param {Object} uid: ID of the element
 * @param {Object} level: Level of parents
 * @param {Object} callback: Function to callback
 */
function GetPoF(uid, level, callback) {
	doRequest(path + "socketlogic.php?cmd=GetPoF&TokenId=" + tokenid + "&uid=" + uid + "&level=" + level, callback);
}

function consoleOut(msg) {
	console.log(msg);
}

function getAllClassObjects(db_class, callback){
	doRequest(path + "socketlogic.php?cmd=GetAllObjectsOfAClass&TokenId=" + tokenid + "&class_name=" + db_class, callback);
}



function GetCommandsHistory(callback){
	doRequest(path + "socketlogic.php?cmd=GetCommandsHistory&TokenId=" + tokenid, callback);
}
function GetActions(callback){
	doRequest(path + "socketlogic.php?cmd=GetActionsHistory&TokenId=" + tokenid, callback);
}
function GetActiveActions(callback){
	doRequest(path + "socketlogic.php?cmd=GetActiveActions&TokenId=" + tokenid, callback);
}

function GetActiveInputs(callback){
	doRequest(path + "socketlogic.php?cmd=GetActiveInputs&TokenId=" + tokenid, callback);
}

function GetComputation(uid,callback){
	doRequest(path + "socketlogic.php?cmd=GetComputation&TokenId=" + tokenid+ "&uid=" + uid, callback);
}
function GetComputationAll(callback){
	doRequest(path + "socketlogic.php?cmd=GetComputationAll&TokenId=" + tokenid, callback);
}
function GetAttributeCounts(class_name, attr_name, callback){
	doRequest(path + "socketlogic.php?cmd=GetAttributeCounts&TokenId=" + tokenid+ "&class=" + class_name+ "&attribute=" + attr_name, callback);
}
function GetTestingCMD(cmd, callback){
	doRequest(path + "socketlogic.php?cmd="+cmd+"TokenId=" + tokenid, callback);
}

function GetFormulaPoF(uid, level_deep, callback){
	doRequest(path + "socketlogic.php?cmd=GetFormulaPoF&TokenId=" + tokenid+ "&uid=" + uid+"&level=" + level_deep, callback);
}

function GetReliabilityOfClass(name,callback){
	doRequest(path + "socketlogic.php?cmd=GetReliabilityOfClass&TokenId=" + tokenid+ "&class_name=" + name, callback);
}

function GetObjectParentsTree(uid, level_deep, parameters, callback){
	doRequest(path + "socketlogic.php?cmd=GetObjectParentsTree&TokenId=" + tokenid+ "&uid=" + uid+ "&level=" + level_deep+ "&parameters=" + parameters, callback);
}

function GetAttrObjectParentsTree(uid, level_deep, info, attr, line, callback){
	doRequest(path + "socketlogic.php?cmd=GetAttrObjectParentsTree&TokenId=" + tokenid+ "&uid=" + uid+ "&level=" + level_deep + "&info=" + info+ "&attr=" + attr+ "&line=" + line, callback);
}

function GetObjectChildrenTree(uid, depth, parameters, callback){
	doRequest(path + "socketlogic.php?cmd=GetObjectChildrenTree&TokenId=" + tokenid+ "&uid=" + uid+ "&level=" + depth + "&parameters=" + parameters, callback);
}


/**
 * GetSummaryOfObjects
 * @param {Object} uid: ID of the element
 * @param {Object} callback: Function to callback
 */
function GetSummaryOfObjects(uid, callback) {
	doRequest(path + "socketlogic.php?cmd=GetSummaryOfObjects&TokenId=" + tokenid, callback);
}


function GetSimulationJSON(callback){
	doRequest(path + "socketlogic.php?cmd=GetSimulationJSON&TokenId=" + tokenid, callback);	
}

function CheckSimulationJSON(simulation, callback){
	doRequest(path + "socketlogic.php?cmd=CheckSimulationJSON&TokenId=" + tokenid+"&simulation="+simulation, callback);	
}
function isSystem(uid, callback){
	doRequest(path + "socketlogic.php?cmd=isSystem&TokenId=" + tokenid+ "&uid=" + uid, callback);	
}
function GetClassAndLocation(json, callback){
	doRequest(path + "socketlogic.php?cmd=GetJSONObjectsClassAndLocation&TokenId=" + tokenid + "&json=" + json, callback);
}

/**
Used in simulator_evaluator
**/
function GetSimulations(callback){
	doRequest(path + "socketlogic.php?cmd=GetSimulations&TokenId=" + tokenid, callback);	
}
function RunSimulationFromDB(simulation, callback){
	doRequest(path + "socketlogic.php?cmd=RunSimulationFromDB&TokenId=" + tokenid+"&simulation="+simulation, callback);	
}



function ChangeAttribute(uid, attr, value, callback){
	doRequest(path + "socketlogic.php?cmd=ChangeAttribute&uid=" + uid + "&TokenId=" + tokenid + "&attr=" + attr+ "&value=" + value, callback);
}

function ChangeBoolAttribute(uid, attr, value, callback){
	doRequest(path + "socketlogic.php?cmd=ChangeBoolAttribute&uid=" + uid + "&TokenId=" + tokenid + "&attr=" + attr+ "&value=" + value, callback);
}

function ChangeBoolJSONAttribute(json, attr, value, callback){
	doRequest(path + "socketlogic.php?cmd=ChangeBoolJSONAttribute&json=" + json + "&TokenId=" + tokenid + "&attr=" + attr+ "&value=" + value, callback);
}




function GetSummaryGroupsAffected(callback){
	doRequest(path + "socketlogic.php?cmd=GetSummaryGroupsAffected&TokenId=" + tokenid , callback);
}
function triggerSniffers(callback) {
	doRequest(path + "socketlogic.php?cmd=triggerSniffers&TokenId=" + tokenid, callback);
}


function GetMinimaxDeployed( callback){
	doRequest(path + "socketlogic.php?cmd=GetMinimaxDeployed&TokenId=" + tokenid, callback);
}


function SetSwitchNoDB(json, callback){
	// json must be of type JSON.stringify
	doRequest(path + "socketlogic.php?cmd=SetSwitchNoDB&TokenId=" + tokenid + "&json=" + json, callback);
}


function InhibitAllActions(callback){
	doRequest(path + "socketlogic.php?cmd=InhibitAllActions&TokenId=" + tokenid , callback);
}


function CheckStateAllObjects(callback){
	doRequest(path + "socketlogic.php?cmd=CheckStateAllObjects&TokenId=" + tokenid  + "&ctime=" + ctime, callback);
}

function FindCauses(elementsjson, type, callback){
    doRequest(path + "socketlogic.php?cmd=FindCauses&TokenId=" + tokenid  + "&type=" + type  + "&json=" + elementsjson, callback);
}

function UpdateSimulationDB(callback){
    doRequest(path + "socketlogic.php?cmd=UpdateSimulationDB&TokenId=" + tokenid , callback);
}

function GetObjsClasses(uids,callback){
    doPostRequest(path + "socketlogic.php?cmd=GetObjsClasses&TokenId=" + tokenid  + "&uids=" + JSON.stringify(uids), callback,1,false);
}

// For the MPC tool
function findMPC(json, callback, isExhaustive=true, elapsed_time="1", inhibited_objects_uids=[]){
	// json must be of type JSON.stringify
	console.log("-- findMPC(isExhaustive="+isExhaustive+",elapsed_time="+elapsed_time+",inhibited_objects_uids="+inhibited_objects_uids+") --");
	doPostRequest(path + "socketlogic.php?cmd=FindMPC&TokenId=" + tokenid +"&isExhaustive="+ isExhaustive +"&elapsedTime="+elapsed_time+ "&json=" + json+"&inhibitedObjects="+JSON.stringify(inhibited_objects_uids), callback, 1, false);
}

function GetMPCCalculationAndStatus(callback){
    doRequest(path + "socketlogic.php?cmd=GetMPCCalculationAndStatus&TokenId=" + tokenid , callback);
}


function GetMPCCalculationDetails(uid, callback){
	doRequest(path + "socketlogic.php?cmd=GetMPCCalculationDetails&TokenId=" + tokenid+ "&uid=" + uid, callback);	
}

function stopMPC(uid, callback){
	doRequest(path + "socketlogic.php?cmd=StopMPC&TokenId=" + tokenid+ "&uid=" + uid, callback);	
}

// for the alarm helper
function ReadDisplayEventsLogs(json, callback){
	doPostRequest(path + "socketlogic.php?cmd=ReadDisplayEventsLogs&TokenId=" + tokenid +"&json="+ json, callback);
}

function WriteDisplayEventsLogs(json, callback){
	doPostRequest(path + "socketlogic.php?cmd=WriteDisplayEventsLogs&TokenId=" + tokenid +"&json="+ json, callback);
}

function GetAlarmStats(json, callback){
	doPostRequest(path + "socketlogic.php?cmd=GetAlarmStats&TokenId=" + tokenid +"&json="+ json, callback);
}

function MatchLogsLogactions(json, callback){
	doPostRequest(path + "socketlogic.php?cmd=MatchLogsLogactions&TokenId=" + tokenid + "&json="+ json , callback);
}

function CreateSlides(json, callback){
	doPostRequest(path + "socketlogic.php?cmd=CreateSlides&TokenId=" + tokenid + "&json="+ json , callback);
}
