var alarms = [];
var selected_alarms = [];
var events = [];
var events_dict = new Object();

$().ready(function(){
  var statsDate = new Date();
  var statsString = statsDate.toISOString().split('T')[0];

  // check if in url
  var dateParam = getUrlParameter('date');
  if (dateParam !== '') { statsString = dateParam; }

  document.getElementById("stats_date").value = statsString;

  updateBarIcons();
  GetAllAlarms("Alarm");
  CreateLegend();
});


function buttonChartEventTypeDistribution(){
  console.log("buttonChartEventTypeDistribution");

  if (selected_alarms.length === 0) {
    $("#alarmStats-reply").html("Please select at least one alarm.");
    return;
  }

  var jsonDict = {
    "alarms": selected_alarms,
    "date": document.getElementById("stats_date").value,
    "nevents" : -1,
  }

  // change the class of the divs with id  ["div-last-month", "div-last-year", "div-inclusive" ] to "class="chart-container" to display the chart
  let ids = ["div-last-month", "div-last-year", "div-inclusive"];
  for (let i = 0; i < ids.length; i++) { 
    console.log("buttonChartEventTypeDistribution ids[i]: ", ids[i]);
    document.getElementById(ids[i]).className = "chart-container"; }

  GetAlarmStats(JSON.stringify(jsonDict), function(response) {
    DisplayAlarmStats(JSON.parse(response['Result']));
  });
}

function DisplayAlarmStats(response) {
  console.log("DisplayAlarmStats");

  var stats = response['stats'];
  ChartEventTypeDistribution("chart-last-month", stats["last-month"]["sum"]);
  ChartEventTypeDistribution("chart-last-year", stats["last-year"]["sum"]);
  ChartEventTypeDistribution("chart-inclusive", stats["inclusive"]["sum"]);

  events = response["events"];
  DisplayEvents(events, true);

  // convert event array to dict with key event[uid]
  for (let i = 0; i < events.length; i++) { events_dict[events[i]["uid"]] = events[i]; }

  $("#alarmStats-reply").html("Loaded stats.");
}


// * * * * *
// charts
// * * * * *

function ChartEventTypeDistribution( chart_id, data){ 
  console.log("ChartEventTypeDistribution", chart_id);

  const eValue = data["E"] || 0;
  const iValue = data["I"] || 0;
  const uValue = data["U"] || 0;

  var canvas = document.getElementById(chart_id);
  if (!canvas) {
    console.error("Canvas with ID '" + chart_id + "' not found.");
    return;
  }

  // Ensure the canvas is not already used
  if (canvas.chart) { canvas.chart.destroy(); }

  var ctx = canvas.getContext('2d');

  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [''],
          datasets: [
          {
            label: 'Error ' + String(eValue),
            data: [eValue],
            backgroundColor: 'rgba(255, 159, 64, 1)',
            borderColor: 'rgba(255, 159, 64, 1)',
          },
          {
            label: 'Intervention ' + String(iValue),
            data: [iValue],
            backgroundColor: 'rgba(54, 162, 235, 1)',
            borderColor: 'rgba(54, 162, 235, 1)',
          },
          {
            label: 'Unknown ' +  String(uValue),
            data: [uValue],
            backgroundColor: 'rgba(201, 203, 207, 1)',
            borderColor: 'rgba(201, 203, 207, 1)',
          }
          ]
      },
      options: {
        animation: false, 
        indexAxis: 'y',
        scales: {
            x: {
              // display: false,
              // stacked: true,
              min: 0, 
              max: Math.max(eValue, iValue, uValue) === 0 ? 1 : Math.max(eValue, iValue, uValue),
              ticks: {
                // display: false,
                // beginAtZero: true,
                stepSize: 1,
              },
              grid: {
                display: false, // Turn off grid lines
              },
            },
            y: {
            }
        },
        legend: {
          display: false // Hide the legend
        },
        plugins: {
          legend: {
              display: false, // Hide legend labels
          },
          tooltip: {
            callbacks: {
                label: function (context) {
                  return context.dataset.label; // Display only the dataset label
                }
            }
          },
        },
      }

  });

  canvas.chart = myChart;
}


// * * * * *
// alarm table
// * * * * *
function DisplayAlarmTable() {
  console.log("DisplayAlarmTable");

  var html_string = '';
  html_string += '<table style="overflow-y:auto; max-height: 300px; width: 100%; font-size:20px;">';
  
  for (var i = 0; i < alarms.length; i++) {
    var alarm = alarms[i];

    html_string += '<tr>';
    html_string += '<td><button class="button-plus-alarmHelper" onclick="addToSelected(' + JSON.stringify(alarm).replace(/"/g, '&quot;') + ')"><img src="../images/alarmHelper/plus.png" alt="select_button"></button></td>';
    html_string += '<td>' + alarm + '</td>';
    html_string += '</tr>';
  }
  html_string += '</table>';
  $("#table_alarms_to_select").html(html_string);
}

function GetAllAlarms(type) {
  console.log("GetAllAlarms");
  
  getAllClassObjects(type, function(response) {
    alarms_obj = response["Result"] 

    alarms = Object.entries(alarms_obj)
      .filter(([key, value]) => value.startsWith("AL_"))
      .map(([key, value]) => value);
    alarms.sort();

    // Retrieve the alarm parameter from the URL
    var alarmParam = getUrlParameter('alarm');

    if (alarmParam !== '') {
      if (alarms.includes(alarmParam)) { addToSelected(alarmParam); }
    }
    DisplayAlarmTable(alarms);

    // if alarms not empty
    if (selected_alarms.length > 0) {
      buttonChartEventTypeDistribution();
    }
  });
}

function PseudoSearchTable() {
  console.log("PseudoSearchTable");

  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("alarm-to-be-searched");
  filter = input.value.toUpperCase();

  table = document.getElementById("table_alarms_to_select");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1];
      if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) { tr[i].style.display = ""; } 
          else { tr[i].style.display = "none"; }
      }
  }

  buttonChartEventTypeDistribution();
}

function DisplaySelectedTable() {
  console.log("DisplaySelectedTable");

  // create a table with max height that is shown
  var html_string = '';
  html_string += '<table style="font-size:20px;">';
  
  for (var i = 0; i < selected_alarms.length; i++) {
    var alarm = selected_alarms[i];
    html_string += '<tr>';
    html_string += '<td><button class="button-minus-alarmHelper" onclick="removeFromSelected(' + JSON.stringify(alarm).replace(/"/g, '&quot;') + ')"><img src="../images/alarmHelper/minus.png" alt="deselect_button"></button></td>';
    html_string += '<td>' + alarm + '</td>';
    html_string += '</tr>';
  }
  html_string += '</table>';
  $("#table_alarms_selected").html(html_string);

  PseudoSearchTable();
}

function CreateLegend(){
  console.log("CreateLegend");
  
  html_string = '<table style="min-width: 300px; max-width: 500px; margin:10px; font-size:20px;">';

  html_string += '<tr style="background-color: DarkOrange; color:white; padding:5px; height: 20px;">';
  html_string += '<td><div style="display: flex; align-items: center;">'; // Use flexbox for layout
  html_string += '<div style="flex: 0 0 15px; padding: 0 5px;"><img src="../images/alarmHelper/error_icon.png" width="20" alt="icon_button"></div>';
  html_string += '<span style="font-size: 15px; font-weight: bold;"> Error </span></div></td>';
  html_string += '</tr>';

  html_string += '<tr style="background-color: DodgerBlue; color:white; padding:5px; height: 20px;">';
  html_string += '<td><div style="display: flex; align-items: center;">'; // Use flexbox for layout
  html_string += '<div style="flex: 0 0 15px; padding: 0 5px;"><img src="../images/alarmHelper/intervention_icon.png" width="20" alt="icon_button"></div>';
  html_string += '<span style="font-size: 15px; font-weight: bold;"> Intervention </span></div></td>';
  html_string += '</tr>';

  html_string += '<tr style="background-color: DarkGray; color:white; padding:5px; height: 20px;">';
  html_string += '<td><div style="display: flex; align-items: center;">'; // Use flexbox for layout
  html_string += '<div style="flex: 0 0 15px; padding: 0 5px;"><img src="../images/alarmHelper/unknown_icon.png" width="20" alt="icon_button"></div>';
  html_string += '<span style="font-size: 15px; font-weight: bold;"> Unknown </span></div></td>';
  html_string += '</tr>';

  html_string += '</table>';


  $("#alarmStats-legend").html(html_string);
}


function addToSelected(alarm) {
  selected_alarms = selected_alarms.filter(item => item !== alarm);
  selected_alarms.push(alarm);

  alarms = alarms.filter(item => item !== alarm);

  DisplayAlarmTable();
  DisplaySelectedTable();
}

function removeFromSelected(alarm) { 
  selected_alarms = selected_alarms.filter(item => item !== alarm);

  alarms = alarms.filter(item => item !== alarm);
  alarms.push(alarm);
  alarms.sort();

  DisplayAlarmTable();
  DisplaySelectedTable();
}

function buttonSortSelected() {
  console.log("buttonSortSelected");
  selected_alarms.sort();
  DisplaySelectedTable();
}


// * * * * *
// events
// * * * * *


function CollapseAllEvents(){
  // collapse all events apart from their title bar
  for (let key in events_dict) { ToggleEventVisibility(key); }

  // change the image of the button
  $buttonImage = $('#image-collapseAll-alarmHelper');
  if ($buttonImage.attr('src') === "../images/alarmHelper/collapseAll.png") {
    $buttonImage.attr('src', "../images/alarmHelper/expandAll.png");
  } else {
    $buttonImage.attr('src', "../images/alarmHelper/collapseAll.png");
  }
}



// Function to parse the URL query parameters
function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}