/**
 * Simulator editor UI tools
 * Ignacio.Asensi@cern.ch
 * October 2016
 */

/*
function getDBElements(param){
	//get attributes: get type, 
	var type="";
	
	function paintBoxes(){
		
		var top=130;
		var left=100;
		for (var i in boxes){
			
			//left=((left/1000)>=1)?left+100:(100;top+100);
			
			boxes[i].top=top;
			boxes[i].left=left;
			createBoxFromDBObject(boxes[i]);
			if((left/1000)<=1){
				left+=100;
			}else{
				left=100;
				top+=100;
			}
			
		}
	}
	function parseForBoxes(jsonforboxes){
		var counter=0;
		for(var index in jsonforboxes){
			
			if(index==="EOD2_15A"){//if(index.substring(0, 4)==="EOD2"){//if(index.charAt(0)==="E"){
				counter+=10;
				box= new Object();
				box.power=new Object();
				box.links=new Object();
				box.name=index;
				box.type="";
				box.top=0;
				box.left=0;
				box.height=0;
				box.width=0;
				
				var obj = jsonforboxes[index];
				
				
				
				if("State" in obj){
					box.sysstate=obj["State"];
				}
				if("Switch" in obj){
					box.sysswitch=obj["Switch"];
				}
				$.each(box.power, function(key, value){
					console.log("key: "+key+".value: "+value);
					box.powers[value]=value;
				});
				//box.top=0+counter;
				//box.left=0+counter;
				box.height=60;
				box.width=80;
				box.color="#2322B4";
				boxes[box.name]=box;
				
			
			}
		}
		console.log("Boxes: "+counter);
		paintBoxes();
	}
	
	
	function show(whatever){
		paint(whatever);
	}
	function getAttributes(item){
		type=item["Result"][uid];
		loopingStates(parseForBoxes);
	}
	
	GetObject(parseForBoxes);
	
	
	
}*/
function getDBElements(param){
	//get attributes: get type, 
	var type="";
	
	function paintBoxes(){
		var top=130;
		var left=100;
		for (var i in boxes){
			if(boxes[i].isparent===true){	
				boxes[i].top=top;
				boxes[i].left=left;
				createBoxFromDBObject(boxes[i]);
				if((left/1000)<=1){
					left+=100;
				}else{
					left=100;
					top+=100;
				}
			}
		}
		for (var i in boxes){
			if(boxes[i].isparent===false){	
				boxes[i].top=top;
				boxes[i].left=left;
				createBoxFromDBObject(boxes[i]);
				if((left/1000)<=1){
					left+=100;
				}else{
					left=100;
					top+=100;
				}
			}
		}
	}
	function parseForBoxes(jsonforboxes){
		var counter=0;
		for(var index in jsonforboxes){
			
			if(index==="EOD2_15A"){//if(index.substring(0, 4)==="EOD2"){//if(index.charAt(0)==="E"){
				counter+=10;
				box= new Object();
				box.powers=new Object();
				box.links=new Object();
				box.name=index;
				box.type="";
				box.top=0;
				box.left=0;
				box.height=0;
				box.width=0;
				
				var obj = jsonforboxes[index];
				
				
				
				if("State" in obj){
					box.sysstate=obj["State"];
				}
				if("Switch" in obj){
					box.sysswitch=obj["Switch"];
				}
				box.isparent=false;
				$.each(jsonforboxes[index]["powers"], function(key, value){
					console.log("key: "+key+".value: "+value);
					box.powers[value]=value;
					box.links[value]=value;
					box.isparent=true;
					var childbox=new Object();
					childbox.name=value;
					childbox.parent=index;
					childbox.isparent=false;
					childbox.links={};
					childbox.height=60;
					childbox.width=100;
					boxes[childbox.name]=childbox;
				});

				box.height=60;
				box.width=80;
				box.color="#2322B4";
				boxes[box.name]=box;
				
			
			}
		}
		console.log("Boxes: "+counter);
		paintBoxes();
	}
	
	
	function show(whatever){
		paint(whatever);
	}
	function getAttributes(item){
		type=item["Result"][uid];
		loopingStates(parseForBoxes);
	}
	
	GetCurrentStateFull(parseForBoxes);
	
	
	
}