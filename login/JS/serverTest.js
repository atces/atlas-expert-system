/**
 * server test interface
 * maybe one day it is useful
 * Carlos.Solans@cern.ch
 **/

function loadServerTest(elem){
  
  s='';
  s+='<hr>';
	s+='<h3>Server Test</h3>';
	s+='<div><label>Command sequence</label></div>';
	s+='<div><textarea id="input" style="height:200px;width:80%;" ></textarea></div>';
	s+='<div><label>Reply</label></div>';
	s+='<div><textarea id="output" style="height:200px;width:80%;"></textarea></div>';
	s+='<div><label>Repeat</label><input type="text" id="repeat" name="repeat" value="5"/>';
	s+='<label>Progress</label><input type="text" id="progress" name="progress" value=""/></div>';
	s+='<button onclick="document.getElementById('output').value=''">Clear</button>';
	s+='<button onclick="req()">Start</button>';
			
  elem.innerHTML=s;
  
}

function httpRequest(url){
      request = new XMLHttpRequest();
  	request.open("GET", url, false);
  	request.onreadystatechange = function(){
          if(this.readyState == 4 && this.status == 200) {
    	    t1=performance.now();
    	    document.getElementById("output").value+=url+" " + Math.round(t1 - this.t0) + " ms\n";
	        //console.log(url+" " + (t1 - t0) + " milliseconds.");
	        //console.log(this.responseText);
        }
  	}
  	//console.log("send: "+url);
  	request.t0=performance.now();
	request.send(null);
  }
  
function req(){
	console.log("start")
	var iteration=0;
	var step=0;
	var repeat=parseInt($('#repeat').val());
	var tmp = $('#input').val().split("\n");
	var cmds = [];
	for(k in tmp){
		cmd=tmp[k];
		if(String(cmd).indexOf("http")==-1){continue;}
		cmds.push(cmd);
	}
	while (iteration < repeat ){
		var cmds = $('#input').val().split("\n");
		for(k in cmds){
			cmd=cmds[k]
			if(String(cmd).indexOf("TokenId")==-1){cmd+="&TokenId="+tokenid;}
			httpRequest(cmd);
			step++;
			document.getElementById("progress").value=step + " of " + repeat*cmds.length ;
		};
    		iteration++;
	}
  }
