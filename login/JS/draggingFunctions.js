/**
 * 
 * Ignacio.Asensi@cern.ch
 * Nov 2016
 * 
 * Functions for dragging elements
 * 
 *  
 * */

/**
 * addListenersPreview
 * adds eventlistener
 */
function addListenersPreview(){
    document.getElementById('current_previewbox_cursor_div').addEventListener('mousedown', mouseDown, false);
    window.addEventListener('mouseup', mouseUp, false);

}


/**
 * moves div up
 */
function mouseUp()
{
    window.removeEventListener('mousemove', divMovePreview, true);
}

/**
 * moves div down
 */
function mouseDown(e){
  window.addEventListener('mousemove', divMovePreview, true);
}

/**
 * moves div
 */
function divMovePreview(e){
  var div = document.getElementById('current_previewbox_div');
  div.style.position = 'absolute';
  div.style.top = e.clientY + 'px';
  div.style.left = e.clientX + 'px';
  
  updatePreviewSizePosition();
  
}




function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

