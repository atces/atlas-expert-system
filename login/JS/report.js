/*
ignacio.asensi@cern.ch
Aug 2018
Functions for reports and simulation tools
Requires process_table from ui.js
Requires GetSimulationJSON from db.js
*/

var dsslogfile="";

function specificPageFunctions(){
	clean_session= (n_affected_sys==0 && n_affected_al ==0)
	toggleButtons();
}

// report page
function loadReport(){
    loading("start");
    loadIntervenedSystems();
    loadAffectedSystems();
    loading("stop");

}

/*
simulation evaluator check buttons avaiability

*/
function toggleButtons(){
	console.log("toogleing buttons");

		if (clean_session){
			$("button#download_simulation").prop("disabled",true);
			$("button#save_simulation").prop("disabled",true);
			$("#file1").prop("disabled",false);
		}else{
			$("#file1").prop("disabled",true);
			$("button#download_simulation").prop("disabled",false);
			$("button#save_simulation").prop("disabled",false);
		}

}

//simulator evaluator page
function loadSimulator_evaluatorPage(){
    


    loading("start");
    process_table("report_intervened_file");
    process_table("report_affected_current");
    process_table("report_affected_file");
    process_table("report_intervened_current");
    loading("stop");

}
/**

Load all the functions to be executed on the page
*/
function initialize_simulator_evalutatorPage(){
    


    loading("start");
    process_table("report_intervened_file");
    process_table("report_affected_current");
    process_table("report_affected_file");
    process_table("report_intervened_current");
    //loading("stop");
    

}
/**
 * loadSaved_simulations_table
 * call php template to get saved simulations and return the tbody
 * on callback process table for tablesorter pluggin
 */
function loadSaved_simulations_table(){
	loading("start");
	$.ajax({
		    url: 'templates/table_saved_simulations.php',
		        data: {},
		        type: 'post',
		        success: function(data) {
			$("#saved_simulations_tbody").html(data);
			    process_table("saved_simulations");
			    loading("stop");
		    }
		});
}



function loadIntervenedSystemsNoLocation(table_s="report_intervened"){
    function parseGetIntervenedSystemsNoLocation(json){
	console.log(json);
	if (json.Reply==="Error"){
	    console.log("Error");
	}else{
	    $.ajax({
		    url: 'templates/report_table_intervened_NO_LOCATION.php',
		        data: {json:json["commands"]},
		        type: 'post',
		        success: function(data) {
			$("#"+table_s+"_tbody").html(data);
			    process_table(table_s);
			    loading("stop");
			}
		});
	}
    }
    GetCommandsHistory(parseGetIntervenedSystemsNoLocation);
}

function loadIntervenedSystems(table_s="report_intervened"){
    function parseGetIntervenedSystemsWithLocation(json){
	console.log(json);
	if (json.Reply==="Error"){
	    console.log("Error");
	}else{
	    $.ajax({
		    url: 'templates/report_table_intervened.php',
		        data: {json:json},
		        type: 'post',
		        success: function(data) {
			$("#"+table_s+"_tbody").html(data);
			    process_table(table_s);
			    loading("stop");
		    }
		});
	}
    }
    GetIntervenedSystemsWithLocation(parseGetIntervenedSystemsWithLocation);
}


function loadAffectedSystems(table_s="report_affected"){
    function parseGetAffectedSystemsWithLocation(json){
	//console.log(json);
	//console.log(json);
	if (json.Reply==="Error"){
	    console.log("Error");
	}else{
	    $.ajax({
		    url: 'templates/report_table_intervened.php',
		        data: {json:json},
		        type: 'post',
		        success: function(data) {
			$("#"+table_s+"_tbody").html(data);
			    process_table(table_s);
			    loading("stop");
		    }
		});
	}
    }
    GetAffectedSystemsWithLocation(parseGetAffectedSystemsWithLocation);
}



function DownloadCurrentSimulation(){
	/**
	 * downloadObjectAsJson
	 * Download simulation file to client
	 */

	function downloadObjectAsJson(exportObj, exportName){
	    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj, null , "\t"));
	    var downloadAnchorNode = document.createElement('a');
	    downloadAnchorNode.setAttribute("href",     dataStr);
	    downloadAnchorNode.setAttribute("download", exportName + ".json");
	    document.body.appendChild(downloadAnchorNode); // required for firefox
	    downloadAnchorNode.click();
	    downloadAnchorNode.remove();
	  }
	/**
	 * triggerDownloadSimulationFile
	 * With json object from server, trigger download to client
	 */
	function triggerDownloadSimulationFile(json){
		if ("Error" ==json["Result"]){
			console.log("Error");
		}else{
			//commands=json["Result"][0]
			//affected_sys=json["Result"][1]
			var today_d  = new Date();
			today=today_d.toLocaleDateString("en-US");
			saveName = today + '/' + today_d.getHours() + '_' + today_d.getMinutes() + '_' + today_d.getSeconds();
			json["Result"]["date"]=today;
			downloadObjectAsJson(json["Result"], saveName);

		}

	}
	
	/**
	 * GetSimulationJSON
	 * SaveCurrentState
	 * get json object from server
	 */
	 if(!clean_session){
		GetSimulationJSON(triggerDownloadSimulationFile);
	}else{
		$("#upload_output").html("<div><b>Error: No simulation to download</b></div>");
	}
}
function RunSimulationRemote(name){

	// 	get simulation
	
	loading("start");
	$.getJSON( 'data/simulations/'+name, function( json ) {
	    console.log( "JSON Data received, name is " + json);
	    json["affected"]="";
	    json_simulation=JSON.stringify(json)
	    CheckSimulationJSON(json_simulation, after_simulation_sent)
	});
}

function DownloadSimulationFile(filename){
    var downloadAnchorNode = document.createElement("a");
    downloadAnchorNode.setAttribute("href", "data/simulations/"+filename);
    downloadAnchorNode.setAttribute("download", filename);
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

function SaveSimulationFile(){

	function triggerSaveSimulationFile(json){
		if ("Error" ==json["Result"]){
			console.log("Error");
		}else{
			
			$.ajax({
			    url: 'scripts/file_helper.php',
			        data: {cmd:"write", content: json["Result"], path:"../data/simulations/", name:sim_name, type:"json"},
			        type: 'post',
			        success: function(data) {
			        	$("#simulation_name_input").val("");
			        	loadSaved_simulations_table();
			    }
			});

		}
	}
    
	 var sim_name=$("#simulation_name_input").val();
	 if (sim_name == ""){
	 	$("#simulation_output").html("<div><b>Error: No name selected</b></div>");
    return;
	 }
   
	 GetSimulationJSON(triggerSaveSimulationFile);
	   
}


function UploadSimulationFile(event) {
  var selectedFile = event.target.files[0];
  console.log(event.target.files[0]);
  var reader = new FileReader();

  var result = document.getElementById("simulation_content");

  reader.onload = function(event) {
    result.innerHTML = event.target.result;
  };

  reader.readAsText(selectedFile);
}

function UploadSimulation(){
  
  console.log("Upload simulation");
     
  var sim_name=$("#simulation_name_input").val();
  if (sim_name == ""){
 	  $("#simulation_output").html("<div><b>Error: No name selected</b></div>");
    return;
  }
  
  console.log($("#simulation_file_input"));
  
  //var sim_file=$("#simulation_file_input").files[0];
  var sim_file=document.getElementById("simulation_file_input").files[0];
  console.log(sim_file);
  
  if (sim_file == ""){
 	  $("#simulation_output").html("<div><b>Error: No file selected</b></div>");
 	  return;
  }
  
  var reader = new FileReader();
  
  reader.onload = function(event) {
    var json=JSON.parse(event.target.result);
    console.log(json);
    
    $.ajax({
		       url: 'scripts/file_helper.php',
			     data: {cmd:"write", content: json, path:"../data/simulations/", name:sim_name, type:"json"},
			     type: 'post',
			     success: function(data) {
			       $("#simulation_name_input").val("");
             $("#simulation_file_input").val("");
			       loadSaved_simulations_table();
             console.log("UploadSimulation done!")   
			    }
		});
  };
  
  reader.readAsText(sim_file);
  console.log("Uploading file: "+sim_file); 
  
}


	/**
	 * CODE TO UPLOAD SIMULATION AND TRIGGER IT
	 */
/*
* getFile
* Triggered when file is uploaded
* callback to parseFileContent 
*/
function getFile(event) {
	var _content="";

	function parseFileContent(target, file) {
		function readFileContent(file) {
			const reader = new FileReader()
		  	return new Promise((resolve, reject) => {
		    	reader.onload = event => resolve(event.target.result)
		    	reader.onerror = error => reject(error)
		    	reader.readAsText(file)
		  })
		}

		

		readFileContent(file).then(content => {
	  		//if want to see 
	  		//target.value = content
	  		_content=JSON.parse(content);
	  		json_simulation=JSON.stringify(content)
	  		parseUploadedFileContentToTables(JSON.parse(content));
	  		CheckSimulationJSON(content, after_simulation_sent)
	  	}).catch(error => console.log(error))
	}
	
	if(n_affected_sys!=0 || n_affected_al !=0){
		$("#upload_output").html("<div><b>Error: Simulation can only be checked in clear session. Please reset session</b></div>");
	}else{
		loading("strat");
		const input = event.target
		if ('files' in input && input.files.length > 0) {
			  parseFileContent(document.getElementById('upload_output'),input.files[0])
		  }

	}

	
}
/*
* simulation_sent
* callback function executed after uploaded simulation is done
* has to trigger tables render
*/
function after_simulation_sent(_content){
	updateBarIcons();
	/*
	_date=_content["date"];
	$("#upload_output").html("<div>Simulation from "+_date+" uploaded</div><div>Simulation applied</div>");
	console.log(_content);
	loadAffectedSystems("report_affected_current")
	loadIntervenedSystemsNoLocation("report_intervened_current");
	toggleButtons();
	*/
	loading("stop");
}



function parseUploadedFileContentToTables(c){
	console.log("content:");
	console.log(c);
	f_commands=c["commands"];
	console.log("f_commands")
	console.log(f_commands)
	$.ajax({
	    url: 'templates/report_table_intervened_NO_LOCATION.php',
	        data: {json:f_commands},
	        type: 'post',
	        success: function(data) {
			$("#report_intervened_file_tbody").html(data);
		    process_table("report_intervened_file");
		    loading("stop");
	    }
		});
	
	
	json_wrapper={}
	json_wrapper["Result"]=c["affected"]
	console.log(json_wrapper);
	$.ajax({
	    url: 'templates/report_table_intervened.php',
	        data: {json:json_wrapper},
	        type: 'post',
	        success: function(data) {
				$("#report_affected_file_tbody").html(data);
		    	process_table("report_affected_file");
		    	loading("stop");
	    	}
		});


	}

/*
DSSLogFileUpload 
for the tool to compare DSS log file to Expert System simulation
*/
function DSSLogFileUpload(){
	loading("start");
	if (jsdebug.report) console.log(dsslogfile);
	//get file
	var lines = dsslogfile.split('\n');   
	//get what to trigger
	var trigger=$("#LogFileSelect option:selected").val();
	field="None";
	switch(trigger){
		case "Alarm":
			tfield="ALARM_CAME"; 
			tprefix="AL_";
			break;
		case "Action":
			tfield="ACTION_CAME";
			tprefix="O_";
			break;
		case "DigitalInput":
			tfield="DIGITAL_TRUE_CAME";
			tprefix="DI_";
			break;
		default: console.log("DSSLogFileUpload: Unknown trigger subject!");return 0;
	}
	names=new Array();
	if (jsdebug.report) console.log("Searching for "+trigger);
	//get objects from file
	for (var n = 0; n < lines.length; n++) {
	  line=lines[n]
	  if (line.includes(tfield)){
	  	parts=line.split(" ");
	  	for (var p = 0; p < parts.length; p++) {
	  		if (parts[p].startsWith(tprefix)) names.push(parts[p]);
	  	}
	  }
	}

	namesjson=JSON.stringify(names);
	if (jsdebug.report) console.log("Requesting server for "+names);
	$("#LogFileDebug").html(names);
	$.ajax({
	    url: 'templates/table_LogFileDebug.php',
	        data: {json:names},
	        type: 'post',
	        success: function(data) {
			$("#LogFileDebug").html(data);
		    process_table("LogFileDebug");
	    }
		});
	ChangeBoolJSONAttribute(namesjson, "switch", "off", updateBarIconsWithLoadingStop);
}

/*
getDSSLogFile 
Part of the tool to compare DSS log file to Expert System simulation
Get the file 
Parse it, store in global var dsslogfile
*/
function getDSSLogFile(event) {
	var _content="";

	function parseFileContent(target, file) {
		function readFileContent(file) {
			const reader = new FileReader()
		  	return new Promise((resolve, reject) => {
		    	reader.onload = event => resolve(event.target.result)
		    	reader.onerror = error => reject(error)
		    	reader.readAsText(file)
		  })
		}

		

		readFileContent(file).then(content => {
	  		//if want to see 
	  		//target.value = content
	  		//console.log(content);
	  		//_content=JSON.parse(content);
	  		//json_simulation=JSON.stringify(content)
	  		//parseUploadedFileContentToTables(JSON.parse(content));
	  		//CheckSimulationJSON(content, after_simulation_sent)
	  		dsslogfile=content;
	  	}).catch(error => console.log(error))
	}
	
	if(n_affected_sys!=0 || n_affected_al !=0){
		$("#LogFileDebug").html("<div><b>Error: Simulation can only be checked in clear session. Please reset session</b></div>");
	}else{
		loading("strat");
		const input = event.target
		if ('files' in input && input.files.length > 0) {
			  parseFileContent(document.getElementById('upload_output'),input.files[0])
		  }

	}

	
}
