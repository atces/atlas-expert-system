/**
 * 
 * Ignacio.Asensi@cern.ch 
 * October 2016	 
 * Require functions.js
 * **/	

	
/**
 General purpose search function to find objects
 parameters
 uid: name of the object
 type: class  
 output: jquery selector for the id of the container for the output. Requieres the '#'
 */	
var activeAlarms=0;
var stored_triggeredAlarms;
var hist= new Array();//history.js 

/*general*/	

/**
 * Orders the attributes based on the order specified in attribute_order.json
 * This requires manual update of the attribute_order.json (who is going to maintain it?)
 * @param {*} obj 
 * @param {*} callback 
 */
function orderAttributes(obj,callback) {
	var attr_order;
	var req = $.getJSON("data/attribute_order.json", function(json) {
		console.log("----------------Ordering description attributes ----------------------------------------------------------------------------------------------------------------------------------------------------------------");
		attr_order = json;
		ordered_actions = {}
		for(var i=0; i<Object.keys(obj).length-1; i++) {
			attr_name = attr_order[(i+1).toString()]
			if(obj[attr_name] != undefined) {
				ordered_actions[attr_name] = obj[attr_name]
			}
		}
		for(a in obj) {
			ordered_actions[a] = obj[a];
		}
		callback(ordered_actions)
	});
	req.fail(function(){
		console.log("orderAttribute failed");
		oa={}
		for(a in obj) {
			oa[a] = obj[a];
		}
		callback(oa);
	})
}


function search(uid, output){
	if(output=="#page2") $('#page3').html("");
	loading("start");
	isAdmin();
	console.log("Start searchTools::search uid:"+uid+", output:"+output+", (lang:"+window.lang+")");
	if (window.mobilecheck()){
		if(output=="#page2") showPage2();
		if(output=="#page3") showPage3();
	}
	uid=uid.replace(/\s+/g, '');
	$('#debug').html("");
	GetNoClassObject(uid, //actions
		function(json){
			//Actions contains the attributes of the object
			actions=json;
			if(json["Reply"]==="Error"){
				$('#debug').html("Server error");
				console.log("Server is responding with an error message: "+json);
    			loading("stop");
				return;
			} 

			orderAttributes(actions, function(ordered_actions) {
				ordered_actions["uid"]=uid;
				if(actions["Class"]==="alarm"){
					if(uid in stored_triggeredAlarms){
						ordered_actions["triggered"]=true;
					} else{
						ordered_actions["triggered"]=false;
					}	
				}
				
				ordered_actions["page"]=output;
				ordered_actions["type"]=actions["Class"];
				if (jsdebug.high) console.log(ordered_actions);
				getSystemClasses();
				if(window.sessionStorage["system_classes"]!=undefined){system_classes=window.sessionStorage["system_classes"];}
				$.ajax({ url: 'templates/description.php',
					data: {content: ordered_actions, system_classes:JSON.parse(system_classes),lang:window.lang},
					type: 'POST',
					success: function(data) {
						var add=true;
						for( var i = 0, len = hist.length; i < len; i++ ) {
							if( hist[i][0] === uid ) {
								add=false;
								break;
							}
						}
						if (add && output==="#page3") hist.push([uid, ordered_actions["type"]]);
						loading("stop");    
						$(output).css("display", "initial");
						$(output).html(data);
						LoadJSONCurrentState(); //this triggeres loading the current state of systems
					}
				});
			});
		}			
	);
}

function parseSearch(json){
	/*
	if(json.hasOwnProperty("error")){
		GetToken(function(){FindObject("AL&type=Alarm",parse);});
	return;
	}
	*/
	var tablename=document.getElementById("searchTools");
	tablename=tablename.getAttribute('table');
	var ar_alarms=new Array;
	var s="";
	for (k in json["Result"]){
		ar_alarms.push(k);
		s+='<tr><td onclick="search(\''+k+'\', \'#page2\');"><a class="cursor">'+k+'</a><div id="'+k+'"></div></td><td><span class="'+k+' alarmLed ledRed"></span></td></tr>';    
	}

	document.getElementById(tablename+"_tbody").innerHTML=s;
	$("#"+tablename+"_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	loading("stop");
	
	/*
	//check URL for vars
	if(QueryString[""]!=="undefined"){//if there is var
		function refineUrl()
		{
		    var url = window.location.href;
		    var value = url.substring(url.lastIndexOf('/') + 1);
		    value  = value.split("?")[0];   
		    return "atlas-expert-system/login/"+value;     
		}
		console.log("There is var!"+QueryString["uid"]);
		var newUrl = refineUrl();//fetch new url
		window.history.pushState("atlas-expert-system/login/dssalarms.php", "Title", "/"+newUrl );
		
		setTimeout(function(){search(QueryString["uid"], 'alarm', '#page2');},500); 
		
	}
	*/
	GetTriggeredAlarms(parseTriggeredAlarms);
	
}
function closePage4(){
	$('#page4').css("display", "none");

}


function scrollTable(rowname){
	$("a:contains('"+rowname+"')").css( "background-color", "red" );
	$("a:contains('"+rowname+"')").delay(8000).css( "background-color", "blue" );
	var elem = document.getElementById(rowname);
	elem.scrollIntoView({block: "end", behavior: "smooth"}); 
	
}


function getVerboseSchema(id){
	//don't use JQUERY pointers here because the dots in racks in confused to the element class
	//var b=document.getElementById('get_object_schema_'+id);
	if(document.getElementById('get_object_schema_'+id).innerHTML==="Display schema"){
		document.getElementById('get_object_schema_'+id).innerHTML="Hide schema";
		function dumpJson(json){
		$.ajax({ url: 'templates/page2verbose.php',//'+output.replace('#','')+'
			         data: {content:json},
			         type: 'post',
			         success: function(data) {
			        	 document.getElementById('verbose_schema_'+id).innerHTML=data;
	                  
	                  loading("stop");
	              }
				});
		
		}
		loading("start");
		id_nopage=id.replace("page2_","").replace("page3_","");
		GetSchema(id_nopage, dumpJson);
	}else{
		document.getElementById('get_object_schema_'+id).innerHTML="Display schema";
		document.getElementById('verbose_schema_'+id).innerHTML="";
	}
}

function getVerboseData(id){
	elementESC="."+id.replace(/\./g,"\\\.");//elementESC=".Y\\.55-25\\.A2";
	if(document.getElementById('get_object_data_'+id).innerHTML==="Display data"){
		document.getElementById('get_object_data_'+id).innerHTML="Hide data";
		loading("start");
		document.getElementById('verbose_data_'+id).removeAttribute("hidden");
		loading("stop");
	}else{
		document.getElementById('get_object_data_'+id).innerHTML="Display data";
		document.getElementById('verbose_data_'+id).setAttribute("hidden", "true");
	}
}

function searchToNewTab(element){
	//var query=$(element).text();
	window.open("search.php?query="+element);
}

//debug function to print json content



function editDelayedActions(){
	$('.edit_toggle').toggle();
	getDelayedActions();	
}

function getDelayedActions(){
	FindObject("O&type=DelayedAction",fillDelayedActionsSelect);
}

function fillDelayedActionsSelect(delayedactions){
	actionshtml="";
	$.each(delayedactions["Result"], function(i,obj){
		actionshtml+="<tr><td>"+obj+"</td><td><button onClick='addDelayedAction()'>Add</button></td></tr>";
		
	});
	$('#delayedactions_tbody').html(actionshtml);
	$("table").trigger("update").trigger("appendCache").trigger("applyWidgets");
}

function parsetriggerAlarm(text){
		loading("stop");
		LoadJSONCurrentState();
		
	}
	
function triggerAlarm(alarm_id){
	
	loading("start");
	var alarm_status=$('#trigger_alarm').text();
	if(alarm_status==="Trigger alarm"){
		TriggerAlarm(alarm_id, parsetriggerAlarm);
		$('#trigger_alarm').text("Stop alarm");
	}else{
		UnTriggerAlarm(alarm_id, parsetriggerAlarm);
		$('#trigger_alarm').text("Trigger alarm");
	}
	setTimeout(function(){ GetTriggeredAlarms(parseTriggeredAlarms); }, 200);
		
}

function magnetTriggerAlarm(alarm_id,button_id){
	console.log("Entered magnetTriggeredAlarm");
	GetTriggeredAlarms(parseTriggeredAlarms);
	loadingIcon(500);
	value="yes";
	//GetNoClassObject(alarm_id, checkForValue(parse, value));
	var alarm_status=button_id;
 	console.log("Alarm=", alarm_status);
	GetNoClassObject(alarm_id, //actions
		function(json){
			actions=json;
			if(json["Reply"]==="Error"){
				$('#debug').html("Server error");
				return;
			} 
	
			actions["uid"]=alarm_id;
			if(actions["Class"]==="Alarm"){
				console.log("class=alarm");
				if(actions["triggered"]==="no"){
					console.log("false");
					console.log(actions["triggered"]);
					TriggerAlarm(alarm_id, output);
					alarm_status.style.backgroundColor = "red";
				} else{
					console.log(actions["triggered"]);
					UnTriggerAlarm(alarm_id, output);
					alarm_status.style.backgroundColor = "transparent";
				}	
			}
			});
	setTimeout(function(){ GetTriggeredAlarms(parseTriggeredAlarms); }, 200);
}



function checkAlarm(alarm_id, button_id){
	console.log("checkAlarm");
	GetNoClassObject(alarm_id, //actions
	function(json){
		actions=json;
		if(json["Reply"]==="Error"){
			$('#debug').html("Server error");
			return;
		} 
		
		actions["uid"]=alarm_id;
		if(actions["Class"]==="Alarm"){
			console.log("class=alarm");
			if(actions["triggered"]==="no"){
				console.log("false");
				console.log(actions["triggered"]);
				button_id.style.backgroundColor = "transparent";
			} else{
				console.log(actions["triggered"]);
				button_id.style.backgroundColor = "red";
			}	
		}
	});
}

function AdvancedSearchDefault(){
	$('#ad_search_toggle').prop('checked', false);
}
function AdvancedSearchToggle(){
	$('#toolsbar2').toggle();
}

function AdvancedSearch(){
	var type= $('#ad_search_type').val();
		$('#tableFunctions_search').attr('table', 'search')
		var e="<script id='tableFunctions_search' class='tableFunctions' src='JS/tableFunctions.js' table='search'></script>";
		$('head').append(e);
	
	function parseFullTable(results){
		
		$.ajax({ url: 'templates/advancedSearch.php',//'+output.replace('#','')+'
	         data: {content:results},
	         type: 'post',
	         success: function(data) {
	        	 $("#ad_search_full_wrapper").html(data);
	        	 //$("#ad_search_full_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	        	 process_table("advanced_search_full");
	        	 loading("stop");
	         }
		});
	}
	loading("start");
	AdvancedSearchObjectFullTable("", "uid", type, parseFullTable);
}


/**
 * searchInputToggle 
 * Function to add/remove search buttons depending on selected type of element.
 * Depending on the type selected, search fields are added corresponding to the attributes of that type
 * @returns
 */
function searchInputToggle(){
	$('#search_center').hide();


	var type=$('#ad_search_type').val();
	AdvancedSearch();
	//$('.ad_search_attr_row').remove();
/*
	function createCorrespondingFields(result) {
		orderAttributes(result.schema, function(oa) {
			for(attr in oa) {
				tt='<tr class="ad_search_attr_row">';
				tt+='<td>' + attr + '</td>';
				tt+='<td><input type="text" size="20" id="ad_search_' + attr + '"><input id="button_description" type="button" class="search_button" value="Search" onClick="AdvancedSearch(\'' + attr + '\')"></td>';
				tt+='</tr>';
				$('#advanced_search_table').append(tt)
			}
		});
	}
	if(type != "All") {
		GetClassSchema(type, createCorrespondingFields);
	}
	*/
}


function removeDelayedActionLine(callback){
	if (jsdebug.low) console.log(callback);
}

function urlExtractor(text_string){
	var urlPattern = new RegExp(/(http?):\/\/(www\.)?[a-z0-9\.:].*?(?=\s)/);
	var a=text_string.match(urlPattern);
	if(a!==null){
		return a[0];
	}	
	return null;
}


function showPanel3(){
	if (!window.mobilecheck()){
		$('#page3').css("width","40%");
		$('#page2').css("width","58%");
	}
	$('#page3slider').html("<b>>></b>").css("border", "1px solid black");
	$('#page3slider').attr("onclick","hidePanel3()");
}
function hidePanel3(){
	$('#page3').css("width","4%");
	$('#page2').css("width","94%");
	$('#page3slider').show();
	$('#page3slider').html("<b><<</b>").css("border", "1px solid red");
	$('#page3slider').attr("onclick","showPanel3()");
}

/**
 * Gets systems properties comparing it with codification.js 
 * and sets the code to tag with template (same name as tag) 
 * @param {Object} uid
 * @param {Object} tag
 */
function getSystemProperties(uid, tag, template){
	
	loading("start");
	/*
	 * Check it is a supported name
	 * First condition is for first letter (First letter defines system type)
	 * Second condition checks rest of letters to ensure that type of system is supported
	 * E Electrical system
	 */
	var group="";
	var electrical="E";
	var UA="UA";// air handling unit
	var UI="UI";
	var properties=new Object();
	var json=new Object();
	try{
		switch(uid[0]){
		
			case (electrical):
				//Electrical system
				if (!(( /[A-Z]/.test(uid[0]) ) && ( /[A-Z]/.test(uid[1]) ) && ( /[A-Z]/.test(uid[2]) ) && ( /\d/.test(uid[3]) ))){
				$("#"+tag).html("Unknown type of electrical system");loading("stop");
				return;
				}
				group=electrical;
				var reseau="";
				if(codification["E"]["reseau"]["BT"].indexOf(uid[1])!==-1){
					reseau="BT";
				} else {reseau="HT";}
				properties.Power=codification[electrical]["reseau"][uid[1]];
				properties.Type=codification[electrical]["type"][reseau][uid[2]];
				
				break;
			case ("U"):
				if (uid[1]==="A"){
					//air handling unit
					group=UA;
					if(codification[UA][uid.slice(0,4)]!==undefined){
						properties.Type=codification[UA][uid.slice(0,4)][0];
						properties.kW=codification[UA][uid.slice(0,4)][1];
					} else {}
				}else if(uid[1]==="I"){
					//air handling unit
					group=UI;
					if(codification[UI][uid.slice(0,4)]!==undefined){
						properties.Type=codification[UI][uid.slice(0,4)][0];
					} else {}
				} 
				
				break;
			default:
				$("#"+tag).html("<b>Type:</b> None");
				loading("stop");
				return;
			
			
		}
		
		switch(group){
			case (electrical):
				console.log("geting location");
				locationcode=(uid.split('/')[1]===undefined)?uid.split('_')[1]:uid.split('/')[1]
				break;
			default:locationcode=null;break;
		}
		
		var batiment=new Object();
		if(locationcode!==null){
			batiment.number=batiments[locationcode][0];
			batiment.code=batiments[locationcode][2];
			batiment.description=batiments[locationcode][1];
		}else{batiment=null}
		$.ajax({
			url: 'templates/'+template+'.php',
			data: {batiment:batiment,properties:properties, uid:uid, group:groups[group]},
	        type: 'post',
	        success: function(data) {
		       	$("#"+$.escapeSelector(tag)).html(data);
		       	
		       	loading("stop");
		       },
		    error: function(err){
		    	console.log(err);
		     	$("#"+tag).html("Error. Object could not be retrieved: "+err.statusText);
		     	loading("stop");
		     }
			});
			
	} catch(err){
		$("#"+tag).html("Error. Object could not be retrieved: "+err);
	}
	
	
}


function showImage(thumbsrc, thumbdir) {
	if(window.mobilecheck()) {
		window.open(thumbsrc.replace("thumbnail_",""))
	}
	else {
		showImageInViewer(thumbsrc, thumbdir)
	}	
}

function showImageInViewer(thumbsrc, thumbdir) {
	src = thumbsrc.replace("thumbnail_","");
	dir = [];
	thumbdir.forEach(function(e){
		dir.push(e.replace("thumbnail_",""));
	});

	var modal = $("#img-modal");

	modal.css("display", "block");

	var span = $("#close-modal");
	span.click(function() {
		modal.css("display", "none");
		$(document).off('keyup.myevent')
	});

	var span = $("#open-full-image");
	span.click(function() {
		$("#open-full-image > a").attr("href", src);
	});

	var span = $("#previous-image");
	span.click(function() {
		pos = pos-1 >= 0 ? pos-1 : pos;
		$('#img-slot').children('#child-img').css({'background-image': 'url('+ dir[pos] +')'});
		src = dir[pos];
		updateArrows(pos, dir.length)
		updateImgSize(thumbdir[pos]);
	});

	var span = $("#next-image");
	span.click(function() {
		pos = pos+1 < dir.length ? pos+1 : pos;
		$('#img-slot').children('#child-img').css({'background-image': 'url('+ dir[pos] +')'});
		src = dir[pos];
		updateArrows(pos, dir.length)
		updateImgSize(thumbdir[pos]);
	});

	var pos = dir.indexOf(src);

	$(document).on('keyup.myevent', function(event){
		if(event.which == 39) {	//Next image
			$("#next-image")[0].click();
		} 
		if(event.which == 37) {	//Previous image
			$("#previous-image")[0].click();
		} 
		if(event.which == 27) {	//Close
			modal.css("display", "none");
			$(document).off('keyup.myevent')		
		}
		if(event.which == 13) {	//Open in new window
			$("#open-full-image > a").attr("href", src);
			$("#open-full-image > a")[0].click();
		}
	});

	$('#img-slot')
	// tile mouse actions
	.on('mouseover', function(){
	$(this).children('#child-img').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
	})
	.on('mouseout', function(){
	$(this).children('#child-img').css({'transform': 'scale(1)'});
	})
	.on('mousemove', function(e){
	$(this).children('#child-img').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
	})

	// tiles set up
	if($('#child-img').length <= 0) {
		// add a photo container
		$('#img-slot').append('<div id="child-img"></div>')
	}
	// set up a background image for each tile based on data-image attribute
	$('#child-img').css({'background-image': 'url('+ src +')'});


	updateArrows(pos, dir.length)
	updateImgSize(thumbsrc);
}

function updateImgSize(thumbsrc) {
	var thumb = $('img.photo_item[src$="' + thumbsrc + '"]');
	var image_h = thumb.height();
	var image_w = thumb.width();
	if(image_h > image_w) {
		$('#img-slot').css('height', '90vh').css('width', image_w*($('#img-slot').height()/image_h))
	}
	else {
		$('#img-slot').css('width', '80vw').css('height', image_h*($('#img-slot').width()/image_w))
	}
}

function updateArrows(pos, dir_length) {
	if(pos == dir_length-1) {
		$("#next-image").attr("hidden", "true");
	}
	else {
		$("#next-image").removeAttr("hidden");
	}
	if(pos <= 0) {
		$("#previous-image").attr("hidden", "true");
	}
	else {
		$("#previous-image").removeAttr("hidden");
	}
}
