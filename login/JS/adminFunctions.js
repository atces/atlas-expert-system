/**
 * ATLAS Expert System Team
 * Administrator functions for the website
 * 
 * Ignacio.Asensi@cern.ch
 *  
 * */


/**
 * Function to refresh administrators list
 * params - no params
 */
function refreshAdminList(){
	var items=[];
	$.ajax({
		dataType:'json',		
		url:"adminFunctions.php",
		data:{adminlist:'true'},
		type:'get',
		success:function(response){
				console.log("success");
				output=response;
				$.each(output, function(adfsLogin, adfsName){
					if(adfsLogin!=="solans"){
					items.push ('<tr><td>'+adfsLogin+ '</td><td>'+adfsName +'</td><td><button id="'+adfsLogin+'" onClick="deleteAdmin(this.id)">Delete</button></td></tr>');
					} else{
					items.push ('<tr><td>'+adfsLogin+ '</td><td>'+adfsName+'</td><td></td></tr>');	
					}				
				});
				$('#administrators_tbody').html(items.join(''));
				$("#administrators_table").trigger("update").trigger("appendCache").trigger("applyWidgets");					
			}	
	});	

}

/**
 * Function that adds logged in CERN user to admin list getting username from cernlogin (Shibolleth)
 * params - no params
 */

function addAdmin(){
		
		cernlogin=$('#newAdminLogin').val();
		
		if(cernlogin!==""){
			$.ajax({
			dataType:"json",
			url:"adminFunctions.php",
			data:{addAdmin:cernlogin, name:"undefined"	},
			type:'get',
			success:function(response){
				refreshAdminList();	
			},
			error:function(err){//firefox fails success callback but gets the job done
				refreshAdminList();		
			}
			});
		}else{
			alert("Cern login can not be empty");
		}
		//location.reload();	

}

/**
 * Function that removes logged in CERN user from admin list
 * params:
 *     adminLogin : login of the administrator to remove
 */


function deleteAdmin(adminLogin){
	$.ajax({
		url:"adminFunctions.php",
		data:{delAdminLogin:adminLogin},
		type:'get',
		success:function(response){
			refreshAdminList();		
		},
		error:function(err){//firefox fails success callback but gets the job done
			refreshAdminList();		
		}


	});
	//location.reload();
}

