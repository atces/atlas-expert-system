/*
 * ignacio.asensi@cern.ch
 * tools for navigation history bar
 * June 2017
 */

var position=2;


function displayHistoryBar(){
	if(!$('#right_bar').is(':visible')){
		$('#right_bar').show();
		var text="<input id=\"history_button_inbar\" type=\"button\" value=\"X\" onclick=\"displayHistoryBar();\"/>"
			+"<div class=\"CELL TITLE\">History</div>"
			+"<hr>";
		if(hist.length===0){
			text+="No elements visited";
		}else{
			for (var item in hist){
				if (hist.hasOwnProperty(item)){
					text+="<div class=\"history_row\"><span class='cursor link' onclick=\"search('"
						+hist[item][0]+"', '#page3'); closeHistoryBar("+item+");\">"+hist[item][0]+"</span> <button class=\"object_type_bar noselect\">"
						+hist[item][1]+"</button></div>";
				}
			}
		}
		
		$('#right_bar').html(text);
	}else{
		$('#right_bar').hide();
	}
	
}

function closeHistoryBar(clicked){
	position=(hist.length+1)-clicked;
	$('#right_bar').hide();
}

function navPage3(direction){
	if(direction=="prev"){
		if(hist[hist.length-position]!==undefined){
			search(hist[hist.length-position][0], "#page3")
			position++;
		}
	}else{
		if(hist[hist.length-(position-2)]!==undefined){
			search(hist[hist.length-(position-2)][0], "#page3")
			position--;
		}
	}
}