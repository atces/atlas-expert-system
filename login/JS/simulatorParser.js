/**
 * Parse function for simulator pages
 * parse the array of states returned by the server
 * param json dictionary 
 * require functions.js
 */
var pageElements=new Array();
var alarmElements=new Array();
//var display_subsystem=true;

var relatedPages=false;


function parseJSONCurrentState(json) {
	function switchToOFF(uid){
		$('g[id^="'+uid+'sw"] path').each(function(){this.attributes.fill.value="#ff0000";});
		//$("#"+uid+"st")[0].attributes.fill.value="#ff0000";
		$("#"+uid+"sw circle").css("transform","translate(0,"+$("#"+uid+"sw circle")[0].r.baseVal.value*9/6+"px)");
	}
	function switchToON(uid){
		$('g[id^="'+uid+'sw"] path').each(function(){this.attributes.fill.value="#14b828";});
		//$("#"+uid+"st")[0].attributes.fill.value="#14b828";
		$("#"+uid+"sw circle").css("transform","translate(0,0)");
	}
	function stateToOFF(uid){
		//$("#"+uid+"sw path")[0].attributes.fill.value="#ff0000";
		$('rect[id^="'+uid+'st"]').each(function(){this.attributes.fill.value="#ff0000";});
	}
	function stateToON(uid){
		//$("#"+uid+"sw path")[0].attributes.fill.value="#14b828";
		$('rect[id^="'+uid+'st"]').each(function(){this.attributes.fill.value="#14b828";});
	}
	
	console.warn(json);
	//if (jsdebug.parse) loading
	console.log("--------->   parseJSONCurrentState(): Parsing elements....");
	var verbose=""; 
	counter=0;
	verbose = "";
	verbose+="<table id='items_table' class='tablesorter'><thead><th>Items (<span id='affected_num'></span>)</th><th>Switch</th><th>State</th></th></thead><tbody>";
	var on="on";
	var off="off"
	var rounds=1;
	if($("#page2").length !== 0){
		if(jsdebug.parse) console.log("Parsing.... It is a search page. Two rounds of parsing");
		rounds=2;
		}else{
			if(jsdebug.parse) console.log("Parsing.... It is a simulator page. One round of parsing");
		}
	
	for (i = 1; i <= rounds; i++) {
		 
		for(var index in json){
			noverbose=false;	
			
			
			pageprefix="";
			//no pageprefix for simulator pages
			if (rounds===2){
				//prefix page2 on first round
				if (i===1){ 
					pageprefix="page2_";
					}
				//prefix page2 on second round
				else if (i===2) {
					pageprefix="page3_";
					//break loop if page3 is empty
					if ($('#page3').html()==""){
						if(jsdebug.parse) console.log("Parsing.... Page 3 is empty. One round of parse (#page2)");
						break;
					}
				}
			}
			
			//reverse state logic for alarms and actions 
			if (false ){ //} || index.startsWith("O_") ){
				on="off";
				off="on";
				noverbose=true;
			}else{
				on="on";
				off="off";
			}
			if (index=="Reply" || index==="Trace_exception" || index=="dt" || index=="Resolved"){continue;}
			//if (simulatorPage===true && (index.startsWith("AL_") || index.startsWith("O_"))){continue;}
			var obj = json[index];
			if (index.startsWith("AL")){noverbose=true;}
			if(jsdebug.parse) console.log("Parsing...."+pageprefix + index+". Switch: "+ obj["Switch"]+". State: "+obj["State"]);
			if (noverbose===false) verbose+= "<tr>";
			if ((noverbose===false) ){verbose+= "<td class=\"cursor\">"+index+" </td>";}
			if(pageElements.indexOf(index)!== -1){
				counter++;
				try{
					if("Switch" in obj){
						if (noverbose===false) verbose += "<td>" + ((obj["Switch"].toLowerCase()==="on") ? "<span class='green'>ON</span></td>" : "<span class='red'>OFF</span></td>");
						if (obj["Switch"].toLowerCase()=="off"){ 
							switchToOFF(pageprefix+index.replace(/\./g,"\\\."));
						}
						else if(obj["Switch"].toLowerCase()=="on" ){ 
							switchToON(pageprefix+index.replace(/\./g,"\\\."));
						}
					}
				}catch(err){
					if(jsdebug.parse) console.log("Error in simulatorParser.js. No \"Switch\" tag. Object: " + pageprefix + index + ": " + err)
				}
				try{
					if("State" in obj){
						if (noverbose===false) verbose += "<td>" + ((obj["State"].toLowerCase()==="on") ? "<span class='green'>ON</span></td>" : "<span class='red'>OFF</span></td>");
						if (obj["State"].toLowerCase()=="off"){
							stateToOFF(pageprefix+index.replace(/\./g,"\\\."));
						}
						else if(obj["State"].toLowerCase()=="on" ){ 
							stateToON(pageprefix+index.replace(/\./g,"\\\."));
						}
					}
				}catch(err){
					if(jsdebug.parse) console.log("Error in simulatorParser.js. No \"State\" tag. Object: " + pageprefix + index + ": " + err)
				}
				
				try{
					//if (jsdebug.parse) console.log("Description of "+element+": "+json["Result"][element]);
					if("Description" in obj){
						var objname=pageprefix+index;
						objnameESC=objname.replace(/\./g,"\\\.");//elementESC="Y\\.55-25\\.A2";
						var description=(obj.Description.length ===0)?"No description provided":obj.Description;
						description= objname+": "+ description;
						$("[data-uid="+objnameESC+"] > title").text(description);
					}
				}catch(err){
					if(jsdebug.parse) console.log("Error in simulatorParser.js. No \"Description\" tag. Object: " + pageprefix + index + ": " + err)
				}
				if ("class_" in obj){
					if (obj["class_"]=="Group" || obj["class_"]=="Rack"){
						var objname=pageprefix+index;
						objnameESC="#"+objname.replace(/\./g,"\\\.");//elementESC=".Y\\.55-25\\.A2";
						$(objnameESC+"sw").hide();
					}
				}
				
			}else{verbose+="<td></td><td></td>"}
			verbose+= "</tr>";
			}
		}
	verbose+="</table>";
	if ($('#verbose').length) document.getElementById('verbose').innerHTML =verbose;
	$('#affected_num').html(counter);
	console.log("Update number of alarms and affected systems");
	updateBarIcons(); 
	if ($('.grouploading').length) $('.grouploading').hide();
	setTime();
	$(".Loading").removeClass("Loading").show();//The wrappers are shown after load the states and removed the switches from groups and racks.
	loading("stop");  
	if (jsdebug.parse) loading("End of parsing elements....");
	process_table('items');
	//if (!window.mobilecheck()) $('#desktopmenubutton').css("width",$('#maintable').width());//adjust absolute upper bar to maintable width
}

function displayHistory(hw) {
  var url=new URL(window.location.href);
  var params=new URLSearchParams(url.search);
  params.set('query',hw);
  params.delete('page');
  //params.delete('uid');
  var url2="search.php?"+ params.toString();
  
	var win = window.open(url2, '_blank');
	if (win) {
		//Browser has allowed it to be opened
		win.focus();
	} else {
		//Browser has blocked it
		alert('Please allow popups for this website');
	}
}

/**
 * HighLightElement highlights the box of the name selected in the verbose table
 * The highlight effect is hiden with a document event listener
 * @param uid
 * @returns
 */
function HighLightElement(suid){
	var uid=suid.replace(/\./g,"\\."); //adding "\" to strings with "."
	console.log("HighLightElement:"+uid);
    if(pageElements.length==0){ 
        console.log("Element does not exist yet (wait and call later): "+uid);
        if (tries_HighLightElement<5){
        	tries_HighLightElement+=1;
        	setTimeout(function(){HighLightElement(suid)},500);
        }
        
    }else if(pageElements.length>0 && !$('#'+uid+'st').length){
	    console.log("Element problably does not exist in this page: "+uid);
        return;
    }
	if($('#'+uid+'st').length!== 0){
		if ($('#'+uid+'st').parent().css('margin-top')==="0px"){
			var top=$('#'+uid+'st').position().top;
			var left=$('#'+uid+'st').position().left;
		}else{
			var top=$('#'+uid+'st').css('margin-top');
			var left=$('#'+uid+'st').css('margin-left');
			top=parseInt(top.replace("px",""));
			left=parseInt(left.replace("px",""));
		}
		top-=80;
		left+=0;
		$('#pointing_lineY').attr({'x2':left, 'y2':top, 'x1':left});
		$('#pointing_lineX').attr({'x2':(left+10), 'y2':top, 'y1':top});
		$('#pointing_svg').show(500);
	}else{
		alert("\""+uid+"\" could not found in page.");
	}
	

}


function getUrlParam(param) {
	var url = decodeURI(window.location.href);
	var vars = {};
	if(url.indexOf("?")==-1){return null;}
	var hashes = url.split("?")[1];
	var hash = hashes.split('&');
	for (var i = 0; i < hash.length; i++) {
		params=hash[i].split("=");
		if(params[0]==param){return params[1];}
	}
	return null;
}

function trigger_highlight(){
	var url = decodeURI(window.location.href);
	var uid=getUrlParam('uid_finder');	
	if (uid == null) uid=getUrlParam('uid');
	if (uid !== null && url.indexOf("simulator.php") != -1) HighLightElement(uid);
}



function GetGroup(uid){
	function GetGroupParser(json){
		//console.log("json: "+json+". uid: "+uid);
		$.ajax({
			url: 'templates/group.php',
	        data: {json:json,
	        		uid:uid
	        },
	        type: 'post',
	        success: function(data) {
	        	group_wrapper=document.getElementById(["distribution"]);
	        	var textnode = document.createElement("div");
	        	textnode.innerHTML=data;
	        	
	       	 	group_wrapper.appendChild(textnode);
			var state_pos=$("#"+uid+"st").position();
			document.getElementById("drag_"+uid).setAttribute("style","top:"+(state_pos['top']-100)+"px;left:"+state_pos['left']+"px");
	       	 	LoadJSONCurrentState();
	       	   	dragElement(document.getElementById("drag_"+uid));	
			$(".wrapper_"+uid).addClass("Loading").hide();//Hidden for loading the state and removes the switches of the groups and racks
	        }
		});
	}
	
	if ($(".wrapper_"+uid).length){
		$(".wrapper_"+uid).toggle();
	}else{
		loading("start");
		GetNoClassObject(uid,GetGroupParser);
	}
	
}


function GetRack(uid){
	function GetRackParser(json){
		//console.log("json: "+json+". uid: "+uid);
		$.ajax({
			url: 'templates/rack.php',
	        data: {json:json,
	        		uid:uid
	        },
	        type: 'post',
	        success: function(data) {
	        	
	        	rack_wrapper=document.getElementById(["distribution"]);
	        	var textnode = document.createElement("div");
	        	textnode.innerHTML=data;
	        	
	       	 	rack_wrapper.appendChild(textnode);
			var state_pos=$("#"+uid.replace(/\./g,"\\\.")+"st").position();
			document.getElementById("drag_"+uid).setAttribute("style","top:"+(state_pos['top']-100)+"px;left:"+state_pos['left']+"px");
	       	 	LoadJSONCurrentState();
	       	   	dragElement(document.getElementById("drag_"+uid));	
			$(".wrapper_"+uid).addClass("Loading").hide();//Hidden for loading the state and removes the switches of the groups and racks
	        }
		});
	}
	
	if ($(".wrapper_"+uid).length){
		$(".wrapper_"+uid).toggle();
	}else{
		loading("start");
		GetNoClassObject(uid,GetRackParser);
	}
	
}

function dragElement(elmnt) {

	  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
	  if (document.getElementById(elmnt.id + "header")) {
	    /* if present, the header is where you move the DIV from:*/
	    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
	  } else {
	    /* otherwise, move the DIV from anywhere inside the DIV:*/
	    elmnt.onmousedown = dragMouseDown;
	  }

	  function dragMouseDown(e) {
	  	if (window.sessionStorage.dragging===undefined){
	  		window.sessionStorage.dragging=1;
	  	}else{
	  		window.sessionStorage.dragging=parseInt(window.sessionStorage.dragging)+1
	  	}
	  	elmnt.style.zIndex=window.sessionStorage.dragging;
	    e = e || window.event;
	    // get the mouse cursor position at startup:
	    pos3 = e.clientX;
	    pos4 = e.clientY;
	    document.onmouseup = closeDragElement;
	    // call a function whenever the cursor moves:
	    document.onmousemove = elementDrag;
	  }

	  function elementDrag(e) {
	    e = e || window.event;
	    // calculate the new cursor position:
	    pos1 = pos3 - e.clientX;
	    pos2 = pos4 - e.clientY;
	    pos3 = e.clientX;
	    pos4 = e.clientY;
	    // set the element's new position:
	    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
	    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
	  }

	  function closeDragElement() {
	    /* stop moving when mouse button is released:*/
	    document.onmouseup = null;
	    document.onmousemove = null;
	  }
	}

function closeGroup(uid){
	//var e = document.getElementById("drag_"+uid);
	//e.style.display="none";
	document.getElementById("drag_"+uid).remove();
}

function closeRack(uid){
	document.getElementById("drag_"+uid).remove();
}



function LoadJSONCurrentState(){
	function JSONCurrentState(){
		//if there are elements to be requested to the server
		if(pageElements.length>0){
			loading("start");  	
			console.warn(pageElements);
			elementsjson=JSON.stringify(pageElements);
			//elementsjson=pageElements;
			console.warn(elementsjson);
			GetCurrentState(elementsjson, parseJSONCurrentState);
		}
		
	}
	loadPageElements(JSONCurrentState);
}


/**
 * loadPageElements
 * iterates page nodes looking for objects that should be rendered and adds them to the var pageElements. 
 * This var will be sent to server in a JSON string
 */
function loadPageElements(callback){
	pageElements=new Array();
	treeboxes=$("g g[data-uid],g g[data-UID]");
	for ( var i = 0, l = treeboxes.length; i < l; i++ ) {
		if ("data-uid" in treeboxes[i].attributes){
			pageElements.push(treeboxes[i].attributes["data-uid"].value);
		}else{
			pageElements.push(treeboxes[i].attributes["data-UID"].value);
		}
	}
	
	
	if(isSimulatorPage()){
		pageElements=pageElements.concat(alarmElements);	
		
		//group elements
		$('.group_elements_list_row > div').each(function(){pageElements.push(this.className);});
	}else{
		$('.element_pointer').each( function (){
			pageElements.push(this.id.slice(0, -2).slice(6));
		});
	}
	if (jsdebug.parse) console.log("loadPageElements(): Elements found in page: "+pageElements);
	callback();
	
}


function ToggleAlarm(uid){
	
	DB_ToggleAlarm(uid, parseJSONCurrentState)
}

/*
 * EVENT related functions
 * */
$(document).ready(function(){
	//trigers highlight effect if passed by url parameter in simulator pages 
	trigger_highlight();
	
	/*activate onclick event for verbose tables to highlight on the image clicked elements (of the table) */
	$('#verbose').on("click", "td",function(){
	var uid=$(this).text().replace(" ","");
	HighLightElement(uid);
	});
	var svg_metadata_text=$("svg > metadata").text();
	var svg_metadata=(svg_metadata_text!="") ? JSON.parse(svg_metadata_text) : {alarms:[],actions:[]};
	
	if (isSimulatorPage()) loadSimulatorTables(document.getElementById("table2"),svg_metadata["alarms"], svg_metadata["actions"]);
	
	alarmElements=svg_metadata["alarms"].concat(svg_metadata["actions"]);
	LoadJSONCurrentState();
});

/**
 * event listener to hide highlight effect
 * it listens to any click outside #verbose table
 */
$(document).click(function(event) { 
	if(!$(event.target).closest('#verbose').length) {
		$('#pointing_svg').hide();
	}
});


/*
 * function to get a list of alarms that affect the systems of page
 * get all systems in page and get the list
 * TODO: PLEASE CHECK IT SEEMS THAT IS NOT WORKING
 */
function getAlarmsAffectingPage(){

		/**
		 *  parse_dashboard_affeted_systems parses affected systems using a php template
	 	 * @param {Object} jso n
		 */
		function parsegetAlarmsAffectingPage(json){
			if (json.Reply=="Error"){
				console.log("parsegetAlarmsAffectingPage():Error. Server response error");
			}else{
				$.ajax({
					url: 'templates/table_alarms_affecting_1.php',
			        data: {json:json['Result']},
			        type: 'post',
			        success: function(data){
				       	 	$("#all_alarms_wrapper").html(data);
				       	 	$('#all_alarms_wrapper').show();
							$('#env_alarms_wrapper').hide();
							loadPageElements(LoadJSONCurrentState);
							$('#all_alarms_table').on("click", "span",function(){
							  		var uid=$(this).text().replace(" ","");
							  		console.log("clicked: "+uid);
							  		HighLightElement(uid);
								});
							process_table("alarms_affectingpage");
							loading("stop");
				
						}
					});
			
		}
	}
		if (pageElements!= undefined){loadPageElements(dummyCallback);}
		GetAlarmsAffectingPage(JSON.stringify(pageElements),parsegetAlarmsAffectingPage);
}

function dummyCallback(response){
	if(response!=undefined){
		console.log(response);
	}
	
}

function loadSimulatorTables(container, alarmList, actionList){
	console.log(actionList);
	var tableWrapperString = '<br/>';
	tableWrapperString += '<div id="simulator_tables_wrapper">\n';
	tableWrapperString += '<div id="simulator_tables_caption"><span id="env" class="bold cursor blue" onclick="toggleSimulatorTable(\'env\')">Environmental alarms</span> <b>/</b> <span id="all" class="cursor blue" onclick="toggleSimulatorTable(\'all\')">All alarms affecting this page</span></div>\n';
	tableWrapperString += '<div id="env_alarms_wrapper"></div>\n';
	tableWrapperString += '<div id="env_actions_wrapper"></div>\n';
	tableWrapperString += '<div id="all_alarms_wrapper"></div>\n';
	tableWrapperString += '</div>\n';

	container.insertAdjacentHTML('beforeend', tableWrapperString);

	//console.log(alarmList);

	$.ajax({
		url: 'templates/table_alarms_environment.php',
        data: {json:alarmList},
        type: 'post',
        success: function(data){
	       	$("#env_alarms_wrapper").html(data);
			}
		});
}

function toggleSimulatorTable(table){
	loading("start");
	if (table==="env"){
		
		$('#env').addClass("bold");
		$('#all').removeClass("bold");
		$('#all_alarms_wrapper').hide();
		$('#env_alarms_wrapper').show();
		loading("stop");
	}else{
		$('#all').addClass("bold");
		$('#env').removeClass("bold");
		if ($('#all_alarms_wrapper').text()===""){
			//This is triggered by the user on click
		      getAlarmsAffectingPage();
		}else{
			$('#env_alarms_wrapper').hide();
			$('#all_alarms_wrapper').show();
			loading("stop");
		}
	}
	
}
function OpenWindowWithPost(url, windowoption, name, params)
	   {
	            var form = document.createElement("form");
	            form.setAttribute("method", "post");
	            form.setAttribute("action", url);
	            form.setAttribute("target", name);
	 
	            for (var i in params) {
	                if (params.hasOwnProperty(i)) {
	                    var input = document.createElement('input');
	                    input.type = 'hidden';
	                    input.name = i;
	                    input.value = params[i];
	                    form.appendChild(input);
	                }
	            }
	            
	            document.body.appendChild(form);
	            
	            //note I am using a post.htm page since I did not want to make double request to the page 
	           //it might have some Page_Load call which might screw things up.
	            window.open("post.htm", name, windowoption);
	            
	            form.submit();
	            
	            document.body.removeChild(form);
	    }

function openTableWithObjectInNewTab(){
	
   function parseSimulationTable(json)
	   {
	       
	      OpenWindowWithPost('templates/template_table_with_objects.php',
	      "width=730,height=345,left=100,top=100,resizable=yes,scrollbars=yes", 
	      "NewFile", json);		
	    }

	 GetClassAndLocation(JSON.stringify(pageElements), parseSimulationTable);
	   

}


function fillRelatedPages(){
	pagesHTML="";
	console.log("fillRelatedPages");
	console.log(menuTree);
	$.each(menuTree, function(k, v){
      if (v["related-pages"]!== undefined){
      	if(k.toLowerCase()===pageName.toLowerCase()){
      		$.each(v["related-pages"], function(related_pos, related_val){
			      console.log(v);
			      pagesHTML+="<span class='related-page-link'><a href='/login/simulator.php?page="+related_val+"'>"+menuTree[related_val]["description"]+"</a></span>";
			      relatedPages=true;
			    });
      	}
      	
      }
    });
	if (relatedPages===true){
		$("#related-pages-links").append(pagesHTML);
		$("#related-pages-links").show();
		$(".CONTENT").css({ "margin-top": '10px' });
	}
}
