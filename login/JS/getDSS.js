function fillAlarms(data){
    console.log(data);
    src="<table id=\"DSS_alarms_table\">";
    src+="<thead>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Option</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    src+="</thead>";
    src+="<tbody>";
    for(ele of data){
      button_disable=AES_triggered_alarms.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"disabled":"";
      button_label=AES_triggered_alarms.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"Already triggered in Expert System":"Trigger in Expert System";
      src+="<tr>";
      src+="<td><a href='search.php?query="+ele['name']+"'>"+ele['name']+"</a></td>";
      src+="<td><input class='cursor' type='button' "+button_disable+" onclick='ChangeSwitchAndReloadDSS(\""+ele['name'].replace(/\s/g, '')+"\",update_DSS_tables);' styple=\"white-space:nowrap\" value=\""+button_label+"\"</input></td>";
      src+="<td styple=\"white-space:nowrap\"><span styple=\"white-space:nowrap\">"+new Date(parseFloat(ele['came'])*1000).toLocaleString('en-GB')+"</span></td>";
      src+="</tr>";
    }
    src+="</tbody>";
    src+="</table>";
    $('#alarms').html(src);
    process_table('DSS_alarms',5);
  }
  
  function fillInputs(data){
    console.log(data);
    src="<table id=\"DSS_inputs_table\">";
    src+="<thead>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Option</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    src+="</thead>";
    src+="<tbody>";
    for(ele of data){
      button_disable=AES_triggered_inputs.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"disabled":"";
      button_label=AES_triggered_inputs.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"Already triggered in Expert System":"Trigger in Expert System";
      src+="<tr>";
      src+="<td><a href='search.php?query="+ele['name']+"'>"+ele['name']+"</a></td>";
      src+="<td><input class='cursor' type='button' "+button_disable+" onclick='ChangeSwitchAndReloadDSS(\""+ele['name'].replace(/\s/g, '')+"\",update_DSS_tables);' styple=\"white-space:nowrap\" value=\""+button_label+"\"</input></td>";
      src+="<td styple=\"white-space: nowrap;\"><span styple=\"white-space: nowrap;\">"+new Date(parseFloat(ele['came'])*1000).toLocaleString('en-GB')+"</span></td>";
      src+="</tr>";
    }
    src+="</tbody>";
    src+="</table>";  
    $('#inputs').html(src);
    process_table('DSS_inputs',5);
  }
  function trigger_all_DSS_inputs(){
    for(ele of triggered_all_DSS_inputs){
      e=ele['name'].replace(/\s/g, '');
      console.log("Triggering: "+e);
      if (AES_triggered_inputs.hasOwnProperty(e) == false ) {ChangeSwitch(e,dummyCallback); }
    }
    update_DSS_tables();
   }
  
  function trigger_all_DSS_actions(){
    for(ele of triggered_all_DSS_actions){
      e=ele['name'].replace(/\s/g, '');
      console.log("Triggering: "+e);
      if (AES_triggered_actions.hasOwnProperty(e) == false ) {ChangeSwitch(e,dummyCallback); }
     }
     update_DSS_tables();
   }

   function trigger_all_DSS_alarms(){
    for(ele of triggered_all_DSS_alarms){
      e=ele['name'].replace(/\s/g, '');
      console.log("Triggering: "+e);
      if (AES_triggered_alarms.hasOwnProperty(e) == false ) {ChangeSwitch(e,dummyCallback); }
    }
    update_DSS_tables();
   }
   function parseGetTriggeredAlarms(json){
    AES_triggered_alarms=json["Result"];
    fillAlarms(dssdata['alarms'].sort((a,b)=>(a.came>b.came?1:-1)));
   }
   function parseGetTriggeredInputs(json){
    AES_triggered_inputs=json["Result"];
    fillInputs(dssdata['inputs'].sort((a,b)=>(a.came>b.came?1:-1)));
   }
   function parseGetTriggeredActions(json){
    AES_triggered_actions=json["Result"];
    fillActions(dssdata['actions'].sort((a,b)=>(a.came>b.came?1:-1)));
   }
  function fillActions(data){
    console.log(data);
    src="<table id=\"DSS_actions_table\">";
    src+="<thead>";
    src+="<tr>";
    src+="<th>Name</th>";
    src+="<th>Option</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    src+="</thead>";
    src+="<tbody>";
    for(ele of data){
      button_disable=AES_triggered_actions.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"disabled":"";
      button_label=AES_triggered_actions.hasOwnProperty(ele['name'].replace(/\s/g, ''))?"Already triggered in Expert System":"Trigger in Expert System";
      src+="<tr>";
      src+="<td><a href='search.php?query="+ele['name']+"'>"+ele['name']+"</a></td>";
      src+="<td><input class='cursor' type='button' "+button_disable+" onclick='ChangeSwitchAndReloadDSS(\""+ele['name'].replace(/\s/g, '')+"\",update_DSS_tables);' styple=\"white-space:nowrap\" value=\""+button_label+"\"</input></td>";
      src+="<td><span styple=\"white-space:nowrap\">"+new Date(parseFloat(ele['came'])*1000).toLocaleString('en-GB')+"</span></td>";
      src+="</tr>";
    }
    src+="</tbody>";
    src+="</table>";  
    $('#actions').html(src);
    process_table('DSS_actions',5);
  }
  
  function fillHistory(data){
    console.log(data);
    src="<table class='zebra'>";
    src+="<tr>";
    src+="<th>Type</th>";
    src+="<th>Class</th>";
    src+="<th>Time</th>";
    src+="</tr>";
    for(ele of data){
      src+="<tr>";
      src+="<td>"+ele['TYPE']+"</td>";
      src+="<td>"+ele['CLASS']+"</td>";
      //src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toUTCString()+"</td>";
      src+="<td>"+new Date(parseFloat(ele['PVSSTIME'])*1000).toLocaleString('en-GB')+"</td>";
      src+="</tr>";
    }
    src+="</table>";  
    $('#history').html(src);
  }


  function update_DSS_tables(){
      GetTriggeredAlarms(parseGetTriggeredAlarms);
      GetActiveInputs(parseGetTriggeredInputs);
      GetActiveActions(parseGetTriggeredActions);
      //fillAlarms(data['alarms'].sort((a,b)=>(a.came>b.came?1:-1)));
      //fillInputs(data['inputs'].sort((a,b)=>(a.came>b.came?1:-1)));
      //fillActions(data['actions'].sort((a,b)=>(a.came>b.came?1:-1)));
      triggered_all_DSS_alarms=dssdata['alarms'].sort((a,b)=>(a.came>b.came?1:-1));
      triggered_all_DSS_inputs=dssdata['inputs'].sort((a,b)=>(a.came>b.came?1:-1));
      triggered_all_DSS_actions=dssdata['actions'].sort((a,b)=>(a.came>b.came?1:-1));
      updateBarIcons();
      loading("stop");
  }