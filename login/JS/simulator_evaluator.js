/*
ignacio.asensi@cern.ch
Aug 2018
Functions for reports and simulation tools
Requires process_table from ui.js
Requires GetSimulationJSON from db.js
*/

var dsslogfile="";

/**
 * Retrieve the stored simulations
 DEPRECATED
function load_saved_simulations_table(){
	loading("start");
	$.ajax({
		    url: 'templates/table_saved_simulations.php',
		        data: {},
		        type: 'post',
		        success: function(data) {
			$("#saved_simulations_tbody").html(data);
			    process_table("saved_simulations");
			    loading("stop");
		    }
		});
}

*/
function load_saved_simulations_table(){
	loading("start");
	function parseGetSimulations(json){
		$.ajax({
		    url: 'templates/table_simulations.php',
		        data: {json:json["Result"]},
		        type: 'post',
		        success: function(data) {
							$("#simulations_tbody").html(data);
							if (n_affected_sys!=0 || n_affected_al!=0){
			            $(".simulation_buttons").prop('disabled', 'disabled');
			            $(".simulation_buttons").prop('innerHTML', 'Reset session to run simulation');
			        }
					    process_table("simulations");
					    loading("stop");
		    }
		});	
	}
	
	GetSimulations(parseGetSimulations);
}

/**
 * downloadObjectAsJson
 * Download simulation file to client
 */
function DownloadCurrentSimulation(){

	function downloadObjectAsJson(exportObj, exportName){
	    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj, null , "\t"));
	    var downloadAnchorNode = document.createElement('a');
	    downloadAnchorNode.setAttribute("href",     dataStr);
	    downloadAnchorNode.setAttribute("download", exportName + ".json");
	    document.body.appendChild(downloadAnchorNode); // required for firefox
	    downloadAnchorNode.click();
	    downloadAnchorNode.remove();
	  }
   
    function triggerDownloadSimulationFile(json){
		if ("Error" ==json["Result"]){
			console.log("Error");
		}else{
			//commands=json["Result"][0]
			//affected_sys=json["Result"][1]
			var today_d  = new Date();
			today=today_d.toLocaleDateString("en-US");
			saveName = today + '/' + today_d.getHours() + '_' + today_d.getMinutes() + '_' + today_d.getSeconds();
			json["Result"]["date"]=today;
			downloadObjectAsJson(json["Result"], saveName);

		}

	}
	
	//get json object from server
	GetSimulationJSON(triggerDownloadSimulationFile);
}

/**
 * Save a live simulation
 **/
function SaveSimulationFile(){
	/*
  function triggerSaveSimulationFile(json){
		if ("Error" ==json["Result"]){
      $("#simulation_output").html("<div><b>Error retrieving simulation</b></div>");
      return;
    }
    /*json["Result"]["commands"].shift();
    commands_=new Array();
    for (var n = 0; n < json["Result"]["commands"].length; n++) {
    	commands_.push(json["Result"]["commands"][0]);
    }
    
    $.ajax({url: 'scripts/file_helper.php',
            data: {cmd:"write", content: JSON.stringify(json["Result"]["commands"]), path:"../data/simulations/", simfilename:sim_name, type:"json"},
		        type: 'post',
		        success: function(data) {
		        	$("#simulation_name_input").val("");
              $("#simulation_output").html("<div><b>Saved simulation: "+sim_name+"</b></div>");
		        	load_saved_simulations_table();
            }
    });
  }
    
  var sim_name=$("#simulation_name_input").val();
  if (sim_name == ""){
    $("#simulation_output").html("<div><b>Error: No name selected</b></div>");
    return;
  }
   
  GetSimulationJSON2(triggerSaveSimulationFile);
  */
	SaveSimulation(dummy,$("#simulation_name_input").val(),"comments");
	}

/*
 * Upload a simulation file
 */
function UploadSimulation(){
  
  console.log("Upload simulation");
     
  var sim_name=$("#simulation_name_input").val();
  if (sim_name == ""){
 	  $("#simulation_output").html("<div><b>Error: No name selected</b></div>");
    return;
  }
  
  console.log($("#simulation_file_input"));
  
  //var sim_file=$("#simulation_file_input").files[0];
  var sim_file=document.getElementById("simulation_file_input").files[0];
  console.log(sim_file);
  
  if (sim_file == ""){
 	  $("#simulation_output").html("<div><b>Error: No file selected</b></div>");
 	  return;
  }
  
  var reader = new FileReader();
  
  reader.onload = function(event) {
    var json=JSON.parse(event.target.result);
    console.log(json);
    
    $.ajax({
		       url: 'scripts/file_helper.php',
			     data: {cmd:"write", content: json, path:"../data/simulations/", name:sim_name, type:"json"},
			     type: 'post',
			     success: function(data) {
			       $("#simulation_name_input").val("");
             $("#simulation_file_input").val("");
			       load_saved_simulations_table();
             console.log("UploadSimulation done!")   
			    }
		});
  };
  
  reader.readAsText(sim_file);
  console.log("Uploading file: "+sim_file); 
  
}

/**
 * Update the bar icons and stop loading the page
 **/
function after_simulation_sent(_content){
	updateBarIcons();
	loading("stop");
}

/**
 * Run a simulation that was stored 
 **/
function RunSimulationFile(name){
  loading("start");
  $.getJSON( 'data/simulations/'+name, function( json ) {
    console.log( "JSON Data received, name is " + json);
    //json["commands"]="";
    json_simulation=JSON.stringify(json["commands"])
    CheckSimulationJSON(json_simulation, after_simulation_sent)
  });
}

/**
 * Run a simulation that was stored in the database 
 **/
function CallRunSimulationFromDB(name){
  loading("start");
  function parseRunSimulationFromDB(){
  	loading("stop");
  	window.location.href = 'dashboard.php';
  }
  RunSimulationFromDB(name, parseRunSimulationFromDB);
}

/**
 * Download a stored simulation file
 **/
function DownloadSimulationFile(filename){
  var downloadAnchorNode = document.createElement("a");
  downloadAnchorNode.setAttribute("href", "data/simulations/"+filename);
  downloadAnchorNode.setAttribute("download", filename);
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
}

/**
 * Delete a stored simulation file
 **/
function DeleteSimulationFile(filename){
  loading("start");
  $.ajax({
	       url: 'scripts/file_helper.php',
		     data: {cmd:"delete", content:"", path:"../data/simulations/", name:filename, type:"json"},
		     type: 'post',
		     success: function(data) {
		       $("#simulation_output").html("<div><b>Simulation deleted: "+filename+"</b></div>");
		       load_saved_simulations_table();
           console.log("DeleteSimulation done!")   
		    }
	});
}

/*
DSSLogFileUpload 
for the tool to compare DSS log file to Expert System simulation
*/
function DSSLogFileUpload(){
	loading("start");
	if (jsdebug.report) console.log(dsslogfile);
	//get file
	var lines = dsslogfile.split('\n');   
	//get what to trigger
	var trigger=$("#LogFileSelect option:selected").val();
	field="None";
	switch(trigger){
		case "Alarm":
			tfield="ALARM_CAME"; 
			tprefix="AL_";
			break;
		case "Action":
			tfield="ACTION_CAME";
			tprefix="O_";
			break;
		case "DigitalInput":
			tfield="DIGITAL_TRUE_CAME";
			tprefix="DI_";
			break;
		default: console.log("DSSLogFileUpload: Unknown trigger subject!");return 0;
	}
	names=new Array();
	if (jsdebug.report) console.log("Searching for "+trigger);
	//get objects from file
	for (var n = 0; n < lines.length; n++) {
	  line=lines[n]
	  if (line.includes(tfield)){
	  	parts=line.split(" ");
	  	for (var p = 0; p < parts.length; p++) {
	  		if (parts[p].startsWith(tprefix)) names.push(parts[p]);
	  	}
	  }
	}

	namesjson=JSON.stringify(names);
	if (jsdebug.report) console.log("Requesting server for "+names);
	$("#LogFileDebug").html(names);
	$.ajax({
	    url: 'templates/table_LogFileDebug.php',
	        data: {json:names},
	        type: 'post',
	        success: function(data) {
			$("#LogFileDebug").html(data);
		    process_table("LogFileDebug");
	    }
		});
	ChangeBoolJSONAttribute(namesjson, "switch", "off", updateBarIconsWithLoadingStop);
}

/*
getDSSLogFile 
Part of the tool to compare DSS log file to Expert System simulation
Get the file 
Parse it, store in global var dsslogfile
*/
function getDSSLogFile(event) {
	var _content="";

	function parseFileContent(target, file) {
		function readFileContent(file) {
			const reader = new FileReader()
		  	return new Promise((resolve, reject) => {
		    	reader.onload = event => resolve(event.target.result)
		    	reader.onerror = error => reject(error)
		    	reader.readAsText(file)
		  })
		}

		

		readFileContent(file).then(content => {
	  		//if want to see 
	  		//target.value = content
	  		//console.log(content);
	  		//_content=JSON.parse(content);
	  		//json_simulation=JSON.stringify(content)
	  		//parseUploadedFileContentToTables(JSON.parse(content));
	  		//CheckSimulationJSON(content, after_simulation_sent)
	  		dsslogfile=content;
	  	}).catch(error => console.log(error))
	}
	
	if(n_affected_sys!=0 || n_affected_al !=0){
		$("#LogFileDebug").html("<div><b>Error: Simulation can only be checked in clear session. Please reset session</b></div>");
	}else{
		loading("strat");
		const input = event.target
		if ('files' in input && input.files.length > 0) {
			  parseFileContent(document.getElementById('upload_output'),input.files[0])
		  }

	}

	
}


