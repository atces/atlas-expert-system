/****************************************
 * Expert system communication tools
 * Carlos.Solans@cern.ch
 * Ignacio.Asensi@cern.ch
 * July 2016
 ****************************************/


//GLOBAL VARS

noIE();
var administrator;
var cname = "atlas-expert-system-cookie";
var debug=true;
var baseurl="https://" + window.location.hostname + "/login/";// + window.location.hostname.split(".")[0] + "/login/";
var jsdebug={}; //set level of debug in JS console
jsdebug.dbcalls=false;// 	displays debug info related to database calls
jsdebug.parse=false;//		displays debug info related to parsing functions
jsdebug.session=false;// 	displays debug info related to token id functions
jsdebug.responses=false;// displays the responses from server
jsdebug.tables=false;
jsdebug.style=false;
jsdebug.menuparse=false;
jsdebug.formulas=false;
jsdebug.functions=false;
jsdebug.report=true;
var webdebug={}; 
webdebug.dashboard=true;
var pageElementsSubsystems=new Array();

var tab; //this is the name of the page
var development=window.location.href.indexOf("atlas-expert-system-dev")!==-1;
var simulatorPage=undefined; //true is a page with a distribution image. It is refreshed in ready state in footer calling setPageType() or calling isSimulatorPage() function.
var highlight=false;//true means an element is being highlighted
var addednode=""; //var that keeps coordinates to display multiples elements
var ahist=new Array();// var for taken actions history
var results=new Object;
var n_affected_sys="";
var n_affected_al=0;
var clean_session="";//True if no action has been taken
var ctime=0;//time of simulation. Combobox in header
var MPC_list=new Array();
var currentHTMLMPCCalculation="";
if (localStorage.AddToList_Array!=undefined)
{
	MPC_list=JSON.parse(localStorage.AddToList_Array);
}

if (window.sessionStorage.display_subsystem==undefined){
		window.sessionStorage.display_subsystem=true;	
}

var pageName=(window.location.pathname.includes("simulator.php")) ? window.location.search.substring(1).split("=")[1].split("&")[0]: window.location.pathname.split("/")[2].replace(".php","");

var tables_with_hidden_rows= new Array();
tables_with_hidden_rows["impact"]= new Array();
tables_with_hidden_rows["summary"]= new Array();
tables_with_hidden_rows["dss"]= new Array();
tables_with_hidden_rows["alarms_affectingpage"]= new Array();


tries_HighLightElement=0;


var obj_in_page2="";
var obj_in_page3="";



function noIE(){
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0) // If Internet Explorer, return version number
    	{console.log("Browser NOT supported");
    		alert("Internet Explorer is not supported.");
		throw new Error("Internet Explorer is not supported.");
	}else{
		console.log("Browser supported");
	}
}


function superdebug(msg){
	if (jsdebug.functions == true){
		console.log("Function....... "+ msg);
	}
}

function export_tablesorter(table){
	var $table = $('#'+table);
	$table.trigger('outputTable');
}



/*
get system classes defined in loadhelpers and storethem in mJS emory
*/

function getSystemClasses(){
	if (window.sessionStorage["system_classes"]== undefined || window.sessionStorage["system_classes"]== "undefined"){
		//window.sessionStorage["system_classes"]==
		function putInLocalStorage(json){
			//console.log(json);
			window.sessionStorage["system_classes"]=JSON.stringify(json["SysClasses"]);
		}
		GetSysClasses(putInLocalStorage);
	}
}


/*
 * dummy function to output callback objects
*/
function dummy(json){
	console.log("dummy");
	console.log(json);
	loading("stop");
}
/*
* dummy  function to output callback objects and reload page
*/
function dummyReload(json){
	console.log("dummy");
	console.log(json);
	location.reload();
}


/**
 * isAdmin
 * brief function for javascript to check if user is admin
 * IMPORTANT: client validation has to be checked in server too!!!!
 * param msg string
 */

function isAdmin(){

	$.ajax({ url: baseurl+'isAdminJS.php',
	         type: 'post',
	         success: function(output) {
	                     administrator=output.replace(/(\r\n|\n|\r)/gm,"");
	                 },
	        error:function (err){
	        	$.ajax({ url: baseurl+'../isAdminJS.php',
	         		type: 'post',
	         		success: function(output) {
	                     administrator=output.replace(/(\r\n|\n|\r)/gm,"");
	                 },
	        		error:function (err){
	        			console.log(err);
	        		}
				});
	        }
	});
}



/**
 * LogMsg
 * brief function to log messages
 * param msg string
 */

function logMsg(msg){
  var node=document.getElementById('debug');
  //if(node!=null) node.innerHTML = msg;
  //console.log(msg);
}


function updateBarIcons(){
  console.log("updateBarIcons");
	updateSystemsIcon();
	updateAlarmsIcon();
	
}

function updateBarIconsWithLoadingStop(){
	updateSystemsIcon();
	updateAlarmsIcon();
	loading("stop");
}

/*general purpose function. On modification of alarms it should be called to keep the alarms icon in sync */
function updateAlarmsIcon(){
	GetTriggeredAlarms(parseTriggeredAlarms);
}
/**
 * parseTriggeredAlarms parses the json callback with triggered Alarms. It only adds the triggered flag to the table. Not the alarm names
 */
function parseTriggeredAlarms(triggeredAlarms){
	superdebug(arguments.callee.name);
	/**
	 * renderAlarmsIcon gets number of triggered alarms and updates icons
	 */
	function renderAlarmsIcon(number_alarms){
		
		if(number_alarms>0) {
			//$('#alarms_number').text(number_alarms).show();
			$('#menu_dashboard_wrapper_alarms_icon').text(number_alarms).show();
		}else{
			//$('#alarms_number').hide();
			$('#menu_dashboard_wrapper_alarms_icon').text("0");
		}
		$("#alarm_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
		
	}
	stored_triggeredAlarms=triggeredAlarms["Result"];
	$('.alarmLed').text("");
	$('.alarmLed').text("");
	$.each(triggeredAlarms["Result"], function(i, obj){
		$('.'+i+'.alarmLed').before().text("ON!");
	});
	//call to update triggered alarm flags
	n_affected_sys=Object.keys(stored_triggeredAlarms).length
	renderAlarmsIcon(n_affected_sys);

}

function updateSystemsIcon(){
	superdebug(arguments.callee.name);
	setTimeout(function(){ GetNumberOfAffectedSystems(parseAffectedSystems); }, 400);
}

function parseAffectedSystems(json){
	superdebug(arguments.callee.name);
	var systems_off=0;
	if("Result" in json){
		systems_off=json["Result"];
	}
	if(systems_off>0) {
		n_affected_sys=systems_off;
		$('#sys_number').text(systems_off).show();
		$('#menu_dashboard_wrapper_sys_icon').text(systems_off).show();
	}else{
		$('#sys_number').hide();
		$('#menu_dashboard_wrapper_sys_icon').text("0");
	}


	/*trigger particular page functions that depend on n_affected_sys and n_affected_sys*/
	if (typeof specificPageFunctions === "function") { 
	    specificPageFunctions();
	}
	//setTime();

	setTimeout(setTime, 25);

}



/**
 * retractableDetailsTable
 * Searches for element in header with retractableDetailsTable attribute.
 * If true and is mobile device it hides the table and displays the "?" to show the table
 * @returns
 */
function retractableDetailsTable(){
	superdebug(arguments.callee.name);
	var node=document.getElementById('ui');
	var isThereTable=node.getAttribute('retractableDetailsTable');
	if(isThereTable ==="true"){
		var mobilecheck=window.mobilecheck();
		if (window.mobilecheck()){
			$('#verbosecontrol').show();
			$('#verbose').hide();
		}
	}
}
/**
 * displays tokenid
 * @param tab
 * @returns
 */
function displayTokenid(){
	superdebug(arguments.callee.name);
	if(tokenid!=='undefined')$('.tokenid').html(tokenid);
}

/**
 * Variable QueryString
 * This function is anonymous, is executed immediately and
 * the return value is assigned to QueryString!
 * Ignacio.Asensi@cern.ch
 */

var QueryString = function () {
	superdebug(arguments.callee.name);
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();


/*
 * Displays a loading icon for the time given
 * */
function loadingIcon(time){
	superdebug(arguments.callee.name);
	$("#loading_icon").show();
	$("#loading_icon").fadeOut(time);
}

function loading(cmd){

	if(cmd==="start"){
		$("#loading_icon").show();
	}else{
		$("#loading_icon").fadeOut(400);
	}


}


function getPageElements(){
	superdebug(arguments.callee.name);
	var elements=[];
	$('.element').each(function(img){
			elements[this.id]=[this.id];
	    });
	 return elements;
}


/**
 * fitTable
 * adjust the width table with the results. It's called in the footer after the results fill the table.
 */
function fitTable(){
	superdebug(arguments.callee.name);
	console.log("function fitTable to "+tab);
  var widthValue=$('#'+tab+'_table').width();
	console.log("width: "+widthValue);
  $('.tablesorter-scroller-table').width(widthValue+2);
	$('.tablesorter-scroller-header').width(widthValue+2);

}




$(document).ready(function(){
	
	superdebug(arguments.callee.name);
	/*trigger main menu button*/
	console.log("Document ready");
  	$('#menupngdesktop').click(function (){
  	  $('#mainmenudesktop').toggle();
  	  $('#menupngdesktop').find('img').toggle();
  	  });
  	isAdmin();



  	//set language in upper bar select

  	var mobilecheck=window.mobilecheck();
  	if (window.location.search.toLowerCase().indexOf("lang=en")!==-1) {
  		$('#language_select').val("EN");
  		if(!mobilecheck) $('.language-table').append("<img src='"+baseurl+"../images/en.png' style='float:left;height:2em;margin-top: 2px'>");
  	}else if (window.location.search.toLowerCase().indexOf("lang=fr")!==-1) {
  		$('#language_select').val("FR");
  		if(!mobilecheck)$('.language-table').append("<img src='"+baseurl+"../images/fr.png' style='float:left;height:2em;margin-top: 2px'>");
  	}else if (window.location.search.toLowerCase().indexOf("lang=ru")!==-1) {
  		$('#language_select').val("RU");
  		if(!mobilecheck)$('.language-table').append("<img src='"+baseurl+"../images/ru.png' style='float:left;height:2em;margin-top: 2px'>");
  	}else{
  		if(!mobilecheck)$('.language-table').append("<img src='"+baseurl+"../images/en.png' style='float:left;height:2em;margin-top: 2px'>");
  	}






  	$('#upperbar_searchbox').focusin(function(){
  		if ($('#upperbar_searchbox').val()==="Search...")$('#upperbar_searchbox').val("");

  	});


  	$('.fa-search').on('click', function(){
  		if ($('#upperbar_searchbox').val()==="Search...")$('#upperbar_searchbox').val("");
  		$('#search_form_upperbar').submit();
  	});
  	/*
  	$('#upperbar_searchbox').focusout(function(){
  		$('#upperbar_searchbox').val("Search...");
  		$('#upperbar_searchbox').attr('size',7);
  	});
  	*/
  	//selec change lang event
  	$('#language_select').on('change', function() {
      var url=new URL(window.location.href);
      var params=new URLSearchParams(url.search);
      params.set('lang',this.value);
      var url2=window.location.pathname+ "?"+ params.toString();
  		console.log("go to: "+url2);
  		window.location.href=url2;
	  });





});

/*detect mobile browser user agent*/
window.mobilecheck = function() {
	superdebug(arguments.callee.name);
	/*OLD MOBILE CHECK
	  var check = false;
	  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	  if(window.innerWidth <= 800) {check=true;}
	  //if(check===true) $('body').css('background-color', 'red');
	  return check;
	  */
	 var md = new MobileDetect(window.navigator.userAgent);
	 if (md.mobile()==="" || md.mobile()===null){
	 	if (jsdebug.style) console.log("DESKTOP LAYOUT");
	 		$(".mobilehide").show();
			$(".mobileshow").remove();
	 	return false;

	 }
	 if (jsdebug.style) console.log("MOBILE LAYOUT");
	 SetMobileLayout();
	 return true;
};





/**
 * Change Repowered attribute
*/
function ChangeRealim(uid, attr, callback){
	el=document.getElementsByClassName("repowered_switch");
	value=el[0].getAttribute("repowered");
	if (value=="false"){
		el[0].setAttribute("repowered", "true");
		el[0].setAttribute("src", "../images/repow_switchon.png");
		newvalue="true";
	}else{
		el[0].setAttribute("repowered", "false");
		el[0].setAttribute("src", "../images/repow_switchoff.png");
		newvalue="false";
	}
	ChangeAttribute(uid, attr, newvalue, LoadJSONCurrentState);
}


function ChangeInhibit(uid, attr, callback){
	el=document.getElementsByClassName("inhibit_switch");
	value=el[0].getAttribute("repowered");
	if (value=="false"){
		el[0].setAttribute("repowered", "true");
		el[0].setAttribute("src", "../images/inhibit_true.png");
		newvalue="true";
	}else{
		el[0].setAttribute("repowered", "false");
		el[0].setAttribute("src", "../images/inhibit_false.png");
		newvalue="false";
	}
	ChangeBoolAttribute(uid, attr, newvalue, LoadJSONCurrentState);
}
/**
 * displaySection
 * Toggle function to hide/display one region/image in any simulator page
 * */
function displaySection(section) {
	superdebug(arguments.callee.name);
	if ( document.getElementById(section).style.display != "none") {
		document.getElementById(section).style.display = "none";
		}
	else {
    	document.getElementById(section).style.display = "block";
    }
}
/**
 * messageSent
 * displays a notification as confirmation
 * @returns
 */
function messageSent(){
	superdebug(arguments.callee.name);
	$('#confirmation_message').html("Request succesfully processed").effect("highlight", {color:"#ff0000"}, 3000);
}


function parseDSU(json){
	superdebug(arguments.callee.name);
	$('#DSU_n').append("DSU number: <span class=\"cursor link\" onclick=\"displayHistory('"+json["DSU"]+"')\">"+json["DSU"]+"</span>");
}
/**
 * parseDDVhistory
 * fill description.php page with DDV history
 * @param json
 * @returns
 */
function parseDDVhistory(json){
	superdebug(arguments.callee.name);
	$('#DDVhistory').html("<b>Probability of Failure:</b> "+ json["Result"]);
}


function getProbability_PoF(uid){
	superdebug(arguments.callee.name);
	/**
	 * parsePoF
	 * fill description.php page with DDV history
	 * @param json
	 * @returns
	 */
	function parsePoF(json){
		superdebug(arguments.callee.name);
		console.log(json);
		$('#PoF').html("<b>Probability of Failure:</b> "+ json["Result"]);
	}
	level=1;
	GetPoF(uid, level, parsePoF);
}

/**
 * switchSys function change the
 * @param uid
 * @returns
 */
function switchDSU(uid, page){
	superdebug(arguments.callee.name);
	function parseDSU(json){
		console.log(json);
		search(uid, page);
	}
	ChangeDSUSwitch(uid, parseDSU);
}

/**
 * isSimulatorPage 
 * @returns True if it is a simulator page
 */
function isSimulatorPage(){
	if(simulatorPage===undefined){
		simulatorPage=($( "#distribution" ).length===1);
	}
	return simulatorPage;
}

/**
 * setPageType applies differences between simulator/table pages
 * setPageType is called in footer after ready state
 * @returns
 */
function setPageType(){
	superdebug(arguments.callee.name);

	//getSystemClasses
	getSystemClasses();

	//add DEVELOPMEMNT banner fixed
	if(development){ 
    var dev="<img id='dev_icon' src='"+baseurl+"../images/dev_icon.png' title='You are in the Development Server'/>";
	  $('#mainmenumobiletitle').prepend(dev); 
  }

	//true is simulator page
	if (isSimulatorPage()) {
		if (development){
			//add show coordinates checkbox and event listener
			if(!window.mobilecheck())$('#coordinates_input_wrapper').show();
			$('#input_coordinates').click(function(){
				if($(this).is(":checked")){
					$('#mousetracker').show();
				}else $('#mousetracker').hide();
			});
		}
	}

	//common for development and production
	if(window.mobilecheck()){//if mobile
		$('#reset_tokenid_bar').hide();
	}

  markActiveTab();

}

/**
 * resetTokenId
 * Function to reset the token id.
 * Used from the button in header
 * no need for loading("stop") because it's reloading page
 * @returns
 */
function resetTokenId(){
	superdebug(arguments.callee.name);
	function calling(response){
		console.log("New tokenId: "+response);
        sessionStorage.clear();
        reloadPage();
    }
	var proceed=confirm("This will set every system back to initial state. Do you want to proceed?");
    if(proceed === true){
    	loading("start");
        GetToken(calling);
        //Move to inside the calling function
        //sessionStorage.clear();
        //reloadPage();
    }

}


/**
 * FIXME: This function should be out of here!
 **/
function finder(uid, tagid, style, lang){
	if(style==undefined){style="";};
	loading("start");
	console.log("finder: uid:"+uid+", tagid:"+tagid+", style:"+style+", lang:"+lang);
	$.ajax({
		url: 'templates/finder.php',//'+output.replace('#','')+'
        data: {'uid':uid, 'style':style, 'lang':lang},
        type: 'post',
        success: function(data) {
        	//escaping uid string dots
        	uidESC=uid.replace(/\./g,"\\\.");//elementESC=".Y\\.55-25\\.A2";
       	 	$(tagid+"."+$.escapeSelector(uid)).html(data);
       	 	loading("stop");
        }
	});
}


function element_hover(uid){
	superdebug(arguments.callee.name);
	function hoverDescription(json){
		console.log(json);
		$('#'+uid).attr("title", json["Result"]);
	}
	GetDescription(uid, hoverDescription);
}


function reloadPage(){
	superdebug(arguments.callee.name);
	setTimeout(function reload(){
        	location.reload();
        }, 1000);
}


function testDBFunction(json){
	superdebug(arguments.callee.name);
	console.log("DB response: "+json);
	}

/**
 * critical_error function to display errors in selected tag
 * @param {Object} msg
 */
function critical_error(msg){
	superdebug(arguments.callee.name);
	$('#critical_error_msg').html("!! CRITICAL ERROR: "+ msg);
	//$('#critical_error_wrapper').show().toggle("pulsate");
	//$('#critical_error_wrapper').show().effect("highlight", {color:"#ff0000"}, 30000);;
	$('#critical_error_wrapper').show();
	$('#critical_error_msg').delay(200).fadeOut('slow').delay(50).fadeIn('slow',critical_error);

	//$('#critical_error_wrapper').show()
}
/**
 * critical_error_acknowledge function to remove the error msg
 */
function critical_error_acknowledge(){
	superdebug(arguments.callee.name);
	$('#critical_error_wrapper').hide();
	$('#critical_error_checkbox').prop('checked', false);
}




/**
 * tools_DbAnalizer
 * gets all objects of a class
 * sends them to php, php looks for number of occurrences in files php files
 * thats the number that they appear in expert system pages
 * then names and values are put in table
 * @returns
 */
function tools_DbAnalizer(){
	superdebug(arguments.callee.name);
	function parsedbAnalyzer(json){
		var length=Object.keys(json["Result"]).length;
		for (each in json["Result"]){

			$.ajax({
			url: 'templates/finder_dbAnalyzer.php',
	        data: {uid_finder:each, min_val:number
	        		},
	        type: 'get',
	        success: function(data) {
	       	 	$('#db_analysis_tbody').append(data.replace(/(\r\n|\n|\r)/gm,""));
	       	 	width = (counter * 100)/length;
	       	 	elem.style.width = width+ '%' ;
	       	 	counter++;
	       	 	if(width==100) $('#reading_db_flag').html('');
	        }
			});
		}
	}
	//getting progress bar and setting it to 0
	var elem = document.getElementById("db_Bar");
	elem.style.width = 0;
	$('#reading_db_flag').html('Reading db');
	//reseting values
	var counter=1;
	$('#db_analysis_tbody').html("");
	//feeding information
	var db_class=$('#tools_db_analysis_classname').val();
	$('#tools_db_analysis_table').show();
	$('#db_progress').show();
	var number=parseInt($('#tools_db_analysis_number').val());
	if (isNaN(number))number=null;
	//db call
	getAllClassObjects(db_class, parsedbAnalyzer);
}
/*Function to fill attributs selector in tools page. Panel of Attribute counter.
 * It is called on change of select of class. On event*/
function tools_fill_attributes(selected_class){
	superdebug(arguments.callee.name);
	function parse_fill_attributes(json){
		superdebug(arguments.callee.name);
		options="";
		for (att in json.schema){
			if (typeof json.schema[att] === "object"){
				options+="<option value=\""+att+"\">"+att+"</option>";
			}
		}
		$('#attributeCounter_attribute').html(options);
		
	}
	GetClassSchema(selected_class.value, parse_fill_attributes);
}

function tools_attribute_counter(){
	superdebug(arguments.callee.name);
	function parse_tools_attribute_counter(json){
		superdebug(arguments.callee.name);
		tbody="";
		for (row in json.Result){
			tbody+="<tr><td>"+row+"<img style=\"float:right;\" class=\"cursor newtabicon\" src=\"../images/newtab.png\" onclick=\"displayHistory('"+row+"')\"></td><td>"+json.Result[row]+"</td></tr>";
		}
		$('#attributeCounter_tbody').html(tbody);
		process_table('attributeCounter');
		
	}
	//show download CSV button and set event
	$('#downloadCSV').show();
	$('#downloadCSV').click(function() {
        // tell the output widget do it's thing
        $('#attributeCounter_table').trigger('outputTable');
    });
	class_name=$( "#attributeCounter_class option:selected" ).text();
	attr_name=$( "#attributeCounter_attribute option:selected" ).text();
	GetAttributeCounts(class_name, attr_name, parse_tools_attribute_counter);
}


/*Function to populate the selects in tools page with the names of the classes*/
function tools_fillClassestools(){
	superdebug(arguments.callee.name);
	console.log("DEPRECATED");
	function parseClasses(json){
		var text="";
		for(each in json["Classes"]){
 			text+="<option value=\""+json["Classes"][each]+"\">"+json["Classes"][each]+"</option>";
		}
		$('.ad_search_type').append(text);
		element=document.getElementById("attributeCounter_class");
		element.onchange();
	}
	function parseSysClasses(json){
		superdebug(arguments.callee.name);
		var text="";
		for(each in json["SysClasses"]){
 			text+="<option value=\""+json["SysClasses"][each]+"\">"+json["SysClasses"][each]+"</option>";
		}
		$('.sys_classes').append(text);
		
	}
	
	GetClasses(parseClasses);
	GetSysClasses(parseSysClasses);
}

/**
 * Function to get the name of a building from the filename
 * @param {Object} location
 */
function menuTree_finder(location){
	
}
/* HEADER MENU FUNCTIONS TO FOLD/UNFOLD */


function menu_toggle(element, entrie, level){
	if(level==undefined){level="";}
	function parseMenuStorage(){
		 $( "div#Default_menu>.headerbutton_div" ).each(function( index ) {
				entrie=this.id;
				if($('#'+entrie).is(":visible")===true){
					if (jsdebug.menuparse) console.log("Showing: " + this.id );
		  			window.localStorage[entrie]="show";
				}else{
					if (jsdebug.menuparse) console.log("Hidding: " + this.id );
		  			window.localStorage[entrie]="hide";
				}
				});
	 }
	//show/hide sub entries
	//$('.'+entrie+'.headerbutton_div').toggle("slide",{},200);
	$('.'+entrie+'.headerbutton_div').each(function(index){
		console.log(this.id+"-"+($('#'+this.id).is(":visible")===true));
		$('#'+this.id).toggle("slide",{},200);
	});

	//when folding a L1 entrie, check that all L3 sub subentries close too
	if (level==="L1"){
		subsubentries=$('#'+element.id).nextUntil('.L1');
		for (sub in subsubentries){
			if (subsubentries[sub].id!==""){
				$('#'+subsubentries[sub].id).hide();
				window.localStorage[subsubentries[sub].id]="hide";
			}
		}
	}

	//switch right/down arrow when folding/unfolding
	if ($('#itag-'+entrie).hasClass("fa-angle-down")){
		$('#itag-'+entrie).removeClass("fa-angle-down").addClass("fa-angle-right");
		window.localStorage[element.id]="hide"
		if (level==="L1"){
			$('.L3.headerbutton_div').css('display','none');
		}

	}else{
		$('#itag-'+entrie).addClass("fa-angle-down").removeClass("fa-angle-right");

	}

	//check L2 arrows are to right when unfolding L1
	if (level==="L1"){
		$('div.L2 > .fa-angle-down').removeClass("fa-angle-down").addClass("fa-angle-right");
	}

	 setTimeout(function() {
	        parseMenuStorage();
	    }, 600);




}


function markActiveTab(){
	$( ".headerbutton_div" ).each(function( index ) {
		//var url	= window.location.pathname;
		//var file=url.substring(url.lastIndexOf('/')+1).replace(".php","");

			$('#'+pageName+">a").addClass("activetab");
		
			
		
	});
}


function setTime(){
	if (jsdebug.functions){console.log("setTime");}
	if (n_affected_sys!=0 ){	
		$('#time_select').attr('disabled', 'disabled');
	}
	if (window.localStorage.time!=undefined){
		$('#time_select').val(window.localStorage.time);	
	}
}

function saveTimeOnMem(){
	window.localStorage.setItem("time",parseInt($("#time_select").val()));
}



/**
 * Function to switch between default and advanced menu
 *
 */
function switch_menu(element){
	if(element===undefined){
		$('#menu_switcher').text(window.localStorage.menu);
	}


	if($('#menu_switcher').text()==="Advanced"){
		$('#menu_string_search').show();
		$('#Advanced_menu').show();
		$('#Default_menu').hide();
		$('#menu_switcher').text("Default");
		window.localStorage.setItem("menu","Advanced");
	}else{
		$('#menu_string_search').hide();
		$('#Advanced_menu').hide();
		$('#Default_menu').show();
		$('#menu_switcher').text("Advanced");
		window.localStorage.setItem("menu","Default");
	}
}
	/*
	 * Restore the unfolded entries in the default menu
	 * when loading a page. Using window.localStorage data
	 */
	function restore_default_menu(tab){
		if(tab==undefined){tab="";}
		if (window.localStorage["folder"]!==undefined) $('#Default_menu_folder').text(window.localStorage["folder"]);
		$( ".headerbutton_div" ).each(function( index ) {
			entrie=this.id;
  			if (window.localStorage[entrie]!==undefined){
  				if(window.localStorage[entrie]==="show"){
  					
					$('#'+entrie).show();
					if ($('#'+entrie).text().toLowerCase()===tab.toLowerCase()) $('#'+entrie+">a").addClass("activetab");
				}
  			}

		});
	}
	/*
	 * Empty session memmory. Kind of reset menu. Used when fold/unfold the menu
	 */
	function reset_default_menu(){
		for (entrie in window.localStorage){
			if (entrie.startsWith("entrie_")===true){
				window.localStorage[entrie]="";
			}
			if($('#'+entrie).hasClass("L1")===true) window.localStorage[entrie]="show";
			}
	}


	/*
	 *
	 */
	function iterate_default_menu(){
		$( ".headerbutton_div" ).each(function( index ) {
			entrie=this.id;
			if($('#'+entrie).is(":visible")===true){
				console.log( index + ": " + this.id );
	  			window.localStorage[entrie]="show"
			}
		});
	}
function default_menu_folder(element){
	window.localStorage.clear();
	reset_default_menu();
	if (element.innerText==="Unfold"){
		window.localStorage["folder"]="Fold";
		$('.headerbutton_div').show();
		$('div#Default_menu > div > i').addClass("fa-angle-down").removeClass("fa-angle-right");
		$('#Default_menu_folder').text("Fold");
		iterate_default_menu();

	}else{
		window.localStorage["folder"]="Unfold";
		$('#Default_menu_folder').text("Unfold");
		$('.headerbutton_div.L2').hide();
		$('.headerbutton_div.L3').hide();
		$('div#Default_menu > div > i').addClass("fa-angle-right").removeClass("fa-angle-down");
	}
}

function reset_table(tablename){
	var $table = $("#"+tablename+'_table');
    $table.trigger('filterReset').trigger('sortReset');
	return false;

}


function process_table(tablename, rowslimit,height, pagerwidget=false, headers={}){
	awidgets=["zebra", "uitheme","filter","output"];
	if (pagerwidget==true) {
		awidgets=["zebra", "uitheme","filter","output","pager"];
	}
	if(rowslimit==undefined){rowslimit=0;}
	if(height==undefined){height=810;}
	if(jsdebug.tables) console.log("processing table "+tablename+". Rows limit: "+rowslimit);
	$('#'+tablename+'_table').tablesorter({
        theme: 'blue', 
        showProcessing : true,
        widthFixed: false,
        sortList:[[0,0]],
        widgets: awidgets,
        headers: headers,
        widgetOptions: {
            scroller_height: height,
            scroller_upAfterSort: false,
            scroller_jumpToHeader: false,
            filter_childRows : false,
			filter_childByColumn : false,
			filter_childWithSibs : true,
			filter_columnFilters : true,
			filter_columnAnyMatch: true,
			filter_cellFilter : '',
			filter_cssFilter : '', // or []
			filter_defaultFilter : { 0 : '{query}' },
			filter_excludeFilter : {},
			filter_external : '.getsearch',
			filter_filteredRow : 'filtered',
			filter_formatter : null,
			filter_functions : {},
			filter_hideEmpty : true,
			filter_hideFilters : false,
			filter_ignoreCase : true,
			filter_liveSearch : true,
			filter_matchType : { 'input': 'exact', 'select': 'exact' },
			filter_onlyAvail : 'filter-onlyAvail',
			filter_placeholder : { search : '', select : '' },
			filter_reset : 'button.reset',
			filter_resetOnEsc : true,
			filter_saveFilters : false,
			filter_searchDelay : 200,
			filter_searchFiltered: true,
			filter_selectSource  : null,
			filter_serversideFiltering : false,
			filter_startsWith : false,
			filter_useParsedData : false,
			filter_defaultAttrib : 'data-value',
			filter_selectSourceSeparator : '|',
			output_headerRows    : true,        // output all header rows (multiple rows)
		    output_popupStyle    : 'width=580,height=310',
		    output_saveFileName  : tablename+'_ExpertSystem.csv',

	}}).bind('filterEnd', function(event, filteredRows){//filterEnd
		counterselector="."+tablename+"_table_row_counter";
    	$(counterselector).html(" (" + filteredRows.filteredRows+")");
    	if (jsdebug.tables==true){console.log("Counting rows for: "+counterselector);}
	}).bind('update', function(event){//filterEnd
		counterselector="."+tablename+"_table_row_counter";
	    $(counterselector).html(" (" + (event.currentTarget.rows.length-2)+")");
	    if (jsdebug.tables==true){console.log("Counting rows for: "+counterselector);}
	});
	$('#'+tablename+'_table').trigger("update").trigger("appendCache").trigger("applyWidgets");
	if($('#'+tablename+'_table').text().includes("[\"Reply\"]=>")) {
		$('#'+tablename+'_table').hide();
	}
	
}



function process_table_withdates(tablename, rowslimit,height, pagerwidget=false){
	awidgets=["zebra", "uitheme","filter","output"];
	if (pagerwidget==true) {
		awidgets=["zebra", "uitheme","filter","output","pager"];
	}
	if(rowslimit==undefined){rowslimit=0;}
	if(height==undefined){height=810;}
	if (true) console.log("processing table "+tablename+". Rows limit: "+rowslimit);
	$('#'+tablename+'_table').tablesorter({
        theme: 'blue', 
        showProcessing : true,
        widthFixed: false,
        sortList:[[0,0]],
        headers: {
            6: { sorter: 'date' }
        },
        widgetOptions: {
            scroller_height: height,
            scroller_upAfterSort: false,
            scroller_jumpToHeader: false,
            filter_childRows : false,
			filter_childByColumn : false,
			filter_childWithSibs : true,
			filter_columnFilters : true,
			filter_columnAnyMatch: true,
			filter_cellFilter : '',
			filter_cssFilter : '', // or []
			filter_defaultFilter : { 0 : '{query}' },
			filter_excludeFilter : {},
			filter_external : '.getsearch',
			filter_filteredRow : 'filtered',
			filter_formatter : null,
			filter_functions : {},
			filter_hideEmpty : true,
			filter_hideFilters : false,
			filter_ignoreCase : true,
			filter_liveSearch : true,
			filter_matchType : { 'input': 'exact', 'select': 'exact' },
			filter_onlyAvail : 'filter-onlyAvail',
			filter_placeholder : { search : '', select : '' },
			filter_reset : 'button.reset',
			filter_resetOnEsc : true,
			filter_saveFilters : false,
			filter_searchDelay : 200,
			filter_searchFiltered: true,
			filter_selectSource  : null,
			filter_serversideFiltering : false,
			filter_startsWith : false,
			filter_useParsedData : false,
			filter_defaultAttrib : 'data-value',
			filter_selectSourceSeparator : '|',
			output_headerRows    : true,        // output all header rows (multiple rows)
		    output_popupStyle    : 'width=580,height=310',
		    output_saveFileName  : tablename+'_ExpertSystem.csv',

	}}).bind('filterEnd', function(event, filteredRows){//filterEnd
		counterselector="."+tablename+"_table_row_counter";
    	$(counterselector).html(" (" + filteredRows.filteredRows+")");
    	console.log("Counting rows for: "+counterselector);
	}).bind('update', function(event){//filterEnd
		counterselector="."+tablename+"_table_row_counter";
	    $(counterselector).html(" (" + (event.currentTarget.rows.length-2)+")");
	    console.log("Counting rows for: "+counterselector);
	});
	$('#'+tablename+'_table').trigger("update").trigger("appendCache").trigger("applyWidgets");
	if($('#'+tablename+'_table').text().includes("[\"Reply\"]=>")) {
		$('#'+tablename+'_table').hide();
	}
	
}

function process_smalltable(tablename, h){
	
	$(function () {
	var height=h;
    var $table = $("#"+tablename+"_table"),
        updateScroller = function (height) {
            $('.tablesorter-scroller-table').css({
                height: '',
                'max-height': height + 'px'
            });
        };

    $table.tablesorter({
        theme: 'blue',
        widthFixed: true,
        widgets: ["zebra", "scroller","filter","output"],
        widgetOptions: {
            scroller_height: h,
            scroller_upAfterSort: false,
            scroller_jumpToHeader: false,
            output_headerRows    : true,        // output all header rows (multiple rows)
		      output_popupStyle    : 'width=580,height=310',
		      output_saveFileName  : tablename+'_ExpertSystem.csv'

        },
        initialized: function(){
            updateScroller( h );
        }
    });

    
        updateScroller( height);
        $('#'+tablename+'_table').trigger("update").trigger("appendCache").trigger("applyWidgets");
    

});
}

function toogleTableRows(rawtablename, toogle){
	tablename="#"+rawtablename+" > tbody > tr";
	if(toogle==="show"){
		$(tablename).show();
		$("#"+rawtablename+" > caption.bottomleft").hide();
		$(tablename).trigger("update");
	}else{
		$("#"+rawtablename+" > caption.bottomleft").show();
		$(tablename).hide().slice(0, 3).show();
		$(tablename).trigger("update");
	}
	loading("stop");
}
function add_caption_if_long_table (tablename){
	buttonid=tablename.replace("_table","_buttonid")
	if ($("#"+tablename+" tr").length>3){
		$("#"+tablename).append('<caption id="'+buttonid+'"class="cursor bottomleft" onclick="showMoreRowsFromCaption(\''+tablename.replace("_table","")+'\',\''+buttonid+'\')">More...</caption>');
	};	
}
 






function toogleTablesFromButton(tablename, buttonid){
	loading("start");
	if ($('#'+buttonid).text()==="Show all"){
		toogleTableRows(tablename+"_table", "show");
		$('#'+buttonid).text("Show less");
	}else{
		toogleTableRows(tablename+"_table", "hide");
		$('#'+buttonid).text("Show all");
	}
}


function showMoreRowsFromCaption(tablename, buttonid){
	loading("start");
	toogleTableRows(tablename+"_table", "show");
}

function getPCA(json){
	var order={};
	var biggest_v=0;
	biggest_c="";
	for (c in json){
		if (json[c][0]>biggest_v){
			biggest_v=json[c][0];
			biggest_c=c;
		}
		order[c]=json[c][0];
	}
	return biggest_c;
}

function getAllPCA(json){
	
	var text="<table>";
	for (c in json){
		text+="<tr>";
		text+="<th class='regform-done-caption'><b>"+c+"</b></th><td class='regform-done-title'>"+json[c][0]+" = "+json[c][1]+"</td>";
		//order[c]=json[c][0];
		text+="</tr>";
	}
	text+="</table>";
	return text;
}

function getFormulaPoF(uid){
	function parseFormulaPoF(json){
		if (jsdebug.formulas){
			console.log(json.tree_formula_uids);
			console.log(json.PoS_value);
			console.log(json.PoS_formula);
			console.log(json.PoF_value);
			console.log(json.PoF_formula);
			console.log(json.PCA_values);
		} 
		var PCA_name=getPCA(json.PCA_values);
		
		/*
		$('#tree_formula_uids_'+uid).text(json.tree_formula_uids);
		$('#PoS_value_'+uid).text(json.PoS_value);
		$('#PoS_formula_'+uid).text(json.PoS_formula);
		$('#PoF_value_'+uid).text(json.PoF_value);
		$('#PoF_formula_'+uid).text(json.PoF_formula);
		$('#PCA_all_'+uid).html(getAllPCA(json.PCA_values));
		$('#PCA_result_'+uid).text(json.PCA_values[PCA_name][0]);
		$('#PCA_formula_'+uid).text(json.PCA_values[PCA_name][1]);
		$('#PCA_name_'+uid).text(PCA_name);*/
		$('#tree_formula_uids_').text(json.tree_formula_uids);
		$('#PoS_value_').text(json.PoS_value);
		$('#PoS_formula_').text(json.PoS_formula);
		$('#PoF_value_').text(json.PoF_value);
		$('#PoF_formula_').text(json.PoF_formula);
		$('#PCA_all_').html(getAllPCA(json.PCA_values));
		$('#PCA_result_').text(json.PCA_values[PCA_name][0]);
		$('#PCA_formula_').text(json.PCA_values[PCA_name][1]);
		$('#PCA_name_').text(PCA_name);
		
	}
	depth=document.getElementById("depth").value;
	GetFormulaPoF(uid,depth, parseFormulaPoF);
}

function getReliabilityOfClass(){
	function parsegetReliabilityOfClass(json){
		
		var text="";
		objects=json["Result"];
		if (objects.constructor!==Object){
			text+="<td colspan=\"3\" >ERRROR</td>";
		}else{
			for (c in objects){
				text+="<tr>";
				text+="<td><b>"+c+"<a href=\"https://atlas-expert-system-dev.web.cern.ch/atlas-expert-system-dev/login/search.php?query=\""+c+"></a></b></td><td>"+  objects[c][0] * 100 +"</td><td><span style=\"display: inline\">"+objects[c][1]+"</span></td>";//Math.round(objects[c][0] * 100000) / 1000
				//order[c]=json[c][0];
				text+="</tr>";
			}
		}
		$('#PoS_tbody').html(text);
		$('#PoS_table').show();
		process_table('PoS');
		
	}
	var name=$( "#PoS_class option:selected" ).text();
	GetReliabilityOfClass(name, parsegetReliabilityOfClass);
}
function removeProp(obj, propToDelete) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (typeof obj[property] == "object") {
        removeProp(obj[property], propToDelete);
      } else {
        if (property === propToDelete && obj[property] !== undefined) {
          delete obj[property];
        }
      }
    }
  }
  return obj;
}




function triggerProbabilitySearch(uid, attr, info,  opts){
	if (uid==undefined){uid="";}
	$('#error_searching').text("");
	if (uid==="") uid=$("#search_explanation").val();
	//get formula and display it
	getFormulaPoF(uid);
	//hide name of uid in hidden element. So it is available when changing deep level
	$("#hidden_uid_tree").text(uid);
	//get tree draw and display it
	drawObjectTree(uid, info, opts);
	
	
}

/*Get tree of an object and paint parents tree*/
function drawObjectTree(uid, info, draw_options){
	if (uid==undefined){uid="";}
	var tree_structure_s = { 
		    chart: {
		        container: "#OrganiseChart6",
		        levelSeparation:    20,
		        siblingSeparation:  15,
		        subTeeSeparation:   15,
		        rootOrientation: draw_options.orientation,//"SOUTH",

		        node: {
		            HTMLclass: "tennis-draw",
		            drawLineThrough: true
		        },
		        connectors: {
		            type: "straight",
		            style: {
		                "stroke-width": 2,
		                "stroke": "#ccc",
		                'arrow-start': 'classic-wide-long'//'block-wide-long'
		            }
		        }
		    },
		    
		    nodeStructure:{}};
	var tree_draw="";
	function parseGetObjectParentsTree(json){
		if (json.Reply.startsWith("Error")){
			$('#error_searching').text("Object not found");
		}else{
		//console.log(json)
		tree_draw=json.Result;
		tree_structure_s.nodeStructure=json.Result;
		if (webdebug.dashboard) $("#tree_json").html(JSON.stringify( tree_structure_s));
		new Treant( tree_structure_s);
		loading("stop");
		tuneTree();

		}
		
		
	}
	
	//get depth level
	$('#fieldset_tree').show();
	level_deep=document.getElementById("level_deep").value;
	
	//if not uid provided to the function,  search for hidden value in panel
	//else put it in the hidden value
	if (uid==""){
		uid=$("#hidden_uid_tree").text();
		
	}else{
		$("#hidden_uid_tree").text(uid);
	}
	console.log("Drawing tree of "+uid);
	//get probability parameters from json file
	$.getJSON('data/parameters_probability.json').done(function (parameters) {
		parameters_s = JSON.stringify(parameters);
		console.log("Parameters sent to server: "+parameters_s);
		GetObjectParentsTree(uid, level_deep,parameters_s, parseGetObjectParentsTree);
	});
	
}


function parseGetDescription(json) {
		$('#alarm_description').text(json.Result);
	}

/*Get tree of an object and paint parents */
function drawObjectAttrTree(uid, depth, attr, info, opts, line, arrow_style){
	if (uid==undefined){
	  console.log("UI.JS cannot drawObjectAttrTree"); return;
	}
	var tree_structure_s = { 
		    chart: {
		        container: "#OrganiseChart6",
		        levelSeparation:    20,
		        siblingSeparation:  15,
		        subTeeSeparation:   15,
		        rootOrientation: opts.orientation,
		        node: {
		            HTMLclass: "tennis-draw",
		            drawLineThrough: true
		        },
		        connectors: {
		            type: "straight",
		            style: arrow_style
		        }
		    },		    
		    nodeStructure:{}
      };
	var tree_draw="";
	function parseGetAttrObjectParentsTree(json){
		if (json.Reply.startsWith("Error")){
			$('#error_searching').text("Object not found");
		}else{
		if (json.Result.children==undefined){//if nothing to show, display a message
			if (line=="parents"){
				if (uid.startsWith("AL_")){
					$("#tree_warning_response").html("<span style='color:red'><b>"+uid+ "</b> has no digital input!</span>");
				}else{ $("#tree_warning_response").html("<span style='color:red'><b>"+uid+ "</b> has no parents!</span>");}
			}
			if (line=="children"){
				if (uid.startsWith("AL_")){
					$("#tree_warning_response").html("<span style='color:red'><b>"+uid+ "</b> has no actions!</span>");
				}else{$("#tree_warning_response").html("<span style='color:red'><b>"+uid+ "</b> has no children!</span>");}
				
			}
    }else{$("#tree_warning_response").html("");}
		
		tree_draw=json.Result;
		tree_structure_s.nodeStructure=json.Result;
		if (webdebug.dashboard) $("#tree_json").html(JSON.stringify( tree_structure_s));
		new Treant( tree_structure_s);
		loading("stop");
		tuneTree();

		}
	}

	console.log("Calling DB.JS GetAttrObjectParentsTree");
	GetAttrObjectParentsTree(uid, depth, info, attr, line, parseGetAttrObjectParentsTree);
	if (uid.startsWith("AL_")){
		GetDescription(uid, parseGetDescription);
	}
	
}


/*set probability values for each class in reliability test .*/
function saveParameters(cmd){
	
	var user_params={};
	var OK=true;
	$(".parametervalue > input").each(function() {
		//console.log($(this)[0].id.replace("param_",""));
		if (parseInt($(this)[0].value)>1){
			OK=false;
		}else{
			user_params[$(this)[0].id.replace("param_","")]=$(this)[0].value;
		}
		
		
		}); 
	if (OK==true){
		//console.log(user_params);
		s_user_params=JSON.stringify( user_params)
		$.ajax({
			url: 'scripts/access_probability.php',
	        data: {cmd:cmd, parameters:user_params},
	        type: 'post',
	        success: function(data) {
	        	$("#parameters_messages").text(data);
	        	triggerProbabilitySearch();
	       	 	
	        }
			});
	}else{
		$("#parameters_messages").text("Values must be between 0 and 1");
	}
	
}

function init_LoadDSU(dsu_name){
	function parseDSU(json){
		$.ajax({
			url: 'templates/DSU_pins.php',
	        data: {actions:json["actions"], DIs: json["DIs"], dsu:dsu_name },
	        type: 'post',
	        success: function(data) {
	        	$("#image_td >div").remove();
	       	 	$("#image_td").append(data);
	       	 	$("#main_page_title").text(dsu_name);
	       	 	$("img#distribution").attr("src","img/dsu/"+dsu_name+".png");
	       	 	$("#DSU_details").click(function(){displayHistory(dsu_name);});
	        }
			});
	}
	if (dsu_name==="select"){
		dsu_name=$("#select_DSU option:selected").val()
	}
	if (dsu_name==="None"){
		$("#image_td >div").remove();
		$("#verbose").html("");
	}else{
		GetFullDSU(dsu_name, parseDSU);
	}

}










// store elements for the MPC in the localStorage
// use the "AddToList_Array" key (create if necessary)
function AddToMPCList(uid){	
	if(typeof(Storage) !== "undefinied"){
		var button = document.getElementById("button_"+uid);	
		if(localStorage.AddToList_Array){
			// FIXME make sure that the localSession is not too old (1d?) FIXME
			// every element is only ONCE in the list
			if (!localStorage.AddToList_Array.includes(uid)){
				var ListMPC_array_get = JSON.parse(localStorage.AddToList_Array);
				ListMPC_array_get.push(uid);				
				localStorage.AddToList_Array = JSON.stringify(ListMPC_array_get);
				//remove from main table
				button.innerHTML = "Added to list";	
				button.disabled = true;	
				$("#MPC_tr_"+uid).hide();
			} 
		} else {
			var ListMPC_array_init = [uid];			
			localStorage.AddToList_Array = JSON.stringify(ListMPC_array_init);
			$("#MPC_tr_"+uid).hide();
					
		}
	} else {
		console.log("Sorry, your browser does not support web storage.");
	}		
}


// delete the MPC list from local storage & update the shown list
function DeleteListMPC(output){
	var display = document.getElementById(output);
	if(localStorage.AddToList_Array){
		localStorage.removeItem("AddToList_Array");
		display.innerHTML = "List deleted";
	}
	else { display.innerHTML = "No list selected.";}
	loadMPCtoAddItemsTable($("#inhibit_type option:selected").val(), 0,'#table_MPC_toadd_wrapper','inhibit');
	loadMPCTable("#table_MPC_added_wrapper","MPC_added");
}

// remove an item from MPC list if the button is clicked, if last item
function RemoveFromList(value) {
	//check if the chosen element is part of the list
	if(localStorage.AddToList_Array && localStorage.AddToList_Array.includes(value)){ 
		var selectedArray = JSON.parse(localStorage.AddToList_Array); // format: array
	
		// remove the item from the list  	
		if(selectedArray.length > 1) { 					
			var indexRemove = selectedArray.indexOf(value);
			if(indexRemove > -1) { selectedArray.splice(indexRemove,1); }
			localStorage.AddToList_Array = JSON.stringify(selectedArray);
		} else {  
			localStorage.removeItem("AddToList_Array"); // clean up the localStorage if the last element is deleted
		} 	
	} else { console.log("!! no MPC list found in local storage"); } 
	loadMPCtoAddItemsTable($("#inhibit_type option:selected").val(), 0,'#table_MPC_toadd_wrapper','inhibit');
	loadMPCTable("#table_MPC_added_wrapper","MPC_added");
}


function fromArrayToTable(data, tablename){
	$("#"+tablename+"_table").show();
	s="";
	for (k in data){
		s+='<tr><td>'+data[k]+'</td><td><img class="cursor" src="../images/newtab.png" onclick="displayHistory(\''+data[k]+'\', \'#page2\');">';//<td><span class="'+data[k]+' alarmLed ledRed"></span></td></tr>';    
	}
	$("#"+tablename+"_tbody").html(s);
	process_table(tablename);

}


function tuneTree(){
	$( ".tennis-draw" ).each(function( index ) {
		if (this.childNodes.length==1){
			this.childNodes[0].style["color"]="black";
		}else if (this.childNodes.length==2){
			this.childNodes[0].style["font-weight"]="bold";
		}
	//console.log( index + ": " + $( this ).text() );
	});
}

function showServerException(){
	if (development == true){
		alert("There was a problem in the server. An exception.");
		$("#main_page_title").html("<span style='color:red'>There was a problem in the server!!!</span>");
	}
}
function update_page2and3objects(){
	if (obj_in_page2!=""){
		search(obj_in_page2, '#page2');
	}
	if (obj_in_page3!=""){
		search(obj_in_page3, '#page3');
	}
}



function getDate(date_s,format){
	if (format==1){
		year=parseInt(date_s.substring(6, 10));
		month=parseInt(date_s.substring(3, 5))-1;
		day=parseInt(date_s.substring(0, 2));
		hour=parseInt(date_s.substring(11, 13));
		min=parseInt(date_s.substring(14, 16));
	}else{
		year=parseInt("20"+date_s.substring(6, 8));
		month=parseInt(date_s.substring(3, 5))-1;
		day=parseInt(date_s.substring(0, 2));
		hour=parseInt(date_s.substring(9, 11));
		min=parseInt(date_s.substring(12, 14));
	}
	dt = new Date(year, month, day, hour, min, 0, 0);
	return dt;
}


function checkRowPassedDate(getDateType, tablename, rowclass){
	var today = new Date();
	$('#'+tablename+'_table > tbody  > tr > td.'+rowclass).each(function(index, tr) {
		if (today > getDate(tr.innerText,getDateType)){
			for (i=0;i< tr.parentNode.children.length;i++){
				if (i!="length"){
					console.log("Expired: "+ tr.innerText);
					tr.parentNode.children[i].className="expired_color";
				}
			}
		}
	});
}

function checkRowPassedDateRemove(){
	$('.expired_color').each(function(index, tr) {
		tr.className="";
		
	});
}

// get the common parents for the elements in the list for the MPC
function callFindMPC(isExhaustive=true,elapsed_time='1',apply_inhibits=true) {
 	loading("start");
 			
	// call the function FindMPC() -> db.js
	if(localStorage.AddToList_Array) {
		//inhibited_objects_uids is defined in getMPC.php
		findMPC(localStorage.AddToList_Array,callbackFindMPC,isExhaustive,elapsed_time,apply_inhibits ? inhibited_objects_uids : []);		
	}

	function callbackFindMPC(listofobjects_json) { 
		setTimeout(() => {updateCalculationsTable();	loading("stop");}, 500);
	}
}

function  callStopMPC(mpc_calculation){
	function callbackStopMPC(){
		updateCalculationsTable();
	}
	stopMPC(mpc_calculation,callbackStopMPC);	
}

function updateCalculationsTable(){
	if (jsdebug.functions == true){console.log("called dashboard_get_commands");}
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_getTableCalculations(json){
		if (json.Reply==="Error"){
			console.log("dashboard_get_commands():Error. Server response error");
			$("#dashborrd_commands_wrapper").html("Server error! Commands history could not be retrieved.");
		}else{
			$.ajax({
				url: 'templates/table_calculations.php',
		        data: {json:json['Result']},
		        type: 'post',
		        success: function(data) {
			       	 $("#calculations_table_wrapper").html(data);
			        }
			});
		}
	}
	GetMPCCalculationAndStatus(parse_getTableCalculations);
}



function load_Calculation(type, uid){
	if (jsdebug.functions == true){console.log("called dashboard_get_commands");}
	/**
	 *  parse_dashboard_affeted_systems parses affected systems using a php template
 	 * @param {Object} json
	 */
	function parse_load_Calculation(json){
		if (json.Reply==="Error"){
			console.log("dashboard_get_commands():Error. Server response error");
			$("#dashboard_commands_wrapper").html("Server error! Commands history could not be retrieved.");
		}else{
			$.ajax({
				url: 'templates/details_calculation.php',
		        data: {json:json},
		        type: 'post',
		        success: function(data) {
						if(!(currentHTMLMPCCalculation==data)){
					       	 $("#calculation_details").html(data);
							currentHTMLMPCCalculation=data;
						}
			        }
			});
		}
	}
	GetMPCCalculationDetails(uid,parse_load_Calculation);
}


function loadMPCtoAddItemsTable(type, delay, wrapper, tablename){
	//setTimeout(function(){getAllClassObjects(type,parseRequestTable);}, delay);
	loading("start");
	if (localStorage.AddToList_Array!=undefined)
	{
		MPC_list=JSON.parse(localStorage.AddToList_Array);
	}else{
		MPC_list=Array();
	}
	function parseRequestTableWithCaption(json){
		//Remove items already added
		for( i in MPC_list){
		  if (json.Result.hasOwnProperty(MPC_list[i])){
		  	delete json.Result[MPC_list[i]];
		  }
		}
		$.ajax({
		url: 'templates/table_MPC.php',
        data: {json:json, tablename:type},
        type: 'post',
        success: function(data) {
	       	 $(wrapper).html(data);
	       	 //console.log(data);
	       	 process_table("MPC",300);
	       	 //refresh_MPC_items_on_table(tablename);
	       	 //$("#inhibit_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	       	 //process_table("inhibit");
	       	 loading("stop");
	        }
		});
	}
	getAllClassObjects(type,parseRequestTableWithCaption)
}
function refresh_MPC_items_on_table(tablename){
	if (localStorage.AddToList_Array!=undefined)
	{
		MPC_list=JSON.parse(localStorage.AddToList_Array);
	}else{
		MPC_list=Array();
	}
	for (a=0; a<MPC_list.length; a++){
		if (MPC_list[a]!==undefined)AddToMPCList(MPC_list[a]);
	}
	loading("stop");
}
function MPC_disable_added_items(uid){
	for ( u in MPC_list){
		if(uid === MPC_list[u]){$("#button_"+uid).prop('disabled', true);}
	}
	
}

function loadMPCTable(wrapper, tablename){
	//data=DisplayListMPC();
	function parseRequestTableWithCaption(MPC_content){
		function MPC_add_types_to_table(json){
			console.log(json)
	       	$("#typeof_"+json["UID"]).text(json["Class"]);
	       	 
		}
		$.ajax({
		url: 'templates/table_MPC_added.php',
        data: {json:MPC_content},
        type: 'post',
        success: function(data) {
	       	 $(wrapper).html(data);
	       	 process_table("MPCadded",300);
	       	 refresh_MPC_items_on_table(tablename);
	       	 //$("#inhibit_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
	       	 //process_table("inhibit");
	       	 for (o in MPC_content){
	       	 	GetNoClassObject(MPC_content[o],MPC_add_types_to_table)
	       	 }
	        }
		});
	}


	if (localStorage.AddToList_Array!=undefined)
	{
		MPC_content=JSON.parse(localStorage.AddToList_Array);
		
	}else{
		MPC_content=new Array();
	}
	parseRequestTableWithCaption(MPC_content);
	
	
}

function fillClassesSelect(uid){
		function parseClasses(json){ 
			for(each in json["Classes"]){
	 			text+="<option value=\""+json["Classes"][each]+"\">"+json["Classes"][each]+"</option>";
			}
			$(uid).append(text);
			loadMPCtoAddItemsTable($("#inhibit_type option:selected").val(), 0,'#table_MPC_toadd_wrapper','inhibit');
		}
		var text="";
		GetClasses(parseClasses);
}

async function addMPCItemsFromClipboard() {
	loading("start");
	function parse_validate_uid(json){
		if (json.Reply==="False"){
			console.log("UID not valid");
		}else{
			uid=json.uid
			if(localStorage.AddToList_Array){
				if (!localStorage.AddToList_Array.includes(uid)){
					var ListMPC_array_get = JSON.parse(localStorage.AddToList_Array);
					ListMPC_array_get.push(uid);				
					localStorage.AddToList_Array = JSON.stringify(ListMPC_array_get);
				} 
			} else {
				var ListMPC_array_init = [uid];			
				localStorage.AddToList_Array = JSON.stringify(ListMPC_array_init);
			}
		}
	}
	if(typeof(Storage) !== "undefinied"){
		const text = await navigator.clipboard.readText();
		var uids = text.replace(/ /g,'').split("\n");
		for (const uid of uids){
			if (uid.length>0){isAValidUID(uid,parse_validate_uid);}
		}		
		await new Promise(r => setTimeout(r, 500));
		loadMPCTable('#table_MPC_added_wrapper','MPC_added');
	} else {
		console.log("Sorry, your browser does not support web storage.");
	}
	loading("stop");
}


/*Menu box search */
      function showLocationResult(query){
        if(query===""){
          $('.menuentry').show();
          return;
        }
        responses=[];
        $.each(menuTree, function(k, v){
          if (v.locations!== undefined){
            if(v.locations.indexOf(query.toUpperCase())!==-1){
              responses.push(k+"<br>");
              $('#'+k+".menuentry").show();
            }else{
              $('#'+k+".menuentry").hide();
            }
              
          }
        });
      }

      function showPanelResult(query) {
        if(query === "") {
          $('.menuentry').show();
          return;
        }
        responses = [];
        console.log("AA")
        $.each(menuTree, function(k, v) {
          if(k.toLowerCase().includes(query.replace(/\s/g, '').toLowerCase()) && !responses.includes(k)) {
            responses.push(k);
            $('#'+k+".menuentry").show();
          }
          else {
            $('#'+k+".menuentry").hide();
          }
        });
      }
