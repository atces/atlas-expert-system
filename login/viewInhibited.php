<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
include_once("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
definePage("viewInhibit");
?>
<!DOCTYPE html>
<html>
<head>
<title>Request view</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
<?php include ("favicon.php");?>
<link rel="stylesheet" type="text/css" href="css/style.css">
<?php $scripter->includeScripts(); ?>
<?php $styler->includeStyle(); ?>
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
<script src="JS/ui.js" retractableDetailsTable="true" id="ui"></script>
<script src="JS/adminFunctions.js"></script>
<!--<script src="JS/simulatorParser.js"></script>-->
<!--<script src="JS/inhibitRequest.js"></script>-->
<?php $scripter->includeScripts(); ?>

<script>

$(document).ready(function(){
  //loading("start");
	updateBarIcons();
});

</script>

<style>

	table#request{
		margin-top:100px;
		margin-left:20px;
	}
	table#request > td{
		vertical-align:top;
		margin-bottom:50px;
		height:30px;
	}
	input{margin-left:20px;}
	
</style>
</head>
<body>
<br><br><br>
<?php  
include("header.php"); 
?>
<?php 
$filename='data/requestsDB.json';

	
$uid=$_GET["uid"];
$file=file_get_contents($filename);
$list=json_decode($file, true);
$counter=0;
//var_dump($file);			


//if (count($file) > 0 ){
foreach( $list as $item  ){
	if (strcmp($item["id"], $uid)===0){
		$counter++;
		?>
		<table id="request">
			<tr>
				<td><b>Request status:</b></td>
				<td><b>
				<?php 
				switch($item["status"]){
					case "approved": echo "<span style=\"color:green;\">Approved</span>";break;
					case "declined": echo "<span style=\"color:red;\">Declined</span>";break;
					case "archived": echo "<span style=\"color:grey;\">Archived</span>";break;
					case "pending" : echo "<span style=\"color:orange;\">Your request has not been revised yet</span>";break;
					default: echo "<span>".$item["status"]."</span>";break;
				}	
				?>
				</b></td>
			</tr>
			<tr>
				<td><b>DSS status:</b></td>
				<td><b>
				<?php 
				switch($item["inhibited"]){
					case "yes": echo "<span style=\"color:green;\">Currently applied on DSS</span>";break;
					case "no": echo "<span style=\"color:red;\">Currently NOT applied on DSS</span>";break;
					default: echo "<span style=\"color:red;\">Unknown. Conntact DSS team</span>";break;
				}	
				?>
				</b></td>
			</tr>
			<tr>
				<td><b>Alarms:</b></td>
				<td><?php 
					if(is_array($item["alarms"])){
						foreach ($item["alarms"] as $value => $subvalue){
							echo "<p>".$subvalue."</p>";
						}
					}else{ echo $item["alarms"];}
					?>
				 </td>
			</tr>
			<tr>
				<td><b>Requestor:</b></td>
				<td><?= $item["requestor"]?></td>
			</tr>
			<tr>
				<td><b>Mail:</b></td>
				<td><?= $item["mail"]?></td>
			</tr>
			<tr>
				<td><b>Beginning:</b></td>
				<td><?= $item["beginning"]?></td>
			</tr>
			<tr>
				<td><b>Ends on:</b></td>
				<td><?= $item["expiration"]?></td>
			</tr>
			<tr>
				<td><b>Reason:</b></td>
				<td><?= $item["reason"]?></td>
			</tr>
		</table>
		
		<?php 
		}	
}
if ($counter==0) echo "<p>This request was not found</p>";
?>
<br/>
<p>
<button type="submit" onclick="window.location='inhibitRequest.php'">Back to list</button>
<button type="submit" onclick="window.location='inhibitRequest.php?edit=<?=$item['id'];?>'">Edit request</button>
</p>
<div class="footer">
	<?php include("footer.php"); ?>
</div>
</body>
</html>
