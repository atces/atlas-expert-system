<!-- include other scripts -->
<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("simulator_evaluator");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
$uid=$_GET["uid"];
?>

<!-- beginn the HTML document -->
<!DOCTYPE html>
<html lang="en">
	<!-- head 
	run several scripts
	-->
	<head>
		<title>Get MPC - ATLAS Expert System</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		
		<?php include ("favicon.php");?>
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>
    
		
		<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
		<script id="tableFunctions_search" class="tableFunctions" src="JS/tableFunctions.js" table="computation"></script>
		<script src="JS/report.js"></script>
	
	<!-- set the style -->

	</head>

	<!-- body -->
	<body class="white_background">  
		<? include("header.php"); ?>
		
 				
		<div class="CONTENT grid"> 
			<div id="calculation_details"></div>
			
			</div>
		<script>
			load_Calculation("MPC","<?=$uid;?>");
			setInterval(load_Calculation,5000,"MPC","<?=$uid;?>");
		</script>
		<div class="footer">
		<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
