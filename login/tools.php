<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("Tools");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<?php include ("favicon.php");?>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js?<?=strftime("%Y%m%d%H%M%S");?>"></script>
		<script src="JS/ui.js?<?=strftime("%Y%m%d%H%M%S");?>" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>
		
	</head>
	<body class="grey_background">  
		<? include("header.php"); ?>
		<div class="CONTENT grid"> 
			<!-- Systems -->
			<?php include("templates/panel_db_analyzer.php");?>
			
			<!-- Systems -->
			<span id="tools_db_attributeCounter_wrapper"></span>
			<fieldset class="tool_fieldset">
				<legend id="tools_attributeCounter">Attribute counter</legend>
				<div>Select class and attribute to find the number of attribute objects</div>
				<table>
					<tr>
						<td>Class</td>
						<td>
    						<select id="attributeCounter_class" class="ad_search_type" onchange="tools_fill_attributes(this);" style="width:120px">
    						</select>
						</td>
					</tr>
					<tr>
						<td>Attribute</td><td style="text-align: right">
						<select id="attributeCounter_attribute"></select>
						</td>
					</tr>
				</table>
				<div>Columns can be filtered using relational operators</div>
				<button onclick="tools_attribute_counter();">Go</button>
				<button id="downloadCSV" hidden>Get CSV</button>
				<div id="reading_db_flag"></div>
				<table  id="attributeCounter_table" hidden>
					<thead>
						<tr>
							<td>Object</td>
							<td>Number</td>
						</tr>
					</thead>
					<tbody id="attributeCounter_tbody">
						
					</tbody>
				</table>
			</fieldset>
			<!-- POS -->
			<fieldset class="tool_fieldset">
			<legend>Probability of survival of a class</legend>
			<div>Select class to find the PoS of each object of given class</div>
				<table>
					<tr>
						<td>Class</td>
						<td>
    						<select id="PoS_class" class="sys_classes" style="width:120px">
    						</select>
						</td>
					</tr>
				</table>
				<button onclick="getReliabilityOfClass();">Go</button>
				<div id="PoS_wrapper">
				<div><button id="PoS_button" hidden onclick="export_tablesorter('PoS_table');">CSV</button></div>
				<table id="PoS_table" hidden style="width:100%!important;">
                    <thead>
                        <th>Element</th>
                        <th>Reliability</th>
                        <th>Formula</th>
                    </thead>
                    <tbody id="PoS_tbody" ></tbody>
				</table>
				</div>		
			</fieldset>
			<!-- Object count -->
			<fieldset class="tool_fieldset">
				<legend>Count number of objects of a class</legend>
				<div>Return the number of objects of each class</div>
				<button onclick="get_SummaryOfObjects();">Go</button>
				<div id="SoO_wrapper">
				<div><button id="SoO_button" hidden onclick="export_tablesorter('SoO_table');">CSV</button></div>
				<table id="SoO_table" hidden style="width:100%!important;">
                    <thead>
                        <th>Class</th>
                        <th>Count</th>
                    </thead>
                    <tbody id="SoO_tbody" ></tbody>
				</table>
				</div>
			</fieldset>
			
			
		</div>
		<script>
		
		$(document).ready(function(){
			
			tools_fillClassestools();
			
		});
		
		function get_SummaryOfObjects(){
			function parse_SummaryOfObjects(json){
		
				var text="";
				objects=json["Result"];
				if (objects.constructor!==Object){
					text+="<td colspan=\"3\" >ERRROR</td>";
				}else{
					for (c in objects){
						text+="<tr>";
						text+="<td><b>"+c+"</b></td>";
						text+="<td><b>"+objects[c]+"</b></td>";
						text+="</tr>";
					}
				}
				$('#SoO_tbody').html(text);
				$('#SoO_table').show();
				$('#SoO_button').show();
				process_table('SoO');
		
			}
			GetSummaryOfObjects(name, parse_SummaryOfObjects);
		}


		</script>
		
		<div class="footer">
		<?php include("footer.php"); ?>
		
		</div>
	</body>	
</html>
