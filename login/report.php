<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("report");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		
		<?php include ("favicon.php");?>
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
   
		<script src="JS/db.js"></script>
		<script src="node_modules/jspdf/dist/jspdf.umd.min.js"></script>
		<script src="node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.min.js"></script>
		<script src="JS/exporttablepdf.js"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/dashboard.js"></script>
		<script src="JS/simulatorParser.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="data/codification.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>

		<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>		
		<script src="JS/report.js"></script>
	</head>
	<body class="grey_background">  
		<? include("header.php"); ?>
		<div class="CONTENT grid"> 
			<!-- Dashboard options-->
			<div class="dashboard_level">
					  
				<fieldset id="report_header">

			<h1 class="title">Report</h1> 
			<p>Token id: <span class="tokenid"></span></p>

		<!-- Playing around with the formatting of the pdf on the export button -->
		<div><button onclick="exporttablepdf('report_intervened_table','report_affected_table');">Export As PDF</button></div>
		<br>
		</fieldset>
								  
				<fieldset id="intervened" class="report">
				<legend><b>Intervened systems</b> (Systems with switch off)</legend>
				<div id="row_counter" class="report_intervened_table"></div>
				<div><button onclick="export_tablesorter('report_intervened_table');">CSV</button></div>
				<table id="report_intervened_table">
					<thead>
						 <th>Location</th>
						 <th>System</th>
						 <th>Type</th>
					 </thead>
					<tbody id="report_intervened_tbody"></tbody>
				</table>
							</fieldset>
				<fieldset id="affected" class="report">
					<legend><b>Affected systems</b> (System with state off)</legend>
					<div id="row_counter" class="report_affected_table"></div>
					<div><button onclick="export_tablesorter('report_affected_table');">CSV</button></div>
					<table id="report_affected_table" style="border-width:1px;">
						<thead>
							 <th>Location</th>
							 <th>System</th>
							 <th>Type</th>
						<tbody id="report_affected_tbody"></tbody>
					</table>
				</fieldset>
			</div>
		<script>
			$(document).ready(function(){
				loadReport();
			});
		</script>

		<div class="footer">
		<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
