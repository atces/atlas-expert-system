<?php
include_once("functions.php");
include ("scripts/stylehelper.php");
$styler = new StyleHelper("css/style.css","css/styleMobile.css");
definePage("simulator_evaluator");
include ("scripts/jsscripthelper.php");
$scripter = new ScriptHelper();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=$pagetitle;?> - ATLAS Expert System</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF8"> 
		
		<?php include ("favicon.php");?>
		<link rel="stylesheet" type="text/css" href="css/theme.blue.css">
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
		<script type="text/javascript" src="node_modules/tablesorter/dist/js/jquery.tablesorter.js"></script>
		<script src="node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
		<script src="JS/db.js"></script>
		<script src="JS/ui.js" retractableDetailsTable="true" id="ui" ></script>
		<script src="JS/simulator_evaluator.js"></script>
		<script src="JS/searchTools.js"></script>
		<script src="JS/inhibitRequest.js"></script>
		<script src="JS/explanation.js"></script>
		<script src="node_modules/tablesorter/dist/js/widgets/widget-output.min.js"></script>
		<script src="node_modules/tablesorter/dist/js/parsers/parser-input-select.min.js"></script>
		<link href="css/theme.bootstrap.css" rel="stylesheet">
		<?php $scripter->includeScripts(); ?>
		<?php $styler->includeStyle(); ?>
		<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>
		<script id="tableFunctions_search" class="tableFunctions" src="JS/tableFunctions.js" table="computation"></script>
	  <style type="text/css">
	  	.tablesorter  td:nth-child(1)
					{ width:50px!important;
					  min-width : 50px !important;
					}
			.tablesorter  td:nth-child(2),.tablesorter  td:nth-child(3)
					{ width:200px!important;
					  min-width : 200px !important;
					}
	  </style>
	</head>
	<body class="grey_background">  
		<? include("header.php"); ?>
		
 				
		<div class="CONTENT grid"> 
			
			<div class="dashboard_level">

				<!-- General inhibits -->
        <!-- JS/inhibitRequest.js -->
        <!--
        <fieldset class="border_dark">
					<legend>General Inhibits</legend>
					Inhibit all actions in DSS. Actions will not be triggered unless DSU losses power. 
					<div><button onclick="callInhibitAllActions()">Inhibit all actions</button></div>
					<div id="callInhibitAllActions_debug"></div>
				</fieldset>
				-->
        
        <!--
        <fieldset class="mobileshow border_dark">
					<legend>Simulation settings</legend>
					<div>
						<a id="reset_tokenid_button" class="cursor mobilebutton" onclick="resetTokenId();">Reset Session <span class="tokenid"></span></a>
					</div>
					<div>
					    Time (min.): 
					    <select id="time_select" class="mobileselect" onchange="saveTimeOnMem()">
					      <option value="5">5</option>
					      <option value="10">10</option>
					      <option value="15">15</option>
					      <option value="60">60</option>
					      <option value="120">120</option>
					      <option value="0" selected="selected">Max</option>
					    </select>	
					  </div>
				</fieldset>
        -->    
        <fieldset class="border_dark">
					<legend>Simulation of annual interventions</legend>
					<div>
          <p>This tool allows you to simulate an annual intervention. 
          </p>
					<p>Simulations can run only in a clear starte. If not, click on Reset Session before. </p>
					</div>
          <div id="simulation_output"></div>
					<br>
					<!--
					<table>
						<tr>
							<td>Save current simulation as <input id="simulation_name_input" type="text"></td>
							<td><button id="save_simulation" onclick="SaveSimulationFile()">Save</button></td>
						</tr>
						<tr>
							<td>Download current simulation</td>
							<td><button id="download_simulation" onclick="DownloadCurrentSimulation()">Download</button></td>
						</tr>
            <tr>
              <td>Upload a simulation file <input type="file" id="simulation_file_input" onclick="UploadSimulationFile(event)"><div hidden id="simulation_content"></div></td>
              <td><button id="upload_simulation" onclick="UploadSimulation()">Upload</button></td>
            </tr>
					</table>
				-->
					<br>
					<table id="simulations_table" style="width:100% !important;">
				        <thead >
		                    <th>Action</th>
		                    <th>Simulation</th>
		                    <th>Date (Last modification)</th>
		                    <th>Comment</th>
		                </thead>
					    <tbody id="simulations_tbody"></tbody>
					</table>
				</fieldset>
	              
                    
        <!-- 
				<fieldset id="report_header"  class="border_dark">
					<legend>Check DSS log file against Expert System simulation</legend>
					<p>This tool lets you check the log file of a DSS set of events and the input and output of the Expert System. 
              Select how the simulation will start. It can start from triggering alarms, digital inputs or actions.
              Then it will check the others match the DSS log.
          </p>
					<select id="LogFileSelect">
						<option value="Alarm">Alarms</option>
						<option value="DigitalInput">Digital input</option>
						<option value="Action">Actions</option>
					</select>
					<br>
					<input type="file" id="DSSLogFileUploadInput" name="file" /></td>
					<button id="LogFileUploadButton" onclick="DSSLogFileUpload()">Execute simulation</button>
					<div id="LogFileDebug"></div>
				</fieldset>
				
        
        <fieldset class="border_dark">
					<legend>Simulation consistency check</legend>
					<p>This tool checks every system-based object in the database and returns any object that should have state OFF given the state of the first level parents.</p>
					<div><button  onclick="callCheckStateAllObjects();">Check simulation</button></div>
					<div id="callCheckStateAllObjects_debug"></div>
				</fieldset>
			-->
			</div>
		<script>
			$(document).ready(function(){
        updateBarIcons();
				load_saved_simulations_table();
        document.getElementById('DSSLogFileUploadInput').addEventListener('change', getDSSLogFile)
			});
		</script>
		<div class="footer">
		<?php include("footer.php"); ?>
		</div>
	</body>	
</html>
