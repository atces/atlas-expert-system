<?php

function is_asso($a) {
  foreach(array_keys($a) as $key)
    if (!is_int($key)) return TRUE;
  return FALSE;
}

function jencode($arr){
  $deb=false;
  if(@$_GET["debug"]=="true") $deb=true;
  //if($deb) print_r($arr);
  $str="";
  $ini=true;
  foreach($arr as $key=>$val){
    if($ini) {
      $ini=false;
	  }else{
		  $str=$str.",";
	  }
    //if(is_string($key)){ $str=$str.'"'.$key.'":'; }
    $str=$str.'"'.$key.'":';
    if(is_array($val)){
      if(is_asso($val)){
        $str=$str.'{'.jencode($val).'}';  
      }else{
        $str=$str.'['.jencode($val).']';  
      }
		}else{
		  if(is_numeric($val)){
        $str=$str.'"'.$val.'"';
      }else{
        $str=$str.'"'.$val.'"';
        //$str=$str.$val;
      }
    }	
  }
  return $str;
}

function jparse($str){
	$deb=false;
  if(@$_GET["debug"]=="true") $deb=true;
	if($deb) echo "parse ".$str."<br>";
  $arr=array();
	$key="";
	$val=null;
	$mix=false;
 	$pos=0;
  $ii=0;
  while($ii<strlen($str)){
  	if($deb) echo "parse '".substr($str,$ii,1)."'<br>";
    if(substr($str,$ii,1)=="="){
 			if($deb) echo "found = <br>";
			$key=substr($str,$pos,$ii-$pos);
			$pos=$ii+1;
			if($deb) echo "key is ".$key." <br>";
		}else if(substr($str,$ii,1)=="{" && strpos($str,"}",$ii)!=false){
			$i2=strpos($str,"}",$ii);
			if($deb) echo "found arr: ".substr($str,$ii+1,$i2-$ii-1)."<br>";
			//$val=jparse(substr($str,$ii+1,$i2-$ii-1));
			$val=explode(",",substr($str,$ii+1,$i2-$ii-1));
			$pos=$i2+1;
			$ii=$i2;
			$mix=true;
		}else if(substr($str,$ii,1)==","){
			if($deb) echo "found , <br>";
			if($mix==false){
				$val=substr($str,$pos,$ii-$pos);
	  		$pos=$ii+1;
		}
		if($deb) echo "val is ".$val." <br>";	
		$arr[$key]=$val;
		$key="";
		$val=null;
		$mix=false;
    }
      $ii=$ii+1;
    }
    if($deb) echo "val is ".$val." <br>";	
    $arr[$key]=$val;
    return $arr;
}

?>