## updating Alarm Helper temporary knowledge to oks database

```
cd /eos/user/a/atopes/www/atlas-expert-system-dev/server/data/alarmHelper

source /eos/home-a/atopes/www/atlas-expert-system-dev/server/python/setup.sh

scp root@atlas-expert-system-el9.cern.ch:/tmp/root/tmp/666/* fromServer/.

python update_oks_data.py /eos/user/a/atopes/www/atlas-expert-system-dev/data/sb.data.xml -d <days since last update (or larger)>
```
(the last command can be run with -v and -s sandbox)

then

```
git add sb.data.xml
```

### Troubleshooting

- if an event cannot be commited, this can be due to illegal characters that might have been introduced when copying text from Elisa logs into the description or title. Remove them in the JSON file, and try again
- the alarm helper cannot save changes -> this might be due to size limitations of the server communication. Its time to update the oks (and to introduce a method for larger json commands)
