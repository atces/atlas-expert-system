#!/bin/bash
#########################################
# Standalone compiler for ATCES Watchdog
# Requires gcc-c++
# yum install gcc-c++
# Carlos.Solans@cern.ch
#########################################

(

hn=`python3 -c 'import socket; print ("-".join(socket.gethostname().split("-")[:-1]))'`
cwd=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
SRC="${cwd}/../src"
INC="-I ${cwd}/../."
DST="${cwd}/../build"
GCC="g++"
CFL="-Wall -fPIC "
CLK="-shared"
DDD="-D DEBUG -D DAEMON -D ATCES_HOST=\"${hn}\""

LIBS=""
OBJS=""
LIB_FILES="AtcesCommands"
LIB_NAME="Atces"
BIN_FILES="atces_watchdog"

echo "Removing old files"
rm -rf $DST
mkdir -p $DST
cd $DST

echo "Compiling library sources"
for f in $LIB_FILES
do
  cmd="$GCC $DDD $INC $CFL -c -o $DST/$f.o $SRC/$f.cpp"
  echo $cmd
  echo `$cmd`
  OBJS="$OBJS $DST/$f.o"
done

echo "Linking library"
cmd="$GCC $CLK $OBJS -o $DST/lib$LIB_NAME.so"
echo $cmd
echo `$cmd`
LIBS="$LIBS -L$DST -l$LIB_NAME"

echo "Creating static library"
cmd="ar rcs $DST/lib$LIB_NAME.a $OBJS"
echo $cmd
echo `$cmd`

echo "Compiling binaries"
for f in $BIN_FILES
do
  cmd="$GCC $DDD $INC $CFL -c -o $DST/$f.o $SRC/$f.cpp"
  echo $cmd
  echo `$cmd`
  cmd="$GCC $DST/$f.o -o $DST/$f -Wl,-Bstatic $LIBS -Wl,-Bdynamic"
  echo $cmd
  echo `$cmd`
done
)
