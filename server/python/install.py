#!/usr/bin/env python
#######################################################
# Preliminary installer for the expert system server
#
# Carlos.Solans@cern.ch
# May 2017
# Updated 
#######################################################
import os
import sys
import argparse

dest = os.path.dirname(os.path.realpath(__file__))
host = "https://atlas-expert-system.web.cern.ch/server/python"

parser = argparse.ArgumentParser()
parser.add_argument("-s","--source",help="source")
parser.add_argument("-t","--type",help="type")
parser.add_argument("-d","--destination",help="destination")
args=parser.parse_args()

if args.type and "dev" in args.type: 
    host = "https://atlas-expert-system-dev.web.cern.ch/server/python"
    pass

if args.destination: dest=args.destination
if args.source: host=args.source
if not host.startswith("https://"): host="https://"+host

files = ["airdryer.py",
         "action.py",
         "alarm.py",
         "atces.py",
         "aug.py",
         "board.py",
         "camera.py",
         "codihelper.py",
         "coolingloop.py",
         "coolingpneumaticvalve.py",
         "coolingstation.py",
         "coolingsystem.py",
         "coolingvacuumpipe.py",
         "compressor.py",
         "compressedairsystem.py",
         "computer.py",
         "crate.py",
         "cryostat.py",
         "delayedaction.py",
         "digitalinput.py",
         "dsu.py",
         "dsslog.py",
         "dsslogaction.py",
         "dssevent.py",
         "electricalbreaker.py",
         "electricalcabinet.py",
         "electricalrepartitionboxus15.py",
         "electricalrepartitionboxusa15.py",
         "electricalswitch.py",
         "elevator.py",
         "evacuationalarm.py",
         "evacuationdigitalinput.py",
         "evacuationcentral.py",
         "evacuationsensor.py",
         "evacuationsiren.py",
         "feedthrough.py",
         "fta.py",
         "ftajson.py",
         "gassystem.py",
         "gascentral.py",
         "gassensor.py",
         "graphhelper.py",
         "group.py",
         "heater.py",
         "heatexchanger.py",
         "helper.py",
         "interlock.py",
         "lacs.py",
         "lighting.py",
         "loadhelpers.py",
         "magnet.py",
         "messenger.py",
         "metainfo.py",
         "minimax.py",
         "minimaxcentral.py",
         "mpccalculation.py",
         "networkrouter.py",
         "networkservicegroup.py",
         "networksubnet.py",
         "networkswitch.py",
         "networkhelper.py",
         "oscilloscope.py",
         "pipe.py",
         "plc.py",
         "plcmodule.py",
         "pneumaticcontroller.py",
         "pneumaticvalve.py",
         "powerdistributionsystem.py",
         "powersupply.py",
         "powerstrip.py",
         "pptxhelper.py",
         "pressuresensor.py",
         "probabilitytools.py",
         "ProbTree.py",
         "pump.py",
         "rack.py",
         "rackelectricaldistribution.py",
         "rackturbine.py",
         "session.py",
         "server.py",
         "simulations.py",
         "simulationdbupdater.py",
         "setup.sh",
         "singleboardcomputerblade.py",
         "smokecentral.py",
         "smokesensor.py",
         "sniffercentral.py",
         "sniffermodule.py",
         "sniffertiroir.py",
         "subdetector.py",
         "switchboard.py",
         "system.py",
         "tank.py",
         "thermalscreen.py",
         "thermoswitch.py",
         "transformer.py",
         "treehelper.py",
         "twidobox.py",
         "ups.py",
         "updateMenuTree.py",
         "vacuumpump.py",
         "vacuumvalve.py",
         "ventilationsystem.py",
         "vessel.py",
         "virtualmachine.py",
         "watersensor.py",
         "watersystem.py",
         "watervalve.py",
         "zone.py",
         ]
dirs = ["fta"]
#files1 = [f for f in os.listdir(dest) if os.path.isfile(os.path.join(dest, f)) and str(f).endswith(".py")]
#files_diff=[f for f in files1 if f not in files]
#print (files_diff)

for f in files:
    print ("Checkout %s/%s => %s/%s" % (host,f,dest,f))
    os.system("rm -rf %s/%s " % (dest,f))
    os.system("curl -s %s/%s -o %s/%s" % (host,f,dest,f))
    os.system("chmod +x %s/%s" % (dest,f))
    pass
for d in dirs:
    if os.path.exists("%s/%s" % (dest,d)): continue
    print ("mkdir %s/%s " % (dest,d))
    os.system("mkdir %s/%s" % (dest,d))
    pass
print ("Have a nice day")
