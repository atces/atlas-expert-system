#!/usr/bin/env python
#############################################
# Feedthrough helper
# Carlos.Solans@cern.ch
# May 2019
#############################################

import helper

class FeedthroughHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("Feedthrough")
    print ("Have a nice day")
