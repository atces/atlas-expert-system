#!/usr/bin/env python
# ############################################
# Helper class to deal with tree logic
# 
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
# Florian.Haslbeck@cern.ch
# Gustavo.Uribe@cern.ch
# ############################################

import sys
from datetime import datetime
import json
import traceback
import config
import re

def log(ss):
    print("%s\t%s"%(timestamp(),ss))
    pass

def timestamp():
    return datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]

class TreeHelper:
    def __init__(self, conn_pool, sess_pool, helpers, classes):
        log("TreeHelper init")
        self.conn_pool = conn_pool
        self.sess_pool = sess_pool
        self.helpers = helpers
        self.classes = classes
        pass
        
    def GetIntervenedSystemsWithLocation(self,tid):
        try:
            objs = self.conn_pool[tid].get_objs("SystemBase")
            obj_list = {}
            obj_list["Unknown"]=[]
            for obj in objs:
                #log ("--> %s" % obj.UID())
                if obj["switch"]=="off":
                    #log ("--> %s" % obj.UID())
                    if obj["default"]=="on":
                        if obj['location'] == "":
                            obj_list["Unknown"].append({obj.UID():obj.class_name()})
                        else:
                            if obj['location'] not in obj_list: obj_list[obj["location"]]=[]
                            obj_list[obj["location"]].append({obj.UID():obj.class_name()})
                            pass
                        pass
                    pass
                pass
            return obj_list
            pass
        except Exception as inst:
            log ("Error in GetIntervenedSystemsWithLocation")
            print (inst)
            print (sys.exc_info()[0])
            return "Error"
        pass


    def GetAffectedSystemsWithLocation(self,tid):
        try:
            objs = self.conn_pool[tid].get_objs("SystemBase")
            obj_list = {}
            obj_list["Unknown"]=[]
            for obj in objs:
                #log ("--> %s" % obj.UID())
                if obj["state"]=="off":
                    #log ("--> %s" % obj.UID())
                    if obj["default"]=="on":
                        if obj['location'] == "":
                            obj_list["Unknown"].append({obj.UID():obj.class_name()})
                        else:
                            if obj['location'] not in obj_list: obj_list[obj["location"]]=[]
                            obj_list[obj["location"]].append({obj.UID():obj.class_name()})
                            pass
                        pass
                    pass
                pass
            return obj_list
            pass
        except Exception as inst:
            log ("Error in GetAffectedSystemsWithLocation")
            print (inst)
            print (sys.exc_info()[0])
            return "Error"
        pass


    def GetAffectedSystems(self,tid):
        try:
            objs = self.conn_pool[tid].get_objs("SystemBase")
            affected_systems = {}
            for obj in objs:
                if obj["state"]=="off" :#and obj.class_name() in sysclasses
                    if obj["default"]=="on":
                        affected_systems[obj.UID()]=obj.class_name()
                        pass
                    pass
                pass
            return affected_systems
        except Exception as inst:
            log ("Error in def GetAffectedSystems()")
            return None
        pass

    def get_all_objects_of_class(self, class_name):
        try:
            obj_list = {}
            objs = conn_pool[tid].get_objs(class_name)
            for obj in objs:
                obj_list[obj.UID()]=obj.UID()
                pass
            return obj_list
        except Exception as inst:
            log ("Exception in def get_all_objects_of_class")
            return None
        pass

    def get_parents(self,obj,aff,level=1000):
        parents=[]
        #print "[def]get_parents"
        #print "get_parents() - level: ", level, " ", "getting parents of ",obj.UID()
        parents.extend(self.helpers[obj.class_name()].get_parents(obj))
        for parent in parents:
            if not parent.UID() in aff:
                aff[parent.UID()]=parent
                if level>0: self.get_parents(parent,aff,level-1)
                pass
            pass
        pass
    
    def get_parents_tree(self,tree,obj,aff,level=5, child="null",req="?", relation="rel?"):
        reports=[]
        if child != "null":
            tree.append([level, obj.class_name(), obj.UID(), child, obj.get_string("state"),req, relation])
            reports.extend(helpers[obj.class_name()].get_parents(obj))
            pass
        else:
            tree.append([level,obj.class_name(),obj.UID(), obj.get_string("state"),req, relation])
            reports.extend(helpers[obj.class_name()].get_parents(obj))
            pass
        counter=0
        #recursion
        for report in reports:
            parents= report[2]
            for parent in parents:
                if not parent.UID() in aff:
                    req=report[1]
                    relation=report[0]
                    counter=counter+1
                    aff[parent.UID()]=parent
                    if level>0: self.get_parents_tree(tree,parent,aff,level-1, obj.UID(), req , relation)
                    pass
                pass
            pass
        pass


    def saveFullTableToFile(self):
        obj_list = self.searchObjectNoClass(tid,"",["description","otherIds"])
        with open('fullTable.json', 'w') as outfile:
            json.dump(obj_list, outfile)
            pass
        pass

    def getFullTableFromFile(self):
        log ("getFullTableFromFile---------------1")
        with open('fullTable.json') as json_file:
            data = json.load(json_file)
            pass
        log ("getFullTableFromFile---------------2")
        return data
    def string_found(self, string1, string2):
        if re.search(r"\b" + re.escape(string1) + r"\b", string2):
            return True
        return False

    def searchObjectNoClass(self,tid,criteria_raw,fields):
        ignore_classes=["Session"]
        if criteria_raw=="":
            vcriteria=[""]
            pass
        else:
            criteria_raw=criteria_raw.strip()
            vcriteria=criteria_raw.split()
            pass
        ret={}
        ret["1"]={}#Likehood 1 string in UID
        ret["2"]={}#Likehood 2 string in otherids
        ret["3"]={}#Likehood 3 full string in description
        try:
            for criteria in vcriteria:
                for cc in self.classes:
                    if cc in ignore_classes: continue
                    objs = self.conn_pool[tid].get_objs(cc)
                    for obj in objs:
                        if obj.UID() in ret["1"]: continue
                        if criteria=="":
                            ret["1"][obj.UID()] = cc
                            pass
                        elif criteria.lower().replace("/","_") in obj.UID().lower().replace("/","_"):
                            ret["1"][obj.UID()] = cc
                            pass
                        pass
                    pass
                pass
            for criteria in vcriteria:
                if criteria=="": continue
                for cc in self.classes:
                    if cc in ignore_classes: continue
                    objs = self.conn_pool[tid].get_objs(cc)
                    for obj in objs:
                        if obj.class_name() != cc: continue
                        if obj.UID() in ret["1"] or obj.UID() in ret["2"]: continue 
                        if not "otherIds" in self.helpers[cc].attrs: continue
                        for item in obj["otherIds"]:
                            if criteria.lower() in item.lower():
                                ret["2"][obj.UID()] = cc
                                break
                            pass
                        pass
                    pass
                pass
            for criteria in vcriteria:
                if criteria=="": continue
                for cc in self.classes:
                    if cc in ignore_classes: continue
                    objs = self.conn_pool[tid].get_objs(cc)
                    for obj in objs:
                        if obj.class_name() != cc: continue
                        if obj.UID() in ret["1"] or obj.UID() in ret["2"]  or obj.UID() in ret["3"]: continue 
                        if not "description" in self.helpers[cc].attrs: continue
                        if self.string_found(criteria.lower(),obj["description"].lower()): ret["3"][obj.UID()] = cc
                        pass
                    pass
                pass
            
            pass
        except Exception as inst:
            log("Error in searchObjectNoClass")
            print (inst)
            print (sys.exc_info()[0])
            pass
        return ret
