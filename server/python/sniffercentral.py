#!/usr/bin/env python
#############################################
# Sniffer Central helper
# Carlos.Solans@cern.ch
# February 2019
#############################################

import helper

class SnifferCentralHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("SnifferCentral")
    print ("Have a nice day")
    pass
