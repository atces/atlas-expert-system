#!/usr/bin/env python

# Remote Database client
#
# Carlos.Solans@cern.ch
# November 2017

import os
import sys
import socket
import json

class Client:
    def __init__(self,host,port):
        self.host=host
        self.port=port
        self.sock=None
        self.verbose=False
        pass
    def setVerbose(self,enable):
        self.verbose=enable
        pass
    def open(self):
        self.sock = socket.socket()
        try:
            if self.verbose: print ("Connecting to %s:%s" % (self.host,self.port))
            self.sock.connect((self.host,self.port))
            return True
        except:
            print ("Error: cannot connect to %s:%s" % (self.host,self.port))
            return False
        pass
    def close(self):
        if self.verbose: print("Close connection")
        self.sock.close()
        pass    
    def test(self,sreq):
        try:
            test = json.loads(sreq)
            return True
        except:
            return False
    def send(self,sreq):
        try:
            if self.verbose: print("Send request: %s" % sreq)
            self.sock.send(sreq.encode())
            pass
        except:
            print(sys.exc_info())
            print ("Error: cannot send request")
            return None
        try:
            if self.verbose: print("Recv size")
            srep = self.sock.recv(8)
            esz = int(srep.decode(), 16)
            if self.verbose: print ("Size: %s" % esz)
            msz = 0
            srep = ""
            if self.verbose: print ("Recv reply")
            while( msz < esz ):
                msg = self.sock.recv(1000)
                msg = msg.decode('utf-8')
                srep+=msg
                msz +=len(msg)
                pass
            if self.verbose: print ("Reply: %s" % srep)
            return srep
        except:
            print(sys.exc_info())
            print ("Error: cannot read reply")
            return None
        pass
    pass

if __name__=='__main__':
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('host')
    parser.add_argument('request')
    parser.add_argument('-p','--port',help="9999=watchdog, 9998=server",type=int,default=9999)
    parser.add_argument('-v','--verbose',action="store_true")
    args = parser.parse_args()

    cli=Client(args.host,args.port)
    cli.setVerbose(args.verbose)
    cli.open()
    print (cli.send(args.request))
    cli.close()

