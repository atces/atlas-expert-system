#!/usr/bin/env python
# ############################################
# Compressed Air System Helper
# Carlos.Solans@cern.ch
# June 2017
# ############################################

import helper

class CompressedAirSystemHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("CompressedAirSystem")
    print ("Have a nice day")
