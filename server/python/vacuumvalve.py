#!/usr/bin/env python
#############################################
# VacuumValve Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
#############################################

import helper

class VacuumValveHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("VacuumValve")
    print ("Have a nice day")
