#!/usr/bin/env python
#############################################
# Gas Sensor helper
# ignacio.asensi@cern.ch
# carlos.solans@cern.ch
# Aug 2018
# April 2020: remove AL2, AL3
#############################################

import helper

class GasSensorHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
    helper.Helper().cmdline("GasSensor")
    print ("Have a nice day")
