#!/usr/bin/env python
# Expert system tool to update the php code of the menu
# Ignacio.Asensi@cern.ch
# Feb 2018
#
# This requires ordereddict
# sudo port install py-pip
# sudo port select --set pip pip27
# sudo pip install ordereddict
#
# Modified to use native collections and json modules
# Carlos.Solans@cern.ch
# June 2019 
#
# Modified to use simulator.php as unified page
# Ignacio
# March 2020

import os
import sys
import json
import datetime
from collections import OrderedDict

now = datetime.datetime.now()
snow = datetime.date.strftime(now,"%H:%M:%S %d-%m-%Y")

print("Starting updateMenuTree at %s" % snow)

#files
menuTree_file='/eos/user/a/atopes/www/atlas-expert-system-dev/login/data/menuTree.json'
menuOrder_file='/eos/user/a/atopes/www/atlas-expert-system-dev/login/data/menuTreeOrder.json'
menuPhp_path='/eos/user/a/atopes/www/atlas-expert-system-dev/login/menuTree.php'

#this allows to run from anywhere
basedir=os.path.dirname(os.path.realpath(__file__))
menuTree_file='%s/../../login/data/menuTree.json' % basedir
menuOrder_file='%s/../../login/data/menuTreeOrder.json' % basedir
menuPhp_path='%s/../../login/menuTree.php' % basedir

#Get json menu tree
print ("Open menu tree: %s" % os.path.realpath(menuTree_file))
menuTree=[]
menuTree_str=open(menuTree_file)
menuTree = json.load(menuTree_str, object_pairs_hook=OrderedDict)


#Get json menu tree order
print ("Open menu order: %s" % os.path.realpath(menuOrder_file))
menuOrder=[]
menuOrder_str=open(menuOrder_file)
menuOrder=json.load(menuOrder_str, object_pairs_hook=OrderedDict)

#Begin writing file
lines=[]
lines.append("<?php");
lines.append("/*************************************");
lines.append("* MenuTree was automatically generated");
lines.append("* by updateMenuTree.py on ");
lines.append("* %s " %snow);
lines.append("* Please do not modify");
lines.append("*************************************/");
lines.append("include_once(\"functions.php\");");
lines.append("?>");

#advanced menu
lines.append("<div id='Advanced_menu' hidden>")
for entrie in menuTree:
    if menuTree[entrie]["isSimulatorPage"]==True: 
        line="<div id='%s' class='cursor headerbutton_div menuentry'><a class='headerbutton_a' href='<?=$stage;?>/login/simulator.php?page=%s%s&lang=<?=$lang?>'><?=$strings->get('%s');?></a></div>" % (entrie,str(menuTree[entrie]["path"]), entrie, entrie)
    else: line="<div id='%s' class='cursor headerbutton_div menuentry'><a class='headerbutton_a' href='<?=$stage;?>/login/%s%s.php?lang=<?=$lang?>'><?=$strings->get('%s');?></a></div>" % (entrie,str(menuTree[entrie]["path"]), entrie, entrie)
    lines.append(line)
    pass

lines.append("</div>")
lines.append("")

#default menu
lines.append("<?php if(!isset($lang)){$lang=\"en\";} ?>")
lines.append("<div id='Default_menu'>")
lines.append("<div id='Default_menu_folder'  class='cursor' onclick='default_menu_folder(this)'>Unfold</div>")
counter=0
for key in menuOrder:
    print ("Key: %s" % key)
    if not isinstance(menuOrder[key], list):
        print (" => Single")
        for page in menuTree:
            if not key.lower() in menuTree[page]: continue
            print (" => add page: %s" % page)
            counter+=1
            line ="<div id='entrie_%s' class='cursor headerbutton_div menuentry L1 %s' >" % (counter, key)
            if menuTree[entrie]["isSimulatorPage"]==True: 
                line+="<a class='headerbutton_a L1' href='<?=$stage;?>/login/simulator.php?page=%s%s&lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
            else: line+="<a class='headerbutton_a L1' href='<?=$stage;?>/login/%s%s.php?lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
            line+="<?=$strings->get('%s');?></a></div>" % page
            lines.append(line)
            pass
        pass
    elif isinstance(menuOrder[key], list):
        print (" => List")
        print (" => add page: %s" % key)
        counter+=1
        line ="<div id='entrie_%s' class='cursor headerbutton_div menuentry L1' onclick=\"menu_toggle(this,'%s','L1')\">" % (counter,key)
        line+="<i id='itag-%s' class=\"fa fa-angle-right cursor\" aria-hidden=\"true\"></i>" % (key)
        line+="<?=$strings->get('%s');?></div>" % (key)
        lines.append(line)
        if len(menuOrder[key])==0: 
            print (" => Sub")
            for page in menuTree:
                if not key in menuTree[page] and not key.lower() in menuTree[page]: continue
                print ("  => add page: %s" % page)
                counter+=1
                line ="<div id='entrie_%s' class='cursor headerbutton_div menuentry L2 %s' style='display:none'>" % (counter,key)
                if menuTree[page]["isSimulatorPage"]==True: 
                    line+="<a class='headerbutton_a L2' href='<?=$stage;?>/login/simulator.php?page=%s%s&lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
                else: line+="<a class='headerbutton_a L2' href='<?=$stage;?>/login/%s%s.php?lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
                line+="<?=$strings->get('%s');?></a></div>" % (page)
                lines.append(line)
                pass
            pass
        else:
            print (" => Sub-sub")
            for key2 in menuOrder[key]:
                print (" Key2: %s" % key2)
                counter+=1
                line ="<div id='entrie_%s' class='cursor headerbutton_div menuentry L2 %s' style='display:none' onclick=\"menu_toggle(this,'%s')\">" % (counter, key, key2.replace(" ","_"))
                line+="<i id='itag-%s' class=\"fa fa-angle-right cursor\" aria-hidden=\"true\"></i>" % (key2.replace(" ","_"))
                line+="<?=$strings->get('%s');?></div>" % (key2)
                lines.append(line)
                for page in menuTree:
                    kk=None
                    if key in menuTree[page]: kk=key
                    elif key.lower() in menuTree[page]: kk=key.lower()
                    else: continue
                    if not key2 in menuTree[page][kk] and not key2.lower() in menuTree[page][kk]: continue
                    print ("  => add page: %s" % page)
                    counter+=1
                    line ="<div id='entrie_%s' class='cursor headerbutton_div menuentry L3 %s' style='display:none'>" % (counter,key2.replace(" ","_"))
                    if menuTree[page]["isSimulatorPage"]==True: 
                        line+="<a class='headerbutton_a L3' href='<?=$stage;?>/login/simulator.php?page=%s%s&lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
                    else: line+="<a class='headerbutton_a L3' href='<?=$stage;?>/login/%s%s.php?lang=<?=$lang;?>'>" % (menuTree[page]["path"],page)
                    line+="<?=$strings->get('%s');?></a></div>" % (page)
                    lines.append(line)
                    pass
                pass
            pass
        pass
    pass
lines.append("</div>")

print ("Write menu php: %s" % os.path.realpath(menuPhp_path))
menuPhp_file=open(menuPhp_path, "w")
for line in lines:
    menuPhp_file.write(line+"\n")
    pass
menuPhp_file.close()
print ("Have a nice day")

# Index page menu generator
