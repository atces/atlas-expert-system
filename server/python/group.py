#!/usr/bin/env python
#############################################
# Group
# ignacio.asensi@cern.ch
# Carlos.Solans@cern.ch
# Oct 2017
# August 2018: fix cmdline
#############################################

import helper

class GroupHelper (helper.Helper):
    pass
    
if __name__ == '__main__':
    
    helper.Helper().cmdline("Group")
    print ("Have a nice day")
