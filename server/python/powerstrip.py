#!/usr/bin/env python
#############################################
# PowerStrip helper
# Carlos.Solans@cern.ch
# June 2019
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class PowerStripHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("PowerStrip")
    print ("Have a nice day")
