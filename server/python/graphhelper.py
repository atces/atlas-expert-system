#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Helper class for process the ES data using graphs 

import networkx as nx
import config
import logging
from collections import OrderedDict
from itertools import combinations 
from itertools import chain
from statistics import mean
import json

class GraphHelper:
	def __init__(self,conn_pool, sess_pool,helpers, classes,graph=None,feeding_graph=None,depending_graph=None,objs_cycles_graph=None,out_centrality_dict=None, verbose=False):
		logging.basicConfig(format='%(asctime)-15s %(message)s')
		self.logger=logging.getLogger(__name__)
		self.logger.setLevel(logging.DEBUG if verbose else logging.INFO)
		self.logger.info("Graph helper init.")	
		self.helpers = helpers
		self.classes = classes
		self.conn_pool = conn_pool
		self.sess_pool = sess_pool
		self.graph=graph
		self.feeding_graph=feeding_graph
		self.depending_graph=depending_graph
		self.objs_cycles_graph=objs_cycles_graph
		self.out_centrality_dict=out_centrality_dict
		self.verbose = verbose
		self.reversed_feeding_graph=None
		self.centrality_per_subsystem={}
		self.expert_reliability_per_class={clazz:2 for clazz in self.classes} # 1: Poorly reliable 2:Reliable 3:Highly reliable
		self.expert_reliability_per_class['DigitalInput']=3
		self.expert_reliability_per_class['NetworkSubnet']=3
		self.expert_reliability_per_class['VirtualMachine']=3
		self.expert_reliability_per_class['AUG']=3
 
	def setGraph(self,graph):
		self.graph=graph

	def load_graph_from_file(self,graph_path=None):
		self.logger.info("Loading graph from file %s..."%graph_path)	
		self.graph=nx.MultiDiGraph(nx.read_gexf(graph_path)) 

	def generate_objs_cycles_graph(self,cycles, file_path=None):
		self.logger.info("Creating graph cycles per obj...")	
		objs_cycles_graph=nx.Graph()
		for cycle in cycles:
			for element in cycle:
				objs_cycles_graph.add_edge("cycle%s"%hash(str(cycle)),element) 
		if file_path: nx.write_gexf(objs_cycles_graph,file_path)
		self.objs_cycles_graph=objs_cycles_graph
		return objs_cycles_graph	

	def create_graph(self,db,file_path=None):
		expert_system_graph=nx.MultiDiGraph()
		self.logger.info("Graph construction started...")	
		helpers=self.helpers
		for clazz in self.classes:
			for obj in db.get_objs(clazz):
				expert_system_graph.add_node(obj.UID())			
				expert_system_graph.nodes[obj.UID()]['class']=obj.class_name()
				for attribute in obj.__schema__['attribute']: expert_system_graph.nodes[obj.UID()][attribute]=str(obj[attribute])
				for relation in obj.__schema__['relation']: 
					node_list=obj[relation] if isinstance(obj[relation],list) else [obj[relation]]
					for node in node_list: 
						if node:
							expert_system_graph.add_edge(obj.UID(),node.UID(),key=relation)
							if "feed" in helpers[obj.class_name()].attrs[relation] and helpers[obj.class_name()].attrs[relation]["feed"]: 
								expert_system_graph[obj.UID()][node.UID()][relation]["feed"]=True
							if "gather" in helpers[obj.class_name()].attrs[relation] and helpers[obj.class_name()].attrs[relation]["gather"]: 
								expert_system_graph[obj.UID()][node.UID()][relation]["gather"]=True
		self.logger.info("Graph construction ended. "+str(len(expert_system_graph))+" nodes added.")	
		if file_path: nx.write_gexf(expert_system_graph,file_path)
		self.graph=expert_system_graph
		return expert_system_graph

	#Downlink: provider => dependant
	def generate_feeding_graph(self,file_path=None):
		graph=self.graph
		self.logger.info("Obtaining the feeding graph...")	
		graph_feedings=nx.MultiDiGraph()
		graph_feedings.add_nodes_from(graph.nodes(data=True))
		for edge,attr in graph.edges.items():
			if 'feed' in attr and attr['feed']: 
				graph_feedings.add_edge(edge[0],edge[1],key=edge[2])
		if file_path: nx.write_gexf(graph_feedings,file_path)
		self.feeding_graph=graph_feedings
		return graph_feedings

	def calculate_out_centrality(self):
		self.logger.info("Calculating eigenvector out centrality...")	
		self.out_centrality_dict=nx.eigenvector_centrality(nx.DiGraph(self.feeding_graph).reverse(),250)
		#self.logger.info("Calculating closeness out centrality...")	
		#self.out_centrality_dict=nx.closeness_centrality(self.feeding_graph.reverse()) #takes 6 seconds more
		#self.logger.info("Calculating communicability_betweenness_centrality out centrality...")	
		#self.out_centrality_dict=nx.communicability_betweenness_centrality(nx.DiGraph(self.feeding_graph).to_undirected()) #Not working probable due to the Python version
		#self.out_centrality_dict={uid:20000-i for i,uid in enumerate(nx.voterank(nx.MultiDiGraph(self.feeding_graph).reverse()))} #No all elements included
		return self.out_centrality_dict	

	def calculate_granularity_per_subsystem(self):
		self.logger.info("Calculating the granularity per subsystem...")	
		counter_per_subsystem={}
		for node in self.graph.nodes:
			if 'subsystem' in self.graph.nodes[node]:
				if self.graph.nodes[node]['subsystem'] not in counter_per_subsystem:
					counter_per_subsystem[self.graph.nodes[node]['subsystem']]=0 
				counter_per_subsystem[self.graph.nodes[node]['subsystem']]+=1 
		self.granularity_per_subsystem={subsystem:counter_per_subsystem[subsystem]/len(self.graph.nodes) for subsystem in counter_per_subsystem}
		

	def calculate_centrality_per_subsystem(self):
		self.logger.info("Calculating the centrality per subsystem...")	
		counter_per_subsystem={}
		sum_centrality_per_subsystem={}
		for node,target,key in self.feeding_graph.edges:
			if 'subsystem' in self.graph.nodes[node]:
				if self.graph.nodes[node]['subsystem'] not in counter_per_subsystem:
					counter_per_subsystem[self.graph.nodes[node]['subsystem']]=0 
					sum_centrality_per_subsystem[self.graph.nodes[node]['subsystem']]=0 
				counter_per_subsystem[self.graph.nodes[node]['subsystem']]+=1 
				sum_centrality_per_subsystem[self.graph.nodes[node]['subsystem']]+=self.out_centrality_dict[node] 
		self.centrality_per_subsystem={subsystem:sum_centrality_per_subsystem[subsystem]/counter_per_subsystem[subsystem] for subsystem in counter_per_subsystem}
		return self.centrality_per_subsystem

	#Uplink: provider <= dependant
	def generate_depending_graph(self,pattern=None,file_path=None):
		graph=self.graph
		self.logger.info("Obtaining the depending graph...")	
		if pattern: logger.info("Ignoring relations without the string '"+pattern+"'")	
		graph_depending=nx.MultiDiGraph()
		graph_depending.add_nodes_from(graph.nodes(data=True))
		for edge,attr in graph.edges.items():
			if 'gather' in attr and attr['gather']: 
				if not pattern or pattern in edge[2]: 
					graph_depending.add_edge(edge[0],edge[1],key=edge[2])
		if file_path: nx.write_gexf(graph_depending,file_path)
		self.depending_graph=graph_depending
		return graph_depending

	def print_tree(self,graph,node,level=0):
		print(''.join([' ' for i in range(0,level)])+node+'\n|')
		for target in graph[node]:
			for relationship in graph[node][target]: 
				print('|-----'+relationship+'----'+target)		


	def get_cycle_path(self,graph,node):
		self.logger.info("Detecting path...")	
		try:
			path=nx.find_cycle(graph,node) 
			self.logger.debug("Found a cycle of "+str(len(list(path)))+" steps")
		except:
			self.logger.warning("Path of cycle in "+str(node)+" is not possible to obtain. Error in the algorithm.")
			path=[]
		return path
			
	def detect_cycles(self,graph):
		self.logger.info("Detecting cycles...")	
		cycles=list(nx.simple_cycles(graph)) #simple_cycles or find_cycle (other functions doen't work over multidigraphs  		
		return cycles

	def remove_subcycles(self,cycles):
		self.logger.info("Removing sub-cycles...")
		for cycle in cycles.copy():
			if cycle in cycles: 
				for cycle2 in cycles.copy():
					if not cycle==cycle2 and  set(cycle2).issubset(cycle) and cycle2 in cycles: 
						cycles.remove(cycle2)
		self.logger.debug("%i cycles after the sub-cycles removal"%len(cycles))
		return cycles

	def get_object(self,db,uid):
		return db.get_obj(self.graph.nodes[uid]['class'],uid) if uid in self.graph.nodes and db.test_object(self.graph.nodes[uid]['class'],uid) else None

	def get_class_name(self,uid):
		return self.graph.nodes[uid]['class'] if uid in self.graph.nodes else None

	def change_switch(self,tid,uid,elementsjson=None,ctime=0):
		self.logger.debug("ChangeSwitch()")
		drep={}
		drep["Reply"]="OK"
		db=self.conn_pool[tid]
		obj=self.get_object(db,uid)
		if not obj:
			self.logger.error("%s is not found"%uid)
			drep["Reply"]="Error"
			drep["Error"]="Object not found"
			return drep
		new_state="off" if obj["switch"]=="on" else "on"
		obj.set_enum("switch",new_state)
		action_number=self.helpers["Session"].addAction(self.sess_pool[tid],"[%s, %s]"%(uid,new_state))
		self.helpers[obj.class_name()].add_log(obj, "[%s] Switched OFF by user"%action_number)
		action_number=self.helpers["Session"].getActionNumber(self.sess_pool[tid])
		reasons,affected=self.propagate_change_effect(db,obj,ctime)
		if self.verbose: self.helpers["Session"].addComputation(self.sess_pool[tid],action_number, reasons)
		if elementsjson == None: elementsjson = affected
		for uid in elementsjson: 
			if isinstance(uid,str):
				if uid in affected: drep[uid]={"Switch":affected[uid].get_string("switch"),"State": affected[uid].get_string("state")} 
				else: 
					class_name=self.get_class_name(uid);
					if class_name==None: continue
					drep[uid]={"Switch":db.get_obj(class_name,uid)["switch"],"State": db.get_obj(class_name,uid)["state"]}
					pass
		#drep["affected"]=list(affected.keys())
		return drep
	
	# if return_iterations=True returns (the list of affected (state='on' included), number of iterations over the affected)
	def propagate_change_effect(self,db,first_obj,ctime=0,max_state_changes=15,return_iterations=False):
		self.logger.debug("Obtaining the list of nodes that can be affected...")
		nodes_generator = nx.bfs_edges(self.feeding_graph, first_obj.UID())
		nodes=[related_objs for obj, related_objs in nodes_generator]
		self.logger.debug("Propagating the change effect...")
		reasons=[]
		affected=OrderedDict([(first_obj.UID(),first_obj)])
		affected.update([(uid,self.get_object(db,uid)) for uid in nodes])
		for i in range(max_state_changes):
			perturbated=[]
			for uid in list(affected.keys()): 
				prev_state=affected[uid]["state"]
				ret=self.helpers[affected[uid].class_name()].update_state(affected[uid]._obj, ctime)
				if ret: reasons.append("%s, %s" % (uid,ret))
				if not prev_state==affected[uid]["state"]: perturbated.append(uid)
			self.logger.debug("%s perturbated in T=%i"%(perturbated,i))
			if len(perturbated)==0: break 
		if len(perturbated)>0: self.logger.debug("unestable elements: %s"%perturbated)
		if return_iterations: return affected,i+1
		return reasons,affected

	def get_common_parents(self,objs):
		self.logger.debug("Get_common_parents ...")
		if not self.reversed_feeding_graph: self.reversed_feeding_graph=self.feeding_graph.reverse()
		common_predecessors=[]
		for obj in objs:
			#predecessors_generator=nx.bfs_predecessors(self.reversed_feeding_graph,obj)
			predecessors_generator=nx.bfs_edges(self.reversed_feeding_graph,obj)
			if len(common_predecessors)==0: common_predecessors=[predecessor for child,predecessor in predecessors_generator]+objs
			else:
				common_predecessors=[predecessor for child,predecessor in predecessors_generator if predecessor in common_predecessors]
		return common_predecessors

	# Most Probable Cost (MPC) 
	# faulty_systems	Number of systems that are considered to fail at the same time. It increase a lot the resolving time.
	def find_MPC(self,db,objs,ctime=0,faulty_systems=1,is_exhaustive=True,return_complexity_params=False,calculation_obj=None):
		self.logger.info("Finding MPC (faulty_systems=%s,is_exhaustive=%s)..."%(faulty_systems,is_exhaustive))
		common_predecessors=self.get_common_parents(objs)
		#leave only systembase predecessors
		common_predecessors=[obj_uid for obj_uid in common_predecessors if db.test_object("SystemBase",obj_uid)]
		common_predecessors=sorted(common_predecessors, key=lambda x:mean([nx.shortest_path_length(self.feeding_graph,x,y) for y in objs])) 
		common_predecessors=sorted(common_predecessors,key=lambda x:self.expert_reliability_per_class[self.get_class_name(x)] )
		data_per_predecessor={}
		num_results_per_tuple_size={}
		#counter of total iterations, i.e. number of times the function update_state of the helpers is called
		objs_classes={self.feeding_graph.nodes[obj]["class"]:True for obj in objs}
		counter_iterations=0
		MPC=[]
		num_obj_off_per_predecessor_tuple={}
		tuple_size=faulty_systems
		self.logger.info("Validating %i point%s of failure ..."%(tuple_size,'s' if tuple_size>1 else ''))
		num_results=1
		try_num=0
		exact_match=False #Flag to indicate that an exact match have been found 
		combinations_tuple=combinations(common_predecessors,tuple_size)
		combinations_tuple = sorted(combinations_tuple, key = lambda x:common_predecessors.index(x[tuple_size-1]))
		self.logger.debug("Combination are:%s"%combinations_tuple)
		for predecessor_tuple in combinations_tuple:
		    try_num+=1
		    print('.',end='',flush=True)
		    for predecessor in predecessor_tuple:
		        faulty_obj=self.get_object(db,predecessor)
		        faulty_obj.set_enum("switch","off")
		        affected,iterations=self.propagate_change_effect(db,faulty_obj,ctime,return_iterations=True)
		        if return_complexity_params:
		            counter_iterations+=iterations*len(affected)	
		            data_per_predecessor[predecessor]={"number of sucessors":len(affected),"subsystem":self.graph.nodes[predecessor]['subsystem']}
		    if is_exhaustive:#Exahustive mode 
		        previous_MPC_list=list(set(chain.from_iterable(MPC))) if tuple_size>1 else MPC
		        obj_off=sorted([obj.UID() for obj in affected.values() if obj["state"]=="off" and obj.class_name() in objs_classes and obj.UID() not in previous_MPC_list and obj.UID() not in predecessor_tuple ])
		        if sorted(objs)==obj_off :
		            exact_match=True
		            MPC.append(predecessor_tuple if tuple_size>1 else predecessor_tuple[0])
		            MPC=sorted(MPC,key=lambda x:mean([self.expert_reliability_per_class[self.get_class_name(elem)] for elem in x]) if isinstance(x,tuple) else self.expert_reliability_per_class[self.get_class_name(x)] )
		            self.logger.info("The most probable cause%s %s: %s"%("s" if len(MPC)>1 else "", "are" if len(MPC)>1 else "is",MPC))
		            num_results+=1
		    else:#Non-exahustive mode
		        obj_off=sorted([obj.UID() for obj in affected.values() if obj["state"]=="off" and obj.class_name() in objs_classes]) 
		        num_obj_off_per_predecessor_tuple[predecessor_tuple]=len(obj_off)
		        if set(objs).issubset(set(obj_off)): 
		            previous_MPC_list=list(set(chain.from_iterable(MPC))) if tuple_size>1 else MPC
		            MPC.append(predecessor_tuple if tuple_size>1 else predecessor_tuple[0])
		            MPC=sorted(MPC,key=lambda x:mean([self.expert_reliability_per_class[self.get_class_name(elem)] for elem in x]) if isinstance(x,tuple) else self.expert_reliability_per_class[self.get_class_name(x)] )
		            MPC=sorted(MPC,key=lambda x:num_obj_off_per_predecessor_tuple[x if isinstance(x,tuple) else tuple([x])])
		            self.logger.info("The most probable cause%s %s: %s"%("s" if len(MPC)>1 else "", "are" if len(MPC)>1 else "is",MPC))
		            num_results+=1
		    db.abort() #Can be replaced by self.generate_feeding_graph() if propagate_change_effect works better without the OKS database
		    if calculation_obj: 
		        self.helpers["MPCCalculation"].update_status(db,calculation_obj,"Analyzing common parents...")
		        self.helpers["MPCCalculation"].update_progress(db,calculation_obj,try_num/len(combinations_tuple)*100)
		        self.helpers["MPCCalculation"].update_results(db,calculation_obj,MPC,self)
		    num_results_per_tuple_size[tuple_size]=num_results
		self.logger.info("The most probable cause%s %s: %s"%("s" if len(MPC)>1 else "", "are" if len(MPC)>1 else "is",MPC))
		if calculation_obj: self.helpers["MPCCalculation"].update_status(db,calculation_obj,"Finished.")
		if return_complexity_params: return MPC,{"total_iterations":counter_iterations,"number_of_predecessors":len(common_predecessors),"data_per_predecessor":data_per_predecessor,"num_results_per_tuple_size":num_results_per_tuple_size,"exact_match":exact_match}
		return MPC
