#!/usr/bin/env python
#############################################
# Evacuation Central helper
# ignacio.asensi@cern.ch
# Nov 2020
#############################################

import helper

class EvacuationCentralHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("EvacuationCentral")
    print ("Have a nice day")
