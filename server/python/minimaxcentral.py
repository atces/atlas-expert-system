#!/usr/bin/env python
#############################################
# Minimax Central helper
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
# August 2018
#############################################

import helper

class MinimaxCentralHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("MinimaxCentral")
    print ("Have a nice day")
