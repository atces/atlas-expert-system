#!/usr/bin/env python
#############################################
# Camera helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# gustavo.uribe@cern.ch
# February 2020
#############################################

from networkhelper import NetworkHelper as NetHelper
import helper

class CameraHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("Camera")
    print ("Have a nice day")
    
