#!/usr/bin/env python
# ############################################
# CoolingSystem Helper
# Carlos.Solans@cern.ch
# May 2019
# ############################################

import helper

class CoolingSystemHelper(helper.Helper):
    pass


if __name__ == '__main__':
    helper.Helper().cmdline("CoolingSystem")
    print ("Have a nice day")
