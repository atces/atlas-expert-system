#!/usr/bin/env python
#############################################
# Water Sensor helper
# ignacio.asensi@cern.ch
# January 2022
#############################################

import helper

class WaterSensorHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
    helper.Helper().cmdline("WaterSensor")
    print ("Have a nice day")
