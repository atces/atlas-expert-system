#!/usr/bin/env python
#############################################
# Oscilloscope helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# gustavo.uribe@cern.ch
# February 2020
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class OscilloscopeHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("Oscilloscope")
    print ("Have a nice day")
    
