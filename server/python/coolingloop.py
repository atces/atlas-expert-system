#!/usr/bin/env python
# ############################################
# CoolingLoop Helper
# Carlos.Solans@cern.ch
# Miguel.Garcia.Garcia@cern.ch
# July 2018
# August 2018: fix cmdline
# ############################################

import helper

class CoolingLoopHelper(helper.Helper):
    pass
  

if __name__ == '__main__':

    helper.Helper().cmdline("CoolingLoop")
    print ("Have a nice day")
