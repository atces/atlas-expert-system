#!/usr/bin/env python
# Expert system inhibit reminder tool
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
#########################################
# Dec 2017 : first version
# Jan 2022 : Get password from database
# Jan 2025 : use anonymous mail service
#########################################

# status: requested, archived, approved, 
# inhibited: yes, no

import os
import time
import json
import config
import smtplib
import datetime
import argparse

jsonfile="/eos/user/a/atopes/www/atlas-expert-system/login/data/requestsDB.json" 
recipients=["atlas-dss@cern.ch",]
sender="no-reply@cern.ch"
passwd=""
smtp="cernmx.cern.ch"
port=25

parser=argparse.ArgumentParser()
parser.add_argument("-j","--json",help="inhibit request file. Default: %s"%jsonfile, default=jsonfile)
parser.add_argument("-r","--recipients", help="recipients. Default: %s"%str(recipients), default=recipients, nargs="*")
parser.add_argument("-s","--sender",help="sender email. Default: %s"%sender, default=sender)
parser.add_argument("-p","--password",help="sender password. Default: %s"%passwd, default=passwd)
parser.add_argument("-S","--smtp",help="SMTP host. Default: %s"%smtp, default=smtp)
parser.add_argument("-P","--port",help="SMTP port. Default: %s"%port, default=port, type=int)
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
args=parser.parse_args()

#from dateutil.parser import parse
#from cryptography.fernet import Fernet
#print("Get secret key")
#key=open(os.path.expanduser("~")+"/private/akey",'r').read()
#print("Create crypto tool")
#f = Fernet(key)
#print("Open db")
#db=config.Configuration("oksconfig:sb.data.xml")
#print("Get meta info")
#metainfo_obj=db.get_obj('MetaInfo','Singleton')
#print("Get key")
#asp=f.decrypt(bytes(metainfo_obj.get_string("asp"),'utf-8')).decode("ASCII").replace("atopes","")

#print time stamp
print("Inhibit request reminders started at %s" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

#Get inhibit requests from file
requests=json.load(open(args.json))

for request in requests:
	if args.verbose: print("Process request: %s" %request)
	beginning=datetime.datetime.strptime(requests[request]["beginning"], '%d-%m-%Y %H:%M')
	expiration=datetime.datetime.strptime(requests[request]["expiration"], '%d-%m-%Y %H:%M')
	today=datetime.datetime.today()
	status=requests[request]["status"]
	inhibited=requests[request]["inhibited"]
	days_from_starting=(beginning - today).days
	days_from_ending=(expiration - today).days
	subject=""
	comment=""
	
	if status == "archived":
		continue
	elif days_from_starting<=1 and days_from_starting>=0 and inhibited=="no":
		print ("Request %s starting in %i days. Send reminder" % (request,days_from_starting))
		subject= "Inhibit Request pending"
		comment="Starting in %i days" % (days_from_starting)
		pass
	elif days_from_ending<=1 and days_from_ending>=0 and inhibited=="yes":
		print ("Request %s expiring in %i days. Send reminder" % (request,days_from_ending))
		subject= "Inhibit Request expiring"
		comment="Expiring in %i days\n" % (days_from_ending)
		pass
	elif days_from_ending<=-1 and days_from_ending>=-100 and inhibited=="yes":
		print ("Request %s expired %i days ago. Send reminder" % (request,-1*days_from_ending))
		subject= "Inhibit Request expired"
		comment="Expired %i days ago\n" % (-1*days_from_ending)
		pass
	else:
		continue

	body="This is a reminder to handle an Inhibit Request\n"
	body+="\n"
	body+="Intervention dates:\n"
	body+="Start: %s \n" % requests[request]["beginning"]
	body+="End: %s \n" % requests[request]["expiration"]
	body+=comment
	body+="----------------------------------------------------\n"
	body+="Status: %s \n" % requests[request]["status"]
	body+="Requestor: %s \n" % requests[request]["requestor"]
	body+="Email: %s \n" % requests[request]["mail"].strip().replace(" ","").replace("\n\n","\n").replace("\n",", ")	
	body+="Reason: %s \n" % requests[request]["reason"]
	body+="\n"
	body+="Inhibits: \n"
	for x in requests[request]["alarms"]:
		x=x.strip()
		if len(x)==0: continue
		body+="%s\n"%x
		pass
	body+="\n"
	body+="Link: https://atlas-expert-system.web.cern.ch/login/viewInhibited.php?uid=%s \n" % (requests[request]["id"])
	body+="\n"
	body+="This is an automatic email.\n"
	body+="Have a nice day\n"
	for recipient in args.recipients:
		header = 'To: %s\nFrom: %s\nSubject:%s\n'%(recipient,args.sender,subject)
		msg = header+"\n"+body
		if args.verbose: print(msg)
		smtp=smtplib.SMTP(args.smtp,args.port)
		smtp.sendmail(args.sender, recipient, msg)
		time.sleep(7) #make the mailer happy, 10 mails per minute
		pass
	pass

print("Have a nice day!")
print("Inhibit request reminders finished at %s" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
