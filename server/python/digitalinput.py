#!/usr/bin/env python
#############################################
# DigitalInput
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
# May 2017
# August 2018: fix cmdline
# August 2018: Add DigitalInputProviderBase attribute signalSource
# March 2019: fix update_state inhibit
#############################################

import helper

class DigitalInputHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("DigitalInput")
    print ("Have a nice day")
    pass
