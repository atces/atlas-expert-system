#!/usr/bin/env python
#############################################
# ActionHelper
# Carlos.Solans@cern.ch
# April 2017
# July 2018: reverse the logic
# August 2018: fix the cmdline
#############################################

import helper

class ActionHelper(helper.Helper):
	pass

if __name__ == '__main__':

    helper.Helper().cmdline("Action")
    print ("Have a nice day")
