#!/usr/bin/env python
# ############################################
# Magnet Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
# ############################################

import helper

class MagnetHelper(helper.Helper):
    pass
  
if __name__ == '__main__':

    helper.Helper().cmdline("Magnet")
    print ("Have a nice day")
