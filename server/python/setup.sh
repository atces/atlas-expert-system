#!/bin/bash

export TDAQ_VERSION=tdaq-11-02-01

TDAQPATHS=(/sw/atlas /cvmfs/atlas.cern.ch/repo/sw/tdaq)
for _cand in ${TDAQPATHS[@]}; do
    if [ -d ${_cand}/tdaq/$TDAQ_VERSION ]; then
	export TDAQ_RELEASE_BASE=${_cand}
	export CMAKE_PROJECT_PATH=${_cand}
        break
    fi
done
LCGPATHS=(/sw/atlas/sw/lcg/releases /cvmfs/sft.cern.ch/lcg/releases)
for _cand in ${LCGPATHS[@]}; do
    if [ -d ${_cand} ]; then
	export LCG_RELEASE_BASE=${_cand}
	break
    fi
done

if [ -z ${TDAQ_RELEASE_BASE+x} ]; then echo "TDAQ release base not found"; return 0; fi

echo "---------"
echo "TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE}"
echo "LCG_RELEASE_BASE=${LCG_RELEASE_BASE}"
echo "CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}"
source $TDAQ_RELEASE_BASE/tdaq/$TDAQ_VERSION/installed/setup.sh 
source $TDAQC_INST_PATH/share/cmake_tdaq/bin/setup.sh 
echo "---------"

export PATH=$TDAQ_PYTHON_HOME/bin:$PATH

export MY_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export MY_DB_PATH=$MY_PATH/../../data
export TDAQ_DB_PATH=$MY_DB_PATH:$TDAQ_DB_PATH
export PYTHONPATH=$MY_PATH:$PYTHONPATH
export PYTHONHOME=$(dirname $(dirname $(which python)))
export PATH=$MY_PATH:$PATH

#Network related modules
export ATCN_NETWORK_PROJECT_PATH="/eos/user/g/guribego/atcn-network-analysis/" 
export ATCN_NETWORK_PYTHON_PATH="/eos/user/g/guribego/atcn-network-analysis/Scripts/" 
export PYTHONPATH=$ATCN_NETWORK_PYTHON_PATH:$PYTHONPATH

#setup Root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#missing tools
export PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/gnuplot/5.4.2/${CMTCONFIG}/bin:${PATH}
export LD_LIBRARY_PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/pango/1.48.9/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/cairo/1.17.2/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/harfbuzz/2.7.4/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/libxslt/1.1.38/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
