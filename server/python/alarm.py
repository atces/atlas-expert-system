#!/usr/bin/env python
#############################################
# Alarm helper
# Carlos.Solans@cern.ch
# May 2017
# Alarm logic is reversed:
# State ON = Something is going on
# State OFF = Idle state
# July 2018: reverse the logic
# August 2018: Fix cmdline
# March 2019: Remove obsolete relationships
#############################################

import helper

class AlarmHelper(helper.Helper):
	pass

if __name__ == '__main__':

    helper.Helper().cmdline("Alarm")
    print ("Have a nice day")
