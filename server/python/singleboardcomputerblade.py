#!/usr/bin/env python
#############################################
# SingleBoardComputerBlade helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# gustavo.uribe@cern.ch
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class SingleBoardComputerBladeHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("SingleBoardComputerBlade")
    print ("Have a nice day")
    
