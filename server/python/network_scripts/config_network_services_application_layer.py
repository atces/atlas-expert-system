#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Allows to create/remove new services, add/remove servers, add/remove clients and define criteria for select the clients
import os
import sys
import config
import argparse
import json
import re
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #config.sh
import query_JSON_architecture as query
sys.path.insert(1,'../scripts/')
from add_oks_relation_without_duplication import add_oks_relation_without_duplication

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def change_type(service_obj=None):
	while True:
		type_num=input("Please select the type of service:\n "+'\n '.join([str(i+1)+'.'+value for i,value in enumerate(service_type_range)])+"\n:")
		if type_num.isdigit() and int(type_num)<=len(service_type_range):
			if service_obj: service_obj['networkServiceTypeOffered']=service_type_range[int(type_num)-1]
			return  service_type_range[int(type_num)-1]
		elif type_num in service_type_range:
			if service_obj: service_obj['networkServiceTypeOffered']=type_num
			return  type_num
		else:
			print("Invalid input") 

def new_service():
	service_type=change_type()
	option=input("Is the service provided by a group of servers? (y*/n):")
	if option.startswith("n"):
		server_obj=add_server()
		client_objs=select_clients(server_obj)
		server_obj['networkServiceTypeOffered']=service_type
	else:
		service_name=input("Service name:")
		if not db.test_object("NetworkServiceProviderBase",service_name): 
			service_obj=db.create_obj("NetworkServiceGroup",service_name)
			while True:
				option=input("Do you want to add a server to the service? (y*/n):")
				if option.lower().startswith('n'): break
				server_obj=add_server(service_obj)
			client_objs=select_clients(service_obj)
			service_obj['networkServiceTypeOffered']=service_type
			if len(client_objs)==0:
				db.abort()
				print("Operation aborted. The service should have at least one client")
				input("Prese any key for continue...")
				return 
		else:
			db.abort()
			print("Operation aborted. A service with this name exist already!")
			input("Prese any key for continue...")
			return
	db.commit("")
	print("Service created.")
	input("Prese any key for continue...")

def remove_service():
	list_services()
	service_name=input("Service name:")
	if db.test_object("NetworkServiceProviderBase",service_name):
		service_obj=db.get_obj("NetworkServiceProviderBase",service_name)
		for client in service_obj["hasClient"]:
			if service_obj in client["isClientOf"]: client["isClientOf"].remove(service_obj)
			client["isClientOf"]=client["isClientOf"] #For adding in database transaction
		service_obj['networkServiceTypeOffered']="None"
		if service_obj.class_name()=="NetworkServiceGroup": 
			for server in service_obj['providers']:
				if service_obj in server["providesNetworkService"]: server["providesNetworkService"].remove(service_obj)
				server["providesNetworkService"]=server["providesNetworkService"]
		for obj in service_obj.referenced_by():
			obj=db.get_obj(obj.class_name(),obj.UID())
			for relation in obj.__schema__['relation']:
				if service_obj in obj[relation]: obj[relation].remove(service_obj); obj[relation]=obj[relation]
		db.destroy_obj(service_obj)	
	else:
		db.abort()
		print("Operation aborted. Service not in the database.")
		input("Prese any key for continue...")
		return
	db.commit("")
	print("Service removed.")
	input("Prese any key for continue...")

def list_services():
	print("These are the current services in the database:")
	for service in db.get_objs("NetworkServiceProviderBase"):
		if len(service['hasClient'])>0:
			print(service.UID()+''.join(['\t' for i in range(4-int(len(service.UID())/8))])+"type: "+service['networkServiceTypeOffered'])

def list_clients(service_name):
	print("These are the clients of the service:")
	service_obj=db.get_obj("NetworkServiceProviderBase",service_name)
	for client in service_obj['hasClient']:
		print(client.UID())
	print("Total of clients:"+str(len(service_obj['hasClient'])))
	print("Current selection criteria string is "+service_obj['clientSelectionCriteria'])

def load_default_values_in_selection_criteria(selection_criteria,service_obj,G):
	#Load defaul values
	if 'serviceName' not in selection_criteria: selection_criteria['serviceName']='.*(-IP|-PI|-SQ)'
	if 'type' not in selection_criteria: selection_criteria['type']='(?!ROUTER|SWITCH)'
	sets_per_service=[]
	if 'domain' not in selection_criteria or 'trustingSet' not in selection_criteria  and service_obj:
		selection_criteria['domain']=''
		domains=[]
		if not service_obj.class_name()=="NetworkServiceGroup": servers=service_obj.UID()
		else: servers=[server.UID() for server in service_obj['providers']]
		sets_per_servers_json=query.query_edges(G,source_properties={"label":"","type":"SWITCH|ROUTER"},target_properties={"label":"$|".join(servers)+'$'},edge_properties={"sets":".*EXPOSED TO.*|None|.*IT NETWORK SERVICES.*","domain":""},hidden_properties=["source_label","type"])
		for sets_per_server_json in sets_per_servers_json:
			sets_per_server=json.loads(sets_per_server_json)	
			sets=sets_per_server['sets'].replace('[','').replace(']','').split(', ')
			sets_per_service+=sets
			if sets_per_server['domain'] not in domains: domains+=[sets_per_server['domain']]
			for set in sets:
				if re.match(".*IT NETWORK SERVICES.*",set): domains=[".*"]; sets_per_service=[".*"]; break #This devices are exposed to all the domains
				if re.match(".* EXPOSED TO (.*)",set):	
					domain=re.match(".* EXPOSED TO (.*)",set).group(1)
					if domain not in domains: domains+=[domain]
		selection_criteria['domain']= "$|".join(domains)+'$'
		selection_criteria['trustingSet']= '^'+"$|^".join(list(dict.fromkeys(sets_per_service)))+'$'
		if not "^None$" in selection_criteria['trustingSet']: selection_criteria['trustingSet']+="|^None$" #For the clients in the same domain of the server and not protected by a control set 
	return selection_criteria

def get_clients_using_selection_criteria(selection_criteria,service_obj,G):
	node_attribute=[x for x in G.nodes().values()][0].keys()
	if 'label' not in selection_criteria: selection_criteria['label']=''
	client_results=query.query_edges(G,source_properties={x:y for x,y in selection_criteria.items() if x in node_attribute},edge_properties={x:y for x,y in selection_criteria.items() if x not in node_attribute}) 
	clients=[json.loads(x)['source_label'] for x in client_results ]
			
	#Avoid self dependency
	if not service_obj.class_name()=="NetworkServiceGroup": 
		if service_obj.UID() in clients: clients.remove(service_obj.UID())
	else: 
		servers=[server.UID() for server in service_obj['providers']]
		for server in servers:
			if server in clients: clients.remove(server)
	return clients

def list_servers(service_name):
	service_obj=db.get_obj("NetworkServiceProviderBase",service_name)
	if service_obj.class_name()=="NetworkServiceGroup":
		if len(service_obj['providers'])==0: print("No servers registered. Therefore, it is a black-box service."); return
		print("These are the servers of the service:")
		for server in service_obj['providers']:
			print(server.UID())
	else:
		print("This is the server of the service:")
		print(service_obj.UID())

def add_server(service_obj=None):
	while True:
		server_name=input("Introduce the name of the server:")
		if db.test_object("NetworkPeerBase",server_name):
			server_obj=db.get_obj("NetworkPeerBase",server_name)
			break
		else:
			option=input("Server doesn't exist in the database, Do you want to create it? (y/n*/[q]uit):")
			if option.lower().startswith("y"):
				type=input("Introduce the type of the new server:")
				server_obj=db.create_obj(type,server_name)
				break
			elif option.lower().startswith("q"):
				return 
	if service_obj and service_obj.class_name()=="NetworkServiceGroup": 
		if server_obj not in service_obj['providers']: service_obj['providers']+=[server_obj]
		if service_obj not in server_obj['providesNetworkService']: server_obj['providesNetworkService']+=[service_obj]
	print("Server "+server_obj.UID()+" added.")
	return server_obj

def remove_server(service_obj):
	while True:
		server_name=input("Introduce the name of the server:")
		if db.test_object("NetworkPeerBase",server_name):
			server_obj=db.get_obj("NetworkPeerBase",server_name)
			break
		else:
			input("Server doesn't exist in the database. Operation cancelled.")
			return
	if service_obj and service_obj.class_name()=="NetworkServiceGroup": 
		if server_obj in service_obj['providers']: service_obj['providers'].remove(server_obj); service_obj['providers']=service_obj['providers'] #Confirm assignation for adding in database transaction
		if service_obj in server_obj['providesNetworkService']: server_obj['providesNetworkService'].remove(service_obj);  server_obj['providesNetworkService']= server_obj['providesNetworkService']
	print("Server "+server_obj.UID()+" removed.")
	return server_obj

def save_clients(clients,service_obj,db,G):
	client_objs=[]
	
			
	for client in clients:
		if db.test_object("NetworkPeerBase",client):
			client_obj=db.get_obj("NetworkPeerBase",client)
			if client_obj not in service_obj["hasClient"]: service_obj["hasClient"]+=[client_obj]
			if service_obj not in client_obj["isClientOf"]: client_obj["isClientOf"]+=[service_obj]	
			client_objs+=[client_obj]
			
			#{trusted_set:subnet}
			client_trusted_sets={list(G[s][t].values())[0]['trustingSet']:list(G[s][t].values())[0]['serviceName'] for s,t in G.edges(client)}
			#Make the clients reachable for the servers
			if not all(lanset == 'None' for lanset in client_trusted_sets.keys() ):#Control set case, at least one interface trusting a set
				for server in service_obj['providers']:
					server_sets=[list(G[s][t].values())[0]['sets'] for s,t in G.edges(server.UID())]
					for lanset in client_trusted_sets.keys():
						if lanset in str(server_sets) or re.match(".*IT NETWORK SERVICES.*",str(server_sets)):
							add_oks_relation_without_duplication(server,'isReachableIn',db.get_obj("NetworkSubnet",client_trusted_sets[lanset]))
							add_oks_relation_without_duplication(server,'isReachableThroughASetIn',db.get_obj("NetworkSubnet",client_trusted_sets[lanset]))
			else:
				#{domain:subnet}
				client_domains={list(G[s][t].values())[0]['domain']:list(G[s][t].values())[0]['serviceName'] for s,t in G.edges(client)}
				for server in service_obj['providers']:
					server_domains=[list(G[s][t].values())[0]['domain'] for s,t in G.edges(server.UID())]
					for domain in client_domains.keys():
						if domain not in server_domains:
							server_sets=[list(G[s][t].values())[0]['sets'] for s,t in G.edges(server.UID())]
							#Interdomain set, not in the same domain so accesible via a gate
							if domain in str(server_sets) or re.match(".*IT NETWORK SERVICES.*",str(server_sets)):#Looking for set .* EXPOSED TO {domain}
								add_oks_relation_without_duplication(server,'isReachableIn',db.get_obj("NetworkSubnet",client_domains[domain]))
								add_oks_relation_without_duplication(server,'isReachableThroughASetIn',db.get_obj("NetworkSubnet",client_domains[domain]))
		else:
			print("WARNING: "+client+" not in the Expert System database.")
	#Remove clients not longer in the list 	
	if not set(clients)==set([_client.UID() for _client in service_obj["hasClient"]]):
		clients_objs=service_obj["hasClient"].copy()
		for client_obj in clients_objs:
			if client_obj.UID() not in clients:
				service_obj["hasClient"].remove(client_obj)
				service_obj["hasClient"]=service_obj["hasClient"]
				client_obj["isClientOf"].remove(service_obj)
				client_obj["isClientOf"]=client_obj["isClientOf"]
	elif not set(clients)==set([_client.UID() for _client in service_obj.referenced_by()]): #Removes clients referencing the service but no longer clients (scrap cleaning) 
		clients_objs=[db.get_obj(_obj.class_name(),_obj.UID()) for _obj in service_obj.referenced_by()]
		for client_obj in clients_objs:
			if client_obj.UID() not in clients:
				client_obj["isClientOf"]=[_obj for _obj in client_obj["isClientOf"] if not _obj.UID() == service_obj.UID()]
	return client_objs

def select_clients(service_obj):
	client_objs=[]
	while True:
		client_name=input("Introduce the name of a client or 'regex' for define a selection criteria (press enter to quit):")
		if len(client_name)==0:
			break 
		elif "regex" in client_name:
			while True:
				print("Choose the attribute of the client which you want to use for selection:")
				print(" 1. label/name.")
				print(" 2. type (ROUTER,SWITCH,Computer,Camera,...). Default:'(?!ROUTER|SWITCH)'")
				print(" 3. operatingSystem (CC7, SCL6, Linux, Windows, ...)")
				print(" 4. system (TDAQ,ID,LAR,Pixel,...)")
				print(" 5. building (SDX1,USA15,US15,3196,...)")
				print(" 6. zone (rack name)")
				print(" 7. dhcpProvider (ATLAS,IT)")
				print(" 8. serviceName (IT network service as defined in LanDB. Default:'.*(-IP|-PI|-SQ)'")
				print(" 9. domain (network domain as in LanDB e.g. ATLAS, ATLAS-PIXEL, ...) Default:{Autodetect domain and interdomain sets of servers}")
				print(" 10. sets (sets as defined in LanDB e.g. ATLAS-PIXEL EXPOSED TO ATLAS). ")
				print(" 11. trustingSet (sets trusted by the clients in LanDB e.g. ATLAS CONTROL WATER LEAK). Default:{Autodetect domain and interdomain sets of servers}")
				option=input(":")
				att_per_option={"1":"label","2":"type",'3':"operatingSystem",'4':"system",'5':"building",'6':"zone",'7':"dhcpProvider",'8':"serviceName",'9':"domain",'10':"sets",'11':"trustingSet"}
				if option.isdigit() or option in att_per_option.values():
					if option in att_per_option.values(): option=list(att_per_option.keys())[list(att_per_option.values()).index(option)]
					selection_criteria=json.loads(service_obj['clientSelectionCriteria']) if service_obj['clientSelectionCriteria'] else {}	
					if att_per_option[option] in  service_obj['clientSelectionCriteria']:
						print("The current regex cirteria for "+att_per_option[option]+" is "+selection_criteria[att_per_option[option]])
					regex=input("Introduce the new regex selection criteria for the "+att_per_option[option]+" attribute.\n:")
					selection_criteria[att_per_option[option]]=regex
					if regex=='': del(selection_criteria[att_per_option[option]])
					load_default_values_in_selection_criteria(selection_criteria,service_obj,G)	
					service_obj['clientSelectionCriteria']=json.dumps(selection_criteria)
				else:
					print("Option not valid")
					continue
				print("Current selection criteria string is "+service_obj['clientSelectionCriteria'])
				option=input("Do you want to define a regex selection criteria for other attribute? (y*/n)")
				if option.lower().startswith('n'): 		
					print("Selection criteria saved.")
					print("Listing clients result of the regex query:")
					clients=get_clients_using_selection_criteria(selection_criteria,service_obj,G)
					clients=list(dict.fromkeys(clients))
					for client in clients:
						print(client)
					print(str(len(clients))+" client in total.")
					option=input("Do you confirm that these are the clients of the service "+service_obj.UID()+" ?(y*/n):")
					if option.lower().startswith('n'): continue
					client_objs+=save_clients(clients,service_obj,db,G)
					print("Clients selected and saved.")
					return client_objs
			break
		if db.test_object("NetworkPeerBase",client_name):
			client_obj=db.get_obj("NetworkPeerBase",client_name)
		else:
			option=input("Client doesn't exist in the database, Do you want to create it? (y/n*):")
			if option.startswith("y"):
				type=input("Introduce the type of the new client:")
				client_obj=db.create_obj(type,client_name)
			else:
				continue
		selection_criteria=json.loads(service_obj['clientSelectionCriteria']) if service_obj['clientSelectionCriteria'] else {}	
		if client_obj not in service_obj["hasClient"]: 
			service_obj["hasClient"]+=[client_obj]
			if service_obj not in client_obj["isClientOf"]: client_obj["isClientOf"]+=[service_obj]	
			client_objs+=[client_obj]
			if 'label' in selection_criteria:
				selection_criteria['label']+='|'+client_obj.UID()+'$' 
			else:
				selection_criteria['label']=client_obj.UID()+'$'
			print("Client "+client_obj.UID()+" added.")
		else:
			service_obj["hasClient"].remove(client_obj)
			service_obj["hasClient"]=service_obj["hasClient"] #To add in the database transaction
			if service_obj in client_obj["isClientOf"]:  client_obj["isClientOf"].remove(service_obj); client_obj["isClientOf"]=client_obj["isClientOf"]
			if client_obj in client_objs: client_objs.remove(client_obj)
			selection_criteria['label']="$|".join([name for name in selection_criteria['label'].replace('$','').split('|') if not name==client_obj.UID()])+'$'
			print("Client "+client_obj.UID()+" removed.")
		service_obj['clientSelectionCriteria']=json.dumps(selection_criteria)
		print("Current selection criteria string is "+service_obj['clientSelectionCriteria'])
	return client_objs

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Manage the servers and clients of the network services (application level)')
	parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:"+args.database)
	service_type_range=db.__schema__.data["NetworkServiceProviderBase"]['attribute']['networkServiceTypeOffered']['range']
	graph_file_path=os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf"
	G=query.load_architecture(graph_file_path)
	while True:
		cls()
		option=input("Please select the operation to perform:\n - [N]ew service\n - [M]anage a service\n - [R]emove service\n - [L]ist services\n - List service [C]lients\n - List service [S]ervers\n - [E]xit\n:")
		if option.lower().startswith('n'):
			new_service()
		elif option.lower().startswith('m'):
			list_services()
			service_name=input("Introduce the service name for managing:")
			if db.test_object("NetworkServiceProviderBase",service_name):
				option=input("Select the operation to perform on the "+service_name+" service\n - [A]dd a new server\n - [S]elect clients\n - [R]emove a server\n - Re[N]ame\n - Change [T]ype\n:")
				service_obj=db.get_obj("NetworkServiceProviderBase",service_name)
				if option.lower().startswith('a'):
					add_server(service_obj)
				elif option.lower().startswith('s'):
					list_clients(service_obj.UID())	
					select_clients(service_obj)
				elif option.lower().startswith('r'):
					list_servers(service_obj.UID())	
					remove_server(service_obj)
				elif option.lower().startswith('n'):
					new_name=input("New name for the service:")
					new_service_obj=db.create_obj(service_obj.class_name(),new_name)
					for attribute in service_obj.__schema__['attribute']:
						new_service_obj[attribute]=service_obj[attribute]
					for relation in service_obj.__schema__['relation']:
						new_service_obj[relation]=service_obj[relation] 
					for obj in service_obj.referenced_by():
						obj=db.get_obj(obj.class_name(),obj.UID())
						for relation in obj.__schema__['relation']:
							if service_obj in obj[relation]: obj[relation].remove(service_obj); obj[relation]+=[new_service_obj]
					db.destroy_obj(service_obj) 
					print("Service name changed.")
				elif option.lower().startswith('t'):
					change_type(service_obj)
					print("Service type changed.")
				db.commit("")
			else:
				print("Service not in the database.")
			option=input("Press a key to continue...")
		elif option.lower().startswith('r'):
			remove_service()
		elif option.lower().startswith('l'):
			list_services()
			option=input("Press a key to continue...")
		elif option.lower().startswith('c'):
			list_services()
			service_name=input("Introduce the service name for listing the clients:")
			if db.test_object("NetworkServiceProviderBase",service_name):
				list_clients(service_name)
			else:
				print("Service not in the database.")
			option=input("Press a key to continue...")
		elif option.lower().startswith('s'):
			list_services()
			service_name=input("Introduce the service name for listing the servers:")
			if db.test_object("NetworkServiceProviderBase",service_name):
				list_servers(service_name)
			else:
				print("Service not in the database.")
			option=input("Press a key to continue...")
		elif option.lower().startswith('e'):
			break
