#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Used to updated the network data since the last updating date or one given date following the changelog files

import os
import re
import json
import argparse
import datetime
from load_all import NetworkLoader
import networkx as nx
import sys
from collections import OrderedDict
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #from config.sh
import building_sigles as sigles
import IT_nomenclature
import rack_nomenclature
import query_JSON_architecture as query
sys.path.insert(1,'../scripts/')
from delete_oks_object_with_references import delete_oks_object_with_references 
from add_oks_relation_without_duplication import add_oks_relation_without_duplication,get_or_create_oks_obj 
from power_source_adviser import advice_power_source
from change_type_oks_object import change_type_oks_object

class NetworkUpdater():

	own_relations_per_field=OrderedDict([("system","subsystem"),
					("building","location"),
					("description","description"),
					("landb_link","landbLink"),
					("rack_name","inDependantContainer"),
					("peer_in","isNetworkPeerIn"),
					("reachable_in","isReachableIn"),
					("reachable_through_set_in","isReachableThroughASetIn"),
					("path_in","isPathOfSubnet"),
					("services","isClientOf")
					])
	feeder_relation_per_field={	"rack_name":"isDependantContainerOf",
					"peer_in":"hasNetworkPeer",
					"path_in":"hasPath",
					"services":"hasClient"
					}

	created_services={}
	net_loader=None
	db=None
		
	
	def __init__(self):
		self.net_loader=NetworkLoader(os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf",None)
		self.db=self.net_loader.db

	def load_service(self,service): 
		if service in self.created_services: return #Loads the service only one time
		starpoint_building=IT_nomenclature.get_starpoint_building_of_service(service)
		sigle=sigles.BUILDING_SIGLE_BY_NUMBER[starpoint_building]
		network="Undefined"
		for (source,target,port) in self.net_loader.G.edges:
			if service in self.net_loader.G[source][target][port]['serviceName']:
				if "ATLAS" in self.net_loader.G[source][target][port]['domain']:
					network="ATCN"
				else:
					network=self.net_loader.G[source][target][port]['domain']
		starpoint_name=network+'-'+sigle
		if starpoint_name not in self.net_loader.created_starpoints: self.load_starpoint(starpoint_building,network)
		#Get the paths of the current service (except path of switches)
		results_paths=query.query_edges(self.net_loader.G,source_properties={"label":"","type":"SWITCH","building":"","zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":""},target_properties={"type":"(?!ROUTER|SWITCH)"},edge_properties={"serviceName":service+'$',"domain":""},hidden_properties=["target_type"])
		#print ("Creating service",service,starpoint_building,sigle,network)
		if "EX" not in IT_nomenclature.get_service_type(service) and (len(results_paths)==0 or self.net_loader.created_starpoints[starpoint_name]==json.loads(results_paths[0])['domain']):#Protected or isolated services should not be connected to the starpoint subnet
			service_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",service)
			starpoint_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",starpoint_name)
			add_oks_relation_without_duplication(service_obj,"isPartOfSubnet",starpoint_obj)
			add_oks_relation_without_duplication(starpoint_obj,"hasSubnet",service_obj)
		#Calculating min eccentricity
		path_names=[re.match('.*source_label": "([^"]+)',x).group(1) for x in results_paths]
		min_eccentricity=200 #Any high value
		for path_name in path_names:
			if path_name in self.net_loader.eccentricity[starpoint_name]: min_eccentricity=min(min_eccentricity,self.net_loader.eccentricity[starpoint_name][path_name])
		for json_device in results_paths:
			path_info=self.net_loader._process_result(json_device,"source_")
			path_name=path_info['name']
			#print ("Creating path on service",service,path_name,device_type,sigle_device,rack_name,system)
			#Get the peers of the current service  
			results_peers=query.query_edges(self.net_loader.G,source_properties={"label":path_name,"type":"SWITCH"},target_properties={"label":"","building":"","zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":"","type":"(?!ROUTER)"},edge_properties={"serviceName":service+'$',"domain":"","sets":"","trustingSet":""},hidden_properties=["source_type"])
			created_paths={}
			for json_device in results_peers:
				device_dict=json.loads(json_device)
				peer_info=self.net_loader._process_result(json_device,"target_")
				peer_name=peer_info['name']
				if peer_name not in self.net_loader.eccentricity[starpoint_name] or self.net_loader.eccentricity[starpoint_name][peer_name] >  self.net_loader.eccentricity[starpoint_name][path_name]:#No uplinks
					#Check if the peers are connected with all paths
					paths_counter=0
					for edge in self.net_loader.G[peer_name]:
						if self.net_loader.G.nodes[edge]['type'] == "SWITCH":
							for link in self.net_loader.G[peer_name][edge]:
								if self.net_loader.G[peer_name][edge][link]['serviceName']==service:
									paths_counter+=1
									break
					if len(results_paths)==paths_counter: 
						if path_name not in created_paths:
							created_paths[path_name]=True
							path_info['path_in']=service
							self.load_device(path_info)
						peer_info['peer_in']=service
						self.net_loader.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
						self.load_device(peer_info)
					elif paths_counter==1:
						if path_name+IT_nomenclature.get_reduced_zone(path_name) not in created_paths:
							created_paths[path_name+IT_nomenclature.get_reduced_zone(path_name)]=True
							service_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",service)
							sub_subnet_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",service+'-'+IT_nomenclature.get_reduced_zone(path_name)) 
							add_oks_relation_without_duplication(sub_subnet_obj,"isPartOfSubnet",service_obj)
							add_oks_relation_without_duplication(service_obj,"hasSubnet",sub_subnet_obj)
							path_info['path_in']=[service+'-'+IT_nomenclature.get_reduced_zone(path_name)]
							if path_name in self.net_loader.eccentricity[starpoint_name] and self.net_loader.eccentricity[starpoint_name][path_name] == min_eccentricity: 
								 path_info['path_in']+=[service]
							self.load_device(path_info)
						peer_info['peer_in']=service+'-'+IT_nomenclature.get_reduced_zone(path_name)
						self.net_loader.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
						self.load_device(peer_info)
		self.created_services[service]=True
		return self.net_loader.created_starpoints

	def load_starpoint(self,starpoint_building,network):
		sigle=sigles.BUILDING_SIGLE_BY_NUMBER[starpoint_building]
		starpoint_name=network+'-'+sigle
		self.net_loader.created_starpoints[starpoint_name]=network #By default is the network if not peers, otherwise is replace by the domain name
		starpoint_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",starpoint_name) 
		network_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",network)
		add_oks_relation_without_duplication(network_obj,"hasSubnet",starpoint_obj)
		add_oks_relation_without_duplication(starpoint_obj,"isPartOfSubnet",network_obj)
		#print ("Creating startpoint",starpoint_name)
		results_routers=query.query_nodes(self.net_loader.G,source_properties={"label":"","type":"ROUTER","building":starpoint_building,"zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":""}) #get routers of starpoint
		for result in list(results_routers):
			if network not in IT_nomenclature.get_extended_network(json.loads(result)['label']):
				results_routers.remove(result)
		self.net_loader.eccentricity[starpoint_name]={} #Init the eccentricity to avoid error in case of none routers known in the starpoint
		for json_device in results_routers:
			router_info=self.net_loader._process_result(json_device)
			router_name=router_info['name']
			component=nx.node_connected_component(self.net_loader.IT_G,router_name)
			connected_G=self.net_loader.G.subgraph(component)	
			self.net_loader.eccentricity[starpoint_name]=nx.eccentricity(connected_G)
			router_info['path_in']=starpoint_name
			self.load_device(router_info)
			#print ("Creating router",router_name,rack_name,sigle,system)
			#get the peers of the router
			results_peers=query.query_edges(self.net_loader.G,source_properties={"type":"ROUTER","label":router_name},target_properties={"label":"","building":"","zone":"","system":"","type":"","dhcpProvider":"","operatingSystem":"","description":""},edge_properties={"serviceName":"","domain":"","sets":"","trustingSet":""},hidden_properties=["source_type","source_building"])
			for json_device in results_peers:
				peer_info=self.net_loader._process_result(json_device,"target_")
				self.net_loader.created_starpoints[starpoint_name]=peer_info['domain']
				#Check if the peers are connected with all the starpoint
				router_counter=0
				for edge in self.net_loader.G[peer_info['name']]:
					if self.net_loader.G.nodes[edge]['type'] == "ROUTER":
						router_counter+=1
				if len(results_routers)==router_counter: 
					peer_info['peer_in']=starpoint_name
					self.net_loader.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
				elif router_counter==1:
					router_subnet_obj=get_or_create_oks_obj(self.db,"NetworkSubnet",starpoint_name+"-R"+router_name[-1:])
					add_oks_relation_without_duplication(router_subnet_obj,"isPartOfSubnet",starpoint_obj)
					add_oks_relation_without_duplication(starpoint_obj,"hasSubnet",router_subnet_obj)
					router_info['path_in']=starpoint_name+"-R"+router_name[-1:]
					self.load_device(router_info)
					peer_info['peer_in']=starpoint_name+"-R"+router_name[-1:]
					self.net_loader.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
				else:
					peer_info['peer_in']=starpoint_name
					self.net_loader.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
					pass #Scenario of more than two routers in an starpoint is not considered because is a rare configuration, not present until now
				#print ("Creating device",peer_name,device_type,sigle_device,rack_name,system,"connected to",router_counter,"routers")

	def load_device(self,device_info):

			rack_name=device_info['rack_name'] if 'rack_name' in device_info else None
			device_info['rack_name']=self.net_loader.container_per_other_id[rack_name] if rack_name in self.net_loader.container_per_other_id else rack_name
			device_info_string=''
			pattern=re.compile("(?i).*("+device_info['type']+")")
			for type in self.net_loader.class_per_device_type:
				if (pattern.match(type) and pattern.match(type).end()==len(type)) or self.db.test_object("SystemBase",device_info['name']):
					clazz=self.net_loader.class_per_device_type[type] if not self.db.test_object("SystemBase",device_info['name']) else self.db.get_obj("SystemBase",device_info['name']).class_name()
					device_obj=get_or_create_oks_obj(self.db,clazz,device_info['name'])
					for field in device_info:
						if field in self.own_relations_per_field and device_info[field]:
							device_info_array= device_info[field] if isinstance(device_info[field],list) else [device_info[field]]	
							for value in device_info_array:
								if field in self.feeder_relation_per_field:
									if not self.db.test_object("SystemBase",value):
										feeder_obj=self.db.create_obj(self.net_loader.feeder_class_per_field[field],value)
									else:
										feeder_obj=self.db.get_obj("SystemBase",value)	
									add_oks_relation_without_duplication(device_obj,self.own_relations_per_field[field],feeder_obj)
									add_oks_relation_without_duplication(feeder_obj,self.feeder_relation_per_field[field],device_obj)
								elif self.own_relations_per_field[field] in device_obj.__schema__['relation']:
										feeder_obj=self.db.get_obj("SystemBase",value)	
										add_oks_relation_without_duplication(device_obj,self.own_relations_per_field[field],feeder_obj)
								elif self.own_relations_per_field[field] in device_obj.__schema__['attribute'] and  device_obj.__schema__['attribute'][self.own_relations_per_field[field]]['multivalue']==True:
									if value not in device_obj[self.own_relations_per_field[field]]: device_obj[self.own_relations_per_field[field]]+=[value]
								else:
									device_obj[self.own_relations_per_field[field]]=value
					break

	def process_changelog_file(self,filepath):
		with open(filepath,"r") as file:
			for line in file:
				pattern=re.compile("(?P<sign>\+|\-|\+\+)(?P<type>Node|Edge Attribute|Edge|Attribute|Service|Server|Client)(?P<complement>.*)")
				match=pattern.match(line)
				if match:
					sign=match.group("sign")
					change_type=match.group("type")
					device_info={}
					if "+" in sign:
						if "Node" in change_type: #New node
							pattern=re.compile(" (?P<device_name>((?!:).)*):(?P<attributes>.*)")
							match=pattern.match(match.group("complement"))
							device_info['name']=match.group("device_name")
							attributes=match.group("attributes")
							pattern=re.compile(".*'building'((?!').)*'(?P<building>((?!').)*)'")
							building=pattern.match(attributes).group("building")
							device_info['building']=sigles.BUILDING_SIGLE_BY_NUMBER[building] if building and not building == 'None' else None
							pattern=re.compile(".*'zone'((?!').)*'(?P<zone>((?!').)*)'")
							rack_name=pattern.match(attributes).group("zone")
							device_info['rack_name']=self.net_loader.clean_rack_name(rack_name)
							pattern=re.compile(".*'system'((?!').)*'(?P<system>((?!').)*)'")
							device_info['system']=pattern.match(attributes).group("system").split('|',1)[0]
							pattern=re.compile(".*'type'((?!').)*'(?P<type>((?!').)*)'")
							device_info['type']=pattern.match(attributes).group("type")
							pattern=re.compile(".*'description'((?!').)*'(?P<description>((?!').)*)'")
							device_description=pattern.match(attributes).group("description")
							prev_description=None
							if self.db.test_object("SystemBase",device_info['name']):
								prev_description=self.db.get_obj("SystemBase",device_info['name']).get_string("description")
							if (not prev_description or len(prev_description)==0) and not device_description.startswith("None"):
									device_info['description']=device_description
							device_info['landb_link']="http://landb.cern.ch/portal/devices/"+device_info['name']
							self.load_device(device_info)
							if device_info['rack_name']:
								if not args.interactive: print("Please add power to the device %s. You can use the power_source_adviser script."%device_info['name'])	
								else:
									pattern=re.compile("(?i).*("+device_info['type']+")")
									for type in self.net_loader.class_per_device_type:
										if (pattern.match(type) and pattern.match(type).end()==len(type)) or self.db.test_object("SystemBase",device_info['name']):
											if not self.db.test_object("SystemBase",device_info['name']): self.db.create_obj(self.net_loader.class_per_device_type[type],device_info['name'])
											advice_power_source(self.db,[device_info['name']],force_update=True)
											break
											
						elif "Attribute" in change_type:#Changes in node attributes 
							pattern=re.compile("(?i) +(?P<attribute>zone|type|description|operatingSystem|dhcpProvider|building|system) +change +in +node +(?P<entity>[a-z,\-,_,0-9,<,>,=]+) +now +(?P<now>.*) +before +(?P<before>.*)")
							match_att=pattern.match(match.group("complement"))
							if match_att: 
								entity=match_att.group("entity")
								attribute=match_att.group("attribute")
								now=match_att.group("now")
								before=match_att.group("before")
								if entity in self.net_loader.G.nodes and self.db.test_object("NetworkPeerBase",entity):
									if "type" in attribute:
									    for type in self.net_loader.class_per_device_type:
									        if (pattern.match(type) and pattern.match(type).end()==len(type)):
									            change_type_oks_object(self.db,entity,self.db.get_obj("NetworkPeerBase").class_name(),net_loader.class_per_device_type[type])
									            break
									else:
										building=self.net_loader.G.nodes[entity]['building']
										device_info['name']=entity
										device_info['type']= self.net_loader.G.nodes[entity]['type']
										device_info['system']=self.net_loader.G.nodes[entity]['system'].split('|',1)[0]
										device_info['landb_link']=self.db.get_obj("SystemBase",device_info['name']).get_string("landbLink")
										device_info['building']=sigles.BUILDING_SIGLE_BY_NUMBER[building] if building and not building == 'None' else None
										device_info['dhcpProvider']=self.net_loader.G.nodes[entity]['dhcpProvider']
										device_info['operatingSystem']=self.net_loader.G.nodes[entity]['operatingSystem']
										rack_name=self.net_loader.G.nodes[entity]['zone']
										device_info['rack_name']=self.net_loader.clean_rack_name(rack_name)
										if "zone" in attribute:
											obj=self.db.get_obj("NetworkPeerBase",entity)
											old_containers=obj["containedIn"]+obj["inDependantContainer"]
											obj['containedIn']=[]
											obj['inDependantContainer']=[]
											for old_container in old_containers:
											    old_container["contains"]=[ele for ele in old_container["contains"] if not ele.UID() == entity] 
											    old_container["isDependantContainerOf"]=[ele for ele in old_container["isDependantContainerOf"] if not ele.UID() == entity] 
											if not args.interactive: print("Please add power to the device %s. You can use the power_source_adviser script."%device_info['name'])	
											else:
												pattern=re.compile("(?i).*("+device_info['type']+")")
												for type in self.net_loader.class_per_device_type:
													if (pattern.match(type) and pattern.match(type).end()==len(type)) or self.db.test_object("SystemBase",device_info['name']):
														if not self.db.test_object("SystemBase",device_info['name']): self.db.create_obj(self.net_loader.class_per_device_type[type],device_info['name'])
														advice_power_source(self.db,[device_info['name']],force_update=True)
														break
										if "description" in attribute:
											yn=input("Do you want to keep the description of "+entity+"? \n Previous in Expert System: "+self.db.get_obj("NetworkPeerBase",entity).get_string("description")+"\n New in landb: "+now+"\n Enter (y) for yes or any character for no:") if args.interactive else 'y'
											if not yn.lower().startswith('y'):
												device_info['description']=self.net_loader.G.nodes[entity]['description']
										self.load_device(device_info)
								elif entity in self.net_loader.G.nodes:
								    pattern=re.compile("(?i).*("+self.net_loader.G.nodes[entity]['type']+")")
								    for type in self.net_loader.class_per_device_type:
								        if pattern.match(type) and pattern.match(type).end()==len(type) :
								            self.db.create_obj(self.net_loader.class_per_device_type[type],entity)
								            break
							else:#Edge attributes
								pattern=re.compile("(?i) +(?P<attribute>sets|trustingSet) +change +in +link +(?P<source>[a-z,\-,_,0-9]+):(?P<port>[a-z,0-9,/]+)[<,>,=]+(?P<target>[a-z,\-,_,0-9]+) +now +(?P<now>.*) +before +(?P<before>.*)")
								match_att=pattern.match(match.group("complement"))
								if match_att:
									attribute=match_att.group("attribute")
									path=match_att.group("source")
									entity=match_att.group("target")
									port=match_att.group("port")
									if path in self.net_loader.G.nodes and entity in self.net_loader.G[path] and port in self.net_loader.G[path][entity] and self.db.test_object("NetworkPeerBase",entity):
										building=self.net_loader.G.nodes[entity]['building']
										device_info['name']=entity
										device_info['type']= self.net_loader.G.nodes[entity]['type']
										device_info['trustingSet']=self.net_loader.G[path][entity][port]['trustingSet']
										device_info['peer_in']=[subnet.UID() for subnet in self.db.get_obj("SystemBase",entity)['isNetworkPeerIn']]
										device_info['reachable_in']=[]
										self.net_loader.load_reached_in_on_device_info(device_info,device_info['peer_in'])
										self.load_device(device_info)
						elif "Edge" in change_type:#New Edge
							pattern=re.compile(" (?P<path>.*)->(?P<device_name>((?!:).)*):{u'(?P<port>((?!').)*)'(?P<attributes>.*)")
							match=pattern.match(match.group("complement"))
							device_info['name']=match.group("device_name")
							device_name=device_info['name']
							attributes=match.group("attributes")
							pattern=re.compile(".*'serviceName'((?!').)*'(?P<service>((?!').)*)'")
							service=pattern.match(attributes).group("service")
							self.load_service(service) #Load the complete service in order to avoid the complex logic twice here
						elif "Service" in change_type: #New Service
							pattern=re.compile(" +(?P<service_name>.*)")
							match=pattern.match(match.group("complement"))
							service_name=match.group("service_name")
							self.load_service(service_name)
					else: #If "-" in sign. Node or Edge deleted
						if "Edge" in change_type:
							pattern=re.compile(" (?P<path>.*)->(?P<device_name>((?!:).)*):{u'(?P<port>((?!').)*)'(?P<attributes>.*)")
							match=pattern.match(match.group("complement"))
							device_name=match.group("device_name")
							path_name=match.group("path")
							attributes=match.group("attributes")
							pattern=re.compile(".*'serviceName'((?!').)*'(?P<service>((?!').)*)'")
							service=pattern.match(attributes).group("service")							
							
							if self.db.test_object("SystemBase",device_name) and self.db.test_object("NetworkSubnet",service):
								device_obj=self.db.get_obj("SystemBase",device_name)
								subnet_name=service if service+'-'+IT_nomenclature.get_reduced_zone(path_name) not in [peer.UID() for peer in device_obj["isNetworkPeerIn"]] else service+'-'+IT_nomenclature.get_reduced_zone(path_name) #takes the sub-subnet in case it is only connected to one device 						
								#Up to now there is uniqueness in the name of the service, so the domain is not needed to make the difference
								device_obj["isNetworkPeerIn"]=[subnet for subnet in device_obj["isNetworkPeerIn"] if not subnet_name == subnet.UID() ] #Remove the subnet 
								subnet_obj=self.db.get_obj("NetworkSubnet",subnet_name)
								subnet_obj["hasNetworkPeer"]=[peer for peer in subnet_obj["hasNetworkPeer"] if not peer.UID()== device_name ] #Remove the device from the subnet
								#TODO Review this logic
								if service in [peer.UID() for peer in device_obj["isNetworkPeerIn"]]: #The last sub-subnet was removed
									device_obj["isNetworkPeerIn"]=[subnet for subnet in device_obj["isNetworkPeerIn"] if not service == subnet.UID() ] #Remove the subnet 
									subnet_obj=self.db.get_obj("NetworkSubnet",service)
									subnet_obj["hasNetworkPeer"]=[peer for peer in subnet_obj["hasNetworkPeer"] if not peer.UID() == device_name ] #Remove the device from the subnet
									

						elif "Node" in change_type:#Node deleted
							pattern=re.compile(" (?P<device_name>((?!:).)*):(?P<attributes>.*)")
							match=pattern.match(match.group("complement"))
							device_info['name']=match.group("device_name")
							if self.db.test_object("NetworkPeerBase",device_info['name']):
								obj=self.db.get_obj("NetworkPeerBase",device_info['name'])
								delete_oks_object_with_references(self.db,obj)
						elif "Service" in change_type:#Service deleted
							pattern=re.compile(" +(?P<service_name>.*)")
							match=pattern.match(match.group("complement"))
							service_name=match.group("service_name")
							if self.db.test_object("NetworkSubnet",service_name):
								service_obj=self.db.get_obj("NetworkSubnet",service_name)
								delete_oks_object_with_references(self.db,service_obj)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", "--date", help="Update from specific date, format: YYYY.MM.DD", required=False)
	parser.add_argument("-i", "--interactive", help="Interactive mode for updating descriptions and power description.", action="store_true",required=False) #When is not interactive all questions are answer by yes and no power description is provided
	args = parser.parse_args()
	net_updater=NetworkUpdater()
	db=net_updater.db
	dir_path=os.environ['ATCN_NETWORK_PROJECT_PATH']+'/atcn_architecture/Changelogs/'
	filenames = os.listdir(dir_path)
	date=datetime.date.today()

	if args.date:
		pattern=re.compile("(?P<year>[0-9]{4}).(?P<month>0[1-9]|1[0-2]?).(?P<day>[0-9]{2})")
		match=pattern.match(args.date)
		if match:
			date=datetime.date(int(match.group('year')),int(match.group('month')),int(match.group('day')))
		else:
			print ("Incorrect date format. Use YYYY.MM.DD")
	else:
		date=datetime.datetime.strptime(db.get_obj('MetaInfo','Singleton').get_string("lastNetworkUpdateDate"),"%Y-%b-%d").date()

	for filename in filenames:
		pattern=re.compile("changes_(?P<year>[0-9]{4})(?P<month>0[1-9]|1[0-2]?)(?P<day>[0-9]{2})")
		match=pattern.match(filename)
		if match:
			file_date=datetime.date(int(match.group('year')),int(match.group('month')),int(match.group('day')))
			if file_date>=date:
				net_updater.process_changelog_file(dir_path+filename)

	#delete Undefined startpoint
	for subnet in db.get_objs("NetworkSubnet"):
		if "Undefined" in subnet.UID():
			delete_oks_object_with_references(db,subnet)

	db.get_obj("MetaInfo","Singleton")["lastNetworkUpdateDate"]=str(datetime.date.today()-datetime.timedelta(days=1))
	db.commit('')
