import config
import csv
import loadhelpers
from graphhelper import GraphHelper

if __name__== "__main__":
	database="../../../data/sb.data.xml"
	db=config.Configuration("oksconfig:%s"%database)
	gLH = loadhelpers.LoadHelpers(db)
	classes=gLH.getClasses()
	searchableClasses=gLH.getSearchableClasses()
	sysclasses=gLH.getSysClasses()
	helpers=gLH.getHelpers()
	graph_helper=GraphHelper(None,None,helpers,classes)
	graph_helper.create_graph(db)
	with open('network_services_to_remove.csv', 'r',newline='') as csvfile:
		reader = csv.reader(csvfile,delimiter=',')
		for row in reader:
			obj=graph_helper.get_object(db,row[0])
			if obj:
				obj['isNetworkPeerIn']=[obj_x for obj_x in obj['isNetworkPeerIn'] if obj_x.UID() not in row]
				for network_uid in row[1:len(row)]:
					network=graph_helper.get_object(db,network_uid)
					if network: network['hasNetworkPeer']=[obj_x for obj_x in network['hasNetworkPeer'] if not obj_x.UID()==obj.UID()  ]
	db.commit("")
