#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Used to updated the network data of a service using the information of landb collected in the ATCN architecture graph

import argparse
import os
from update_data import NetworkUpdater

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("service", help="Service to be updated")
	args = parser.parse_args()
	net_updater=NetworkUpdater()
	net_updater.load_service(args.service)
	net_updater.db.commit('')
