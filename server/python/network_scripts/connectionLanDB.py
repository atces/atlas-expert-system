#!/usr/bin/python
# Authentication for consume the Network SOAP service

import os
import getpass
import config
from suds.client import Client
from suds.sax.element import Element
from suds.xsd.doctor import ImportDoctor, Import
from cryptography.fernet import Fernet

# Client setup
url = 'https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL'
imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
doc = ImportDoctor(imp)
client = Client(url, doctor=doc, cache=None)

# Authentication
def authenticate(username=getpass.getuser(),password=None):
	if not password:
		home_dir=os.path.expanduser("~")
		if  os.path.exists(home_dir+"/private/akey") and os.path.exists("../../../data/sb.data.xml"):
			with open(home_dir+"/private/akey","r") as key_file:
				key=key_file.read()
				f = Fernet(key)
				saltpassword=f.decrypt(bytes(config.Configuration("oksconfig:../../../data/sb.data.xml").get_obj('MetaInfo','Singleton').get_string("asp"),'utf-8')).decode('utf-8')
				password=saltpassword.replace(getpass.getuser(),'')
		elif os.path.isfile(home_dir+"/.netrc"):
			with open(home_dir+"/.netrc") as authentication_file:
				is_machine=False
				is_user=False
				for line in authentication_file:
					if "machine" in line and "cern.ch" in line:
						is_machine=True
					if  "login" in line and username in line:
						is_user=True
					else:
						is_machine=False
					if is_user and "password" in line:
						password=line[line.index("password ")+9:-2]
						break 
			if not password: return False 
		else:
			return False
	token = client.service.getAuthToken(username, password, 'CERN')
	authenticationHeader = Element('Auth').insert(Element('token').setText(token))
	client.set_options(soapheaders=authenticationHeader)
	print ("Authentication done - SOAP Landb")
	return True
