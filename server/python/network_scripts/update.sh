#!/bin/sh
#source ./config.sh
if ! [ $ATCN_NETWORK_PROJECT_PATH ]; then 
	echo "Put the correct path to the network project in the config.sh. The network project is available in https://gitlab.cern.ch/guribego/atcn-network-analysis.git"
	exit -1 
fi
architecture_path=$ATCN_NETWORK_PROJECT_PATH/atcn_architecture/atcn_architecture.gexf

#exit 0
#python ../scripts//update_rack_subsystems.py
#python update_atonr_clients_and_servers.py
if python update_data.py -i ; then
	python update_network_services_application_layer.py
	python delete_forgotten_peers.py
fi
