## Network Scripts

These scripts are for loading and updating the network data used by the expert system. The main source of the information is LanDB, however, the git project "ATCN network analysis" is used in order to get a graph representation of the LanDB data and enriched by some Glance AES information. The graph representation allows to perform advance queries over the data and reduce the execution time, because all processing is made on memory and the data is requested only in one opportunity.

### load_all.sh

Used for loading all the network related data. This script doesn't perform any removal of obsolete data.

### update.sh

Used for keeping the information of the expert system aligned with the information in LanDB. Also loads the ATONR servers and clients.

### config.sh

There should be written the path of the git project "ATCN network analysis".
To run it:
`source config.sh`

### config_network_service.py

Used for configuring the application level network services. It allows us to perform CRUD over services, servers, and clients. config.sh should be executed first.

### Other scripts

Other scripts are steps of the updating process. Is not advisable to execute them separately, unless you know how it works.
config.sh should be called first.
  
