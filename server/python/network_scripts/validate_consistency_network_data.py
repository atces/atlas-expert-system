#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# validate consitency of the network data with the landb data.

import os
import sys
import argparse
import networkx as nx
import config
import logging
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #config.sh
from building_sigles import BUILDING_SIGLE_BY_NUMBER as sigle
import IT_nomenclature as nomenclature

def validate_consistency(db,G):
	logger=logging.getLogger(__name__)
	logger.debug("Graph construction started...")	
	expert_system_graph=nx.MultiDiGraph()
	for peer in db.get_objs("NetworkPeerBase"):
		expert_system_graph.add_node(peer)			
		for attribute in peer.__schema__['attribute']: expert_system_graph.nodes[peer][attribute]=peer[attribute]
		for relation in peer.__schema__['relation']: 
			node_list=peer[relation] if isinstance(peer[relation],list) else [peer[attribute]]
			for node in node_list: expert_system_graph.add_edge(peer,node,key=relation)
	#Nodes
	logger.debug("Graph construction ended. Checking the missing nodes in the expert system...")	
	expert_system_str_graph=[ y.UID() for y in expert_system_graph.nodes]
	nodes_missing_in_expert_system=[ x for x in G.nodes if x not in expert_system_str_graph and not nomenclature.get_service_type(x)] #Nodes missing in the ES, except network services and GW (e.g. S3125-U2-IP34-GW).
	if len(nodes_missing_in_expert_system)>0:
		logger.info("The next elements are present in LanDB but not in the expert system:")
		logger.info(nodes_missing_in_expert_system)
		logger.info("Total missing elements:"+str(len(nodes_missing_in_expert_system)))
		logger.info("This usually happens because the device type is not registered in the variable class_per_device_type.")
	logger.debug("Checking nodes with a different location in LanDB than in the expert system...")

	#Location 
	nodes_different_location=[]
	try:
		nodes_different_location=[x for x in expert_system_graph.nodes if x.UID() in G.nodes and not sigle[G.nodes[x.UID()]['building']]==None and not x['location']==sigle[G.nodes[x.UID()]['building']]] 
	except Exception as e:
		print("Error, new building added and not present in building_sigles.py")
		print("Building"+e)
	if len(nodes_different_location)>0:
		logger.info("The next elements have different location in LanDB than in the expert system:")
		for node in nodes_different_location:
			print(node.UID()+''.join(['\t' for i in range(0,4-int(len(node.UID())/8))])+"LanDB/ACES:"+str(G.nodes[node.UID()]['building'])+''.join(['\t' for i in range(0,4-int(len(G.nodes[node.UID()]['building'])/8))])+"ES:"+expert_system_graph.nodes[node]['location'])
		logger.info("Total elements:"+str(len(nodes_different_location)))

	#Description
	logger.debug("Checking nodes with a different description in LanDB than in the expert system...")
	nodes_different_description=[x for x in expert_system_graph.nodes if x.UID() in G.nodes and "None" not in G.nodes[x.UID()]['description'] and not x['description']==G.nodes[x.UID()]['description'].replace("'","")] 
	if len(nodes_different_description)>0:
		logger.info("The next elements have different description in LanDB than in the expert system:")
		for node in nodes_different_description:
			print(node.UID()+''.join(['\t' for i in range(0,4-int(len(node.UID())/8))])+"LanDB:"+str(G.nodes[node.UID()]['description'])+''.join(['\t' for i in range(0,6-int(len(G.nodes[node.UID()]['description'])/16))])+"ES:"+expert_system_graph.nodes[node]['description'])
		logger.info("Total elements:"+str(len(nodes_different_description)))
	#Rack/container
	logger.debug("Checking nodes with a different rack/container in LanDB/ACES than in the expert system...")
	nodes_different_rack=[]
	for x in expert_system_graph.nodes:
		if x.UID() in G.nodes:
			containers=[]
			if 'containedIn' in x.__schema__['relation']  and  len(x['containedIn'])>0: containers=x['containedIn']
			if 'inDependantContainer' in x.__schema__ ['relation'] and len(x['inDependantContainer'])>0: containers+=x['inDependantContainer']
			if len(containers) == 0 and not G.nodes[x.UID()]['zone'].lower() in ["none","rack","0000","atlas"]:
		        	nodes_different_rack.append(x)
			else:
			    for container in containers:
   			        if not G.nodes[x.UID()]['zone'].lower() in ["none","rack","0000","atlas"]:
   			            if not container.UID().replace(' ','-').replace('=','-').replace('.','') in G.nodes[x.UID()]['zone'].replace(' ','-').replace('=','-').replace('.',''):
   			                nodes_different_rack.append(x) 
   			                for otherid in container['otherIds']:
   			                    if otherid in G.nodes[x.UID()]['zone']: 
   			                        nodes_different_rack.remove(x) 
   			            elif x in nodes_different_rack:
   			                nodes_different_rack.remove(x) 
   			                break
					 
	if len(nodes_different_rack)>0 or len(nodes_different_rack)>0:
		logger.info("The next elements have different rack/container in LanDB than in the expert system:")
		for node in nodes_different_rack:
			print(node.UID()+''.join(['\t' for i in range(0,4-int(len(node.UID())/8))])+"LanDB/ACES:"+str(G.nodes[node.UID()]['zone'])+''.join(['\t' for i in range(0,4-int(len(G.nodes[node.UID()]['zone'])/8))])+"ES:"+str([x.UID() for x,y in expert_system_graph[node].items() if 'containedIn' in y.keys() or 'inDependantContainer' in y.keys()]))
		logger.info("Total elements:"+str(len(nodes_different_rack)))

	#isReachableThroughASetIn
	logger.debug("Checking that the sets in landb allows the communication with the subnets in the expert system...")
	nodes_reachables_using_sets={node:service for node in expert_system_graph.nodes if node.UID() in G.nodes and 'isReachableThroughASetIn' in node.__schema__['relation'] and len(node['isReachableThroughASetIn'])>0 for service in node['isReachableThroughASetIn']}
	sets_per_node={node:value for source,target,port in G.edges for att,value in G[source][target][port].items() if att=='sets' and not value=="None" for node in [source,target]}
	domain_per_service={G[source][target][port]['serviceName']:G[source][target][port]['domain'] for source,target,port in G.edges}
	nodes_different_set=nodes_reachables_using_sets.copy()
	for node in nodes_different_set.copy():
		if node.UID() in sets_per_node and "IT NETWORK SERVICES" in sets_per_node[node.UID()].upper():
			del(nodes_different_set[node])
		elif node.UID() not in sets_per_node:
			sets_per_node.update({node.UID():""})
		elif node in nodes_reachables_using_sets and node.UID() in sets_per_node and  domain_per_service[nodes_reachables_using_sets[node].UID()] in sets_per_node[node.UID()]:
			del(nodes_different_set[node])
		
	if len(nodes_different_set)>0:
		logger.info("(NOT WORKING FINE, PLEASE CHECK) The next nodes has a LanDB sets that not all are reachable from the ES subnet:")
		for node in nodes_different_set:
			print(node.UID()+''.join(['\t' for i in range(0,4-int(len(node.UID())/8))])+"LanDB:"+str(sets_per_node[node.UID()])+''.join(['\t' for i in range(0,4-int(len(sets_per_node[node.UID()])/8))])+"ES:"+str([x.UID() for x,y in expert_system_graph[node].items() if 'isReachableThroughASetIn' in y.keys()]))
		logger.info("Total elements:"+str(len(nodes_different_set)))

	#Subnets
	logger.debug("Checking nodes with a different subnets/'IT services' in LanDB than in the expert system...")
	nodes_different_subnet=[x for x in expert_system_graph.nodes if x.UID() in G.nodes and not G.nodes[x.UID()]['type']=="ROUTER" and 'isNetworkPeerIn' in x.__schema__['relation'] and len(x['isNetworkPeerIn'])>0 and not sorted([y.UID() for y in x['isNetworkPeerIn']])==sorted({att['serviceName']:1 for att in [list(edges.values())[0] for target,edges in G[x.UID()].items() if nomenclature.get_extended_type(target)=="SWITCH" or nomenclature.get_extended_type(target)=="ROUTER"]}.keys()) ] 
	if len(nodes_different_subnet)>0:
		logger.info("The next elements have different subnets/'IT services' in LanDB than in the expert system:")
		deleted_counter=0
		for node in nodes_different_subnet:
			subnets_of_node_landb={serviceName:target for serviceName,target in {list(edge.values())[0]['serviceName']:target for target,edge in G[node.UID()].items() if nomenclature.get_extended_type(target)=="SWITCH" or nomenclature.get_extended_type(target)=="ROUTER"}.items()}
			subnets_of_node_expert_sys={x.UID():"None" for x,y in expert_system_graph[node].items() if 'isNetworkPeerIn' in y.keys()}
			unmatched_networks=subnets_of_node_expert_sys.copy()
			unmatched_networks.update(subnets_of_node_landb)
			for subnet in unmatched_networks.copy():
					if subnet in unmatched_networks:
						#Case of subnet amplified due to connected only to path in a multi path service
						if nomenclature.get_reduced_zone(unmatched_networks[subnet]) and subnet+'-'+nomenclature.get_reduced_zone(unmatched_networks[subnet]) in unmatched_networks: 
							subnet_amplified=subnet+'-'+nomenclature.get_reduced_zone(unmatched_networks[subnet])
							if not subnet in subnets_of_node_expert_sys: del(unmatched_networks[subnet])
							del(unmatched_networks[subnet_amplified])	
						#Case the switch or router is not in the atcn+tn segment of landb
						elif subnet not in subnets_of_node_landb:
							subnets_of_node_landb={}
							for target,edges in G[node.UID()].items():
								for attrs in edges.values():
									subnets_of_node_landb.update({attrs['serviceName']:target})
							if subnet in subnets_of_node_landb:
								del(unmatched_networks[subnet])
						else:
							#Case of connected to a router
							router_subnet="ATCN-"+sigle[nomenclature.get_starpoint_building_of_service(subnet)] if nomenclature.get_starpoint_building_of_service(subnet) in sigle else None
							if router_subnet:
								if router_subnet in unmatched_networks: del(unmatched_networks[router_subnet]); del(unmatched_networks[subnet]) 
								elif router_subnet+"-R1" in unmatched_networks: del(unmatched_networks[router_subnet+"-R1"]); del(unmatched_networks[subnet])
								elif router_subnet+"-R2" in unmatched_networks: del(unmatched_networks[router_subnet+"-R2"]); del(unmatched_networks[subnet])
								#Case of network included
								elif subnet in subnets_of_node_landb:
									del(unmatched_networks[subnet])
						
					
			if len(unmatched_networks)>0: print(node.UID()+''.join(['\t' for i in range(0,4-int(len(node.UID())/8))])+"LanDB:%s"%(list(subnets_of_node_landb))+''.join(['\t' for i in range(0,4-int(len(subnets_of_node_landb)/8))])+"ES:%s"%list(subnets_of_node_expert_sys))
			else: deleted_counter+=1
		logger.info("Total elements:%i"%(len(nodes_different_subnet)-deleted_counter))
	

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='validate consitency of the network data with the landb data.')
	parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:"+args.database)

	graph_file_path=os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf"
	G=nx.MultiGraph(nx.read_gexf(graph_file_path))
	validate_consistency(db,G)	
