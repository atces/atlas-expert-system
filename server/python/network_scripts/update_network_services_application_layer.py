#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Updates the DNS and NTP servers per domain

import sys
import os
import json
import argparse
import connectionLanDB as landb
import config
import config_network_services_application_layer as config_network_services 
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #config.sh
import query_JSON_architecture as query

def update_NTP_and_DNS_servers(db,G):
	results=query.query_edges(G,edge_properties={"domain":"[^-]*$"},hidden_properties=["label"])
	device_per_domain={}
	for result_json in results:
		result=json.loads(result_json)
		domain=result['domain']
		for device,link,port in G.edges:
			if domain in G[device][link][port]['domain'] and 'ROUTER' not in G.nodes[device]['type']: device_per_domain[domain]=device; break
	landb.authenticate("atopes")
	for domain in device_per_domain:
		device_info=landb.client.service.getDeviceInfo(device_per_domain[domain])
		if not db.test_object("NetworkServiceGroup",domain+"NameService_DNS"): db.create_obj("NetworkServiceGroup",domain+"NameService_DNS")
		name_service_obj=db.get_obj("NetworkServiceGroup",domain+"NameService_DNS")
		if not db.test_object("NetworkServiceGroup",domain+"TimeService_NTP"): db.create_obj("NetworkServiceGroup",domain+"TimeService_NTP")
		time_service_obj=db.get_obj("NetworkServiceGroup",domain+"TimeService_NTP")
		name_service_obj['providers']=[]
		time_service_obj['providers']=[]
		name_service_obj['networkServiceTypeOffered']="DNS"
		time_service_obj['networkServiceTypeOffered']="NTP"
		for interface in device_info.Interfaces:
			if domain==interface.NetworkDomainName: 
				name_servers=interface.NameServers
				time_servers=interface.TimeServers
				for server in name_servers:
					criteria= landb.client.factory.create("types:DeviceSearch")	
					criteria.Location = None
					criteria.IPAddress=server
					server_name=landb.client.service.searchDevice(criteria)[0]
					server_obj=db.get_obj("NetworkPeerBase",server_name)
					if server_obj not in name_service_obj['providers']: name_service_obj['providers']+=[server_obj]
					if name_service_obj not in server_obj['providesNetworkService']:  server_obj['providesNetworkService']+=[name_service_obj]
				for server in time_servers:
					criteria= landb.client.factory.create("types:DeviceSearch")	
					criteria.Location = None
					criteria.IPAddress=server
					server_name=landb.client.service.searchDevice(criteria)[0]
					server_obj=db.get_obj("NetworkPeerBase",server_name)
					if server_obj not in time_service_obj['providers']: time_service_obj['providers']+=[server_obj]
					if time_service_obj not in server_obj['providesNetworkService']:  server_obj['providesNetworkService']+=[time_service_obj]
				break
		if name_service_obj['clientSelectionCriteria']=='':
			selection_criteria={"domain":domain} if "ATLAS" not in domain else {}
			config_network_services.load_default_values_in_selection_criteria(selection_criteria,name_service_obj,G)
			name_service_obj['clientSelectionCriteria']=json.dumps(selection_criteria)
		if time_service_obj['clientSelectionCriteria']=='':
			selection_criteria={"domain":domain} if "ATLAS" not in domain else {}
			config_network_services.load_default_values_in_selection_criteria(selection_criteria,time_service_obj,G)
			time_service_obj['clientSelectionCriteria']=json.dumps(selection_criteria)
		print("NTP and DNS servers of domain "+domain+" updated")
		db.commit("")
	print("NTP and DNS servers updated")

def update_all_client_services(db,G):
	services_objs=db.get_objs("NetworkServiceGroup")
	for service_obj in services_objs:
		#clean "isReachableThroughASetIn". It is renew in save_clients
		for server in service_obj['providers']:
			server['isReachableThroughASetIn']=[]
		selection_criteria=json.loads(service_obj['clientSelectionCriteria'])
		clients=config_network_services.get_clients_using_selection_criteria(selection_criteria,service_obj,G)
		clients=list(dict.fromkeys(clients))
		config_network_services.save_clients(clients,service_obj,db,G)
		print("Clients of service %s updated"%service_obj.UID())
		db.commit("")


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Updates the DNS and NTP servers per domain')
	parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:"+args.database)

	graph_file_path=os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf"
	G=query.load_architecture(graph_file_path)
	update_NTP_and_DNS_servers(db,G)	
	update_all_client_services(db,G)	
