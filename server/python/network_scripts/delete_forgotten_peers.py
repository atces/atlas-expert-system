#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Check if there are NetworkPeers not longer present in the Graph and delete them. Also deletes the empty subnets

import sys
import os
import argparse
import config
import networkx as nx
sys.path.insert(1,'../scripts/')
from delete_oks_object_with_references import delete_oks_object_with_references 
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #config.sh

db=config.Configuration("oksconfig:../../../data/sb.data.xml")
G=nx.MultiGraph(nx.read_gexf(os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf"))
for peer in db.get_objs("NetworkPeerBase"):#Object created by the network scripts using LanDB
	yn='n'
	if len(peer.get_objs('isNetworkPeerIn'))>0: 
		if peer.UID() not in G.nodes:
			yn=input("Do you want to delete the device %s? which is not in LanDB[ATCN+TN(ATLAS)] but in ES is connected to the networks: \n %s \n (y/yes/n/no):"%(peer.UID(),",".join(["%r"%service for service in peer['isNetworkPeerIn']])))
	elif len(peer['isNetworkPeerIn'])==0:
		if peer.UID() not in G.nodes:
			if len(peer['isClientOf'])>0:
					if peer['isClientOf'][0].UID()=="ATONR":
						yn='no' #ATONR clients are keep despite that some are not connected to ATCN or TN network
					else:
						yn=input("Do you want to delete the device %s? which is not connected to any network but is client of: \n %s \n (y/yes/n/no):"%(peer.UID(),",".join(["%r"%service for service in peer['isClientOf']])))		
			else:
				is_empty=True
				for relation in peer.__schema__['relation']:
					if len(peer.get_objs(relation))>0:
						is_empty=False
						break
				if is_empty: yn='yes' #Delete empty network objects not present in LanDB ATCN + TN (ATLAS)
	else:
		if peer.UID() not in G.nodes:
			yn=input("Do you want to delete the object %s whis is not in LanDB[ATCN+TN(ATLAS)]? (y/yes/n/no):"%(peer.UID()))
	if yn.lower().startswith('y'): 
		delete_oks_object_with_references(db,peer)
		print("Deleted "+peer.UID())
for subnet in db.get_objs("NetworkSubnet"): #Remove empty subnets
	is_empty=True
	for relation in subnet.__schema__['relation']:
		if len(subnet.get_objs(relation))>0: is_empty=False; break
	if is_empty: db.destroy_obj(subnet)
db.commit('')	
