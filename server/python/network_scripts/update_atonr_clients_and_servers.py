#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Obtain the list of clients of the ATONR database from the web dbmonitoring service and the list of servers 

import re
import os
import argparse
import config
import connectionLanDB as landb
from datetime import date,timedelta
import json


def update_servers(db):
	landb.authenticate("atopes")
	criteria = landb.client.factory.create("types:DeviceSearch")
	criteria.Location = None
	interfaces=landb.client.service.getSetAllInterfaces("ATLAS DB SERVERS ONLINE")
	servers_dict={}
	for interface in interfaces:
		criteria.Name=interface
		servers_dict[landb.client.service.searchDevice(criteria)[0]]=True
	servers=servers_dict.keys()
	servers=[str(x) for x in servers]
	print("These are the servers of ATONR database:")
	print(servers)
	if not db.test_object("NetworkServiceGroup","ATONR"): db.create_obj("NetworkServiceGroup","ATONR")
	service_obj=db.get_obj("NetworkServiceGroup","ATONR")
	servers_obj=[]
	for server in servers:
		if not db.test_object("NetworkServiceProviderBase",server): db.create_obj("Computer",server)
		server_obj=db.get_obj("NetworkServiceProviderBase",server)
		if service_obj not in server_obj['providesNetworkService']: server_obj['providesNetworkService']+=[service_obj]
		servers_obj+=[server_obj]
	service_obj['providers']=servers_obj
	db.commit("")



def update_clients(db):
	if not db.test_object("NetworkServiceGroup","ATONR"): db.create_obj("NetworkServiceGroup","ATONR")
	service_obj=db.get_obj("NetworkServiceGroup","ATONR")

	stream = os.popen("curl -L 'https://ords.cern.ch/ords/atlr/atlas_dbmon_r/all_schemas/no_all/ATONR' 2> /dev/null")
	json_schemas = stream.read()
	schemas=json.loads(json_schemas)
	list_of_clients=[]
	for schema in schemas["items"]:	
		schema_name=schema['schema_name']
		stream = os.popen("curl -L 'https://ords.cern.ch/ords/atlr/atlas_dbmon_r/schema/session_distribution/ATONR/%s' 2> /dev/null"%schema_name)
		json_connections = stream.read()
		connections=json.loads(json_connections)
		for connection in connections["items"]:
			machine_name=connection["machine"]
			if not machine_name: continue
			machine_name=machine_name[machine_name.find('\\')+1:].replace('.cern.ch','').upper()
			if machine_name not in list_of_clients: list_of_clients.append(machine_name)
	
	print("These are the clients of ATONR database:")
	print(list_of_clients)					
	
	_delete_previous_clients(db)		
	clients_obj=[]
	for client in list_of_clients:
		if not db.test_object("NetworkPeerBase",client): db.create_obj("Computer",client)
		client_obj=db.get_obj("NetworkPeerBase",client)
		if service_obj not in client_obj["isClientOf"]: client_obj["isClientOf"]+=[service_obj]
		clients_obj+=[client_obj]
	service_obj['hasClient']=clients_obj
	service_obj['clientSelectionCriteria']='{"label":"'+"$|".join(list_of_clients)+'$"}'
	db.commit("")

def _delete_previous_clients(db):
	if db.test_object("NetworkServiceGroup","ATONR"):
		service_obj=db.get_obj("NetworkServiceGroup","ATONR")
		for client_obj in service_obj['hasClient']:
			client_obj['isClientOf']=[ obj for obj in client_obj['isClientOf'] if not obj == service_obj]
	service_obj["hasClient"]=[]
				

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Obtain the list of clients of the ATONR database from the web dbmonitoring service and the list of servers')
	parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()
	db=config.Configuration("oksconfig:"+args.database)
	update_servers(db)
	update_clients(db) 
