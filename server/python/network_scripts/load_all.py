#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Loading of current network values to the expert system data
# WARNING: Run two times when is executed for first time in one database/when a new IT network is created

import os
import json
import re
import datetime
import networkx as nx
import config
from collections import OrderedDict
import sys
#sys.path.insert(1,os.environ['ATCN_NETWORK_PROJECT_PATH']+'/Scripts/') #from config.sh
import building_sigles as sigles
import query_JSON_architecture as query
import IT_nomenclature
import rack_nomenclature

class NetworkLoader():
	
	class_per_device_type=OrderedDict([("switch","NetworkSwitch"),
				("router","NetworkRouter"),
				("sbc","SingleBoardComputerBlade"),
				("virtual machine","VirtualMachine"),
				("optical sensor","Camera"),
				("camera","Camera"),
				("oscilloscope","Oscilloscope"),
				("disk shelf","Crate"),
				("plc module","PLCModule"),
				("plc","PLC"),
				("rcm","PowerSupply"),
				("power supply","PowerSupply"),
				("power strip","PowerStrip"),
				("computer","Computer"),
				("sts","Board"),
				("ipmc","Board"),
				("emp","Board"),
				("lwdaq","Board"),
				("board","Board")])
	
	own_relationships_per_field=OrderedDict([("system","subsystem"),
					("building","location"),
					("description","description"),
					("landb_link","landblink"),
					("rack_name","independantcontainer"),
					("peer_in","isnetworkpeerin"),
					("reachable_in","isreachablein"),
					("reachable_through_set_in","isreachablethroughasetin"),
					("path_in","ispathofsubnet"),
					("services","isclientof")
					])
	feeder_relationship_per_field={	"rack_name":"isdependantcontainerof",
					"peer_in":"hasnetworkpeer",
					"path_in":"haspath",
					"services":"hasclient"
					}
	#Default feeder class per each field of the device_info
	feeder_class_per_field={"rack_name":"Rack",
				"peer_in":"NetworkSubnet",
				"reachable_in":"NetworkSubnet",
				"path_in":"NetworkSubnet",
				"services":"NetworkServiceGroup"
				}

	def __init__(self,graph_file_path,commands_file):
		self.created_starpoints={} #e.g: {"ATCN-SDX1":"ATLAS","TN-513":"TN"}
		self.commands_file=commands_file
		self.G=query.load_architecture(graph_file_path)
		self.services=query.query_edges(self.G,source_properties={"label":"","type":"SWITCH|ROUTER"},edge_properties={"serviceName":""},hidden_properties=["label","type"]) #get all services
		self.IT_G=self.G.copy()
		for device in list(self.IT_G.nodes()):
			if not IT_nomenclature.is_valid_name(device):
				self.IT_G.remove_node(device)	
		self.eccentricity={}
		mdbfile="sb.data.xml"
		self.db=config.Configuration("oksconfig:%s/%s"%("../../../data/",mdbfile))
		self.container_per_other_id={}
		for container in self.db.get_objs("ContainerProviderBase"):
			for other_id in container.get_string_vec("otherIds"):	
				self.container_per_other_id[other_id]=container.UID()
		self.domains_per_server={}
		self.domains_reachable_through_set_per_server={}
		self.sets_per_server={}
		servers=[]
		self.services_objs=[]
		for service in self.db.get_objs("NetworkServiceProviderBase"):
			if len(service['hasClient'])>0:
				self.services_objs+=[service]
				if service.class_name()=="NetworkServiceGroup":
					for server in service['providers']:
						servers+=[server.UID()]
				else:
						servers+=[service.UID()]
		sets_per_server_results_json=query.query_edges(self.G,source_properties={"label":"","type":"SWITCH|ROUTER"},target_properties={"label":"$|".join(servers)+'$'},edge_properties={"sets":".*EXPOSED TO.*|None|.*IT NETWORK SERVICES.*","domain":""},hidden_properties=["source_label","type"])
		for sets_per_server_result_json in sets_per_server_results_json:
			sets_per_server_result=json.loads(sets_per_server_result_json)	
			server=sets_per_server_result['target_label']
			sets=sets_per_server_result['sets'].replace('[','').replace(']','').split(', ')
			self.sets_per_server[server]=sets
			if server not in self.domains_per_server: self.domains_per_server[server]=[];  self.domains_reachable_through_set_per_server[server]=[]
			if sets_per_server_result['domain'] not in self.domains_per_server[server]: self.domains_per_server[server]+=[sets_per_server_result['domain']]
			for set in sets:
				if re.match(".* EXPOSED TO (.*)",set):	self.domains_reachable_through_set_per_server[server]+=[re.match(".* EXPOSED TO (.*)",set).group(1)]
				if re.match(".*IT NETWORK SERVICES.*",set): self.domains_reachable_through_set_per_server[server]+=["ALL"] #This devices are exposed to all the domains

	def load_device(self,device_info):
		'''This procedure put in the self.commands_file the commands for add one network_peer to the expert system database according with its type'''

		rack_name=device_info['rack_name'] if 'rack_name' in device_info else None
		device_info['rack_name']=self.container_per_other_id[rack_name] if rack_name in self.container_per_other_id else rack_name
		self.load_services_in_device_info(device_info,self.load_device)
		device_info_string=''
		pattern=re.compile("(?i).*("+device_info['type']+")")
		for type in self.class_per_device_type:
			if (pattern.match(type) and pattern.match(type).end()==len(type)) or self.db.test_object("SystemBase",device_info['name']):
				clazz=self.class_per_device_type[type] if not self.db.test_object("SystemBase",device_info['name']) else self.db.get_obj("SystemBase",device_info['name']).class_name()
				for field in device_info:
					if field in self.own_relationships_per_field and device_info[field]:
						feeder_class=""
						device_info_array= device_info[field] if isinstance(device_info[field],list) else [device_info[field]]	
						for value in device_info_array:
							if field in self.feeder_relationship_per_field:
								if not self.db.test_object("SystemBase",value):
									self.commands_file.write(self.feeder_class_per_field[field]+" -e --uid '"+value+"'\n")
									feeder_class=self.feeder_class_per_field[field]
								else:
									feeder_class=self.db.get_obj("SystemBase",value).class_name()	
							self.commands_file.write(clazz+" -e --uid '"+device_info['name']+"' --"+self.own_relationships_per_field[field]+" '"+value+"'\n")
							if field in self.feeder_relationship_per_field:
								self.commands_file.write(feeder_class+" -e --uid '"+value+"' --"+self.feeder_relationship_per_field[field]+" '"+device_info['name']+"'\n")
				break

	#Process to load the subnets where the device is reachable
	def load_reached_in_on_device_info(self,device_info,subnet):
		if 'reachable_in' not in device_info: device_info['reachable_in']=[]
		if not isinstance(subnet,list): subnet=[subnet]
		for net in subnet:
			if net not in device_info['reachable_in']: device_info['reachable_in']+=[net] 
			if not device_info['trustingSet'].lower()=='none': continue
			if self.db.test_object("NetworkSubnet",net):
				for super_net in self.db.get_obj("NetworkSubnet",net).get_objs("isPartOfSubnet"):
					self.load_reached_in_on_device_info(device_info, super_net.UID())
			

	#Process for load in the device info the services where the device is client
	# @param device_info device_info containing the information of the device (name,system, rack_name, domain, ...) and will be loaded with the corresponding services
	def load_services_in_device_info(self,device_info,load_device_method):
		device_info['services']=[]
		device_info['label']=device_info['name']
		for service_obj in self.services_objs:
			match=True
			if len(service_obj['clientSelectionCriteria'])>0: selection_criteria=json.loads(service_obj['clientSelectionCriteria'])
			else: match=False; break
			for criteria in selection_criteria:
				if criteria not in device_info or not re.match(selection_criteria[criteria],device_info[criteria], re.IGNORECASE):
					match=False
					break
			if match: 
				#Checking if the domain have access to the server
				servers=[x.UID() for x in service_obj['providers']] if "NetworkServiceGroup"==service_obj.class_name() else [service_obj.UID()]
				if len(servers)==0: device_info['services'].append(service_obj.UID()) #Black-box services where servers are unknown
				if device_info['name'] not in servers: #Avoid dependencies from himself
					for server in servers:
						if server in self.domains_per_server:
							if 'domain' in device_info and  device_info['domain'] in self.domains_per_server[server] and device_info['trustingSet'].lower()=='none':
								device_info['services'].append(service_obj.UID())
							elif 'domain' in device_info and  device_info['domain'] in self.domains_reachable_through_set_per_server[server] or "ALL" in self.domains_reachable_through_set_per_server[server] or (server in self.sets_per_server and 'trustingSet' in device_info and not device_info['trustingSet'].lower()=='none' and device_info['trustingSet'] in self.sets_per_server[server]): 
								device_info['services'].append(service_obj.UID())
								server_info={}
								server_info['name']=server
								server_info['type']=self.G.nodes[server]['type'] if server in self.G.nodes else "Computer"
								if 'peer_in' in device_info: 
									server_info['reachable_in']=device_info['peer_in']
									server_info['reachable_through_set_in']=device_info['peer_in']
								load_device_method(server_info)
						else:
							device_info['services'].append(service_obj.UID()) #For some servers that are not in the graph (by default connected)
		if len(device_info['services'])==0: del(device_info['services'])
	
	def clean_rack_name(self,rack_name):
		rack_name=rack_nomenclature.get_rack_name(rack_name) if rack_nomenclature.is_valid_name(rack_name) else re.sub("RACK(.)?",'',rack_name) if "none" not in rack_name.lower() else None
		if rack_name and (rack_name=="ATLAS" or rack_name in sigles.BUILDING_SIGLE_BY_NUMBER or rack_name in sigles.BUILDING_SIGLE_BY_NUMBER.values() or re.match("/.*|".join(sigles.BUILDING_SIGLE_BY_NUMBER.values()),rack_name)) : rack_name=None
		if rack_name and ',' in rack_name: rack_name=rack_name[:rack_name.find(',')]
		if rack_name and '=' in rack_name: rack_name=rack_name.replace('=','-')
		if rack_name and rack_name.startswith("STAR"): rack_name=rack_name.replace(' ','-')
		if rack_name and ' ' in rack_name: rack_name=rack_name[:rack_name.find(' ')]
		return rack_name if not rack_name == '' else None
		
	#Function to process the result of the JSON script and return variables
	# @param node_position Could be "source_" or "target_" or None  
	# @param json_device result to be process
	def _process_result(self,json_device,node_position=''):
		device_dict=json.loads(json_device)
		device_info={}
		device_info['name']=device_dict[node_position+'label']
		#device_name=device_name.replace('-','_')
		device_info['system']=device_dict[node_position+'system'].split('|',1)[0]
		building=device_dict[node_position+'building'] if ("None" not in device_dict[node_position+'building']) and len(device_dict[node_position+'building'])>0 else None  
		rack_name=device_dict[node_position+'zone']
		device_info['rack_name']=self.clean_rack_name(rack_name)
		device_info['type']=device_dict[node_position+'type']
		device_info['building']=sigles.BUILDING_SIGLE_BY_NUMBER[building] if building else None
		device_info['domain']=device_dict['domain']  if 'domain' in device_dict else ''
		device_info['sets']=device_dict['sets']  if 'sets' in device_dict else ''
		device_info['trustingSet']=device_dict['trustingSet']  if 'trustingSet' in device_dict else ''
		device_info['dhcpProvider']=device_dict[node_position+'dhcpProvider']
		device_info['operatingSystem']=device_dict[node_position+'operatingSystem']
		if self.db.test_object("SystemBase",device_info['name']):
			prev_description=self.db.get_obj("SystemBase",device_info['name']).get_string("description")
			if (not prev_description or len(prev_description)==0) and not device_dict[node_position+'description'].startswith("None"):
					device_info['description']=device_dict[node_position+'description'].replace("'",'')
			prev_doc=self.db.get_obj("SystemBase",device_info['name']).get_string("landbLink")
			if (not prev_doc or len(prev_description)==0):
				device_info['landb_link']="http://landb.cern.ch/portal/devices/"+device_info['name']
		return device_info

	def load_service(self,service):
		starpoint_building=IT_nomenclature.get_starpoint_building_of_service(service)
		sigle=sigles.BUILDING_SIGLE_BY_NUMBER[starpoint_building]
		network='Undefined'
		for (source,target,port) in self.G.edges:
			if service in self.G[source][target][port]['serviceName']:
				if "ATLAS" in self.G[source][target][port]['domain']:
					network="ATCN"
				else:
					network=self.G[source][target][port]['domain']
		starpoint_name=network+'-'+sigle
		if starpoint_name not in self.created_starpoints: self.load_starpoint(starpoint_building,network)
		#Get the paths of the current service (except path of switches)
		results_paths=query.query_edges(self.G,source_properties={"label":"","type":"SWITCH","building":"","zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":""},target_properties={"type":"(?!ROUTER|SWITCH)"},edge_properties={"serviceName":service+'$',"domain":""},hidden_properties=["target_type"])
		#print ("Creating service",service,starpoint_building,sigle,network)
		if "EX" not in IT_nomenclature.get_service_type(service) and (len(results_paths)==0 or self.created_starpoints[starpoint_name]==json.loads(results_paths[0])['domain']):#User or isolated services should not connected to the routers
			self.commands_file.write("NetworkSubnet -e --uid "+service+" --ispartofsubnet "+starpoint_name+"\n")
			self.commands_file.write("NetworkSubnet -e --uid "+starpoint_name+" --hassubnet "+service+"\n")
		#Calculating min eccentricity
		path_names=[re.match('.*source_label": "([^"]+)',x).group(1) for x in results_paths]
		min_eccentricity=200 #Any high value
		for path_name in path_names:
			if path_name in self.eccentricity[starpoint_name]: min_eccentricity=min(min_eccentricity,self.eccentricity[starpoint_name][path_name])
		for json_device in results_paths:
			path_info=self._process_result(json_device,"source_")
			path_name=path_info['name']
			#print ("Creating path on service",service,path_name,device_type,sigle_device,rack_name,system)
			#Get the peers of the current service  
			results_peers=query.query_edges(self.G,source_properties={"label":path_name,"type":"SWITCH"},target_properties={"label":"","building":"","zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":"","type":"(?!ROUTER)"},edge_properties={"serviceName":service+'$',"domain":"","sets":"","trustingSet":""},hidden_properties=["source_type"])
			created_paths={}
			for json_device in results_peers:
				device_dict=json.loads(json_device)
				peer_info=self._process_result(json_device,"target_")
				peer_name=peer_info['name']
				if peer_name not in self.eccentricity[starpoint_name] or self.eccentricity[starpoint_name][peer_name] >  self.eccentricity[starpoint_name][path_name]:#No uplinks
					#Check if the peers are connected with all paths
					paths_counter=0
					for edge in self.G[peer_name]:
						if self.G.nodes[edge]['type'] == "SWITCH":
							for link in self.G[peer_name][edge]:
								if self.G[peer_name][edge][link]['serviceName']==service:
									paths_counter+=1
									break
					if len(results_paths)==paths_counter: 
						if path_name not in created_paths:
							created_paths[path_name]=True
							path_info['path_in']=service
							self.load_device(path_info)
						peer_info['peer_in']=service
						self.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
						self.load_device(peer_info)
					elif paths_counter==1:
						if path_name+IT_nomenclature.get_reduced_zone(path_name) not in created_paths:
							created_paths[path_name+IT_nomenclature.get_reduced_zone(path_name)]=True
							self.commands_file.write("NetworkSubnet -e --uid "+service+"\n")
							self.commands_file.write("NetworkSubnet -e --uid "+service+'-'+IT_nomenclature.get_reduced_zone(path_name)+" --ispartofsubnet "+service+"\n")
							self.commands_file.write("NetworkSubnet -e --uid "+service+" --hassubnet "+service+'-'+IT_nomenclature.get_reduced_zone(path_name)+"\n")
							path_info['path_in']=[service+'-'+IT_nomenclature.get_reduced_zone(path_name)]
							if path_name in self.eccentricity[starpoint_name] and self.eccentricity[starpoint_name][path_name] == min_eccentricity: 
								 path_info['path_in']+=[service]
							self.load_device(path_info)
						peer_info['peer_in']=service+'-'+IT_nomenclature.get_reduced_zone(path_name)
						self.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
						self.load_device(peer_info)
		return self.created_starpoints

	def load_starpoint(self,starpoint_building,network):
		sigle=sigles.BUILDING_SIGLE_BY_NUMBER[starpoint_building]
		starpoint_name=network+'-'+sigle
		self.created_starpoints[starpoint_name]=network #By default is the network if not peers, otherwise is replace by the domain name
		self.commands_file.write("NetworkSubnet -e --uid "+starpoint_name+"\n")
		self.commands_file.write("NetworkSubnet -e --uid "+network+" --hassubnet "+starpoint_name+"\n")
		self.commands_file.write("NetworkSubnet -e --uid "+starpoint_name+" --ispartofsubnet "+network+"\n")
		#print ("Creating startpoint",starpoint_name)
		results_routers=query.query_nodes(self.G,source_properties={"label":"","type":"ROUTER","building":starpoint_building,"zone":"","system":"","dhcpProvider":"","operatingSystem":"","description":""}) #get routers of starpoint
		for result in list(results_routers):
			if network not in IT_nomenclature.get_extended_network(json.loads(result)['label']):
				results_routers.remove(result)
		self.eccentricity[starpoint_name]={} #Init the eccentricity to avoid error in case of none routers known in the starpoint
		for json_device in results_routers:
			router_info=self._process_result(json_device)
			router_name=router_info['name']
			component=nx.node_connected_component(self.IT_G,router_name)
			connected_G=self.G.subgraph(component)	
			self.eccentricity[starpoint_name]=nx.eccentricity(connected_G)
			router_info['path_in']=starpoint_name
			self.load_device(router_info)
			#print ("Creating router",router_name,rack_name,sigle,system)
			#get the peers of the router
			results_peers=query.query_edges(self.G,source_properties={"type":"ROUTER","label":router_name},target_properties={"label":"","building":"","zone":"","system":"","type":"","dhcpProvider":"","operatingSystem":"","description":""},edge_properties={"serviceName":"","domain":"","sets":"","trustingSet":""},hidden_properties=["source_type","source_building"])
			for json_device in results_peers:
				peer_info=self._process_result(json_device,"target_")
				self.created_starpoints[starpoint_name]=peer_info['domain']
				#Check if the peers are connected with all the starpoint
				router_counter=0
				for edge in self.G[peer_info['name']]:
					if self.G.nodes[edge]['type'] == "ROUTER":
						router_counter+=1
				if len(results_routers)==router_counter: 
					peer_info['peer_in']=starpoint_name
					self.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
				elif router_counter==1:
					self.commands_file.write("NetworkSubnet -e --uid "+starpoint_name+"-R"+router_name[-1:]+" --ispartofsubnet "+starpoint_name+"\n")
					self.commands_file.write("NetworkSubnet -e --uid "+starpoint_name+" --hassubnet "+starpoint_name+"-R"+router_name[-1:]+"\n")
					router_info['path_in']=starpoint_name+"-R"+router_name[-1:]
					self.load_device(router_info)
					peer_info['peer_in']=starpoint_name+"-R"+router_name[-1:]
					self.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
				else:
					peer_info['peer_in']=starpoint_name
					self.load_reached_in_on_device_info(peer_info,peer_info['peer_in'])
					self.load_device(peer_info)
					pass #Scenario of more than two routers in an starpoint is not considered because is a rare configuration, not present until now
				#print ("Creating device",peer_name,device_type,sigle_device,rack_name,system,"connected to",router_counter,"routers")
	
		

if __name__ == '__main__':
	with open("commands.tmp","w+") as commands_file:
		net_loader=NetworkLoader(os.environ['ATCN_NETWORK_PROJECT_PATH']+"/atcn_architecture/atcn_architecture.gexf",commands_file)	
		for json_service in net_loader.services:
			service_dict=json.loads(json_service)
			service=service_dict['serviceName'] 
			net_loader.load_service(service)
		commands_file.write("MetaInfo -e --uid Singleton --lastnetworkupdatedate '"+str(datetime.date.today()-datetime.timedelta(days=1))+"'\n")
