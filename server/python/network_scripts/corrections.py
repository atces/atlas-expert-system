#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# For performing correcction directly from python

import config
import argparse
import re

parser = argparse.ArgumentParser(description='For performing correcction directly from python')
parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
args = parser.parse_args()

db=config.Configuration("oksconfig:"+args.database)
#Additons
#obj=db.get_obj("SystemBase","UX15_Solenoid")
#dest=db.get_obj("SystemBase","PV2900")
#if not dest in obj['requiresVacuumFrom']: obj['requiresVacuumFrom']+=[dest]

#Editing
#network_objs=db.get_objs("NetworkPeerBase")
#for network_obj in network_objs:
#	attribute=network_obj['documentation']
#	if re.match(".*http.*landb",attribute): network_obj['documentation']=''

usa15alarm=db.create_obj("EvacuationAlarm","Evacuation_AL_USA15")
us15alarm=db.create_obj("EvacuationAlarm","Evacuation_AL_US15")
ux15alarm=db.create_obj("EvacuationAlarm","Evacuation_AL_UX15")
usa15di=db.create_obj("EvacuationDigitalInput","Evacuation_DI_USA15_SESEV101")
us15di=db.create_obj("EvacuationDigitalInput","Evacuation_DI_US15_SESEV101")
ux15di=db.create_obj("EvacuationDigitalInput","Evacuation_DI_UX15_SESEV101")
usa15di["evacuationAlarm"]=[usa15alarm]
usa15alarm["requiresEvacuationDigitalInput"]=[usa15di]
ux15di["evacuationAlarm"]=[ux15alarm]
ux15alarm["requiresEvacuationDigitalInput"]=[ux15di]
us15di["evacuationAlarm"]=[us15alarm]
us15alarm["requiresEvacuationDigitalInput"]=[us15di]
usa15di_dss=db.get_obj("DigitalInput","DI_INF_USA15_Evacuation_SESEV101")
ux15di_dss=db.get_obj("DigitalInput","DI_INF_UX15_Evacuation_SESEV101")
us15di_dss=db.get_obj("DigitalInput","DI_INF_US15_Evacuation_SESEV101")
usa15di["signalSource"]=usa15di_dss["signalSource"]
for signal_source in usa15di["signalSource"]:
    signal_source["digitalOutput"]=[x for x in signal_source["digitalOutput"] if not x == usa15di]+[usa15di]
ux15di["signalSource"]=ux15di_dss["signalSource"]
for signal_source in ux15di["signalSource"]:
    signal_source["digitalOutput"]=[x for x in signal_source["digitalOutput"] if not x == ux15di]+[ux15di]
us15di["signalSource"]=us15di_dss["signalSource"]
for signal_source in us15di["signalSource"]:
    signal_source["digitalOutput"]=[x for x in signal_source["digitalOutput"] if not x == us15di]+[us15di]
objs=db.get_objs("EvacuationSiren")
for obj in objs:
    if "UX15" in obj["description"]:
        obj["triggeredBy"]=[x for x in obj["triggeredBy"] if not x == ux15alarm ]+[ux15alarm]
        ux15alarm["triggersSiren"]=[x for x in ux15alarm["triggersSiren"] if not x==obj ]+[obj]     
    elif "USA15" in obj["description"]:
        obj["triggeredBy"]=[x for x in obj["triggeredBy"] if not x == usa15alarm ]+[usa15alarm]
        usa15alarm["triggersSiren"]=[x for x in usa15alarm["triggersSiren"] if not x==obj ]+[obj]     
    elif "US15" in obj["description"]:
        obj["triggeredBy"]=[x for x in obj["triggeredBy"] if not x == us15alarm ]+[us15alarm]
        us15alarm["triggersSiren"]=[x for x in us15alarm["triggersSiren"] if not x==obj ]+[obj]     

#	if re.match(".*http.*landb",attribute): network_obj['documentation']=''


#Removals
#if db.test_object("SystemBase","S3125-2-IP57-1614"): 
#	obj=db.get_obj("SystemBase","S3125-2-IP57-1614")
#	for referencer in obj.referenced_by():
#		referencer=db.get_obj("SystemBase",referencer.UID())
#		for relation in referencer.__schema__['relation']:
#			if obj in referencer[relation]:
#				referencer[relation]=[ x for x in referencer[relation] if not x==obj]
#	db.destroy_obj(obj)

#obj=db.get_obj("SystemBase","ATLASVHDC13")
#dest=db.get_obj("SystemBase","ATCN-SDX1")
#if dest in obj['isNetworkPeerIn']: obj['isNetworkPeerIn'].remove(dest); obj['isNetworkPeerIn']=obj['isNetworkPeerIn']
#if obj in dest['hasNetworkPeer']: dest['hasNetworkPeer'].remove(obj); dest['hasNetworkPeer']=dest['hasNetworkPeer']

#magnets=db.get_objs("Magnet")
#for magnet in magnets:
#	magnet["vacuumFrom"]=[]



db.commit("")
