#!/bin/sh
#source ./config.sh
if ! [ $ATCN_NETWORK_PROJECT_PATH ]; then 
	echo "Put the correct path to the network project in the config.sh. The network project is available in https://gitlab.cern.ch/guribego/atcn-network-analysis.git"
	exit -1 
fi

architecture_path=$ATCN_NETWORK_PROJECT_PATH/atcn_architecture/atcn_architecture.gexf

#exit 0
echo  "Loading data from network project...(takes aprox. 10 min)"
python update_network_services_application_layer.py
if python load_all.py ; then
	cat -n commands.tmp | sort -uk2 | sort -nk1 | cut -f2- > commands.reduced.tmp
	python ../editor.py -d  ../../../data/sb.data.xml -y commands.reduced.tmp
	rm commands.tmp commands.reduced.tmp
fi
