#!/usr/bin/env python
###################################################
# Generate the PHPdatagrouped file for the finder
# April 2019: first version
# June 2019: fix to execute from anywhere
# August 2019: read mxgraph xml
# March 2020: all pages are xml
#
# payload is URL encoded, compressed (base 15), 
# and b64 encoded. To decode:
# import base64
# import zlib
# import urllib
# data=base64.b64decode(data)
# data=zlib.decompress(data,-15)
# data=urllib.unquote(data)
#
# ignacio.asensi@cern.ch
# carlos.solans@cern.ch
# eva.lott@cern.ch
####################################

# FIXME
# DB will not be assigned if find_objects_in_groups==False
# The generated file is double its size
# Propose to use another variable: groups for the groups
# Now this script cannot be executed without the setup

import os
import sys
import json
import zlib
from urllib.parse import unquote_to_bytes
import xml.dom.minidom
import base64
import argparse
import datetime
import config

parser=argparse.ArgumentParser()
parser.add_argument('-v','--verbose',help="enable verbose mode", action='store_true')
parser.add_argument('-d','--dry',help="dry run", action='store_true')
args=parser.parse_args()

#this allows to run from anywhere
basedir=os.path.dirname(os.path.realpath(__file__))
infile="%s/../../login/data/menuTree.json" % basedir
outfile="%s/../../login/data/PHPdatagrouped.php" % basedir
indir="%s/../../login/" %basedir
database="%s/../../data/sb.data.xml" % basedir
find_objects_in_groups=True #Fixme, this can never be False, otherwise db will not be defined

print("Open file: %s" % infile)
data = json.load(open(infile))
if find_objects_in_groups:
    print("Load database: %s" % database)
    db=config.Configuration("oksconfig:%s"%(database))
    pass

pages={}
for page in data:
    print ("-------------------------------")
    print ("Parse file: %s\n" % page)
    feed=False
    pages[page]=[]
    try:
        xfile="%sdata/xmlgraphs/%s.xml"%(indir,page)
        xfile2="%sdata/xmlgraphs/%s.drawio"%(indir,page)
        if os.path.exists(xfile2)==True:
            xfile=xfile2
        if os.path.exists(xfile)==False:
            continue
        print ("parse xml: %s" % xfile)
        doc=xml.dom.minidom.parse(xfile)
        xdata=doc.getElementsByTagName("diagram")[0].firstChild.nodeValue
        xdata=base64.b64decode(xdata)
        xdata=zlib.decompress(xdata,-15)
        xdata=unquote_to_bytes(xdata)
        gdata=xml.dom.minidom.parseString(xdata)
        for ele in gdata.getElementsByTagName('object'):
            if ele.hasAttribute("UID") and find_objects_in_groups==True and ele.hasAttribute("group"): 
                uid=ele.getAttribute("UID").strip()
                if uid=="": continue
                try:
                    obj=db.get_obj("Group", str(uid))
                    #print ("Found group %s" % uid)
                    grouped_objs=obj.get_objs("groupTo")
                    for o in grouped_objs:
                        #print (o.UID())
                        pages[page].append([o.UID(), uid])# object.UID, group.UID
                        pass
                    pages[page].append([uid,uid])
                    pass
                except:
                    print ("  NOT Found group %s" % uid)
                    pass
                pass
            elif ele.hasAttribute("UID"): 
                uid=ele.getAttribute("UID").strip()
                if uid=="": continue
                print ("  Found %s" % uid)
                #pages[page].append(uid)
                pages[page].append([uid,uid])
                pass
            elif ele.hasAttribute("uid"): 
                uid=ele.getAttribute("uid").strip()
                if uid=="": continue
                print ("  Found %s" % uid)
                #pages[page].append(uid)
                pages[page].append([uid,uid])
                pass
            pass
        pass
    except: 
        print ("ERROR in file: %s" % page)
        print (sys.exc_info())
        pass
    print ("")
    pass

if not args.dry:
    now = datetime.datetime.now()
    snow = datetime.date.strftime(now,"%H:%M:%S %d-%m-%Y")
    print ("Save output to file: %s"%outfile)
    fw = open(outfile, "w")
    fw.write("<?php\n")
    fw.write("/*****************************************\n")
    fw.write(" * This file was automatically generated  \n")
    fw.write(" * by updateFinder.py on \n")
    fw.write(" * %s \n" %snow)
    fw.write(" * Please do not modify\n")
    fw.write(" ****************************************/\n")
    fw.write("\n")
    fw.write("$data = array();\n")
    for page in pages:
        ##text='","'.join(sorted(pages[page]))#
        text=""
        text+='","'.join(['"=>"'.join(o) for o in pages[page]])
        text+=""
        fw.write('$data["%s"]=array("%s");\n' % (page,text));
        pass
    fw.write("\n")
    fw.write("?>\n")
    fw.close()
    pass
    
print("Have a nice day")
