#!/usr/bin/env python
#############################################
# AUG helper
# Carlos.Solans@cern.ch
# Florian.Haslbeck@cern.ch
# January 2020
#############################################

import helper

class AUGHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("AUG")
    print ("Have a nice day")
