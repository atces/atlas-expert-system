#!/usr/bin/env python
# ############################################
# Gas System Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
# ############################################

import helper

class GasSystemHelper(helper.Helper):
    pass
  
if __name__ == '__main__':

    helper.Helper().cmdline("GasSystem")
    print ("Have a nice day")
