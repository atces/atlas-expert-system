#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-u","--uid",help="UID",required=True)
    parser.add_argument("-n","--new_uid",help="New UID",required=True)
    parser.add_argument("-t","--type",help="Class type",required=True)
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print ("Load database: %s" % args.database)
    db=config.Configuration("oksconfig:%s"%(args.database))

    #parint "Find object: %s@%s" % (args.uid,args.type)
    obj=db.get_obj(args.type,args.uid)
    if not obj:
        print ("Object not found")
        pass
    else:
        print ("Rename object: %s@%s => %s@%s" % (args.uid,args.type,args.new_uid,args.type))
        obj.rename(args.new_uid)
        ans=input("Do you wish to continue? [y/n]: ")
        if "y" in ans.lower(): 
            print ("Commit changes")
            db.commit("")
            pass
        pass
    print ("Have a nice day")
    pass
