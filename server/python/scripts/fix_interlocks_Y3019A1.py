#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack


    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    mdt_barrel = filter(lambda e: e.UID().startswith("MDT_B"), db.get_objs("SubDetector"))
    mdt_sideA = filter(lambda e: e.UID().startswith("MDT_") and e.UID()[9]=="A", db.get_objs("SubDetector"))
    mdt_sideC = filter(lambda e: e.UID().startswith("MDT_") and e.UID()[9]=="C", db.get_objs("SubDetector"))
    csc = filter(lambda e: e.UID().startswith("CSC_"), db.get_objs("SubDetector"))
    regex = re.compile('RPC_C[0-2].')
    rpc = filter(lambda e: re.match(regex, e.UID()), db.get_objs("SubDetector"))

    lvl1 = [
        "Y.03-02.A1",
        "Y.04-02.A1",
        "Y.06-02.A2",
        "Y.07-02.A2",
        "Y.07-11.A2",
        "Y.08-02.A2",
        "Y.09-02.A2",
        "Y.10-02.A2",
        "Y.11-02.A2",
        "Y.12-02.A2",
        "Y.14-02.A1",
        "Y.15-02.A1",
        "Y.16-02.A2",
        "Y.17-02.A2",
        "Y.18-02.A2",
        "Y.19-02.A2",
        "Y.20-02.A1",
        "Y.20-02.A2",
        "Y.21-02.A1",
        "Y.21-02.A2",
        "Y.22-02.A1",
        "Y.22-02.A2",
        "Y.23-02.A1",
        "Y.23-02.A2",
        "Y.24-02.A2",
        "Y.25-02.A2",
        "Y.26-02.A2",
        "Y.27-02.A2",
        "Y.28-02.A2",
        "Y.29-02.A1",
        "Y.29-02.A2",
        "Y.30-02.A1"
    ]


    hp1 = interlock.InterlockHelper()
    hp1.setDb(db)
    hp2 = crate.CrateHelper()
    hp2.setDb(db)
    """
    for el in mdt_barrel:
        cr_list = el["requiresPowerFrom"]
        for cr in cr_list:
            obj1 = {
                "UID": "MUN_MDT_Interlock_Barrel_Power_Y3019A1",
                "interlocks": str(cr.UID()),
                "interlockedBy": "O_MUN_MDT_Barrel_Power_Y3019A1"
            }
            hp1.addObj(obj1,False,True)
            obj2 = {
                "UID": str(cr.UID()),
                "interlockedBy": "MUN_MDT_Interlock_Barrel_Power_Y3019A1"
            }
            hp2.addObj(obj2,False,True)
    db.commit("")
    
    for el in mdt_sideA:
        cr_list = el["requiresPowerFrom"]
        for cr in cr_list:
            obj1 = {
                "UID": "MUN_MDT_Interlock_SideA_CSC_All_Power_Y3019A1",
                "interlocks": str(cr.UID()),
                "interlockedBy": "O_MUN_MDT_SideA_CSC_All_Power_Y3019A1"
            }
            hp1.addObj(obj1,False,True)
            obj2 = {
                "UID": str(cr.UID()),
                "interlockedBy": "MUN_MDT_Interlock_SideA_CSC_All_Power_Y3019A1"
            }
            hp2.addObj(obj2,False,True)
    db.commit("")
    """
    hp3 = powersupply.PowerSupplyHelper()
    hp3.setDb(db)
    """
    for el in csc:
        cr_list = el["requiresPowerFrom"]
        for cr in cr_list:
            obj1 = {
                "UID": "MUN_MDT_Interlock_SideA_CSC_All_Power_Y3019A1",
                "interlocks": str(cr.UID()),
                "interlockedBy": "O_MUN_MDT_SideA_CSC_All_Power_Y3019A1"
            }
            hp1.addObj(obj1,False,True)
            obj2 = {
                "UID": str(cr.UID()),
                "interlockedBy": "MUN_MDT_Interlock_SideA_CSC_All_Power_Y3019A1"
            }
            hp3.addObj(obj2,False,True)
    db.commit("")
    
    for el in mdt_sideC:
        cr_list = el["requiresPowerFrom"]
        for cr in cr_list:
            obj1 = {
                "UID": "MUN_MDT_Interlock_SideC_Power_Y3019A1",
                "interlocks": str(cr.UID()),
                "interlockedBy": "O_MUN_MDT_SideC_Power_Y3019A1"
            }
            hp1.addObj(obj1,False,True)
            obj2 = {
                "UID": str(cr.UID()),
                "interlockedBy": "MUN_MDT_Interlock_SideC_Power_Y3019A1"
            }
            hp2.addObj(obj2,False,True)
    db.commit("")
    """
    hp4 = rack.RackHelper()
    hp4.setDb(db)
    """
    for el in lvl1:
        obj1 = {
            "UID": "MUN_LVL1_Interlock_Power_Y3019A1",
            "interlocks": el,
            "interlockedBy": "O_MUN_LVL1_Power_Y3019A1"
        }
        hp1.addObj(obj1,False,True)
        obj2 = {
            "UID": el,
            "interlockedBy": "MUN_LVL1_Interlock_Power_Y3019A1"
        }
        hp4.addObj(obj2,False,True)
    db.commit("")
    """
    for el in rpc:
        cr_list = el["requiresPowerFrom"]
        for cr in cr_list:
            obj1 = {
                "UID": "MUN_RPC_Interlock_Power_Y3019A1",
                "interlocks": str(cr.UID()),
                "interlockedBy": "O_MUN_RPC_Power_Y3019A1"
            }
            hp1.addObj(obj1,False,True)
            obj2 = {
                "UID": str(cr.UID()),
                "interlockedBy": "MUN_RPC_Interlock_Power_Y3019A1"
            }
            hp4.addObj(obj2,False,True)
    db.commit("")
    
