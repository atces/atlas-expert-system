#!/usr/bin/env python
# florian.haslbeck@cern.ch
# November 2024
# adds a relation DI <-> DSU in an OKS object





def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj


def create_di(db, uid, dsu, crate, slot, channel, cable, verbose=False):
    """ create DI and link to DSU, also add the reverse relation
    """

    try : 
        obj = db.get_obj("DigitalInput",uid)
        if verbose: print(f"UPDATING DigitalInput {uid}")
    except: 

        obj = db.create_obj("DigitalInput", uid)
        if verbose: print(f"CREATING DigitalInput {uid}")

    attributes = [
        ("crate",   crate),
        ("slot",    int(slot)),
        ("channel", int(channel)),
        ("cable",   cable)
        ]

    for attr, value in attributes:
        obj[attr] = value

    # DSU relation
    dsu_obj = get_obj(db, "DSU", dsu)
    if dsu_obj:
        obj["DSU"] = dsu_obj
        if verbose: print(f"Added relation {uid} -> {dsu}")
    else:
        print(f"DSU {dsu} not found in database")

    # reverse relation
    if dsu_obj:
        dis = dsu_obj["digitalInput"]
        dis.append(obj)
        dsu_obj["digitalInput"] = dis
        if verbose: print(f"Added relation {dsu} -> {uid}")
        

def create_actions(db, uid, dsu, crate, slot, channel, cable, verbose=False):
    """ create DI and link to DSU, also add the reverse relation
    """

    try : 
        obj = db.get_obj("Action",uid)
        if verbose: print(f"UPDATING Action {uid}")
    except: 

        obj = db.create_obj("Action", uid)
        if verbose: print(f"CREATING Action {uid}")

    attributes = [
        ("crate",   crate),
        ("slot",    int(slot)),
        ("channel", int(channel)),
        ("cable",   cable)
        ]

    for attr, value in attributes:
        obj[attr] = value

    # DSU relation
    dsu_obj = get_obj(db, "DSU", dsu)
    if dsu_obj:
        obj["DSU"] = dsu_obj
        if verbose: print(f"Added relation {uid} -> {dsu}")
    else:
        print(f"DSU {dsu} not found in database")

    # reverse relation
    if dsu_obj:
        actions = dsu_obj["actions"]
        actions.append(obj)
        dsu_obj["actions"] = actions
        if verbose: print(f"Added relation {dsu} -> {uid}")
        
def create_alarm(db, uid, digitalInput, verbose=False):

    try : 
        obj = db.get_obj("Alarm",uid)
        if verbose: print(f"UPDATING Alarm {uid}")
    except: 

        obj = db.create_obj("Alarm", uid)
        if verbose: print(f"CREATING Alarm {uid}")

    # DI relation
    di_obj = get_obj(db, "DigitalInput", digitalInput)
    if di_obj:
        print(f"DI {digitalInput} found in database")

        linked_dis = obj["requiresDigitalInput"]
        print(linked_dis)
        if di_obj not in linked_dis:
            linked_dis.append(di_obj)

        obj["requiresDigitalInput"] = linked_dis
        if verbose: print(f"Added relation {uid} -> {digitalInput}")

        # reverse relation
        alarms = di_obj["triggers"]
        print(alarms)
        if obj not in alarms:
            alarms.append(obj)
        di_obj["triggers"] = alarms
        if verbose: print(f"Added relation {digitalInput} -> {uid}")

    else:
        print(f"DI {digitalInput} not found in database")
        input("Press enter to continue")

        # create DI
        di_obj = db.create_obj("DigitalInput", digitalInput)
        if verbose: print(f"CREATING DigitalInput {digitalInput}")

        # add DI to alarm
        linked_dis = obj["requiresDigitalInput"]
        if di_obj not in linked_dis: linked_dis.append(di_obj)
        obj["requiresDigitalInput"] = linked_dis

        # add alarm to DI
        alarms = di_obj["triggers"]
        if obj not in alarms:
            alarms.append(obj)
        di_obj["triggers"] = alarms

        if verbose: print(f"Added relation {uid} -> {digitalInput}")




def get_dis_from_file(input_file, columns = ["crate", "slot", "channel", "cable"], separator = ","):
    """ convert file to dict of dict { DIname : { } }"""

    output = {}
    
    with open(input_file, "r") as f:
        for l in f:
            if l.startswith("#"): continue
            l = l.split(separator)
            
            DIname = l[0].strip()
            output[DIname] = { columns[i] : l[i+1].strip() for i in range(len(columns))}

    return output


def get_alarm_di_from_file(fname):
    alarms_dis = {}
    with open(fname, "r") as f:
        for l in f:
            if l.startswith("#"): continue
            l = l.split(",")
            alarm = l[0].strip()
            di = l[1].strip()
            alarms_dis[alarm] = di
    return alarms_dis


if __name__ == '__main__':

    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
    args = parser.parse_args()

    db=config.Configuration("oksconfig:%s"%args.database)

    

    # create DI objects
    # di_dict = get_dis_from_file("../../data/dsu8_dis.txt")
    # print(di_dict)
    # for di, values in di_dict.items():
    #     create_di(db, di, "DSU_8", **values, verbose=True)

    # create Action objects
    # action_dict = get_dis_from_file("../../data/dsu8_dos.txt")
    # print(action_dict)
    # for action, values in action_dict.items():
    #     create_actions(db, action, "DSU_8", **values, verbose=True)

    # create Alarm objects
    alarm_di_dict = get_alarm_di_from_file("../../data/dsu8_add_alarms.csv")

    for alarm, di in alarm_di_dict.items():
        create_alarm(db, alarm, di, verbose=True)

    # uncomment to save changes
    db.commit()

    