#!/usr/bin/env python
# @author gustavo.uribe@cern.ch

import config
import loadhelpers
import networkx as nx
import argparse
import csv
from datetime import datetime
from statistics import mean
from graphhelper import GraphHelper



if __name__== "__main__":
	parser = argparse.ArgumentParser(description='Generate the paraments to be use training using maching learning algorithms. Result of the test are saved in ml_parameters.csv')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)
	gLH = loadhelpers.LoadHelpers(db)
	classes=gLH.getClasses()
	searchableClasses=gLH.getSearchableClasses()
	sysclasses=gLH.getSysClasses()
	helpers=gLH.getHelpers()
	graph_helper=GraphHelper(None,None,helpers,classes)
	graph_helper.create_graph(db)
	graph_helper.generate_feeding_graph()
	graph_helper.calculate_granularity_per_subsystem()
	graph_helper.calculate_out_centrality()
	graph_helper.calculate_centrality_per_subsystem()
	nodes_sorted_by_centrality=sorted(graph_helper.out_centrality_dict.items(), key=lambda x: x[1], reverse=True)

	print("Calculating degree centrality")
	deg_centrality=nx.degree_centrality(graph_helper.feeding_graph.reverse())
	print("Calculating closeness centrality")
	cl_centrality=nx.closeness_centrality(graph_helper.feeding_graph.reverse())
	print("Calculating betweenness centrality")
	bet_centrality=nx.betweenness_centrality(nx.DiGraph(graph_helper.feeding_graph).reverse())
	print("Calculating harmonic centrality")
	harm_centrality=nx.harmonic_centrality(nx.DiGraph(graph_helper.feeding_graph).reverse())
	
	csv_output=[]
	nodes_sorted_iter=iter(nodes_sorted_by_centrality)


	for faulty_uid,centrality in nodes_sorted_iter:  
		if not db.test_object("SystemBase",faulty_uid): continue
		print("Simulation the fault of the object %s."%faulty_uid)
		faulty_obj=graph_helper.get_object(db,faulty_uid)
		faulty_obj['switch']="off"
		t_start=datetime.now() 	
		affected,iterations=graph_helper.propagate_change_effect(db,faulty_obj,return_iterations=True)
		iterations=iterations*len(affected)
		tdelta = datetime.now()-t_start
		objs_off=[obj for obj in affected if graph_helper.get_object(db,obj)["state"]=="off"]
		alarms_trigged=[obj for obj in objs_off if graph_helper.get_class_name(obj)=="Alarm"]
		subsystem=graph_helper.graph.nodes[faulty_uid]["subsystem"]
		predecessors=graph_helper.get_common_parents([faulty_uid])

		average_centrality_objs=mean([graph_helper.out_centrality_dict[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		average_centrality_alarms=mean([graph_helper.out_centrality_dict[obj] for obj in alarms_trigged]) if len(alarms_trigged)>0 else 0
		average_centrality_predecessors=mean([graph_helper.out_centrality_dict[obj] for obj in predecessors]) if len(predecessors)>0 else 0

		average_deg_centrality_objs=mean([deg_centrality[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		average_deg_centrality_alarms=mean([deg_centrality[obj] for obj in alarms_trigged]) if len(alarms_trigged)>0 else 0
		average_deg_centrality_predecessors=mean([deg_centrality[obj] for obj in predecessors]) if len(predecessors)>0 else 0

		average_cl_centrality_objs=mean([cl_centrality[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		average_cl_centrality_alarms=mean([cl_centrality[obj] for obj in alarms_trigged]) if len(alarms_trigged)>0 else 0
		average_cl_centrality_predecessors=mean([cl_centrality[obj] for obj in predecessors]) if len(predecessors)>0 else 0

		average_bet_centrality_objs=mean([bet_centrality[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		average_bet_centrality_alarms=mean([bet_centrality[obj] for obj in alarms_trigged]) if len(alarms_trigged)>0 else 0
		average_bet_centrality_predecessors=mean([bet_centrality[obj] for obj in predecessors]) if len(predecessors)>0 else 0

		average_harm_centrality_objs=mean([harm_centrality[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		average_harm_centrality_alarms=mean([harm_centrality[obj] for obj in alarms_trigged]) if len(alarms_trigged)>0 else 0
		average_harm_centrality_predecessors=mean([harm_centrality[obj] for obj in predecessors]) if len(predecessors)>0 else 0

		entry={"class":faulty_obj.class_name(),"faulty_obj":faulty_uid,"subsystem":subsystem,"granularity_subsystem":graph_helper.granularity_per_subsystem[subsystem],"simulation_tdelta":tdelta.total_seconds(), "number_iteraciones":iterations,"number_obj_off":len(objs_off),"alarms_trigged":len(alarms_trigged),"number_of_predecessors":len(predecessors),"eigenvector_centrality":centrality, "avg. eigen. centrality objects off":average_centrality_objs,"avg.eigen. centrality alarms trigged":average_centrality_alarms,"avg. eigen. centrality predecessors":average_centrality_predecessors,"degree_centrality":deg_centrality[faulty_uid], "avg. degree. centrality objects off":average_deg_centrality_objs,"avg.degree. centrality alarms trigged":average_deg_centrality_alarms,"avg. degree. centrlity predecessors":average_deg_centrality_predecessors,"closeness_centrality":cl_centrality[faulty_uid], "avg. cl. centrality objects off":average_cl_centrality_objs,"avg.cl. centrality alarms trigged":average_cl_centrality_alarms,"avg. cl. centrlity predecessors":average_cl_centrality_predecessors,"betweenness_centrality":bet_centrality[faulty_uid], "avg. bet. centrality objects off":average_bet_centrality_objs,"avg.bet. centrality alarms trigged":average_bet_centrality_alarms,"avg. bet. centrlity predecessors":average_bet_centrality_predecessors,"harmonic_centrality":harm_centrality[faulty_uid], "avg. harm. centrality objects off":average_harm_centrality_objs,"avg.harm. centrality alarms trigged":average_harm_centrality_alarms,"avg. harm. centrlity predecessors":average_harm_centrality_predecessors}
		csv_output.append(entry)
		print(entry)
	with open('ml_parameters.csv', 'w', newline='') as csvfile:
		fieldnames = csv_output[0].keys()
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

		writer.writeheader()
		for entry in csv_output:
			writer.writerow(entry)
	print("Results saved in file %s."%csvfile.name)
