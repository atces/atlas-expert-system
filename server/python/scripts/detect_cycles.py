#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Create a graph based on the database and detect cycles on it.

import networkx as nx
import config
import logging
import argparse
import loadhelpers
from graphhelper import GraphHelper
import os.path
import scripts.AtlasStyle as Style
from ROOT import TCanvas,TH1F,kBlue


 
if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Create a graph based on the database and detect cycles on it.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-gp','--graph_path', required=False, help='Complete graph path', default='../../../data/graph.gexf')
	parser.add_argument('-f','--feedings', required=False, help='Detect cycles in up-down direction', action='store_true')
	parser.add_argument('-g','--graph', required=False, help='If a graph is required', action='store_true')
	parser.add_argument('-n','--min_steps', required=False, type=int,help='Print the steps of the cycles with minimun a [min_steps]', default=25)
	args = parser.parse_args()
	 
	logger.debug("Loading database file...")	
	db=config.Configuration("oksconfig:"+args.database)
	logger.debug("Loading helpers...")	
	gLH = loadhelpers.LoadHelpers(db)
	helpers=gLH.getHelpers()
	classes=gLH.getClasses()
	graphHelper=GraphHelper(None,None,helpers,classes)
	if not os.path.isfile(args.graph_path):
		graph=graphHelper.create_graph(db,args.graph_path) 
	else:
		graph=nx.MultiDiGraph(nx.read_gexf(args.graph_path)) 
		graphHelper.setGraph(graph)
	graphHelper.print_tree(graph,"PC-ATLAS-PUB-03")
	graphHelper.print_tree(graph,"EOD1_1DX")
	if args.feedings:
		unidirectional_graph=graphHelper.generate_feeding_graph()
	else:
		unidirectional_graph=graphHelper.generate_depending_graph()
	cycles=graphHelper.detect_cycles(unidirectional_graph)
	logger.info("%i founded"%len(list(cycles)))
	graphHelper.remove_subcycles(cycles)

	counter_frequency_steps={}
	counter_frequency_steps_per_class={}
	counter_frequency_cycles_per_class={}
	cycle_hashes=[]
	for cycle in cycles:
		if len(cycle) not in counter_frequency_steps:  counter_frequency_steps[len(cycle)]=0
		cycle_ordered_str=str(sorted([str(x) for x in cycle]))
		if hash(cycle_ordered_str) not in cycle_hashes: 
			counter_frequency_steps[len(cycle)]+=1
			if len(cycle)>=args.min_steps:
				logger.debug("Cycle of %i steps: %s"%(len(cycle),str(cycle)))
				#path=get_cycle_path(unidirectional_graph,cycle[0])
				#for step in path:
				#	logger.info(step)
			cycle_hashes.append(hash(cycle_ordered_str))
		classes_per_cycle=[]
		for step in cycle:
			if 'class' not in unidirectional_graph.nodes[step]: print ("No class in %s"%step)
			clazz=unidirectional_graph.nodes[step]['class']
			if clazz not in counter_frequency_steps_per_class:  counter_frequency_steps_per_class[clazz]=0
			counter_frequency_steps_per_class[clazz]+=1
			if clazz not in classes_per_cycle: classes_per_cycle.append(clazz)
		for clazz in classes_per_cycle:
			if clazz not in counter_frequency_cycles_per_class: counter_frequency_cycles_per_class[clazz]=0
			counter_frequency_cycles_per_class[clazz]+=1
	logger.debug("There are "+str(sum(counter_frequency_steps.values()))+" cycles with different elements.")

	if (args.graph):
		Style.SetAtlasStyle()
		c1 = TCanvas( 'c1', 'Steps per cycle', 800, 600 )
		gr= TH1F('gr','Steps per cycle',max(counter_frequency_steps.keys()),1,max(counter_frequency_steps.keys())+1)
		
		for i in sorted(counter_frequency_steps.keys()):
			for j in range(counter_frequency_steps[i]): gr.Fill(i)
		 
		gr.GetXaxis().SetTitle( 'Steps' )
		gr.GetYaxis().SetTitle( 'Cycles' )
		gr.SetLineColor(kBlue-2)
		gr.Draw() 
		c1.Update()

		c2 = TCanvas( 'c2', 'Steps per each class', 800, 600 )
		gr2= TH1F('gr2','Steps per each class',len(counter_frequency_steps_per_class.keys()),0,len(counter_frequency_steps_per_class.keys()))
		
		for i in sorted(counter_frequency_steps_per_class.keys()):
			for j in range(counter_frequency_steps_per_class[i]): gr2.Fill(i,1)
		 
		gr2.LabelsDeflate()
		gr2.GetXaxis().SetTitle( 'Classes' )
		gr2.GetYaxis().SetTitle( 'Steps' )
		gr2.SetLineColor(kBlue-2)
		gr2.Draw() 
		c2.Update()

		c3 = TCanvas( 'c3', 'Cycles per each class', 800, 600 )
		gr3= TH1F('gr3','Cycles per each class',len(counter_frequency_cycles_per_class.keys()),0,len(counter_frequency_cycles_per_class.keys()))
		
		for i in sorted(counter_frequency_cycles_per_class.keys()):
			for j in range(counter_frequency_cycles_per_class[i]): gr3.Fill(i,1)
		 
		gr3.GetXaxis().SetTitle( 'Classes' )
		gr3.GetYaxis().SetTitle( 'Cycles' )
		gr3.SetLineColor(kBlue-2)
		gr3.Draw() 
		c3.Update()
		input("Press enter to quit...")
