#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import action
    
    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db=config.Configuration("oksconfig:%s"%(args.database))
    hp = action.ActionHelper()
    hp.setDb(db)
    actions=db.get_objs("Action")
    for action in actions:
        print "Process action: %s" % action.UID()
        '''
        for affected in action["interlocks"]:
            print " %s@%s.interlockedBy=%s" % (affected.UID(),affected.class_name(),action.UID())
            obj=db.get_obj(affected.class_name(),affected.UID())
            obj["interlockedBy"]=[action,]
            pass
        '''
        obj=db.get_obj('Action', action.UID())

        obj={
            "UID":action.UID(),
            "switch":"on",
            "state":"on"}
        hp.addObj(obj, False, True)
        pass
        
    db.commit("")
    sys.exit(0)
