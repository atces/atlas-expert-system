#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Creates a graph.gexf file using the information of the database


import networkx as nx
import config
import logging
import argparse
import loadhelpers
from graphhelper import GraphHelper


if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Create a graph based on the database and detect cycles on it.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	logger.debug("Loading database file...")	
	db=config.Configuration("oksconfig:"+args.database)
	meta_db=config.Configuration("oksconfig:%s%s"%(db.active_database[:db.active_database.rfind('/')+1],db.get_obj("MetaInfo","Singleton")['metadataFilename'])) if db else None
	logger.debug("Loading helpers...")	
	gLH = loadhelpers.LoadHelpers(db)
	helpers=gLH.getHelpers()
	classes=gLH.getClasses()
	graphHelper=GraphHelper(None,None,helpers,classes)
	gLH = loadhelpers.LoadHelpers(db)

	logger.debug("Creating graph...")	
	graph=graphHelper.create_graph(db,"graph.gexf") 

