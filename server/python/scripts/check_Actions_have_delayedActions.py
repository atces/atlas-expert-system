#!/usr/bin/env python
# check actions have delayed actions
# print actions that don't have delayed actions and possible delayedactions


if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers
    helpers=loadhelpers.LoadHelpers()
    classes=helpers.classes
    helps=loadhelpers.LoadHelpers().getHelpers()

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    #db = config.Configuration("oksconfig:../../../data/sb.data.xml")
    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hpdi = digitalinput.DigitalInputHelper()
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper()
    hpal.setDb(db)
    hpac = action.ActionHelper()
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper()
    hpda.setDb(db)
    # f = open("MissingDigitalInputs.txt", "w")
    counter=0
    more={}
    less={}
    newda={}
    problems={}
    errors=[]
    #get every delayed action
    da_list = db.get_objs("DelayedAction")
    for da in da_list:
        s_da=helps["DelayedAction"].serialize(da)
        da_name=da.UID()
        #print s_db_action
        #print len(s_db_action["delayedactions"])
        try:
            if True: #len(s_da["action"])==0:
                #print "Empty: ", a
                #remove delay time to get action name
                a= da_name[:da_name.rfind("_")]
                #print a
                try:
                    db_action=db.get_obj("Action", a)
                    #print a_name, " -> ", db_action.UID() 
                    s_a=helps["Action"].serialize(db_action)
                    
                    if da_name not in s_a["delayedactions"]:
                        print "------------------------------------------------"
                        print "%s not in %s" % (da_name, a)
                        a_obj={
                            "UID":a,
                            "delayedactions":da_name
                        }
                        #print a_obj
                        #hpac.addObj(a_obj, False, True)
                        #print "DONE"
                        pass
                    else:
                        #print "ERROR"
                        pass
                    pass
                except:
                    print "NO: ", a
                    pass
                pass
            pass
        except:
            print "---Error in ", da_name
            pass
        #print "OK"
        pass
    #db.commit("")
    pass



'''
        for block in block_text:
            print "---"
            f_alarms=[]
            #print block
            for line in block.split("\n"):
                if line.startswith("ACTION  "):
                    f_action_name= line.split(" ")[2]
                    print f_action_name
                    pass
                if line.startswith("      AL_"):
                    f_alarms.append(line.split(" ")[6])
                    pass
                pass
            print f_action_name
            #print f_alarms
            try:
                db_action=db.get_obj("Action", f_action_name)
                pass
            except:
                print db_action.UID()
                errors.append(f_action_name)
                pass
            pass
        pass
    print "Errors:"
    for e in errors:
        print e
        pass
    #db.commit("")
    
    print counter
    print "More:"
    for m in more:
        print m
    print "less"
    for l in less:
        print l
    print "Actions in file but not in db: ", problems
    print "DelayedActions in file that are not in db", newda
    print "New actions to be created:", problems
    print "present_in_file_not_in_db", present_in_file_not_in_db
    print "present_in_file_not_in_db", len(present_in_file_not_in_db)
    
    pass
'''


'''
#!/usr/bin/env python
# check actions have delayed actions
# print actions that don't have delayed actions and possible delayedactions


if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers
    helpers=loadhelpers.LoadHelpers()
    classes=helpers.classes
    helps=loadhelpers.LoadHelpers().getHelpers()

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    #db = config.Configuration("oksconfig:../../../data/sb.data.xml")
    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hpdi = digitalinput.DigitalInputHelper()
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper()
    hpal.setDb(db)
    hpac = action.ActionHelper()
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper()
    hpda.setDb(db)
    # f = open("MissingDigitalInputs.txt", "w")
    counter=0
    more={}
    less={}
    newda={}
    problems={}
    errors=[]
    with open("../../data/actions_without_delayedactions.txt", 'r') as file:
        data="".join(line for line in file)
        block_text=data.split("\n")
        print block_text
        for a in block_text:
            try:
                #print "+",a
                db_action=db.get_obj("Action", a)
                s_db_action=helps["Action"].serialize(db_action)
                #print s_db_action
                #print len(s_db_action["delayedactions"])
                if len(s_db_action["delayedactions"])==1:
                    #print "Empty: ", a
                    possible_da="%s_0" % a
                    #print possible_da
                    try:
                        db_poss_daction=db.get_obj("DelayedAction", possible_da)
                        #print a, " -> ", db_poss_daction.UID() 
                        pass
                    except:
                        print a
                        pass
                    
                    pass
                #print "OK"
                pass
            except:
                print "-",a
                pass
            pass
        pass
    pass


        for block in block_text:
            print "---"
            f_alarms=[]
            #print block
            for line in block.split("\n"):
                if line.startswith("ACTION  "):
                    f_action_name= line.split(" ")[2]
                    print f_action_name
                    pass
                if line.startswith("      AL_"):
                    f_alarms.append(line.split(" ")[6])
                    pass
                pass
            print f_action_name
            #print f_alarms
            try:
                db_action=db.get_obj("Action", f_action_name)
                pass
            except:
                print db_action.UID()
                errors.append(f_action_name)
                pass
            pass
        pass
    print "Errors:"
    for e in errors:
        print e
        pass
    #db.commit("")
    
    print counter
    print "More:"
    for m in more:
        print m
    print "less"
    for l in less:
        print l
    print "Actions in file but not in db: ", problems
    print "DelayedActions in file that are not in db", newda
    print "New actions to be created:", problems
    print "present_in_file_not_in_db", present_in_file_not_in_db
    print "present_in_file_not_in_db", len(present_in_file_not_in_db)
    
    pass
'''
