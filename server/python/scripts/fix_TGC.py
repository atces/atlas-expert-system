#ignacio.asensi@cern.ch
import os

import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import fta
import messenger
import loadhelpers
import ProbTree
import traceback
import helper
import argparse
import group
import subdetector
#import classes from loadhelpers
helpers=loadhelpers.LoadHelpers()
classes=helpers.classes



parser = argparse.ArgumentParser()
parser.add_argument("database",help="database file")
args=parser.parse_args()
print "Load database: %s" % args.database
db = config.Configuration("oksconfig:%s"%(args.database))
verbose=True
errors=""


#getting map relations

    
obj_list = filter(lambda e: e.UID().startswith("TGC_"), db.get_objs("SubDetector"))

hp_group = group.GroupHelper()
hp_group.setDb(db)
hp_subdetector = subdetector.SubDetectorHelper()
hp_subdetector.setDb(db)

g="TGC_subdetector"
g_o={"UID":g, "description":"Group for TGC subdetector"}
hp_group.addObj(g_o, False, True)

for o in obj_list:
    print o
    sub_o={"UID":o.UID(), "groupedBy":g}
    hp_subdetector.addObj(sub_o, False, True)

    g_o={"UID":g, "groupTo":o.UID()}
    hp_group.addObj(g_o, False, True)
    pass
db.commit("")
