#!/usr/bin/env python
#######################################################
# Check the Alarms in ATCES have the Actions as in DSS
# - Extra actions will be removed
# - Missing actions will be added
# Alarms that don't have an action cannot be checked
# Carlos.Solans@cern.ch
# October 2021
#######################################################

import os
import sys
import json
import config
import argparse

def get_or_create(source,class_name,uid,verbose):
    obj=None
    try: obj=source.get_obj(class_name,uid)
    except: pass
    if not obj:
        if verbose: print("%s does not exist... we should create it: %s" % (class_name,uid))
        db.create_obj(class_name, uid)
        obj=source.get_obj(class_name,uid)
        pass
    return obj

def safe_append(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    found=False
    for obj in objs: 
        if obj.UID()==what.UID(): found=True
        pass
    if not found: objs.append(what)
    source[rel]=objs
    pass

def safe_remove(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    new_objs=[]
    for obj in objs: 
        if obj.UID()!=what.UID(): new_objs.append(obj)
        pass
    source[rel]=new_objs
    pass

def safe_remove_single(source,rel,what):
    source[rel]=None
    pass
    
parser=argparse.ArgumentParser()
parser.add_argument("-d","--database",default="sb.data.xml",help="database file")
parser.add_argument("-a2a",help="A2A json file",required=True)
parser.add_argument("-input",help="Input json file, to delete extra inputs")
parser.add_argument("-alarm",help="Alarm json file, to delete extra alarms")
parser.add_argument("-action",help="Action json file, to delete extra actions")
parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
args=parser.parse_args()

print ("Load database: %s" % args.database)
db=config.Configuration("oksconfig:%s"%(args.database))

fr1=open(args.a2a)
a2a=json.load(fr1)
for dssAlarm in a2a:
    print("Processing alarm: %s" % dssAlarm)
    atcesAlarm=get_or_create(db,"Alarm",dssAlarm,args.verbose)
    atcesDelayedActions=atcesAlarm.get_objs("actions")
    atcesDelayedActionsUIDs=[]
    for a in atcesDelayedActions: atcesDelayedActionsUIDs.append(a.UID())
    dssDelayedActions=[]
    if args.verbose: print("-- Check missing DSS actions in ATCES")
    for dssAction in a2a[dssAlarm]:
        if dssAction=="DSU": continue
        dssDelayedAction = "%s_%i" % (dssAction,a2a[dssAlarm][dssAction])
        if not dssDelayedAction in atcesDelayedActionsUIDs:
            print("-- We should add DelayedAction: %s to Alarm: %s" % (dssDelayedAction,dssAlarm))
            atcesDelayedAction = get_or_create(db,"DelayedAction",dssDelayedAction,args.verbose)
            atcesDelayedAction.set_u32("delay",a2a[dssAlarm][dssAction])
            atcesAction = get_or_create(db,"Action",dssAction,args.verbose)
            atcesDelayedAction["action"]=atcesAction
            safe_append(atcesAction,"delayedactions",atcesDelayedAction)
            safe_append(atcesAlarm,"actions",atcesDelayedAction)
            safe_append(atcesDelayedAction,"startedByAlarms",atcesAlarm)
            pass
        dssDelayedActions.append(dssDelayedAction)
        pass
    if args.verbose: print("-- Check that ATCES does not have any extra actions")
    for atcesDelayedAction in atcesDelayedActions:
        if not atcesDelayedAction.UID() in dssDelayedActions:
            print("-- We should remove DelayedAction: %s from Alarm: %s" % (atcesDelayedAction.UID(),dssAlarm))
            safe_remove(atcesAlarm,"actions",atcesDelayedAction)
            pass
        pass
    pass

if args.input:
    fr2=open(args.input)
    atcesInputs=db.get_objs("DigitalInput")
    dssInputs=json.load(fr2)
    atcesInputsUIDs=[]
    for a in atcesInputs: 
        if "DI_" not in a.UID(): continue
        atcesInputsUIDs.append(a.UID())
        pass
    for atcesInput in atcesInputs:
        print("Processing input: %s" % atcesInput.UID())
        if not atcesInput.UID() in dssInputs:
            print("-- We should remove DigitalInput: %s" % (atcesInput.UID()))
            pass
        pass
    pass

if args.action:
    fr2=open(args.action)
    atcesActions=db.get_objs("Action")
    dssActions=json.load(fr2)
    for atcesAction in atcesActions:
        print("Processing Action: %s" % atcesAction.UID())
        if not atcesAction.UID() in dssActions:
            print("-- We should remove Action: %s" % (atcesAction.UID()))
            print("-- First remove DSU from Action and Action from DSU")
            atcesDsu=atcesAction["DSU"]
            atcesAction["DSU"]=None
            if atcesDsu: safe_remove(atcesDsu,"actions",atcesAction)
            print("-- Then remove DelayedActions from Action and Action from DelayedActions")
            atcesDelayedActions=atcesAction["delayedactions"]
            atcesAction["delayedactions"]=[]
            for atcesDelayedAction in atcesDelayedActions:
                safe_remove_single(atcesDelayedAction,"action",atcesAction)
                pass
            print("-- Then remove Interlocks from Action and Action from Interlocks")
            atcesInterlocks=atcesAction["interlocks"]
            atcesAction["interlocks"]=[]
            for atcesInterlockedBy in atcesInterlocks:
                safe_remove(atcesInterlockedBy,"interlockedBy",atcesAction)
                pass
            print("-- Finally destroy object")
            db.destroy_obj(atcesAction)
            pass
        else:
            dssAction=dssActions[atcesAction.UID()]
            print("-- Modify the parameters of the Action")
            dsu_dss2atces={"DSU1":"DSU_1","DSU2":"DSU_2","DSU3":"DSU_3","DSU4":"DSU_4","DSU5":"DSU_5","DSU6":"DSU_6","DSU7":"DSU_7",}
            atcesDSU=get_or_create(db,"DSU",dsu_dss2atces[dssAction["DSU"]],args.verbose)
            atcesAction["DSU"]=atcesDSU
            atcesAction["crate"]=dssAction["Crate"]
            atcesAction["module"]=int(dssAction["Module"])
            atcesAction["slot"]=int(dssAction["Slot"])
            atcesAction["channel"]=int(dssAction["Channel"])
            print("-- Attach the action to the DSU")
            safe_append(atcesDSU,"actions",atcesAction)
            pass
        pass
    pass

    
if args.yes: db.commit("import_dss_json")













