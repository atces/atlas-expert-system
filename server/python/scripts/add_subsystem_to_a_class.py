#!/usr/bin/env python
# @author gustavo.uribe@cern.ch

import config
import logging
import argparse
import loadhelpers
from graphhelper import GraphHelper


if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Create a graph based on the database and detect cycles on it.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-c','--clazz', required=True, help='Class where the subsystem will be set.')
	parser.add_argument('-s','--subsystem', required=True, help='Subsytem to be assigned to all the members of the class.')
	args = parser.parse_args()

	logger.debug("Loading database file...")	
	db=config.Configuration("oksconfig:"+args.database)

	class_objs=db.get_objs(args.clazz)
	for obj in class_objs:
		obj["subsystem"]=args.subsystem
	db.commit()
	
