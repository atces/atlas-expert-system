#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Suggests the most probable power supply if not assigned 

import config
import re
import logging
import argparse
import helper
import IT_nomenclature

def add_power_source(db,device,power_source,relation):
	helper_obj=helper.Helper(db=db)
	inverse_relation=helper_obj.get_inverse_relation(relation)
	power_source=db.get_obj("PowerProviderBase",power_source.UID())
	if power_source.UID() not in [_device.UID() for _device in device[relation]]: device[relation]+=[power_source] 
	if device.UID() not in [_obj.UID() for _obj in power_source[inverse_relation]]: power_source[inverse_relation]=[db.get_obj("PowerReceiverBase",x.UID()) for x in power_source.get_objs(inverse_relation)]+[device]
	

def advice_power_source(db,uids=None,classes=None,force_update=False):
	if not classes: classes= ["PowerReceiverBase"]
	for clazz in classes:
		objects=db.get_objs(clazz) if not uids else [db.get_obj(clazz,uid) for uid in uids] 
		for device in objects:
			if force_update: 
				helper_obj=helper.Helper(db=db)
				power_relations=['poweredBy','requiresPowerFrom']
				for relation in power_relations:
					inverse_relation=helper_obj.get_inverse_relation(relation)
					prev_powers=device[relation]
					device[relation]=[]
					for prev_power in prev_powers:
						prev_power[inverse_relation]=[obj for obj in prev_power[inverse_relation] if not obj.UID()==device.UID()]
			if force_update or (not device['poweredBy'] and not device['requiresPowerFrom']):
				if 'inDependantContainer' in device.__schema__['relation'].keys() and device['inDependantContainer']:
					for container in device.get_objs("inDependantContainer"):
						if db.test_object("PowerProviderBase",container.UID()):
							yn=input("Is the device "+device.UID()+" receiving power from "+container.UID()+"? (y/yes/n/no/u/unknown/r/required):")
							if yn.lower().startswith('y'):
								add_power_source(db,device,container,'poweredBy')
							elif yn.lower().startswith('r'):
								add_power_source(db,device,container,'requiresPowerFrom')
						for element in container.get_objs("isDependantContainerOf"):
							if (IT_nomenclature.is_valid_name(element.UID()) and "POE" in IT_nomenclature.get_extended_configuration(element.UID())) or (not IT_nomenclature.is_valid_name(element.UID()) and db.test_object("PowerProviderBase",element.UID())):

								yn=input("Is the device "+device.UID()+" receiving power from "+element.UID()+"? (y/yes/n/no/u/unknown/r/required):")
								if yn.lower().startswith('y'):
									add_power_source(db,device,element,'poweredBy')
								elif yn.lower().startswith('r'):
									add_power_source(db,device,element,'requiresPowerFrom')
				if 'isNetworkPeerIn' in device.__schema__['relation'].keys() and device['isNetworkPeerIn']:
					for subnet in device.get_objs("isNetworkPeerIn"):
						for path in subnet.get_objs("hasPath"):
							if IT_nomenclature.get_extended_configuration(path.UID()) and "POE" in IT_nomenclature.get_extended_configuration(path.UID()) and db.test_object("PowerProviderBase",path.UID()):
								yn=input("Is the device "+device.UID()+" receiving power from "+path.UID()+"? (y/yes/n/no/u/unknown):")
								if yn.lower().startswith('y'):
									add_power_source(db,device,element,'poweredBy')
		
def get_power_receivers_in_container(db,containers,uids=None):
	if not uids: uids=[]
	if not isinstance(containers,list): containers=[containers]
	for container in containers: uids+=[element.UID() for element in db.get_obj("ContainerProviderBase",container)['isDependantContainerOf'] if not "SwitchBoard"==element.class_name() if not "RackElectricalDistribution"==element.class_name() if not element.class_name()=="ElectricalRepartitionBoxUSA15" if not element.class_name()=="ElectricalRepartitionBoxUS15"]
	return uids

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Helps to select the correct power source for an object according to its relationships.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid',nargs="+",required=False,help="UID(s) of the object for updating the power source.")
	parser.add_argument('-c','--classes',nargs="+", required=False,help="Classes of the objects for updating the power source. All objects of the class are evaluated.")
	parser.add_argument('-ctr','--container',nargs="+", required=False,help="List of containers for updating the power sources of their elements.")
	parser.add_argument("-f", "--force_update", help="Force update of the power source of the objects", action="store_true",required=False)
	args = parser.parse_args()

	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:%s"%(args.database))
	uids=args.uid
	if args.container: 
		uids=get_power_receivers_in_container(db,args.container,uids)
		if len(uids)>0: advice_power_source(db,uids,args.classes,args.force_update)
	else:
		advice_power_source(db,uids,args.classes,args.force_update)
	db.commit('')
