#!/usr/bin/env python
# check the already filled DSSEvents for the DSSLogs
# Florian.Haslbeck@cern.ch
# February 2024

def find_DSSLogs(db, verbose=False):
    """ find all DSSLog config_objs in database
        input:
            db: database object
        output:
            list of DSSLog objects
    """
    # get all DSSLogs
    config_objs = db.get_objs("DSSLog")
    if verbose: print(f"Found {len(config_objs)} DSSLogs")
    return config_objs


def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj



if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()


    # get DSSLogs from database
    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    dsslogs_config_objs = find_DSSLogs(db, verbose=args.verbose) # this config_objs

    for dsslog in dsslogs_config_objs:
        if not dsslog.UID().startswith("log_"): continue
        print(dsslog, dsslog['time'])

        if dsslog['time'].count("-") > 0:
            print("ERROR: time also contains a date")
            
            date, time = dsslog['time'].split(" ")
            dsslog.set_string("date", date)
            dsslog.set_string("time", time)

    db.commit()
        
        
        

