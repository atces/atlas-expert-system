#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import re
    import minimax
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import action
    
    import minimaxcentral


    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    #ecs = filter(lambda e: e.UID().startswith("TRT_E"), db.get_objs("SubDetector"))

    hp_rack = rack.RackHelper()
    hp_rack.setDb(db)
    hp_mc = minimaxcentral.MinimaxCentralHelper()
    hp_mc.setDb(db)
    hp_minimax= minimax.MinimaxHelper()
    hp_minimax.setDb(db)
    hp_action= action.ActionHelper()
    hp_action.setDb(db)
    actions = filter(lambda e: e.UID().startswith("O_INF_MINIMAX_Y"), db.get_objs("Action"))
    errors={}
    for act in actions:
        actname=act.UID()
        print actname
        rackname=actname.replace("O_INF_MINIMAX_","")
        rackname=rackname[:1] + '.' + rackname[1:]
        rackname=rackname[:4] + '-' + rackname[4:]
        rackname=rackname[:7] + '.' + rackname[7:]
        minimaxname="%s_Minimax" % rackname
        try:
            obj_act={
                "UID":actname,
                "deploys":minimaxname
            }
            hp_action.addObj(obj_act, False, True)
            obj_m={
                "UID":minimaxname,
                "deployedBy":actname
            }
            hp_minimax.addObj(obj_m, False, True)
            pass
        except:
            errors[rackname]=minimaxname
        pass
    print errors
    pass
db.commit("")

'''
    controlracks=["Y.08-09.A2", "Y.16-07.S2"]
    #m_controlracks=["Y.08-09.A2_minimaxCentral", "Y.16-07.S2_minimaxCentral"]
    r_minimax=["Y.24-14.A1_Minimax","Y.25-14.A1_Minimax","Y.26-14.A1_Minimax","Y.27-14.A1_Minimax","Y.07-16.A1_Minimax","Y.09-16.A1_Minimax","Y.25-19.A2_Minimax","Y.26-19.A2_Minimax","Y.27-19.A2_Minimax","Y.28-19.A2_Minimax","Y.29-19.A2_Minimax","Y.10-16.A2_Minimax","Y.11-16.A2_Minimax","Y.12-16.A2_Minimax","Y.13-16.A2_Minimax","Y.14-16.A2_Minimax","Y.15-16.A2_Minimax","Y.16-16.A2_Minimax","Y.22-16.A2_Minimax","Y.23-16.A2_Minimax","Y.24-16.A2_Minimax","Y.25-16.A2_Minimax","Y.26-16.A2_Minimax","Y.27-16.A2_Minimax","Y.28-16.A2_Minimax","Y.29-16.A2_Minimax","Y.04-14.A2_Minimax","Y.05-14.A2_Minimax","Y.06-14.A2_Minimax","Y.07-14.A2_Minimax","Y.22-14.A2_Minimax","Y.23-14.A2_Minimax","Y.24-14.A2_Minimax","Y.25-14.A2_Minimax","Y.26-14.A2_Minimax","Y.27-14.A2_Minimax","Y.28-14.A2_Minimax","Y.29-14.A2_Minimax","Y.04-11.A2_Minimax","Y.05-11.A2_Minimax","Y.22-11.A2_Minimax","Y.23-11.A2_Minimax","Y.24-11.A2_Minimax","Y.25-11.A2_Minimax","Y.26-11.A2_Minimax","Y.27-11.A2_Minimax","Y.28-11.A2_Minimax","Y.29-11.A2_Minimax","Y.25-05.A2_Minimax","Y.26-05.A2_Minimax","Y.27-05.A2_Minimax","Y.28-05.A2_Minimax","Y.29-05.A2_Minimax","Y.07-02.A2_Minimax","Y.08-02.A2_Minimax","Y.24-02.A2_Minimax","Y.25-02.A2_Minimax","Y.05-14.S2_Minimax","Y.10-15.S2_Minimax","Y.11-15.S2_Minimax","Y.12-15.S2_Minimax","Y.23-13.S2_Minimax","Y.24-13.S2_Minimax","Y.25-13.S2_Minimax","Y.26-13.S2_Minimax","Y.23-15.S2_Minimax","Y.24-15.S2_Minimax","Y.25-15.S2_Minimax","Y.26-15.S2_Minimax","Y.27-15.S2_Minimax","Y.28-15.S2_Minimax","Y.29-15.S2_Minimax","Y.32-05.X8_Minimax","Y.55-05.X8_Minimax","Y.29-04.X7_Minimax","Y.60-04.X7_Minimax","Y.34-03.X1_Minimax","Y.60-04.X1_Minimax","Y.31-05.X0_Minimax","Y.57-05.X0_Minimax","Y.35-23.X8_Minimax","Y.36-23.X8_Minimax","Y.54-23.X8_Minimax","Y.55-23.X8_Minimax","Y.53-25.X4_Minimax","Y.35-25.X2_Minimax","Y.36-25.X2_Minimax","Y.59-24.X1_Minimax"]
    
    for controlrack in controlracks:
        if controlrack is "Y.08-09.A2":
            o_rack={
            "UID":controlrack,
            "location":"USA15"
            }
            hp_rack.addObj(o_rack, False, True)
            o_minimax_central={
                "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                "poweredBy": controlrack,
                "containedIn":controlrack,
                "location":"USA15","subsystem":"Minimax",
            }
            hp_mc.addObj(o_minimax_central, False, True)
            o_rack={
            "UID":controlrack,
            "powers": "%s%s" % (controlrack, "_minimaxCentral"),
            "contains":"%s%s" % (controlrack, "_minimaxCentral"),
            "location":"USA15"
            }
            hp_rack.addObj(o_rack, False, True)
            for m in r_minimax:
                if "A" in m:
                    o_minimax={
                        "UID":m,
                        "containedIn":m.replace("_Minimax",""),
                        "deployedBy":o_minimax_central["UID"],
                        "subsystem":"Minimax",
                        "poweredBy":o_minimax_central["UID"],
                    }
                    hp_minimax.addObj(o_minimax, False, True)
                    o_minimax_central={
                        "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                        "deploys": m,
                        "powers":m,
                    }
                    hp_mc.addObj(o_minimax_central, False, True)
                    
                    o_containerrack={
                        "UID":m.replace("_Minimax",""),
                        "contains":m
                    }
                    print "Adding ",  m
                    hp_rack.addObj(o_containerrack, False, True)
                    pass
                pass
            pass
        else:
            o_rack={
            "UID":controlrack,
            "location":"US15"
            }
            hp_rack.addObj(o_rack, False, True)
            o_minimax_central={
                "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                "poweredBy": controlrack,
                "containedIn":controlrack,
                "location":"US15","subsystem":"Minimax",
            }
            hp_mc.addObj(o_minimax_central, False, True)
            o_rack={
            "UID":controlrack,
            "powers": "%s%s" % (controlrack, "_minimaxCentral"),
            "contains":"%s%s" % (controlrack, "_minimaxCentral"),
            "location":"US15"
            }
            hp_rack.addObj(o_rack, False, True)
            for m in r_minimax:
                if "S" in m or "X" in m:
                    o_minimax={
                        "UID":m,
                        "containedIn":m.replace("_Minimax",""),
                        "deployedBy":o_minimax_central["UID"],
                        "subsystem":"Minimax",
                        "poweredBy":o_minimax_central["UID"],
                    }
                    hp_minimax.addObj(o_minimax, False, True)
                    o_minimax_central={
                        "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                        "deploys": m,
                        "powers":m,
                    }
                    hp_mc.addObj(o_minimax_central, False, True)
                    
                    o_containerrack={
                        "UID":m.replace("_Minimax",""),
                        "contains":m
                    }
                    print "Adding ",  m
                    hp_rack.addObj(o_containerrack, False, True)
                    pass
                pass
        
        pass
    db.commit("")
    
    
    
    
    
    
    
    racksUX15=["Y.53-23.X0","Y.59-23.X8","Y.27-23.X8", "Y.53-05.X8", "Y.33-05.X8"]
    
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec)
        #powersuplies=rackobj["contains"]
        for rack in racks:
            print "python rack.py ../../data/sb.data.xml --uid %s --controls %s -e -y" % (rackobj.UID(), powersuplie.UID())
            print ""
            print "python powersupply.py ../../data/sb.data.xml --uid %s --controlledby %s  -e -y" % ( powersuplie.UID(), rackobj.UID())
            print ""
            print ""
            pass
        pass
        

    racks=["Y.35-04.X7_supply","Y.36-04.X7_return ",
           "Y.35-03.X1_supply","Y.36-03.X1_return",
           "Y.51-24.X7_supply","Y.52-24.X7_return",
           "Y.52-25.X1_supply","Y.53-25.X1_return",]
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec.UID())
        for rack in racks:
            
            obj_rack={
                "UID":str(rack),
                "gasTo":ec.UID(),
                }
            #print "Adding Rackgas: ",  obj_rack["UID"]
            hp_gas.addObj(obj_rack, False, True)
            
            obj_ec={
                "UID":str(ec.UID()),
                "requiresGasFrom":str(rack)
                }
            #print "Adding Subdetector: ",  obj_ec["UID"]
            hp_sub.addObj(obj_ec, False, True)
            pass
        pass
        
    racks=["Y.24-14.A1","Y.25-14.A1","Y.26-14.A1", "Y.27-14.A1"]
    for rack in racks:
        r=db.get_obj("Rack", rack)
        print r.UID()
        pss=r["contains"]
        for ps in pss:
            obj_rack={
                "UID":str(rack),
                "controls":ps.UID(),
            }
            hp_rack.addObj(obj_rack, False, True)
            obj_ps={
                "UID":str(ps.UID()),
                "controlledBy":str(rack),
            }
            hp_ps.addObj(obj_ps, False, True)
        pass
    
    db.commit("")
    '''
