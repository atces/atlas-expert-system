#!/usr/bin/env python
import os
import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import fta
import messenger
import loadhelpers
import ProbTree
import traceback
import helper
import argparse
import copy

#import classes from loadhelpers
helpers=loadhelpers.LoadHelpers()
classes=helpers.classes
helps=loadhelpers.LoadHelpers().getHelpers()


parser = argparse.ArgumentParser()
parser.add_argument("database",help="database file")
args=parser.parse_args()
print "Load database: %s" % args.database
db = config.Configuration("oksconfig:%s"%(args.database))
verbose=False
errors=""


uids=["QSC1H-1EKXH","QSC1H-2EKXH","QSC1H-3EKXH","QSC1H-4EKXH","QSC1H-5EKXH","QSC1H-6EKXH","QSC1H-A-EH0601","QSC1H-A-EH0602","QSC1N-1EKXN"]

class_to="compressor"
class_from="gas"

to_module = __import__(class_to)
to_help=getattr(to_module, dir(to_module)[0])()
to_help.setDb(db)
to_attrs=getattr(to_module, dir(to_module)[0])().attrs

from_module = __import__(class_from)
from_help=getattr(from_module, dir(from_module)[0])()
from_help.setDb(db)
from_attrs=getattr(from_module, dir(from_module)[0])().attrs



classname_to= getattr(to_module, dir(to_module)[0])().classname
classname_from= getattr(from_module, dir(from_module)[0])().classname


#classes=loadhelpers.LoadHelpers().getClasses()


for at in to_attrs:
    if at not in from_attrs:
        print "Attribute %s not in target class %s. This relation will not be copied" % (at, classname_to)
        pass
    pass


if verbose == True:
    print to_attrs
    print ""
    print from_attrs
    pass




def removeCounterpart(original, counterpart):
    #print "Counter:", counterpart
    counter_ob=""
    removed=False
    for c in classes:
        #print c[0]
        try:
            counter_ob=db.get_obj(c[0], counterpart)
            #print "Found as ", c[0]
            #print "Before:", counter_ob
            pass
        except:
            pass
        pass
    if verbose == True: print "counter_ob:", type(counter_ob)
    if type(counter_ob)!="str":
        counter_ser= helps[counter_ob.class_name()].serialize(counter_ob)
        for at in counter_ser:
            if isinstance(counter_ser[at], list): 
                for a in counter_ser[at]:
                    if original == a:
                        #print "   %s --  %s -->  %s" % (counter_ob.UID(), at, a)
                        rel_delete={
                            "UID": counter_ob.UID(), 
                            at:a}
                        if verbose == True: print "Deleting relation: ", rel_delete
                        removed=True
                        pass
                    pass
                pass
            pass
        pass
    return removed




for u in uids:
    #get object from db
    db_o=db.get_obj(classname_from, u)
    if verbose == True: print "Changing ", db_o.UID()
    o= helps[classname_from].serialize(db_o)
    o["UID"]=u
    old_o=copy.copy(o)
    print "------------------------------------------------"
    print u
    #print old_o
    #check object relations to other
    for at in o:
        if at == "UID":continue
        #print len(at)
        if isinstance(o[at], list):
            arr=[]
            for a in o[at]:
                if at == "otherIds": continue
                #print len(a)
                if len(a)> 0: 
                    #remove counterpart relation
                    print "From %s -- %s --> %s" % (u, at, a)
                    print removeCounterpart(u, a)
                    pass
                pass
            pass
        pass
    #if verbose == True: print "Original object", old_o
    pass






print "end"
'''
attrs=getattr(module, dir(module)[0])().attrs



 
#getting map relations
module={}
for u in uids:
    if c[1] is not "water": continue
    if c[3] is True:
        filename=c[1]
        classname=c[0]
        print ""
        print "Loading objects of class: %s" % filename
        module = __import__(c[1])
        help=getattr(module, dir(module)[0])()
        help.setDb(db)
        obj_list = {}
        if verbose is True: print "Getting all objects of %s" % classname
        obj_list = db.get_objs(classname)
        #print obj_list
        if verbose is True: print "Stablishing gatherer class relations..."
        gatherers={}
        feeders={}# do we need this?
        attrs=getattr(module, dir(module)[0])().attrs
        for at in attrs:
            if "gather" in attrs[at]:# or  "feed" in attrs[at] :
                gatherers[at]=attrs[at]["type"]
                #print attrs[at]
                pass
            if "feed" in attrs[at]:# or  "feed" in attrs[at] :
                feeders[at]=attrs[at]["type"]
                #print attrs[at]
                pass
            pass
        print "gatherers", gatherers
        print "feeders", feeders
    
    
   ]
    racksUX15=["Y.53-23.X0","Y.59-23.X8","Y.27-23.X8", "Y.53-05.X8", "Y.33-05.X8"]
'''
