#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Delete an oks object and all the references to this object

import argparse
import config

def delete_oks_object_with_references(db,obj):
	for referencer in obj.referenced_by():
		referencer=db.get_obj(referencer.class_name(),referencer.UID())	
		for relation in referencer.__schema__['relation']:
			if isinstance(referencer[relation],list) and obj in referencer[relation]:
				referencer[relation]=[ x for x in referencer[relation] if not x==obj]	
			elif not isinstance(referencer[relation],list) and obj==referencer[relation]:
				referencer[relation]=None
	db.destroy_obj(obj)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-u", "--uid", help="UID of the object to delete", required=True)
	parser.add_argument("-c", "--clazz", help="Class of the object to delete", required=True)
	parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)
	delete_oks_object_with_references(db,db.get_obj(args.clazz,args.uid))
	db.commit('')	

