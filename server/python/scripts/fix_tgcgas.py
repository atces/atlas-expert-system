#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db=config.Configuration("oksconfig:%s"%(args.database))

    gss=db.get_objs("GasSystem")
    for gs in gss:
        if not "HCXGDIS001" in gs.UID(): continue
        if not "TGC" in gs["description"]: continue
        print "Process: %s" % gs.UID()
        sds=gs["gasTo"]
        for sd in sds:
            if not "TGC" in sd.UID(): continue
            ee=[]#sd["requiresGasFrom"]
            ee.append(gs)
            sd["requiresGasFrom"]=ee
            print "%s requiresGasFrom:" % sd.UID(),sd["requiresGasFrom"] 
            pass
        pass
    
    if args.yes: db.commit("")
    print ("Have a nice day")
    
