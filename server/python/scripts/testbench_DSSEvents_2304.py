#!/usr/bin/env python
#######################################################
# Testbench for DSSEvents
# Florian.Haslbeck@cern.ch
# November 2023
#######################################################

import pandas as pd
import datetime

import urllib.request
import json


not_unique = []
not_matched = []

def find_DSSLogs(db, verbose=False):
    """ find all DSSLogs in database
        input:
            db: database object
        output:
            list of DSSLog objects
    """
    # get all DSSLogs
    DSSLogs = db.get_objs("DSSLog")

    # filter DSSLogs (until they have been deleted from database)
    DSSLogs = [log for log in DSSLogs if "log_" in log.UID()]
    if verbose: print(f"Found {len(DSSLogs)} DSSLogs")
    return DSSLogs

def read_csv_to_df(csv):
    """ read in DSS events from csv file
        input:
            csv: csv file
        output:
            pandas dataframe
    available columns:
        'description', 'short_description', 'date', 'time', 'alarms',
       'alarm_times', 'people', 'elisa', 'type', 'converted_time', 'systems',
       'event_criticality', 'datetime', 'unixtime', 'weekday',
       'detector_state', 'meta_detector_state'
    
    """
    # read csv file
    df = pd.read_csv(csv, sep=";", header=0)

    # sort by date
    # df = df.sort_values(by=['datetime'])

    df = df.sort_values(by=['date'])
    # df["date"] = df["date"].apply(lambda x: x.strip())

    return df

def convert_row_col_to_list(row_col):
    """ convert row to list """
    return str(row_col).replace("[","").replace("]","").replace("'","").replace(" ", "").split(",")


def find_event_for_alarm(df, alarm, date_ddmmyy, time_hhmm):
    """ find event for alarm, date and time
        input:
            df: pandas dataframe
            alarm: alarm name
            date: date string
            time: time string
        output:
            event uid, description, date, time, alarms, alarm_times
    """

    def _uid(date_ddmmyy, time_hhmm):
        """ datetime as unix + type first 3 chars """
        # convert datetime string dd.mm.yy hh:mm to unix 
        date_str = f"{date_ddmmyy} {time_hhmm}"

        print("Creating uid...", date_str)

        try:
            unix = int(datetime.datetime.strptime(date_str, '%d.%m.%y %H:%M').timestamp())
        except:
            print("\n\n")
            print(date_ddmmyy, time_hhmm)
            input("Cannot create event uid Press Enter to continue...")
            unix  = "123"
        uid = f"event_{unix}"
        return uid

    def get_all_alarm_times(row, alarm_string):
        """ per row, return a list of alarm times for a given alarm """

        # first check if alarm_string is in row['alarms']
        if alarm_string in str(row['alarms']):
            alarm_list = convert_row_col_to_list(row['alarms'])
            
            # get indices of alarm_string in alarm_list
            alarm_indices = [i for i, alarm in enumerate(alarm_list) if alarm == alarm_string]

            # get alarm_times for alarm_indices
            if len(alarm_indices) > 0:
                
                alarm_times = convert_row_col_to_list(row['alarm_times'])
                alarm_times = [alarm_times[i] for i in alarm_indices]
                alarm_times = [datetime.datetime.strptime(time, "%H:%M") for time in alarm_times]
                return alarm_times
            
        return None
    
    # filter the date and to events with alarm
    df_filter = df[df['date'] == date_ddmmyy]



    print("df_filter", df_filter)
    print(alarm in str(df_filter['alarms']))
    df_filter = df_filter[df_filter['alarms'].apply(lambda x: alarm in x)]

    print("df_filter after alarm", df_filter)
   

    # if df is empty, return None
    if len(df_filter) == 0:
        not_matched.append((alarm, date_ddmmyy, time_hhmm))
        return None, None, None, None, None, None, None, None, None
    
    

    # if df has only one row, return this row
    if len(df_filter) == 1:

        print("Found unique event for ", alarm, date_ddmmyy, time_hhmm)

        row = df_filter.iloc[0]
        ret_time = time_hhmm if row['time'] is None else row['time']

        print(repr(ret_time))

        return _uid(row['date'], ret_time), row['description'], row['date'], ret_time, row['alarms'], row['alarm_times'], row["type"], row["people"], row["elisa"]


    # check the timestamp to resolve ambiguity
    df_filter["conv_alarm_times"] = df_filter.apply(lambda row: get_all_alarm_times(row, alarm), axis=1) 


    # for each row, check if time_hhmm is in alarm_times +- time_range

    # time might resolve ambiguity
    time_hhmm_datetime = datetime.datetime.strptime(time_hhmm, "%H:%M")
    time_range = datetime.timedelta(minutes=30)

    # filter df for rows where time is within time_range of time_hhmm
    df_filter = df_filter[df_filter['conv_alarm_times'].apply(lambda x: x is not None and any([time_hhmm_datetime - time_range <= time <= time_hhmm_datetime + time_range for time in x]))]

    if len(df_filter) == 1:
        row = df_filter.iloc[0]
        ret_time = time_hhmm if row['time'] is None else row['time']

        print("Needed to further refine")

        return _uid(row['date'], ret_time), row['description'], row['date'], ret_time, row['alarms'], row['alarm_times'], row["type"], row["people"], row["elisa"]

    if len(df_filter) == 0:
        print("DataFrame is empty after filtering.")
        not_matched.append((alarm, date_ddmmyy, time_hhmm))
        return None, None, None, None, None, None, None, None, None

    if len(df_filter) > 1:
        print("Cannot uniquely identify event")
        not_unique.append((alarm, date_ddmmyy, time_hhmm))
        return None, None, None, None, None, None, None, None, None


def edit_DSSEvent_object(db, 
                        uid, 
                        date = None,
                        time = None,
                        description = None,
                        event_type = None,
                        logs = [],

                        person = None,
                        elisa = None,

                        verbose=False):
    """ edit DSSEvent object in database
        if DSSEvent does not exist, create it, else update it
    
        input:
            db: database object
            uid: event_<unix_time>
            date: name of DSSLog
            time: time of DSSLog
            description: description of event
            event_type: type of event: {intervention, error, unknown}
        output:
            None
    """
    # check if DSSEvent already exists, else create it
    try : 
        obj = db.get_obj("DSSEvent",uid)
        if verbose: print(f"UPDATING DSSEvent {uid}")
    except: 
        obj = db.create_obj("DSSEvent", uid)
        if verbose: print(f"CREATING DSSEvent {uid}")

    # attributes
    attributes = [("date", date), 
                  ("time", time), 
                  ("description", description), 
                  ("event_type", event_type),

                  ("person", person),
                  ("elisa", elisa),
                  ]
                   
    for attr, value in attributes:

        print(f"Setting {attr} to {value}")

        if value is not None and obj[attr] != value:
            obj[attr]  = value
        

    # relations
    # append any logs
    relations  = [("logs", logs)] 
    for rel, values in relations:
        stored_values = obj[rel] 
        for val in values:
            if val not in stored_values: stored_values.append(val)
        obj[rel] = stored_values

    # also add reverse relation
    log["event"] = obj



        
def get_DSS_alarms_from_web_range(date_str_start, date_str_end):
    """ get all DSS alarms from DSS logbook web interface since a given date 
        input: 
            date as string of format yyyy-mm-dd
        output: 
            iterable object with DSSLog-like tuples (name, time, came)
    """
    
    # get json data since
    assert len(date_str_start.split("-")) == 3, "date_str must be of format yyyy-mm-dd"
    url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_range&since={date_str_start}&until={date_str_end}"
    data = urllib.request.urlopen(url).read()
    json_data = json.loads(data)

    cleaned_logs = [ d["name"].replace("\n","").replace("AL_AL_", "AL_").strip() + "++" + d["time"] for d in json_data["report"] ]
    
    # remove duplicates
    cleaned_logs = list(set(cleaned_logs))

    # sort by date
    cleaned_logs = sorted(cleaned_logs, key=lambda x: x.split("++")[1])

    return cleaned_logs

        
def get_sql_alarms_from_web_range(date_str_start, date_str_end):
    """ get all DSS alarms from DSS logbook web interface since a given date 
        input: 
            date as string of format yyyy-mm-dd
        output: 
            iterable object with DSSLog-like tuples (name, time, came)
    """
    
    # get json data since
    assert len(date_str_start.split("-")) == 3, "date_str must be of format yyyy-mm-dd"
    url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_range&since={date_str_start}&until={date_str_end}"
    data = urllib.request.urlopen(url).read()
    json_data = json.loads(data)

    # map came to alarm
    sql_logs_dict = { d["came"] : d["name"].replace("\n","").replace("AL_AL_", "AL_").strip() for d in json_data["report"] }

    return sql_logs_dict


def find_non_ascii(file_path):
    import csv
    non_ascii_characters = set()

    with open(file_path, 'r', encoding='utf-8') as file:
        reader = csv.reader(file)
        for row in reader:
            for field in row:
                non_ascii_characters.update(char for char in field if ord(char) > 127)

    return non_ascii_characters


if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    # # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    # read in events from csv file
    # event_file = "../../data/new_allDSS.csv" # 18/ - 23/04
    event_file = "../../data/summary_2304_fixeddate_fixedtype.csv" # 23/12 - 24/03

    non_ascii_chars = find_non_ascii(event_file)
    print("Non-ASCII characters found:", non_ascii_chars)

    # get *all* logs from db
    dsslogs = find_DSSLogs(db, verbose=args.verbose)


    df = read_csv_to_df(event_file)

    # start_date = "2018-01-02"
    # end_date   = "2023-04-22"

    start_date = "2023-04-23"
    end_date   = "2023-12-17"

    # start_date = "2023-12-18"
    # end_date   = "2024-03-01"

    datetime_start = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    datetime_end   = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    
    # get logs in time range
    sql_alarms_came = get_sql_alarms_from_web_range(start_date, end_date)
    print("-> sql_alarms_came: %i" % len(sql_alarms_came))
    # input("Press Enter to continue...")
   
    # find matching events
    not_matched = []
    not_created = []
    for i, log in enumerate(dsslogs):   

        log_date = log['date']
        log_date = datetime.datetime.strptime(log_date, "%Y-%m-%d")

        # skip if log outside start and end
        if log_date < datetime_start or log_date > datetime_end: continue


        if not "log_" in log.UID(): 
            print(log.UID())
            input("not 'log_' in log.UID() Press Enter to continue...")
            continue

        # skip one log wo alarm
        if log.UID() == "log_513455": continue
   
        # alarm , date_time = log["alarm"], log["time"]
        alarm , date, time = log["alarm"], log["date"], log["time"]

        print("@@@@@@@@@@ Log: ", alarm , date, time)


        if alarm is None: 
            print(log, "has no alarm" )
            print(log.UID())
            input("alarm is None. Press Enter to continue...")
            continue

        alarm_name = alarm.UID()

        # check in sql logs for "real" alarm name
        came = log.UID().replace("log_", "")


        # # convert time from hh:mm:ss to hh:mm
        time_hhmm = time[:5].strip()

        print("Converting time ", repr(time))

        # print('Matching ', repr(alarm), ",", date,",", time)

        
        # find event for alarm, date and time
        # need to convert date_yyyy-mm-dd to date_ddmmyy
        date_ddmmyy = date[8:] + "." + date[5:7] + "." + date[2:4]

        uid, description, date, time, alarms, alarm_times, event_type, person, elisa = find_event_for_alarm(df, alarm_name, date_ddmmyy, time_hhmm)
        print("Found event for ", uid, description, date_ddmmyy, time_hhmm, alarms, event_type)
        # input("Press Enter to continue...")


        if uid is None:
            print("No event found for alarm", alarm_name, date, time)
            not_matched.append([alarm, date, time])
            input("No event found for alarm. Press Enter to continue...")
            continue

        else:
            # create DSSEvent
            try:  edit_DSSEvent_object(db, uid, date, time, description, event_type.capitalize(), [log], str(person), elisa, verbose=args.verbose)
            except:
                print(f"{i} Could not create DSSEvent for", uid, description, date, time, event_type, [log], alarm_name)
                not_created.append(log.UID())
                input("Could not create DSSEvent Press Enter to continue...")

db.commit()



        
# how many alarms are grouped into events?
no = 0
for i, row in df.iterrows():
    no += len(convert_row_col_to_list(row["alarms"]))

print("no of alarms in events: ", no)




print("not_unique", len(not_unique))
print("not_matched", len(not_matched) , " / ", len(dsslogs))

print("___________")

not_matched_18 , not_matched_19, not_matched_20, not_matched_21, not_matched_22, not_matched_23 = [], [], [], [], [], []

not_matched_24 = []

for nm  in not_matched:

    # print(nm)

    alarm, date, time = nm

    # print(alarm, date, time)

    if alarm is None: continue
    if date is None: continue
    if time is None: continue

    if date.endswith("18"): not_matched_18.append((alarm, date, time))
    if date.endswith("19"): not_matched_19.append((alarm, date, time))
    if date.endswith("20"): not_matched_20.append((alarm, date, time))
    if date.endswith("21"): not_matched_21.append((alarm, date, time))
    if date.endswith("22"): not_matched_22.append((alarm, date, time))
    # if date.endswith("23"): 
        
    #     month = int(date.split("-")[1])
    #     if month >= 12 : 
    #         not_matched_24.append((alarm, date, time))
    #     else:
    #         not_matched_23.append((alarm, date, time))

    
    

print("not_matched_18", len(not_matched_18))
print("not_matched_19", len(not_matched_19))
print("not_matched_20", len(not_matched_20))
print("not_matched_21", len(not_matched_21))
print("not_matched_22", len(not_matched_22))
print("not_matched_23", len(not_matched_23))
print("not_matched_24", len(not_matched_24))

# # sort not_matched_22 by date and then by time
# sorted_matched_21 = sorted(not_matched_21, key=lambda x: (x[1], x[2]))

# for alarm, date, time in sorted_matched_21:
#     print(f"{alarm.ljust(50)} {date}  {time}")

# # sort not_matched_22 by date and then by time
sorted_matched_24 = sorted(not_matched_24, key=lambda x: (x[1], x[2]))

for alarm, date, time in sorted_matched_24:
    print(f"{alarm.ljust(50)} {date}  {time}")

print("DSS SQL : %i" % len(sql_alarms_came))
print("DSS missing: %i" % len(sorted_matched_24))
