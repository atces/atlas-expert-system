#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Script to generate the key for ciphering the accounts in the database
# The key is stored in a file by default outside the workspace
# Cipher the account in the database with the new key. Ask for the system pass if is not stored in the database.

import argparse
import config
import getpass
import os.path 
from cryptography.fernet import Fernet

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", help="File where the key is stored", required=False, default=os.path.expanduser("~")+"/private/akey")
parser.add_argument("-d", "--database", help="Database file", required=False, default="../../../data/sb.data.xml")
parser.add_argument("-a", "--account", help="Account to save the password", required=False, default="asp")
parser.add_argument("-r", "--reset", help="Reset password key and all passwords", action='store_true', required=False)
args = parser.parse_args()

if not args.reset and os.path.exists(args.file):
	with open(args.file,"r") as key_file:
		key=key_file.read()
else:
	key = Fernet.generate_key()
	with open(args.file,"w") as key_file:
		key_file.write(key.decode('utf-8'))

f = Fernet(key)
db=config.Configuration("oksconfig:"+args.database)
metainfo_obj=db.get_obj('MetaInfo','Singleton')
if args.reset:
	asp=getpass.getpass("Please enter the  password for the account atopes:")
	aip=getpass.getpass("Please enter the password for the account atlas_infrastructure_pub:")
	asp=getpass.getuser()+asp
	aip=getpass.getuser()+aip
	metainfo_obj["asp"]=f.encrypt(bytes(asp,'utf-8'))	
	metainfo_obj["atlas_infrastructure_pub"]=f.encrypt(bytes(aip,'utf-8'))	
elif metainfo_obj.get_string(args.account):
	asp=getpass.getpass("Please enter the "+getpass.getuser() +" password for the account %s:"%args.account)
	asp=getpass.getuser()+asp
	metainfo_obj[args.account]=f.encrypt(bytes(asp,'utf-8'))	
else:
	asp=f.decrypt(bytes(metainfo_obj.get_string("asp"),'utf-8'))
	aip=f.decrypt(bytes(metainfo_obj.get_string("atlas_infrastructure_pub"),'utf-8'))
	key = Fernet.generate_key()
	with open(args.file,"w") as key_file:
		key_file.write(key.decode('utf-8'))
	f = Fernet(key)
	metainfo_obj["asp"]=f.encrypt(asp)
	metainfo_obj["atlas_infrastructure_pub"]=f.encrypt(aip)
	print("Key refreshed")	
	
db.commit('')

