#!/usr/bin/env python

if __name__ == '__main__':
    import system
    import os
    import sys
    import config
    import argparse
    import re
    import minimax
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    
    import minimaxcentral


    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    #ecs = filter(lambda e: e.UID().startswith("TRT_E"), db.get_objs("SubDetector"))

    hp_rack = rack.RackHelper()
    hp_rack.setDb(db)
    hp_sys = system.SysHelper()
    hp_sys.setDb(db)
    
    
    sys_n=[15,16,17,60,21,20,26,27,28,29,30,31,35,36]
    '''
    for sys in sys_n:
        sysname="FCTIR_000%i_PLC" %sys
        rackname="FCTIR_000%i" %sys
        ok=True
        print sys
        try:
            o_sys=db.get_obj("System", sysname)
        except:
            print "%s does not exist" % sysname
            ok=False
        if ok == True:
            j_sys={
                "UID":sysname,
                "containedIn":rackname
            }            
            hp_sys.addObj(j_sys, False, True)
            j_rack={
                "UID":rackname,
                "contains":sysname,
                "subsystem":"cooling",
                "location":"USA15 CV room",
            }
            hp_rack.addObj(j_rack, False, True)
            pass
        pass
    
    controller="FCTIR_00015_PLC"
    controlled=["FCTIR_00026_PLC","FCTIR_00027_PLC","FCTIR_00028_PLC"]

    for c in controlled:
        o_ed={
            "UID":c,
            "controlledBy":controller
            }
        hp_sys.addObj(o_ed, False, True)
        o_er={
            "UID":controller,
            "controls":c
            }
        hp_sys.addObj(o_er, False, True)
        pass
    db.commit("")
    

    
    controller="FCTIR_00016_PLC"
    controlled=["FCTIR_00029_PLC","FCTIR_00030_PLC","FCTIR_00031_PLC"]

    for c in controlled:
        o_ed={
            "UID":c,
            "controlledBy":controller
            }
        hp_sys.addObj(o_ed, False, True)
        o_er={
            "UID":controller,
            "controls":c
            }
        hp_sys.addObj(o_er, False, True)
        pass
    db.commit("")
    '''

    controller="FCTIR_00017_PLC"
    controlled=["FCTIR_00021_PLC","FCTIR_00020_PLC"]

    for c in controlled:
        o_ed={
            "UID":c,
            "controlledBy":controller
            }
        hp_sys.addObj(o_ed, False, True)
        o_er={
            "UID":controller,
            "controls":c
            }
        hp_sys.addObj(o_er, False, True)
        pass
    db.commit("")
    



    
    


    '''
    for controlrack in controlracks:
        if controlrack is "Y.08-09.A2":
            o_rack={
            "UID":controlrack,
            "location":"USA15"
            }
            hp_rack.addObj(o_rack, False, True)
            o_minimax_central={
                "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                "poweredBy": controlrack,
                "containedIn":controlrack,
                "location":"USA15","subsystem":"Minimax",
            }
            hp_mc.addObj(o_minimax_central, False, True)
            o_rack={
            "UID":controlrack,
            "powers": "%s%s" % (controlrack, "_minimaxCentral"),
            "contains":"%s%s" % (controlrack, "_minimaxCentral"),
            "location":"USA15"
            }
            hp_rack.addObj(o_rack, False, True)
            for m in r_minimax:
                if "A" in m:
                    o_minimax={
                        "UID":m,
                        "containedIn":m.replace("_Minimax",""),
                        "deployedBy":o_minimax_central["UID"],
                        "subsystem":"Minimax",
                        "poweredBy":o_minimax_central["UID"],
                    }
                    hp_minimax.addObj(o_minimax, False, True)
                    o_minimax_central={
                        "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                        "deploys": m,
                        "powers":m,
                    }
                    hp_mc.addObj(o_minimax_central, False, True)
                    
                    o_containerrack={
                        "UID":m.replace("_Minimax",""),
                        "contains":m
                    }
                    print "Adding ",  m
                    hp_rack.addObj(o_containerrack, False, True)
                    pass
                pass
            pass
        else:
            o_rack={
            "UID":controlrack,
            "location":"US15"
            }
            hp_rack.addObj(o_rack, False, True)
            o_minimax_central={
                "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                "poweredBy": controlrack,
                "containedIn":controlrack,
                "location":"US15","subsystem":"Minimax",
            }
            hp_mc.addObj(o_minimax_central, False, True)
            o_rack={
            "UID":controlrack,
            "powers": "%s%s" % (controlrack, "_minimaxCentral"),
            "contains":"%s%s" % (controlrack, "_minimaxCentral"),
            "location":"US15"
            }
            hp_rack.addObj(o_rack, False, True)
            for m in r_minimax:
                if "S" in m or "X" in m:
                    o_minimax={
                        "UID":m,
                        "containedIn":m.replace("_Minimax",""),
                        "deployedBy":o_minimax_central["UID"],
                        "subsystem":"Minimax",
                        "poweredBy":o_minimax_central["UID"],
                    }
                    hp_minimax.addObj(o_minimax, False, True)
                    o_minimax_central={
                        "UID":"%s%s" % (controlrack, "_minimaxCentral"),
                        "deploys": m,
                        "powers":m,
                    }
                    hp_mc.addObj(o_minimax_central, False, True)
                    
                    o_containerrack={
                        "UID":m.replace("_Minimax",""),
                        "contains":m
                    }
                    print "Adding ",  m
                    hp_rack.addObj(o_containerrack, False, True)
                    pass
                pass
        
        pass
    db.commit("")
    
    
    
    
    
    
    
    racksUX15=["Y.53-23.X0","Y.59-23.X8","Y.27-23.X8", "Y.53-05.X8", "Y.33-05.X8"]

    for ec in ecs:
        ec=db.get_obj("SubDetector",ec)
        #powersuplies=rackobj["contains"]
        for rack in racks:
            print "python rack.py ../../data/sb.data.xml --uid %s --controls %s -e -y" % (rackobj.UID(), powersuplie.UID())
            print ""
            print "python powersupply.py ../../data/sb.data.xml --uid %s --controlledby %s  -e -y" % ( powersuplie.UID(), rackobj.UID())
            print ""
            print ""
            pass
        pass
        

    racks=["Y.35-04.X7_supply","Y.36-04.X7_return ",
           "Y.35-03.X1_supply","Y.36-03.X1_return",
           "Y.51-24.X7_supply","Y.52-24.X7_return",
           "Y.52-25.X1_supply","Y.53-25.X1_return",]
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec.UID())
        for rack in racks:
            
            obj_rack={
                "UID":str(rack),
                "gasTo":ec.UID(),
        <        }
            #print "Adding Rackgas: ",  obj_rack["UID"]
            hp_gas.addObj(obj_rack, False, True)
            
            obj_ec={
                "UID":str(ec.UID()),
                "requiresGasFrom":str(rack)
                }
            #print "Adding Subdetector: ",  obj_ec["UID"]
            hp_sub.addObj(obj_ec, False, True)
            pass
        pass
        
    racks=["Y.24-14.A1","Y.25-14.A1","Y.26-14.A1", "Y.27-14.A1"]
    for rack in racks:
        r=db.get_obj("Rack", rack)
        print r.UID()
        pss=r["contains"]
        for ps in pss:
            obj_rack={
                "UID":str(rack),
                "controls":ps.UID(),
            }
            hp_rack.addObj(obj_rack, False, True)
            obj_ps={
                "UID":str(ps.UID()),
                "controlledBy":str(rack),
            }
            hp_ps.addObj(obj_ps, False, True)
        pass
    
    db.commit("")
    '''
