#!/usr/bin/env python
#############################################
#
# Carlos.Solans@cern.ch
# July 2017
#############################################

import os
import sys
import config
import argparse

parser = argparse.ArgumentParser("check delayed actions")
parser.add_argument("database",help="database file")
parser.add_argument("-u","--uid",help="UID")
parser.add_argument("-y","--yeah",action="store_true",help="Not a dry run")
args=parser.parse_args()

print "Load database: %s" % args.database
db=config.Configuration("oksconfig:%s"%(args.database))

alarms = db.get_objs("Alarm")

for alarm in alarms:
    print "Process alarm: %s" % alarm.UID()
    delayedactions = alarm.get_objs("actions")
    for delayedaction in delayedactions:
        print " DelayedAction: %s" % delayedaction.UID()
        alarms2 = delayedaction.get_objs("alarms")
        alarms2obj = []
        alarmfound = False
        for alarm2 in alarms2:
            alarms2obj.append(db.get_obj("Alarm",alarm2.UID()))
            if alarm2.UID()==alarm.UID(): alarmfound=True
            pass
        if not alarmfound:
            alarms2obj.append(db.get_obj("Alarm",alarm.UID()))
            print " Append alarms: ", alarms2obj
            delayedactionobj = db.get_obj("DelayedAction",delayedaction.UID())
            #delayedactionobj["alarms"]=alarms2
            delayedactionobj.set_objs("alarms",alarms2obj)
            pass
        pass
    pass
    
if args.yeah:
    print "Commit changes"
    db.commit()
    pass

print ("Have a nice day")
