#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import rack

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_r = rack.RackHelper()
    hp_r.setDb(db)
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)

    ######################################################
    #addObj(self,data,remove=False,extend=False):
    ######################################################

    #from list
    #ls=["PVA1_TRT", "PVA2_TRT", "PVA3_TRT", "PVA4_TRT"]

    #from db
    #ls=db.get_obj("SubDetector",ec.UID())
    dbls = filter(lambda e: e.UID().startswith("TRT_"), db.get_objs("SubDetector"))
    
    #print ls
    group="TRT_Subdetector"
	l_racks=["Y.36-04.X7_return ","Y.36-03.X1_return","Y.51-24.X7_supply","Y.52-24.X7_return","Y.53-25.X1_return",]
    
    #ec=db.get_obj("SubDetector",ec.UID())
    
    #grouping from ls
    
    for u in l_racks:
        #group
        
        obj=db.get_obj("Rack",u)
        todeletes=obj["gasTo"]
        for todelete in todeletes:
            j_r={"UID":obj.UID(), "gasTo":todelete.UID()}
            hp_r.addObj(j_r, True, False)
            pass
        hp_g.addObj(j_g, False, True)
        
        #grouped
        #u_o=db.get_obj("PneumaticValve",u)
    	j_u={
            "UID":u,
            "groupedBy":group
        }
        hp_sd.addObj(j_u, False, True)
    '''


    #grouping from db
    for dbu in dbls:
        #group
        dbobj=db.get_obj("SubDetector",dbu.UID())
        u=dbobj.UID()
        j_g={"UID":group, "groupTo":u}
        hp_g.addObj(j_g, False, True)
        
        #grouped
        #u_o=db.get_obj("PneumaticValve",u)
        j_u={
            "UID":u,
            "groupedBy":group
        }
        hp_sd.addObj(j_u, False, True)
    '''
    db.commit("")
