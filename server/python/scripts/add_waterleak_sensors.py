#!/usr/bin/env python
#######################################################
# Add water sensors
# Ignacio.Asensi@cern.ch
# October 2021
#######################################################

import os
import sys
import json
import config
import argparse

def get_or_create(source,class_name,uid,verbose):
    obj=None
    try: obj=source.get_obj(class_name,uid)
    except: pass
    if not obj:
        if verbose: print("%s does not exist... we should create it: %s" % (class_name,uid))
        db.create_obj(class_name, uid)
        obj=source.get_obj(class_name,uid)
        pass
    return obj

def safe_append(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    found=False
    for obj in objs: 
        if obj.UID()==what.UID(): found=True
        pass
    if not found: objs.append(what)
    source[rel]=objs
    pass

def safe_remove(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    new_objs=[]
    for obj in objs: 
        if obj.UID()!=what.UID(): new_objs.append(obj)
        pass
    source[rel]=new_objs
    pass

def safe_remove_single(source,rel,what):
    source[rel]=None
    pass
    
parser=argparse.ArgumentParser()
parser.add_argument("-d","--database",default="sb.data.xml",help="database file")
parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
args=parser.parse_args()

print ("Load database: %s" % args.database)
db=config.Configuration("oksconfig:%s"%(args.database))
data=[
["DI_INF_WaterLeak_USA15L2_RackRow19_21_SideC","WaterSensor_USA15L2_RackRow19_21_SideC"],
["DI_INF_WaterLeak_USA15L2_RackRow19_21_SideA","WaterSensor_USA15L2_RackRow19_21_SideA"],
["DI_INF_WaterLeak_USA15L2_RackRow14_16_SideC","WaterSensor_USA15L2_RackRow14_16_SideC"],
["DI_INF_WaterLeak_USA15L2_RackRow14_16_SideA","WaterSensor_USA15L2_RackRow14_16_SideA"],
["DI_INF_WaterLeak_USA15L2_RackRow11","WaterSensor_USA15L2_RackRow11"],
["DI_INF_WaterLeak_USA15L1_RackRow2_5_SideC","WaterSensor_USA15L1_RackRow2_5_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow19_SideA","WaterSensor_USA15L1_RackRow19_SideA"],
["DI_INF_WaterLeak_USA15L1_RackRow19_21_SideC","WaterSensor_USA15L1_RackRow19_21_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow14_16_SideC","WaterSensor_USA15L1_RackRow14_16_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow11_SideC","WaterSensor_USA15L1_RackRow11_SideC"],
["DI_INF_WaterLeak_TRT_Y5323X0","WaterSensor_TRT_Y5323X0"],
["DI_INF_WaterLeak_TRT_Y3305X8orY5305X8","WaterSensor_TRT_Y3305X8orY5305X8"],
["DI_INF_WaterLeak_TRT_Y2723X8orY5923X8","WaterSensor_TRT_Y2723X8orY5923X8"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F47_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F25_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F8_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F47_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_TIL_CoolingStation_UX15","WaterSensor_TIL_CoolingStation_UX15"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F17_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F17_Y0815A1"],
["DI_INF_WaterLeak_SDX1L2_RackRow3","WaterSensor_SDX1L2_RackRow3"],
["DI_INF_WaterLeak_SDX1L2_RackRow2","WaterSensor_SDX1L2_RackRow2"],
["DI_INF_WaterLeak_SDX1L1_RackRow3","WaterSensor_SDX1L1_RackRow3"],
["DI_INF_WaterLeak_SDX1L1_RackRow1","WaterSensor_SDX1L1_RackRow1"],
["DI_INF_WaterLeak_MUN_CoolingStationSideC_UX15","WaterSensor_MUN_CoolingStationSideC_UX15"],
["DI_INF_WaterLeak_MUN_CSC_Y5823X0","WaterSensor_MUN_CSC_Y5823X0"],
["DI_INF_WaterLeak_MUN_CSC_Y2916A1","WaterSensor_MUN_CSC_Y2916A1"],
["DI_INF_WaterLeak_LAR_CoolingStation_UX15","WaterSensor_LAR_CoolingStation_UX15"],
["DI_INF_WaterLeak_IBL_CO2Cooling_PlantB_Y0814A1","WaterSensor_IBL_CO2Cooling_PlantB_Y0814A1"],
["DI_INF_WaterLeak_IBL_CO2Cooling_PlantA_Y0814A1","WaterSensor_IBL_CO2Cooling_PlantA_Y0814A1"],
["DI_INF_WaterLeak_SDX1L2_RackRow1","WaterSensor_SDX1L2_RackRow1"],
["DI_INF_WaterLeak_Failure_UX15","WaterSensor_Failure_UX15"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow2_5","WaterSensor_Failure_USA15L2_RackRow2_5"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow19_21_SideA","WaterSensor_Failure_USA15L2_RackRow19_21_SideA"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow14_16_SideC","WaterSensor_Failure_USA15L2_RackRow14_16_SideC"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow11","WaterSensor_Failure_USA15L2_RackRow11"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow2_5_SideC","WaterSensor_Failure_USA15L1_RackRow2_5_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow2_5_SideA","WaterSensor_Failure_USA15L1_RackRow2_5_SideA"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow19_SideA","WaterSensor_Failure_USA15L1_RackRow19_SideA"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow19_21_SideC","WaterSensor_Failure_USA15L1_RackRow19_21_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow14_16_SideC","WaterSensor_Failure_USA15L1_RackRow14_16_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow11_SideC","WaterSensor_Failure_USA15L1_RackRow11_SideC"],
["DI_INF_WaterLeak_Failure_TrenchesUX15","WaterSensor_Failure_TrenchesUX15"],
["DI_INF_WaterLeak_Failure_TRT_Y5923X8","WaterSensor_Failure_TRT_Y5923X8"],
["DI_INF_WaterLeak_Failure_TRT_Y5323X0","WaterSensor_Failure_TRT_Y5323X0"],
["DI_INF_WaterLeak_Failure_TRT_Y3305X8orY5305X8","WaterSensor_Failure_TRT_Y3305X8orY5305X8"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F8_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F47_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F57_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F57_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F40_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F40_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F17_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F17_Y0815A1"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow3","WaterSensor_Failure_SDX1L2_RackRow3"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow2","WaterSensor_Failure_SDX1L2_RackRow2"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow1","WaterSensor_Failure_SDX1L2_RackRow1"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow3","WaterSensor_Failure_SDX1L1_RackRow3"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow1","WaterSensor_Failure_SDX1L1_RackRow1"],
["DI_INF_WaterLeak_Failure_IBL_CO2Cooling_PlantB_Y0814A1","WaterSensor_Failure_IBL_CO2Cooling_PlantB_Y0814A1"],
["DI_INF_WaterLeak_Failure_IBL_CO2Cooling_PlantA_Y0814A1","WaterSensor_Failure_IBL_CO2Cooling_PlantA_Y0814A1"],
["DI_INF_WaterLeak_Failure_CVRoomGallery_Y0814A","WaterSensor_Failure_CVRoomGallery_Y0814A"],
["DI_INF_WaterLeak_Cables_CoolingStation_UX15","WaterSensor_Cables_CoolingStation_UX15"],
["DI_INF_WaterLeak_CVRoomGallery_Y0814A","WaterSensor_CVRoomGallery_Y0814A"],
["DI_INF_WaterLeak_ArgonTrench_UX15","WaterSensor_ArgonTrench_UX15"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F57_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F57_Y0815A1"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow2","WaterSensor_Failure_SDX1L1_RackRow2"],
["DI_INF_WaterLeak_Failure_MUN_CSC_Y2924X1","WaterSensor_Failure_MUN_CSC_Y2924X1"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow19_21_SideC","WaterSensor_Failure_USA15L2_RackRow19_21_SideC"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow14_16_SideA","WaterSensor_Failure_USA15L2_RackRow14_16_SideA"],
["DI_INF_WaterLeak_MUN_CSC_CableFailure_Y2916A1","WaterSensor_MUN_CSC_CableFailure_Y2916A1"],
["DI_INF_WaterLeak_MUN_CoolingStationSideA_UX15","WaterSensor_MUN_CoolingStationSideA_UX15"],
["DI_INF_WaterLeak_Failure_MUN_CSC_Y5823X0","WaterSensor_Failure_MUN_CSC_Y5823X0"],
["DI_INF_WaterLeak_SDX1L1_RackRow2","WaterSensor_SDX1L1_RackRow2"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F8_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F40_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F40_Y0815A1"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow11_14_16_SideA","WaterSensor_Failure_USA15L1_RackRow11_14_16_SideA"],
["DI_INF_WaterLeak_USA15L1_RackRow11_14_16_SideA","WaterSensor_USA15L1_RackRow11_14_16_SideA"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F25_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_TRT_CoolingStation_UX15","WaterSensor_TRT_CoolingStation_UX15"],
["DI_INF_WaterLeak_MAG_CoolingStation_UX15","WaterSensor_MAG_CoolingStation_UX15"],
["DI_INF_WaterLeak_USA15L2_RackRow2_5","WaterSensor_USA15L2_RackRow2_5"],
["DI_INF_WaterLeak_Sector13Trench_UX15","WaterSensor_Sector13Trench_UX15"],
["DI_INF_WaterLeak_USA15L1_RackRow2_5_SideA","WaterSensor_USA15L1_RackRow2_5_SideA"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F47_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F8_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F25_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F25_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_MUN_CSC_Y2924X1","WaterSensor_MUN_CSC_Y2924X1"]]

for s in data:
	di_UID=str(s[0])
	ws_UID=str(s[1])
	location=""
	description="Water leak sensing cable"
	if "USA15"in ws_UID: location="USA15"
	if "SDX1"in ws_UID: location="SDX1"
	if "UX15"in ws_UID: location="UX15"
	if "Failure" in di_UID and "Failure"in di_UID: location="US15"
	if "failure" in di_UID: description="Water leak sensor failure. Module or cable failure."
	ws_obj=get_or_create(db,"WaterSensor",ws_UID,args.verbose)
	#di_obj=get_or_create(db,"DigitalInput",di_UID,args.verbose)
	#ws_obj.set_string("description", description)
	ws_obj.set_string("subsystem", "Safety")
	ws_obj.set_string("location", location)
	#safe_append(ws_obj,"digitalOutput",di_obj)
	#safe_append(di_obj,"signalSource",ws_obj)
	pass
db.commit()
pass

########################################################
'''
fr1=open(args.a2a)
a2a=json.load(fr1)
for dssAlarm in a2a:
    print("Processing alarm: %s" % dssAlarm)
    atcesAlarm=get_or_create(db,"Alarm",dssAlarm,args.verbose)
    atcesDelayedActions=atcesAlarm.get_objs("actions")
    atcesDelayedActionsUIDs=[]
    for a in atcesDelayedActions: atcesDelayedActionsUIDs.append(a.UID())
    dssDelayedActions=[]
    if args.verbose: print("-- Check missing DSS actions in ATCES")
    for dssAction in a2a[dssAlarm]:
        if dssAction=="DSU": continue
        dssDelayedAction = "%s_%i" % (dssAction,a2a[dssAlarm][dssAction])
        if not dssDelayedAction in atcesDelayedActionsUIDs:
            print("-- We should add DelayedAction: %s to Alarm: %s" % (dssDelayedAction,dssAlarm))
            atcesDelayedAction = get_or_create(db,"DelayedAction",dssDelayedAction,args.verbose)
            atcesDelayedAction.set_u32("delay",a2a[dssAlarm][dssAction])
            atcesAction = get_or_create(db,"Action",dssAction,args.verbose)
            atcesDelayedAction["action"]=atcesAction
            safe_append(atcesAction,"delayedactions",atcesDelayedAction)
            safe_append(atcesAlarm,"actions",atcesDelayedAction)
            safe_append(atcesDelayedAction,"startedByAlarms",atcesAlarm)
            pass
        dssDelayedActions.append(dssDelayedAction)
        pass
    if args.verbose: print("-- Check that ATCES does not have any extra actions")
    for atcesDelayedAction in atcesDelayedActions:
        if not atcesDelayedAction.UID() in dssDelayedActions:
            print("-- We should remove DelayedAction: %s from Alarm: %s" % (atcesDelayedAction.UID(),dssAlarm))
            safe_remove(atcesAlarm,"actions",atcesDelayedAction)
            pass
        pass
    pass

if args.input:
    fr2=open(args.input)
    atcesInputs=db.get_objs("DigitalInput")
    dssInputs=json.load(fr2)
    atcesInputsUIDs=[]
    for a in atcesInputs: 
        if "DI_" not in a.UID(): continue
        atcesInputsUIDs.append(a.UID())
        pass
    for atcesInput in atcesInputs:
        print("Processing input: %s" % atcesInput.UID())
        if not atcesInput.UID() in dssInputs:
            print("-- We should remove DigitalInput: %s" % (atcesInput.UID()))
            pass
        pass
    pass

if args.action:
    fr2=open(args.action)
    atcesActions=db.get_objs("Action")
    dssActions=json.load(fr2)
    for atcesAction in atcesActions:
        print("Processing Action: %s" % atcesAction.UID())
        if not atcesAction.UID() in dssActions:
            print("-- We should remove Action: %s" % (atcesAction.UID()))
            print("-- First remove DSU from Action and Action from DSU")
            atcesDsu=atcesAction["DSU"]
            atcesAction["DSU"]=None
            if atcesDsu: safe_remove(atcesDsu,"actions",atcesAction)
            print("-- Then remove DelayedActions from Action and Action from DelayedActions")
            atcesDelayedActions=atcesAction["delayedactions"]
            atcesAction["delayedactions"]=[]
            for atcesDelayedAction in atcesDelayedActions:
                safe_remove_single(atcesDelayedAction,"action",atcesAction)
                pass
            print("-- Then remove Interlocks from Action and Action from Interlocks")
            atcesInterlocks=atcesAction["interlocks"]
            atcesAction["interlocks"]=[]
            for atcesInterlockedBy in atcesInterlocks:
                safe_remove(atcesInterlockedBy,"interlockedBy",atcesAction)
                pass
            print("-- Finally destroy object")
            db.destroy_obj(atcesAction)
            pass
        else:
            dssAction=dssActions[atcesAction.UID()]
            print("-- Modify the parameters of the Action")
            dsu_dss2atces={"DSU1":"DSU_1","DSU2":"DSU_2","DSU3":"DSU_3","DSU4":"DSU_4","DSU5":"DSU_5","DSU6":"DSU_6","DSU7":"DSU_7",}
            atcesDSU=get_or_create(db,"DSU",dsu_dss2atces[dssAction["DSU"]],args.verbose)
            atcesAction["DSU"]=atcesDSU
            atcesAction["crate"]=dssAction["Crate"]
            atcesAction["module"]=int(dssAction["Module"])
            atcesAction["slot"]=int(dssAction["Slot"])
            atcesAction["channel"]=int(dssAction["Channel"])
            print("-- Attach the action to the DSU")
            safe_append(atcesDSU,"actions",atcesAction)
            pass
        pass
    pass

    
if args.yes: db.commit("import_dss_json")
















#!/usr/bin/env python
# @author Ignacio

import config
import logging
import argparse
import loadhelpers
from graphhelper import GraphHelper
import watersensor 
import digitalinput



if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Create a graph based on the database and detect cycles on it.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	logger.debug("Loading database file...")	
	db=config.Configuration("oksconfig:"+args.database)
	
	hpws = watersensor.WaterSensorHelper()
	hpws.setDb(db)
	hpdi = digitalinput.DigitalInputHelper()
	hpdi.setDb(db)
	data=[
["DI_INF_WaterLeak_USA15L2_RackRow19_21_SideC","WaterSensor_USA15L2_RackRow19_21_SideC"],
["DI_INF_WaterLeak_USA15L2_RackRow19_21_SideA","WaterSensor_USA15L2_RackRow19_21_SideA"],
["DI_INF_WaterLeak_USA15L2_RackRow14_16_SideC","WaterSensor_USA15L2_RackRow14_16_SideC"],
["DI_INF_WaterLeak_USA15L2_RackRow14_16_SideA","WaterSensor_USA15L2_RackRow14_16_SideA"],
["DI_INF_WaterLeak_USA15L2_RackRow11","WaterSensor_USA15L2_RackRow11"],
["DI_INF_WaterLeak_USA15L1_RackRow2_5_SideC","WaterSensor_USA15L1_RackRow2_5_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow19_SideA","WaterSensor_USA15L1_RackRow19_SideA"],
["DI_INF_WaterLeak_USA15L1_RackRow19_21_SideC","WaterSensor_USA15L1_RackRow19_21_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow14_16_SideC","WaterSensor_USA15L1_RackRow14_16_SideC"],
["DI_INF_WaterLeak_USA15L1_RackRow11_SideC","WaterSensor_USA15L1_RackRow11_SideC"],
["DI_INF_WaterLeak_TRT_Y5323X0","WaterSensor_TRT_Y5323X0"],
["DI_INF_WaterLeak_TRT_Y3305X8orY5305X8","WaterSensor_TRT_Y3305X8orY5305X8"],
["DI_INF_WaterLeak_TRT_Y2723X8orY5923X8","WaterSensor_TRT_Y2723X8orY5923X8"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F47_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F25_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F8_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F47_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_TIL_CoolingStation_UX15","WaterSensor_TIL_CoolingStation_UX15"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F17_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F17_Y0815A1"],
["DI_INF_WaterLeak_SDX1L2_RackRow3","WaterSensor_SDX1L2_RackRow3"],
["DI_INF_WaterLeak_SDX1L2_RackRow2","WaterSensor_SDX1L2_RackRow2"],
["DI_INF_WaterLeak_SDX1L1_RackRow3","WaterSensor_SDX1L1_RackRow3"],
["DI_INF_WaterLeak_SDX1L1_RackRow1","WaterSensor_SDX1L1_RackRow1"],
["DI_INF_WaterLeak_MUN_CoolingStationSideC_UX15","WaterSensor_MUN_CoolingStationSideC_UX15"],
["DI_INF_WaterLeak_MUN_CSC_Y5823X0","WaterSensor_MUN_CSC_Y5823X0"],
["DI_INF_WaterLeak_MUN_CSC_Y2916A1","WaterSensor_MUN_CSC_Y2916A1"],
["DI_INF_WaterLeak_LAR_CoolingStation_UX15","WaterSensor_LAR_CoolingStation_UX15"],
["DI_INF_WaterLeak_IBL_CO2Cooling_PlantB_Y0814A1","WaterSensor_IBL_CO2Cooling_PlantB_Y0814A1"],
["DI_INF_WaterLeak_IBL_CO2Cooling_PlantA_Y0814A1","WaterSensor_IBL_CO2Cooling_PlantA_Y0814A1"],
["DI_INF_WaterLeak_SDX1L2_RackRow1","WaterSensor_SDX1L2_RackRow1"],
["DI_INF_WaterLeak_Failure_UX15","WaterSensor_Failure_UX15"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow2_5","WaterSensor_Failure_USA15L2_RackRow2_5"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow19_21_SideA","WaterSensor_Failure_USA15L2_RackRow19_21_SideA"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow14_16_SideC","WaterSensor_Failure_USA15L2_RackRow14_16_SideC"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow11","WaterSensor_Failure_USA15L2_RackRow11"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow2_5_SideC","WaterSensor_Failure_USA15L1_RackRow2_5_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow2_5_SideA","WaterSensor_Failure_USA15L1_RackRow2_5_SideA"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow19_SideA","WaterSensor_Failure_USA15L1_RackRow19_SideA"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow19_21_SideC","WaterSensor_Failure_USA15L1_RackRow19_21_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow14_16_SideC","WaterSensor_Failure_USA15L1_RackRow14_16_SideC"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow11_SideC","WaterSensor_Failure_USA15L1_RackRow11_SideC"],
["DI_INF_WaterLeak_Failure_TrenchesUX15","WaterSensor_Failure_TrenchesUX15"],
["DI_INF_WaterLeak_Failure_TRT_Y5923X8","WaterSensor_Failure_TRT_Y5923X8"],
["DI_INF_WaterLeak_Failure_TRT_Y5323X0","WaterSensor_Failure_TRT_Y5323X0"],
["DI_INF_WaterLeak_Failure_TRT_Y3305X8orY5305X8","WaterSensor_Failure_TRT_Y3305X8orY5305X8"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F8_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F47_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F57_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F57_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F40_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F40_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_BarrelA_CSgarage_F17_Y0815A1","WaterSensor_Failure_TIL_BarrelA_CSgarage_F17_Y0815A1"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow3","WaterSensor_Failure_SDX1L2_RackRow3"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow2","WaterSensor_Failure_SDX1L2_RackRow2"],
["DI_INF_WaterLeak_Failure_SDX1L2_RackRow1","WaterSensor_Failure_SDX1L2_RackRow1"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow3","WaterSensor_Failure_SDX1L1_RackRow3"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow1","WaterSensor_Failure_SDX1L1_RackRow1"],
["DI_INF_WaterLeak_Failure_IBL_CO2Cooling_PlantB_Y0814A1","WaterSensor_Failure_IBL_CO2Cooling_PlantB_Y0814A1"],
["DI_INF_WaterLeak_Failure_IBL_CO2Cooling_PlantA_Y0814A1","WaterSensor_Failure_IBL_CO2Cooling_PlantA_Y0814A1"],
["DI_INF_WaterLeak_Failure_CVRoomGallery_Y0814A","WaterSensor_Failure_CVRoomGallery_Y0814A"],
["DI_INF_WaterLeak_Cables_CoolingStation_UX15","WaterSensor_Cables_CoolingStation_UX15"],
["DI_INF_WaterLeak_CVRoomGallery_Y0814A","WaterSensor_CVRoomGallery_Y0814A"],
["DI_INF_WaterLeak_ArgonTrench_UX15","WaterSensor_ArgonTrench_UX15"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F57_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F57_Y0815A1"],
["DI_INF_WaterLeak_Failure_SDX1L1_RackRow2","WaterSensor_Failure_SDX1L1_RackRow2"],
["DI_INF_WaterLeak_Failure_MUN_CSC_Y2924X1","WaterSensor_Failure_MUN_CSC_Y2924X1"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow19_21_SideC","WaterSensor_Failure_USA15L2_RackRow19_21_SideC"],
["DI_INF_WaterLeak_Failure_USA15L2_RackRow14_16_SideA","WaterSensor_Failure_USA15L2_RackRow14_16_SideA"],
["DI_INF_WaterLeak_MUN_CSC_CableFailure_Y2916A1","WaterSensor_MUN_CSC_CableFailure_Y2916A1"],
["DI_INF_WaterLeak_MUN_CoolingStationSideA_UX15","WaterSensor_MUN_CoolingStationSideA_UX15"],
["DI_INF_WaterLeak_Failure_MUN_CSC_Y5823X0","WaterSensor_Failure_MUN_CSC_Y5823X0"],
["DI_INF_WaterLeak_SDX1L1_RackRow2","WaterSensor_SDX1L1_RackRow2"],
["DI_INF_WaterLeak_TIL_ECC_CSgarage_F8_Y0815A1","WaterSensor_TIL_ECC_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_TIL_BarrelA_CSgarage_F40_Y0815A1","WaterSensor_TIL_BarrelA_CSgarage_F40_Y0815A1"],
["DI_INF_WaterLeak_Failure_USA15L1_RackRow11_14_16_SideA","WaterSensor_Failure_USA15L1_RackRow11_14_16_SideA"],
["DI_INF_WaterLeak_USA15L1_RackRow11_14_16_SideA","WaterSensor_USA15L1_RackRow11_14_16_SideA"],
["DI_INF_WaterLeak_TIL_ECA_CSgarage_F25_Y0815A1","WaterSensor_TIL_ECA_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_TRT_CoolingStation_UX15","WaterSensor_TRT_CoolingStation_UX15"],
["DI_INF_WaterLeak_MAG_CoolingStation_UX15","WaterSensor_MAG_CoolingStation_UX15"],
["DI_INF_WaterLeak_USA15L2_RackRow2_5","WaterSensor_USA15L2_RackRow2_5"],
["DI_INF_WaterLeak_Sector13Trench_UX15","WaterSensor_Sector13Trench_UX15"],
["DI_INF_WaterLeak_USA15L1_RackRow2_5_SideA","WaterSensor_USA15L1_RackRow2_5_SideA"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F47_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F47_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F8_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F8_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECC_CSgarage_F25_Y0815A1","WaterSensor_Failure_TIL_ECC_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_Failure_TIL_ECA_CSgarage_F25_Y0815A1","WaterSensor_Failure_TIL_ECA_CSgarage_F25_Y0815A1"],
["DI_INF_WaterLeak_MUN_CSC_Y2924X1","WaterSensor_MUN_CSC_Y2924X1"]]



	for s in data:
		di=str(s[0])
		ws=str(s[1])
		location=""
		description="Water leak sensing cable"
		if "USA15"in ws: location="USA15"
		if "SDX1"in ws: location="SDX1"
		if "UX15"in ws: location="UX15"
		if "Failure" in di and "Failure"in di: location="US15"
		if "failure" in di: description="Water leak sensor failure. Module or cable failure."
		ws_obj={
			"UID":ws,
			#"location":str(s[1]),
			#"sensor_location": str(s[2]),
			#"detects":str(s[3]),
			"description":description,
			#"AL3":"SGGAZ-00158"
			"digitalInput":di

		}
		print(ws_obj)
		hpws.addObj(ws_obj, False, True)
		di_obj={
			"UID":di,
			"signalSource":ws
		}
		print(di_obj)
		hpdi.addObj(di_obj, False, True)
		pass
	#db.commit()
	pass
	
'''
