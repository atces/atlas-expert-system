#!/usr/bin/env python
# every delayed action 

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers
    helpers=loadhelpers.LoadHelpers()
    classes=helpers.classes
    helps=loadhelpers.LoadHelpers().getHelpers()

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hpdi = digitalinput.DigitalInputHelper()
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper()
    hpal.setDb(db)
    hpac = action.ActionHelper()
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper()
    hpda.setDb(db)
    # f = open("MissingDigitalInputs.txt", "w")

    counter=0
    more=0
    less=0
    newda={}
    problems={}


    #clear alarms
    for al in alarms_db:
        objs_di=al["digitalInput"]
        for di in  objs_di:
            di_obj={
                "UID":di.UID(),
                "triggers":al.UID()
            }
            #hpdi.addObj(di_obj, True, False)
            al_obj={
                "UID":al.UID(),
                "digitalInput":di.UID()
            }
            #hpal.addObj(al_obj, True, False)
            pass
        pass
    
    with open("AlarmListFull.txt", 'r') as file:
        data="".join(line for line in file)
        alarms_text=data.split("===========================================================")
        #print len(alarms_text)
        for block in alarms_text:
            al_name=""
            di_names=[]
            ac_names=[]
            o_names=[]
            
            for line in block.split("\n"):
                #print line
                if line.startswith("ALARM  AL_"):
                    al_name= line.split("  ")[1]
                    pass
                if " DI_" in line:
                    di_names.append(line.strip().split(" ")[0])
                    pass
                if " O_" in line:
                    o_names.append([line.strip().split(" ")[0],line.strip().split("(delay ")[1].split(")")[0]])
                    #print line.strip().split(" ")[0],"--",line.strip().split(" ")[1]
                    pass

            #print o_names
            #print di_names

            if al_name!= "":
                
                db_alarm=db.get_obj("Alarm", al_name)
                db_alarm_ser= helps["Alarm"].serialize(db_alarm)
                #print "DB:", db_alarm_ser["actions"]
                #make sure objects are in db
                obj_al={
                    "UID":al_name,
                    "switch":"on",
                    "state":"on"
                }
                #hpal.addObj(obj_al,False,True)
                for di_name in di_names:
                    obj_d={
                        "UID":di_name
                    }
                    #hpdi.addObj(obj_d,False, True)
                    pass
                #se relations
                for di_name in di_names:
                    obj_d={
                        "UID":di_name,
                        "triggers":al_name
                    }
                    #hpdi.addObj(obj_d,False, True)
                    obj_al={
		                "UID":al_name,
                        "digitalInput":di_name
	                   }
                    #hpal.addObj(obj_al,False,True)
                    pass
                #check that 
                file_actions={}
                db_actions={}
                present_in_file_not_in_db={}
                for o_name in o_names:
                    #if o_name[0]+"_"+o_name[1] not in db_alarm_ser["actions"]:
                    file_actions[o_name[0]]=o_name[1]
                    if o_name[0]+"_"+o_name[1] not in db_alarm_ser["actions"]:
                        #action is there, but with wrong delay
                        #print "-----------------ERROR1------Action from file not in db --------------------->",o_name
                        present_in_file_not_in_db[o_name[0]]=o_name[1]
                        pass
                    pass
                for action in db_alarm_ser["actions"]:
                    #print action
                    if action!="":
                        db_actions[action]=action
                        pass
                    pass
                #print "file_actions", file_actions
                #print "db_actions", db_actions
                if len(db_actions)>len(file_actions):
                    more+=1
                    pass
                if len(db_actions)<len(file_actions):
                    less+=1
                    pass

                #find alarms in db that are missing delayed actions that are in file
                if True:
                    counter+=1
                    print "#Alarm from file: ", al_name
                    print "-----------------DIfference-----------------"
                    print len(file_actions)," file_actions:", file_actions
                    print len(db_actions), " db_actions:", db_actions
                    print len(present_in_file_not_in_db), " present_in_file_not_in_db:", present_in_file_not_in_db
                    for fileactionname, delay in present_in_file_not_in_db.items():
                        daname=fileactionname+"_"+delay
                        print "daname", daname
                        try:
                            db_delayedAction=db.get_obj("DelayedAction", daname)
                            pass
                        except:
                            
                            print "DelayedAction [%s] missing for alarm [%s] : " %  (str(daname), al_name)
                            print "First, check if corresponding action exist"


                            try:
                                dbaction=db.get_obj("Action", fileactionname)
                                print "It exists. "
                                pass
                            except:
                                problems[fileactionname]=al_name
                                print fileactionname,"DOES NOT EXIST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                                break;
                            #hpal.addObj(obj_al,False,True)
                            newda[daname]=""
                            '''
                            #new delayedAction
                            obj_da={
                                "UID":daname,
                                "action":fileactionname,
                                "delay":delay, 
                                "alarms":al_name
                                #action is missing!!
                            }
                            print "Creating: ", daname
                            #hpda.addObj(obj_da,False,True)
                            print "Created"
                            #link action to delayedaction
                            obj_ac={
                                "UID":fileactionname,
                                "delayedAction":daname
                            }
                            print "Creating: ", fileactionname
                            #hpac.addObj(obj_ac,False,True)
                            print "Created"
                            #link delayedaction to alarm
                            obj_al={
                                "UID":al_name,
                                "actions":daname
                            }
                            print "Creating: ", al_name
                            #hpal.addObj(obj_al,False,True)
                            print "Created"
                            print "CREATED delayedaction [%s] missing for alarm [%s] : " %  (str(daname), al_name)
                            '''
                            pass
                        #print db_delayedAction.UID()
                        pass
                    pass
                
                pass
            pass
        #print alarms_text[1]
        pass
    #db.commit("")
    print counter
    print "more", more
    print "less", less
    print "Actions in file but not in db: ", problems
    print "DelayedActions in file that are not in db", newda
    print "New actions to be created:", problems
    print "present_in_file_not_in_db", present_in_file_not_in_db
    print "present_in_file_not_in_db", len(present_in_file_not_in_db)

    pass
