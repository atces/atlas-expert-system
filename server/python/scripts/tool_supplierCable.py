#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re
    import group
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import rack
    import pneumaticcontroller
    import vessel

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_r = rack.RackHelper()
    hp_r.setDb(db)
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)
    hp_pc=pneumaticcontroller.PneumaticControllerHelper()
    hp_pc.setDb(db)
    hp_ve=vessel.VesselHelper()
    hp_ve.setDb(db)
    #supply, return, pipe(coolingLoop), destination(vessel), location, description
    uids=[["PVA1","PVA6", "L1", "M2_5","Side A / USA15", "Platform 0"],
     ["PVA1","PVA6", "L1", "M1_9","Side A / USA15", "Platform 1"],
     ["PVA1","PVA6", "L1", "M3_5","Side A / USA15", "Platform 1"],
     ["PVA1","PVA6", "L1", "M5_5","Side A / USA15", "Platform 1"],
     ["PVA1","PVA6", "L1", "M6_2","Side A / USA15", "Platform 1"],
     ["PVA1","PVA6", "L1", "M7_4","Side A / USA15", "Platform 1"],
     ["PVA1","PVA6", "L1", "M1_8","Side A / USA15", "Platform 3"],
     ["PVA2","PVA7", "L2", "M1_7", "Side A / USA15", "Platform 4"],
     ["PVA2","PVA7", "L2", "M2_3a", "Side A / USA15", "Platform 4"],
     ["PVA2","PVA7", "L2", "M2_4a", "Side A / USA15", "Platform 4"],
     ["PVA2","PVA7", "L2", "M5_4", "Side A / USA15", "Platform 4"],
     ["PVA2","PVA7", "L2", "M3_4", "Side A / USA15", "Platform 4"],
     ["PVA2","PVA7", "L2", "M1_6", "Side A / USA15", "Platform 8"],
     ["PVA2","PVA7", "L2", "M2_3a", "Side A / USA15", "Platform 8"],
     ["PVA2","PVA7", "L2", "M2_4a", "Side A / USA15", "Platform 8"],
     ["PVA2","PVA7", "L2", "M5_4", "Side A / USA15", "Platform 8"],
     ["PVA2","PVA7", "L2", "M3_4", "Side A / USA15", "Platform 8"],
     ["PVA3","PVA8", "L3", "M3_6","Side C / US15", "Platform 0"],
     ["PVA3","PVA8", "L3", "M5_6","Side C / US15", "Platform 0"],
     ["PVA3","PVA8", "L3", "M1_1","Side C / US15", "Platform 1"],
     ["PVA3","PVA8", "L3", "M7_1","Side C / US15", "Platform 1"],
     ["PVA4","PVA9", "L4", "M2_1a","Side C / US15", "Platform 4"],
     ["PVA4","PVA9", "L4", "M2_2a","Side C / US15", "Platform 4"],
     ["PVA4","PVA9", "L4", "M3_1","Side C / US15", "Platform 4"],
     ["PVA4","PVA9", "L4", "M4_1","Side C / US15", "Platform 4"],
     ["PVA4","PVA9", "L4", "M5_1","Side C / US15", "Platform 4"],
     ["PVA4","PVA9", "L4", "M6_1","Side C / US15", "Platform 4"],
     ["PVA5","PVA8", "L5", "M1_2","Side C / US15", "Platform 5"],
     ["PVA5","PVA8", "L5", "M1_3a","Side C / US15", "Platform 5"],
     ["PVA5","PVA8", "L5", "M1_3b","Side C / US15", "Platform 5"],
     ["PVA5","PVA8", "L5", "M1_4","Side C / US15", "Platform 5"],
     ["PVA5","PVA8", "L5", "M1_5","Side C / US15", "Platform 7"],
     ["PVA5","PVA8", "L5", "M2_1b","Side C / US15", "Platform 7"],
     ["PVA5","PVA8", "L5", "M2_2b","Side C / US15", "Platform 7"],
     ["PVA5","PVA8", "L5", "M3_2","Side C / US15", "Platform 7"],
     ["PVA5","PVA8", "L5", "M5_2","Side C / US15", "Platform 7"],
     ["PVA5","PVA8", "L5", "M7_2","Side C / US15", "Platform 7"]]
     #supply, return, line(coolingLoop), destination(vessel), location, description
    '''
    for x in range(1,26):
        print "[\"PL-LAr_PCVA%i\",\"PL-LAr_PVA%i\", \"\"]" % (x,x)
        pass
    '''
    groupVA="PVAs_Cables"
    groupManifolds="Manifolds_Cables"
    groupLines="Cables_Lines_Group"
    coolingstation="FCUM_00005"
    pneumaticcontroller="FCTIR_00020_PC_Cables"
    #create pneumaticcontroller
    o_pc={
        "UID":pneumaticcontroller,
        }
    hp_pc.addObj(o_pc, False, True)
    #create group
    o_groupVA={"UID":groupVA}
    hp_g.addObj(o_groupVA, False, True)
    o_groupVE={"UID":groupManifolds}
    hp_g.addObj(o_groupVE, False, True)
    o_groupLines={"UID":groupLines}
    hp_g.addObj(o_groupLines, False, True)
    for u in uids:
        n_pva1="%s_Cables" %u[0]#class pva
        n_pva2="%s_Cables" %u[1]#class pva
        n_line="Cooling_Line%s" % u[2]#class cooling loop
        n_manifold="Manifold_%s" % u[3]#class vessel
        n_location=u[4]#manifold location
        n_description=u[5]#manifold description

        #create pvas
        o_pva1={
            "UID":n_pva1, "groupedBy":groupVA
        }
        hp_pv.addObj(o_pva1, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":n_pva2, "groupedBy":groupVA
        }
        hp_pv.addObj(o_pva2, False, True)

        #create manifolds
        o_ve1={
        "UID":n_manifold, "groupedBy":groupManifolds, "location":n_location, "Description":"Manifold %s" % n_description
        }
        hp_ve.addObj(o_ve1, False, True)


        #Coolingloop controlledbyPVA both
        o_cl={
            "UID":n_line,"controlledByAirFrom":n_pva1, "coolingTo":n_manifold, "requiresCoolingFrom":coolingstation, "groupedBy":groupLines
        }
        hp_cl.addObj(o_cl, False, True)
        #add it one
        o_cl={
            "UID":n_line,"controlledByAirFrom":n_pva2, "coolingTo":n_manifold,  "requiresCoolingFrom":coolingstation, "groupedBy":groupLines
                }
        hp_cl.addObj(o_cl, False, True)


        # manifolds cooling from coolingloop
        o_ve1={
            "UID":n_manifold, "requiresCoolingFrom":n_line
        }
        hp_ve.addObj(o_ve1, False, True)

        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva1={
            "UID":n_pva1,"controlsByAirTo":n_line, "controlledByAirFrom":pneumaticcontroller
        }
        hp_pv.addObj(o_pva1, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":n_pva2,"controlsByAirTo":n_line, "controlledByAirFrom":pneumaticcontroller
        }
        hp_pv.addObj(o_pva2, False, True)


        #pneumactic cont.
        o_pc={
            "UID":pneumaticcontroller,
            "controlsByAirTo":n_pva1,
        }
        hp_pc.addObj(o_pc, False, True)
        #pneumactic cont.
        o_pc={
            "UID":pneumaticcontroller,
                "controlsByAirTo":n_pva2,
                }
        hp_pc.addObj(o_pc, False, True)
        #create line (coolingloop)


        #grouping
        o_g={"UID":groupVA, "groupTo":n_pva1}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":groupVA, "groupTo":n_pva2}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":groupLines, "groupTo":n_line}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":groupManifolds, "groupTo":n_manifold}
        hp_g.addObj(o_g, False, True)
        pass
    db.commit("")
