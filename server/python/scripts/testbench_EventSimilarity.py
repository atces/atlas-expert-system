#!/usr/bin/env python
#######################################################
# add relation DSSLog to alarm  
# Florian.Haslbeck@cern.ch
# November 2023
#######################################################

def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj



if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))


    # simple case: one alarm
    name = "AL_FG_SGX1_TechnicalFaultSGGAZ00158"

    
    def find_events_es_alarm(es_alarm, verbose=False):
        # then find the related DSSLogs
        print(es_alarm["logs"])
    
        return [dsslog["event"] for dsslog in es_alarm["logs"]]

    
    # first find the expert system alarm
    es_alarm  = get_obj(db, "Alarm", name, verbose=args.verbose)
    dssevents = find_events_es_alarm(es_alarm, verbose=args.verbose)


    print(dssevents)

    # more complicated case: several alarms
    # solve this recursively

    
    # alarms = ["alarm1", "alarm2"]

    # def find_events_of_two_alarms(dssevents_es_alarm1, es_alarm2, verbose=False):
    #     # # find the events of alarm1
    #     # dssevents = find_events_es_alarm(es_alarm1, verbose=verbose)

    #     new_dssevents = []
    #     for event in dssevents:
    #         for log in event["logs"]:
    #             if es_alarm2 in log["alarm"]:
    #                 new_dssevents.append(event)
    #     return new_dssevents

    
    
    # def find_events_of_alarms(es_alarms, verbose=False):
        
    #     dssevents = find_events_es_alarm(es_alarms[0], verbose=verbose)
    #     # per iteration, only keep events matchin A and B
    #     # so (A and B) and C = A and B and C ... 
    #     for es_alarm in es_alarms[1:]:
    #         if len(dssevents) == 0: return []
    #         dssevents = find_events_of_two_alarms(dssevents, es_alarm, verbose=verbose)

    #     return dssevents

        
         
       
