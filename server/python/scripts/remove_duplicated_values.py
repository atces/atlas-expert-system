#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# OKS allow duplicated entries in the relationships. This tool remove all the duplications from an object.

import config
import logging
import argparse

def remove_duplicates(obj):
	schema=db.__schema__.data[obj.class_name()]
	for relation in schema['relation']:
		objects_in_relations={obj_in_relation.UID():obj_in_relation.class_name() for obj_in_relation in obj[relation]}
		obj[relation]=[]
		obj[relation]=[db.get_obj(clazz,obj_in_relation) for obj_in_relation,clazz in objects_in_relations.items()]


if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Create a graph based on the database and detect cycles on it.')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid', required=True, help='UID of the object to update')
	parser.add_argument('-c','--clazz', required=True, help='Class of the object')
	args = parser.parse_args()
	 
	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	obj=db.get_obj(args.clazz,args.uid)

	remove_duplicates(obj)	
	db.commit()
		
