#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db=config.Configuration("oksconfig:%s"%(args.database))

    alarms=db.get_objs("Alarm")
    nodsu=[]
    for alarm in alarms:
        print "Process alarm: %s" % alarm.UID()
        alarm["state"]="on"
        alarm["switch"]="on"
        pass
        
    if args.yes: db.commit("")
    sys.exit(0)
