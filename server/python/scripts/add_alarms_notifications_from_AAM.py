#!/usr/bin/env python
#######################################################
# Add water sensors
# Ignacio.Asensi@cern.ch
# October 2021
#######################################################

import os
import sys
import json
import config
import argparse

def get_or_create(source,class_name,uid,verbose):
    obj=None
    try: obj=source.get_obj(class_name,uid)
    except: pass
    if not obj:
        if verbose: print("%s does not exist... we should create it: %s" % (class_name,uid))
        db.create_obj(class_name, uid)
        obj=source.get_obj(class_name,uid)
        pass
    return obj

def safe_append(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    found=False
    for obj in objs: 
        if obj.UID()==what.UID(): found=True
        pass
    if not found: objs.append(what)
    source[rel]=objs
    pass

def safe_remove(source,rel,what):
    objs=source[rel]
    if objs is None: objs=[]
    new_objs=[]
    for obj in objs: 
        if obj.UID()!=what.UID(): new_objs.append(obj)
        pass
    source[rel]=new_objs
    pass

def safe_remove_single(source,rel,what):
    source[rel]=None
    pass
    
parser=argparse.ArgumentParser()
parser.add_argument("-d","--database",default="sb.data.xml",help="database file")
parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
args=parser.parse_args()

#print ("Load database: %s" % args.database)
db=config.Configuration("oksconfig:%s"%(args.database))
counter=0
max_c=10000
with open("AlarmList_20220209.txt", 'r') as file:
    contents = file.read()
    for alarm in contents.split('==========================================================='):
        if counter>=max_c:break
        if "ALARM " not in alarm: continue
        alarm_name=alarm.splitlines()[1].split(" ")[2]
        print(alarm_name)
        counter+=1
        #mail
        if "Send email to::" in alarm:
            start="Send email to::\n"
            end="\nEmail Subject:"
            s_mail=alarm[alarm.find(start)+len(start):alarm.rfind(end)]
        else:
            s_mail=""
        #SMS
        if "Send SMS " in alarm:
            start="to:\n"
            end="\n\n"
            s_SMS=alarm[alarm.find(start)+len(start):alarm.rfind(end)].split("\n",1)[1]
        else:
            s_SMS=""
        print(s_mail)
        print(s_SMS)
        obj_alarm=get_or_create(db,"Alarm",alarm_name,args.verbose)
        obj_alarm.set_string("mailTo", s_mail)
        obj_alarm.set_string("SMSto", s_SMS)
        pass
    db.commit()
