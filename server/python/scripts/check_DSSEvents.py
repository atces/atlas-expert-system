#!/usr/bin/env python
# check the already filled DSSEvents for the DSSLogs
# Florian.Haslbeck@cern.ch
# February 2024

def find_DSSEvents(db, verbose=False):
    """ find all DSSEvent config_objs in database
        input:
            db: database object
        output:
            list of DSSEvent objects
    """
    # get all DSSLogs
    config_objs = db.get_objs("DSSEvent")
    if verbose: print(f"Found {len(config_objs)} DSSEvents")
    return config_objs


def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj


def convert_to_swedish(date):
    """ convert date to swedish format """
    date = date.split(".")
    return f"20{date[2]}-{date[1]}-{date[0]}"


if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()


    # get DSSLogs from database
    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    dssevents_config_objs = find_DSSEvents(db, verbose=args.verbose) # this config_objs

    no_error, no_intervention, no_unknown, no_none = 0, 0, 0, 0

    for dssevent in dssevents_config_objs:
        if not dssevent.UID().startswith("event_"): continue
        # print(dssevent, dssevent['date'], dssevent['time'])

        # fix date and time
        if '.' in dssevent['date']:
            print("ERROR: date has a dot")
            swedish = convert_to_swedish(dssevent['date'])
            dssevent.set_string("date", swedish)

        if dssevent["time"].count(":") == 1:
            print("ERROR: time has only one colon")
            time = dssevent["time"] + ":00"
            dssevent.set_string("time", time)

        # capitalise event type
        if dssevent["event_type"] in ["intervention", "unknown", "error"]:
            dssevent.set_string("event_type", dssevent["event_type"].capitalize())

        if dssevent["event_type"] == "Error":          no_error += 1
        elif dssevent["event_type"] == "Intervention": no_intervention += 1
        elif dssevent["event_type"] == "Unknown":      no_unknown += 1
        else: no_none += 1

        # fix elisa
        if "[" in dssevent["elisa"] or "]" in dssevent["elisa"] or "'" in dssevent["elisa"] or '"' in dssevent["elisa"] or dssevent["elisa"].strip() == "":
            elisa = dssevent["elisa"].replace("[", "").replace("]", "").replace("'","").replace('"','').strip()
            if elisa == "": elisa = "No Elisa log."
            print("Fixing elisa: %s -> %s" % (dssevent["elisa"], elisa))
            dssevent["elisa"] = elisa

         # fix person
        if "[" in dssevent["person"] or "]" in dssevent["person"] or "'" in dssevent["person"] or '"' in dssevent["person"] or dssevent["person"].strip() == "":
            person = dssevent["person"].replace("[", "").replace("]", "").replace("'","").replace('"','').strip()
            if person == "": person = "Unknown"
            print("Fixing person: %s -> %s" % (dssevent["person"], person))
            dssevent["person"] = person

        # fix short description
        if dssevent["short_description"].strip() == "" or dssevent["short_description"] == "None.":
            dssevent["short_description"] = "None"

    db.commit()

    print(f"Errors: {no_error}, Interventions: {no_intervention}, Unknown: {no_unknown} None {no_none}")
        
        
        

