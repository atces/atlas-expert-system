#!/usr/bin/env python
#######################################################
# add DSSLogs from DSS SQL
# Florian.Haslbeck@cern.ch
# June 2023
#######################################################

import urllib.request
import json
import re

def get_DSS_report_since(days):

    """ get DSS report since *days* , contains alarms and actions"""
   
    # get json data since
    url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_report&days={days}"
    response = urllib.request.urlopen(url)
    html = response.read()
    rs = json.loads(html)

    report = [ f"{row['came']} {row['time']} {row['name'].strip()}" for row in rs["report"] ]

    return report


def _get_obj(db, _type, _name, verbose=False):
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj
    


def edit_DSSLogAction_object(db, 
                        uid, 
                        name = None, 
                        date = None,
                        time = None,
                        verbose=False):
    """ edit DSSLogAction object in database
        if DSSLog does not exist, create it, else update it
    
        input:
            db:    database object
            uid:   unique id of DSSLog -> came
            name:  name of DSSLog -> name
            time:  time of DSSLog -> time
            # event: DSSEvent uid 
        output:
            None
    """

    ret = None

    # check if DSSLog already exists, else create it
    try : 
        obj = db.get_obj("DSSLogAction",uid)
        if verbose: print(f"UPDATING DSSLogAction {uid}")
    except: 
        obj = db.create_obj("DSSLogAction", uid)
        if verbose: print(f"CREATING DSSLogAction {uid}")

   
    # check if DSSLog needs to be updated
    
    # attributes
    attributes = [("name", name), ("date", date), ("time", time)]
    for attr, value in attributes:
        if value is not None and obj[attr] != value:  obj[attr]  = value

    # add the action object
    action_obj = _get_obj(db, "Action", name)
    if action_obj is not None:
        obj["action"] = action_obj
        print("edit_DSSLogAction_object: Found action!", action_obj)

        # TODO also do the reverse and add the logaction to the action object
        action_obj = _get_obj(db, "Action", name)
        dsslogactions = action_obj["dsslogaction"] if action_obj else []
        
        if obj not in dsslogactions:
            dsslogactions.append(obj)
            print("added reverse relation " , action_obj, " -> ", obj)


        action_obj["dsslogaction"] = dsslogactions
        print('action_obj["dsslogaction"] ... ', action_obj["dsslogaction"])

    return obj
    
def strip_last_integer(input_string):
    """ Strip the last integer from a string _i"""

    # Regular expression to find the last integer preceded by an underscore
    pattern = r'(.*)_(\d+)$'
    
    # Search for the pattern in the input string
    match = re.search(pattern, input_string)
    
    # If a match is found, return the string without the last integer
    if match: return match.group(1)
    
    # If no match is found, return the original string
    return input_string




if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    # parser.add_argument("date", help="date string of format yyyy-mm-dd")
    parser.add_argument("-d","--days", help="days", default = 20, type=int)
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    parser.add_argument("-s","--sandbox",help="sandbox mode, not committing",action='store_true')
    args=parser.parse_args()

    # # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    # get DSSLogs from web
    report = get_DSS_report_since(args.days)

    print("report", report)

    # filter report to only contain actions
    actions = [ r for r in report if r.split(" ")[3].startswith("O_") ]
    alarms  = [ r for r in report if r.split(" ")[3].startswith("AL_") ]

    print("actions:", actions)
    print("alarms :", alarms)

    # for every alarm get the actions
    # make a dict with came as key, and a dict with alarm, date, time, actions as values
    alarms_actions = {}
    print("Iterating over alarms")

    alarms_not_in_db = []
    for alarm in alarms:

        came, date, time, name = alarm.split(" ")
        print("alarm :", alarm, came, date, time, name)

        alarm_obj = _get_obj(db, "Alarm", name)
        if alarm_obj is None: 
            print("Alarm not in database", name)
            alarms_not_in_db.append(name)
            continue
        
        # get the actions of the alarm
        _alarm_actions = alarm_obj["actions"] if alarm_obj else []
        if len(_alarm_actions) > 0:
            _alarm_actions = [ strip_last_integer(a.UID()) for a in _alarm_actions ]
            print("... -> ", _alarm_actions)
        else:
            print("No actions for alarm", name)

        alarms_actions[int(came)] = {
            'alarm' : name,
            'date' : date,
            'time' : time,
            'actions' : _alarm_actions,
        }
            

    print()
    # print(alarms_actions)
    for k, v in alarms_actions.items():
        print(k, v)
 
    input("continue?")


    # next, create DSSLogActions objects
    # iterate over actions
    for i, action in enumerate(actions):

        print("\n\n>>>>>>Trying to match this action :  ", action)
        came, date, time, name = action.split(" ")
        uid = f"logaction_{came}"

        logaction_obj = edit_DSSLogAction_object(db, uid, name, date, time, verbose = args.verbose)

        # find the alarm that triggered the action, normally it should come before the action
        print("Matching logaction to log: ", came, date, time, name)
        
        matched = False
        # see if came - i is in alarms_actions
        for i in range(0, 10):
            if matched: continue
            
            print("Checking", int(came) - i)

            if int(came) - i in alarms_actions:
                alarm = alarms_actions[int(came) - i]
                print("Found alarm", int(came) - i,  alarm)
                these_actions = alarm['actions']

                # print("These actions", these_actions)
                # print("Name", name , name in these_actions)

                # check is name == these_actions_x
                if name in these_actions:
                    
                    # match to the log object
                    log_obj = db.get_obj("DSSLog", "log_%i"%(int(came) - i))

                    print("Log obj", log_obj)

                    if log_obj is not None:
                        logaction_obj["dsslog"] = log_obj

                        print("Matched logaction to log", log_obj)

                        alarms_actions[int(came) - i]['actions'].remove(name)

                        # add the logaction to the log object
                        dsslogactions_belonging_to_log = log_obj["dsslogaction"]

                        print("dsslogactions_belonging_to_log", dsslogactions_belonging_to_log)
                        # input("continue? %s" % (logaction_obj not in dsslogactions_belonging_to_log))

                        if logaction_obj not in dsslogactions_belonging_to_log:
                            dsslogactions_belonging_to_log.append(logaction_obj)
                            print("added reverse relation ", log_obj, " -> ", logaction_obj)
                        
                        log_obj["dsslogaction"] = dsslogactions_belonging_to_log

                        print(".. dsslogactions_belonging_to_log", dsslogactions_belonging_to_log)

                        matched = True
                        break
                else:
                    print("Name not in these actions")
        if not matched : print("Try the other way around")
        
        # see if came - i is in alarms_actions
        for i in range(1, 10):
            if matched: continue
            print("Checking", int(came) + i)
            if int(came) + i in alarms_actions:
                alarm = alarms_actions[int(came) + i]
                print("Found alarm", alarm)
                these_actions = alarm['actions']

                # check is name == these_actions_x
                if name in these_actions:
                    

                    # match to the log object
                    log_obj = db.get_obj("DSSLog", f"log_{came}")
                    if log_obj is not None:
                        logaction_obj["dsslog"] = log_obj

                        print("Matched logaction to log", log_obj)
                        alarms_actions[int(came) + i]['actions'].remove(name)


                        # add the logaction to the log object
                        dsslogactions_belonging_to_log = log_obj["dsslogaction"]
                        print("dsslogactions_belonging_to_log", dsslogactions_belonging_to_log)
                        input("continue?")

                        if logaction_obj not in dsslogactions_belonging_to_log:
                            dsslogactions_belonging_to_log.append(logaction_obj)
                            print("added reverse relation ", log_obj, " -> ", logaction_obj)

                        log_obj["dsslogaction"] = dsslogactions_belonging_to_log



                        matched = True
                        break

        if not matched: print("Could not match logaction to log")
        
    if not args.sandbox: db.commit()



    print("Not in db:")
    for e in list(set(alarms_not_in_db)):
        print(repr(e))
    # print("Not in db:")
    # for e in list(set(not_in_es)):
    #     print(repr(e))

    
    

    
    
    
