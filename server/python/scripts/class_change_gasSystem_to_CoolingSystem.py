#!/usr/bin/env python
# ignacio.asensi@cern.ch
# Aug 2018
import os
import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import fta
import messenger
import loadhelpers
import ProbTree
import traceback
import helper
import argparse
import copy
import relations_checker
import water
import subdetector



#import classes from loadhelpers
helpers=loadhelpers.LoadHelpers()
classes=helpers.classes
helps=loadhelpers.LoadHelpers().getHelpers()

     
parser = argparse.ArgumentParser()
parser.add_argument("database",help="database file")


args=parser.parse_args()
print "Load database: %s" % args.database
db = config.Configuration("oksconfig:%s"%(args.database))
verbose=True
errors=""

#uids=["EMT103_15","EMT204_1UX","EMT205_1UX","EMT206_1H","EMT207_1UX","EMT208_1X","EMT208_15A","EMT209_15A","EMT210_15A","EMT301_1E","EMT302_15A","EMT303_1U","EMT304_1R","EMT305_1DX","EMT403_1H","EMT404_15A","EMT405_15A","EMT406_1F","EMT407_1X","EMT408_17","EMT410_1DX"]


#print helps
for h in helps:
    helps[h].setDb(db)
    pass


supply_racks=["Y.35-04.X7_supply", "Y.35-03.X1_supply", "Y.51-24.X7_supply", "Y.52-25.X1_supply"]
'''


#add new objects
for supply_rack_name in supply_racks:
    print "-------------"
    print supply_rack_name
    print "-------------"
    db_supply_rack_GS=db.get_obj("WaterSystem", supply_rack_name)
    WS_tempname="%s_temp" % str(supply_rack_name)
    obj_rack_WS={
        "UID":WS_tempname,
        }
    helps["CoolingSystem"].addObj(obj_rack_WS, False, True)
    s_supply_rack_GS= helps["WaterSystem"].serialize(db_supply_rack_GS)
    print "-------------2"
    print s_supply_rack_GS
    for gt in s_supply_rack_GS["waterTo"]:
        print gt
        if gt!="":
            obj_rack_WS={
                "UID":WS_tempname,
                "coolingTo":gt
            }
            helps["CoolingSystem"].addObj(obj_rack_WS, False, True)
            print "-------------3"
            obj_rack_SUB={
                "UID":gt,
                "requiresCoolingFrom":WS_tempname
            }
            helps["SubDetector"].addObj(obj_rack_SUB, False, True)
            pass
        pass
    pass

db.commit("")
'''

'''

#remove old objects
for supply_rack_name in supply_racks:
    print "-------------"
    print supply_rack_name
    print "-------------"
    db_supply_rack_GS=db.get_obj("WaterSystem", supply_rack_name)
    s_supply_rack_GS= helps["WaterSystem"].serialize(db_supply_rack_GS)
    print "-------------2"
    print s_supply_rack_GS
    for gt in s_supply_rack_GS["waterTo"]:
        print gt
        if gt!="":
            obj_rack_WS={
                "UID":supply_rack_name,
                "waterTo":gt
            }
            helps["WaterSystem"].removeObj(obj_rack_WS)
            print "-------------3"
            obj_rack_SUB={
                "UID":gt,
                "requiresWaterFrom":supply_rack_name
            }
            helps["SubDetector"].removeObj(obj_rack_SUB)
            pass
        pass
    pass

db.commit("")





supply_racks=["Y.36-04.X7_return", "Y.36-03.X1_return", "Y.52-24.X7_return", "Y.53-25.X1_return"]
#add new objects
for supply_rack_name in supply_racks:
    print "-------------"
    print supply_rack_name
    print "-------------"
    db_supply_rack_GS=db.get_obj("WaterSystem", supply_rack_name)
    WS_tempname="%s_temp" % str(supply_rack_name)
    obj_rack_WS={
        "UID":WS_tempname,
        }
    helps["CoolingSystem"].addObj(obj_rack_WS, False, True)
    s_supply_rack_GS= helps["WaterSystem"].serialize(db_supply_rack_GS)
    print "-------------2"
    print s_supply_rack_GS
    for gt in s_supply_rack_GS["waterTo"]:
        print gt
        if gt!="":
            obj_rack_WS={
                "UID":WS_tempname,
                "coolingTo":gt
            }
            helps["CoolingSystem"].addObj(obj_rack_WS, False, True)
            print "-------------3"
            obj_rack_SUB={
                "UID":gt,
                "requiresCoolingFrom":WS_tempname
            }
            helps["SubDetector"].addObj(obj_rack_SUB, False, True)
            pass
        pass
    pass

db.commit("")



supply_racks=["Y.35-04.X7_supply", "Y.35-03.X1_supply", "Y.51-24.X7_supply", "Y.52-25.X1_supply"]
#add new objects
for supply_rack_name in supply_racks:
    print "-------------"
    print supply_rack_name
    print "-------------"
    db_supply_rack_GS=db.get_obj("WaterSystem", supply_rack_name)
    s_supply_rack_GS= helps["WaterSystem"].serialize(db_supply_rack_GS)
    print "-------------2"
    print s_supply_rack_GS
    for gt in s_supply_rack_GS["waterTo"]:
        print gt
        if gt!="":
            obj_rack_WS={
                "UID":supply_rack_name,
                "waterTo":gt
            }
            helps["WaterSystem"].removeObj(obj_rack_WS)
            print "-------------3"
            obj_rack_SUB={
                "UID":gt,
                "requiresWaterFrom":supply_rack_name
            }
            helps["SubDetector"].removeObj(obj_rack_SUB)
            pass
        pass
    pass

db.commit("")





'''


supply_racks=["Y.35-04.X7_supply", "Y.35-03.X1_supply", "Y.51-24.X7_supply", "Y.52-25.X1_supply"]
return_racks=["Y.36-04.X7_return", "Y.36-03.X1_return", "Y.52-24.X7_return", "Y.53-25.X1_return"]

for r in return_racks:
    rtmp="%s_temp" % r
    container= r.replace("_return","")
    obj_rack_WS={
        "UID":rtmp,
        "description":"TRT Cooling return",
        "location":"UX15",
        "containedIn":container
    }
    helps["CoolingSystem"].addObj(obj_rack_WS, False, True)
    obj_rack={
        "UID":container,
        "contains":rtmp
    }
    helps["Rack"].addObj(obj_rack, False, True)
    pass
db.commit("")
'''
def getClass(uid):
    for c in classes:
        o=""
        try:
            o=db.get_obj(c[0], uid)
            return o.class_name()
        except:
            pass
        pass
    return 0

        


def removeCounterpart(original, counterpart):
    #get object
    counter_ob=""
    removed=False
    classname=getClass(counterpart)
    counter_ob=db.get_obj(classname, counterpart)
    #if verbose == True: print "counter_ob:", type(counter_ob)
    if type(counter_ob)!="str":
        counter_ser= helps[classname].serialize(counter_ob)
        #check attributes
        for at in counter_ser:
            if isinstance(counter_ser[at], list): 
                for a in counter_ser[at]:
                    if original == a:
                        if verbose == True: print "   %s --  %s -->  %s" % (counter_ob.UID(), at, a)
                        rel_delete={
                            "UID": counter_ob.UID(), 
                            at:a}
                        if verbose == True: print "Deleting relation: ", rel_delete
                        #remove relation and return it to replicate later
                        helps[classname].addObj(rel_delete, True, False)
                        return {at:counter_ob.UID()}
                        pass
                    pass
                pass
            pass
        pass
    return {"Error":counterpart}


    


def executeChange(u):
    db_o=""
    #get object from db
    try:
        db_o=db.get_obj(classname_from, u)
        pass
    except:
        print "Object %s of class %s not found in database" % (u, classname_from)
        failed.append(u)
        return False
    #check consistency
    multi=relations_checker.getMultiAttributesEXT(class_from, db)
    problems=relations_checker.checkObject(db_o, multi)
    if problems!=0:
        print "Object %s of class %s has broken relations:" % (u, classname_from)
        print problems
        return False
    if verbose == True: print "Changing ", db_o.UID()
    o= helps[classname_from].serialize(db_o)
    o["UID"]=u
    old_o=copy.copy(o)
    print "-----------------------------------------------------------------------------------------------"
    #Remove relations to other objects
    removedRels=[]
    for at in o:
        if at == "UID":continue
        if isinstance(o[at], list):
            arr=[]
            for a in o[at]:
                if at == "otherIds": continue
                if len(a)> 0: 
                    #remove counterpart relation
                    if verbose == True: print "From %s -- %s --> %s" % (u, at, a)
                    removedRels.append(removeCounterpart(u, a))
                    pass
                pass
            pass
        pass
    
    #remove object from db
    removing=helps[classname_from].destroyObj(classname_from, u)
    if removing==False:
        print "Object %s could not be deleted!!" % u
        failed.append(u)
        return False
    #Save object in db with new class
    if verbose== True:
        print "Saving object as class ", classname_to
        print old_o
        pass
    helps[classname_to].addObj(old_o)

    #restoring relations between this object and others
    if verbose == True: 
        print ""
        print "Replicating removed rels"

    print removedRels
    for rel in removedRels:
        #details
        obj_name=rel[rel.keys()[0]]
        relation=rel.keys()[0]
        if relation=="Error":
            print "ERROR IN RELATION", rel
            continue
        #object
        replicate_o={
            "UID":obj_name,
            relation:u
            }
        print "--"
        print replicate_o
        #save
        class_of_o=getClass(obj_name)
        helps[class_of_o].addObj(replicate_o, False, True)
        pass
    to_help.dbCommit()
    return True

def checkCompatibility():
    if len(nocopy)>0:
        print ""
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        print "Following attributes will not be copied:"
        for n in nocopy:
            print n
            pass
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        print ""
        ans = raw_input("Continue? [y/n]")
        pass
    if ans=="y":
        print "Continue..."
        return True
    else:
        print "Stop..."
        return False
    pass




#Magic starts here
if verbose == True:
    print to_attrs
    print ""
    print from_attrs
    pass

if verbose==True:
    print "-----------------------"
    print "To class: ", class_to
    print to_attrs
    print "From class: ", class_from
    print from_attrs
    print "-----------------------"
    pass

#Incompatibilities warnings
nocopy=[]
for at in from_attrs:
    if at not in to_attrs:
        nocopy.append(at)
        #print "Attribute %s not in target class %s. This relation will not be copied" % (at, classname_to)
        pass
    pass



if nocheck==True:
    cont=True
    pass
else:
    cont=checkCompatibility()
    pass

if cont== True:
    failed=[]
    if isinstance(uids, list):
        for u in uids:
            executeChange(u)
            pass
        pass
    else:
        executeChange(uids)
        pass
    print ""
    print ""
    if len(failed)>0:
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        print "FOLLOWING OBJECTS COULD NOT BE CHANGED"
        print failed
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        print ""
        pass
    else:
        print "All OK"
    pass
print "END"
'''
