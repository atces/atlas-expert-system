#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import json

    import group
    import subdetector
    import powersupply
    import rack
    import crate
    import gas
    import heater
    import interlock

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    """
    with open("sct_racks.json") as sct_racks_json:
        sct_racks = json.load(sct_racks_json)
    hp1 = powersupply.PowerSupplyHelper()
    hp1.setDb(db)
    hp2 = rack.RackHelper()
    hp2.setDb(db)
    for rack in sct_racks:
        for ps in sct_racks[rack]:
            obj1 = {
                "UID": "SCT_PS_%s_%02d" % (str(rack.replace(".","").replace("-","")), ps),
                "containedIn": str(rack),
                "requiresPowerFrom": str(rack)
            }
            hp1.addObj(obj1,False,True)

            obj2 = {
                "UID": str(rack),
                "contains": "SCT_PS_%s_%02d" % (str(rack.replace(".","").replace("-","")), ps),
                "powers": "SCT_PS_%s_%02d" % (str(rack.replace(".","").replace("-","")), ps)
            }
            hp2.addObj(obj2,False,True)
    db.commit("")
    """
    
    with open("sct_modules.json") as sct_modules_json:
        sct_modules = json.load(sct_modules_json)

    hp1 = group.GroupHelper()
    hp1.setDb(db)
    hp2 = subdetector.SubDetectorHelper()
    hp2.setDb(db)
    hp3 = powersupply.PowerSupplyHelper()
    hp3.setDb(db)
    ps_list = filter(lambda e: e.UID().startswith("SCT_PS_"), db.get_objs("PowerSupply"))
    
    """
    for group in sct_modules:
        for module in sct_modules[group]:
            for ps in sct_modules[group][module]["Crates"]:
                ps_name = next((p.UID() for p in ps_list if p.UID().endswith("%02d" % (ps))))
                obj1 = {
                    "UID": "%s_%s" % (str(group), str(module)),
                    "loop": sct_modules[group][module]["Loop"],
                    "requiresPowerFrom": ps_name
                }
                print obj1
                hp2.addObj(obj1,False,True)

                obj2 = {
                    "UID": ps_name,
                    "powers": "%s_%s" % (str(group), str(module))
                }
                hp3.addObj(obj2,False,True)
    db.commit("")
    """

    """
    for group in sct_modules:
        for module in sct_modules[group]:
            part = ""
            if group.startswith("SCT_EC_A"): part = "Endcap side A"
            elif group.startswith("SCT_EC_C"): part = "Endcap side C"
            elif group.startswith("SCT_Barrel"): part = "Barrel"
            obj3 = {
                "UID": str(group),
                "groupTo": "%s_%s" % (str(group), str(module)),
                "description": "Semiconductor Tracker %s quadrant %s" % (part, str(group[-1:])),
                "subsystem": "SCT"
            }
            hp1.addObj(obj3,False,True)

            obj4 = {
                "UID": "%s_%s" % (str(group), str(module)),
                "groupedBy": str(group),
                "subsystem": "SCT"
            }
            hp2.addObj(obj4,False,True)
    db.commit("")
    """

    hp4 = gas.GasSystemHelper()
    hp4.setDb(db)
    hp5 = crate.CrateHelper()
    hp5.setDb(db)
    """
    for group in sct_modules:
        for module in sct_modules[group]:
            if group.startswith("SCT_EC_A"):
                obj1 = {
                    "UID": "%s_%s" % (str(group), str(module)),
                    #"requiresGasFrom": "HCXGIDN001_CR300002",
                    "controlledBy": ["SCT_ROD_EC_A_1", "SCT_ROD_EC_A_2"]
                }
                hp2.addObj(obj1,False,True)
                # obj2 = {
                #     "UID": "HCXGIDN001_CR300002",
                #     "location": "UX15",
                #     "subsystem": "SCT",
                #     "gasTo": "%s_%s" % (str(group), str(module))
                # }
                # hp4.addObj(obj2,False,True)
                obj3 = {
                    "UID": "SCT_ROD_EC_A_1",
                    "controls": "%s_%s" % (str(group), str(module)),
                }
                hp5.addObj(obj3,False,True)
                obj4 = {
                    "UID": "SCT_ROD_EC_A_2",
                    "controls": "%s_%s" % (str(group), str(module)),
                }
                hp5.addObj(obj4,False,True)
            elif group.startswith("SCT_EC_C"):
                obj1 = {
                    "UID": "%s_%s" % (str(group), str(module)),
                    #"requiresGasFrom": "HCXGIDN001_CR300002",
                    "controlledBy": ["SCT_ROD_EC_C_1", "SCT_ROD_EC_C_2"]
                }
                hp2.addObj(obj1,False,True)
                # obj2 = {
                #     "UID": "HCXGIDN001_CR300002",
                #     "location": "UX15",
                #     "subsystem": "SCT",
                #     "gasTo": "%s_%s" % (str(group), str(module))
                # }
                # hp4.addObj(obj2,False,True)
                obj3 = {
                    "UID": "SCT_ROD_EC_C_1",
                    "controls": "%s_%s" % (str(group), str(module)),
                }
                hp5.addObj(obj3,False,True)
                obj4 = {
                    "UID": "SCT_ROD_EC_C_2",
                    "controls": "%s_%s" % (str(group), str(module)),
                }
                hp5.addObj(obj4,False,True)
            elif group.startswith("SCT_Barrel"):
                obj1 = {
                    "UID": "%s_%s" % (str(group), str(module)),
                    #"requiresGasFrom": ["HCXGIDN001_CR300002", "HCXGIDN001_CR300001"],
                    "controlledBy": ["SCT_ROD_Barrel_1", "SCT_ROD_Barrel_2", "SCT_ROD_Barrel_3", "SCT_ROD_Barrel_4"]
                }
                hp2.addObj(obj1,False,True)
                # obj2 = {
                #     "UID": "HCXGIDN001_CR300002",
                #     "location": "UX15",
                #     "subsystem": "SCT",
                #     "gasTo": "%s_%s" % (str(group), str(module))
                # }
                # hp4.addObj(obj2,False,True)
                obj3 = {
                    "UID": "SCT_ROD_Barrel_1",
                    "controls": "%s_%s" % (str(group), str(module))
                }
                hp5.addObj(obj3,False,True)
                obj4 = {
                    "UID": "SCT_ROD_Barrel_2",
                    "controls": "%s_%s" % (str(group), str(module))
                }
                hp5.addObj(obj4,False,True)
                obj5 = {
                    "UID": "SCT_ROD_Barrel_3",
                    "controls": "%s_%s" % (str(group), str(module))
                }
                hp5.addObj(obj5,False,True)
                obj6 = {
                    "UID": "SCT_ROD_Barrel_4",
                    "controls": "%s_%s" % (str(group), str(module))
                }
                hp5.addObj(obj6,False,True)

            obj7 = {
                "UID": "SCT_TTC",
                "controls": "%s_%s" % (str(group), str(module))
            }
            hp5.addObj(obj7,False,True)
            obj8 = {
                "UID": "%s_%s" % (str(group), str(module)),
                "location": "UX15",
                "controlledBy": "SCT_TTC"
            }
            hp2.addObj(obj8,False,True)

    db.commit("")
    """
    """
    hp6 = heater.HeaterHelper()
    hp6.setDb(db)
    for group in sct_modules:
            for module in sct_modules[group]:
                obj1 = {
                    "UID": "%s_%s" % (str(group), str(module)),
                    "requiresCoolingFrom": "SCT_Heater_%s" % str(group[-2:])
                }
                hp2.addObj(obj1,False,True)
                obj2 = {
                    "UID": "SCT_Heater_%s" % str(group[-2:]),
                    "coolingTo": "%s_%s" % (str(group), str(module))
                }
                hp6.addObj(obj2,False,True)
    db.commit("")
    """

    hp7 = interlock.InterlockHelper()
    hp7.setDb(db)
    """
    ps_list = filter(lambda e: e.UID().startswith("SCT_PS_"), db.get_objs("PowerSupply"))
    for group in sct_modules:
        for module in sct_modules[group]:
            for ps in sct_modules[group][module]["Crates"]:
                ps_name = next((p.UID() for p in ps_list if p.UID().endswith("%02d" % (ps))))
                sd_type = ""
                if group.startswith("SCT_Barrel"): sd_type = "Barrel"
                elif group.startswith("SCT_EC_A"): sd_type = "EndCapA"
                elif group.startswith("SCT_EC_C"): sd_type = "EndCapC"

                if ps_name[-5:].startswith("S2"):
                    obj1 = {
                        "UID": "SCT_Interlock_%s_Y0405S2" % sd_type,
                        "interlocks": ps_name
                    }
                    hp7.addObj(obj1,False,True)
                    obj2 = {
                        "UID": ps_name,
                        "interlockedBy": "SCT_Interlock_%s_Y0405S2" % sd_type
                    }
                    hp3.addObj(obj2,False,True)
                elif ps_name[-5:].startswith("A2"):
                    obj1 = {
                        "UID": "SCT_Interlock_%s_Y0611A2" %sd_type,
                        "interlocks": ps_name
                    }
                    hp7.addObj(obj1,False,True)
                    obj2 = {
                        "UID": ps_name,
                        "interlockedBy": "SCT_Interlock_%s_Y0611A2" % sd_type
                    }
                    hp3.addObj(obj2,False,True)
    db.commit("")
    """

    dcs_part = {"EC_A": [""], "EC_C": [""], "Barrel": ["_1","_2"]}
    for part in dcs_part:
        for q in range(1, 5):   #4 sectors
            for group in sct_modules:
                if group.startswith("SCT_%s_Q%s" % (part,q)):
                    for module in sct_modules[group]:
                        for i in dcs_part[part]:
                            obj1 = {
                                "UID": "%s_%s" % (str(group), str(module)),
                                "controlledBy": "SCT_DCS_%s_Q%s%s" % (part,str(q),str(i))
                            }
                            hp2.addObj(obj1,False,True)
                            obj2 = {
                                "UID": "SCT_DCS_%s_Q%s%s" % (part,str(q),str(i)),
                                "controls": "%s_%s" % (str(group), str(module))
                            }
                            hp5.addObj(obj2,False,True)
    db.commit("")
                
