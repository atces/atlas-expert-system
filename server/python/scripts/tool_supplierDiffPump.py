#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re
    import group
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import rack
    import pneumaticcontroller
    import vessel
    import vacuumpump

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_r = rack.RackHelper()
    hp_r.setDb(db)
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)
    hp_pc=pneumaticcontroller.PneumaticControllerHelper()
    hp_pc.setDb(db)
    hp_ve=vessel.VesselHelper()
    hp_ve.setDb(db)
    hp_pu=vacuumpump.VacuumPumpHelper()
    hp_pu.setDb(db)
    #supply, return, "L1",
    uids=[["PCVA1","PVA1", "L1", "Toroid end-cap side A", ["1P9901", "1P9902"]],
    ["PCVA2","PVA2", "L2", "LAr end-cap side A", ["1P4205"]],
    ["PCVA3","PVA3", "L3", "LAr barrel side A", ["2P4205"]],
    ["PCVA4","PVA4", "L4", "Toroid Barrel side A, level 1", ["bt1P7902", "bt2P7902", "bt4P7902", "bt5P7902"]],
    ["PCVA5","PVA5", "L5", "Toroid Barrel side A, level 2", ["bt3P7902", "bt6P7902", "bt7P7902", "bt8P7902"]],
    ["PCVA6","PVA6", "L6", "Toroid Barrel side C, level 1", ["bt1P7901", "bt2P7901", "bt4P7901", "bt5P7901"]],
    ["PCVA7","PVA7", "L7", "Toroid Barrel side C, level 2", ["bt3P7901", "bt6P7901", "bt7P7901", "bt8P7901"]],
    ["PCVA8","PVA8", "L8", "LAr Barrel side C", ["3P4205"]],
    ["PCVA9","PVA9", "L9", "LAr End-cap side C", ["4P4205"]],
    ["PCVA10","PVA10", "L10", "Toroid End-cap side C", ["3P9901", "3P9902"]],
    ["PCVA11","PVA11", "L11", "Valeve box, He Dewar",["He_Dewar_pump"]],
    ["PCVA12","PVA12", "L12", "LAr Solenoid dewar",["LAr_Solenoid_Dewar_pump"]]]
     #supply, return, line(coolingLoop), destination(vessel), location, description
    '''
    for x in range(1,26):
        print "[\"PL-LAr_PCVA%i\",\"PL-LAr_PVA%i\", \"\"]" % (x,x)
        pass
    '''
    groupVA="PVAs_Diff_Pumps"
    groupPumps="Vaccum_pumps"
    groupLines="Diff_Pumps_Lines_Group"
    coolingstation="FCUL_00016"
    pneumaticcontroller="FCTIR_00020_PC_Pumps"
    #create pneumaticcontroller
    o_pc={
        "UID":pneumaticcontroller,
        }
    hp_pc.addObj(o_pc, False, True)
    #create group
    o_groupVA={"UID":groupVA}
    hp_g.addObj(o_groupVA, False, True)
    o_groupPumps={"UID":groupPumps}
    hp_g.addObj(o_groupPumps, False, True)
    o_groupLines={"UID":groupLines}
    hp_g.addObj(o_groupLines, False, True)
    for u in uids:
        n_pva1="%s_Pumps" %u[0]#class pva
        n_pva2="%s_Pumps" %u[1]#class pva
        n_line="Cooling_Line_Pumps_%s" % u[2]#class cooling loop

        n_location="UX15 %s" % u[3]#manifold location
        #n_description=u[5]#manifold description

        #create pvas
        o_pva1={
            "UID":n_pva1, "description": "Cooling Line supply for Diffussion pumps", "groupedBy":groupVA
        }
        hp_pv.addObj(o_pva1, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":n_pva2, "description": "Cooling Line return for Diffussion pumps", "groupedBy":groupVA
        }
        hp_pv.addObj(o_pva2, False, True)
        for target in u[4]:
            print target
            n_pump=target#class vessel
            #create pump
            o_pump={
            "UID":n_pump, "groupedBy":groupPumps, "location":n_location
            }
            hp_pu.addObj(o_pump, False, True)
            o_cl={
                "UID":n_line,"controlledByAirFrom":n_pva1, "coolingTo":target, "requiresCoolingFrom":coolingstation, "groupedBy":groupLines
            }
            hp_cl.addObj(o_cl, False, True)
            #add it one
            o_cl={
                "UID":n_line,"controlledByAirFrom":n_pva2, "coolingTo":target,  "requiresCoolingFrom":coolingstation, "groupedBy":groupLines
                    }
            hp_cl.addObj(o_cl, False, True)


            # manifolds cooling from coolingloop
            o_pump={
                "UID":n_pump, "requiresCoolingFrom":n_line
            }
            hp_pu.addObj(o_pump, False, True)

            #grouping
            o_g={"UID":groupPumps, "groupTo":n_pump}
            hp_g.addObj(o_g, False, True)
            pass

        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva1={
            "UID":n_pva1,"controlsByAirTo":n_line, "controlledByAirFrom":pneumaticcontroller
        }
        hp_pv.addObj(o_pva1, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":n_pva2,"controlsByAirTo":n_line, "controlledByAirFrom":pneumaticcontroller
        }
        hp_pv.addObj(o_pva2, False, True)


        #pneumactic cont.
        o_pc={
            "UID":pneumaticcontroller,
            "controlsByAirTo":n_pva1,
        }
        hp_pc.addObj(o_pc, False, True)
        #pneumactic cont.
        o_pc={
            "UID":pneumaticcontroller,
                "controlsByAirTo":n_pva2,
                }
        hp_pc.addObj(o_pc, False, True)
        #create line (coolingloop)


        #grouping
        o_g={"UID":groupVA, "groupTo":n_pva1}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":groupVA, "groupTo":n_pva2}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":groupLines, "groupTo":n_line}
        hp_g.addObj(o_g, False, True)
        pass
    db.commit("")
