#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    alarms_db = db.get_objs("Alarm")
    hp1 = alarm.AlarmHelper()
    hp1.setDb(db)

    #Check if alarm exists on DB. If not, save the name into a text file.
    #Check if the alarm has description. If not, add the description to the DB.

    f = open("MissingAlarms.txt", "w")
    with open("AlarmListFull.txt", 'r') as file:
        try:
            line = next(file)
            while line:
                line = next(file)
                if line.startswith("====="):
                    name_line = next(file).split()
                    name = name_line[1]
                    found = False
                    for a in alarms_db:
                        if a.UID() == name:
                            if not a["description"]:
                                next(file)
                                description = next(file)
                                obj1 = {
                                    "UID": a.UID(),
                                    "description": description
                                }
                                hp1.addObj(obj1,False,True)
                            found = True
                            break
                    if not found:
                        f.write("%s\n" % name)
        except StopIteration:
            file.close()
    db.commit("")
    
