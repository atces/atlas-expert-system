#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import json

    import group
    import crate as crateHelper
    import subdetector
    import powersupply
    import gas
    import rack

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    
    chamber_types = {
        "BIS": [["1A","2A","3A","4A","5A","6A","7A","8A","1C","2C","3C","4C","5C","6C","7C","8C"], [2,4,6,8,10,12,14,16]], 
        "BIL": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [1,3,5,7,9,13]], 
        "BIM": [["1A","2A","3A","4A","5A","1C","2C","3C","4C","5C"], [11,15]], 
        "BIR": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [11,15]], 
        "BMS": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [2,4,6,8,10,16]],
        "BME_A": [["4A"], [13]],
        "BME_C": [["4C"], [13]],
        "BMF": [["1A","2A","3A","1C","2C","3C"], [12,14]],
        "BMG": [["2A","4A","6A","2C","4C","6C"], [12,14]],
        "BMR": [["4A","4C"], [13]],
        "BML": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [1,3,5,7,9,11,13,15]], #Here the sector 13 is not present in index 4!!!
        "BEE_A": [["1A","2A"], [2,4,6,8,10,12,14,16]],
        "BEE_C": [["1C","2C"], [2,4,6,8,10,12,14,16]],
        "BOS": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [2,4,6,8,10,16]],
        "BOF": [["1A","3A","5A","7A","1C","3C","5C","7C"], [12,14]],
        "BOG": [["0B","2A","4A","6A","8A","2C","4C","6C","8C"], [12,14]],
        "BOR": [["3A","3C"], [13]],
        "BOE_A": [["3A"], [13]],
        "BOE_C": [["3C"], [13]],
        "BOL": [["1A","2A","3A","4A","5A","6A","1C","2C","3C","4C","5C","6C"], [1,3,5,7,9,11,13,15]],
        "EIS_A": [["1A","2A"], [2,4,6,8,10,12,14,16]],
        "EIS_C": [["1C","2C"], [2,4,6,8,10,12,14,16]],
        "EIL_A": [["1A","2A","4A"], [1,3,5,7,9,11,13,15]],
        "EIL_C": [["1C","2C","4C"], [1,3,5,7,9,11,13,15]],
        "EES_A": [["1A","2A"], [2,4,6,8,10,12,14,16]],
        "EES_C": [["1C","2C"], [2,4,6,8,10,12,14,16]],
        "EEL_A": [["1A","2A"], [1,3,5,7,9,11,13,15]], #Here the 5 sector 5 is only present in index 2!!!
        "EEL_C": [["1C","2C"], [1,3,5,7,9,11,13,15]], #Here the 5 sector 5 is only present in index 2!!!
        "EMS_A": [["1A","2A","3A","4A","5A"], [2,4,6,8,10,12,14,16]],
        "EMS_C": [["1C","2C","3C","4C","5C"], [2,4,6,8,10,12,14,16]],
        "EML_A": [["1A","2A","3A","4A","5A"], [1,3,5,7,9,11,13,15]],
        "EML_C": [["1C","2C","3C","4C","5C"], [1,3,5,7,9,11,13,15]],
        "EOS_A": [["1A","2A","3A","4A","5A","6A"], [2,4,6,8,10,12,14,16]],
        "EOS_C": [["1C","2C","3C","4C","5C","6C"], [2,4,6,8,10,12,14,16]],
        "EOL_A": [["1A","2A","3A","4A","5A","6A"], [1,3,5,7,9,11,13,15]],
        "EOL_C": [["1C","2C","3C","4C","5C","6C"], [1,3,5,7,9,11,13,15]]
    }

    descriptions = [
        {
            "B": "Barrel",
            "E": "Endcap"
        },
        {
            "I": "Inner",
            "M": "Middle",
            "E": "Extra",
            "O": "Outer"
        },
        {
            "S": "Small",
            "L": "Large",
            "M": "close to calorimeter rails",
            "R": "below calorimeter rails and special RPC using MDT read-out",
            "F": "between feet",
            "E": "on endcap toroid and elevator shafts",
            "G": "inside foot"
        }
    ]

    #HV, LV, Duo
    rack_crates = {
        "Y.04-17.X7": [[5], [4], []],
        "Y.05-11.X2": [[2], [1], []],
        "Y.05-20.X1": [[], [0], []],
        "Y.06-10.X7": [[], [3], []],
        "Y.26-24.X1": [[], [], []],     #!!!!!!
        "Y.29-04.X7": [[], [], [0]],
        "Y.31-05.X0": [[], [], [0]],
        "Y.32-05.X8": [[], [], [1]],
        "Y.34-03.X1": [[], [], [1]],
        "Y.35-23.X8": [[], [], [0]],
        "Y.35-25.X2": [[], [], [0]],
        "Y.36-23.X8": [[], [], [1]],
        "Y.36-25.X2": [[], [], [1]],
        "Y.36-25.X4": [[1], [0], []],
        "Y.37-23.X0": [[3], [2], []],
        "Y.38-23.X0": [[1], [0], []],
        "Y.41-05.X8": [[2], [3], []],
        "Y.42-03.X1": [[0], [1], []],
        "Y.45-02.X2": [[0], [1], []],
        "Y.46-05.X0": [[3], [2], []],
        "Y.51-23.X0": [[3], [2], []],
        "Y.53-25.X4": [[], [3], []],
        "Y.54-23.X8": [[], [2], []],
        "Y.55-05.X8": [[], [2], []],
        "Y.55-23.X8": [[], [3], []],
        "Y.57-05.X0": [[], [3], []],
        "Y.59-24.X1": [[], [2], []],
        "Y.60-04.X1": [[], [2], []],
        "Y.60-04.X7": [[], [3], []],
        "Y.62-24.X1": [[], [], []],     #!!!!!!
        "Y.82-10.X7": [[], [3], []],
        "Y.83-11.X2": [[2], [1], []],
        "Y.83-20.X1": [[], [0], []],
        "Y.84-17.X7": [[5], [4], []],
        "Y.M03-EM.XA": [[], [3], []],
        "Y.M03-EM.XC": [[], [3], []],
        "Y.M07-EM.XA": [[5], [4], []],
        "Y.M07-EM.XC": [[5], [4], []],
        "Y.M11-EM.XA": [[1], [0], []],
        "Y.M11-EM.XC": [[1], [0], []],
        "Y.M15-EM.XA": [[], [2], []],
        "Y.M15-EM.XC": [[], [2], []],
    }

    #Chamber type, side(s), HV, LV, both
    rack_power = [
        ["Y.04-17.X7", "EOL", "A", [3,9], [], [5,7]],
        ["Y.05-11.X2", "EOL", "A", [1,11], [], [13,15]],
        ["Y.05-20.X1", "EOL", "A", [], [9,11], []],
        ["Y.06-10.X7", "EOL", "A", [], [1,3], []],
        ["Y.82-10.X7", "EOL", "C", [], [1,3], []],
        ["Y.83-11.X2", "EOL", "C", [1,11], [], [13,15]],
        ["Y.83-20.X1", "EOL", "C", [], [9,11], []],
        ["Y.84-17.X7", "EOL", "C", [3,9], [], [5,7]],

        ["Y.04-17.X7", "EOS", "A", [2,4], [], [6,8]],
        ["Y.05-11.X2", "EOS", "A", [10,12], [], [14,16]],
        ["Y.05-20.X1", "EOS", "A", [], [10,12], []],
        ["Y.06-10.X7", "EOS", "A", [], [2,4], []],
        ["Y.82-10.X7", "EOS", "C", [], [2,4], []],
        ["Y.83-11.X2", "EOS", "C", [10,12], [], [14,16]],
        ["Y.83-20.X1", "EOS", "C", [], [10,12], []],
        ["Y.84-17.X7", "EOS", "C", [2,4], [], [6,8]],

        #IT'S BETTER TO ADD THE COMMENTED ONES MANUALLY

        # ["Y.26-24.X1", "BEE", "A", [], [], [2,4,6,8,10,12,14,16]],  #--!
        # ["Y.62-24.X1", "BEE", "C", [], [], [2,4,6,8,10,12,14,16]],  #--!

        # ["Y.26-24.X1", "EEL", "A", [], [], [1,3,5,7,9,11,13,15]],   #5 only to index 2    #--!
        # ["Y.62-24.X1", "EEL", "C", [], [], [1,3,5,7,9,11,13,15]],   #5 only to index 2    #--!

        # ["Y.26-24.X1", "EES", "A", [], [], [2,4,6,8,10,12,14,16]],  #--!
        # ["Y.62-24.X1", "EES", "C", [], [], [2,4,6,8,10,12,14,16]],  #--!

        # ["Y.26-24.X1", "EIL", "A", [], [], [1,3,5,7,9,11,13,15]],   #--!  
        # ["Y.62-24.X1", "EIL", "C", [], [], [1,3,5,7,9,11,13,15]],   #--!

        # ["Y.26-24.X1", "EIS", "A", [], [], [2,4,6,8,10,12,14,16]],  #--!
        # ["Y.62-24.X1", "EIS", "C", [], [], [2,4,6,8,10,12,14,16]],  #--!

        ["Y.29-04.X7", "BOL", "A", [3], [1], []],
        ["Y.31-05.X0", "BOL", "A", [15], [13], []],
        ["Y.32-05.X8", "BOL", "A", [5], [3], []],
        ["Y.34-03.X1", "BOL", "A", [1], [15], []],
        ["Y.35-23.X8", "BOL", "A", [7], [5], []],
        ["Y.35-25.X2", "BOL", "A", [11], [9], []],
        ["Y.36-23.X8", "BOL", "A", [9], [7], []],
        ["Y.36-25.X2", "BOL", "A", [13], [11], []],
        ["Y.29-04.X7", "BOL", "C", [3], [], []],
        ["Y.31-05.X0", "BOL", "C", [15], [], []],
        ["Y.32-05.X8", "BOL", "C", [5], [], []],
        ["Y.34-03.X1", "BOL", "C", [1], [], []],
        ["Y.35-23.X8", "BOL", "C", [7], [], []],
        ["Y.35-25.X2", "BOL", "C", [11], [], []],
        ["Y.36-23.X8", "BOL", "C", [9], [], []],
        ["Y.36-25.X2", "BOL", "C", [13], [], []],
        ["Y.53-25.X4", "BOL", "C", [], [9], []],
        ["Y.54-23.X8", "BOL", "C", [], [5], []],
        ["Y.55-05.X8", "BOL", "C", [], [3], []],
        ["Y.55-23.X8", "BOL", "C", [], [7], []],
        ["Y.57-05.X0", "BOL", "C", [], [13], []],
        ["Y.59-24.X1", "BOL", "C", [], [11], []],
        ["Y.60-04.X1", "BOL", "C", [], [15], []],
        ["Y.60-04.X7", "BOL", "C", [], [1], []],

        ["Y.29-04.X7", "BOS", "A", [], [], [2]],
        ["Y.32-05.X8", "BOS", "A", [], [], [4]],
        ["Y.34-03.X1", "BOS", "A", [], [], [16]],
        ["Y.35-23.X8", "BOS", "A", [], [], [6]],
        ["Y.35-25.X2", "BOS", "A", [], [], [10]],
        ["Y.36-23.X8", "BOS", "A", [], [], [8]],
        ["Y.29-04.X7", "BOS", "C", [2], [], []],
        ["Y.32-05.X8", "BOS", "C", [4], [], []],
        ["Y.34-03.X1", "BOS", "C", [16], [], []],
        ["Y.35-23.X8", "BOS", "C", [6], [], []],
        ["Y.35-25.X2", "BOS", "C", [10], [], []],
        ["Y.36-23.X8", "BOS", "C", [8], [], []],
        ["Y.53-25.X4", "BOS", "C", [], [10], []],
        ["Y.54-23.X8", "BOS", "C", [], [6], []],
        ["Y.55-05.X8", "BOS", "C", [], [4], []],
        ["Y.55-23.X8", "BOS", "C", [], [8], []],
        ["Y.60-04.X1", "BOS", "C", [], [16], []],
        ["Y.60-04.X7", "BOS", "C", [], [2], []],

        ["Y.31-05.X0", "BOE", "C", [13], [], []],
        ["Y.36-25.X2", "BOE", "A", [13], [], []],
        ["Y.57-05.X0", "BOE", "A", [], [13], []],
        ["Y.57-05.X0", "BOE", "C", [], [13], []],

        ["Y.31-05.X0", "BOF", "A", [], [], [14]],
        ["Y.36-25.X2", "BOF", "A", [], [], [12]],
        ["Y.31-05.X0", "BOF", "C", [14], [], []],
        ["Y.36-25.X2", "BOF", "C", [12], [], []],
        ["Y.57-05.X0", "BOF", "C", [], [14], []],
        ["Y.59-24.X1", "BOF", "C", [], [12], []],

        ["Y.31-05.X0", "BOG", "B", [], [], [14]],   #!!
        ["Y.36-25.X2", "BOG", "B", [], [], [12]],   #!!
        ["Y.31-05.X0", "BOG", "A", [], [], [14]],
        ["Y.36-25.X2", "BOG", "A", [], [], [12]],
        ["Y.31-05.X0", "BOG", "C", [14], [], []],
        ["Y.36-25.X2", "BOG", "C", [12], [], []],
        ["Y.57-05.X0", "BOG", "C", [], [14], []],
        ["Y.59-24.X1", "BOG", "C", [], [12], []],

        ["Y.36-25.X4", "BML", "A", [], [], [7,9]],
        ["Y.37-23.X0", "BML", "A", [], [], [11,13]],    #13 NOT FOR INDEX 4!!!!
        ["Y.41-05.X8", "BML", "A", [], [], [3,5]],
        ["Y.45-02.X2", "BML", "A", [], [], [1,15]],
        ["Y.36-25.X4", "BML", "C", [], [], [7,9]],
        ["Y.37-23.X0", "BML", "C", [], [], [11,13]],    #13 NOT FOR INDEX 4!!!!
        ["Y.41-05.X8", "BML", "C", [], [], [3,5]],
        ["Y.45-02.X2", "BML", "C", [], [], [1,15]],

        ["Y.36-25.X4", "BMS", "A", [6], [10], [8]],
        ["Y.37-23.X0", "BMS", "A", [10], [], []],
        ["Y.41-05.X8", "BMS", "A", [2], [6], [4]],
        ["Y.45-02.X2", "BMS", "A", [], [2], [16]],
        ["Y.36-25.X4", "BMS", "C", [6], [10], [8]],
        ["Y.37-23.X0", "BMS", "C", [10], [], []],
        ["Y.41-05.X8", "BMS", "C", [2], [6], [4]],
        ["Y.45-02.X2", "BMS", "C", [], [2], [16]],

        ["Y.37-23.X0", "BME", "A", [], [], [13]],
        ["Y.37-23.X0", "BME", "C", [], [], [13]],

        ["Y.37-23.X0", "BMF", "A", [], [14], [12]],
        ["Y.45-02.X2", "BMF", "A", [14], [], []],
        ["Y.37-23.X0", "BMF", "C", [], [14], [12]],
        ["Y.45-02.X2", "BMF", "C", [14], [], []],

        ["Y.37-23.X0", "BMG", "A", [12,14], [], []],
        ["Y.59-24.X1", "BMG", "A", [], [12], []],
        ["Y.60-04.X1", "BMG", "A", [], [14], []],
        ["Y.37-23.X0", "BMG", "C", [12,14], [], []],
        ["Y.59-24.X1", "BMG", "C", [], [12], []],
        ["Y.60-04.X1", "BMG", "C", [], [14], []],

        ["Y.38-23.X0", "BIL", "A", [13], [5], [7,9]],
        ["Y.42-03.X1", "BIL", "A", [5], [13], [1,3]],
        ["Y.46-05.X0", "BIL", "C", [5], [13], [1,3]],
        ["Y.51-23.X0", "BIL", "C", [13], [5], [7,9]],

        ["Y.38-23.X0", "BIM", "A", [], [], [11]],
        ["Y.42-03.X1", "BIM", "A", [], [], [15]],
        ["Y.46-05.X0", "BIM", "C", [], [], [15]],
        ["Y.51-23.X0", "BIM", "C", [], [], [11]],

        ["Y.38-23.X0", "BIR", "A", [], [], [11]],
        ["Y.42-03.X1", "BIR", "A", [], [], [15]],
        ["Y.46-05.X0", "BIR", "C", [], [], [15]],
        ["Y.51-23.X0", "BIR", "C", [], [], [11]],

        ["Y.38-23.X0", "BIS", "A", [], [], [6,8,10,12]],
        ["Y.42-03.X1", "BIS", "A", [], [], [2,4,14,16]],
        ["Y.46-05.X0", "BIS", "C", [], [], [2,4,14,16]],
        ["Y.51-23.X0", "BIS", "C", [], [], [6,8,10,12]],

        ["Y.57-05.X0", "BMR", "A", [], [13], []],
        ["Y.57-05.X0", "BMR", "C", [], [13], []],

        ["Y.57-05.X0", "BOR", "A", [], [13], []],
        ["Y.57-05.X0", "BOR", "C", [], [13], []],

        ["Y.M03-EM.XA", "EML", "A", [], [1,3], []],
        ["Y.M03-EM.XC", "EML", "C", [], [1,3], []],
        ["Y.M07-EM.XA", "EML", "A", [1,3], [], [5,7]],
        ["Y.M07-EM.XC", "EML", "C", [1,3], [], [5,7]],
        ["Y.M11-EM.XA", "EML", "A", [13,15], [], [9,11]],
        ["Y.M11-EM.XC", "EML", "C", [13,15], [], [9,11]],
        ["Y.M15-EM.XA", "EML", "A", [], [13,15], []],
        ["Y.M15-EM.XC", "EML", "C", [], [13,15], []],

        ["Y.M03-EM.XA", "EMS", "A", [], [2,4], []],
        ["Y.M03-EM.XC", "EMS", "C", [], [2,4], []],
        ["Y.M07-EM.XA", "EMS", "A", [2,4], [], [6,8]],
        ["Y.M07-EM.XC", "EMS", "C", [2,4], [], [6,8]],
        ["Y.M11-EM.XA", "EMS", "A", [14,16], [], [10,12]],
        ["Y.M11-EM.XC", "EMS", "C", [14,16], [], [10,12]],
        ["Y.M15-EM.XA", "EMS", "A", [], [14,16], []],
        ["Y.M15-EM.XC", "EMS", "C", [], [14,16], []],
    ]

    gen_racks = {
        "MDT_Gen_Y0805S2_1": {
            "Y.04-17.X7": [5,4],
            "Y.05-11.X2": [1]
        },
        "MDT_Gen_Y0805S2_2": {
            "Y.05-11.X2": [2],
            "Y.05-20.X1": [0],
            "Y.26-24.X1": [2],
            "Y.06-10.X7": [3]
        },
        "MDT_Gen_Y0805S2_3": {
            "Y.26-24.X1": [1,3],
            "Y.M11-EM.XA": [1,0]
        },
        "MDT_Gen_Y0805S2_4": {
            "Y.M03-EM.XA": [3],
            "Y.M15-EM.XA": [2],
            "Y.26-24.X1": [0],
            "Y.M07-EM.XA": [5,4]
        },
        "MDT_Gen_Y0905S2_1": {
            "Y.31-05.X0": [0],
            "Y.34-03.X1": [1],
            "Y.37-23.X0": [3,2],
            "Y.38-23.X0": [1]
        },
        "MDT_Gen_Y0905S2_2": {
            "Y.35-25.X2": [0],
            "Y.38-23.X0": [0],
            "Y.29-04.X7": [0]
        },
        "MDT_Gen_Y0905S2_3": {
            "Y.32-05.X8": [1],
            "Y.36-25.X2": [1],
            "Y.36-25.X4": [1,0]
        },
        "MDT_Gen_Y0905S2_4": {
            "Y.35-23.X8": [0],
            "Y.36-23.X8": [1],
            "Y.41-05.X8": [2,3]
        },
        "MDT_Gen_Y2505S2_1": {
            "Y.46-05.X0": [3],
            "Y.51-23.X0": [3],
            "Y.54-23.X8": [2],
            "Y.57-05.X0": [3],
            "Y.42-03.X1": [0],
            "Y.45-02.X2": [1]
        },
        "MDT_Gen_Y2505S2_2": {
            "Y.46-05.X0": [2],
            "Y.60-04.X1": [2],
            "Y.45-02.X2": [0],
            "Y.55-23.X8": [3]
        },
        "MDT_Gen_Y2505S2_3": {
            "Y.51-23.X0": [2],
            "Y.59-24.X1": [2],
            "Y.53-25.X4": [3]
        },
        "MDT_Gen_Y2505S2_4": {
            "Y.42-03.X1": [1],
            "Y.60-04.X7": [3],
            "Y.55-05.X8": [2]
        },
        "MDT_Gen_Y2605S2_1": {
            "Y.84-17.X7": [5,4],
            "Y.83-11.X2": [1]
        },
        "MDT_Gen_Y2605S2_2": {
            "Y.62-24.X1": [2],
            "Y.83-11.X2": [2],
            "Y.83-20.X1": [0],
            "Y.82-10.X7": [3]
        },
        "MDT_Gen_Y2605S2_3": {
            "Y.62-24.X1": [1,3],
            "Y.M11-EM.XC": [1,0]
        },
        "MDT_Gen_Y2605S2_4": {
            "Y.M03-EM.XC": [3],
            "Y.M15-EM.XC": [2],
            "Y.62-24.X1": [0],
            "Y.M07-EM.XC": [5,4]
        }
    }

    """
    #Adding the relationships between Generators and Racks
    hp1 = powersupply.PowerSupplyHelper()
    hp1.setDb(db)
    hp2 = crateHelper.CrateHelper()
    hp2.setDb(db)
    for gen in gen_racks:
        obj1 = {
            "UID": gen,
            "location": "US15",
        }
        powers = []
        for r in gen_racks[gen]:
            for cr in gen_racks[gen][r]:
                if rack_crates[r][0]:
                    if cr in rack_crates[r][0]:
                        crate_uid = "MDT_HVPS_%s_%02d" % (r.replace(".","").replace("-",""), cr)
                        obj2 = {
                            "UID": crate_uid,
                            "requiresPowerFrom": gen
                        }
                        hp2.addObj(obj2,False,True)
                        powers.append(crate_uid)
                if rack_crates[r][1]:
                    if cr in rack_crates[r][1]:
                        crate_uid = "MDT_LVPS_%s_%02d" % (r.replace(".","").replace("-",""), cr)
                        obj2 = {
                            "UID": crate_uid,
                            "requiresPowerFrom": gen
                        }
                        hp2.addObj(obj2,False,True)
                        powers.append(crate_uid)
                if rack_crates[r][2]:
                    if cr in rack_crates[r][2]:
                        crate_uid = "MDT_PS_%s_%02d" % (r.replace(".","").replace("-",""), cr)
                        obj2 = {
                            "UID": crate_uid,
                            "requiresPowerFrom": gen
                        }
                        hp2.addObj(obj2,False,True)
                        powers.append(crate_uid)      
        #obj1["powers"] = powers
        #hp1.addObj(obj1,False,True)
    db.commit("")
    """                          


    """
    #Adding the chamber groups
    hp = group.GroupHelper()
    hp.setDb(db)
    for ch in chamber_types:
        descr = ""
        lst = list(ch)
        for i in range (0,3):
            if(i == 2 and lst[i] != "S" and lst[i] != "L"):
                descr += descriptions[i][lst[i]]
            else:
                descr = descriptions[i][lst[i]] + " " + descr

        obj = {
            "UID": "MDT_%s" % ch,
            "description": descr.strip(),
            "location": "UX15",
            "subsystem": "MDT"
        }
        hp.addObj(obj)

    db.commit("")
    """

    """
    #Adding the chambers
    hp1 = subdetector.SubDetectorHelper()
    hp1.setDb(db)
    hp2 = group.GroupHelper()
    hp2.setDb(db)
    hp3 = crateHelper.CrateHelper()
    hp3.setDb(db)
    for ct in chamber_types:
        descr = ""
        lst = list(ct)
        for i in range (0,3):
            if(i == 2 and lst[i] != "S" and lst[i] != "L"):
                descr += descriptions[i][lst[i]]
            else:
                descr = descriptions[i][lst[i]] + " " + descr

        for index in chamber_types[ct][0]:
            for sector in chamber_types[ct][1]:
                chamber = "MDT_%s_%s_%02d" % (ct.replace("_A","").replace("_C",""), index, sector)
                ind_lst = list(index)
                d = descr.strip() + (" index %s side %s sector %02d" % (ind_lst[0], ind_lst[1], sector))
                
                obj1 = {
                    "UID": chamber,
                    "description": d,
                    "location": "UX15",
                    "subsystem": "MDT",
                    "groupedBy": "MDT_%s" % ct,
                    "controlledBy": "MDT_TTC_%s" % ("EC" if ct.endswith(("_A", "_C")) else "B")
                }                
                hp1.addObj(obj1,False,True)                    

                obj2 = {
                    "UID": "MDT_%s" % ct,
                    "groupTo": chamber
                }
                hp2.addObj(obj2,False,True)

                obj3 = {
                    "UID": "MDT_TTC_%s" % ("EC" if ct.endswith(("_A", "_C")) else "B"),
                    "controls": chamber
                }
                hp3.addObj(obj3,False,True)

        db.commit("")
    """

    """
    def printPowerRel(crate, sector, crate_type="PS"):
        hp1 = crateHelper.CrateHelper()
        hp1.setDb(db)
        hp2 = subdetector.SubDetectorHelper()
        hp2.setDb(db)

        for index in chamber_types[ch_type][0]:
            if index.endswith(ch_side) :
                obj1 = {
                    "UID": "MDT_%s_%s_%02d" % (crate_type, r_name.replace(".","").replace("-",""), crate),
                    "powers": "MDT_%s_%s_%02d" % (ch_type, index, sector)
                }
                hp1.addObj(obj1,False,True)

                obj2 = {
                    "UID": "MDT_%s_%s_%02d" % (ch_type, index, sector),
                    "requiresPowerFrom": "MDT_%s_%s_%02d" % (crate_type, r_name.replace(".","").replace("-",""), crate)
                }
                hp2.addObj(obj2,False,True)

        db.commit("")
    

    #Adding the relationships from crates to chambers
    for r in rack_power:
        r_name = r[0]
        ch_type = r[1]
        ch_side = r[2]
        ch_hv = r[3]
        ch_lv = r[4]
        ch_duo = r[5]


        if ch_hv:
            for sector in ch_hv:
                if rack_crates[r_name][0]:
                    printPowerRel(rack_crates[r_name][0][0], sector, "HVPS")
                else:
                    printPowerRel(rack_crates[r_name][2][0], sector)
        if ch_lv:
            for sector in ch_lv:
                if rack_crates[r_name][1]:
                    printPowerRel(rack_crates[r_name][1][0], sector, "LVPS")
                else:
                    printPowerRel(rack_crates[r_name][2][0], sector)
        if ch_duo:
            for sector in ch_duo:
                if rack_crates[r_name][2]:
                    printPowerRel(rack_crates[r_name][2][0], sector)
                else:
                    printPowerRel(rack_crates[r_name][0][0], sector, "HVPS")
                    printPowerRel(rack_crates[r_name][1][0], sector, "LVPS")
    """

    """
    #Adding gas
    with open("mdt_gas.json") as gas_json:
        gas_racks = json.load(gas_json)
    hp1 = gas.GasSystemHelper()
    hp1.setDb(db)    
    hp2 = subdetector.SubDetectorHelper()
    hp2.setDb(db)
    for rack in gas_racks:
        sides = []
        if gas_racks[rack][0] == "AC":
            sides = ["A","C"]
        else:
            sides = [gas_racks[rack][0]]
        for ch_type in gas_racks[rack][1]:
            for index in gas_racks[rack][1][ch_type][0]:
                for sector in gas_racks[rack][1][ch_type][1]:
                    for side in sides:
                        obj1 = {
                            "UID": str(rack),
                            "gasTo": "MDT_%s_%s_%02d" % (str(ch_type), str(index) + str(side), sector)
                        }
                        print obj1
                        hp1.addObj(obj1,False,True)
                        obj2 = {
                            "UID": "MDT_%s_%s_%02d" % (str(ch_type), str(index) + str(side), sector),
                            "requiresGasFrom": str(rack)
                        }
                        hp2.addObj(obj2,False,True)
    db.commit("")
    """

    #Adding contains relationship between racks and crates
    hp1 = crateHelper.CrateHelper()
    hp1.setDb(db)
    hp2 = rack.RackHelper()
    hp2.setDb(db)
    crates = db.get_objs("Crate")
    for crate in crates:
	uid = crate.UID()
        if uid.startswith("MDT_PS") or uid.startswith("MDT_HVPS") or uid.startswith("MDT_LVPS"):
            split_uid = crate.UID().split('_')
	    rack_name = list(split_uid[2])

            if rack_name[1] == "M":
		rack_full_name = rack_full_name = rack_name[0]+"."+rack_name[1]+rack_name[2]+rack_name[3]+"-"+rack_name[4]+rack_name[5]+"."+rack_name[6]+rack_name[7]
	    else:
            	rack_full_name = rack_name[0]+"."+rack_name[1]+rack_name[2]+"-"+rack_name[3]+rack_name[4]+"."+rack_name[5]+rack_name[6]
	    print rack_full_name

            obj1 = {
                "UID": uid,
                "containedIn": rack_full_name
            }
            hp1.addObj(obj1,False,True)

            obj2 = {
                "UID": rack_full_name,
                "contains": uid
            }
            hp2.addObj(obj2,False,True)

    db.commit("")
            
