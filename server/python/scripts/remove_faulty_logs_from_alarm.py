#!/usr/bin/env python
#######################################################
# remove any log not starting with "log_"
# from ES_Alarm
# Florian.Haslbeck@cern.ch
# November 2023
#######################################################


if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    # get all ES_Alarms
    es_alarms = db.get_objs("Alarm")

    # loop over all ES_Alarms
    for alarm in es_alarms:
        # get all logs
        logs = alarm["logs"]

        new_logs = []
        for log in logs:
            if "log_" not in log.UID():
                print(f"Remove log {log.UID()} from alarm {alarm.UID()}")
            else:
                new_logs.append(log)

        alarm["logs"] = new_logs
        db.commit()
    
    print("Done")

        
               

