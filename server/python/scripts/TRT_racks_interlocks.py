cd #!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector


    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    ecs = filter(lambda e: e.UID().startswith("TRT_E"), db.get_objs("SubDetector"))

    hp_rack = rack.RackHelper()
    hp_rack.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_sub = subdetector.SubDetectorHelper()
    hp_sub.setDb(db)
    hp_sub.setVerbose(True)
    hp_ps = powersupply.PowerSupplyHelper()
    hp_ps.setDb(db)

    racksUX15=["Y.53-23.X0","Y.59-23.X8","Y.27-23.X8", "Y.53-05.X8", "Y.33-05.X8"]
    '''
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec)
        #powersuplies=rackobj["contains"]
        for rack in racks:
            print "python rack.py ../../data/sb.data.xml --uid %s --controls %s -e -y" % (rackobj.UID(), powersuplie.UID())
            print ""
            print "python powersupply.py ../../data/sb.data.xml --uid %s --controlledby %s  -e -y" % ( powersuplie.UID(), rackobj.UID())
            print ""
            print ""
            passa
        pass
        

    racks=["Y.35-04.X7_supply","Y.36-04.X7_return ",
           "Y.35-03.X1_supply","Y.36-03.X1_return",
           "Y.51-24.X7_supply","Y.52-24.X7_return",
           "Y.52-25.X1_supply","Y.53-25.X1_return",]
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec.UID())
        for rack in racks:
            
            obj_rack={
                "UID":str(rack),
                "gasTo":ec.UID(),
                }
            #print "Adding Rackgas: ",  obj_rack["UID"]
            hp_gas.addObj(obj_rack, False, True)
            
            obj_ec={
                "UID":str(ec.UID()),
                "requiresGasFrom":str(rack)
                }
            #print "Adding Subdetector: ",  obj_ec["UID"]
            hp_sub.addObj(obj_ec, False, True)
            pass
        pass
        
    racks=["Y.24-14.A1","Y.25-14.A1","Y.26-14.A1", "Y.27-14.A1"]
    for rack in racks:
        r=db.get_obj("Rack", rack)
        print r.UID()
        pss=r["contains"]
        for ps in pss:
            obj_rack={
                "UID":str(rack),
                "controls":ps.UID(),
            }
            hp_rack.addObj(obj_rack, False, True)
            obj_ps={
                "UID":str(ps.UID()),
                "controlledBy":str(rack),
            }
            hp_ps.addObj(obj_ps, False, True)
        pass
    ecs = filter(lambda e: e.UID().startswith("TRT_E"), db.get_objs("SubDetector"))
    '''
    sub_endcap = filter(lambda e: e.UID().startswith("TRT_Endcap"), db.get_objs("SubDetector"))
    sub_barrel = filter(lambda e: e.UID().startswith("TRT_Barrel"), db.get_objs("SubDetector"))


    gas_sys="HCXGDIS001-CR301102"
    for sub in sub_barrel:
        o_gas_sys={
            "UID":gas_sys,
            "gasTo":str(sub.UID()),
        }
        hp_gas.addObj(o_gas_sys, False, True)
        o_sub={
            "UID":str(sub.UID()),
            "requiresGasFrom":gas_sys,
        }
        hp_sub.addObj(o_sub, False, True)
        pass

    db.commit("")
