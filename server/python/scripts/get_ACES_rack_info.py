#!/usr/bin/env python
# @author gustavo.uribe@cern.ch

import getpass
import os
import config
import argparse
import logging
import re
import cx_Oracle
from cryptography.fernet import Fernet


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Prints the rack information from ACES')
	parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid',nargs="+",required=False,help="UID(s) of the object for fixing the consistency.")
	args = parser.parse_args()

	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.INFO)
	logger.debug("Loading database...")	
	db=config.Configuration("oksconfig:"+args.database)

	dsn_tns = cx_Oracle.makedsn('atlr-s.cern.ch', '10121', service_name='atlas_glance.cern.ch')
	home_dir=os.path.expanduser("~")
	with open(home_dir+"/private/akey","r") as key_file:
		key=key_file.read()
		f = Fernet(key)
		saltpassword=f.decrypt(bytes(config.Configuration("oksconfig:../../../data/sb.data.xml").get_obj('MetaInfo','Singleton').get_string("atlas_infrastructure_pub"),'utf-8')).decode('utf-8')
		password=saltpassword.replace(getpass.getuser(),'')

	conn = cx_Oracle.connect(user='atlas_infrastructure_pub', password=password, dsn=dsn_tns)

	cur = conn.cursor()
	logger.debug("Getting the racks from ACES...")	
	#query=cur.execute("select * from ATLAS_INFRASTRUCTURE.ATCN_RACK_INFO where LABEL="+racks[0]+" OR LABEL=".join(racks))
	#query=cur.execute("select * from ATLAS_INFRASTRUCTURE.ATCN_RACK_INFO")
	#aces_racks=query.fetchall()
	#result[0]:ACES_id
	#result[1]:LABEL
	#result[2]:Usage
	#result[3]:Subsystem
	#result[4]:Location
	#result[5]:height
	#result[6]:width
	#result[7]:weight?
	#result[8]:orientation
	#result[9]:responsible persons separated with ;
	#result[10]:responsible groups separated with ;for aces_rack in aces_racks:
	if args.uid:
		objects=[db.get_obj("Rack",uid) for uid in args.uid] 
	else:
		objects=db.get_objs("Rack")
	print("ID;%sLabel;%sUsage;%sSubsytem;%sLocation;%sHeight;%sWeight;%sOrientation;"%(" "*(15-len("ID"))," "*(15-len("Label"))," "*(15-len("Usage"))," "*(15-len("Subsystem"))," "*(15-len("Location"))," "*(10-len("Height"))," "*(10-len("Width"))))
	for obj in objects:
		query_str="select * from ATLAS_INFRASTRUCTURE.ATCN_RACK_INFO where LABEL='"+obj.UID()+"'"
		if obj["otherIds"]:
			query_str+=" OR LABEL='"
			query_str+="' OR LABEL='".join(obj["otherIds"])
			query_str+="'"
		query=cur.execute(query_str)
		aces_racks=query.fetchall()
		if not aces_racks: print(obj.UID())
		if len(aces_racks)>1: print("Warning! Multiple (%s) results per rack %s"%(len(aces_racks),obj.UID())) 
		for aces_rack in aces_racks:
			values=[None]*11
			for i,value in enumerate(aces_rack):
				values[i]=str(value) if aces_rack[i] else ""
			print("%s%s%s%s%s%s%s"%(values[0]+";"+" "*(15-len(values[0])),values[1]+";"+" "*(15-len(values[1])),values[2]+";"+" "*(15-len(values[2])),values[3]+";"+" "*(15-len(values[3])),values[4]+";"+" "*(15-len(values[4])),values[5]+";"+" "*(10-len(values[5])),values[6]+";"+" "*(10-len(values[6]))))
	cur.close()
	conn.close()

