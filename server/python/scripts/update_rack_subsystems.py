#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Updates the rack information based on the ACES data (some exception are considered where the ES data is more accuare)

import os
import getpass
import config
import argparse
import logging
import re
import cx_Oracle
from cryptography.fernet import Fernet


subsystems_maps={"cryogenics":"Cryo",
		"ass":"Safety"}

racks_to_be_corrected_in_aces=["Y.05-05.S2","Y.06-05.S2","Y.23-05.S2","Y.26-07.S2","Y.27-07.S2",
				"Y.11-02.A1","Y.12-02.A1","Y.13-02.A1","Y.28-21.A1","Y.29-21.A1",
				"Y.30-21.A1","Y.27-21.A2","Y.28-21.A2","Y.29-21.A2",
				"Y.59-23.A2",
				"Y.25-07.A2","Y.33-02.X7","Y.34-02.X7","Y.39-02.X7","Y.40-02.X7",
				"Y.41-02.X7","Y.47-02.X7","Y.48-02.X7","Y.35-04.X7","Y.36-04.X7","Y.52-25.X1",
				"Y.54-02.X4","Y.34-02.X4",
				"Y.53-25.X1","SGX1.X10-Y03","SGX1.X10-Y02","SGX1.X10-Y05","SGX1.X10-Y06","SGX1.X10-Y07"]

parser = argparse.ArgumentParser(description='Updates the subsystem info of the rack using ACES')
parser.add_argument('-d','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
args = parser.parse_args()

logging.basicConfig(format='%(asctime)-15s %(message)s')
logger=logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.debug("Loading database...")	
db=config.Configuration("oksconfig:"+args.database)

logger.debug("Loading racks from the Expert System and the otherids...")
rack_obj_per_uid={rack.UID():rack for rack in db.get_objs("Rack")}
rack_obj_per_uid.update({cabinet.UID():cabinet for cabinet in db.get_objs("ElectricalCabinet")})
rack_obj_per_uid.update({switchboard.UID():switchboard for switchboard in db.get_objs("Switchboard")})
rack_per_other_id={}
for uid,rack in rack_obj_per_uid.items():
	for other_id in rack.get_string_vec("otherIds"):	
		rack_per_other_id[other_id]=rack

dsn_tns = cx_Oracle.makedsn('atlr-s.cern.ch', '10121', service_name='atlas_glance.cern.ch')
home_dir=os.path.expanduser("~")
with open(home_dir+"/private/akey","r") as key_file:
	key=key_file.read()
	f = Fernet(key)
	saltpassword=f.decrypt(bytes(config.Configuration("oksconfig:../../../data/sb.data.xml").get_obj('MetaInfo','Singleton').get_string("atlas_infrastructure_pub"),'utf-8')).decode('utf-8')
	password=saltpassword.replace(getpass.getuser(),'')

conn = cx_Oracle.connect(user='atlas_infrastructure_pub', password=password, dsn=dsn_tns)

cur = conn.cursor()
logger.debug("Getting the racks from ACES...")	
logger.warning("The information of the next racks in ACES is considered less accurate than the one in the Expert System: %s"%racks_to_be_corrected_in_aces)
#query=cur.execute("select * from ATLAS_INFRASTRUCTURE.ATCN_RACK_INFO where LABEL="+racks[0]+" OR LABEL=".join(racks))
query=cur.execute("select * from ATLAS_INFRASTRUCTURE.ATCN_RACK_INFO")
aces_racks=query.fetchall()
#result[0]:ACES_id
#result[1]:LABEL
#result[2]:Usage
#result[3]:Subsystem
#result[4]:Location
#result[5]:height
#result[6]:width
#result[7]:weight?
#result[8]:orientation
#result[9]:responsible persons separated with ;
#result[10]:responsible groups separated with ;
for aces_rack in aces_racks:
	if aces_rack[1] in racks_to_be_corrected_in_aces: continue
	if aces_rack[1] in rack_obj_per_uid:
		subsystem=aces_rack[3].lower().replace("; ","-") if aces_rack[3] else '' 
		subsystem=subsystem if not subsystem in subsystems_maps else subsystems_maps[subsystem].lower()
		if not rack_obj_per_uid[aces_rack[1]]['subsystem'].lower()==subsystem:
			logger.info("Updating the subsytem in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[3],rack_obj_per_uid[aces_rack[1]]['subsystem']))
			rack_obj_per_uid[aces_rack[1]]['subsystem']=str(aces_rack[3]).replace("; ","-") if aces_rack[3].lower() not in subsystems_maps else subsystems_maps[aces_rack[3].lower()]
			logger.info("Updating the description in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[2],rack_obj_per_uid[aces_rack[1]]['description']))
			rack_obj_per_uid[aces_rack[1]]['description']=str(aces_rack[2])
		if len(rack_obj_per_uid[aces_rack[1]]['description'])==0:
			rack_obj_per_uid[aces_rack[1]]['description']=str(aces_rack[2])
		if not rack_obj_per_uid[aces_rack[1]]['location'].lower()==aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 ").lower(): 
			logger.info("Updating the location in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 "),rack_obj_per_uid[aces_rack[1]]['location']))
			rack_obj_per_uid[aces_rack[1]]['location']=aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 ")
	elif aces_rack[1] in rack_per_other_id:
		subsystem=aces_rack[3].lower().replace("; ","-") if aces_rack[3] else '' 
		subsystem=subsystem if not subsystem in subsystems_maps else subsystems_maps[subsystem].lower()
		if not rack_per_other_id[aces_rack[1]]['subsystem'].lower()==subsystem:
			logger.info("Updating the subsytem in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[3],rack_per_other_id[aces_rack[1]]['subsystem']))
			rack_per_other_id[aces_rack[1]]['subsystem']=str(aces_rack[3]).replace("; ","-") if aces_rack[3].lower() not in subsystems_maps else subsystems_maps[aces_rack[3].lower()]
			logger.info("Updating the description in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[2],rack_per_other_id[aces_rack[1]]['description']))
			rack_per_other_id[aces_rack[1]]['description']=str(aces_rack[2])
		if len(rack_per_other_id[aces_rack[1]]['description'])==0:
			rack_per_other_id[aces_rack[1]]['description']=str(aces_rack[2])
		if not rack_per_other_id[aces_rack[1]]['location'].lower()==aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 ").lower(): 
			logger.info("Updating the location in rack %s ACES:%s ==> ES:%s"%(aces_rack[1],aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 "),rack_per_other_id[aces_rack[1]]['location']))
			rack_per_other_id[aces_rack[1]]['location']=aces_rack[4].replace("UX ","UX15 ").replace("SDX ","SDX1 ")
	elif 'Y.' in aces_rack[1] or 'SGX1.' in  aces_rack[1]:
		logger.warning("The rack %s is not registered in the Expert System"%aces_rack[1])
		continue
cur.close()
conn.close()
db.commit()

