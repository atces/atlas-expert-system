# load the contact persons from a file and add them to the alarms
# florian.haslbeck@cern.ch
# November 2024


import config

database = "../../../data/sb.data.xml"
db=config.Configuration("oksconfig:%s"%database)

with open("../../data/alarm_contact_persons.txt", "r") as f:
    
    for l in f:
        if l.startswith("#"):
            continue

        print(l.split("&")[0], len(l.split("@")))

        alarm,  _, _, reportTo, reportTo_nonworkinghours, _ = l.split("@")

        alarm = alarm.strip()
        reportTo = reportTo.strip()
        # remove any " or ' from the string"
        reportTo = reportTo.replace('"', "").replace('"', "")

        reportTo_nonworkinghours = reportTo_nonworkinghours.strip()
        reportTo_nonworkinghours = reportTo_nonworkinghours.replace('"', "").replace('"', "")


        

        alarm_obj = db.get_obj("Alarm", alarm)
        if alarm_obj is None:
            print(f"Alarm {alarm} not found")
            continue
        else:
            print(f"Found {alarm}")
            alarm_obj["reportTo"] = reportTo
            alarm_obj["reportTo_nonworkinghours"] = reportTo_nonworkinghours

            print(f"-> {reportTo}")
            print(f"-> {reportTo_nonworkinghours} ")

db.commit()