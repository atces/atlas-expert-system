#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))



    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasHelper()
    hp_gas.setDb(db)






	racks=["Y.35-04.X7_supply","Y.36-04.X7_return ",
	           "Y.35-03.X1_supply","Y.36-03.X1_return",
	           "Y.51-24.X7_supply","Y.52-24.X7_return",
	           "Y.52-25.X1_supply","Y.53-25.X1_return",]
    for ec in ecs:
        ec=db.get_obj("SubDetector",ec.UID())
        for rack in racks:
