#!/usr/bin/env python
# every delayed action 



class Alarm:
    def __init__(self, uid, digitalInputs,delayeActions,description,v=False):
        self.digitalInputs = digitalInputs
        self.delayeActions = delayeActions
        self.description = description

        if v==True: print("AL created %s with %i DIs and %i DAs" % (uid, len(digitalInputs),len(delayeActions)))
        pass
    pass

class DigitalInput:
    def __init__(self, uid,v=False):
        self.uid=uid
        self.alarms = []
        if v==True: print("DI created %s with %i ALs" % (uid, len(alarms)))
        pass
    def addAlarm(self,alarm):
        if alarm not in self.alarms: self.alarms.append(alarm)
    pass

class DelayedAction:
    def __init__(self, uid, actions,alarms,v=False):
        self.actions = actions
        self.alarms = alarms
        if v==True: print("DA created %s with %i ACs and %i ALs" % (uid, len(actions), len(alarms)))
        pass
    pass


def existsInAESNotinDSS(alarms_db_o, alarms_f_o):
    list_InDatabaseNotinDSS=[]
    counter=0
    s_alarms_db_o = sorted(alarms_db_o)
    for a in s_alarms_db_o:
        counter+=1
        #print("%i\t%s" % (counter,a))
        if a not in alarms_f_o:
            list_InDatabaseNotinDSS.append(a)
            print("%s is in database but not in DSS. To be deleted" % a)
    return list_InDatabaseNotinDSS

def existsInDSSNotinAES(alarms_db_o, alarms_f_o):
    list_InDSSNotinAES=[]
    counter=0
    s_alarms_f_o = sorted(alarms_f_o)
    for a in s_alarms_f_o:
        counter+=1
        #print("%i\t%s" % (counter,a))
        if a not in alarms_db_o:
            list_InDSSNotinAES.append(a)
            print("%s is in DSS but not in Expert System. To be added to Expert System" % a)
    return list_InDSSNotinAES

def compareAcionsInAlarms_existsInAESNotinDSS(alarms_db_o, alarms_f_o):
    s_alarms_db_o = sorted(alarms_db_o)
    counter=0
    for alarm in alarms_db_o:
        #print("alarms_db_o[alarm].delayeActions")
        #print(alarms_db_o[alarm].delayeActions)
        #print("alarms_f_o[alarm].delayeActions")
        #print(alarms_f_o[alarm].delayeActions)
        for da in alarms_db_o[alarm].delayeActions:
            #print(da.rsplit('_', 1)[0])
            #da=da.rsplit('_', 1)[0]
            if da not in alarms_f_o[alarm].delayeActions:
                counter+=1
                print("Alarm %s in AES has %s delayedaction that is not in DSS. This action should be removed in AES" % (alarm, da))
    return counter
def compareAcionsInAlarms_existsInDSSNotinAES(alarms_db_o, alarms_f_o):
    s_alarms_db_o = sorted(alarms_db_o)
    counter=0
    for alarm in alarms_f_o:
        #print("alarms_db_o[alarm].delayeActions")
        #print(alarms_db_o[alarm].delayeActions)
        #print("alarms_f_o[alarm].delayeActions")
        #print(alarms_f_o[alarm].delayeActions)
        for da in alarms_f_o[alarm].delayeActions:
            #print(da.rsplit('_', 1)[0])
            #da=da.rsplit('_', 1)[0]
            if da not in alarms_db_o[alarm].delayeActions:
                counter+=1
                print("Alarm %s in DSS has %s delayedaction that is not in AES. This action should be added to the AES" % (alarm, da))
    return counter

def compareDIsExistInDSSNotinAES(digitalinputs_db_o,digitalinputs_f_o):
    counter=0
    for di in digitalinputs_f_o:
        if di not in digitalinputs_db_o:
            counter+=1
            #print(di)
            #print(di.alarms)
            #print("\t%s missing in AES " % (di))
            for a in digitalinputs_f_o[di].alarms:
                print("\t\t%s missing for %s in AES " % (di,a))
    return counter

def compareDIsExistInAESNotinDSS(digitalinputs_db_o,digitalinputs_f_o):
    counter=0
    for di in digitalinputs_db_o:
        if di not in digitalinputs_f_o:
            # Dss output file doesn't contain inputs that are not associated to any alarm. Therefore next line will print also inputs that don't have alarm, even if they are present in DSS 
            #print("\t%s exits in AES but not in DSS. Should be removed from the AES " % (di))# print this will show inputs that don't have alarm
            for a in digitalinputs_db_o[di].alarms:
                counter+=1
                print("\t%s for %s exits in AES but not in DSS. Should be removed from the AES " % (di,a))
    return counter

if __name__ == '__main__':

    import os
    import sys
    sys.path.append("../")
    import argparse
    import config
    #import helper
    
    #import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    
    #import classes from loadhelpers
    parser = argparse.ArgumentParser()
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()
    
    limit=5000
    dss_file="AlarmList.txt"
    database_file="/eos/home-a/atopes/www/atlas-expert-system-dev/data/sb.data.xml"
    v=args.verbose
    db = config.Configuration("oksconfig:%s"%(database_file))    
    helper=helper.Helper(db=db)
    _classes= db.classes()
    
    alarms_db=db.get_objs("Alarm")
    digitalInputs_db=db.get_objs("DigitalInput")

    counter, more, less=0,0,0
    alarms_db_o,alarms_f_o={},{}
    digitalinputs_db_o,digitalinputs_f_o={},{}

    print("Reading data in AES database in: %s" % database_file)
    for al in alarms_db:
        digitalInputs=[]
        delayeActions=[]
        obj_desc=""
        counter+=1
        if counter >limit: break
        if v:print("%i\t%s" % (counter,al.UID()))
        objs_di= al.get_objs("requiresDigitalInput")
        objs_da= al.get_objs("actions")
        obj_desc= al["description"]
        for di in  objs_di:
            #print("\t%s" % di.UID())
            digitalInputs.append(di.UID())
        for da in  objs_da:
            #print("\t%s" % da.UID())
            delayeActions.append(da.UID())
        alarms_db_o[al.UID()]=Alarm(al.UID(),digitalInputs,delayeActions,obj_desc,v)
        pass
    pass

    for di in digitalInputs_db:
        for al in di.get_objs("triggers"):
            if di.UID() not in digitalinputs_db_o: digitalinputs_db_o[di.UID()] = DigitalInput(di.UID(),v)
            digitalinputs_db_o[di.UID()].addAlarm(al.UID())
            pass
        pass
    print("Done reading database")
    print("Reading file from DSS: %s" % dss_file)

    with open(dss_file , 'r') as file:
        counter=0
        data="".join(line for line in file)
        alarms_text=data.split("===========================================================")
        #print (len(alarms_text))
        for block in alarms_text:
            if counter > limit: break
            al_name=""
            digitalInputs, ac_names, delayeActions=[],[],[]
            prevline,obj_desc="",""
            for line in block.split("\n"):
                #print (line)
                
                if line.startswith("ALARM  AL_"):
                    al_name= line.split(" ")[2]
                    pass
                if " DI_" in line or " AI_" in line or " PT_" in line:
                    DIname=line.strip().split(" ")[0]
                    digitalInputs.append(DIname)
                    if DIname not in digitalinputs_f_o: 
                        digitalinputs_f_o[DIname]=DigitalInput(DIname,v)
                    digitalinputs_f_o[DIname].addAlarm(al_name)
                    pass
                if " O_" in line:
                    delayeActions.append(line.strip().split(" ")[0]+"_"+line.strip().split("(delay ")[1].split(")")[0])
                    #print (line.strip().split(" ")[0],"--",line.strip().split(" "))[1]
                    pass
                if "Description:" in prevline and "(optional)" not in line:
                    obj_desc=line
                    pass
                prevline=line
            if "Send email to::" in block:
                start="Send email to::\n"
                end="\nEmail Subject:"
                s_mail=block[block.find(start)+len(start):block.rfind(end)]
            else:
                s_mail=""
            #SMS
            if "Send SMS " in block:
                start="to:\n"
                end="\n\n"
                s_SMS=block[block.find(start)+len(start):block.rfind(end)].split("\n",1)[1]
            else:
                s_SMS=""
            #print(s_mail)
            #print(s_SMS)    
            if len(al_name)>4: 
                counter+=1
                alarms_f_o[al_name]=Alarm(al_name,digitalInputs,delayeActions,v)


    print("Done reading file")
    print("")
    print("Number of alarms in AES:  \t%i" % len(alarms_db_o))
    print("Number of alarms in DSS:\t%i" % len(alarms_f_o))
    print("Number of Inputs in AES:  \t%i" % len(digitalinputs_db_o))
    print("Number of Inputs in DSS:\t%i" % len(digitalinputs_f_o))
    print("")
    print("Comparing...")
    AlarmExistsInAESNotinDSS=existsInAESNotinDSS(alarms_db_o, alarms_f_o)
    AlarmExistsInDSSNotinAES= existsInDSSNotinAES(alarms_db_o, alarms_f_o)
    print("%s alarms in AES that are not present in DSS. Should be removed from AES datbase" % len(AlarmExistsInAESNotinDSS))
    print("%s alarms in DSS that are not present in AES. Should be added to AES" % len(AlarmExistsInAESNotinDSS))
    nActionsExistsInAESNotinDSS=compareAcionsInAlarms_existsInAESNotinDSS(alarms_db_o, alarms_f_o)
    nActionsExistsInDSSNotinAES=compareAcionsInAlarms_existsInDSSNotinAES(alarms_db_o, alarms_f_o)
    if nActionsExistsInAESNotinDSS!=0: 
        print("%i actions in alarms of AES are present in actions in alarms of DSS" % nActionsExistsInAESNotinDSS)
    else:
        print("All the actions in alarms of AES are present in actions in alarms of DSS")
    if nActionsExistsInDSSNotinAES!=0: 
        print("%i actions in alarms of DSS are present in actions in alarms of AES" % nActionsExistsInDSSNotinAES)
    else:
        print("All the actions in alarms of DSS are present in actions in alarms of AES")
    

    
    nDIsExistInDSSNotinAES= compareDIsExistInDSSNotinAES(digitalinputs_db_o,digitalinputs_f_o)
    if nDIsExistInDSSNotinAES!=0:
        print("%i inputs (DI,AI,PT) (that are associated with at least one alarm) found in DSS but not in AES. Should be added to AES" % nDIsExistInDSSNotinAES)
    else:
        print("All inputs (DI,AI,PT) (that are associated with at least one alarm) found in DSS are in AES")

    nDIsExistInAESNotinDSS= compareDIsExistInAESNotinDSS(digitalinputs_db_o,digitalinputs_f_o)
    if nDIsExistInAESNotinDSS!=0:
        print("%i inputs (DI,AI,PT) (that are associated with at least one alarm) found in AES but not in DSS. Should be removed from AES" % nDIsExistInAESNotinDSS)
    else:
        print("All inputs (DI,AI,PT) (that are associated with at least one alarm) found in AES are in DSS")
    print("Done")
    pass
    
