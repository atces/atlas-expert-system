import config
import networkx as nx
import loadhelpers
from graphhelper import GraphHelper

if __name__ == "__main__":
	db=config.Configuration("oksconfig:/afs/cern.ch/user/g/guribego/git/atlas-expert-system/data/sb.data.xml")
	gLH = loadhelpers.LoadHelpers(db)
	classes=gLH.getClasses()
	searchableClasses=gLH.getSearchableClasses()
	sysclasses=gLH.getSysClasses()
	helpers=gLH.getHelpers()
	graph_helper=GraphHelper(None,None,helpers,classes)
	graph_helper.create_graph(db)
	graph_helper.generate_feeding_graph()
	uids=[
	"AL_COL_BeamPipe_VJA_CoolingNotRunning",	
	"AL_COL_BeamPipe_VJC_CoolingNotRunning",	
	"AL_COL_IBL_CO2_Vacuum_Interlocked",	
	"AL_COL_IBL_CO2_Vacuum_Low",	
	"AL_COL_IBL_CO2_Vacuum_Stopped",	
	"AL_COL_LAR_CoolingFailure",	
	"AL_COL_LAR_RodGlink_CoolingFailure",	
	"AL_COL_MUN_CoolingFailure_SideA",	
	"AL_COL_MUN_CoolingFailure_SideC",	
	"AL_COL_MUN_StationA_Loop1_Stopped",	
	"AL_COL_MUN_StationA_Loop2_Stopped",	
	"AL_COL_MUN_StationA_Loop3_Stopped",	
	"AL_COL_MUN_StationA_Loop4_Stopped",	
	"AL_COL_MUN_StationA_Loop5_Stopped",	
	"AL_COL_MUN_StationA_Loop6_Stopped",	
	"AL_COL_MUN_StationA_Loop7_Stopped",	
	"AL_COL_MUN_StationA_Loop8_Stopped",	
	"AL_COL_MUN_StationA_Loop9_Stopped",	
	"AL_COL_MUN_StationA_Loop10_Stopped",	
	"AL_COL_MUN_StationA_Loop11_Stopped",	
	"AL_COL_MUN_StationA_Loop12_Stopped",	
	"AL_COL_MUN_StationA_Loop13_Stopped",	
	"AL_COL_MUN_StationA_Loop14_Stopped",	
	"AL_COL_MUN_StationA_Loop15_Stopped",	
	"AL_COL_MUN_StationC_Loop1_Stopped",	
	"AL_COL_MUN_StationC_Loop2_Stopped",	
	"AL_COL_MUN_StationC_Loop3_Stopped",	
	"AL_COL_MUN_StationC_Loop4_Stopped",	
	"AL_COL_MUN_StationC_Loop5_Stopped",	
	"AL_COL_MUN_StationC_Loop6_Stopped",	
	"AL_COL_MUN_StationC_Loop7_Stopped",	
	"AL_COL_MUN_StationC_Loop8_Stopped",	
	"AL_COL_MUN_StationC_Loop9_Stopped",	
	"AL_COL_MUN_StationC_Loop10_Stopped",	
	"AL_COL_MUN_StationC_Loop11_Stopped",	
	"AL_COL_MUN_StationC_Loop12_Stopped",	
	"AL_COL_MUN_StationC_Loop13_Stopped",	
	"AL_COL_TIL_CoolingFailure",	
	"AL_CRY_LAR_FeedthroughTempLow",	
	"AL_INF_RackCoolingFailure_SDX1",	
	"AL_INF_RackCoolingFailure_USA15"]
	#uids=["CAM-CR-EOZ-TE14-01","CAM-CR-EOZ-TE16-01","CAM-CR-SCR-IDG-01"]
	#uids=["CAM-CR-SCX-L0-02","PLC-SISMIC-SCX-01"]
	#print("The alarms trigged are:%s"%uids)
	print("The systems affected are:%s"%uids)
	MPC=graph_helper.find_MPC(db,uids,max_trys=64,exhaustive_mode="Alarm")
	print("The most probable causes are: %s"%MPC)
