#!/usr/bin/env python

if __name__ == '__main__':

     import os
     import sys
     import config
     import argparse

     import alarm
     import action
     import dsu
     import digitalinput

     parser = argparse.ArgumentParser()
     parser.add_argument("database", help="database file")
     parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
     parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
     args=parser.parse_args()

     print "Load database: %s" % args.database
     db = config.Configuration("oksconfig:%s"%(args.database))



     hpdi = digitalinput.DigitalInputHelper()
     hpdi.setDb(db)
     hpdsu = dsu.DSUHelper()
     hpdsu.setDb(db)

     uids=[["AI_COL_BeamPipe_VA_CoolingCirquitPressure","A","DSU3","upper","4","5","1"],
     ["AI_INF_US15_UX15_PressureDifference","A","DSU3","upper","4","4","0"],
     ["AI_INF_USA15_UX15_PressureDifference","A","DSU3","upper","4","3","0"],
     ["AI_INF_UX15A_LHCtunnelS8_PressureDifference","A","DSU3","upper","4","1","0"],
     ["AI_INF_UX15C_LHCtunnelS2_PressureDifference","A","DSU3","upper","4","2","0"],
     ["AI_MAG_Toroid_Current","A","DSU5","upper","4","1","1"],
     ["AI_VEN_Pressure_PX15_USA15","A","DSU1","upper","9","1","1"],
     ["AI_VEN_Pressure_UX15_USA15","A","DSU1","upper","9","2","0"],
     ["AI_VEN_GasExtraction_UX15","A","DSU1","upper","9","3","0"],
     ["AI_VEN_UX15_ArgonExtraction","A","DSU1","upper","9","4","0"],
     ["AI_VEN_USA15_GasExtraction","A","DSU1","upper","9","5","0"],
     ["AI_VEN_USA15_Extraction","A","DSU1","upper","9","6","0"],
     ["AI_VEN_UX15_AbsolutePressure","A","DSU1","upper","9","7","0"],
     ["AI_VEN_Pressure_USA15_GazRoom","A","DSU1","upper","9","8","0"],
     ["AI_VEN_USA15_AbsolutePressure","A","DSU1","lower","10","1","0"],
     ["AI_VEN_UX15_InletFlow_1","A","DSU1","lower","10","2","0"],
     ["AI_VEN_UX15_InletFlow_2","A","DSU1","lower","10","3","0"],
     ["AI_INF_CompressedAir_ReservoirPressure_SH1","A","DSU1","lower","10","6","1"]]

     ptuids=[
     ["PT_CRY_RackTempHigh_Y0521A2","A","DSU2","upper","10","1","1"],
     ["PT_CRY_RackTempHigh_Y0621A2","A","DSU2","upper","10","2","1"],
     ["PT_CRY_RackTempHigh_Y0721A2","A","DSU2","upper","10","3","1"],
     ["PT_INF_TDQ_SDX1_PT1_Level1","A","DSU1","upper","10","2","1"],
     ["PT_INF_TDQ_SDX1_PT1_Level2","A","DSU1","upper","10","3","1"],
     ["PT_INF_TDQ_SDX1_PT2_Level1","A","DSU1","upper","10","4","1"],
     ["PT_INF_TDQ_SDX1_PT2_Level2","A","DSU1","upper","10","1","1"],
     ["PT_LUC_Temp_Probe13_SideA","A","DSU2","upper","10","6","1"],
     ["PT_LUC_Temp_Probe13_SideC","A","DSU2","upper","5","1","1"],
     ["PT_LUC_Temp_Probe1_SideA","A","DSU2","upper","10","4","1"],
     ["PT_LUC_Temp_Probe1_SideC","A","DSU2","upper","10","7","1"],
     ["PT_LUC_Temp_Probe5_SideA","A","DSU2","upper","10","5","1"],
     ["PT_LUC_Temp_Probe5_SideC","A","DSU2","upper","10","8","1"],
     ["PT_MUN_TGC_HeaterTemp_BWA_LowerGasRack","A","DSU5","lower","10","5","1"],
     ["PT_MUN_TGC_HeaterTemp_BWA_LowerGasRack_T2","A","DSU5","lower","10","8","1"],
     ["PT_MUN_TGC_HeaterTemp_BWC_LowerGasRack","A","DSU5","lower","10","6","1"],
     ["PT_MUN_TGC_HeaterTemp_BWC_LowerGasRack_T2","A","DSU5","lower","10","7","1"],
     ["PT_MUN_TGC_Temperature1_BigWheelTrenchA","A","DSU5","lower","10","3","1"],
     ["PT_MUN_TGC_Temperature1_BigWheelTrenchC","A","DSU5","lower","10","1","1"],
     ["PT_MUN_TGC_Temperature2_BigWheelTrenchA","A","DSU5","lower","10","4","1"],
     ["PT_MUN_TGC_Temperature2_BigWheelTrenchC","A","DSU5","lower","10","2","1"]
     ]
    #Item, Type, DSU, Crate, Module, Slot, n. Alarms
     for l in ptuids:
          print "------------"
          n_name=l[0]
          n_dsu=l[2]
          n_crate=l[3]
          n_slot=l[4]
          n_channel=l[5]
          n_module=l[5]

          db_dsu=db.get_obj("DSU","DSU_%s"%n_dsu[3])
          #print db_dsu
          o_dsu={
          "UID": db_dsu.UID(),
          "digitalInput":n_name
          }
          
          #print o_dsu
          ac_di = {
            "UID": n_name,
            "DSU": db_dsu.UID(),
            "crate": n_crate,
            "module": n_module,
            "slot": n_channel,
            "channel": n_channel
          }
          hpdi.addObj(ac_di,False,True)
          print "--"
          hpdsu.addObj(o_dsu,False,True)
          #print ac_di
          pass
     db.commit("")
     pass
