#!/usr/bin/env python
#############################################
#
# Carlos.Solans@cern.ch
# July 2017
#############################################

import os
import sys
import config
import argparse

parser = argparse.ArgumentParser("check delayed actions")
parser.add_argument("database",help="database file")
parser.add_argument("-u","--uid",help="UID")
parser.add_argument("-y","--yeah",action="store_true",help="Not a dry run")
args=parser.parse_args()

print "Load database: %s" % args.database
db=config.Configuration("oksconfig:%s"%(args.database))

delayedactions = db.get_objs("DelayedAction")

for delayedaction in delayedactions:
    print "Process delayedaction: %s" % delayedaction.UID()
    action = delayedaction.get_obj("action")
    print " Action: %s" % action.UID()
    delayedactions2 = action.get_objs("delayedactions")
    delayedactions2obj = []
    delayedactionfound = False
    for delayedaction2 in delayedactions2:
        delayedactions2obj.append(db.get_obj("DelayedAction",delayedaction2.UID()))
        if delayedaction2.UID()==delayedaction.UID(): delayedactionfound=True
        pass
    if not delayedactionfound:
        delayedactions2obj.append(db.get_obj("DelayedAction",delayedaction.UID()))
        print " Append delayedactions: ", delayedactions2obj
        actionobj = db.get_obj("Action",action.UID())
        actionobj.set_objs("delayedactions",delayedactions2obj)
        pass
    pass
    
if args.yeah:
    print "Commit changes"
    db.commit()
    pass

print ("Have a nice day")
