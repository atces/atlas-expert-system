#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# adds a relation in an OKS object guaranteeing uniqueness


import argparse
import config

def add_oks_relation_without_duplication(obj,relation,obj_to_add):
	if obj_to_add.UID() not in [_obj.UID() for _obj in obj[relation]]: obj[relation]+=[obj_to_add] 

def get_or_create_oks_obj(db,clazz,uid): #Move to a lib of functions?
	return db.get_obj(clazz,uid) if db.test_object(clazz,uid) else db.create_obj(clazz,uid)
	

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("clazz_s", help="UID of the subject ", required=True)
	parser.add_argument("uid_s", help="Class of the subject", required=True)
	parser.add_argument("relation", help="realtion", required=True)
	parser.add_argument("clazz_o", help="Class of the object", required=True)
	parser.add_argument("uid_o", help="UID of the object ", required=True)
	parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)
	add_oks_relation_without_duplication(db.get_obj(args.clazz_s,args.uid_s),args.relation,db.get_obj(args.clazz_o,args.uid_o))
	db.commit('')	
