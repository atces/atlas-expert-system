#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re
    import group
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import rack
    import pneumaticcontroller

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_r = rack.RackHelper()
    hp_r.setDb(db)
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)
    hp_pc=pneumaticcontroller.PneumaticControllerHelper()
    hp_pc.setDb(db)
    
    uids=[["PL-Tile_PCVA2","PL-Tile_PVA2", "Tile_CoolingLoop_EBA_1L"],
    ["PL-Tile_PCVA3","PL-Tile_PVA3", "Tile_CoolingLoop_EBA_2L"],
    ["PL-Tile_PCVA4","PL-Tile_PVA4", "Tile_CoolingLoop_EBA_3L"],
    ["PL-Tile_PCVA5","PL-Tile_PVA5", "Tile_CoolingLoop_EBA_1R"],
    ["PL-Tile_PCVA6","PL-Tile_PVA6", "Tile_CoolingLoop_EBA_2R"],
    ["PL-Tile_PCVA7","PL-Tile_PVA7", "Tile_CoolingLoop_EBA_3R"],
    ["PL-Tile_PCVA8","PL-Tile_PVA8", "Tile_CoolingLoop_EBC_1L"],
    ["PL-Tile_PCVA9","PL-Tile_PVA9", "Tile_CoolingLoop_EBC_2L"],
    ["PL-Tile_PCVA10","PL-Tile_PVA10", "Tile_CoolingLoop_EBC_3L"],
    ["PL-Tile_PCVA11","PL-Tile_PVA11", "Tile_CoolingLoop_EBC_1R"],
    ["PL-Tile_PCVA12","PL-Tile_PVA12", "Tile_CoolingLoop_EBC_2R"],
    ["PL-Tile_PCVA13","PL-Tile_PVA13", "Tile_CoolingLoop_EBC_3R"],
    ["PL-Tile_PCVA14","PL-Tile_PVA14", "Tile_CoolingLoop_LBA_1L"],
    ["PL-Tile_PCVA15","PL-Tile_PVA15", "Tile_CoolingLoop_LBA_2L"],
    ["PL-Tile_PCVA16","PL-Tile_PVA16", "Tile_CoolingLoop_LBA_3L"],
    ["PL-Tile_PCVA17","PL-Tile_PVA17", "Tile_CoolingLoop_LBA_1R"],
    ["PL-Tile_PCVA18","PL-Tile_PVA18", "Tile_CoolingLoop_LBA_2R"],
    ["PL-Tile_PCVA19","PL-Tile_PVA19", "Tile_CoolingLoop_LBA_3R"],
    ["PL-Tile_PCVA20","PL-Tile_PVA20", "Tile_CoolingLoop_LBC_1L"],
    ["PL-Tile_PCVA21","PL-Tile_PVA21", "Tile_CoolingLoop_LBC_2L"],
    ["PL-Tile_PCVA22","PL-Tile_PVA22", "Tile_CoolingLoop_LBC_3L"],
    ["PL-Tile_PCVA23","PL-Tile_PVA23", "Tile_CoolingLoop_LBC_1R"],
    ["PL-Tile_PCVA24","PL-Tile_PVA24", "Tile_CoolingLoop_LBC_2R"],
    ["PL-Tile_PCVA25","PL-Tile_PVA25", "Tile_CoolingLoop_LBC_3R"]]
    '''
    for x in range(1,26):
        print "[\"PL-LAr_PCVA%i\",\"PL-LAr_PVA%i\", \"\"]" % (x,x)
        pass
    '''
    group="Valves_Tile"
    o_pc={
        "UID":"FCTIR_00020_PC_Tile",
        }
    hp_pc.addObj(o_pc, False, True)
    for u in uids:
        o_g={"UID":group}
        hp_g.addObj(o_g, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva={
            "UID":u[0],"controlsByAirTo":u[2], "controlledByAirFrom":"FCTIR_00020_PC_Tile", "groupedBy":group
        }
        hp_pv.addObj(o_pva, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":u[1],"controlsByAirTo":u[2], "controlledByAirFrom":"FCTIR_00020_PC_Tile", "groupedBy":group
        }
        hp_pv.addObj(o_pva2, False, True)
        #pneumactic cont.
        o_pc={
            "UID":"FCTIR_00020_PC_Tile",
            "controlsByAirTo":u[0],
        }
        hp_pc.addObj(o_pc, False, True)
        #pneumactic cont.
        o_pc={
            "UID":"FCTIR_00020_PC_Tile",
                "controlsByAirTo":u[1],
                }
        hp_pc.addObj(o_pc, False, True)
        #both Coolingloop controlledbyPVA
        o_cl={
            "UID":u[2],"controlledByAirFrom":u[0]
        }
        hp_cl.addObj(o_cl, False, True)
        #add it one
        o_cl={
            "UID":u[2],"controlledByAirFrom":u[1]
        }
        hp_cl.addObj(o_cl, False, True)
        #grouping
        o_g={"UID":group, "groupTo":u[0]}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":group, "groupTo":u[1]}
        hp_g.addObj(o_g, False, True)
        pass
    db.commit("")
