#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hp1 = digitalinput.DigitalInputHelper()
    hp1.setDb(db)
    hp2 = alarm.AlarmHelper()
    hp2.setDb(db)

    # f = open("MissingDigitalInputs.txt", "w")
    with open("AlarmListFull.txt", 'r') as file:
        try:
            line = next(file)
            while line:
                line = next(file)
                if line.startswith("====="):
                    name_line = next(file).split()
                    name = name_line[1]
                    found = False
                    for a in alarms_db:
                        if a.UID() != "AL_INF_Power_SDX1_EOD1_UPSFailure": continue
                        if a.UID() == name:
                            print "ALARM ------------ %s" % name
                            while line:
                                line = next(file)
                                if "ALARM CONDITION" in line:
                                    print "####AC"
                                    di_line = next(file).strip()
                                    while not di_line.startswith("ACTION") and not line.startswith("NO ACTION"):
                                        if di_line.startswith("DI_"):
                                            di_line_split = di_line.split()
                                            di = di_line_split[0]
                                            persistency = di_line_split[len(di_line_split)-1].replace(")","")
                                            print di
                                            obj1 = {
                                                "UID": di,
                                                "triggers": a.UID(),
                                                "persistency": int(persistency)
                                            }
                                            hp1.addObj(obj1,False,True)
                                            obj2 = {
                                                "UID": a.UID(),
                                                "digitalInput": di
                                            }
                                            hp2.addObj(obj2,False,True)
                                        di_line = next(file).strip()
                                    print "WHILE ENDED"
                                    break
                            found = True
                            break
                    # if not found:
                    #     f.write("%s\n" % name)
        except StopIteration:
            file.close()
    db.commit("")
    
