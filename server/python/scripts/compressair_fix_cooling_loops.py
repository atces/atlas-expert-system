#!/usr/bin/env python
# Ignacio.asensi@cern.ch
#
if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import pneumaticvalve
    import pneumaticcontroller
    import group
    
    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))



    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_pc = pneumaticcontroller.PneumaticControllerHelper()
    hp_pc.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_gr = group.GroupHelper()
    hp_gr.setDb(db)
    loops=[["MUN_StationA_Loop1","01","20"],
       ["MUN_StationA_Loop2","02","21"],
       ["MUN_StationA_Loop3","03","22"],
       ["MUN_StationA_Loop4","04","23"],
       ["MUN_StationA_Loop5","05","24"],
       ["MUN_StationA_Loop6","06","25"],
       ["MUN_StationA_Loop7","07","26"],
       ["MUN_StationA_Loop8","08","27"],
       ["MUN_StationA_Loop9","09","28"],
       ["MUN_StationA_Loop10","10","29"],
       ["MUN_StationA_Loop11","11","30"],
       ["MUN_StationA_Loop12","12","31"],
       ["MUN_StationA_Loop13","13","32"],
       ["MUN_StationC_Loop1","01","20"],
       ["MUN_StationC_Loop2","02","21"],
       ["MUN_StationC_Loop3","03","22"],
       ["MUN_StationC_Loop4","04","23"],
       ["MUN_StationC_Loop5","05","24"],
       ["MUN_StationC_Loop6","06","25"],
       ["MUN_StationC_Loop7","07","26"],
       ["MUN_StationC_Loop8","08","27"],
       ["MUN_StationC_Loop9","09","28"],
       ["MUN_StationC_Loop10","10","29"],
       ["MUN_StationC_Loop11","11","30"],
       ["MUN_StationC_Loop12","12","31"],
       ["MUN_StationC_Loop13","13","32"]]
    '''
    for loop in loops:
        l=db.get_obj("CoolingLoop", loop[0])
        side=loop[0][11]
        print l.UID()
        obj_pv1={
            "UID":"Muon_Station%s_PVA%s" % (side, loop[1]),
            "controlsByAirTo": l.UID(),
            "controlledByAirFrom":"FCTIR_00020_PC_Muon_Station%s" % side,
        }
        hp_pv.addObj(obj_pv1, False, True)
        obj_pv2={
            "UID":"Muon_Station%s_PVA%s" % (side, loop[2]),
            "controlsByAirTo": l.UID(),
            "controlledByAirFrom":"FCTIR_00020_PC_Muon_Station%s" % side,
        }
        hp_pv.addObj(obj_pv2, False, True)
        obj_pc={
            "UID":"FCTIR_00020_PC_Muon_Station%s" % side,
            "controlsByAirTo": "Muon_Station%s_PVA%s" % (side, loop[1]),
        }
        hp_pc.addObj(obj_pc, False, True)
        obj_pc={
            "UID":"FCTIR_00020_PC_Muon_Station%s" % side,
            "controlsByAirTo": "Muon_Station%s_PVA%s" % (side, loop[2]),
        }
        hp_pc.addObj(obj_pc, False, True)
        obj_loop={
            "UID": l.UID(),
            "controlledByAirFrom":"Muon_Station%s_PVA%s" %  (side, loop[1]),
            "groupedBy":"MUN_Station%s_loops" % side
        }
        hp_cl.addObj(obj_loop, False, True)
        obj_loop={
            "UID": l.UID(),
            "controlledByAirFrom": "Muon_Station%s_PVA%s" % (side, loop[2]),
        }
        hp_cl.addObj(obj_loop, False, True)
        obj_gr={
            "UID":"MUN_Station%s_loops" % side,
            "groupTo":loop[0]
        }
        hp_gr.addObj(obj_gr, False, True)
        pass
    '''
    for loop in loops:
        l=db.get_obj("CoolingLoop", loop[0])
        side=loop[0][11]
        print l.UID()
        obj_gr={
            "UID":"MUN_Station%s_PVAs" % side,
            "groupTo":"Muon_Station%s_PVA%s" % (side, loop[1]),
        }
        hp_gr.addObj(obj_gr, False, True)
        obj_pv={
           "UID":"Muon_Station%s_PVA%s" % (side, loop[1]),
           "groupedBy":"MUN_Station%s_PVAs" % side,
        }
        hp_pv.addObj(obj_pv, False, True)
        pass
    db.commit("")
