#!/usr/bin/env python
# every delayed action 

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers
    helpers=loadhelpers.LoadHelpers()
    classes=helpers.classes
    helps=loadhelpers.LoadHelpers().getHelpers()

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    #db = config.Configuration("oksconfig:../../../data/sb.data.xml")
    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hpdi = digitalinput.DigitalInputHelper()
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper()
    hpal.setDb(db)
    hpac = action.ActionHelper()
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper()
    hpda.setDb(db)
    # f = open("MissingDigitalInputs.txt", "w")

    counter=0
    more={}
    less={}
    newda={}
    problems={}


    #clear alarms
    for al in alarms_db:
        objs_di=al["digitalInput"]
        for di in  objs_di:
            `
            al_obj={
                "UID":al.UID(),
                "digitalInput":di.UID()
            }
            #hpal.addObj(al_obj, True, False)
            pass
        pass
    errors=[]
    with open("ActionList.txt", 'r') as file:
        data="".join(line for line in file)
        block_text=data.split("Published on DIP")
        #print len(block_text)
        
        for block in block_text:
            print "---"
            f_alarms=[]
            #print block
            for line in block.split("\n"):
                if line.startswith("ACTION  "):
                    f_action_name= line.split(" ")[2]
                    print f_action_name
                    pass
                if line.startswith("      AL_"):
                    f_alarms.append(line.split(" ")[6])
                    pass
                pass
            print f_action_name
            #print f_alarms
            try:
                db_action=db.get_obj("Action", f_action_name)
                pass
            except:
                print db_action.UID()
                errors.append(f_action_name)
                pass
            pass
        pass
    print "Errors:"
    for e in errors:
        print e
        pass
    #db.commit("")
    '''
    print counter
    print "More:"
    for m in more:
        print m
    print "less"
    for l in less:
        print l
    print "Actions in file but not in db: ", problems
    print "DelayedActions in file that are not in db", newda
    print "New actions to be created:", problems
    print "present_in_file_not_in_db", present_in_file_not_in_db
    print "present_in_file_not_in_db", len(present_in_file_not_in_db)
    '''
    pass
