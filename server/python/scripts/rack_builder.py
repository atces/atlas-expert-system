#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Build the racks considering  RackElectricalDistribution or ElectricalRepartitionBox, rack turbines and smoke sensors

import config
import logging
import argparse
import re
import rack_nomenclature
import power_source_adviser as power_adviser

def add_turbine(db,rack_obj,interlocked_source_obj=None):
	rack_name=rack_obj.UID()
	if db.test_object("RackTurbine", "RackTurbine_"+get_normalized_name(rack_name)): turbine_obj=db.get_obj("RackTurbine", "RackTurbine_"+get_normalized_name(rack_name))
	else: turbine_obj=db.create_obj("RackTurbine", "RackTurbine_"+get_normalized_name(rack_name))
	add_object_in_relation_without_duplication(rack_obj,'isDependantContainerOf',turbine_obj)
	add_object_in_relation_without_duplication(turbine_obj,'inDependantContainer',rack_obj)
	creation_method=db.get_obj if db.test_object("ThermoSwitch","ThermoSwitch_"+get_normalized_name(rack_name)) else db.create_obj 
	thermo_switch_obj=creation_method("ThermoSwitch","ThermoSwitch_"+get_normalized_name(rack_name))
	if interlocked_source_obj:
		thermo_switch_obj['interlocks']=[interlocked_source_obj]
		add_object_in_relation_without_duplication(interlocked_source_obj,'interlockedBy',thermo_switch_obj)	
	thermo_switch_obj['inDependantContainer']=[turbine_obj]
	turbine_obj['isDependantContainerOf']=[thermo_switch_obj]
	smoke_sensors_objs=db.get_objs("SmokeSensor")
	for smoke_sensor_obj in smoke_sensors_objs:
		if rack_name in smoke_sensor_obj['description'] or get_normalized_name(rack_name) in smoke_sensor_obj['description']:
			add_object_in_relation_without_duplication(turbine_obj,'contains',smoke_sensor_obj) 
			add_object_in_relation_without_duplication(smoke_sensor_obj,'containedIn',turbine_obj)

def add_object_in_relation_without_duplication(obj,relation,obj_to_add):
	if obj_to_add.UID() not in [_obj.UID() for _obj in obj[relation]]: obj[relation]+=[obj_to_add] 

#Replaces '*','/' by '_' and removes '.' and '-'  	
def get_normalized_name(name):
	return name.replace('/','_').replace('*','_').replace('.','').replace('-','')

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Build the racks considering  RackElectricalDistribution or ElectricalRepartitionBox, rack turbines and smoke sensors')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-r','--rack', required=False, help='Rack to update')
	args = parser.parse_args()
	 
	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	rack_name=input("Enter the rack name to be build: ") if not args.rack else args.rack
	rack_obj=db.get_obj("Rack",rack_name) if db.test_object("Rack",rack_name) else db.create_obj("Rack",rack_name)
	#For racks without energy
	#yn=input("Is there a turbine in the rack %s? (y/n*): "%rack_name)
	#if yn.lower()=='y' or yn.lower()=="yes":	
	#	add_turbine(db,rack_obj,None) #Turbine normaly doesn't interlocks the electrical breaker
	#db.commit()

	power_sources_uids=get_normalized_name(input("Enter the names of the switch boards feeding the rack (use comma for many):")).replace(" ","").split(",")
	power_sources=[db.get_obj("Switchboard",source) for source in power_sources_uids]

	interlocks=rack_obj['interlockedBy']
	logger.debug ("Current power sources of the rack are %s."%power_sources)
	logger.debug ("Current interlocks of the rack are %s."%interlocks)
	for power_source in power_sources:
		power_source["powers"]=[obj for obj in power_source["powers"] if not obj==rack_obj]
		if "EOD" in power_source.UID() or "ESD" in power_source.UID(): #Long UPS coverage case
			electrical_breaker_name=input("Please enter the name of the electrical breaker associated with the power source %s and the rack %s: "%(power_source.UID(),rack_name))
			electrical_breaker_obj=db.create_obj("ElectricalBreaker", get_normalized_name(electrical_breaker_name)) if not db.test_object("ElectricalBreaker", get_normalized_name(electrical_breaker_name)) else db.get_obj("ElectricalBreaker", get_normalized_name(electrical_breaker_name))
			if '/' in electrical_breaker_name: electrical_breaker_obj['otherIds']=[electrical_breaker_name] 
			add_object_in_relation_without_duplication(electrical_breaker_obj,'poweredBy',power_source)
			add_object_in_relation_without_duplication(power_source,'powers',electrical_breaker_obj)
			rack_electrical_distribution_name="%s_%s"%(get_normalized_name(rack_name),electrical_breaker_obj.UID())
			rack_electrical_distribution_obj=db.get_obj("RackElectricalDistribution",rack_electrical_distribution_name) if db.test_object("RackElectricalDistribution",rack_electrical_distribution_name) else db.create_obj("RackElectricalDistribution",rack_electrical_distribution_name)
			add_object_in_relation_without_duplication(rack_electrical_distribution_obj,"poweredBy",electrical_breaker_obj)
			add_object_in_relation_without_duplication(electrical_breaker_obj,'powers',rack_electrical_distribution_obj)
			add_object_in_relation_without_duplication(rack_obj,'isDependantContainerOf',rack_electrical_distribution_obj)
			add_object_in_relation_without_duplication(rack_electrical_distribution_obj,'inDependantContainer',rack_obj)
			for interlock in interlocks:
				yn=input("Is this electrical breaker interlocked by %s? (y/n*):"%interlock.UID())
				if yn.lower()=='y' or yn.lower()=="yes":
					add_object_in_relation_without_duplication(interlock,"interlocks",electrical_breaker_obj)
					add_object_in_relation_without_duplication(electrical_breaker_obj,"interlockedBy",interlock)
					interlock["interlocks"]=[obj for obj in interlock["interlocks"] if not obj==rack_obj]
					rack_obj["interlockedBy"]=[obj for obj in rack_obj["interlocks"] if not obj==interlock]
			yn=input("Is there a turbine in the rack %s? (y/n*): "%rack_name)
			if yn.lower()=='y' or yn.lower()=="yes":	
			    add_turbine(db,rack_obj,None) #Turbine normaly doesn't interlocks the electrical breaker
		else:#Normal power + short UPS = Broad coverage
			yn=input("Is the power source %s attached to a twido box over this rack? (y*/n): "%power_source.UID())
			if not yn.lower()=='n' and not yn.lower()=='no':
				twido_box_name=input("Please enter the name of the twido box: ")
				if not db.test_object("TwidoBox", get_normalized_name(twido_box_name)):
					twido_box_obj=db.create_obj("TwidoBox", get_normalized_name(twido_box_name))
				else:
					twido_box_obj=db.get_obj("TwidoBox", get_normalized_name(twido_box_name))
				if '/' in twido_box_name: twido_box_obj['otherIds']=[twido_box_name] 
				add_object_in_relation_without_duplication(twido_box_obj,'poweredBy',power_source)
				add_object_in_relation_without_duplication(power_source,'powers',twido_box_obj)
				repartition_box_name=twido_box_name.replace("EXD","EXJ") if "EXD" in twido_box_name else input("Please enter the name of the repartition box connected to the twido box %s: "%twido_box_name)
				if not db.test_object("ElectricalRepartitionBox"+rack_nomenclature.get_building(rack_obj.UID()), get_normalized_name(repartition_box_name)):
					repartition_box_obj=db.create_obj("ElectricalRepartitionBox"+rack_nomenclature.get_building(rack_obj.UID()), get_normalized_name(repartition_box_name))
				else:
					repartition_box_obj=db.get_obj("ElectricalRepartitionBox"+rack_nomenclature.get_building(rack_obj.UID()), get_normalized_name(repartition_box_name))
				if '/' in repartition_box_name: repartition_box_obj['otherIds']=[repartition_box_name]
				add_object_in_relation_without_duplication(repartition_box_obj,'poweredBy',twido_box_obj)
				add_object_in_relation_without_duplication(twido_box_obj,'powers',repartition_box_obj)
				repartition_box_obj['inDependantContainer']=[rack_obj]
				add_object_in_relation_without_duplication(rack_obj,'isDependantContainerOf',repartition_box_obj)
				
				for interlock in interlocks:
					yn=input("Is this repartition box interlocked by %s? (y*/n):"%interlock.UID())
					if not yn.lower()=='n' and not yn.lower()=='no':
						add_object_in_relation_without_duplication(interlock,"interlocks",repartition_box_obj)
						add_object_in_relation_without_duplication(repartition_box_obj,"interlockedBy",interlock)
						interlock["interlocks"]=[obj for obj in interlock["interlocks"] if not obj==rack_obj]
						rack_obj["interlockedBy"]=[obj for obj in rack_obj["interlocks"] if not obj==interlock]

				yn=input("Is there a turbine in the rack %s? (y/n*): "%rack_name)
				if yn.lower()=='y' or yn.lower()=="yes":	
					add_turbine(db,rack_obj,twido_box_obj)
			else: 
				electrical_breaker_name=input("Please enter the name of the electrical breaker associated with the power source %s and the rack %s: "%(power_source.UID(),rack_name))
				if db.test_object("ElectricalBreaker", get_normalized_name(electrical_breaker_name)):  electrical_breaker_obj=db.get_obj("ElectricalBreaker", get_normalized_name(electrical_breaker_name))
				else: electrical_breaker_obj=db.create_obj("ElectricalBreaker", get_normalized_name(electrical_breaker_name))
				if '/' in electrical_breaker_name: electrical_breaker_obj['otherIds']=[electrical_breaker_name] 
				add_object_in_relation_without_duplication(electrical_breaker_obj,'poweredBy',power_source)
				add_object_in_relation_without_duplication(power_source,'powers',electrical_breaker_obj)
				for interlock in interlocks:
					yn=input("Is this electrical breaker interlocked by %s? (y/n*):"%interlock.UID())
					if yn.lower()=='y' or yn.lower()=="yes":
						add_object_in_relation_without_duplication(interlock,"interlocks",electrical_breaker_obj)
						add_object_in_relation_without_duplication(electrical_breaker_obj,"interlockedBy",interlock)
				distribution_objs=[]
				yn=input("Is there a repartition box in the rack %s? (y*/n):"%rack_name)
				if not yn.lower()=='n' and not yn.lower()=='no':
					repartition_box_name_def="%s_%s"%(get_normalized_name(rack_name),electrical_breaker_obj.UID())
					repartition_box_name=input("Please enter the repartition box name (default %s):"%(repartition_box_name_def))
					repartition_box_name=repartition_box_name if repartition_box_name and not repartition_box_name=='' else repartition_box_name_def
					creation_method=db.get_obj if db.test_object("ElectricalRepartitionBox"+rack_nomenclature.get_building(rack_obj.UID()), get_normalized_name(repartition_box_name)) else db.create_obj
					distribution_obj=creation_method("ElectricalRepartitionBox"+rack_nomenclature.get_building(rack_obj.UID()), get_normalized_name(repartition_box_name))
					if '/' in repartition_box_name: distribution_obj['otherIds']=[repartition_box_name]
					add_object_in_relation_without_duplication(distribution_obj,'poweredBy',electrical_breaker_obj)
					add_object_in_relation_without_duplication(electrical_breaker_obj,'powers',distribution_obj)
					distribution_objs.append(distribution_obj)
				else:	
					num_of_distributions=input("How many rack electrical distributions are in the rack (default 1)?: ")
					num_of_distributions= int(num_of_distributions) if num_of_distributions.isdigit() and not num_of_distributions=='0' else 1 
					match = re.match(r"([a-z]+)([0-9]+)([/-_].*)", electrical_breaker_obj.UID(), re.I)
					if match: name_root,name_number,name_end=match.groups()
					else: name_root=electrical_breaker_obj.UID(); name_number=0; name_end=''
					j=0 if num_of_distributions==1 else 1
					for i in range(j,num_of_distributions+j):
						i_breaker_name="%s%s%s"%(name_root,int(name_number)+i if not name_number == 0 else None,name_end) if not num_of_distributions==1 else electrical_breaker_obj.UID()
						i_breaker_obj=db.create_obj("ElectricalBreaker",i_breaker_name) if not db.test_object("ElectricalBreaker",i_breaker_name) else db.get_obj("ElectricalBreaker",i_breaker_name)
						distribution_name="%s_%s%s%s"%(get_normalized_name(rack_name),name_root,int(name_number)+i if not name_number == 0 else None,name_end)
						distribution_obj=db.create_obj("RackElectricalDistribution", get_normalized_name(distribution_name)) if not db.test_object("RackElectricalDistribution", get_normalized_name(distribution_name)) else db.get_obj("RackElectricalDistribution", get_normalized_name(distribution_name))
						if '/' in distribution_name: distribution_obj['otherIds']=[distribution_name]
						add_object_in_relation_without_duplication(distribution_obj,'poweredBy',i_breaker_obj)
						add_object_in_relation_without_duplication(i_breaker_obj,'powers',distribution_obj)
						if not num_of_distributions==1:
							add_object_in_relation_without_duplication(i_breaker_obj,'poweredBy',electrical_breaker_obj)
							add_object_in_relation_without_duplication(electrical_breaker_obj,'powers',i_breaker_obj)
						distribution_objs.append(distribution_obj)
				for distribution_obj in distribution_objs:
					add_object_in_relation_without_duplication(rack_obj,'isDependantContainerOf',distribution_obj)
					add_object_in_relation_without_duplication(distribution_obj,'inDependantContainer',rack_obj)

				yn=input("Is there a turbine in the rack %s? (y/n*): "%rack_name)
				if yn.lower()=='y' or yn.lower()=="yes":	
					add_turbine(db,rack_obj,None) #Turbine normaly doesn't interlocks the electrical breaker
	
	uids=power_adviser.get_power_receivers_in_container(db,rack_name)	
	if len(uids)>0: power_adviser.advice_power_source(db,uids)
	db.commit()
