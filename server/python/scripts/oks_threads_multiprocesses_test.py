import config
import datetime
import threading
from multiprocessing import Process

def function(num,db_path,process_id):
	db=config.Configuration("oksconfig:%s"%db_path)
	for i in range(num):
		obj=db.get_obj("Switchboard","EOD1_1DX")
		obj['description']=str(i)+"Process "+process_id
	db.commit()

if __name__== "__main__":
	start_time=datetime.datetime.now()
	function(80000,"../../../data/sb.data.xml","1")
	function(80000,"../../../data/sb.data2.xml","2")
	print("Processing time using only the main process= %f"%((datetime.datetime.now()-start_time).total_seconds()))
	t1=threading.Thread(target=function,args=(80000,"../../../data/sb.data.xml","1"))
	t2=threading.Thread(target=function,args=(80000,"../../../data/sb.data2.xml","2"))
	start_time=datetime.datetime.now()
	t1.start()
	t2.start()
	t1.join()
	t2.join()
	print("Processing time using 2 Threads= %f"%((datetime.datetime.now()-start_time).total_seconds()))
	t1=Process(target=function,args=(80000,"../../../data/sb.data.xml","1"))
	t2=Process(target=function,args=(80000,"../../../data/sb.data2.xml","2"))
	start_time=datetime.datetime.now()
	t1.start()
	t2.start()
	t1.join()
	t2.join()
	print("Processing time using 2 Process= %f"%((datetime.datetime.now()-start_time).total_seconds()))
