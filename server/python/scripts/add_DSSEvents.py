#!/usr/bin/env python
#######################################################
# add DSSEvents from csv file
# Florian.Haslbeck@cern.ch
# June 2023
#######################################################

import urllib.request
import json
import pandas as pd


def find_DSSLogs(db, verbose=False):
    """ find all DSSLogs in database
        input:
            db: database object
        output:
            list of DSSLog objects
    """
    # get all DSSLogs
    DSSLogs = db.get_objs("DSSLog")
    if verbose: print(f"Found {len(DSSLogs)} DSSLogs")
    return DSSLogs


def get_events_from_csv(csv_file):
    """ get all events from csv file 
        and return a nested hash table with first key alarm name, second date
        the value is another dictionary with keys:
            time, 
            description, 
            alarms, 
            alarm_times
        input:
            csv_file: csv file
        output:
    """
    columns_to_extract = ["description", "date", "time", "alarms", "alarm_times", "datetime"]

    # read csv file
    df = pd.read_csv(csv_file, sep=";", header=0, names=columns_to_extract)

    # show all columns
    # for col in df.columns: print(col)

    print(df.head(5))




    # convert to a nested dictionary with first key alarm name, second datetime
    # events = {}
    # for index, row in df.iterrows():
    #     description = row["description"]
    #     date = row["date"]
    #     time = row["time"]

    #     print(description, date, time)
        # alarm = row["alarms"]
    #     if alarm not in events: events[alarm] = {}
    #     events[alarm][row["datetime"]] = row.to_dict()

    return events


def edit_DSSEvent_object(db, 
                        uid, 
                        date = None, 
                        time = None,
                        description = None,
                        title = None,
                        event_type = None,
                        person = None,
                        elisa = None,

                        logs = None,

                        verbose=False):
    """ edit DSSEvent object in database
        if DSSEvent does not exist, create it, else update it

        output:
            None
    """
    # check if DSSLog already exists, else create it
    try : 
        obj = db.get_obj("DSSEvent",uid)
        if verbose: print(f"UPDATING DSSEvent {uid}")
    except: 

        obj = db.create_obj("DSSEvent", uid)
        if verbose: print(f"CREATING DSSEvent {uid}")

    

   
    # check if DSSEvent needs to be updated
    # attributes
    attributes = [("date", date),
                  ("time", time), 
                  ("description", description), 
                    ("title", title),
                    ("event_type", event_type),
                    ("person", person),
                    ("elisa", elisa)
                ]
                  
    for attr, value in attributes:
        if value is not None and obj.get_string(attr) != value:  obj.set_string(attr, value)

    # relations
    # relations  = [("logs", logs)] #, ("event", event)]
    # for rel_value in relations:
    #     rel, value = rel_value
    #     # get object from database
    #     value = db.get_obj("Alarm", value) # this is an object of class db

        

        # if value is not None: obj[rel] = value
            
   



if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    # parser.add_argument("date", help="date string of format yyyy-mm-dd")
    # parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    dssevent_csv = "../../data/new_allDSS.csv"

    # get DSSEvents from csv file
    events = get_events_from_csv(dssevent_csv)

    # for event in events:
    #     print(event, events[event])


    
    # load oks database and get classes and helpers
    # db  = config.Configuration("oksconfig:%s"%(args.database))

    # read the csv file


    # dsslogs = find_DSSLogs(db, verbose=args.verbose)

    # print(" * * * * ")
    # print("FIX date -> swedish format!!! YYYY-MM-DD")
    # input("Stop!")


    # # create DSSLogs
    # for log in web_logs:
    #     # extract data
    #     came, name, time = log['came'], log['name'], log['time']
    #     came, name, time = came.strip(), name.strip(), time.strip()

    #     # create uid
    #     uid = came + "_" + name

    #     # create DSSLog object
    #     edit_DSSEvent_object(db, uid, name, came, time, verbose = args.verbose)

    
    
    
