#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Delete an oks object and all the references to this object

import argparse
import config

def change_type_oks_object(db,uid,old_type,new_type):
	obj=db.get_obj(old_type,uid)
	obj.rename(uid+"_TEMP")
	new_obj=db.create_obj(new_type,uid)
	for attribute in obj.__schema__['attribute']:
		if attribute in new_obj.__schema__['attribute']: new_obj[attribute]=obj[attribute]
		else: print("Attribute %s lost."%attribute)
	for relation in obj.__schema__['relation']:
		if relation in new_obj.__schema__['relation']: new_obj[relation]=obj[relation]
		else: print("Relation %s lost."%relation)
	for referencer in obj.referenced_by():
		referencer=db.get_obj(referencer.class_name(),referencer.UID())
		for relation in referencer.__schema__['relation']:
			if isinstance(referencer[relation],list) and obj in referencer[relation]:
				referencer[relation]=[any_obj for any_obj in referencer[relation] if not any_obj==obj]+[new_obj]
			elif not isinstance(referencer[relation],list) and obj==referencer[relation]:
				referencer[relation]=new_obj
	db.destroy_obj(obj)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-u", "--uid", help="UID of the object to change", required=True)
	parser.add_argument("-c", "--clazz", help="Old class/type of the object to change", required=True)
	parser.add_argument("-n", "--new", help="New class/type of the object to change", required=True)
	parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)
	change_type_oks_object(db,args.uid,args.clazz,args.new)
	db.commit('')	

