#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import rack
    import subdetector
    import system
    import crate
    import powersupply
    import water
    import coolingstation
    import coolingloop
    import alarm

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    rack_db = db.get_objs("Rack")
    subdetector_db = db.get_objs("SubDetector")
    sys_db = db.get_objs("System")
    crate_db = db.get_objs("Crate")
    ps_db = db.get_objs("PowerSupply")
    water_db = db.get_objs("WaterSystem")
    coolingstation_db = db.get_objs("CoolingStation")
    coolingloop_db = db.get_objs("CoolingLoop")
    alarm_db = db.get_objs("Alarm")
    gas_db = db.get_objs("GasSystem")
    ventilation_db = db.get_objs("VentilationSystem")
    magnet_db = db.get_objs("Magnet")

    hprack = rack.RackHelper()
    hprack.setDb(db)
    hpsubdetector = subdetector.SubDetectorHelper()
    hpsubdetector.setDb(db)
    hpsys = system.SysHelper()
    hpsys.setDb(db)
    hpcrate = crate.CrateHelper()
    hpcrate.setDb(db)
    hpps = powersupply.PowerSupplyHelper()
    hpps.setDb(db)
    hpwater = water.WaterSystemHelper()
    hpwater.setDb(db)
    hpcoolingstation = coolingstation.CoolingStationHelper()
    hpcoolingstation.setDb(db)
    hpcoolingloop = coolingloop.CoolingLoopHelper()
    hpcoolingloop.setDb(db)
    hpalarm = alarm.AlarmHelper()
    hpalarm.setDb(db)

    #waterFrom, gasFrom, coolingFrom

    def checkRels(elem):
        # if elem["waterFrom"]:
        #     print elem["waterFrom"]
        #     elem["requiresWaterFrom"] = elem["waterFrom"]
        #     elem["waterFrom"] = ""
        if elem["gasFrom"]:
            print elem["gasFrom"]
            elem["requiresGasFrom"] = elem["gasFrom"]
            elem["gasFrom"] = ""
        # if elem["coolingFrom"]:
        #     print elem["coolingFrom"]
        #     elem["requiresCoolingFrom"] = elem["coolingFrom"]
        #     elem["coolingFrom"] = ""
        return elem

    # for r in rack_db:
    #     checkRels(r)
    # for s in subdetector_db:
    #     checkRels(s)
    # for s in sys_db:
    #     checkRels(s)
    # for c in crate_db:
    #     checkRels(c)
    # for p in ps_db:
    #     checkRels(p)
    # for w in water_db:
    #     checkRels(w)
    # for c in coolingstation_db:
    #     checkRels(c)
    # for c in coolingloop_db:
    #     checkRels(c)
    # for g in gas_db:
    #     checkRels(g)
    # for v in ventilation_db:
    #     checkRels(v)
    for m in magnet_db:
        checkRels(m)

    # for a in alarm_db:
    #     if a["DSU"]:
    #         print a["DSU"]
    #         a["DSU"] = ""
    #     if a["triggeredBy"]:
    #         print "tb"
    #         a["triggeredBy"] = ""


    db.commit("")

