#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db=config.Configuration("oksconfig:%s"%(args.database))

    actions=db.get_objs("Action")
    nodsu=[]
    for action in actions:
        print "Process action: %s" % action.UID()
        dsu=action["DSU"]
        if not dsu:
            nodsu.append(action.UID())
            continue
        if action in dsu["actions"]: continue
        print " Add action %s to %s" % (action.UID(),dsu.UID())
        #dsu=db.get_obj("DSU",dsu.UID())
        ee=dsu["actions"]
        ee.append(action)
        dsu["actions"]=ee
        pass
    
    dsus=db.get_objs("DSU")
    for dsu in dsus:
        print "DSU: %s" % dsu.UID()
        print " #actions: %i" % len(dsu["actions"])
        pass
        
    print "Actions with no DSU"
    for action in sorted(nodsu):
        print " %s" % action
        pass
        
    if args.yes: db.commit("")
    sys.exit(0)
