
#!/usr/bin/env python
# @author gustavo.uribe@cern.ch

import logging
import argparse
import config
import helper

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)

	parser = argparse.ArgumentParser(description='Changes the contains relation by the isDependantConainerOf and also update the inverse relation inDependantContainer')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	args = parser.parse_args()

	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	
	logger.info("WARNING you are going to remove ALL the contains relations from the racks!\a")
	ans=input("Are you sure? y/n: ")
	if(not ans=='y'): exit()
	helper=helper.Helper(db=db)
	racks=db.get_objs("Rack")
	for rack in racks:
		if ('powers' in rack.__schema__['relation'] and rack['powers']==[]) and ('controls' in rack.__schema__['relation'] and rack['controls']==[]):
			for obj in rack['contains']:
                            if obj not in rack['isDependantContainerOf']:   rack['isDependantContainerOf']+=[obj]
                            if rack not in obj['inDependantContainer']:    obj['inDependantContainer']+=[rack]
			rack['contains']=[]
			logger.debug("%r contains=%s isDependantContainerOf=%s"%(rack,rack['contains'],rack['isDependantContainerOf']))
	db.commit("")
