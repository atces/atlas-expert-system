#!/usr/bin/env python
#######################################################
# add DSSLogs from DSS SQL
# Florian.Haslbeck@cern.ch
# June 2023
#######################################################

import urllib.request
import json


def get_DSS_alarms_from_web(date_str):
    """ get all DSS alarms from DSS logbook web interface since a given date 
        input: 
            date as string of format yyyy-mm-dd
        output: 
            iterable object with DSSLog-like tuples (name, time, came)
    """
    
    # get json data
    assert len(date_str.split("-")) == 3, "date_str must be of format yyyy-mm-dd"
    url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_since&date={date_str}"
    data = urllib.request.urlopen(url).read()
    json_data = json.loads(data)
    dss_logs = json_data["report"]
    return dss_logs


def get_DSS_alarms_from_web_range(date_str_start, date_str_end):
    """ get all DSS alarms from DSS logbook web interface since a given date 
        input: 
            date as string of format yyyy-mm-dd
        output: 
            iterable object with DSSLog-like tuples (name, time, came)
    """
    
    # get json data since
    assert len(date_str_start.split("-")) == 3, "date_str must be of format yyyy-mm-dd"
    url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_range&since={date_str_start}&until={date_str_end}"
    data = urllib.request.urlopen(url).read()
    json_data = json.loads(data)

    dss_logs = json_data["report"]

    return dss_logs

def edit_DSSLog_object(db, 
                        uid, 
                        name = None, time = None,
                        event = None, 
                        verbose=False):
    """ edit DSSLog object in database
        if DSSLog does not exist, create it, else update it
    
        input:
            db:    database object
            uid:   unique id of DSSLog -> came
            name:  name of DSSLog -> name
            time:  time of DSSLog -> time
            # event: DSSEvent uid 
        output:
            None
    """

    def _get_obj(db, _type, _name, verbose=False):
        try:
            retr_obj = db.get_obj(_type, _name) # this is an object of class db
        except:
            retr_obj = None
        return retr_obj

    ret = None

    # check if DSSLog already exists, else create it
    try : 
        obj = db.get_obj("DSSLog",uid)
        if verbose: print(f"UPDATING DSSLog {uid}")
    except: 
        obj = db.create_obj("DSSLog", uid)
        if verbose: print(f"CREATING DSSLog {uid}")

   
    # check if DSSLog needs to be updated
    
    # attributes
    attributes = [("name", name), ("time", time)]
    for attr, value in attributes:
        if value is not None and obj[attr] != value:  obj[attr]  = value

    # relations
    relations  = [("alarm", name)] 
    for rel, value in relations:
        # get object from database
        retr_obj = _get_obj(db, "Alarm", value) # this is an object of class db
        if retr_obj is not None:
            obj[rel] = retr_obj
        else:
            print(f"WARNING: Alarm {value} does not exist in database")
            ret = name
    
    return ret



if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    # parser.add_argument("date", help="date string of format yyyy-mm-dd")
    # parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    special_cases = {

        "AL_INF_WaterLeak_MUN_CSC_Y5823X0"          : None,
        "AL_INF_WaterLeak_Failure_MUN_CSC_Y2924X1"  : None,
        "AL_INF_Power_US15_EXS102_UPS_EndOfAutonomy": "AL_INF_Power_US15_EXS2_UPS_EndOfAutonomy",
        "AL_INF_Power_US15_EXS102_UPSonBattery"     : "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_GAS_MUN_CSC_GasFailure"                 : "AL_GAS_MUN_MMG_GasFailure",
        "AL_TestAlarm"                              : None,
        "AL_INF_Smoke_SR1_Ceiling"                  : "AL_INF_Smoke_SR1",
        "AL_INF_Power_US15_EXS102_UPS_OnBattery"	: "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_INF_Power_US15_EXS102_UPSFailure"	    : "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_GAS_TRT_ArgonActiveGas_Stop"	        : "AL_GAS_TRT_ArgonActiveGas_Stop",
    }


    not_in_es = []

    # start_date = "2018-01-02"
    # end_date   = "2023-04-22"

    
    # start_date = "2023-04-22"
    # end_date   = "2024-03-11"

    # start_date = "2023-03-11"
    # end_date   = "2024-04-10"

    # start_date = "2024-04-09"
    # end_date   = "2024-06-01"

    start_date = "2024-06-01"
    end_date   = "2024-07-25"

    print(f"Getting DSSLogs from {start_date} to {end_date}")
    input("Press Enter to continue...")


    # get DSSLogs from web
    web_logs = get_DSS_alarms_from_web_range(start_date, end_date)

    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    # create DSSLogs
    for i, log in enumerate(web_logs):

        print(f"Processing {log} {i}/{len(web_logs)}")
        # extract data
        came, name, time = log['came'], log['name'], log['time']
        came, name, time = came.strip(), name.strip(), time.strip()

        name = name.replace("AL_AL_", "AL_")

        if name in special_cases.keys():
            name = special_cases[name]
            if name is None: 
                print(f"Skipping {came} {name} {time}")
                continue
            else:
                print(f"Renaming {came} {name} {time} to {name}")


        # create uid
        uid = f"log_{came}"

        # create DSSLog object
        ret = edit_DSSLog_object(db, uid, name, time, verbose = args.verbose)

        if ret is not None: 
            not_in_es.append(ret)

        if i % 100 == 0:
            db.commit()
            print(f"Committing {i}/{len(web_logs)}")

    db.commit()

    print("Not in db:")
    for e in list(set(not_in_es)):
        print(repr(e))

    
    

    
    
    
