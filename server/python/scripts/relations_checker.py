#!/usr/bin/env python
# ignacio.asensi@cern.ch
# Aug 2018
import os
import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import fta
import messenger
import loadhelpers
import ProbTree
import traceback
import helper
import argparse
import copy

verbose=True
helpers=loadhelpers.LoadHelpers()
classes=helpers.classes
helps=loadhelpers.LoadHelpers().getHelpers()

#functions
def getClass(uid):
    for c in classes:
        o=""
        try:
            o=db.get_obj(c[0], uid)
            return o.class_name()
        except:
            pass
        pass
    return 0

def checkCounter(original, counter_ob):
    #print "Checking %s, COUNTER: %s" % (original.UID(), counter_ob.UID())
    if type(counter_ob)!="str":
        counter_ser= helps[counter_ob.class_name()].serialize(counter_ob)
        #check attributes
        for at in counter_ser:
            if isinstance(counter_ser[at], list):
                for a in counter_ser[at]:
                    if original.UID() == a:
                        return True
                        pass
                    pass
                pass
            if original.class_name()=="Action" and at=="action":
                if original.UID() == counter_ser[at]:
                    return True
                pass
            pass
        pass
    return False
####################

def getMultiAttributesEXT(checkclass, db):
    #get relations attributes
    class_module = __import__(checkclass)
    class_help=getattr(class_module, dir(class_module)[0])()
    class_help.setDb(db)
    class_attrs=getattr(class_module, dir(class_module)[0])().attrs
    multi=[]
    for a in class_attrs:
        if class_attrs[a]["multi"] == True or (a=="delayedactions" and checkclass=="Action"):
            if a=="otherIds" or a=="logger": continue
        #if class_attrs[a]["multi"] == True and a!="otherIds" and a!="logger":
            multi.append(a)
            pass
        pass
    if verbose == True: print "Attributes to check:", multi
    return multi
def getMultiAttributes(checkclass):
    #get relations attributes
    class_module = __import__(checkclass)
    class_help=getattr(class_module, dir(class_module)[0])()
    class_help.setDb(db)
    class_attrs=getattr(class_module, dir(class_module)[0])().attrs
    multi=[]
    for a in class_attrs:
        if class_attrs[a]["multi"] == True or (a=="delayedactions" and checkclass=="Action"):
            if a=="otherIds" or a=="logger": continue
        #if class_attrs[a]["multi"] == True and a!="otherIds" and a!="logger":
            multi.append(a)
            pass
        pass
    if verbose == True: print "Attributes  to check:", multi
    return multi

def getClassName(checkclass):
    class_module = __import__(checkclass)
    classname= getattr(class_module, dir(class_module)[0])().classname
    return classname

def checkObject(obj, multi):
    u=obj.UID()
    failed={}
    if verbose == True: print "Checking ", u
    for attribute in multi:
        for counter in obj[attribute]:
            flag=checkCounter(obj, counter)
            if flag == False:
                failed={attribute:counter.UID()}
            pass
        pass
    if len(failed) == 0:
        return 0
        #del failed[u]
        #pass
    return failed
'''
def FullcheckObjectFull(obj, multi):#includes searching all other objects in db
    u=obj.UID()
    failedCounters={}
    if verbose == True: print "Checking ", u
    for attribute in multi:
        for counter in obj[attribute]:
            flag=checkCounter(obj, counter)
            if flag == False:
                failedCounters={attribute:counter.UID()}
            pass
        pass
    
    for c in classes:
        obj_list=db.get_objs(c)
        for o in obj_list:
        
        pass
    if len(failed) == 0:
        return 0
    #del failed[u]
    #pass
    return failed
'''
def execute_test(checkclass):
    multi=getMultiAttributes(checkclass)
    classname= getClassName(checkclass)
    
    failed={}
    obj_list=db.get_objs(classname)
    for obj in obj_list:
        #get object from db
        u=obj.UID()
        res=checkObject(obj, multi)
        if res!=0:
            failed[u]=res
            pass
        '''
        u=obj.UID()
        failed[u]={}
        if verbose == True: print "Checking ", u
        for attribute in multi:
            for counter in obj[attribute]:
                flag=checkCounter(obj, counter)
                if flag == False:
                    failed[u]={attribute:counter.UID()}
                pass
            pass
        if len(failed[u]) == 0:
            del failed[u]
            pass
        pass
        '''
    print ""
    return failed









if __name__ == '__main__':
    verbose=True
    #import classes from loadhelpers
    
         
    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-c","--checkclass",help="Check class")#
    parser.add_argument("-ca","--checkattributes",action="store_true",help="Check attributes")
    
    
    args=parser.parse_args()
    print args.database


    print "Load database: %s" % args.database
    selected_class=args.checkclass
    check_attributes=args.checkattributes
    db = config.Configuration("oksconfig:%s"%(args.database))

    
    if check_attributes == False:
        
        
        
        errors=""

        #MAGIC STARTS HERE
        
        #set helpers
        for h in helps:
            helps[h].setDb(db)
            pass
        
        #REPORTS
        failures={}
        if selected_class=="all":
            for c in classes:
                failures[c[1]]=execute_test(c[1])
                pass
            pass
        else:
            failures[selected_class]=execute_test(selected_class)
            pass
        
        empty=True
        report=""
        totalfailures=0
        print failures
        for failed in failures:
            if len(failures[failed])>0:
                empty=False
                totalfailures+=len(failures[failed])
                report+="==>Class [%s]. Objects have broken relationships: [%i] \n" % (failed, len(failures[failed]))
                for f in failures[failed]:
                    print f,"==>",failures[failed][f]
                    pass
                pass
            pass
        if empty==True:
            print "ALL OK!"
            pass
        else:
            print report
            print "Total failures: ", totalfailures
        pass
    else:
        print "Checking attributes of all classes..."
        for c in classes:
            print "----------------"
            print c[0]
            to_module = __import__(c[1])
            to_help=getattr(to_module, dir(to_module)[0])()
            to_help.setDb(db)
            to_help.setVerbose(True)
            attrs=getattr(to_module, dir(to_module)[0])().attrs
            print attrs
            obj_list=db.get_objs(c[0])
            if len(obj_list)>0:
                ob_db=obj_list[0]
                ob_ser= helps[ob_db.class_name()].serialize(ob_db)
                for a in attrs:
                    if a !="UID":
                        print ob_db[a]
                        pass
                    pass
                
                '''
                for at in attrs:
                    print at
                '''
                pass
            pass
        pass
