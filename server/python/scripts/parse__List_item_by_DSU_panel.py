#!/usr/bin/env python

if __name__ == '__main__':

     import os
     import sys
     import config
     import argparse

     import alarm
     import action
     import dsu
     import digitalinput

     parser = argparse.ArgumentParser()
     parser.add_argument("database", help="database file")
     parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
     parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
     args=parser.parse_args()

     print "Load database: %s" % args.database
     db = config.Configuration("oksconfig:%s"%(args.database))
     file="List_item_by_DSU.txt"


     hpdi = digitalinput.DigitalInputHelper()
     hpdi.setDb(db)
     hpac = action.ActionHelper()
     hpac.setDb(db)
     hpdsu = dsu.DSUHelper()
     hpdsu.setDb(db)
     #uids=[["AI_COL_BeamPipe_VA_CoolingCirquitPressure","A","DSU3","upper","4","5","1"],
     #Item, Type, DSU, Crate, Module, Slot, n. Alarms
     with open(file, 'r') as file:
          data="".join(line for line in file)
          lines=data.split("\n")
          for line in lines:
            l=line.split(" ")
            n_name=l[0]
            n_type=l[1]
            n_dsu=l[2]
            n_crate=l[3]
            n_slot=int(l[4])
            n_channel=int(l[5])
            #n_module=l[5]
            if n_type == "DI" or n_type == "A":
                 db_dsu=db.get_obj("DSU","DSU_%s"%n_dsu[3])
                 #print db_dsu
                 o_dsu={
                 "UID": db_dsu.UID(),
                 "digitalInput":n_name
                 }
                 #print o_dsu
                 o_di = {
                   "UID": n_name,
                   "DSU": db_dsu.UID(),
                   "crate": n_crate,
                   "slot": n_slot,
                   "channel": n_channel,
                   "module":0
                 }
                 #print o_di
                 hpdi.addObj(o_di,False,True)
                 hpdsu.addObj(o_dsu,False,True)
                 pass
            elif n_type == "DO":
                 db_dsu=db.get_obj("DSU","DSU_%s"%n_dsu[3])
                 #print db_dsu
                 o_dsu={
                 "UID": db_dsu.UID(),
                 "actions":n_name
                 }
                 
                 #print o_dsu
                 o_ac = {
                   "UID": n_name,
                   "DSU": db_dsu.UID(),
                   "crate": n_crate,
                   "slot": n_slot,
                   "channel": n_channel,
                   "module":0
                 }
                 #hpac.addObj(o_ac,False,True)
                 #print "--"
                 #hpdsu.addObj(o_dsu,False,True)
                 #print o_ac
                 pass
            pass
          pass
     db.commit("")
     pass
