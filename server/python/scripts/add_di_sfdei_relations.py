#!/usr/bin/env python
# @author florian.haslbeck@cern.ch
# January 2024
# adds a relation DI <-> SFDEI in an OKS object guaranteeing uniqueness


import argparse
import config


def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj

	

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)

	# sfdei_di_relation
	sfdei_di = [("SFDEI-15878" , "DI_Smoke_SNI_Y1505S2"),
				("SFDEI-15879" , "DI_Smoke_SNI_Y1605S2"),
				("SFDEI-15880" , "DI_Smoke_SNI_Y1705S2"),
				("SFDEI-15881" , "DI_Smoke_SNI_Y1805S2"),
				("SFDEI-15882" , "DI_Smoke_SNI_Y1905S2"),
				("SFDEI-15883" , "DI_Smoke_SNI_Y2005S2"),
				("SFDEI-15884" , "DI_Smoke_SNI_Y2105S2"),
				("SFDEI-15837" , "DI_Smoke_MUN_Y0507S2"),
				("SFDEI-15838" , "DI_Smoke_MUN_Y0607S2"),
				("SFDEI-15839" , "DI_Smoke_MUN_Y0707S2"),
				("SFDEI-15840" , "DI_Smoke_MUN_Y0807S2"),
				("SFDEI-15841" , "DI_Smoke_MUN_Y0907S2"),
				("SFDEI-15842" , "DI_Smoke_MUN_Y1007S2"),
				("SFDEI-15843" , "DI_Smoke_MUN_Y1107S2"),
				("SFDEI-15844" , "DI_Smoke_MUN_Y1207S2"),
				("SFDEI-15845" , "DI_Smoke_MUN_Y1307S2"),
				("SFDEI-15846" , "DI_Smoke_MUN_Y1407S2"),
				("SFDEI-15858" , "DI_Smoke_PIX_Y2313S2"),
				("SFDEI-15859" , "DI_Smoke_PIX_Y2413S2"),
				("SFDEI-15860" , "DI_Smoke_PIX_Y2513S2"),
				("SFDEI-15861" , "DI_Smoke_PIX_Y2613S2"),
				("SFDEI-15862" , "DI_Smoke_PIX_Y2713S2"),
				("SFDEI-15863" , "DI_Smoke_PIX_Y2813S2"),
				("SFDEI-15864" , "DI_Smoke_PIX_Y2913S2"),
				("SFDEI-15848" , "DI_Smoke_MUN_Y1707S2"),
				("SFDEI-15849" , "DI_Smoke_MUN_Y1907S2"),
				("SFDEI-15865" , "DI_Smoke_PIX_Y0314S2"),
				("SFDEI-15866" , "DI_Smoke_PIX_Y0314S2"),
				("SFDEI-15867" , "DI_Smoke_SCT_Y0514S2"),
				("SFDEI-15850" , "DI_Smoke_MUN_Y2007S2"),
				("SFDEI-15868" , "DI_Smoke_SCT_Y1015S2"),
				("SFDEI-15869" , "DI_Smoke_SCT_Y1115S2"),
				("SFDEI-15870" , "DI_Smoke_SCT_Y1215S2"),
				("SFDEI-15855" , "DI_Smoke_ID_Y2607S2"),
				("SFDEI-15856" , "DI_Smoke_ID_Y2707S2"),
				("SFDEI-15857" , "DI_Smoke_PIX_Y2807S2"),
				("SFDEI-15871" , "DI_Smoke_SCT_Y2315S2"),
				("SFDEI-15872" , "DI_Smoke_SCT_Y2415S2"),
				("SFDEI-15873" , "DI_Smoke_SCT_Y2515S2"),
				("SFDEI-15874" , "DI_Smoke_SCT_Y2615S2"),
				("SFDEI-15875" , "DI_Smoke_SCT_Y2715S2"),
				("SFDEI-15876" , "DI_Smoke_SCT_Y2815S2"),
				("SFDEI-15877" , "DI_Smoke_SCT_Y2915S2"),
			]

      
	# iterate over all di_sfdei relations
	for sfdei, di in sfdei_di:

		print(f"SFDI {sfdei} <-> DI {di}")

		# get di and sfdei objects
		di_obj 	  = get_obj(db, "DigitalInput", di)
		sfdei_obj = get_obj(db, "SmokeSensor", sfdei)

		# check if objects exist
		if (not di_obj) or (not sfdei_obj):
			print(f"Did not find DI {di} or SFDEI {sfdei}")
			continue

		# check if relation DI -> SFDEI already exists
		print("DI -> SFDEI: ", di_obj["signalSource"])
		print("SFDEI -> DI: ", sfdei_obj["digitalOutput"])

		linked_dis = di_obj["signalSource"]
		if sfdei_obj not in linked_dis:
			linked_dis.append(sfdei_obj)
			print(f"Added relation {di} -> {sfdei}")

		di_obj["signalSource"] = linked_dis
		
		linked_sfdeis = sfdei_obj["digitalOutput"]
		if di_obj not in linked_sfdeis:
			linked_sfdeis.append(di_obj)
			print(f"Added relation {sfdei} -> {di}")

		sfdei_obj["digitalOutput"] = linked_sfdeis

		db.commit()

		

		
		# add relation
		# if sfdei_obj not in di_obj.SFDEIs():
		# 	di_obj.SFDEIs().append(sfdei_obj)
		# 	print(f"Added relation {di} <-> {sfdei}")
		# else:
		# 	print(f"Relation {di} <-> {sfdei} already exists")


	# add_oks_relation_without_duplication(db.get_obj(args.clazz_s,args.uid_s),args.relation,db.get_obj(args.clazz_o,args.uid_o))
	# db.commit('')	
