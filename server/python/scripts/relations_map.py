import os
import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import fta
import messenger
import loadhelpers
import ProbTree
import traceback
import helper
import argparse

#import classes from loadhelpers
helpers=loadhelpers.LoadHelpers()
classes=helpers.classes



parser = argparse.ArgumentParser()
parser.add_argument("database",help="database file")
args=parser.parse_args()
print "Load database: %s" % args.database
db = config.Configuration("oksconfig:%s"%(args.database))
verbose=True
errors=""


def getOposites(rel, gatherers, feeders, ):
    if rel in gatherers:
        return "%s" % gatherers[rel].split("ProviderBase")[0]
    if rel in feeders:
        return feeders[rel].split("ProviderBase")[0]


#getting map relations
module={}
for c in classes:
    if c[1] is not "water": continue
    if c[3] is True:
        filename=c[1]
        classname=c[0]
        print ""
        print "Loading objects of class: %s" % filename
        module = __import__(c[1])
        help=getattr(module, dir(module)[0])()
        help.setDb(db)
        obj_list = {}
        if verbose is True: print "Getting all objects of %s" % classname
        obj_list = db.get_objs(classname)
        #print obj_list
        if verbose is True: print "Stablishing gatherer class relations..."
        gatherers={}
        feeders={}# do we need this?
        attrs=getattr(module, dir(module)[0])().attrs
        for at in attrs:
            if "gather" in attrs[at]:# or  "feed" in attrs[at] :
                gatherers[at]=attrs[at]["type"]
                #print attrs[at]
                pass
            if "feed" in attrs[at]:# or  "feed" in attrs[at] :
                feeders[at]=attrs[at]["type"]
                #print attrs[at]
                pass
            pass
        print "gatherers", gatherers
        print "feeders", feeders
        if verbose is True: print gatherers
        if verbose is True: print "Looping objects..................................."
        for obj in obj_list:
            if verbose is True: print "Object: %s" % (obj.UID())
            for g_rel in gatherers:
                print "1"
                for r in obj[g_rel]:
                    if verbose is True: print "[ %s--from-->%s ]" % (g_rel,  r.UID())
                    #counterpart=db.get_obj(classname, r.UID())
                    counterpart=r
                    #if verbose is True: print "counterpart:", counterpart#has to be of other class
                    ok=False
                    n_counters=getOposites(g_rel, gatherers, feeders)
                    print "n_counters if %s : %s " %  (g_rel, n_counters)
                    for feeder_rel in feeders:# parents with feeeder relation
                        print "ok:", ok
                        print "Feeders in [%s]: %i" % (feeder_rel, len(counterpart[feeder_rel]))
                        #print counterpart[feeder_rel]
                        #hay que hacer un loop con las relaciones que sean feeders
                        #y dentro de ella un loop con los elementos
                        #y dentro ver que coincida uno con el del elemento
                        
                        for candidate in counterpart[feeder_rel]:
                            print "candidate.UID():", candidate.UID()
                            if candidate.UID() == obj.UID():
                                ok=True
                                pass
                            pass
                        print "ok:", ok
                        if ok ==False:
                            print   "Obj %s is missing %s ****************** " % (obj.UID(), feeder_rel)
                            pass
                        '''
                            if ok == False:
                            print "OK IS FALSE!"
                            if verbose is True: print "Obj %s is missing %s ****************** " % (obj.UID(), r.UID())
                            errors+=)
                            pass
                        '''
                        pass
                    pass
                pass
            pass
        pass
    pass
print "ERRORS:"
print errors
print "END"
'''
    
    ecs = filter(lambda e: e.UID().startswith("TRT_E"), db.get_objs("SubDetector"))
    
    hp_rack = rack.RackHelper()
    hp_rack.setDb(db)

    sw=[]
    for c in classes:
    if c[3] is True:#class is switchable
    sw.append(c[1])
    pass
    pass
    

relations={}
for relation in abs_relations:
	relations[relation]=["%sProviderBase" % relation, "%sReceiverBase" % relation]
	pass
print relations["Container"][0]


obj_list = {}
objs_pool[tid] = conn_pool[tid].get_objs(relations["Container"][0])
for obj in objs_pool[tid]:
    obj_list[obj.UID()]=obj.UID()
    pass


print obj_list

'''
