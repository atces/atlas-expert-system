#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db=config.Configuration("oksconfig:%s"%(args.database))

    actions=db.get_objs("Action")
    nodsu=[]
    for action in actions:
        print "Process action: %s" % action.UID()
        action["state"]="on"
        action["switch"]="on"
        '''
        dsu=action["DSU"]
        if dsu:
            print " ==> Already assigned: %s" % dsu.UID()
            continue
        if action["DSUnumber"]<1 or action["DSUnumber"]>7:
            nodsu.append(action.UID())
            print " ==> Missing DSU"
            continue
        print " %s %s.DSUnumber=%s" % (action.class_name(),action.UID(),action["DSUnumber"])
        obj=db.get_obj("DSU","DSU_%i"%action["DSUnumber"])
        print obj.UID()
        action["DSU"]=obj
        '''
        pass

    db.commit("")
    sys.exit(0)
