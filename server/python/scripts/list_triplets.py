#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# List tripplets where the uid, classes and relations listed are involved


import config
import logging
import argparse
import helper
import json
import re

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='List tripplets where the uid, classes and relations listed are involved')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid',nargs="+",required=False,help="UID(s) of the object")
	parser.add_argument('-c','--classes',nargs="+", required=False,help="Classes of the objects")
	parser.add_argument('-r','--relations',nargs="+", required=False,help="Relationships to be considered.", default=[])
	parser.add_argument('-a','--attributes',type=json.loads, required=False,help="Attributes to be considered.",default={})
	args = parser.parse_args()

	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	helper=helper.Helper(db=db)
	fix_counter=0
	unresolved_error_counter=0
	_classes= db.classes() if not args.classes else args.classes
	if not args.classes and args.uid: 
		class_per_uid={}
		for uid in args.uid:
			for clazz in db.classes(): 
				if db.test_object(clazz,uid): class_per_uid.update({uid:clazz}); break 
		_classes=class_per_uid.values()
	elif args.classes and args.uid:
		class_per_uid={}
		for clazz in args.classes:
			objects=db.get_objs(clazz)
			for obj in objects:
				for uid in args.uid:
					if re.match(uid,obj.UID()): class_per_uid.update({obj.UID():obj.class_name()})
	for clazz in _classes:
		objects=db.get_objs(clazz) if not args.uid else [db.get_obj(clazz,uid) for uid,_clazz in class_per_uid.items() if _clazz==clazz] 
		for obj in objects:
			_relations=obj.__schema__['relation'] if args.relations==[] and args.attributes=={} else args.relations
			for relation in	_relations:
				if relation in obj.__schema__['relation']:  
					objs_in_relation=obj[relation] if isinstance(obj[relation],list) else [obj[relation]]
					for obj_in_relation in objs_in_relation:
						print("%s>%s>%s"%(obj.UID(),relation,obj_in_relation.UID()))
			_attributes={ _att:".*" for _att in obj.__schema__['attribute']} if args.relations==[] and args.attributes=={} else args.attributes
			triples_matching={}
			for att in _attributes.keys():
				if att in obj.__schema__['attribute']:  
					values=obj[att] if isinstance(obj[att],list) else [obj[att]]
					for value in values:
						value=str(value)
						if re.match(_attributes[att],value): 
							if att in triples_matching: triples_matching[att]+="\n"
							triples_matching[att]="%s>%s>%s"%(obj.UID(),att,value)
				else:
					if att in triples_matching: del(triples_matching[att])
					break
				if not att in triples_matching: triples_matching={}; break
			for att in triples_matching:
				print(triples_matching[att])
