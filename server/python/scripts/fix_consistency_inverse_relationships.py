#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Fixes the consistency in the inverse relationships


import config
import logging
import argparse
import helper

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Fixes the consistency in the inverse relationships')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid',nargs="+",required=False,help="UID(s) of the object for fixing the consistency.")
	parser.add_argument('-c','--classes',nargs="+", required=False,help="Classes of the objects for fixing the consistency. All objects of the class are evaluated.")
	parser.add_argument('-r','--relations',nargs="+", required=False,help="Relationships to be considered.")
	args = parser.parse_args()

	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	helper=helper.Helper(db=db)
	fix_counter=0
	unresolved_error_counter=0
	_classes= db.classes() if not args.classes else args.classes
	if not args.classes and args.uid: 
		class_per_uid={}
		for uid in args.uid:
			for clazz in db.classes(): 
				if db.test_object(clazz,uid): class_per_uid.update({uid:clazz}); break 
		_classes=class_per_uid.values()
	for clazz in _classes:
		objects=db.get_objs(clazz) if not args.uid else [db.get_obj(clazz,uid) for uid,_clazz in class_per_uid.items() if _clazz==clazz] 
		for obj in objects:
			for relation in	obj.__schema__['relation']:
				if not args.relations or relation in args.relations:  
					objs_in_relation=obj[relation] if isinstance(obj[relation],list) else [obj[relation]]
					for obj_in_relation in objs_in_relation:
						inverse_relation=helper.get_inverse_relation(relation)
						if inverse_relation and obj_in_relation:
							if not isinstance(inverse_relation,list):
							    if isinstance(obj_in_relation[inverse_relation],list): 
							        exist_inverse_relation= obj.UID() in [_obj.UID() for _obj in obj_in_relation[inverse_relation]]
							    else:
   							        exist_inverse_relation= obj.UID() == obj_in_relation[inverse_relation].UID()
							else:
								exist_inverse_relation=False
								for one_inverse_relation in inverse_relation:
									if one_inverse_relation in obj_in_relation.__schema__['relation'] and obj.UID() in [_obj.UID() for _obj in (obj_in_relation[one_inverse_relation] if isinstance(obj_in_relation[one_inverse_relation],list) else [obj_in_relation[one_inverse_relation]]) ]: exist_inverse_relation=True; break
							if not exist_inverse_relation:
								while True:
									option=input("The inverse relation for \033[1m %s %s %s \033[0m is not present. Expected: %s \n Do you want to [r]emove this relation or [a]dd the inverse relation?\n [r]emove | [a]add | [i]gnore | [s]ave: "%(obj.UID(),relation,obj_in_relation.UID(),inverse_relation))
									if option.lower()=='a' or option.lower()=='add':
										if isinstance(inverse_relation,list):
											while True:
												inverse_relation_option=input("Select the inverse relation to use:\n %s \n"%"\n".join(["%i) %s"%(i,relation) for i,relation in enumerate(inverse_relation)]))
												if inverse_relation_option.isdigit() and int(inverse_relation_option)<len(inverse_relation):
													i=int(inverse_relation_option)
													obj_in_relation[inverse_relation[i]]+=[obj]
													logger.debug("\033[1m %s %s %s \033[0m added.\n "%(obj_in_relation.UID(),inverse_relation[i],obj.UID()))
													break
												else:
													logger.error("Invalid input. Must be the number of the inverse relation.")
											
										else:
											obj_in_relation[inverse_relation]+=[obj]
											logger.debug("\033[1m %s %s %s \033[0m added. \n"%(obj_in_relation.UID(),inverse_relation,obj.UID()))
										fix_counter+=1
										break
									elif option.lower()=='r' or option.lower()=='remove': 
										obj[relation].remove(obj_in_relation)
										obj[relation]=obj[relation]
										logger.debug("\033[1m %s %s %s \033[0m removed. \n"%(obj.UID(),relation,obj_in_relation.UID()))
										fix_counter+=1
										break
									elif option.lower()=='i' or option.lower()=='ignore' or option=="": 
										unresolved_error_counter+=1
										logger.info("\n")
										break
									elif option.lower()=='s' or option.lower()=='save':
										db.commit()
										logger.info("%i inverse relations have been fixed and there are %i inverse relations to fix in the future."%(fix_counter,unresolved_error_counter))
										logger.debug("Saving the changes...\n")
									logger.error("Invalid input.")
	logger.info("%i inverse relations have been fixed and there are %i inverse relations to fix in the future."%(fix_counter,unresolved_error_counter))
	logger.debug("Saving the changes...")
	db.commit()



