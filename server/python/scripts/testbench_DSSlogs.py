#!/usr/bin/env python
#######################################################
# Testbench for DSSLogs
# Florian.Haslbeck@cern.ch
# November 2023
#######################################################


def find_DSSLogs(db, verbose=False):
    """ find all DSSLogs in database
        input:
            db: database object
        output:
            list of DSSLog objects
    """
    # get all DSSLogs
    DSSLogs = db.get_objs("DSSLog")
    if verbose: print(f"Found {len(DSSLogs)} DSSLogs")

    # filter DSSLogs
    # DSSLogs = [dl for dl in DSSLogs if "log_" in dl.UID()]
    # sort by name
    # DSSLogs = sorted(DSSLogs, key=lambda x: x.UID())
    return DSSLogs





def find_DSSLog_by_alarm(db, alarm, verbose=False):
    """ find all DSSLogs in database that have alarm as name
        input:
            alarm: alarm name
        output:
            list of DSSLog objects
    """
    def _find_obj(obj_name, obj_type):
        """ find object of type obj_type and name obj_name in database
            return None if not found
        """
        try:
            retr_obj = db.get_obj(obj_type, obj_name) # this is an object of class db
        except:
            retr_obj = None
        return retr_obj

    # get the es_alarm
    es_alarm = _find_obj(alarm, "Alarm")

    # get the DSSLogs
    logs = es_alarm["logs"]

    return logs



    

def sort_logs_by_date(dsslogs):
    """ sort DSSLogs by date string 2023-04-01 06:09:40
        input:
            dsslogs: list of DSSLog objects
        output:
            list of DSSLog objects
    """
    # sort DSSLogs by date
    dsslogs = sorted(dsslogs, key=lambda x: x.get_string("time"))
    return dsslogs


if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    # find all DSSLogs in database
    dsslogs = find_DSSLogs(db, verbose=args.verbose)

    # print all DSSlogs with their attributes for "AL_GAS_ID_IDEPFlushing_AIR"
    alarm = "AL_GAS_ID_IDEPFlushing_AIR"
    logs = find_DSSLog_by_alarm(db, alarm, verbose=args.verbose)

    for l in logs:
        print(l.UID(), l["name"], l["time"], l["alarm"])

    # sort DSSLogs by date
    # matching = sort_logs_by_date(matching)

    # print DSSLogs
    # for dsslog in matching:
        # print(f"{dsslog.get_string('time')} {dsslog.get_string('name')} {dsslog.get_string('came')}")


    # # # # # 
    # verifiy that all DSSLogs are linked to an alarm
    # # # # # 
    # find all DSSLogs in database
    not_linked = []
    dsslogs = find_DSSLogs(db, verbose=args.verbose)
    for dsslog in dsslogs:
        # print(dsslog.UID(), dsslog["alarm"])

        if dsslog["alarm"] is None:
            not_linked.append(dsslog)

    print("___")
    print(f"not linked: {len(not_linked)} DSSLogs")
    for nl in not_linked: 
        print(nl, nl["name"])

    
    # # # # # # 
    # # verifiy that all Alarms are linked to Logs
    # # # # # # 
    # # find all DSSLogs in database
    log_alarm_counter = {}
    not_linked = []
    dsslogs = find_DSSLogs(db, verbose=args.verbose)
    for dsslog in dsslogs:
        if dsslog["alarm"] is None: continue
        # print("* * ", dsslog.UID())

        # if not "log_" in dsslog.UID(): 
        #     print("* * skipping old log")
        #     continue
        # count occurences of alarm
        alarm = dsslog["alarm"]
        alarm = alarm.UID()
       
    
        
        if alarm not in log_alarm_counter.keys():
            log_alarm_counter[alarm] = {"count": 0, "logs": []}
        log_alarm_counter[alarm]["count"] += 1
        log_alarm_counter[alarm]["logs"].append(dsslog.UID())

    # print(log_alarm_counter)

    not_matching = []
    miss_match   = []

    alarms = db.get_objs("Alarm")
    for alarm in alarms:
        # print(alarm.UID(), len(alarm["logs"]))

        # skip alarms that are not in log_alarm_counter
        if alarm.UID() not in log_alarm_counter.keys(): continue

        # remove old logs
        log_es_alarm = [log.UID() for log in alarm["logs"]] # if "log_" in log.UID()]

        # check if number of logs is correct
        if len(log_es_alarm) != log_alarm_counter[alarm.UID()]["count"]:
            print(alarm.UID(), ",", 
                  len(log_es_alarm), ",", log_alarm_counter[alarm.UID()]["count"])
            not_matching.append(alarm)

            linked_logs   = log_alarm_counter[alarm.UID()]["logs"][:]

            miss_match.append([log_es_alarm, linked_logs])


    print("___")
    print(f"not matching: {len(not_matching)} DSSLogs")
    for nl in not_matching: 
        print(nl)


    print("___")
    print(f"miss match: {len(miss_match)} DSSLogs")
    for es, link in miss_match:
        print(es.UID())
        l_es = [l.UID() for l in es["logs"]]
        l_es = sorted(l_es)

        l_l  = sorted(link)
        
        for l in l_es:
            print("ES Alarm links to ", l)
        print(". . . .")
        for l in l_l:
            print("Counted linked to ", l)
