#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re

    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import pipe
    import ventilation

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_pipe=pipe.PipeHelper()
    hp_pipe.setDb(db)
    
    hp_vs=ventilation.VentilationSystemHelper()
    hp_vs.setDb(db)
    
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)


    ls=[
        "FPAC-03184",
        "L1Calo_Subdetector",
        "FTK_Subdetector",
        "Pixel_Subdetector",
        "SCT_Subdetector",
        "TRT_Subdetector",
        "Tile_Subdetector",
        "LAR_Subdetector",
        "MDT_Subdetector",
        "RPC_Subdetector",
        "CSC_Subdetector",
        "TGC_subdetector",
    ]

    for u in ls:
        
        u=db.get_obj("Group",u)
    	u.set_enum("SummaryGroup","yes")
        '''
        j_g={"UID":group, "SummaryGroup":"yes"}
        hp_g.addObj(j_g, False, True)
        '''
        pass
    db.commit("")
