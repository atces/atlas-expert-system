#!/usr/bin/env python
# @author gustavo.uribe@cern.ch
# Display the electrical description of the rack
	

import config
import logging
import argparse
import helper

#Replaces '*','/' by '_' and removes '.' and '-'  	
def get_normalized_name(name):
	return name.replace('/','_').replace('*','_').replace('.','').replace('-','')

if __name__ == '__main__':
	logging.basicConfig(format='%(asctime)-15s %(message)s')
	logger=logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	parser = argparse.ArgumentParser(description='Fixes the consistency in the inverse relationships')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-u','--uid',nargs="+",required=False,help="UID(s) of the object for fixing the consistency.")
	args = parser.parse_args()

	logger.debug("Loading database file...")        
	db=config.Configuration("oksconfig:"+args.database)
	logger.debug("Printing result")
	print("Location;%sRack;%sSubsytem;%sTurbine;%sNormal Power;%sDiesel Power;%sSmoke Sensor;"%(" "*(15-len("Location"))," "*(15-len("Rack"))," "*(15-len("Subsystem"))," "*(30-len("Turbine"))," "*(30-len("Normal Power"))," "*(30-len("Smoke Sensor"))))
	objects=[db.get_obj("Rack",uid) for uid in args.uid] 
	for obj in objects:
		turbine=None
		normal=None
		persistent=None
		smoke=None
		rack_name=obj.UID()
		for element in obj['isDependantContainerOf']:
			if 'Turbine' in element.UID(): 
				turbine=element.UID()
				smoke=[smoke.UID() for smoke in element['contains'] if smoke.class_name() == "SmokeSensor" ]				 
				if len(smoke)==1: smoke=smoke[0]
			if 'EXJ' in element.UID() or 'EXD' in element.UID() or 'EBD' in element.UID():
				normal=[element.UID()] if not normal else  normal+[element.UID()]
			if 'EOD' in element.UID() or 'ESD' in element.UID() or 'EOJ' in element.UID() or 'ESA' in element.UID() or 'ESJ' in element.UID():
				persistent=[element.UID()] if not persistent  else persistent+[element.UID()]
		print("%s%s%s%s%s%s%s"%(obj["location"]+";"+" "*(15-len(obj["location"])),obj.UID()+";"+" "*(15-len(obj.UID())),obj["subsystem"]+";"+" "*(15-len(obj['subsystem'])),str(turbine)+";"+" "*(30-len(str(turbine))) if turbine else ";"+" "*30, str(normal)+";"+" "*(30-len(str(normal))) if normal else ";"+" "*30,str(persistent)+";"+" "*(30-len(persistent)) if persistent else ";"+" "*30,str(smoke)+" " if smoke else " " ))

