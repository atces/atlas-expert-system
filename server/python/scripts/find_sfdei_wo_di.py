#!/usr/bin/env python
# @author florian.haslbeck@cern.ch
# January 2024
# find all SFDEI without DI and vice versa


import argparse
import config


def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-db", "--database", help="Path of the database file", required=False, default="../../../data/sb.data.xml")
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)

	# get all SFDEI 
	sfdeis = db.get_objs("SmokeSensor")

	# iterate over all di_sfdei relations
	no_di = []

	for sfdei in sfdeis:
		if not "SFDEI" in sfdei.UID() : continue


		if len(sfdei["digitalOutput"]) > 0: continue
		# print(sfdei.UID(), sfdei["digitalOutput"])
		no_di.append([sfdei.UID(), sfdei["digitalOutput"]])


	for sfdei in sorted(no_di): 
            print(sfdei[0], len(sfdei[1]))
		

		

