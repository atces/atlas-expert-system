#!/usr/bin/env python
#######################################################
# add relation DSSLog to alarm  
# Florian.Haslbeck@cern.ch
# November 2023
#######################################################


def find_DSSLogs(db, verbose=False):
    """ find all DSSLogs config_objs in database
        input:
            db: database object
        output:
            list of DSSLog objects
    """
    # get all DSSLogs
    config_objs = db.get_objs("DSSLog")
    if verbose: print(f"Found {len(config_objs)} DSSLogs")

    # filter DSSLogs
    config_objs = [dl for dl in config_objs if "log_" in dl.UID()]
    return config_objs

def get_obj(db, _type, _name, verbose=False):
    """ get object of type _type and name _name from database
        return None if not found
    """
    try:
        retr_obj = db.get_obj(_type, _name) # this is an object of class db
    except:
        retr_obj = None
    return retr_obj



if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    not_linked = []

    # start_date = "2018-01-02"
    # end_date   = "2023-04-22"

    # start_date = "2023-04-22"
    # end_date   = "2024-03-11"

    start_date = "2023-03-11"
    end_date   = "2024-04-10"


    # get DSSLogs from database
     # load oks database and get classes and helpers
    db  = config.Configuration("oksconfig:%s"%(args.database))

    dsslogs_config_objs = find_DSSLogs(db, verbose=args.verbose) # this config_objs


    # for dsslog in dsslogs_config_objs:
    #     print(dsslog)



    # create DSSLogs
    for i, log in enumerate(dsslogs_config_objs):

        print(f"Processing {log} {i}/{len(dsslogs_config_objs)}")

        
        # which alarm
        alarm = log["name"]
        

        # get ES alarm
        es_alarm = get_obj(db,"Alarm", alarm)

        print("DSSLog", log, "Alarm", alarm, "ES Alarm", es_alarm)
        print("DSSLog", type(log), "Alarm", type(alarm), "ES Alarm", type(es_alarm))

        if es_alarm is not None and log is not None:
            existing_logs = es_alarm["logs"]
            if log not in existing_logs: 
                existing_logs.append(log)
                print("Linked log", log.UID(), "to alarm", alarm, es_alarm)
            else:
                print("Log", log.UID(), "already linked to alarm", alarm)
            es_alarm["logs"] = existing_logs
            
        else:
            not_linked.append([log, alarm, es_alarm])

            print(log, alarm, es_alarm)

            print("Could not link log", log, "to alarm", alarm, es_alarm)

        if i % 100 == 0:
            db.commit()
            print("Committing")
    
    db.commit()

    print("Not linked:")
    for nl in not_linked:
        print(nl)
