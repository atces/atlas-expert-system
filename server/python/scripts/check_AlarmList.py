#!/usr/bin/env python
# every delayed action 

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print("Load database: %s" % args.database)
    db = config.Configuration("oksconfig:%s"%(args.database))
    gLH = loadhelpers.LoadHelpers(db)
    classes=gLH.getClasses()
    helps=gLH.getHelpers()
    
    

    alarms_db = db.get_objs("Alarm")
    di_db = db.get_objs("DigitalInput")

    hpdi = digitalinput.DigitalInputHelper(db)
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper(db)
    hpal.setDb(db)
    hpac = action.ActionHelper(db)
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper(db)
    hpda.setDb(db)
    # f = open("MissingDigitalInputs.txt", "w")

    counter=0
    more={}
    less={}
    newda={}
    problems={}


    #clear alarms
    for al in alarms_db:
        objs_di=al["digitalInput"]
        for di in  objs_di:
            di_obj={
                "UID":di.UID(),
                "triggers":al.UID()
            }
            #hpdi.addObj(di_obj, True, False)
            al_obj={
                "UID":al.UID(),
                "digitalInput":di.UID()
            }
            #hpal.addObj(al_obj, True, False)
            pass
        pass
    
    with open("AlarmList.txt", 'r') as file:
        data="".join(line for line in file)
        alarms_text=data.split("===========================================================")
        processed_alarms=0
        #print len(alarms_text)
        for block in alarms_text:
            al_name=""
            di_names=[]
            ac_names=[]
            actions_in_file=[]
            #Get file alarm, its DIs and its actions
            for line in block.split("\n"):
                #print line
                if line.startswith("ALARM  AL_"):
                    al_name= next((i) for i in line.split(" ") if "AL_" in i)
                    processed_alarms+=1
                    pass
                if " DI_" in line:
                    di_names.append(next((i) for i in line.split(" ") if "DI_" in i))
                    pass
                if " O_" in line:
                    actions_in_file.append(next((i) for i in line.split(" ") if "O_" in i)+"_"+line.strip().split("(delay ")[1].split(")")[0])
                    #print line.strip().split(" ")[0],"--",line.strip().split(" ")[1]
                    pass

            #print action_in_files
            #print di_names
            if al_name!= "":
                #Get corresponding db alarm
                try:
                    db_alarm=db.get_obj("Alarm", al_name)
                    db_alarm_ser= helps["Alarm"].serialize(db_alarm)
                    #print("DB:", db_alarm_ser["actions"]
                    #make sure objects are in db
                    
                    #Loop actions of alarm in file
                    db_alarm_ser["actions"].pop(0)#first element is always empty
                    file_actions={}
                    db_actions={}
                    present_in_file_not_in_db={}
                    present_in_db_not_in_file={}

                    for action_in_file in actions_in_file:
                        #if action_in_file[0]+"_"+action_in_file[1] not in db_alarm_ser["actions"]:
                        file_actions[action_in_file]=action_in_file
                        if action_in_file not in db_alarm_ser["actions"]:
                            #action is there, but with wrong delay
                            #print("-----------------ERROR1------Action from file not in db --------------------->",action_in_file
                            present_in_file_not_in_db[action_in_file]=action_in_file

                            pass
                        pass
                    for action in db_alarm_ser["actions"]:
                        #print action
                        if action!="":
                            db_actions[action]=action
                            if action not in actions_in_file:
                                present_in_db_not_in_file[action]=actions_in_file
                                pass
                            pass
                        pass
                    #if len(present_in_file_not_in_db)>0 or len(present_in_db_not_in_file)>0: 
                    if len(present_in_file_not_in_db)>0:
                        print("----------------------------------Alarm from file: ", al_name)
                        print("actions_in_file:")
                        print(actions_in_file)
                        print("actions in db:")
                        print(db_alarm_ser["actions"])
                        print("present_in_file_not_in_db: ", present_in_file_not_in_db)
                        pass
                    if len(present_in_db_not_in_file)>0:
                        print("----------------------------------Alarm from file: ", al_name)
                        print("actions_in_file:")
                        print(actions_in_file)
                        print("actions in db:")
                        print(db_alarm_ser["actions"])
                        print("!!!present_in_db_not_in_file!!!:", present_in_db_not_in_file)
                        pass
                    if len(db_actions)>len(file_actions):
                        more[al_name]=len(db_actions)-len(file_actions)
                        pass
                    if len(db_actions)<len(file_actions):
                        less[al_name]=len(db_actions)-len(file_actions)
                        pass
                    '''
                    for delayedactionname in present_in_file_not_in_db:
                        cont=False
                        try:
                            
                            actioname=delayedactionname.replace("_"+delayedactionname.split("_")[-1:][0],"")
                            #print("actioname", actioname
                            delay=delayedactionname.split("_")[-1:][0]
                            db_delayedAction=db.get_obj("DelayedAction", delayedactionname)
                            #checking action exists
                            dbactionobject=db.get_obj("Action", actioname)
                            cont=True
                            pass
                        except:
                            print("CANNOT ADD actionname [%s] to alarm [%s] " % (actioname, al_name)
                            pass
                        if cont==True:
                            #print("Adding delayed action to alarm"
                            #print("db_delayedAction.UID()", db_delayedAction.UID()
                            
                            #print("delay", delay
                            obj_da={
                                    "UID":delayedactionname,
                                    "action":actioname,
                                    "delay":delay, 
                                    "alarms":al_name,
                                    "switch":"on",
                                    "state":"on"
                                    #action is missing!!
                                }
                            obj_al={
                                "UID":al_name,
                                "actions":delayedactionname,
                            }
                            obj_ac={
                                    "UID":actioname,
                                    "delayedactions":delayedactionname,
                                    "switch":"on",
                                    "state":"on"
                                    #action is missing!!
                                }
                            #hpda.addObj(obj_da,False,True)
                            #hpal.addObj(obj_al,False,True)
                            #hpac.addObj(obj_ac,False,True)
                            print("GOOD"
                            pass
                        pass
                    
                    for delayedactionname in present_in_db_not_in_file:
                        cont=False
                        try:
                            
                            actioname=delayedactionname.replace("_"+delayedactionname.split("_")[-1:][0],"")
                            #print("actioname", actioname
                            delay=delayedactionname.split("_")[-1:][0]
                            #db_delayedAction=db.get_obj("DelayedAction", delayedactionname)
                            #checking action exists
                            #dbactionobject=db.get_obj("Action", actioname)
                            cont=True
                            pass
                        except:
                            print("CANNOT remove actionname [%s] to alarm [%s] " % (actioname, al_name)
                            pass
                        if cont==True:
                            print("Removing delayed action [%s] from alarm [%s]"  % (actioname, al_name)
                            #print("db_delayedAction.UID()", db_delayedAction.UID()
                            
                            #print("delay", delay
                            obj_da={
                                    "UID":delayedactionname,
                                    "alarms":al_name,
                                    #action is missing!!
                                }
                            obj_al={
                                "UID":al_name,
                                "actions":delayedactionname,
                            }
                            obj_ac={
                                    "UID":actioname,
                                    "delayedactions":delayedactionname,
                            }
                            hpda.addObj(obj_da,True,False)
                            hpal.addObj(obj_al,True,False)
                            hpac.addObj(obj_ac,True,False)
                            print("GOOD"
                            pass
                        pass
                    pass
                '''

                except Exception as e:
                    print("%s does not exist in ES!" % al_name)
                    print(e)
                    pass
                pass
            pass
        #print alarms_text[1]
        pass
    #db.commit("")
    print(counter)
    print("More:")
    for m in more:
        print(m)
    print("less")
    for l in less:
        print(l)
    print("Actions in file but not in db: ", problems)
    print("DelayedActions in file that are not in db", newda)
    print("New actions to be created:", problems)
    print("present_in_file_not_in_db", present_in_file_not_in_db)
    print("present_in_file_not_in_db", len(present_in_file_not_in_db))

    pass
