#!/usr/bin/env python
# every delayed action 

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import messenger
    import loadhelpers
    import ProbTree
    import traceback
    import helper
    import action
    import delayedaction
    #import classes from loadhelpers
    helpers=loadhelpers.LoadHelpers()
    classes=helpers.classes
    helps=loadhelpers.LoadHelpers().getHelpers()

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    alarms_db = db.get_objs("Alarm")
    #di_db = db.get_objs("DigitalInput")
    #db_delayedactions = db.get_objs("DelayedAction")
    db_actions = db.get_objs("Action")

    hpdi = digitalinput.DigitalInputHelper()
    hpdi.setDb(db)
    hpal = alarm.AlarmHelper()
    hpal.setDb(db)
    hpac = action.ActionHelper()
    hpac.setDb(db)
    hpda = delayedaction.DelayedActionHelper()
    hpda.setDb(db)
    
    counter=0
    more=0
    less=0
    newda={}
    problems={}

    '''
    #Delayed actions
    for ac in db_delayedactions:
        print ac.UID()
        da_obj={
            "UID":ac.UID(),
            "switch":"on",
            "state":"on"
        }
        hpda.addObj(da_obj, False, True)
        pass
    '''
    #Delayed actions
    for ac in db_dactions:
        print ac.UID()
        ac_obj={
            "UID":ac.UID(),
            "switch":"on",
            "state":"on"
        }
        hpac.addObj(ac_obj, False, True)
        pass
    #db.commit("")
    pass
