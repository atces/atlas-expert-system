#!/usr/bin/env python
# @author gustavo.uribe@cern.ch

import config
import loadhelpers
import networkx as nx
import argparse
import csv
from datetime import datetime
from statistics import mean
from graphhelper import GraphHelper



if __name__== "__main__":
	parser = argparse.ArgumentParser(description='Test the MPC tool. Result of the test are saved in mpc_[num_test]_test_results_[algorithm_parameters].csv')
	parser.add_argument('-db','--database', required=False, help='database file path to be updated', default="../../../data/sb.data.xml")
	parser.add_argument('-fs','--faulty-systems', required=False, type=int, help='Number of faulty systems consider by the MPC. Grater number will cause slower performance.', default=1)
	parser.add_argument('-n','--num-test', required=False, type=int, help='Number of test to perform. Each test corresponds with the possible system failure.', default=100)
	parser.add_argument('-c','--clazz', required=False, help='Class to be consider as input for the MPC', default="Alarm")
	parser.add_argument('-ne','--non-exhaustive', required=False, help='Texting MPC no exhaustive mode.', action="store_true",default=False)
	args = parser.parse_args()

	db=config.Configuration("oksconfig:%s"%args.database)
	gLH = loadhelpers.LoadHelpers(db)
	classes=gLH.getClasses()
	searchableClasses=gLH.getSearchableClasses()
	sysclasses=gLH.getSysClasses()
	helpers=gLH.getHelpers()
	graph_helper=GraphHelper(None,None,helpers,classes)
	graph_helper.create_graph(db)
	graph_helper.generate_feeding_graph()
	graph_helper.calculate_granularity_per_subsystem()
	graph_helper.calculate_out_centrality()
	graph_helper.calculate_centrality_per_subsystem()
	nodes_sorted_by_centrality=sorted(graph_helper.out_centrality_dict.items(), key=lambda x: x[1], reverse=True)
	#nodes_sorted_by_centrality=[("EOD1_1DX",1)]
	
	counter_test=0
	max_counter=args.num_test
	counter_empty_MPC=0
	counter_incomplete_MPC=0
	csv_output=[]
	nodes_sorted_iter=iter(nodes_sorted_by_centrality)


	for faulty_uid,centrality in nodes_sorted_iter:  
		if not db.test_object("SystemBase",faulty_uid) or db.test_object("DigitalInput",faulty_uid): continue
		print("Looking for alarms in faulty object %s."%faulty_uid)
		faulty_obj=graph_helper.get_object(db,faulty_uid)
		faulty_obj['switch']="off"
		reasons,affected=graph_helper.propagate_change_effect(db,faulty_obj)
		objs_off=[obj for obj in affected if db.test_object(args.clazz,obj) and graph_helper.get_object(db,obj)["state"]=="off"]
		average_centrality_objs=mean([graph_helper.out_centrality_dict[obj] for obj in objs_off]) if len(objs_off)>0 else 0
		print("Objects off=%s"%objs_off)
		if args.non_exhaustive and len(objs_off)>0: print("%s removed from affected list."%objs_off.pop())
		db.abort()
		if len(objs_off)==0: continue	
		t_start=datetime.now() 	
		MPC,parameters=graph_helper.find_MPC(db,objs_off,faulty_systems=args.faulty_systems,is_exhaustive=not args.non_exhaustive,return_complexity_params=True)	
		tdelta = datetime.now()-t_start
		if len(MPC)==0: counter_empty_MPC+=1; precision=0; recall=0
		else: precision=1/len(MPC); recall=1
		if faulty_uid not in MPC: counter_incomplete_MPC+=1; precision=0; recall=0
		entry={"class":faulty_obj.class_name(),"faulty_obj":faulty_uid,"recall":recall,"precision":precision,"F4-score":17*precision*recall/(precision*16+recall) if not recall==0 else 0,"tdelta":tdelta.total_seconds(), "number_obj_off":len(objs_off), "centrality_faulty_obj":centrality, "avg. centrality objects off":average_centrality_objs,"number_iteraciones":parameters['total_iterations'],"number_of_predecessors":parameters['number_of_predecessors'],"reciprocal_rank":(1/(MPC.index(faulty_uid)+1)) if faulty_uid in MPC else 0,"result":MPC,"data_per_predecessor":parameters['data_per_predecessor']}
		csv_output.append(entry)
		print(entry)
		counter_test+=1
		if counter_test==max_counter: break
		for i in range(1): next(nodes_sorted_iter,None)
	print("Total of MPC empty are %i."%counter_empty_MPC)
	print("Total of incomplete MPCs are %i."%counter_incomplete_MPC)
	with open('mpc_%i_test_results_%s_%i_mfaulty_systems_%i_trys_eigenvector_shortest_path_length_%s_.csv'%(args.num_test,args.clazz,args.faulty_systems,"non_exhaustive" if args.non_exhaustive else "exhaustive"), 'w', newline='') as csvfile:
		fieldnames = csv_output[0].keys()
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

		writer.writeheader()
		for entry in csv_output:
			writer.writerow(entry)
	print("Results saved in file %s."%csvfile.name)
