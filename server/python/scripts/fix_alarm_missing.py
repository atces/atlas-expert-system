#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    import alarm
    import digitalinput
    import delayedaction
    import action

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))

    actions_db = db.get_objs("Action")
    di_db = db.get_objs("DigitalInput")

    hp1 = digitalinput.DigitalInputHelper()
    hp1.setDb(db)
    hp2 = alarm.AlarmHelper()
    hp2.setDb(db)
    hp3 = delayedaction.DelayedActionHelper()
    hp3.setDb(db)
    hp4 = action.ActionHelper()
    hp4.setDb(db)

    missing_alarms = []
    with open("MissingAlarms.txt", 'r') as file:
        try:
            while True:
                missing_alarms.append(next(file).strip())
        except StopIteration:
            file.close()

    digital_inputs = []
    actions = []
    with open("DigitalInputs_Summary_panel.txt", 'r') as file:
        try:
            line = next(file)
            while True:
                line = next(file)
                if not line.startswith("DI_") and not line.startswith("O_"):
                    continue
                line_split = line.split()

                name = line_split[0]
                elem_type = line_split[1]
                DSU = line_split[2]
                crate = line_split[3]
                module = int(line_split[4])
                slot = int(line_split[5])
                num_alarms = line_split[6]

                o = {
                    "name": name,
                    "elem_type": elem_type,
                    "DSU": "DSU_%s" % DSU[3],
                    "crate": crate,
                    "module": module,
                    "slot": slot,
                }
                if num_alarms != "--":
                    o["num_alarms"] = num_alarms

                if name.startswith("DI_"):
                    digital_inputs.append(o)
                elif name.startswith("O_"):
                    actions.append(o)
        except StopIteration:
            file.close()


    # f = open("MissingDigitalInputs.txt", "w")
    with open("AlarmListFull.txt", 'r') as file:
        try:
            line = next(file)
            while line:
                line = next(file)
                if line.startswith("====="):
                    name_line = next(file).split()
                    name = name_line[1]
                    if not name in missing_alarms:
                        continue
                    #Adding the alarm with the description
                    print "--NEW ALARM (%s)--" % name   
                    next(file)
                    description = next(file)
                    obj0 = {
                        "UID": name,
                        "description": description,
                        "state": "on",
                        "switch": "on",
                    }
                    hp2.addObj(obj0,False,True)
                    #Adding the digital inputs
                    while line:
                        if "ALARM CONDITION" in line:
                            print "NEW DIGITAL INPUT"
                            di_line = next(file).strip()
                            while di_line != "":
                                if di_line.startswith("DI_"):
                                    print "DI"
                                    di_line_split = di_line.split()
                                    di = di_line_split[0]
                                    persistency = int(di_line_split[len(di_line_split)-1].replace(")",""))
                                    obj1 = {
                                        "UID": di,
                                        "triggers": name,
                                        "persistency": persistency
                                    }
                                    for i in digital_inputs:
                                        if di == i["name"]:
                                            obj1["slot"] = i["slot"]
                                            obj1["DSU"] = i["DSU"]
                                            obj1["crate"] = i["crate"]
                                            obj1["module"] = i["module"]
                                            break
                                    hp1.addObj(obj1,False,True)
                                    obj2 = {
                                        "UID": name,
                                        "digitalInput": di
                                    }
                                    hp2.addObj(obj2,False,True)
                                elif di_line.startswith("AI_"):
                                    print "AI"
                                    pass        #TODO: Process analog inputs
                                elif di_line.startswith("PT"):
                                    print "PT"
                                    pass        #TODO: I don't know what PT are
                                di_line = next(file).strip()
                            break
                        line = next(file)
                    #Adding the actions
                    while line:
                        line = next(file).strip()
                        if "ACTION(s)" in line:
                            print "NEW ACTION"
                            ac_line = next(file).strip()
                            while ac_line.startswith("O_"):
                                print "AC"
                                print ac_line
                                ac_line_split = ac_line.split()
                                ac = ac_line_split[0]
                                delay = int(ac_line_split[len(ac_line_split)-1].replace(")",""))
                                delayed_ac = "%s_%s" % (ac, delay)
                                obj1 = {
                                    "UID": ac
                                }
                                for i in actions:
                                    if ac == i["name"]:
                                        obj1["slot"] = i["slot"]
                                        obj1["DSU"] = i["DSU"]
                                        obj1["crate"] = i["crate"]
                                        obj1["module"] = i["module"]
                                        break
                                hp4.addObj(obj1,False,True)
                                obj2 = {
                                    "UID": delayed_ac,
                                    "action": ac,
                                    "alarms": name,
                                    "delay": delay
                                }
                                hp3.addObj(obj2,False,True)
                                obj3 = {
                                    "UID": ac,
                                    "delayedactions": delayed_ac
                                }
                                hp4.addObj(obj3,False,True)
                                obj4 = {
                                    "UID": name,
                                    "actions": delayed_ac
                                }
                                hp2.addObj(obj4,False,True)
                            
                                ac_line = next(file).strip()
                            break
                        elif "NO ACTION DEFINED" in line:
                            print "NOACT"
                            break
                    # break
        except StopIteration:
            file.close()
    db.commit("")
    
