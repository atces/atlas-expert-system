#!/usr/bin/env python
# Ignacio.asensi@cern.ch

if __name__ == '__main__':
    import digitalinput
    import os
    import sys
    import config
    import argparse
    import re
    import grouping
    import powersupply
    import interlock
    import crate
    import rack
    import gas
    import subdetector
    import alarm
    import coolingloop
    import group
    import pneumaticvalve
    import rack
    import pneumaticcontroller

    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="database file")
    parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print "Load database: %s" % args.database
    db = config.Configuration("oksconfig:%s"%(args.database))
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_al = alarm.AlarmHelper()
    hp_al.setDb(db)
    hp_cl = coolingloop.CoolingLoopHelper()
    hp_cl.setDb(db)
    hp_di = digitalinput.DigitalInputHelper()
    hp_di.setDb(db)
    hp_gas = gas.GasSystemHelper()
    hp_gas.setDb(db)
    hp_g = group.GroupHelper()
    hp_g.setDb(db)
    hp_pv = pneumaticvalve.PneumaticValveHelper()
    hp_pv.setDb(db)
    hp_r = rack.RackHelper()
    hp_r.setDb(db)
    hp_sd = subdetector.SubDetectorHelper()
    hp_sd.setDb(db)
    hp_pc=pneumaticcontroller.PneumaticControllerHelper()
    hp_pc.setDb(db)
    ######################################################
    #addObj(self,data,remove=False,extend=False):
    ######################################################
    #from list
    #ls=["PVA1_TRT", "PVA2_TRT", "PVA3_TRT", "PVA4_TRT"]
    #from db
    #ls=db.get_obj("SubDetector",ec.UID())
    #dbls = filter(lambda e: e.UID().startswith("TRT_"), db.get_objs("SubDetector"))
    
    #print ls
    #group="TRT_Subdetector"
	#l_racks=["Y.36-04.X7_return ","Y.36-03.X1_return","Y.51-24.X7_supply","Y.52-24.X7_return","Y.53-25.X1_return"]
    
    #ec=db.get_obj("SubDetector",ec.UID())
    
    #grouping from ls
    '''
    for u in l_racks:
        #group
        
        obj=db.get_obj("Rack",u)
        todeletes=obj["gasTo"]
        for todelete in todeletes:
            j_r={"UID":obj.UID(), "gasTo":todelete.UID()}
            hp_r.addObj(j_r, True, False)
            pass
        hp_g.addObj(j_g, False, True)
        
        #grouped
        #u_o=db.get_obj("PneumaticValve",u)
    	j_u={
            "UID":u,
            "groupedBy":group
        }
        hp_sd.addObj(j_u, False, True)
    


    #grouping from db
    for dbu in dbls:
        #group
        dbobj=db.get_obj("SubDetector",dbu.UID())
        u=dbobj.UID()
        j_g={"UID":group, "groupTo":u}
        hp_g.addObj(j_g, False, True)
        
        #grouped
        #u_o=db.get_obj("PneumaticValve",u)
        j_u={
            "UID":u,
            "groupedBy":group
        }
        hp_sd.addObj(j_u, False, True)
    '''
    #db.commit("")
    PCVA_description="Pnematic control valve. Cooling to detector "#v2
    PVA_description="Pnematic valve. Cooling out of detector "#v2

    uids=[["PL-LAr_PCVA2","PL-LAr_PVA2", "LAr_CoolingLoop_EMEC_A_B2"],
        ["PL-LAr_PCVA3","PL-LAr_PVA3", "LAr_CoolingLoop_EMEC_A_L2a"],
        ["PL-LAr_PCVA4","PL-LAr_PVA4", "LAr_CoolingLoop_EMEC_A_L2b"],
        ["PL-LAr_PCVA5","PL-LAr_PVA5", "LAr_CoolingLoop_EMEC_A_R2a"],
        ["PL-LAr_PCVA6","PL-LAr_PVA6", "LAr_CoolingLoop_EMEC_A_R2b"],
        ["PL-LAr_PCVA7","PL-LAr_PVA7", "LAr_CoolingLoop_EMEC_A_T3"],
        ["PL-LAr_PCVA8","PL-LAr_PVA8", "LAr_CoolingLoop_EMEC_C_B2"],
        ["PL-LAr_PCVA9","PL-LAr_PVA9", "LAr_CoolingLoop_EMEC_C_L2a"],
        ["PL-LAr_PCVA10","PL-LAr_PVA10", "LAr_CoolingLoop_EMEC_C_L2b"],
        ["PL-LAr_PCVA11","PL-LAr_PVA11", "LAr_CoolingLoop_EMEC_C_R2a"],
        ["PL-LAr_PCVA12","PL-LAr_PVA12", "LAr_CoolingLoop_EMEC_C_R2b"],
        ["PL-LAr_PCVA13","PL-LAr_PVA13", "LAr_CoolingLoop_EMEC_C_T3"],
        ["PL-LAr_PCVA14","PL-LAr_PVA14", "LAr_CoolingLoop_EMB_A_L2"],
        ["PL-LAr_PCVA15","PL-LAr_PVA15", "LAr_CoolingLoop_EMB_A_L3"],
        ["PL-LAr_PCVA16","PL-LAr_PVA16", "LAr_CoolingLoop_EMB_A_T3"],
        ["PL-LAr_PCVA17","PL-LAr_PVA17", "LAr_CoolingLoop_EMB_A_B3"],
        ["PL-LAr_PCVA18","PL-LAr_PVA18", "LAr_CoolingLoop_EMB_A_R3"],
        ["PL-LAr_PCVA19","PL-LAr_PVA19", "LAr_CoolingLoop_EMB_A_R2"],
        ["PL-LAr_PCVA20","PL-LAr_PVA20", "LAr_CoolingLoop_EMB_C_L3a"],
        ["PL-LAr_PCVA21","PL-LAr_PVA21", "LAr_CoolingLoop_EMB_C_L3b"],
        ["PL-LAr_PCVA22","PL-LAr_PVA22", "LAr_CoolingLoop_EMB_C_T2"],
        ["PL-LAr_PCVA23","PL-LAr_PVA23", "LAr_CoolingLoop_EMB_C_B2"],
        ["PL-LAr_PCVA24","PL-LAr_PVA24", "LAr_CoolingLoop_EMB_C_R3a"],
        ["PL-LAr_PCVA25","PL-LAr_PVA25", "LAr_CoolingLoop_EMB_C_R3b"]]
    '''
    for x in range(1,26):
        print "[\"PL-LAr_PCVA%i\",\"PL-LAr_PVA%i\", \"\"]" % (x,x)
        pass
    pass
    group="Valves_LAr"
    o_pc={
        "UID":"FCTIR_00020_PC_LAr",
        }
    hp_pc.addObj(o_pc, False, True)
    '''
    for u in uids:
        ''' THIS IS ALREADY DONE
        o_g={"UID":group}
        hp_g.addObj(o_g, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva={
            "UID":u[0],"controlsByAirTo":u[2], "controlledByAirFrom":"FCTIR_00020_PC_LAr", "groupedBy":group
        }
        hp_pv.addObj(o_pva, False, True)
        #in PVA, controlledbyair from PC, controls coolingloop
        o_pva2={
            "UID":u[1],"controlsByAirTo":u[2], "controlledByAirFrom":"FCTIR_00020_PC_LAr", "groupedBy":group
        }
        hp_pv.addObj(o_pva2, False, True)
        #pneumactic cont.
        o_pc={
            "UID":"FCTIR_00020_PC_LAr",
            "controlsByAirTo":u[0],
        }
        hp_pc.addObj(o_pc, False, True)
        #pneumactic cont.
        o_pc={
            "UID":"FCTIR_00020_PC_LAr",
                "controlsByAirTo":u[1],
                }
        hp_pc.addObj(o_pc, False, True)
        #both Coolingloop controlledbyPVA
        o_cl={
            "UID":u[2],"controlledByAirFrom":u[0]
        }
        hp_cl.addObj(o_cl, False, True)
        #add it one
        o_cl={
            "UID":u[2],"controlledByAirFrom":u[1]
        }
        hp_cl.addObj(o_cl, False, True)
        #grouping
        o_g={"UID":group, "groupTo":u[0]}
        hp_g.addObj(o_g, False, True)
        o_g={"UID":group, "groupTo":u[1]}
        hp_g.addObj(o_g, False, True)
        pass
        '''
        #FROM HERE ONLY TO ADD DESCRIPTIONS (IT SHOULD HAVE BEEN DONE BEFORE...)
        #def addObj(self,data,remove=False,extend=False):
        subsystem="LAr"
        o_pcva={
            "UID":u[0],"subsystem":subsystem, "description": PCVA_description
        }
        hp_pv.addObj(o_pcva, False, True)
        o_pva={
            "UID":u[1],"subsystem":subsystem, "description": PVA_description
        }
        hp_pv.addObj(o_pva, False, True)
    db.commit("")
