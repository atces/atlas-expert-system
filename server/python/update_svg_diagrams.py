#!/usr/bin/env python
# Updates the corresponding SVG file if the XML file is newer.
# @Contributor Gustavo.Uribe@cern.ch

import os
import subprocess
from datetime import datetime
import socket
import json
import time

backend_host="atlas-expert-system-dev-el9.cern.ch"
server_restarted=False

def restart_server():
  global server_restarted
  if not server_restarted:
    print("Restarting dev server")  
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((backend_host,9999))
    #req = {"cmd": "restart"}
    req ="restart"
    print("Sending command %r to the server"%req)
    sock.sendall(req.encode())
    s_rep=""
    nrep = 0
    try:
        nrep = int(sock.recv(8).decode('utf-8'),16)
    except:
        print("Invalid message size")
        pass
    #print ("Read the message of size: %i" %nrep)
    while len(s_rep)<nrep:
        s_rep += sock.recv(50000).decode('utf-8')
        pass
    try:
        print(s_rep)
        pass
    except:
        print ("Error parsing message")
        pass
    sock.close()
    print("Server Restarted")
    server_restarted=True
    print("Creating a new token")  
    time.sleep(5)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((backend_host,9998))
    req = {"cmd": "GetNewToken"}
    print("Sending command %r to the server"%req)
    s_req = json.dumps(req)
    sock.sendall(s_req.encode())
    s_rep=""
    nrep = 0
    try:
        nrep = int(sock.recv(8).decode('utf-8'),16)
    except:
        print("Invalid message size")
        pass
    #print ("Read the message of size: %i" %nrep)
    while len(s_rep)<nrep:
        s_rep += sock.recv(50000).decode('utf-8')
        pass
    try:
        print(json.loads(s_rep))
        pass
    except:
        print ("Error parsing message")
        pass
    sock.close()
    print("TokenId created")
    time.sleep(5)

def get_script_dir():
  """Gets the directory containing the script."""
  return os.path.dirname(os.path.realpath(__file__))

def update_svg(xml_file):
  """Updates the corresponding SVG file if the XML file is newer."""
  svg_file = xml_file.replace("xmlgraphs/", "svggraphs/").replace(".xml", ".svg").replace(".drawio", ".svg")
  svg_tool = os.path.join(get_script_dir(),"../../login/data/diaeditor/export_svg.js")
  
  if not os.path.exists(svg_file):
    restart_server()
    print("The SVG file for %s is not present. Creating the SVG file..."%os.path.basename(xml_file))
    subprocess.run(["node", svg_tool, os.path.basename(xml_file)])
  else:
    xml_time = datetime.fromtimestamp(os.path.getmtime(xml_file))
    svg_time = datetime.fromtimestamp(os.path.getmtime(svg_file))
    if xml_time > svg_time:
      restart_server()
      print("%s is newer than SVG. Updating the SVG file..."%os.path.basename(xml_file))
      subprocess.run(["node", svg_tool, os.path.basename(xml_file)])

if __name__ == "__main__":
  script_dir = get_script_dir()
  xml_dir = os.path.join(script_dir, "../../login/data/xmlgraphs/")
  for filename in os.listdir(xml_dir):
    file_path = os.path.join(xml_dir, filename)
    if os.path.isfile(file_path):
      update_svg(file_path)
  if os.path.exists("puppeteer_user_data/"):
    subprocess.run(["rm", "-r","puppeteer_user_data/"])
