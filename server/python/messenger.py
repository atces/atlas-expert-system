#!/usr/bin/env python

# ############################################
# Expert System Remote Database Service
#
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
# 2018 April
import sys

class  Messenger():
    def __init__(self):
        pass
    def getOn(self):
    	return "Change state to On"

    def getOff(self):
    	return "Change state to Off"

    def switchOff(self):
        return "Switch is OFF, "
    def deployedYes(self):
        return "Deployed, "
    def unknown(self):
        return "Unknown, " 
    pass
