#!/usr/bin/env python
# ############################################
# NetworkSubnet Helper
# Carlos.Solans@cern.ch
# gustavo.uribe@cer.ch
# September 2019 - December 2019
# ############################################

import helper

class NetworkSubnetHelper(helper.Helper):
    pass
    
if __name__ == '__main__':
    helper.Helper().cmdline("NetworkSubnet")
    print ("Have a nice day")
