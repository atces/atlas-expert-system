#!/usr/bin/env python
#############################################
# PLC Module helper
# carlos.solans@cern.ch
# June 2019
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class PLCModuleHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("PLCModule")
    print ("Have a nice day")
