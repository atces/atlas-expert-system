#!/usr/bin/env python
#############################################
# Finder tool
# Carlos.Solans@cern.ch
# September 2019
#############################################

import loadhelpers
     
if __name__ == '__main__':
    
    import os
    import sys
    import config
    import argparse
    import loadhelpers
    
    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")    
    parser.add_argument("-q","--query",help="query")
    parser.add_argument("-t","--type",help="type")
    args=parser.parse_args()

    print ("Load database: %s" % args.database)
    db=config.Configuration("oksconfig:%s"%(args.database))

    ret=[]
    lh=loadhelpers.LoadHelpers(db)
    for cName in lh.getClasses():
        if args.type and not args.type.lower() in cName.lower(): continue
        objs = db.get_objs(cName)
        for obj in objs:
            if args.query in obj.UID():
                print ("====>%s@%s" % (obj.UID(),cName))
                sobj=lh.getHelpers()[cName].serialize(obj)
                sobj['UID']=obj.UID()
                ret.append(sobj)
                pass
            pass
        pass
    print (ret)
    pass
