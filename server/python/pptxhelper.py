# florian.haslbeck@cern.ch
# January 2022
# 
# contains the PPTXHelper and DSSPPTXHelper classes

import datetime
try:
    from pptx import Presentation
    from pptx.dml.color import RGBColor
except ImportError:
    print("The 'pptx' package is not installed. Please install it by running 'python -m pip install --user python-pptx'.")
    exit(1)


class PPTXHelper:
    """ combine pptx related functionality """

    @staticmethod
    def make_textframe(slide):
        """ retrun the textframe of a slide"""
        return slide.shapes.placeholders[1].text_frame

    @staticmethod
    def add_paragraph_to_textframe(textframe, text, color = None, level=1):
        paragraph = textframe.add_paragraph()
        paragraph.text  = text
        paragraph.level = level

        if color != None: paragraph.font.color.rgb = RGBColor(*color)

        return paragraph
    
    @staticmethod
    def add_hyperlink_to_textframe(textframe, text, url, level=1):
        paragraph = textframe.add_paragraph()
        # paragraph.text = text
        paragraph.level = level

        r = paragraph.add_run()
        r.text = text
        hlink = r.hyperlink
        hlink.address = url

        return paragraph

    @staticmethod
    def get_slide_layout_from_master(presentation, master_slide=1):
        return presentation.slide_master.slide_layouts[master_slide]

    @staticmethod
    def set_slide_title(slide, title):
        slide.shapes.title.text = title



class DSSPPTXHelper(PPTXHelper):
    """ specific to DSS slides """

    @staticmethod
    def add_alarm_action_textbox(slide, alarm_action_list):
        """ alarm_action_list = [ [ day 1, time 1 , alarm 1, [actions] ], ... ] """

        # create a textframe 
        tf      = DSSPPTXHelper.make_textframe(slide)
        day     = alarm_action_list[0][0]
        tf.text = '%s %s'%(DSSPPTXHelper.get_weekday(day), day.split('-')[-1])
        
        # add a paragraph per intervention
        DSSPPTXHelper.add_paragraph_to_textframe(tf, '??? Intervention Short description')
        DSSPPTXHelper.add_paragraph_to_textframe(tf, '??? Intervention Long description', level=2)
        
        for day, time, alarm, actions in alarm_action_list:
            # add alarms
            DSSPPTXHelper.add_paragraph_to_textframe(tf, "%s - %s"%(alarm, time[:5]), level=3) # adds time hh:mm
            # add actions
            if len(actions) == 0: 
                DSSPPTXHelper.add_paragraph_to_textframe(tf, 'No DSS action.', level=4)
            else:
                for action in actions: DSSPPTXHelper.add_paragraph_to_textframe(tf, action, level=4)
        return tf
    
    @staticmethod
    def add_event(slide, event, verbose=False):
        """ event 
            description
            short_description
            event_type
            responsible
            date
            time
            logs
            alarms

            // actions
            logactions_names, logactions_times
        """

        # create a textframe 
        tf      = DSSPPTXHelper.make_textframe(slide)

        # set the day
        day     = event['date']
        tf.text = '%s %s'%(DSSPPTXHelper.get_weekday(day), day.split('-')[-1])
        
        # add a paragraph per intervention with the adequate color
        # add the short description as the first paragraph
        color_map = {'Error': (0xE5, 0x8A, 0x20), 'Intervention' : (0x55, 0x8F, 0xFC), 'Unknown': (0xA7, 0xA7, 0xA7)}
        DSSPPTXHelper.add_paragraph_to_textframe(tf, f"{event['event_type']} - {event['description']}", color_map[event['event_type']], level = 1) 
        DSSPPTXHelper.add_paragraph_to_textframe(tf, event['short_description'], level=2)
           
        # add the responsible person
        if event['person'] != 'Unknown':
            resp_str = f'Followed up by {event["person"]}'.strip()
            if not resp_str.endswith("."): resp_str += '.'
            DSSPPTXHelper.add_paragraph_to_textframe(tf, resp_str, level=2)

        # add this after the responsible person and not as a separate paragraph
        if not " " in event['elisa']:
            DSSPPTXHelper.add_hyperlink_to_textframe(tf, '[Elisa]', event['elisa'], level=2)
        
        # add the alarms
        for log in sorted(event['logs']):
            # add alarms
            alarm = event['alarms'][log]
            if verbose: print(alarm)

            DSSPPTXHelper.add_paragraph_to_textframe(tf, "%s - %s"%(alarm['name'], alarm['time'][:5]), level=3) # adds time hh:mm

            # add actions
            if not ("logactions_times" in alarm and "logactions_names" in alarm): continue
            if len(alarm["logactions_times"]) > 0:
                for action, time in zip(alarm["logactions_names"], alarm["logactions_times"]):
                    DSSPPTXHelper.add_paragraph_to_textframe(tf, f"{action} - {time[:5]}", level=4)
        
        return tf
    
    @staticmethod
    def get_weekday(day_str):
        days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        yy, mm, dd  = day_str.split('-')
        return days[datetime.datetime(int(yy), int(mm), int(dd)).weekday()]
        
    @staticmethod
    def add_slide(presentation, title):
        """ append a new slide with a text box"""
        alarm_slide_layout = DSSPPTXHelper.get_slide_layout_from_master(presentation, 1)
        slide              = presentation.slides.add_slide(alarm_slide_layout)
        DSSPPTXHelper.set_slide_title(slide, title)
        return slide

    @staticmethod
    def add_intervention_slide(presentation):
        slide = DSSPPTXHelper.add_slide(presentation, 'DSS interventions')
        tf = DSSPPTXHelper.make_textframe(slide)
        DSSPPTXHelper.add_paragraph_to_textframe(tf, 'Last week', level=0)
        DSSPPTXHelper.add_paragraph_to_textframe(tf, 'None', level=1)
        DSSPPTXHelper.add_paragraph_to_textframe(tf, 'This week', level = 0)
        DSSPPTXHelper.add_paragraph_to_textframe(tf, 'None', level=1)

    @staticmethod
    def add_inhibit_slide(presentation):
        slide = DSSPPTXHelper.add_slide(presentation, 'DSS Inhibits - next weekhttps://atlas-expert-system.web.cern.ch/login/inhibitRequest.php')

    @staticmethod
    def add_alarm_slide(presentation, title, slide_alarm_action_list):
        """ append a new slide with a text box containg the alarms and actions """
        slide = DSSPPTXHelper.add_slide(presentation, title)
        DSSPPTXHelper.add_alarm_action_textbox(slide, slide_alarm_action_list)

    @staticmethod
    def make_presentation(pptx, week, grouped_day_time_alarms_actions, pptx_out=None, verbose=False):
        """ based on grouped alarms make a new pptx where new sldies are appended to original pptx """
        presentation  = Presentation(pptx)

        for group in grouped_day_time_alarms_actions: DSSPPTXHelper.add_alarm_slide(presentation, week, group)

        DSSPPTXHelper.add_intervention_slide(presentation)
        DSSPPTXHelper.add_inhibit_slide(presentation)
        
        output=pptx.replace('.pptx', '_py.pptx')
        if pptx_out: output=pptx_out
        if verbose: print('saving %s'%output)
        presentation.save(output)
        
    @staticmethod
    def make_title_string():
        """ make the title string for the pptx """
        # Get today's date and find last and next monday + the month of next monday
        today = datetime.datetime.now()
        # make sure that to chose the correct week when making the slides on Monday
        if today.weekday() == 0: last_monday = today - datetime.timedelta(days=7)
        else: last_monday = today - datetime.timedelta(days=today.weekday())
        next_monday = last_monday + datetime.timedelta(days=7)
        next_monday_month = next_monday.strftime("%B")

        # Construct the string
        alarm_string = f"DSS alarms {last_monday.strftime('%d')} - {next_monday.strftime('%d')} {next_monday_month}"
        return alarm_string
    
    @staticmethod
    def make_title_string_from_events(events):
        """ make the title string for the pptx based on the events """

        # get the first and last day of the events
        # sort by date ""2024-08-15" 
        if len(events) == 0: return "DSS alarms"
        

        # Get the first and last date as datetime objects
        first_day = datetime.datetime.strptime(events[sorted(events)[0]]['date'], "%Y-%m-%d")
        last_day  = datetime.datetime.strptime(events[sorted(events)[-1]]['date'], "%Y-%m-%d")

        # last day is the coming Monday of the last event
        if last_day.weekday() != 0: last_day = last_day + datetime.timedelta(days=7 - last_day.weekday())

        if first_day.month == last_day.month:
            return f"DSS alarms {first_day.day} - {last_day.day} {first_day.strftime('%b')}"
        else:
            return f"DSS alarms {first_day.day} {first_day.strftime('%b')} - {last_day.day} {last_day.strftime('%b')}"



def create_slides(pptx_in, pptx_out, event_json, verbose=False):
    
    # load the pptx file
    presentation = Presentation(pptx_in)

    title = DSSPPTXHelper.make_title_string_from_events(event_json)

    # loop over the events and add them to the pptx
    for event in sorted(event_json):
        if verbose:
            print("Add event %s"%event)
            print(event_json[event])
            print("\n")

        # make a new slide and add the event to it
        slide = DSSPPTXHelper.add_slide(presentation, title)
        DSSPPTXHelper.add_event(slide, event_json[event])

    # add the intervention and inhibit slides
    DSSPPTXHelper.add_intervention_slide(presentation)
    DSSPPTXHelper.add_inhibit_slide(presentation)    

    # save the pptx 
    presentation.save(pptx_out)
    if verbose: print(f"Saved {pptx_out}")