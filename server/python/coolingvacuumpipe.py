#!/usr/bin/env python
#############################################
# CoolingVacuumPipe Helper
# Carlos.Solans@cern.ch
# March 2019: new class
#############################################

import helper

class CoolingVacuumPipeHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("CoolingVacuumPipe")
    print ("Have a nice day")
