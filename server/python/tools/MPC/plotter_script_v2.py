import os
import csv
import ROOT
import argparse
from array import array
import math
import time
import datetime


parser=argparse.ArgumentParser()
parser.add_argument('p',help='plot',type=int, default=14)
args=parser.parse_args()
p=args.p
now=datetime.datetime.now()
now_st=now.strftime("%Y%m%d__%H_%M_%S")




counter=0
#filename="mpc_test_results.csv"
filename="mpc_test_results_2.csv"
ROOT.gROOT.SetStyle("ATLAS")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

th_path="/eos/user/a/atopes/reliance-data/tmp"
#by n of tries
if p==1:
    # C1
    signal="F4-score"
    max_results=70
    aY=1
    profile_c1_30 = ROOT.TProfile("profile_c1_30", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    profile_c1_60 = ROOT.TProfile("profile_c1_60", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    profile_c1_90 = ROOT.TProfile("profile_c1_90", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    #linearFit_c90 = ROOT.TF1("linearFit_c1","[0]*x^2+[1]*x+[2]")
    o_c1_axis=profile_c1_30.GetYaxis()
    o_c1_axis.SetRangeUser(0,1)

if p==2:
    signal="Average Time [s]"
    max_results=70
    aY=160
    profile_c2_30 = ROOT.TProfile("profile_c2_30", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    profile_c2_60 = ROOT.TProfile("profile_c2_60", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    profile_c2_90 = ROOT.TProfile("profile_c2_90", "Delta R vs water in 30 cm samples; Max results;%s" % (signal), 1000, 0, max_results, 0, aY)
    #linearFit_c90 = ROOT.TF1("linearFit_c1","[0]*x^2+[1]*x+[2]")
    o_c2_axis=profile_c2_30.GetYaxis()
    o_c2_axis.SetRangeUser(0,aY)


tests=[]

done=[]
#Put csv file in memory
with open(filename, 'U') as f:
    reader = csv.reader(f, delimiter=',')
    rownum=0
    for row in reader:
        counter+=1
        if counter==1:continue
        #print(row)
        rows=row[0].split(",")
        if row[0]=="": continue
        #print(rows)
        tests.append({"max_results":float(row[0]), "max_trys":float(row[1]), "F4-score":float(row[2]), "t":float(row[3])})
        pass
    pass



# filter it
for t in tests:
    #print(t)
    if p==1 and t["max_trys"]==30.0:
        print("30 max_results: %s\tF4-score:%s " % ( t["max_results"],t["F4-score"]) )
        profile_c1_30.Fill(t["max_results"], t["F4-score"])
        pass
    if p==1 and t["max_trys"]==64.0:
        print("64 max_results: %s\tF4-score:%s " % ( t["max_results"],t["F4-score"]) )
        profile_c1_60.Fill(t["max_results"], t["F4-score"])
        pass
    if p==1 and t["max_trys"]==128.0:
        print("128 max_results: %s\tF4-score:%s " % ( t["max_results"],t["F4-score"]) )
        profile_c1_90.Fill(t["max_results"], t["F4-score"])
        pass
    if p==2 and t["max_trys"]==30.0:
        print("30 max_results: %s\tt:%s " % ( t["max_results"],t["t"]) )
        profile_c2_30.Fill(t["max_results"], t["t"])
        pass
    if p==2 and t["max_trys"]==64.0:
        print("64 max_results: %s\tt:%s " % ( t["max_results"],t["t"]) )
        profile_c2_60.Fill(t["max_results"], t["t"])
        pass
    if p==2 and t["max_trys"]==128.0:
        print("128 max_results: %s\tt:%s " % ( t["max_results"],t["t"]) )
        profile_c2_90.Fill(t["max_results"], t["t"])
        pass
    pass


if  p==1:
    legend_water = ROOT.TLegend(0.641566,0.213992,0.906627,0.386831)
    legend_water.SetNColumns(1)
    legend_water.SetBorderSize(1)
    #legend_water.SetHeader("Sample size 0.225m^{2}")
    legend_water.AddEntry(profile_c1_30, "Max 30 tries", "ep")
    legend_water.AddEntry(profile_c1_60, "Max 64 tries", "ep")
    legend_water.AddEntry(profile_c1_90, "Max 128 tries", "ep")
    
    c1 = ROOT.TCanvas("c1","",71,154,666,515)
    profile_c1_30.SetMarkerStyle(21)
    profile_c1_30.SetMarkerColor(4)
    profile_c1_30.SetLineColor(4)
    #profile_c1_30.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c1_30.Draw("pX0 SAMES")
    
    profile_c1_60.SetMarkerStyle(21)
    profile_c1_60.SetMarkerColor(2)
    profile_c1_60.SetLineColor(2)
    #profile_c1_60.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c1_60.Draw("pX0 SAMES")
    
    profile_c1_90.SetMarkerStyle(21)
    profile_c1_90.SetMarkerColor(3)
    profile_c1_90.SetLineColor(3)
    #profile_c1_90.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c1_90.Draw("pX0 SAMES")
    
    legend_water.Draw("same")
    c1.Print("c1_%s.pdf" % (now_st))
    pass

if  p==2:
    legend_water = ROOT.TLegend(0.650602,0.187243,0.917169,0.331276)
    legend_water.SetNColumns(1)
    legend_water.SetBorderSize(1)
    #legend_water.SetHeader("Sample size 0.225m^{2}")
    legend_water.AddEntry(profile_c2_30, "Max 30 tries", "ep")
    legend_water.AddEntry(profile_c2_60, "Max 64 tries", "ep")
    legend_water.AddEntry(profile_c2_90, "Max 128 tries", "ep")
    
    c2 = ROOT.TCanvas("c2","",71,154,666,515)
    profile_c2_30.SetMarkerStyle(21)
    profile_c2_30.SetMarkerColor(4)
    profile_c2_30.SetLineColor(4)
    #profile_c2_30.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c2_30.Draw("pX0 SAMES")
    
    profile_c2_60.SetMarkerStyle(21)
    profile_c2_60.SetMarkerColor(2)
    profile_c2_60.SetLineColor(2)
    #profile_c2_60.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c2_60.Draw("pX0 SAMES")
    
    profile_c2_90.SetMarkerStyle(21)
    profile_c2_90.SetMarkerColor(3)
    profile_c2_90.SetLineColor(3)
    #profile_c2_90.Fit(linearFit_c5,"LR+","",fitmin_c5,fitmax_c5)
    profile_c2_90.Draw("pX0 SAMES")
    
    legend_water.Draw("same")
    c2.Print("c2_%s.pdf" % (now_st))
    pass

input()
print("DONE")
