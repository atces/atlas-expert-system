import sys
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-V","--verbose",help="verbose mode",action='store_true')
parser.add_argument("--f",help="PHP file to parse", required=True)
parser.add_argument("--margin",help="for margin- type CSS",action='store_true')
args=parser.parse_args()



file=args.f
verbose=args.verbose
margin=args.margin

print "Parsing.... %s    " % file
print "Mode verbose %s" % verbose
print "Mode margin %s" % margin
print "------------------------------------------------------------------------------------------------"

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def find_between_r( s, first, last ):
    try:
        start = s.rindex( first ) + len( first )
        end = s.rindex( last, start )
        return s[start:end]
    except ValueError:
        return ""

print "<style id=\"elements_style\">"
  
print "<?php"
print "$data=array("
if(margin==False):
    with open(file) as fp:
        for line in fp:
            line=line.replace("\t", "")
            line=line.replace(" ", "")
            line=line.replace("\\", "")
            if line.startswith("div.")==True:
                #print line
                name=find_between(line, ".","{")
                top=find_between(line, "top:", "px;left:")
                left=find_between(line, "px;left:", "px;}")
                #"EKD1_1H"            => array("group"=>false, "top"=> 80, "left"=> 40),
                print "\t\"%s\"\t\t=> array(\"group\"=>false, \"top\"=> %s ,\"left\"=> %s)," % (name, top, left)
                pass
            pass
        pass
    pass
else:
    with open(file) as fp:
        for line in fp:
            line=line.replace("\t", "")
            line=line.replace(" ", "")
            line=line.replace("\\", "")
            if line.startswith("div.")==True:
                #print line
                name=find_between(line, ".","{")
                top=find_between(line, "top:", "px;margin-left:")
                left=find_between(line, "px;margin-left:", "px;}")
                #"EKD1_1H"            => array("group"=>false, "top"=> 80, "left"=> 40),
                print "\t\"%s\"\t\t=> array(\"group\"=>false, \"top\"=> %s ,\"left\"=> %s)," % (name, top, left)
                pass
            pass
        pass
    pass



    
print ");"
print "printStyle($data);"    
print "?>"
print "</style>"
print "------------------------------------------------------------------------------------------------"
