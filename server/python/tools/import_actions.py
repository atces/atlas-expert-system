#!/usr/bin/env python
# Expert system 
# script to import actions from list to the expert system
# Ignacio.Asensi@cern.ch
# Jan 2018 

import os
import sys
import config
import argparse
import json

parser = argparse.ArgumentParser("check delayed actions")
parser.add_argument("database",help="database file")
parser.add_argument("--json",help="json file")#,nargs='+',default=[])
args=parser.parse_args()



print "Load database: %s" % args.database
db=config.Configuration("oksconfig:%s"%(args.database))

actionsDb = db.get_objs("Action")


'''
file="actions/ActionList.txt"
string=open(file, 'r')
actionsFile={}
actionsFileNames=[]
prevLine=""
output=""
outputFile=open("import_actions_txt.txt","w")



for line in string:
    
    if line.startswith("ACTION"):
        prevAction=line.replace(line[line.find("("):line.find(")")+1], "").replace("ACTION  ", "").replace("\n","").replace("\r","").replace(" ","")
        actionsFileNames.append(line.replace(line[line.find("("):line.find(")")+1], "").replace("ACTION  ", "").replace("\n","").replace("\r","").replace(" ",""))
        actionsFile[prevAction]={}
        actionsFile[prevAction]["Alarms"]=[]
        actionsFile[prevAction]["Description"]=""
        pass
    if line.startswith("ADDRESS"):
        actionsFile[prevAction]["Address"]=line.replace("ADDRESS: ","").replace("\n","").replace("\r","")
        pass
    if "Description:" in prevLine:
        actionsFile[prevAction]["Description"]=line.replace("\n","").replace("\r","").replace("      ","")
        pass
    if line.startswith("      AL_"):
        actionsFile[prevAction]["Alarms"].append(line.replace("\n","").replace("\r","").replace("      ",""))
        pass
    prevLine=line
    pass


print "actions in file:",len(actionsFile)
print "actions in db", len(actionsDb)
actionsDbNames=[]
for action in actionsDb:
    actionsDbNames.append(action.UID())
    pass

print "Actions not present in db:"
counter=0
actionsNotInDb=[]
for action in actionsFileNames:
    if action  not in actionsDbNames:
        actionsNotInDb.append(action)
        pass
    pass


print "actionsNotInDb: ", len(actionsNotInDb)

print "Add actions missing in db"
for actionNotInDb in actionsNotInDb:
    counter=counter+1
    output+="python action.py ../../data/sb.data.xml -y -u %s --description \"%s\" --dsu %s --crate %s --slot %s --channel %s \n" % (actionNotInDb, actionsFile[actionNotInDb]["Description"], actionsFile[actionNotInDb]["Address"][3:4],actionsFile[actionNotInDb]["Address"][5:10],actionsFile[actionNotInDb]["Address"][12:13],actionsFile[actionNotInDb]["Address"][14:16])
    pass
print "lines: ",counter
outputFile.write(output)
outputFile.close()
print "END" 


############################################################
###### DelayedActions

outputDAFile=open("import_delayedactions_txt.txt","w")
delayedactionsDb = db.get_objs("DelayedAction")
delayedactionsDbNames=[]
for delayedaction in delayedactionsDb:
    delayedactionsDbNames.append(delayedaction.UID())
    pass

delayedactionsNotInDb=[]
for delayedaction in actionsFileNames:
    if delayedaction  not in delayedactionsDbNames:
        delayedactionsNotInDb.append(delayedaction)
        pass
    pass

outputdelayed=""

for delayedactionNotInDb in delayedactionsNotInDb:
    stralarm=""
    for alarm in actionsFile[delayedactionNotInDb]["Alarms"]:
        print alarm
        stralarm+=str(alarm) + str(" + ")
        pass
    outputdelayed+="python delayedaction.py ../../data/sb.data.xml -u %s_0 --delay 0 --action %s --alarms %s\n" % (delayedactionNotInDb, delayedactionNotInDb, stralarm)
    pass

outputDAFile.write(outputdelayed)
outputDAFile.close()
'''
if args.json:
    print "processing json: ",args.json
    alarmsdb = db.get_objs("Alarm")
    jsonfile= args.json
    jsonstring=open(jsonfile)
    data=json.load(jsonstring)
    
    alarmsDbNames=[]
    alarmsjsonNames=[]
    
    for alarmdb in alarmsdb:
        alarmsDbNames.append(alarmdb.UID())
        pass
    for alarmjson in data:
        alarmsjsonNames.append(alarmjson)
        pass
    
    
    counteralarms=0
    for alarm in data:
        if alarm not in alarmsDbNames:
            counteralarms=counteralarms+1
            print alarm
            pass
        pass
    print counteralarms
