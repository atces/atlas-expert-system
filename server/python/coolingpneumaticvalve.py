#!/usr/bin/env python
# ############################################
# Cooling Pneumatic Valve
# Carlos.Solans@cern.ch
# January 2019
# ############################################

import helper

class CoolingPneumaticValveHelper(helper.Helper):
	pass

if __name__ == '__main__':

    helper.Helper().cmdline("CoolingPneumaticValve")
    print ("Have a nice day")
