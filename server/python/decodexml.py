#!/usr/bin/env python
# decodexml.py
# ATLAS technical coordination, CERN
# 12/08/2019: Summer Student Eva Lott
# Deflates the jsgraph savefile xml and parses for all data elements
# Client side javascript to do this during operation is developed in rendergraph.js, this is the general script for server side functionalities and utilites

import xml.etree.ElementTree as ET
import base64
import zlib
import urllib

# Define an element object to contain information about the switch
class Element:
	def __init__ (self):
		self.UID = None
		self.x = None
		self.y = None

	# Checks if all fields are defined
	def isDefined (self):
		if self.UID != None and self.x != None and self.y != None:
			return True
		else:
			return False

class Decoder:
	# Pass in the filename
	# XML path is specified on initialisation
	def __init__ (self, xmlName):
		self.xmlName = xmlName
		self.XMLPATH = '/eos/home-a/atopes/www/atlas-expert-system-dev/login/data/xmlgraphs/'
		self.codedStr = None
		self.decodedStr = None

		# List of enviromental alarms as strings
		self.alarms = []

		# List of page elements with UID, x pos, and y pos
		self.elements  = []
		
		self.loadFile()
		self.decodeBase64ToStr()	
		self.parsePageElements()

	# Parse the deflated base64 from the file
	def loadFile (self):
		elementList = []
		tree = ET.parse(self.XMLPATH + self.xmlName)
		root = tree.getroot()
		diagram = root.find('diagram')

		self.codedStr = diagram.text

	# Get the decoded base 64 as a string
	def decodeBase64ToStr (self):
		# Decode the base 64, pass the text to a decompressor, then format the string as xml
		self.decodedStr = urllib.unquote(zlib.decompress(base64.b64decode(self.codedStr), -15))

	# Parse page elements into a list of objects
	def parsePageElements (self):
		root = ET.fromstring(self.decodedStr)
		
		# The global data object is aways the first object in the xml, the alarms are seperated by newline
		self.alarms = root[0][0].get('alarms').split('\n')

		# Loop through all page objects
		for obj in root.iter('object'):
			element = Element()
			element.UID = obj.get('UID')
			
			# If the vertex with data has a defined UID then other data is collected
			if element.UID != None:
				for mxCell in obj.findall('mxCell'):
					mxGeometry = mxCell.find('mxGeometry')
					element.x = mxGeometry.get('x')		
					element.y = mxGeometry.get('y')
					if element.isDefined():
						self.elements.append(element)			
