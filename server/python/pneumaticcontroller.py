#!/usr/bin/env python
# ############################################
# Pneumatic Controller Helper
# Ignacio.Asensi@cern.ch
# Nov 2018
# ############################################

import helper

class PneumaticControllerHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("PneumaticController")
    print ("Have a nice day")
