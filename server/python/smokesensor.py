#!/usr/bin/env python
#############################################
# Smoke Sensor helper
# ignacio.asensi@cern.ch
# Aug 2018
# January 2019
#############################################

import helper

class SmokeSensorHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
    helper.Helper().cmdline("SmokeSensor")
    print ("Have a nice day")
