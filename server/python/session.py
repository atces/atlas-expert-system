#!/usr/bin/env python
#############################################
# Session helper
# ignacio.asensi@cern.ch
# Carlos.Solans@cern.ch
# Dec 2017
# Stores session actions
# Computation is a list of lists: [[obj, state, reason],]
# March 2020 - simplified new_session method
#############################################

import helper

class SessionHelper(helper.Helper):
    
    def addAction(self, obj, action):
        ee=[]
        ee.extend(obj["commands"])
        ee.append(action)
        obj["commands"]=ee
        return len(obj["commands"]) 
    def addComputation(self, obj, actionnumber, computation):
        ee=[]
        ee.extend(obj["computation"])
        if isinstance(computation, str):
            ee.append("%s, %s" % (actionnumber,computation))
            pass
        else:
            for c in computation:
                ee.append("%s, %s" % (actionnumber,c))
                pass
            pass    
        obj["computation"]=ee         
        pass    
    def new_session(self, db, uid):
        db.create_obj(self.classname,uid)
        #data={}
        #data["UID"]=uid
        #data["commands"]=[""]
        #self.setDb(db)
        #self.addObj(data, False, True)
        db.commit()
    def get_number(self, obj):
        return self.getActionNumber()
    def getActionNumber(self, obj):
        return len(obj["commands"])
    def get_last_occurrence(self, obj, uid):
        responses=[]
        for computation in obj["computation"]:
            if uid in computation:
                responses.append(computation)
                pass
            pass
        return responses
    def get_commands_list(self, obj):
        responses=obj["commands"]
        return responses
    def get_computation_list(self, obj):
        responses=obj["computation"]
        return responses
    pass
