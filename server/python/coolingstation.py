#!/usr/bin/env python
# ############################################
# CoolingStation Helper
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
# June 2018
# August 2018: fix cmdline
# March 2019: Add DigitalInputProviderBase
# ############################################

import helper

class CoolingStationHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("CoolingStation")
    print ("Have a nice day")
