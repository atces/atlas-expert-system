#!/usr/bin/env python
#############################################
# Alarm helper
# Carlos.Solans@cern.ch
# Nay 2017
# August 2018: fix cmdline
# Nov 2018: add recv and req air from
#############################################

import helper

class VentilationSystemHelper(helper.Helper):
    pass
    
if __name__ == '__main__':

    helper.Helper().cmdline("VentilationSystem")
    print ("Have a nice day")
