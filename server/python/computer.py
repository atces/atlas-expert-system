#!/usr/bin/env python
#############################################
# Computer helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# July 2018
# August 2018: Fix cmdline
#############################################

from networkhelper import NetworkHelper as NetHelper
import helper

class ComputerHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("Computer")
    print ("Have a nice day")
    
