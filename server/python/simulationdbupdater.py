#!/usr/bin/env python
# ############################################
# Expert System Remote Database Service
#
# Ignacio.Asensi@cern.ch
# https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/javadoc/tdaq-06-00-00/config/ConfigObject.html
# https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/tdaq-06-00-00/html/d4/da7/classConfiguration.html
#
# Load the database directly from the web server
# Store a copy in /tmp
# ############################################
import os
import sys
import socket
import config
import json
import uuid
import time
import subprocess
import re
import pwd
import messenger
import loadhelpers
import ProbTree
import traceback
import datetime
import pexpect
import treehelper
import signal
import json
from graphhelper import GraphHelper 

cont=True
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    global cont
    cont=False
    pass

signal.signal(signal.SIGINT, signal_handler)


def log(ss):
    print("%s\t%s"%(timestamp(),ss))
    pass
    
def timestamp():
    return time.strftime("%Y-%m-%d %H:%M:%S")

def flush():
    if output==None: return
    sys.stdout = open(output,'a+')

def serialize(class_name, obj):
    ret={}
    ret=helpers[obj.class_name()].serialize(obj)
    return ret
    
def get_schema(class_name):
    ret={}
    ret=helpers[class_name].get_schema()
    return ret

# check if list only contains unique elements as a boolean
def isListUnique(myList):
    return len(set(myList))==len(myList)

# check whether common parents exist, return boolean
def checkIfCommonParents(myDict):
    print(myDict)
    # put all lists in one list and check if at least one element is contained in at least two lists
    myListofList = []
    for myKey in myDict:
        # test that dictionary contains a list and list is unique
        if(type(myDict[myKey])==list and isListUnique(myDict[myKey])):
            myListofList += myDict[myKey]
            pass
        else:
            log( "!!!! Error %s is not unique or a list" % str( myDict[myKey]))
            pass
        pass
    return(not isListUnique(myListofList))

# get common elements for a dict of format {key:[..,..,..],...}
# return a list of elements for the lists with key in range,
# if no start parameter is given, start with the first key
# if no stop parameter is given, iterate until the last key
def commonElementsRange(myDict,start=0,stop=0):
    commonParents = set()
    if(checkIfCommonParents(myDict)):
        myDictKeysList = list(myDict)  # get the dict's keys
        if(stop == 0):
            stop = len(myDictKeysList)
            pass
        commonParents = set(myDict[myDictKeysList[start]])
        # iterate through all lists and store so far common elements in commonParents
        for i in range(start+1,stop):
           commonParents.intersection_update(myDict[myDictKeysList[i]])
           pass
        pass
    else:
        print(">>> no common parents detected ", list(commonParents))
        pass
    return list(commonParents)

def loadDatabaseConfiguration(localpath):
    host="-".join(socket.gethostname().split("-")[:-1])
    dbpath="%s.web.cern.ch/%s/data" % (host,host)
    dbfiles=["sb.data.xml","sb.schema.xml", "sb.sim_data.xml", "sb.sim_schema.xml","meta.schema.xml","meta.data.xml"]
    mdbfile="sb.data.xml"
    cmds=[]
    cmds.append("mkdir -p %s" % localpath)
    cmds.append("rm %s/.oks*" % localpath)
    for dbfile in dbfiles:
        print("Load %s" % dbfile)
        found=False
        for path in os.environ["TDAQ_DB_PATH"].split(":"):
            print("  test:: %s/%s" % (path,dbfile))
            if os.path.exists("%s/%s"%(path,dbfile)):
                cmds.append("cp %s/%s %s/." % (path,dbfile,localpath))
                print("From filesystem: %s/%s" % (path,dbfile))
                found=True
                break
            if not found:
                cmds.append("wget -q %s/%s -O %s/%s" % (dbpath,dbfile,localpath,dbfile))
                print("From remote: %s/%s" % (dbpath,dbfile))
            pass
        pass
    for cmd in cmds:
        log(cmd)
        pid=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
        out, err = pid.communicate()
        pass
    log("Create configuration pointer")
    db=config.Configuration("oksconfig:%s/%s"%(localpath,mdbfile))
    return db

def updatedb():
    runing_in_lxplus=True
    reset_db_on_start=True
    quickdbsave=False
    sim_db_report=True
    txtfile="simulationdbtemp.txt"
    mdbfile="sb.data.xml"
    done_classes={}
    errors={}
    output=None
    conn_pool={}
    sess_pool={}
    uname=pwd.getpwuid(os.geteuid())[0]
    if runing_in_lxplus==False:
        tid=999
        tmpdir="/tmp/%s" % uname
        db_load_path="%s/db/%s" % (tmpdir,tid)
        db_load_path_sim="%s/db/%s/%s" % (tmpdir,tid,"sb.sim_data.xml")
    else:
        tid=999
        tmpdir="/eos/home-a/atopes/www/atlas-expert-system-dev/data/tmp/"
        db_load_path="%s/db/%s" % (tmpdir,tid)
        db_load_path_sim="%s/db/%s/%s" % (tmpdir,tid,"sb.sim_data.xml")
    print(db_load_path)
    print(db_load_path_sim)
    db=loadDatabaseConfiguration(db_load_path)
    conn_pool[tid]=db
    gLH = loadhelpers.LoadHelpers(db)
    classes=gLH.getClasses()
    searchableClasses=gLH.getSearchableClasses()
    sysclasses=gLH.getSysClasses()
    helpers=gLH.getHelpers()
    #th = treehelper.TreeHelper(conn_pool, sess_pool, helpers, classes)
    objs = db.get_objs("Session")
    sessionId="Session_"+str(tid)
    objs = db.get_objs("Session")
    sess_pool[tid]=None
    graph_helper=GraphHelper(conn_pool,sess_pool,helpers,classes)
    graph_helper.create_graph(db)
    graph_helper.generate_feeding_graph()
    #th = treehelper.TreeHelper(conn_pool, sess_pool, helpers, classes)
    sim_db=config.Configuration("oksconfig:%s"%(db_load_path_sim))
    
    for obj in objs:
        if obj.UID()==sessionId: 
            sess_pool[tid]=obj
            log("Found session in db: %s" % sess_pool[tid])
            pass
        pass
    if not sess_pool[tid]:
        log("Creating new session: %s" % sessionId)
        helpers["Session"].new_session(db,sessionId)
        sess_pool[tid] = db.get_obj("Session",sessionId)
        pass
    if sim_db_report==True:
        print("Simulation database before updating...")
        done_objs=sim_db.get_objs("object_alarms")
        done_classes={}
        for o in done_objs:
            if reset_db_on_start == True:
                sim_db.destroy_obj(o)
                pass
            else:
                c=o.get_string("classname")
                if c not in done_classes:
                    done_classes[c]=1
                    pass
                else:
                    done_classes[c]+=1
                    pass
                pass
            pass
        for c in done_classes:
            print("[%i]objects\t\tClass\t[%s]" % (done_classes[c],c))
            pass
        pass
    if reset_db_on_start == True: sim_db.commit()
    
    # Find alarms triggered by every object in db
    #skip_cc=[ 'Alarm', 'Action', 'DelayedAction', 'AirDryer', 'AUG', 'Board', 'Camera', 'CompressedAirSystem', 'Compressor', 'Computer', 'CoolingLoop', 'CoolingPneumaticValve', 'CoolingStation', 'CoolingSystem', 'CoolingVacuumPipe', 'Crate', 'Cryostat', 'DigitalInput', 'ElectricalCabinet', 'Elevator', 'Feedthrough', 'GasSystem', 'GasCentral', 'GasSensor', 'Group', 'Heater', 'HeatExchanger', 'Interlock', 'LACS', 'Lighting', 'Magnet', 'MetaInfo', 'Minimax', 'MinimaxCentral', 'NetworkRouter', 'NetworkServiceGroup', 'NetworkSubnet', 'NetworkSwitch', 'Oscilloscope', 'Pipe', 'PLC', 'PLCModule', 'PneumaticController', 'PneumaticValve', 'PressureSensor', 'PowerSupply', 'PowerStrip', 'Rack', 'Session', 'SingleBoardComputerBlade', 'SmokeCentral', 'SmokeSensor', 'SnifferCentral', 'SnifferModule', 'SnifferTiroir', 'SubDetector', 'Switchboard', 'Tank', 'ThermalScreen', 'Transformer', 'UPS', 'VacuumPump', 'VacuumValve', 'VentilationSystem', 'Vessel', 'VirtualMachine', 'WaterSystem', 'WaterValve', 'Zone']
    skip_cc=['Alarm', 'Action', 'DelayedAction', 'AirDryer', 'AUG', 'Board', 'Camera', 'CompressedAirSystem', 'Compressor', 'Computer', 'CoolingLoop', 'CoolingPneumaticValve', 'CoolingStation', 'CoolingSystem', 'CoolingVacuumPipe', 'Crate', 'Cryostat', 'DigitalInput', 'ElectricalCabinet', 'Elevator', 'Feedthrough', 'GasSystem', 'GasCentral', 'GasSensor']#, "Crate", "CoolingLoop", "Action", "DigitalInput","AirDryer", "DelayedAction", "Camera",  "Session"]#, 'NetworkRouter', 'NetworkServiceGroup', 'SingleBoardComputerBlade', 'NetworkSwitch','SubDetector']
    never_do=["Alarm","DigitalInput","Group","Minimax","Zone"]
    only_do=["SmokeSensor"]
    list_simulated={}
    free_mem_every=10
    mem_counter=0
    for c in classes:
        #if c in skip_cc or c in never_do: continue
        #if c not in only_do:continue
        #del(th)
        #th = treehelper.TreeHelper(conn_pool, sess_pool, helpers, classes)
        print("Processing class\t%s" % c)
        objs = conn_pool[tid].get_objs(c)
        obj_counter=0
        for o in objs:
            mem_counter+=1
            #if o.UID() != "DSU_7": continue
            if cont==False: break
            obj_counter+=1
            try:
                graph_helper.change_switch(tid,o.UID(), [], 0)
                pass
            except:
                errors[o.UID()]=c
                continue
                pass
            alarms = list(filter(lambda a: a.get_string("state")=="off", conn_pool[tid].get_objs("Alarm")))
            valarms=[]
            if len(alarms)>0:
                for a in alarms:
                    #print("Triggers.... %s" % a.UID())
                    valarms.append(a.UID())
                    pass
                pass
            list_simulated[o.UID()]=[c,len(valarms),valarms]
            print("Class\t[%s] Obj num [%i]\t Number of alarms[%i]\t%s" %(c,obj_counter, len(valarms),  o.UID()))
            del(conn_pool[tid])
            conn_pool[tid]=config.Configuration("oksconfig:%s/%s"%(db_load_path,mdbfile))
            if mem_counter>free_mem_every:
                #del(th)
                #th = treehelper.TreeHelper(conn_pool, sess_pool, helpers, classes)
                mem_counter=0
                pass
            if cont==False: break
            pass
        print("Class\t%s done" % c)#done searching. Now save data
        done_classes[c]=obj_counter
        try:
            for uid in list_simulated:
                sim_dbuid="#SIMULATED#%s" % uid
                try: 
                    obj_alarm=sim_db.get_obj("object_alarms",sim_dbuid)
                    pass
                except: 
                    sim_db.create_obj("object_alarms",sim_dbuid)
                    obj_alarm=sim_db.get_obj("object_alarms", sim_dbuid)
                    pass
                obj_alarm.set_string("name", uid)
                obj_alarm.set_string_vec("alarms", list_simulated[uid][2])
                obj_alarm.set_u16("nalarms", list_simulated[uid][1])
                obj_alarm.set_string("classname", list_simulated[uid][0])
                pass
            sim_db.commit()
            #print("All objects of class %s saved" % c)
            pass
        except:
            print("ERROR TRYING TO SAVE IN DB")
            pass
        if cont==False: break
        pass
    f = open(txtfile,'a+')
    f.write(json.dumps(list_simulated))
    f.close()
    for e in errors:
        print("Class\t\%s error in \t%s" % (e, errors[e]))
        pass
    print("Report:")
    for c in done_classes:
        print("%i\tobjects\tClass %s" % (done_classes[c],c))
        pass
    print("done")
    pass

if __name__ == "__main__":
    updatedb()
