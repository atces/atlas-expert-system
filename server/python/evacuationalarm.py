#!/usr/bin/env python
#############################################
# Evacuation Sensor helper
# ignacio.asensi@cern.ch
# Aug 2018
# Nov 2018 - where is update_state?
# January 2019
#############################################

import helper

class EvacuationAlarmHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
    helper.Helper().cmdline("EvacuationAlarm")
    print ("Have a nice day")
