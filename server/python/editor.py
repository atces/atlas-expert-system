#!/usr/bin/env python
############################################
# Database editor
# Edit objects in a single transaction
# Parse lines with the following format
# ClassName [-e] [-r] --uid <uid> --param [param]
#
# Carlos.Solans@cern.ch
# January 2020
############################################
import os
import sys
import config
import argparse        
import loadhelpers
import shlex

parser = argparse.ArgumentParser()
parser.add_argument("-d","--database",help="database file",default="sb.data.xml")
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
parser.add_argument("file",help="file to parse")
args=parser.parse_args()

print ("Load database: %s" % args.database)
db=config.Configuration("oksconfig:%s"%(args.database))

print ("Load helpers")
helpers=loadhelpers.LoadHelpers(db).getHelpers()

print ("Open file: %s" % args.file)
for line in open(args.file).readlines():
    line=line.strip()
    if len(line)==0: continue
    if line[0]=="#": continue
    pos=line.find(" ")
    ctype=line[:pos]
    stmt=line[pos:]
    if not ctype in helpers: 
        print("Class: %s not fount. Skip statement: %s"%(ctype,stmt))
        continue
    print("Class: %s Statement: %s" % (ctype,stmt))
    helper=helpers[ctype]
    helper.setDb(db)
    helper.setVerbose(args.verbose)
    parser2=argparse.ArgumentParser()
    parser2.add_argument("-r","--remove",help="remove action",action='store_true')
    parser2.add_argument("-e","--extend",help="extend object",action='store_true')
    helper.fill_parser(parser2)
    args2=parser2.parse_args(shlex.split(stmt))
    data={}
    data["UID"]=args2.uid
    vargs = vars(args2)
    attr_counter=0 #Used to identify if only UID is provided
    for attr in helper.attrs:
        if attr.lower() in vargs:
            data[attr]=vargs[attr.lower()]
            if data[attr]: attr_counter+=1
        pass
    if args.verbose: print (helper.dump(helper.getObj(args2.uid)))
    try:
        if attr_counter==1 and args2.remove: helper.destroyObj(ctype,args2.uid)
        else: helper.addObj(data,args2.remove,args2.extend)
        pass
    except RuntimeError as err:
        print("Sytax error: %s"%err)
        break
    if args.verbose: print (helper.dump(helper.getObj(args2.uid)))
    pass

print("Transaction finished")
batch=args.yes
if not batch:
    ans=input("Do you wish commit the changes? [y/n]: ")
    if "y" in ans.lower(): batch=True
    pass
if batch:
    print ("Commit changes")
    db.commit("")
    print ("Changes committed")
    pass
print ("Have a nice day")
