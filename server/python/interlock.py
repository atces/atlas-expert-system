#!/usr/bin/env python
# ############################################
# Interlock Helper
# Carlos.Solans@cern.ch
# June 2018
# August 2018: fix cmdline
# ############################################

import helper

class InterlockHelper(helper.Helper):
    pass
        
if __name__ == '__main__':

    helper.Helper().cmdline("Interlock")
    print ("Have a nice day")
