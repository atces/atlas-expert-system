#!/usr/bin/env python
#############################################
# Cryostat helper
# Carlos.Solans@cern.ch
# May 2019
#############################################

import helper

class CryostatHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("Cryostat")
    print ("Have a nice day")
