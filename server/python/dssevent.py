#!/usr/bin/env python
#############################################
# DSS Event helper
# Florian.Haslbeck@cern.ch
# June 2023
#############################################

import helper

class DSSEventHelper(helper.Helper):
	pass

if __name__ == '__main__':

    helper.Helper().cmdline("DSSEvent")
    print ("Have a nice day")
