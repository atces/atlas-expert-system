#!/usr/bin/env python
#############################################
# Gas Central helper
# Miguel.Garcia.Garcia@cern.ch
# Carlos.Solans@cern.ch
# July 2018
# August 2018: fix cmdline
# March 2019: add digital input provider
#############################################

import helper

class GasCentralHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
  helper.Helper().cmdline("GasCentral")
  print ("Have a nice day")
