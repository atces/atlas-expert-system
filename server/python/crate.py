#!/usr/bin/env python
#############################################
# Crate helper
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
# July 2018
# August 2018: fix cmdline
#############################################

from networkhelper import NetworkHelper as NetHelper
import helper

class CrateHelper(NetHelper,helper.Helper):
    pass
    

if __name__ == '__main__':

    helper.Helper().cmdline("Crate")
    print ("Have a nice day")
