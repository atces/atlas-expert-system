#!/usr/bin/env python
#############################################
# Smoke Central helper
# Miguel.Garcia.Garcia@cern.ch
# Carlos.Solans@cern.ch
# July 2018
# August 2018: fix cmdline
#############################################

import helper

class SmokeCentralHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("SmokeCentral")
    print ("Have a nice day")
