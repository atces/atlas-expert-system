#!/usr/bin/env python
#############################################
# Helper
# Carlos.Solans@cern.ch
# gustavo.uribe@cern.ch
# May 2017 - April 2021
#############################################

import config
import sys
import messenger
import json

class Helper(object):
    # Constructor
    # brief initialize class attributes and relationships
    def __init__(self, classname=None, db=None,verbose=False):
        self.db = db
        self.meta_db=config.Configuration("oksconfig:%s%s"%(db.active_database[:db.active_database.rfind('/')+1],db.get_obj("MetaInfo","Singleton")['metadataFilename'])) if db else None
        self.verbose = verbose
        self.classname = classname
        self.msgr = messenger.Messenger()
        self.attrs = {}
        if not classname: return
        schema=db.__schema__.data[classname]
        db_sub_attributes_used=["multivalue","not-null","type"]
        self.attrs={"UID":{"multivalue":False,"not-null":True,"type":None}} #UID attribute is a single value that can be null. No python datatype mapping required.
        db_attr_types={'enum':None,'string':None,'u16':int,'u32':int,'bool':None,'s32':int,'float':float,'date':None} #Mapping of the data types in the database to the data types in python 
        self.attrs.update({attr:{sub_attr:value for sub_attr,value in sub_attrs.items() if sub_attr in db_sub_attributes_used} for attr,sub_attrs in schema['attribute'].items()}) 
        self.attrs.update({attr:{sub_attr:value for sub_attr,value in sub_attrs.items() if sub_attr in db_sub_attributes_used} for attr,sub_attrs in schema['relation'].items()}) 
        self.function_per_dependancy={}
        self.additional_attributes_per_dependancy={}
        for attr,sub_attrs in self.attrs.items():
        	self.attrs[attr]['gather']=True if self.meta_db and self.meta_db.test_object("DependencyRelationshipMetaClass",attr) else None
        	self.attrs[attr]['feed']=True if self.meta_db and self.meta_db.test_object("FeedingRelationshipMetaClass",attr) else None 
        	if self.attrs[attr]['gather']:
        		self.function_per_dependancy[attr]=self.meta_db.get_obj("DependencyRelationshipMetaClass",attr)['functionName']
        		self.additional_attributes_per_dependancy[attr]=self.meta_db.get_obj("DependencyRelationshipMetaClass",attr)['jsonAdditionalAttributes'].replace("'",'"')
        		self.additional_attributes_per_dependancy[attr]=json.loads(self.additional_attributes_per_dependancy[attr]) if len(self.additional_attributes_per_dependancy[attr])>0 else {}
        	for sub_attr in list(sub_attrs):
        		if sub_attr=='type' and sub_attrs[sub_attr] in db_attr_types: self.attrs[attr][sub_attr]=db_attr_types[sub_attrs[sub_attr]]
        pass

    #brief Return the name of the inverse relation(s) given the relation 
    #param relation 
    def get_inverse_relation(self,relation):
        if self.meta_db.test_object("RelationshipMetaClass",relation): 
                inverse_relations=[ inverse.UID() for inverse in self.meta_db.get_obj("RelationshipMetaClass",relation)["inverseOf"]]
                return inverse_relations if len(inverse_relations)>1 else inverse_relations[0]

    # setDb
    # brief Set DB pointer used by some methods
    # param db Config object
    def setDb(self,db):
        self.db = db
        pass
    
    # dbCommit
    # commit db
    def dbCommit(self):
        self.db.commit("")
        pass
    
    # initMsgr
    # brief Init the messenger
    def initMsgr(self):
        self.msgr = messenger.Messenger()
        pass
    
    # setVerbose
    # brief set the verbose mode
    # param enable verbose mode
    def setVerbose(self,enable):
        self.verbose = enable
        pass

    # isDataOk
    # brief check if data is ok
    # param data dictionary containing the data
    def isDataOk(self,data):
        errs=[]
        wrns=[]
        for attr in self.attrs:
            if not attr in data:
                if self.attrs[attr]["not-null"]: errs.append("Missing key:: %s" % attr)
                else: wrns.append("Missing key:, %s" % attr)
                pass
            pass
        for err in errs:
            print ("Error: %s" % err)
            pass
        for wrn in wrns:
            #print ("Warning: %s" % wrn)
            pass
        return len(errs)==0
    
    # removeObj
    # brief remove attributes from object 
    # param uid unique id of the object in the database
    # return ConfigObject
    def removeObj(self,data):
        print ("removing obj", data)
        return self.addObj(data,True)
    
    # getObj
    # brief get object from the database 
    # param uid unique id of the object in the database
    # return ConfigObject
    def addObj(self,data,remove=False,extend=False):
        if not self.isDataOk(data): return
        obj=None
        try: obj=self.db.get_obj(self.classname,data["UID"])
        except: pass
        print (obj)
        if not obj:
            print ("Create new %s : %s" % (self.classname,data["UID"]))
            self.db.create_obj(self.classname,data["UID"])
            obj = self.db.get_obj(self.classname,data["UID"])
            pass
        if self.verbose: 
            for key in data: 
                print ("%s: %s" % (key, data[key]))
                pass
            pass    
        obj=self.deserialize(obj,data,remove,extend)
        return obj

    # get
    # brief get object from the database 
    # param uid unique id of the object in the database
    # return ConfigObject
    def get(self,uid):
        return self.getObj(uid)
    
    # getObj
    # brief get object from the database 
    # param uid unique id of the object in the database
    # return ConfigObject
    def getObj(self,uid):
        obj=None
        try: obj=self.db.get_obj(self.classname,uid)
        except: 
            if self.verbose: print (sys.exc_info())
            pass
        return obj    
    
    # deserialize
    # brief deserialize a dict into a configobject
    # param obj ConfigObject
    # param data dict
    # return ConfigObject
    def deserialize(self,obj,data,remove=False,extend=False):
        for attr in self.attrs:
            if not attr in data: continue
            if data[attr]==None: continue
            if attr == "UID": continue
            if self.verbose: print ("process", attr)
            if not self.attrs[attr]["multivalue"]:
                if (not "type" in self.attrs[attr] 
                    or self.attrs[attr]["type"]==None):
                    if self.verbose: print ("add single value") 
                    obj[attr] = data[attr]
                    pass
                elif (self.attrs[attr]["type"]==int):
                    if self.verbose: print ("add integer value") 
                    obj[attr] = int(data[attr])
                    pass
                elif (self.attrs[attr]["type"]==float):
                    if self.verbose: print ("add float value") 
                    obj[attr] = float(data[attr])
                    pass
                else:
                    if self.verbose: print ("add single object" )
                    obj[attr] = self.db.get_obj(self.attrs[attr]["type"],data[attr])
                    pass
                pass
            else:
                if not "type" in self.attrs[attr] or self.attrs[attr]["type"]==None:
                    if self.verbose: print ("add multi value:", data[attr])
                    ee=[]
                    if extend or remove: ee.extend(obj[attr])
                    if isinstance(data[attr], str):
                        ob=data[attr]
                        if not ob in ee: ee.append(ob)
                        if ob in ee and remove: ee.remove(ob)
                        pass
                    else:
                        for ob in data[attr]:
                            if not ob in ee: ee.append(ob)
                            if ob in ee and remove: ee.remove(ob)
                            pass
                        pass
                    obj[attr]=ee
                    pass
                else:
                    if self.verbose: print ("add multi value  object") 
                    ee=[]
                    if extend or remove: ee.extend(obj[attr])
                    if isinstance(data[attr], str):
                        #print ("isInstance %s" % attr)
                        #obj[attr].append(self.db.get_obj(self.attrs[attr]["type"],data[attr]))
                        ob=self.db.get_obj(self.attrs[attr]["type"],data[attr])
                        if not ob in ee: ee.append(ob)
                        if ob in ee and remove: ee.remove(ob)
                        pass
                    else:
                        for ele in data[attr]:
                            #obj[attr].append(self.db.get_obj(self.attrs[attr]["type"],ele))
                            #print ("++++++++++++++++++++++++++++++++++++++++++++++++++++++++ele: [%s] attr[%s]" % ( ele, attr))
                            #print ("++++++++++++++++++++++++++++++++++++++++++++++++++++++++len ele: [%s]" %  len(ele))
                            if len(ele)>0:
                                ob=self.db.get_obj(self.attrs[attr]["type"],ele)
                                if not ob in ee: ee.append(ob)
                                if ob in ee and remove: ee.remove(ob)
                                pass
                            else:
                                print ("Empty attribute")
                                pass
                            pass
                        pass
                    #print (obj)
                    obj[attr]=ee
                    pass
                pass
            pass
        return obj
    
    # serialize
    # brief serialize an object into a dict
    # param obj ConfigObject
    # return dict
    def serialize(self,obj):
        ret = {}
        if not obj: return ret
        for attr in self.attrs:
            if attr == "UID": continue
            if not self.attrs[attr]["multivalue"]:
                if isinstance(obj[attr],config.ConfigObject):
                    ret[attr] = obj[attr].UID()
                    pass
                else:
                    ret[attr] = obj[attr]
                    pass
                pass
            else:
                ret[attr]=['']
                for child in obj[attr]:
                    if isinstance(child,config.ConfigObject):
                        ret[attr].append(child.UID())
                        pass
                    else:
                        ret[attr].append(child)
                        pass
                    pass
                ret[attr]=sorted(ret[attr])
                pass
            pass
        return ret 
    
    # dump
    # brief serialize object to print string
    # param obj the ConfigObject
    # return a string
    def dump(self,obj):
        s=""
        if not obj: return
        s="%20s: %s\n" % ("UID",obj.UID())
        for attr in self.attrs:
            if attr == "UID": continue
            if not self.attrs[attr]["multivalue"]:
                if isinstance(obj[attr],config.ConfigObject):
                    s+="%20s: %s\n" % (attr,obj[attr].UID())
                    pass
                else:
                    s+="%20s: %s\n" % (attr,obj[attr])
                    pass
                pass
            else:
                label=attr
                for child in obj[attr]:
                    if isinstance(child,config.ConfigObject):
                        s+="%20s: %s\n" % (label,child.UID())
                        pass
                    else:
                        s+="%20s: %s\n" % (label,child)
                        pass
                    label=""
                    pass
                pass
            pass
        return s
        
    # dump2
    # brief serialize 2 objects to print string
    # param obj1 ConfigObject 1
    # param obj2 ConfigObject 2
    # return a string
    def dump2(self,obj1,obj2):
        s=""
        if not obj1 or not obj2: return
        s="%20s: %30s : %30s\n" % ("UID",obj1.UID(),obj2.UID())
        for attr in self.attrs:
            if attr == "UID": continue
            if not self.attrs[attr]["multivalue"]:
                if isinstance(obj1[attr],config.ConfigObject):
                    s+="%20s: %30s : %30s\n" % (attr,obj1[attr].UID(),obj2[attr].UID())
                    pass
                else:
                    s+="%20s: %30s : %30s\n" % (attr,obj1[attr],obj2[attr])
                    pass
                pass
            else:
                label=attr
                lenAttr = len(obj1[attr])
                if len(obj2[attr])>lenAttr: lenAttr=len(obj2[attr])
                for i in range(lenAttr):
                    if isinstance(obj1[attr][i],config.ConfigObject):
                        if   i>len(obj1[attr]): s+="%20s: %30s : %30s\n" % (label,"",obj2[attr][i].UID())
                        elif i>len(obj2[attr]): s+="%20s: %30s : %30s\n" % (label,obj1[attr][i].UID(),"")
                        else: s+="%20s: %30s : %30s\n" % (label,obj1[attr][i].UID(),obj2[attr][i].UID())
                        pass
                    else:
                        if   i>len(obj1[attr]): s+="%20s: %30s : %30s\n" % (label,"",obj2[attr][i])
                        elif i>len(obj2[attr]): s+="%20s: %30s : %30s\n" % (label,obj1[attr][i],"")
                        else: s+="%20s: %30s : %30s\n" % (label,obj1[attr][i],obj2[attr][i])
                        pass
                    label=""
                    pass
                pass
            pass
        return s

    # get_schema
    # brief get an empty dict with the schema of the object
    # return dict
    def get_schema(self):
        ret = {}
        for attr in self.attrs:
            if not self.attrs[attr]["multivalue"]: ret[attr] = ''
            else:                             ret[attr] = ['']
        return ret
    pass

    # switch_on
    # brief Deprecated way to update the object state
    # param obj ConfigObject
    # return none
    def switch_on(self,obj):
        return self.update_state(obj)

    # switch_off
    # brief Deprecated way to update the object state
    # param obj ConfigObject
    # return none
    def switch_off(self,obj):
        return self.update_state(obj)
    

    # update_state
    # brief Update the object state
    # param obj ConfigObject
    # return None
    def update_state(self,obj,ctime=0):
        if obj.get_string("switch")=="off":
            obj.set_enum("state","off")
            return self.msgr.switchOff()
        verboseobjects=[""]
       	if self.verbose and obj.UID() in verboseobjects: print ("%s Processing.... %s" % (self.classname,obj.UID()))
        if "inhibit" in self.db.__schema__.data[obj.class_name()]["attribute"] and obj.get_bool("inhibit") is True: obj.set_enum("state","on"); return self.msgr.getOn()+" Element inhibited."
        is_satisfied_per_dependancy={} #True if the dependancy is satisfied
        for relation in self.function_per_dependancy:
        	is_satisfied_per_dependancy[relation]=getattr(self,self.function_per_dependancy[relation])(obj,relation,ctime=ctime,**self.additional_attributes_per_dependancy[relation])
        	if not is_satisfied_per_dependancy[relation]: return self._return_off(obj,is_satisfied_per_dependancy)
        obj.set_enum("state","on")
        return self.msgr.getOn() 
    
    # hasSingleRequiredWithTime
    # param obj object to evaluate
    # param relation relation to consider
    # return true if element has optional elements 
    def hasSingleRequiredWithTime(self,obj,relation,ctime,**kwargs):
        parent=obj.get_obj(relation)
        hasreq=True
        if parent==None:
            print ("===========> Single required not found! FIXME")
            return hasreq
        if parent.get_string("state")=="off":
            if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute'])*float(kwargs['time_factor']),ctime)
            return False
        return hasreq
    
    # hasRequired
    # param obj object to evaluate
    # param relation relation to consider
    # return true if element has optional elements 
    # if no required return true
    # else check that all are on
    def hasRequired(self,obj,relation,**kwargs):
        parents=obj.get_objs(relation)
        if parents==None:return True
        if len(parents)==0:return True
        for parent in parents:
            if parent.get_string("state")=="off": return False
        return True

    def hasRequiredWithTime(self,obj,relation,ctime,**kwargs):
        parents=obj.get_objs(relation)
        if parents==None:return True
        if len(parents)==0:return True
        for parent in parents:
            if parent.get_string("state")=="off":
                if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute'])*float(kwargs['time_factor']),ctime)
                return False
        return True

    def hasMultipleRequiredWithTime(self,obj,relation,ctime,**kwargs):
        parents=obj.get_objs(relation)
        quorum_counter=obj.get_u32(kwargs['quorum_attribute'])
        if parents==None:return True
        if len(parents)==0:return True
        for parent in parents:
            if parent.get_string("state")=="off":
                if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]:
                    if self.hasRemainingTime(obj.get_u32(kwargs['time_attribute'])*float(kwargs['time_factor']),ctime): return True 
                quorum_counter-=1
                if quorum_counter==0: return False
        return True
        
    def hasRequiredWithReenabling(self,obj,relation,ctime,**kwargs):
        parents=obj.get_objs(relation)
        if parents==None:return True
        if len(parents)==0:return True
        for parent in parents:
            if parent.get_string("state")=="off": 
		#Check if it is suceffully reenabled. e.g. repowered sucefully
            	reenabling_attribute=kwargs['reenabling_attribute']
            	reenabling_relation=kwargs['reenabling_relation']
            	if reenabling_attribute in self.db.__schema__.data[obj.class_name()]["attribute"] and obj.get_string(reenabling_attribute)=="true": 
            		reenabling_providers=obj.get_objs(reenabling_relation)
            		if len(reenabling_providers)==0: return True
            		for reenabling_provider in reenabling_providers:
            			if reenabling_provider.get_string("state")=="on": return True
		#Check the time attribute, e.g. UPS timespan has passed
            	if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime)
            	return False 
        return True

    def hasOptionalWithReenabling(self,obj,relation,ctime,**kwargs):
        reenabling_attribute=kwargs['reenabling_attribute']
        reenabling_relation=kwargs['reenabling_relation']
        if reenabling_attribute in self.db.__schema__.data[obj.class_name()]["attribute"] and obj.get_string(reenabling_attribute)=="true": 
        	reenabling_providers=obj.get_objs(reenabling_relation)
        	if len(reenabling_providers)==0: return True
        	for reenabling_provider in reenabling_providers:
        		if reenabling_provider.get_string("state")=="on": return True
        parents=obj.get_objs(relation)
        if len(parents)==0: return True
        for parent in parents:
            if parent.get_string("state")=="on": return True
        if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime)
        return False

    def hasExclusiveWithReenabling(self,obj,relation,ctime,**kwargs):
        reenabling_attribute=kwargs['reenabling_attribute']
        reenabling_relation=kwargs['reenabling_relation']
        if reenabling_attribute in self.db.__schema__.data[obj.class_name()]["attribute"] and obj.get_string(reenabling_attribute)=="true": 
        	reenabling_providers=obj.get_objs(reenabling_relation)
        	if len(reenabling_providers)==0: return True
        	for reenabling_provider in reenabling_providers:
        		if reenabling_provider.get_string("state")=="on": return True
        parents=obj.get_objs(relation)
        if len(parents)==0: return True
        if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime)
        count=0
        for parent in parents:
            if parent.get_string("state")=="on":
                if count==1: return False
                count+=1
        if count==0: return False
        return True

    #The optional relationship has effect only if the required relationship is satisfied. Time attribute is considered if available.
    def hasOptionalIfRequired(self,obj,relation,ctime,**kwargs):
    	required_providers=obj.get_objs(kwargs['require_relation'])
    	for required_provider in required_providers:
    	    if required_provider.get_string("state")=="off": return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime) if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"] else False
    	if len(required_providers)>0: return True
    	optional_providers=obj.get_objs(relation)
    	if len(optional_providers)==0: return True
    	for optional_provider in optional_providers:
    	    if optional_provider.get_string("state")=="on": return True
    	if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime)
    	return False

    #The optional relationship has effect only if the required relationship is satisfied. The entity can be reenable via the reenable relation.
    def hasOptionalIfRequiredWithReenabling(self,obj,relation,ctime,**kwargs):
    	required_providers=obj.get_objs(kwargs['require_relation'])
    	reenabling_attribute=kwargs['reenabling_attribute']
    	reenabling_relation=kwargs['reenabling_relation']
    	if reenabling_attribute in self.db.__schema__.data[obj.class_name()]["attribute"] and obj.get_string(reenabling_attribute)=="true": 
    		reenabling_providers=obj.get_objs(reenabling_relation)
    		if len(reenabling_providers)==0: return True
    		for reenabling_provider in reenabling_providers:
    			if reenabling_provider.get_string("state")=="on": return True
    	for required_provider in required_providers:
    		if required_provider.get_string("state")=="off": return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime) if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"] else False
    	if len(required_providers)>0: return True
    	optional_providers=obj.get_objs(relation)
    	if len(optional_providers)==0: return True
    	for optional_provider in optional_providers:
    	    if optional_provider.get_string("state")=="on": return True
    	if 'time_attribute' in kwargs and kwargs['time_attribute'] in self.db.__schema__.data[obj.class_name()]["attribute"]: return self.hasRemainingTime(obj.get_u32(kwargs['time_attribute']),ctime)
    	return False

    #Used for relations that are already evaluated in the context of other relations (e.g. renabling relations) 
    def reenablingRelation(self,obj,relation,**kwargs):
    	return True

    # hasDeployer
    # param obj object to evaluate
    # param relation relation to consider
    # return value not documented
    def hasDeployer(self,obj,relation,**kwargs):
        parents=obj.get_objs(relation)
        hasreq=True
        if parents==None:
            return hasreq
        if len(parents)==0: 
            hasreq=True
            pass
        for parent in parents:
            if parent.class_name() == "Action":
                if parent.get_bool("inhibit") == True:
                    hasreq=True
                    break
            if parent.get_string("state")=="off" :
                hasreq=False
                break
            pass
        return hasreq
        
    # hasOptional
    # param obj object to evaluate
    # param relation relation to consider
    # return true if element has optional elements 
    # if no optional return true
    # else check at least one is on 
    def hasOptional(self,obj,relation,**kwargs):
        parents=obj.get_objs(relation)
        if len(parents)==0: return True
        for parent in parents:
            if parent.get_string("state")=="on": return True
        return False

    # hasRemainingTime
    # param timeSpan time span
    # param ctime time in simulation
    # returns true if there is battery left or battery time is not set in db
    def hasRemainingTime(self,timeSpan, ctime):
        hastime=False
        if timeSpan==0 or ctime==0: 
            return False
        if timeSpan > ctime: 
            return True
        if timeSpan <= ctime: 
            return False
        return False

    # hasAtLeastOneOn
    # param parents list of parents
    # return true if element has at least one on
    def hasAtLeastOneOn(self,parents):
        ret=False
        if len(parents)==0: 
            ret=False
            pass
        for parent in parents:
            #print (" => %s : %s" % (parent.UID(),parent.get_string("state")))
            if parent.get_string("state")=="on":
                ret=True
                break
            pass
        return ret


    # get_children_tree
    # param obj
    # return dict of children
    def get_children_tree(self,obj):
        tree = {}
        for attr in self.attrs:
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "feed" in self.attrs[attr]: continue
            if not self.attrs[attr]["feed"]==True: continue
            ele=[]
            if self.attrs[attr]["multivalue"]==True:
                for obj2 in obj.get_objs(attr):
                    u={"uid":obj2.UID(),"class":obj2.class_name()}
                    ele.append(u)
                    pass
                pass
            else:
                obj2=obj.get_obj(attr)
                u={"uid":obj2.UID(),"class":obj2.class_name()}
                ele.append(u)
                pass
            if len(ele)==0:continue
            tree[attr]=ele
            pass
        return tree
    
    # get_parent_tree
    # param obj
    # return dict of parents
    def get_parent_tree(self,obj, helpers):
        tree = {}
        for attr in self.attrs:
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "gather" in self.attrs[attr]: continue
            if self.attrs[attr]["gather"] is None: continue
            #if "gather" in self.attrs[attr]:
            #print("gater: %s" % attr)
            ele=[]
            if self.attrs[attr]["multivalue"]==True:
                for obj2 in obj.get_objs(attr):
                    u={"uid":obj2.UID(),"class":obj2.class_name()}
                    ele.append(u)
                    pass
                pass
            else:
                obj2=obj.get_obj(attr)
                u={"uid":obj2.UID(),"class":obj2.class_name()}
                ele.append(u)
                pass
            if len(ele)==0:continue
            tree[attr]=ele
            pass
        return tree
    
    # get_parent_tree_draw
    # param obj
    # return dict of parents in tree format for drawing
    def get_parent_tree_draw(self,obj,info=False):
        tree = []
        for attr in self.attrs:
            print
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "gather" in self.attrs[attr]: continue
            if self.attrs[attr]["gather"] is None: continue
            ele={}
            ele["text"]={"name":attr}
            ele2=[]
            if self.attrs[attr]["multivalue"]==True:
                for obj2 in obj.get_objs(attr):
                    u={"text":{"name":obj2.UID(),"desc":obj2.class_name()}}
                    if info: u["text"]["title"]=obj2.get_string("description")
                    ele2.append(u)
                    pass
                pass
            else:
                obj2=obj.get_obj(attr)
                u={"text":{"name":obj2.UID(),"desc":obj2.class_name()}}
                if info: u["text"]["title"]=obj2.get_string("description")
                ele2.append(u)
                pass
            if len(ele2)==0:continue
            ele["children"]=ele2
            tree.append(ele)
            pass
        return tree
    
    # get_children_tree_draw
    # param obj
    # return dict of children in tree format for drawing
    def get_children_tree_draw(self,obj,info=False):
        tree = []
        for attr in self.attrs:
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "feed" in self.attrs[attr]: continue
            if not self.attrs[attr]["feed"]==True: continue
            ele={}
            ele["text"]={"name":attr}
            ele2=[]
            if self.attrs[attr]["multivalue"]==True:
                for obj2 in obj.get_objs(attr):
                    u={"text":{"name":obj2.UID(),"desc":obj2.class_name()}}
                    if info: u["text"]["title"]=obj2.get_string("description")
                    ele2.append(u)
                    pass
                pass
            else:
                obj2=obj.get_obj(attr)
                u={"text":{"name":obj2.UID(),"desc":obj2.class_name()}}
                if info: u["text"]["title"]=obj2.get_string("description")
                ele2.append(u)
                pass
            if len(ele2)==0:continue
            ele["children"]=ele2
            tree.append(ele)
            pass
        return tree
    

    ### Tentative format ###
    # tree = [
    #     {
    #         "name": "actions",
    #         "children": [
    #             {
    #                 "name": "a",
    #                 "desc": "v"
    #             },
    #             {
    #                 "name": "b",
    #                 "desc": "w"
    #             }
    #             {
    #                 "name": "0ms",  #desc needed?
    #                 "children": [
    #                     {
    #                         "name": "x",
    #                         "desc": "y"
    #                     },
    #                     ...
    #                 ]
    #             },
    #             {
    #                 "name": "300ms",
    #                 "children": [...]
    #             }
    #         ]
    #     }
    # ]
    def get_alarm_tree_draw(self,obj):
        tree = []
        for attr in self.attrs:
            if attr == "actions":
                actions = {"text": {"name": "actions"}, "children": []}
                all_actions = obj.get_objs(attr)
                for action in all_actions:
                    if action.class_name() == "DelayedAction":
                        uid_split = action.UID().split('_')
                        delay = uid_split[len(uid_split)-1]
                        # if not delay in delayed_list:
                        #     delayed_list[delay] = [{"name": action.UID(), "desc": action.class_name()}]
                        # else:
                        #     delayed_list[delay].append({"name": action.UID(), "desc": action.class_name()})
                        found = False
                        for c in actions["children"]:
                            if c["text"]["name"] == delay:
                                c["children"].append({"text": {"name": action.UID().strip('_%s' % delay), "desc": "Action"}})
                                found = True
                                break
                        if not found:
                            actions["children"].append({"text": {"name": delay, "desc": "seconds"}, "children": [{"text": {"name": action.UID().strip('_%s' % delay), "desc": "Action"}}]})
                    else:
                        actions["children"].append({"text": {"name": action.UID(), "desc": action.class_name()}})
                tree.append(actions)
        return tree
        # etc ...
        # if delayed_list:
        #     for d in delayed_list:
        #         delayed_obj = {"name": d, "children": []}
        #         for a in delayed_list[d]:
        #             delayed_obj["children"].append(a)
        #         delayed_actions["children"].append(delayed_obj)
                
                        
                        
                
    
    # get_parents
    # param obj
    # return list of parents
    def get_parents(self,obj):
        parents = []
        for attr in self.attrs:
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "gather" in self.attrs[attr]: continue
            if not self.attrs[attr]["gather"]==True: continue
            if self.attrs[attr]["multivalue"]==True: parents.extend(obj.get_objs(attr))
            else: parents.append(obj.get_obj(attr))
            pass
        return parents
    pass


    # get_parents_and_relation
    # param obj
    # return list of parents
    def get_parents_and_relation(self,obj):
        parents = []
        for attr in self.attrs:
            if not "type" in self.attrs[attr]: continue
            if self.attrs[attr]["type"] is None: continue
            if not "gather" in self.attrs[attr]: continue
            if not self.attrs[attr]["gather"]==True: continue
            if self.attrs[attr]["multivalue"]==True: parents.extend([[attr,obj.get_objs(attr)]])
            #else: parents.append([attr,obj.get_obj(attr)])
            pass
        return parents
    pass

    # add_log
    # param obj the ConfigObject
    # param record message to log
    def add_log(self, obj, record):
        #print "obj.UID()", obj.UID()
        #print obj
        #print dir(obj)
        #print obj.get_objs("logger")
        ee=['',]
        #print "ee1:", ee
        ee.extend(obj.get_string_vec("logger"))
        #print "ee2:", ee
        ee.append(record)
        #print "ee3:", ee
        obj.set_string_vec("logger", ee)
        pass
    
    # get_log
    # param obj the ConfigObject
    # param record message to log
    def get_log(self, obj):
        return obj["logger"]
        
    # edit_object
    # param string uid of the object
    # param dict parameters to process
    # param bool if should be removed
    # param bool if should be extended
    # param bool if run in batch mode
    # return true if changes processed
    def edit_object(self,uid,args,remove,extend,batch):
        if not args: return False
        data={}
        data["UID"]=uid
        vargs = vars(args)
        for attr in self.attrs:
            if attr.lower() in vargs: data[attr]=vargs[attr.lower()]
            pass
        
        print (self.dump(self.getObj(uid)))
        
        self.addObj(data,remove,extend)
    
        print (self.dump(self.getObj(uid)))
        
        if not batch:
            ans=input("Do you wish to continue? [y/n]: ")
            if "y" in ans.lower(): batch=True
            pass
        if batch:
            print ("Commit changes")
            self.db.commit("")
            return True
            pass
        return False
    
    # fill_parser with attributes
    # param parser ArgumentParser object
    # return true if changes processed
    def fill_parser(self,parser):
        for attr in self.attrs:
            if attr == "UID": 
                parser.add_argument("--uid",help="Object UID",required=True)
                continue
            a1 = "--%s" % attr.lower()
            a2 = "%s other object" % attr
            if "multivalue" in self.attrs[attr] and self.attrs[attr]["multivalue"]==True:
                parser.add_argument(a1,help=a2,nargs='+')
                pass
            else:
                parser.add_argument(a1,help=a2)
                pass
            pass
        pass
    
    # delete and object from the database
    # will not work if the object is referenced
    # will print the exception trace in case cannot be deleted
    # param classname the object's class
    # param uid the object's UID
    # return true if deleted, false otherwise
    def destroyObj(self, classname, uid):
        obj=self.db.get_obj(classname,uid)
        if not obj:
            return False
        else:
            try:
                self.db.destroy_obj(obj)
                return True
            except RuntimeError as err:
                print(err)
                return False
            pass
        pass
    
    
    # cmdline
    def cmdline(self,clazz=None):

        import os
        import sys
        import config
        import argparse        
    
        parser = argparse.ArgumentParser()
        parser.add_argument("database",help="database file")
        parser.add_argument("-c","--class_name",help="class to edit")
        parser.add_argument("-V","--verbose",help="verbose mode",default=False,action='store_true')
        parser.add_argument("-e","--extend",help="extend object",action='store_true')
        parser.add_argument("-r","--remove",help="remove action",action='store_true')
        parser.add_argument("-y","--yes",help="dont ask confirmation",action='store_true')
        args, unknown = parser.parse_known_args()
        print(args.verbose)

        print ("Load database: %s" % args.database)
        db=config.Configuration("oksconfig:%s"%(args.database))
        if not clazz: clazz=args.class_name
        helper=Helper(clazz,db,verbose=args.verbose)
        helper.fill_parser(parser)

        args=parser.parse_args()
    
        helper.edit_object(args.uid,args,args.remove,args.extend,args.yes)
        pass

    def getPowerSource(self, obj):
        parents=obj.get_objs("poweredBy")
        if len(parents)==0: return {}
        ret={}
        for parent in parents:
            if parent.class_name()=="Switchboard":
                ptype="Normal"
                if "EXD" in parent.UID(): ptype="Normal" 
                elif "EOD" in parent.UID(): ptype="UPS" 
                elif "ESD" in parent.UID(): ptype="Diesel" 
                ret[parent.UID()]=ptype
                pass
            pass
        return ret

    # Function used to put the object state in off and set the appropiate return value
    def _return_off(self,obj,is_satisfied_per_dependancy):
        verboseobjects=[""]
       	obj.set_enum("state","off")
       	if self.verbose:
        	reasons = ""
        	for relation in is_satisfied_per_dependancy: reasons += "%s [%s]. " % (relation,is_satisfied_per_dependancy[relation])
       		if obj.UID() in verboseobjects:
       	 		print("Reason [%s]---> %s" % (obj.UID(), reasons))
        	return self.msgr.getOff() + reasons  
       	else: return self.msgr.getOff()

if __name__ == '__main__':
    Helper().cmdline()
