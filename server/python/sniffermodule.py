#!/usr/bin/env python
#############################################
# Sniffer Module helper
# Carlos.Solans@cern.ch
# February 2019
#############################################

import helper

class SnifferModuleHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("SnifferModule")
    print ("Have a nice day")
