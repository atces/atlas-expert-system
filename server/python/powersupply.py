#!/usr/bin/env python
# ############################################
# PowerSupply Helper
# ignacio.asensi@cern.ch
# January 2018
# August 2018: fix cmdline
# ############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class PowerSupplyHelper(NetHelper,helper.Helper):
    pass
    
if __name__ == '__main__':

    helper.Helper().cmdline("PowerSupply")
    print ("Have a nice day")
