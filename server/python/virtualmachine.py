#!/usr/bin/env python
#############################################
# VirtualMachine helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# gustavo.uribe@cern.ch
# February 2020
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class VirtualMachineHelper(NetHelper,helper.Helper):
    pass


if __name__ == '__main__':

    helper.Helper().cmdline("VirtualMachine")
    print ("Have a nice day")
    
