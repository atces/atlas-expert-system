#!/usr/bin/env python
# ############################################
# Expert System simulations tester
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
# January 2020 - Major refactoring
# Florian.Haslbeck@cern.ch
# replaced ROOT with gnuplot
# ############################################

import os
import argparse
import socket
import json
import time
import tests
import threading

class SimulationRunner :
    
    def __init__(self,host,port,num):
        self.host=host
        self.port=port
        self.data={}
        self.number=num
        self.verbose=False
        self.output="simulation_number_%d_%s" % (num, time.strftime("%Y%m%d_%H%M%S"))
        pass
        
    def setVerbose(self,enable):
        self.verbose=enable
        pass
        
    def sendCmd(self,cmd, uid="", tokenid=""):
        print("Open socket")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host,self.port))
        
        req = {"cmd": cmd}
        if uid!="": req["uid"]=uid
        if tokenid!="": req["TokenId"]=tokenid
    
        print("Send command: %r"%req)
        s_req = json.dumps(req)
        s.sendall(s_req.encode())
    
        print ("Read the message size")
        rep={}
        s_rep=""
        nrep = 0
        try:
            nrep = int(s.recv(8).decode('utf-8'),16)
        except:
            print("Invalid message size")
            pass
        print ("Read the message of size: %i" %nrep)
        while len(s_rep)<nrep:
            s_rep += s.recv(50000).decode('utf-8')
            pass
        if self.verbose: print(s_rep)
        try:
            rep = json.loads(s_rep)
            print ("Message received")
            pass
        except:
            print ("Error parsing message")
            pass
        print("Close socket")
        s.close()
        return rep
    
    def run(self,threadid=0,timing={}):
        startTime=time.time()
        if not self.number in tests.test: 
            print("Test not found: %i" % self.number)
            return
        print("Running test: %i" % self.number)
        print("Get token")
        tokenid = self.sendCmd("GetNewToken")["TokenId"]
        print("Testing...")
        for uid in tests.test[self.number]:
            self.sendCmd("ChangeSwitch", uid, tokenid)
            pass
        print ("Simulation done. Getting affected systems")
        affected_systems = self.sendCmd("GetAffectedSystems", "dummy", tokenid)["Result"]
        number_of_affected_systems = self.sendCmd("GetNumberOfAffectedSystems", "dummy", tokenid)["Result"]
        print ("Saving to file: %s.txt" % self.output)
        self.data = {
            "uids": tests.test[self.number],
            "number_of_affected_systems": number_of_affected_systems,
            "affected_systems": affected_systems
        }
        timing[threadid]=time.time()-startTime
        pass

    def run_all(self,limit=1000):
        print("Get token")
        tokenid = self.sendCmd("GetNewToken")["TokenId"]
        for test_number in tests.test:
        	print("Running test: %i" % test_number)
        	for uid in tests.test[test_number]:
        		self.sendCmd("ChangeSwitch", uid, tokenid)
        	print ("Simulation done. Getting affected systems")
        	if test_number>limit: break
        pass
    
    def save(self):
        with open(self.output+".txt", 'w') as outfile:
            json.dump(self.data, outfile)
            pass
        pass
    
    def compare(self,path):
        print("Loading previous result")
        prev=json.load(open(path))
        simdiff=False
        print("Comparing number_of_affected_systems")
        if self.data["number_of_affected_systems"]!=prev["number_of_affected_systems"]: 
            simdiff=True
            pass
        print("Comparing uids")
        if set(self.data["uids"])!=set(prev["uids"]): 
            simdiff=True
            pass
        print("Comparing affected_systems")
        if len(self.data["affected_systems"])!=len(prev["affected_systems"]): 
            simdiff=True
            pass
        for k in self.data["affected_systems"]:
            if not k in prev["affected_systems"]: 
                simdiff=True
                break
            if self.data["affected_systems"][k]!=prev["affected_systems"][k]:
                simdiff=True
                pass
            pass
        print("Test: %s" % ("FAILED" if simdiff else "PASSED"))
        pass
    
    def threadRunner(self,nthreads):
        threads=[]
        timing={}
        for i in range(nthreads):
            print("Start thread: %i" %i)
            thread = threading.Thread(target=self.run,args=(i,timing))
            threads.append(thread)
            thread.start()
            pass
        for i,thread in enumerate(threads):
            print("Join thread: %i" %i)
            thread.join()
            pass
        for thread in timing:
            print("Thread: %i took: %.2f seconds" % (thread,timing[thread]))
            pass
        return timing
        
    def runner(self,nthreads):
        # create gnuplot macro 
        f_gnu = open("temp_TimeVsThreadsGNU.tsv","w")
        ##f_gnu.write("set terminal pdf\n") # create pdf and do not open gnu window
        ##f_gnu.write("set output '%s.pdf'\n"%(self.output)) # ...
        f_gnu.write("set output '%s.png'\n"%(self.output))
        f_gnu.write("set xlabel 'NThreads'\n")
        f_gnu.write("set ylabel 'Time [seconds]'\n")
        f_gnu.write("set style line 1 linecolor rgb '#0060ad' pointtype 5 pointsize 1\n") #blue squares
        f_gnu.write("plot 'temp_TimeData.tsv' linestyle 1\n") #plot from file with defined style
        f_gnu.close()
        
        # create data file
        f_data=open("temp_TimeData.tsv","w")
        for n in range(nthreads):
            timing=self.threadRunner(n+1)
            maxtime=0
            for k in timing:
                maxtime+=timing[k]
                pass
            f_data.write("%.1f    %.f\n"%(n+1,maxtime)) #write to data file
            pass
        f_data.close()
        os.system("gnuplot -p temp_TimeVsThreadsGNU.tsv < temp_TimeData.tsv") #call gnuplot 
        pass


if __name__ == '__main__':
    verbose = False
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", help="Server host name", required=True)
    parser.add_argument("-n", "--number", help="Number of test", type=int, required=True)
    parser.add_argument("-p", "--port", help="Server port number. Default 9998", type=int, default=9998)
    parser.add_argument("-v", "--verbose", help="Enable verbose mode", action="store_true")
    parser.add_argument("-c", "--compare", help="Compare with previous simulation file")
    parser.add_argument("-t", "--threads", help="Performance vs thread cound", type=int)
    args = parser.parse_args()
    
    print ("Testing server running in %s:%i" % (args.server, args.port))

    sim = SimulationRunner(args.server,args.port,args.number)
    sim.setVerbose(args.verbose)
    if args.number==-1: sim.run_all()
    else: sim.run()
    sim.save()
    
    if args.compare: sim.compare(args.compare)
    if args.threads: sim.runner(args.threads)
    
    print ("Have a nice day")
