#!/usr/bin/env python
##################################
# Compare json files
#
# Carlos.Solans@cern.ch
# January 2020
##################################

import os
import sys

def compare_list(l1,l2,n1,n2,v=False,d=False):
    diff=False
    for i in range(len(l1)):
        if i>len(l2): break
        if d: print ("Found element %s in %s" % (k,n1))
        if isinstance(l1[i],dict):
            if d: print ("i is a dict")
            diff=diff and compare_dict(l1[i],l2[i],"%s[%s]"%(n1,i),"%s[%s]"%(n2,i),v,d)
            pass
        elif isinstance(l1[i],list):
            if d: print ("i is a list")
            diff=diff and compare_list(l1[i],l2[i],"%s[%s]"%(n1,i),"%s[%s]"%(n2,i),v,d)
            pass
        elif isinstance(str(l1[i]),str):
            if d: print ("k is a string")
            if l1[i] != l2[i]:
                diff=True
                if v: print ("%s[%s]=%s | %s[%s]=%s"%(n1,i,l1[i],n2,i,l2[i]))
                pass
            pass
        pass
    return diff

def compare_dict(d1,d2,n1,n2,v=False,d=False):
    diff=False
    for k in d1:
        if d: print ("Found key %s in %s" % (k,n1))
        if not k in d2:
            diff=True
            if v: print ("%s[%s]=%s | %s[%s]=?"%(n1,k,type(d1[k]),n2,k))
            continue
        if isinstance(d1[k],dict):
            if d: print ("k is a dict")
            diff=diff and compare_dict(d1[k],d2[k],"%s[%s]"%(n1,k),"%s[%s]"%(n2,k),v,d)
            pass
        elif isinstance(d1[k],list):
            if d: print ("k is a list")
            if len(d1[k]) != len(d2[k]):
                diff=True
                if v: print ("%s[%s].size=%i | %s[%s].size=%i"%(n1,k,len(d1[k]),n2,k,len(d2[k])))
                pass
            diff=diff and compare_list(d1[k],d2[k],"%s[%s]"%(n1,k),"%s[%s]"%(n2,k),v,d)
            pass
        elif isinstance(str(d1[k]),str):
            if d: print ("k is a string")
            if d1[k] != d2[k]:
                diff=True
                if v: print ("%s[%s]=%s | %s[%s]=%s"%(n1,k,d1[k],n2,k,d2[k]))
                pass
            pass
        else:
            print ("k is %s" % type(k))
        pass
    return diff

if __name__=="__main__":
    
    import json
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('file1',help='file1')
    parser.add_argument('file2',help='file2')
    parser.add_argument('-v','--verbose',action='store_true',help='verbose')
    parser.add_argument('-debug','--debug',action='store_true',help='verbose')
    args = parser.parse_args()

    j1=json.load(open(args.file1))
    j2=json.load(open(args.file2))

    n1=os.path.basename(args.file1)
    n2=os.path.basename(args.file2)

    if args.verbose: print ("Comparing %s to %s" % (n2, n1))
    
    diff=compare_dict(j1,j2,n1,n2,args.verbose,args.debug)

    if diff: print("Files differ")
    
