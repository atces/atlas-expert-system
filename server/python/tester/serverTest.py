#!/usr/bin/env python
import os
import sys
import ROOT
import argparse

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.035)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
ROOT.gStyle.SetOptStat(0)
    
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', help='input file')
parser.add_argument('-o', '--output', help='output file')
args = parser.parse_args()

fr=open(args.input)
fw=ROOT.TFile(args.output,"RECREATE")
hv={}
i=0
for line in fr.readlines():
    line=line.strip()
    if len(line)==0: continue
    chks=line.split(" ")
    key=chks[0]
    key=key.split("?")[1].split("&")[0]
    key=key.replace("cmd=","")
    if len(key)==0: key="None"
    val=int(chks[1])
    if not key in hv:
        hv[key]=ROOT.TH1F("hv_%i"%i,";Time (ms)",100,50,349)
        hv[key].SetLineColor(i+1)
        hv[key].SetLineWidth(2)
        i+=1
        pass
    hv[key].Fill(val)
    pass

c1=ROOT.TCanvas("cTime","Response Time (ms)",800,600)
hs=ROOT.THStack()
hs.SetTitle(";Time [ms]")
lg=ROOT.TLegend(0.5,0.6,0.9,0.9)
for key in hv:
    hv[key].Write()
    hs.Add(hv[key])
    lg.AddEntry(hv[key],"%s (%.0f #pm %0.f ms)"%(key,hv[key].GetMean(),hv[key].GetRMS()),"l")
    pass
hs.Draw("NOSTACK")
lg.Draw()
c1.Write()
hs.Write()
c1.SaveAs("autoplot.pdf")

raw_input("wait...")

fr.close()
fw.Close()

