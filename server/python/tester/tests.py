#!/usr/bin/env python
# ############################################
import config

test={}
test[0]=""
test[1]=["Y.09-19.A1", "SGX1.X16-Y05"]
test[2]=["EOD1_1DX", "UIAC-00114", "EQD1_1H"]
test[3]=["D3125-U2-IP41-SBR4L-1"] #Failure of switch connecting private services
test[4]=["Breaker_Y2419A2_EOD6","Breaker_Y2419A2_EOD2"] #USA15 starpoint poweroff
test[5]=["D3125-2-RJUEM-1"] #USA15 main router failure
test[6]=["D3125-1V-IP41-OBR4M-0202"] #Failure of single switch
test[7]=["D3178-2V-IP23-LBR4L-1506"] #Failure of switch in redundant configuration
test[8]=["PC-ATLAS-CORE-01","PC-ATLAS-CORE-02"] #Failure of core network services on SDX1
test[9]=["EHD21_9E"] 


#db=config.Configuration("oksconfig:%s"%"../../../data/sb.data.xml")
#systems=db.get_objs("Switchboard")
#test={i:[system.UID()] for i,system in enumerate(systems)}
