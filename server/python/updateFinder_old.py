#!/usr/bin/env python
####################################
# Get $data vars from all pages
# put them together
# ignacio.asensi@cern.ch
# April 2019
# June 2019: fix to execute from anywhere
# August 2019: read mxgraph xml
# 
# MXGRAPH container is URL encoded, compressed (base 15), and b64 encoded
# To decode:
# import base64
# import zlib
# import urllib
#
# data=base64.b64decode(data)
# data=zlib.decompress(data,-15)
# data=urllib.unquote(data)
#
####################################

import os
import sys
import json
import zlib
import urllib
import xml.dom.minidom
import base64
import argparse
import datetime

parser=argparse.ArgumentParser()
parser.add_argument('-v','--verbose',help="enable verbose mode", action='store_true')
parser.add_argument('-d','--dry',help="dry run", action='store_true')
args=parser.parse_args()

#this allows to run from anywhere
basedir=os.path.dirname(os.path.realpath(__file__))
infile="%s/../../login/data/menuTree.json" % basedir
outfile="%s/../../login/data/PHPdatagrouped.php" % basedir
indir="%s/../../login/" %basedir

print("Open file: %s" % infile)
data = json.load(open(infile))

pages={}
for page in data:
    print ("Parse file: %s\n" % page)
    feed=False
    try:
        for line in open('%s/%s.php' % (indir,page)).readlines():
            line=line.strip()
            #Standard type
            if "data=" in line:
                feed=True
                pages[page]=[]
                pass
            elif  feed==True:
                if ");" in line:
                    feed=False
                    pass
                elif "=>" in line:
                    if args.verbose: print ("Parse: %s"%line)
                    ele = line.split("=>")[0].replace('"','').strip()
                    if ele.startswith("/"): continue
                    print ("  Found %s" % ele)
                    pages[page].append(ele)
                    pass
                pass
            #XML type
            if "renderGraph" in line:
                pages[page]=[]
                print ("==========================")
                print ("       RENDER GRAPH       ")
                print ("==========================")
                p1=line.index("(")
                p2=line.index(")")
                xfile=line[p1+1:p2].replace("'","").replace('"','').strip()
                xfile="%sdata/xmlgraphs/%s.xml"%(indir,page)
                xfile2="%sdata/xmlgraphs/%s.drawio"%(indir,page)
                if (os.path.exists(xfile2)): xfile=xfile2
                print ("parse xml: %s"%xfile)
                doc=xml.dom.minidom.parse(xfile)
                xdata=doc.getElementsByTagName("diagram")[0].firstChild.nodeValue;
                xdata=base64.b64decode(xdata)
                xdata=zlib.decompress(xdata,-15)
                xdata=urllib.unquote(xdata)
                gdata=xml.dom.minidom.parseString(xdata)
                for ele in gdata.getElementsByTagName('object'):
                    if ele.hasAttribute("UID"): 
                        uid=ele.getAttribute("UID").strip()
                        print ("  Found %s" % uid)
                        pages[page].append(uid)
                        pass
                    elif ele.hasAttribute("uid"): 
                        uid=ele.getAttribute("uid").strip()
                        print ("  Found %s" % uid)
                        pages[page].append(uid)
                        pass
                    pass
                pass
            pass
        pass
    except:
        print ("ERROR in file: %s" % page)
        pass
    print ("")
    print ("-------------------------------")
    pass

if not args.dry:
    now = datetime.datetime.now()
    snow = datetime.date.strftime(now,"%H:%M:%S %d-%m-%Y")
    print ("Save output to file: %s"%outfile)
    fw = open(outfile, "w")
    fw.write("<?php\n")
    fw.write("/*****************************************\n")
    fw.write(" * This file was automatically generated  \n")
    fw.write(" * by updateFinder.py on \n")
    fw.write(" * %s \n" %snow)
    fw.write(" * Please do not modify\n")
    fw.write(" ****************************************/\n")
    fw.write("\n")
    fw.write("$data = array();\n")
    for page in pages:
        text='","'.join(sorted(pages[page]))
        fw.write('$data["%s"]=array("%s");\n' % (page,text));
        pass
    fw.write("\n")
    fw.write("?>\n")
    fw.close()
    pass
    
print("Have a nice day")
