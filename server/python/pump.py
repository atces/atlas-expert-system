#!/usr/bin/env python
#############################################
# VacuumPump Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
#############################################

import helper

class PumpHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("Pump")
    print ("Have a nice day")
