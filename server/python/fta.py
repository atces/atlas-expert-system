#!/usr/bin/env python
#############################################
# Fault Tree Analysis 
# Saba.Kvesitadze@cern.ch
# Carlos.Solans@cern.ch
# July 2017
#############################################

import os
import math
import datetime
import subprocess

class PoF(dict):
    def __init__(self):
        for k in ("qil","qir","pil","pir"):
            self.__setattr__(k,None)
            pass
        pass
    def __str__(self):
        return "[%s, %s, %s, %s]"%(str(self.qil),str(self.pil),str(self.pir),str(self.qir))
    pass

class FTA(object):
    def __init__(self):
        if not os.path.exists("/root/server/fta"):
            os.system("mkdir -p /root/server/fta")
            pass
        self.pofs = {}
        pass
    
    @staticmethod
    def average(my_list):
        sum_of_grades = sum(my_list)
        average = sum(my_list) / len(my_list)
        return average

    @staticmethod
    def variance(my_list, average):
        variance = 0
        for i in my_list:
            variance += (average - i) ** 2
            pass
        return variance / len(my_list)

    @staticmethod
    def datediff(d1, d2):
        d1 = dt.strptime(d1, "%Y-%m-%d")
        d2 = dt.strptime(d2, "%Y-%m-%d")
        return abs((d2 - d1).days)
    
    @staticmethod
    def total_seconds(td):
        return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

    ## GetPoF
    # @brief Get the Probability of Failure from memory or parse it from the file
    # @param name of the object
    # @return PoF of the object or None if file not found
    def GetPoF(self, name):
        if name in self.pofs: return self.pofs[name]
        if not os.path.exists(self.getFileName(name)): return None
        pof = self.parseFile(self.getFileName(name))
        self.pofs[name]=pof
        return pof
    
    ## parseFile
    # @brief parse a DDV file
    # @param path to file to parse
    # @return PoF
    def parseFile(self,path):
        fr = open(path)
        timedifvalues = []
        fsm=0
        date0=None
        for line in fr.readlines():
            line = line.strip()
            words = line.split(" ")
            name = words[0]
            state = int(float(words[2]))
            date = datetime.datetime.strptime("%s %s" % (words[3],words[4][:-4]), '%d-%m-%Y %H:%M:%S')
            if fsm==2 and state==0:
                fsm=0
                pass
            if fsm==0: 
                date0=date
                fsm=1
                pass
            elif fsm==1 and state==1:
                if not date0: continue
                timedifvalues.append(self.total_seconds(date-date0))
                fsm=2
                pass            
            pass
        avrg = int(self.average(timedifvalues))
        var = int(self.variance(timedifvalues, avrg))
        stdev = int(math.sqrt(var))
        mddev = int(stdev*3)
        mid = date0 + datetime.timedelta(seconds=avrg)
        current = datetime.datetime.now()
        multiplier = 1 #(current-mid) / (mid - datetime.timedelta(seconds=mddev))
        if current > mid: 
            mid = datetime.datetime.now()            
        
        pof = PoF()
        pof.qil = mid - datetime.timedelta(seconds=mddev)
        pof.pil = mid - datetime.timedelta(seconds=stdev)
        pof.pir = mid + datetime.timedelta(seconds=stdev)
        pof.qir = mid + datetime.timedelta(seconds=mddev)
        return pof
    
    def getFileName(self,name):
        return '/root/server/fta/fta_%s.txt'%name
    
    def getStats(self,name,comment,start,end,delta):
        url = 'http://atlas-ddv.cern.ch:8089/multidata/getDataSafely'
        curr = start
        i=0
        fw = open(self.getFileName(name),'w+')
        while curr<end:
            next = min(curr+delta,end)
            cmd = 'wget --post-data "queryInfo=atlas_pvssDCS, comment_, ' + comment +', '
            cmd += curr.strftime("%d-%m-%Y 00:00") + ', ' + next.strftime("%d-%m-%Y 00:00") +', '
            cmd += ', , , , ,no, , +2!" ' + url
            cmd += " -O /tmp/fta_tmp.txt"
            curr=next
            print (cmd)
            #os.system(cmd)
            subprocess.Popen(cmd,shell=True).wait()
            fr=open("/tmp/fta_tmp.txt")
            for line in fr.readlines():
                line=line.strip()
                chks=line.split(",")
                for i in range(len(chks)-1):
                    fw.write("%s %s\n" % (chks[0],chks[i+1]))
                    pass
                pass
            cmd="rm -rf /tmp/fta_tmp.txt"
            #os.system(cmd)
            subprocess.Popen(cmd,shell=True).wait()
            pass
        fw.close()
        pass
    
    pass

#def perdelta(start, end, delta):
#    curr = start
#    while curr < end:
#        yield curr, min(curr + delta, end)
#        curr += delta
#        pass
     

def update():    
    
    table={
"EBD1_15A":{"comment":"EXT ElectricityDistribution Switchboard EBD1/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_0.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EBD1/15A"},
"EBD2_15A":{"comment":"EXT ElectricityDistribution Switchboard EBD2/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_1.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EBD2/15A"},
"EBD3_15A":{"comment":"EXT ElectricityDistribution Switchboard EBD3/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_2.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EBD3/15A"},
"EBD4_15A":{"comment":"EXT Electricity Distribution Switchboard EBD4/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_3.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EBD4/15A"},
"EKD1_15A":{"comment":"EXT Electricity Distribution Switchboard EKD101_15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_2.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EKD101_15A"},
"EOD3_15A":{"comment":"EXT Electricity Distribution Switchboard EOD3/15A - OLD","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_4.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EOD3/15A - OLD"},
"EOD4_15A":{"comment":"EXT Electricity Distribution Switchboard EOD4/15A - OLD","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_6.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EOD4/15A - OLD"},
"EOD5_15A":{"comment":"EXT Electricity Distribution Switchboard EOD5/15A - OLD","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_3.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EOD5/15A - OLD"},
"EQD1_15A":{"comment":"EXT Electricity Distribution Switchboard EQD1/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_5.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EQD1/15A"},
"EQD1_15X":{"comment":"EXT Electricity Distribution Switchboard EQD1/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_4.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EQD1/15X"},
"ESD1_15A":{"comment":"EXT Electricity Distribution Switchboard ESD1/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_7.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/ESD1/15A"},
"ESD2_15A":{"comment":"EXT Electricity Distribution Switchboard ESD2/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_0.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/ESD2/15A"},
"EWD1_15A":{"comment":"EXT Electricity Distribution Switchboard EWD1/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_4.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EWD1/15A"},
"EXD1_15A":{"comment":"EXT Electricity Distribution Switchboard EXD1/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_F_6.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD1/15A"},
"EXD1_15X":{"comment":"EXT Electricity Distribution Switchboard EXD1/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_0.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD1/15X"},
"EXD16_15X":{"comment":"EXT Electricity Distribution Switchboard EXD16/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_1.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD16/15X"},
"EXD17_15X":{"comment":"EXT Electricity Distribution Switchboard EXD17/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_2.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD17/15X"},
"EXD2_15A":{"comment":"EXT Electricity Distribution Switchboard EXD2/15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_5.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD2/15A"},
"EXD2_15X":{"comment":"EXT Electricity Distribution Switchboard EXD2/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_3.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD2/15X"},
"EXD21_15X":{"comment":"EXT Electricity Distribution Switchboard EXD21/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_4.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD21/15X"},
"EXD22_15X":{"comment":"EXT Electricity Distribution Switchboard EXD22/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_5.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD22/15X"},
"EXD23_15X":{"comment":"EXT Electricity Distribution Switchboard EXD23/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_6.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD23/15X"},
"EXD24_15X":{"comment":"EXT Electricity Distribution Switchboard EXD24/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_F_7.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD24/15X"},
"EXD26_15X":{"comment":"EXT Electricity Distribution Switchboard EXD26/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_1.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD26/15X"},
"EXD27_15X":{"comment":"EXT Electricity Distribution Switchboard EXD27/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_2.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD27/15X"},
"EXD29_15X":{"comment":"EXT Electricity Distribution Switchboard EXD29/15X","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_3.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD29/15X"},
"EXD3_15A":{"comment":"EXT Electricity Distribution Switchboard EXD3/15A","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_6.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD3/15A"},
"EXD4_15A":{"comment":"EXT Electricity Distribution Switchboard EXD4/15A","field":"ATLCICUSAL1:ELMB/PSUbus/ELMB_1/DI/di_A_7.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD4/15A"},
"EXD404_15A":{"comment":"EXT Electricity Distribution Switchboard EXD402_15A","field":"ATLCICUSAL1:ELMB/PSUbus/EnvContr_ELMB_2/DI/di_A_1.value","navigation":"DCS/ATLCICUSAL1/EXT/Electricity/Distribution/Switchboard/EXD402_15A"},
"EBD1_15X":{"comment":"EXT Electricity Distribution Switchboard EBD1/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_7.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD1/15X"},
"EBD2_15X":{"comment":"EXT Electricity Distribution Switchboard EBD2/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_0.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD2/15X"},
"EBD3_15X":{"comment":"EXT Electricity Distribution Switchboard EBD3/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_1.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD3/15X"},
"EOD2_15":{"comment":"EXT Electricity Distribution Switchboard EOD2/15 - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_6.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EOD2/15 - OLD"},
"ESD1_15X":{"comment":"EXT Electricity Distribution Switchboard ESD1/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_2.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD1/15X"},
"ESD2_15X":{"comment":"EXT Electricity Distribution Switchboard ESD2/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_3.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD2/15X"},
"ESD2_15":{"comment":"EXT Electricity Distribution Switchboard ESD2_15","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_5.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD2_15"},
"EXD1_15":{"comment":"EXT Electricity Distribution Switchboard EXD1/15","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_4.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD1/15"},
"EXD11_15X":{"comment":"EXT Electricity Distribution Switchboard EXD11/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_0.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD11/15X"},
"EXD12_15X":{"comment":"EXT Electricity Distribution Switchboard EXD12/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_1.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD12/15X"},
"EXD13_15X":{"comment":"EXT Electricity Distribution Switchboard EXD13/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_2.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD13/15X - OLD"},
"EXD14_15X":{"comment":"EXT Electricity Distribution Switchboard EXD14/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_3.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD14/15X"},
"EXD15_15X":{"comment":"EXT Electricity Distribution Switchboard EXD15/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_4.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD15/15X - OLD"},
"EXD25_15X":{"comment":"EXT Electricity Distribution Switchboard EXD25/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_6.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD25/15X - OLD"},
"EBD1_1DX":{"comment":"EXT Electricity Distribution Switchboard EBD1/1DX","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_6.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EBD1/1DX"},
"EBD3_1GX":{"comment":"EXT Electricity Distribution Switchboard EBD3/1GX","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_A_6.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EBD3/1GX"},
"EKD1_1H":{"comment":"EXT Electricity Distribution Switchboard EKD1_1H","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_0.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EKD1_1H"},
"EKD2_1H":{"comment":"EXT Electricity Distribution Switchboard EKD2_1H","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_1.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EKD2_1H"},
"EQD1_1H":{"comment":"EXT Electricity Distribution Switchboard EQD1/1H","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_2.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EQD1/1H"},
"ESD1_1DX":{"comment":"EXT Electricity Distribution Switchboard ESD1/1DX","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_7.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/ESD1/1DX"},
"ESD4_1DX":{"comment":"EXT Electricity Distribution Switchboard ESD4/1DX","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_A_2.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/ESD4/1DX"},
"EXD1_1DX":{"comment":"EXT Electricity Distribution Switchboard EXD1/1DX","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_A_7.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/EXD1/1DX"},
"UIAC111_1UX":{"comment":"EXT Electricity Distribution Switchboard UIAC111/1UX - OLD","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_5.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/UIAC111/1UX - OLD"},
"UIAC114_1UX":{"comment":"EXT Electricity Distribution Switchboard UIAC114/1UX - OLD","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_3.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/UIAC114/1UX - OLD"},
"UIAC115_1UX":{"comment":"EXT Electricity Distribution Switchboard UIAC115/1UX - OLD","field":"ATLCICSDX1:ELMB/PSU_Bus/ELMB_2/DI/di_F_4.value","navigation":"DCS/ATLCICSDX1/EXT/Electricity/Distribution/Switchboard/UIAC115/1UX - OLD"},
"EBD1_15X":{"comment":"EXT Electricity Distribution Switchboard EBD1/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_7.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD1/15X"},
"EBD2_15X":{"comment":"EXT Electricity Distribution Switchboard EBD2/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_0.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD2/15X"},
"EBD3_15X":{"comment":"EXT Electricity Distribution Switchboard EBD3/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_1.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EBD3/15X"},
"EOD2_15":{"comment":"EXT Electricity Distribution Switchboard EOD2/15 - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_6.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EOD2/15 - OLD"},
"ESD1_15X":{"comment":"EXT Electricity Distribution Switchboard ESD1/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_2.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD1/15X"},
"ESD2_15X":{"comment":"EXT Electricity Distribution Switchboard ESD2/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_3.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD2/15X"},
"ESD2_15":{"comment":"EXT Electricity Distribution Switchboard ESD2_15","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_5.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/ESD2_15"},
"EXD1_15":{"comment":"EXT Electricity Distribution Switchboard EXD1/15","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_A_4.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD1/15"},
"EXD11_15X":{"comment":"EXT Electricity Distribution Switchboard EXD11/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_0.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD11/15X"},
"EXD12_15X":{"comment":"EXT Electricity Distribution Switchboard EXD12/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_1.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD12/15X"},
"EXD13_15X":{"comment":"EXT Electricity Distribution Switchboard EXD13/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_2.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD13/15X - OLD"},
"EXD14_15X":{"comment":"EXT Electricity Distribution Switchboard EXD14/15X","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_3.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD14/15X"},
"EXD15_15X":{"comment":"EXT Electricity Distribution Switchboard EXD15/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_4.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD15/15X - OLD"},
"EXD25_15X":{"comment":"EXT Electricity Distribution Switchboard EXD25/15X - OLD","field":"ATLCICUS15:ELMB/PSUbus/Env_Contr_ELMB_1/DI/di_F_6.value","navigation":"DCS/ATLCICUS15/EXT/Electricity/Distribution/Switchboard/EXD25/15X - OLD"},
}

    cont = True
    def handle(signum,frame):
        print ("\nHandle signal:", signum)
        global cont
        cont=False
        pass
        
    signal.signal(signal.SIGINT, handle)
    signal.signal(signal.SIGTERM, handle)
    
    start=datetime.date(2006,5,26)
    end=datetime.date.today()
    delta=datetime.timedelta(days=30)
    fta=FTA()
    for uid in table:
        #print "Parse: %s" % uid
        if not cont: break
        fta.getStats(uid, table[uid]["comment"], start, end, delta)
        pass
    pass

     
if __name__ == '__main__':

    #import os
    #from datetime import date, timedelta        
    #url = 'http://atlas-ddv.cern.ch:8089/multidata/getDataSafely'
    #for s, e in perdelta(date(2006, 5, 26), date(2017, 5, 27), timedelta(days=30)):
    #    cmd = 'wget --post-data "queryInfo=atlas_pvssDCS, comment_, EXT Electricity Distribution Switchboard EXD1/15A, '
    #    cmd += s.strftime("%d-%m-%Y") + ' 13:56, ' + e.strftime("%d-%m-%Y") + ' 13:56, , , , , ,no, , +2!" ' + url
    #    print cmd
    #    os.system(cmd)
    #exit()

    
    import os
    import sys
    import signal
    import argparse
    import datetime
    
    parser = argparse.ArgumentParser()
    parser.add_argument("action",help="update,getpof")
    parser.add_argument("uid",help="uid")
    args=parser.parse_args()

    if args.action.lower()=="update": update()
    else: 
        fta = FTA()
        print fta.GetPoF(args.uid)
        pass
