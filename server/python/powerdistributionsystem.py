#!/usr/bin/env python
#############################################
# PowerDistributionSystem helper
# Gustavo.Uribes@cern.ch
#############################################

import helper

class PowerDistributionSystemHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("PowerDistributionSystem")
    print ("Have a nice day")
