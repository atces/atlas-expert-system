#!/usr/bin/env python
#############################################
# DelayedAction
# Carlos.Solans@cern.ch
# April 2017
# DelayedAction logic is reversed:
# State ON = Something is going on
# State OFF = Idle state
# July 2018 - reverse the logic
# August 2018: fix cmdline
#############################################

import helper

class DelayedActionHelper(helper.Helper):
    pass        
    
if __name__ == '__main__':

    helper.Helper().cmdline("DelayedAction")
    print ("Have a nice day")
