#!/usr/bin/env python

import os
import sys
import argparse
import config
import loadhelpers
import random

class ProbTree:
    # @brief initialize class attributes and relationships
    def __init__(self,db=None):
        self.db = db
        self.verbose = False
        self.helpers = loadhelpers.LoadHelpers(db).getHelpers()
        self.changed=[]#list of uids which their values have been set to 0. To get PCA formulas
        #helpers=loadhelpers.LoadHelpers(db)
        helpers = loadhelpers.LoadHelpers(db)
        classes=helpers.getClasses()
        #classes=helpers.classes
        print("ProbTree classes:")
        print(classes)
        self.probs={}
        for c in classes:
            random_val=random.uniform(0.85, 1)
            self.probs[c]=random_val
            print("Random value: %s\tClass %s" % (str(random_val),c))
            pass
        pass

    def setProbs(self, probs):
        if not probs: return
        for k in probs:
            self.probs[k]=probs[k]
            pass
        pass

    def getProbs(self):
        return self.probs

    # @brief Set DB pointer used by some methods
    # @param db Config object
    def setDb(self,db):
        self.db = db
        self.helpers = loadhelpers.LoadHelpers(db).getHelpers()
        pass

    # @brief Enable verbose mode
    # @param enable boolean verbose value
    def setVerbose(self,enable):
        self.verbose = enable
        pass
     
    # @brief Get the probability tree
    # @param class_name 
    # @param uid
    # @param level
    def getTree(self,class_name,uid,level):
        ret={"uid":uid,"class":class_name}
        obj=self.db.get_obj(class_name,uid)     
        parents=self.helpers[class_name].get_parent_tree(obj, self.helpers)
        if self.verbose: print ("Parents from helper: %s" % str(parents))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for attr in parents:
                if self.verbose: print (attr)
                if len(parents[attr])==0: continue
                for p in parents[attr]:
                    if self.verbose: print (p["uid"])
                    p.update(self.getTree(p["class"],p["uid"],level-1))
                    pass
                pass
            pass
        ret["parents"]=parents
        return ret


    # @brief Get the children tree starting from a node
    # @param class_name 
    # @param uid
    # @param level
    def getChildrenTree(self,class_name,uid,level):
        ret={"uid":uid,"class":class_name}
        obj=self.db.get_obj(class_name,uid)     
        children=self.helpers[class_name].get_children_tree(obj)
        if self.verbose: print ("Parents from helper: %s" % str(parents))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for attr in children:
                if self.verbose: print( attr)
                if len(children[attr])==0: continue
                for p in children[attr]:
                    if self.verbose: print (p["uid"])
                    p.update(self.getChildrenTree(p["class"],p["uid"],level-1))
                    pass
                pass
            pass
        ret["children"]=children
        return ret


    # @brief Get the parent tree in draw format
    # @param class_name 
    # @param uid
    # @param level
    def getTreeDraw(self,class_name,uid,level,info=False):
        ret={}    
        obj=self.db.get_obj(class_name,uid)
        children=self.helpers[class_name].get_parent_tree_draw(obj,info)
        if self.verbose: print ("Parents from  helper: %s" % str(children))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for obj2 in children:
                for obj3 in obj2["children"]:
                    ele=self.getTreeDraw(obj3["text"]["desc"],obj3["text"]["name"],level-1,info)
                    if ele:
                        if not "children" in obj3:
                            obj3["children"]=[]
                            pass
                        obj3["children"].append(ele)
                        pass
                    pass
                pass
            pass
        if len(children)>0:
            ret["children"]=children
            pass
        return ret



    # @brief Get the children tree in draw format
    # @param class_name 
    # @param uid
    # @param level
    def getChildrenTreeDraw(self,class_name,uid,level,info=False):
        ret={}    
        obj=self.db.get_obj(class_name,uid)
        children=self.helpers[class_name].get_children_tree_draw(obj, info)
        if self.verbose: print ("Children from helper: %s" % str(children))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for obj2 in children:
                for obj3 in obj2["children"]:
                    ele=self.getChildrenTreeDraw(obj3["text"]["desc"],obj3["text"]["name"],level-1)
                    if ele:
                        if not "children" in obj3:
                            obj3["children"]=[]
                            pass
                        obj3["children"].append(ele)
                        pass
                    pass
                pass
            pass
        if len(children)>0:
            ret["children"]=children
            pass
        return ret



    # @brief Get the alarm tree in draw format
    # @param class_name 
    # @param uid
    # @param level
    def getAlarmTreeDraw(self,class_name,uid,level):
        ret = {}
        obj=self.db.get_obj(class_name,uid)
        children=self.helpers[class_name].get_alarm_tree_draw(obj)
        if self.verbose: print ("Children from helper: %s" % str(children))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for obj2 in children:
                for obj3 in obj2["children"]:
                    ele=self.getAlarmTreeDraw(obj3["text"]["desc"],obj3["text"]["name"],level-1)
                    if ele:
                        if not "children" in obj3:
                            obj3["children"]=[]
                            pass
                        obj3["children"].append(ele)
                        pass
                    pass
                pass
            pass
        
        if len(children)>0:
            ret["children"]=children
            pass
        return ret

    # @brief Get the alarm tree in draw format
    # @param class_name 
    # @param uid
    # @param level
    def getAttrTreeDraw(self,class_name,uid,level,info,attr):
        attributes={
            "water":"water",
            "power":"power"
        }
        ret={}    
        obj=self.db.get_obj(class_name,uid)
        children=self.helpers[class_name].get_parent_tree_draw(obj, info)
        if self.verbose : print ("=== Children: %s" % str(children))
        if self.verbose : print ("=== Len Children: %i" % len(children))
        toberemoved=[]
        for i in range(0,len(children)):
            if self.verbose : print ("looped %i" % i)
            if attributes[attr].lower() not in children[i]["text"]["name"].lower():
                if self.verbose : print ("Removing child %s" % children[i])
                #children.pop(i)
                toberemoved.append(i)
                pass
            else:
                if self.verbose : print ("Keeping child %s" % children[i])
            pass
        for i in toberemoved[::-1]:
            children.pop(i)
            pass
        if self.verbose: print ("Parents from helper: %s" % str(children))
        if level>0:
            if self.verbose: print ("GetTree Level %i" %(level-1))
            for obj2 in children:
                if attributes[attr].lower() in obj2["text"]["name"].lower():# discarting attributes that are not power
                    for obj3 in obj2["children"]:
                        if self.verbose : print ("2Keeping: ", obj3["text"]["name"])
                        ele=self.getAttrTreeDraw(obj3["text"]["desc"],obj3["text"]["name"],level-1, info, attr)
                        if ele:
                            if not "children" in obj3:
                                obj3["children"]=[]
                                pass
                            obj3["children"].append(ele)
                            pass
                        pass
                    pass
                else:
                    if self.verbose : print ("2NOT Keeping: ", obj3["text"]["name"])
                    pass
                pass
            pass
        if len(children)>0:
            ret["children"]=children
            pass
        return ret

    # @brief Print a tree
    # @param tree
    # @param tab count
    def printTree(self,tree,tab=0):
        t=" "*tab
        print ("%suid: %s class: %s" % (t,tree["uid"],tree["class"]))
        if not "parents" in tree: return
        for attr in tree["parents"]:
            print ("%s%s" % (t,attr))
            for ele in tree["parents"][attr]:
                self.printTree(ele,tab+1)
            pass
        pass
    pass
    


    def getFormula(self,tree):
        if not "parents" in tree: return tree["uid"]
        if len(tree["parents"])==0: return tree["uid"]
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getFormula(ele))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: 
                op = " * "
                types.append("(1-[" + op.join(siblings) + "])")
                pass
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr: 
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            elif "power" in attr:
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            pass
        s="("+" * ".join(types)+")"
        return s



    def getPoSFormula(self, tree):
        if self.verbose: print ("Tree: %s" % str(tree))        
        if not "parents" in tree: return str(self.probs[tree["class"]])
        if len(tree["parents"])==0: return str(self.probs[tree["class"]])
        
        #if not "parents" in tree: return str(tree["class"])
        #if len(tree["parents"])==0: return str(tree["class"])
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getPoSFormula(ele))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: 
                op = " * "
                if self.verbose:  print ("siblings: %s" % str(siblings))
                count=0
                for sib in siblings:
                    siblings[count]="(1-"+str(sib)+")"
                    count+=1
                    pass
                if self.verbose:  print ("siblings: %s" % str(siblings))
                types.append("1-(" + op.join(siblings) + ")")
                pass
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr: 
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            elif "power" in attr:
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            pass
        s="("+" * ".join(types)+")"
        return s
    def getEvaluatedFormula(self, formula):
        PoS_value="Not able to compute formula"#Probability of Survival
        try:
            PoS_value=eval(formula)
            pass
        except:
            pass
        return PoS_value



    def getPoFFormula(self, tree):
        if not "parents" in tree: return str(1.0-self.probs[tree["class"]])
        if len(tree["parents"])==0: return str(1-self.probs[tree["class"]])
        #if not "parents" in tree: return str(tree["class"])
        #if len(tree["parents"])==0: return str(tree["class"])
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getPoFFormula(ele))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: op = " + "
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr: op = " * "
            elif "power" in attr: op = " + "
            types.append("(" + op.join(siblings) + ")")
            pass
        s="("+" * ".join(types)+")"
        return s
    


    def calculateFormula(self,tree):
        if not "parents" in tree: return tree["uid"]
        if len(tree["parents"])==0: return tree["uid"]
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getFormula(ele))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: 
                op = " * "
                if self.verbose: print ("siblings: %s" % str(siblings))
                count=0
                for sib in siblings:
                    siblings[count]="(1-"+str(sib)+")"
                    count+=1
                    pass
                if self.verbose: print ("siblings: %s" % str(siblings))
                types.append("1-(" + op.join(siblings) + ")")
                pass
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr:  
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            elif "power" in attr:
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            pass
        s="("+" * ".join(types)+")"
        return s


    
    def getPCAFormulaIn(self,tree):
        if not "parents" in tree:
            self.changed.append(tree["uid"]) 
            return tree["uid"]
        if len(tree["parents"])==0:
            self.changed.append(tree["uid"])
            return tree["uid"]
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getPCAFormulaIn(ele))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: 
                op = " * "
                if self.verbose: print ("siblings: %s" % str(siblings))
                count=0
                for sib in siblings:
                    siblings[count]="(1-"+str(sib)+")"
                    count+=1
                    pass
                if self.verbose:  print ("siblings: %s" % str(siblings))
                types.append("1-(" + op.join(siblings) + ")")
                pass
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr: 
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            elif "power" in attr:
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            pass
        s="("+" * ".join(types)+")"
        return s


    
    def getPCAValuesCh(self,tree, ch):
        if not "parents" in tree:
            if tree["uid"] is ch:
                return "0"
            else:
                return str(1.0-float(self.probs[tree["class"]]))
        if len(tree["parents"])==0:
            if tree["uid"] is ch:
                return "0"
            else:
                return str(1.0-float(self.probs[tree["class"]]))
        #sibling elements are multiplied
        types=[]
        for attr in tree["parents"]:
            siblings=[]
            if len(tree["parents"][attr])==0: continue
            for ele in tree["parents"][attr]:
                siblings.append(self.getPCAValuesCh(ele, ch))
                pass
            if len(siblings)==0: continue
            op = " o "
            if "receives" in attr: 
                op = " * "
                if self.verbose:  print ("siblings: %s" % str(siblings))
                count=0
                for sib in siblings:
                    siblings[count]="(1-"+str(sib)+")"
                    count+=1
                    pass
                if self.verbose:  print ("siblings: %s" % str(siblings))
                types.append("1-(" + op.join(siblings) + ")")
                pass
            elif "requires" in attr or "digitalInput" in attr or "signalSource" in attr: 
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            elif "power" in attr:
                op = " * "
                types.append("(" + op.join(siblings) + ")")
                pass
            pass
        s="("+" * ".join(types)+")"
        return s


    
    def getPCAFormula(self, tree):
        self.changed=[]
        formulas={}
        self.getPCAFormulaIn(tree)
        for el in self.changed:
            formulas[el]=self.getPCAValuesCh(tree, el)
            pass
        return formulas



    def getPCAValues(self, _formulas):
        formulas_completed={}
        for _formula in _formulas:
            result=self.getEvaluatedFormula(_formulas[_formula])
            formulas_completed[_formula]=[result, _formulas[_formula]]
            pass
        return formulas_completed 
        
    pass
    
    
if __name__ == "__main__":
    import argparse
    parser=argparse.ArgumentParser()
    parser.add_argument('-c','--classname',help="classname",required=True)
    parser.add_argument('-u','--uid',help="uid",required=True)
    parser.add_argument('-l','--level',help="level. default=1",default=1,type=int)
    parser.add_argument('-d','--database',help="database file")
    args=parser.parse_args()
    
    if not args.database: args.database='../../data/sb.data.xml'

    db=config.Configuration("oksconfig:%s"%args.database)
    pt=ProbTree(db)
    tree=pt.getTree(args.classname,args.uid,args.level)
    pt.printTree(tree)
    print (pt.getFormula(tree))
