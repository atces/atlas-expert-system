#!/usr/bin/env python
#############################################
# Vessel Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
# Nov 2018: extend vacuum receiver
#############################################

import helper

class VesselHelper(helper.Helper):
    pass 

if __name__ == '__main__':

    helper.Helper().cmdline("Vessel")
    print ("Have a nice day")
