#!/usr/bin/env python
#############################################
# Simulations helper
# ignacio.asensi@cern.ch
# Carlos.Solans@cern.ch
# Stores Simulations to provide annual intervention outputs
# Jan 2023 - Creation
#############################################

import helper
'''
class   Simulations
att     commands (triggered items)
date    date created
comments
'''
class SimulationsHelper(helper.Helper):
    def new_simulation(self, db, uid):
        db.create_obj(self.classname,uid)
        db.commit()
        pass
    
    def addSimulation(self,obj, commands, comment,date):
        ee=[]
        ee.extend(obj["commands"])
        if isinstance(commands, str):
            ee.append(commands)
            pass
        else:
            for e in commands:
                ee.append(e)
                pass
            pass    
        obj["commands"]=ee
        obj["comment"]=comment
        pass    
    pass
