#!/usr/bin/env python
#############################################
# MetaInfo helper
# Carlos.Solans@cern.ch
# ignacio.asensi@cern.ch
# gustavo.uribe@cern.ch
# February 2020
#############################################

import helper

class MetaInfoHelper(helper.Helper):
    pass


if __name__ == '__main__':

    helper.Helper().cmdline("MetaInfo")
    print ("Have a nice day")
    
