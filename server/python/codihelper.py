class CodiHelper:
    """
    Helper class to write events to a txt file that are 
    inserted to Codi.md
    """
    def __init__(self, events, event_types=["Error", "Intervention", "Unknown"]):
        """_summary_

        Args:
            txt_file (_type_): to which the events are written
            events (_type_)  : event dictionarry read in from json
            event_types (list, optional): Defaults to ["Error", "Intervention", "Unknown"].
        """
        # write the events to the txt file
        self.coditxt = "### DSS Alarms\n"

        for i, event_type in enumerate(event_types):
            events_per_type = self.get_events_per_type(event_type, events)
            self.write_events_per_type(event_type, i+1, events_per_type)

            # inhibits
            self.coditxt += "\n### Inhibits\n- please use the webtool [here](https://atlas-expert-system.web.cern.ch/login/inhibitRequest.php!)\n"

        
    

    def get_events_per_type(self, event_type, events):
        return [e for _, e in events.items() if e["event_type"] == event_type]

    def write_events_per_type(self, event_type, i, events):
        """ follow convention in codimd to write events """
        
        self.coditxt += f"#### {i}. {event_type.capitalize()}s\n"
        for event in events:
            self.coditxt += f"- {event['description']} ({event['short_description']} ({event['date']} {event['time'][:5]}))\n"

    def get_coditxt(self):
        return self.coditxt
    
    def write_to_file(self, txt_file):
        with open(txt_file, "w") as f:
            f.write(self.coditxt)