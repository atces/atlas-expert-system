#!/usr/bin/env python
# ############################################
# NetworkSwitch Helper
# Carlos.Solans@cern.ch
# gustavo.uribe@cern.ch
# September 2019 - December 2019
# ############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class NetworkSwitchHelper(NetHelper,helper.Helper):
    pass 
        
if __name__ == '__main__':
    helper.Helper().cmdline("NetworkSwitch")
    print ("Have a nice day")
