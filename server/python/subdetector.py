#!/usr/bin/env python
#############################################
# SubDetector
# Carlos.Solans@cern.ch
# January 2018
# August 2018: fix cmdline
#############################################

import helper

class SubDetectorHelper(helper.Helper):
    pass 

if __name__ == '__main__':

    helper.Helper().cmdline("SubDetector")
    print ("Have a nice day")
    
