#!/usr/bin/env python
# ############################################
# NetworkRouter Helper
# Carlos.Solans@cern.ch
# gustavo.uribe@cern.ch
# September 2019 - December 2019
# ############################################

from networkhelper import NetworkHelper as NetHelper
import helper

class NetworkRouterHelper(NetHelper,helper.Helper):
	pass
 
if __name__ == '__main__':
    helper.Helper().cmdline("NetworkRouter")
    print ("Have a nice day")
