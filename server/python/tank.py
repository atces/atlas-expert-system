#!/usr/bin/env python
#############################################
# Tank helper
# Ignacio.Asensi@cern.ch
# May 2020
#############################################

import helper

class TankHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("Tank")
    print ("Have a nice day")
