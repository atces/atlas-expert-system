#!/usr/bin/env python
#############################################
# DSU
# Carlos.Solans@cern.ch
# May 2017
# August 2018: fix cmdline
# January 2019: fix digital input feed=True
#############################################

import helper

class DSUHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("DSU")
    print ("Have a nice day")
    
