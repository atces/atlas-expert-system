#!/usr/bin/env python
# ############################################
# Expert System Remote Database Service
#
# Carlos.Solans@cern.ch
# Saba.Kvesitadze@cern.ch
# George.Salukvadze@cern.ch
# Ignacio.Asensi@cern.ch
# Florian.Haslbeck@cern.ch
# gustavo.uribe@cern.ch
#
# https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/javadoc/tdaq-06-00-00/config/ConfigObject.html
# https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/tdaq-06-00-00/html/d4/da7/classConfiguration.html
#
# Load the database directly from the web server
# Store a copy in /tmp
# ############################################
import os
import sys
import socket
import config
import json
import time
import subprocess
import re
import pwd
import loadhelpers
import ProbTree
import traceback
import datetime
import treehelper
from graphhelper import GraphHelper 
from multiprocessing import Process
from copy import deepcopy

# slide creation
from pptxhelper import create_slides
from codihelper import CodiHelper
import base64

output=None
tmpdir="/tmp/%s" % pwd.getpwuid(os.geteuid())[0]


#print before redirection of the output 
#this will not be written to the output file
#consider removing form here
print("----------------------------------------")
print("----------------------------------------")
print("----------------SERVER------------------")
print("------------------v2--------------------")
print("----------------------------------------")
print("----------------------------------------")


classes=[]
searchableClasses=[]
sysclasses=[]
helpers=[]
                    
def log(ss):
    print("%s\t%s"%(timestamp(),ss))
    pass
    
def timestamp():
    return time.strftime("%Y-%m-%d %H:%M:%S")

def flush():
    if output==None: return
    sys.stdout = open(output,'a+')

def serialize(class_name, obj):
    ret={}
    ret=helpers[obj.class_name()].serialize(obj)
    return ret
    
def get_schema(class_name):
    ret={}
    ret=helpers[class_name].get_schema()
    return ret


def loadDatabaseConfiguration(localpath):
    host="-".join(socket.gethostname().split("-")[:-1])
    dbpath="%s.web.cern.ch/data" % (host)
    dbfiles=["sb.data.xml","sb.schema.xml", "sb.sim_data.xml", "sb.sim_schema.xml","meta.data.xml","meta.schema.xml"]
    mdbfile="sb.data.xml"
    cmds=[]
    cmds.append("mkdir -p %s" % localpath)
    cmds.append("rm %s/.oks*" % localpath)
    for dbfile in dbfiles:
        print("Load %s" % dbfile)
        found=False
        for path in os.environ["TDAQ_DB_PATH"].split(":"):
            print("  test:: %s/%s" % (path,dbfile))
            if os.path.exists("%s/%s"%(path,dbfile)):
                cmds.append("cp %s/%s %s/." % (path,dbfile,localpath))
                print("From filesystem: %s/%s" % (path,dbfile))
                found=True
                break
            if not found:
                cmds.append("curl -s https://%s/%s -o %s/%s" % (dbpath,dbfile,localpath,dbfile))
                print("From remote: %s/%s" % (dbpath,dbfile))
                break
            pass
        pass
    cmds.append("cp %s/%s %s/%s" % (localpath,mdbfile,localpath,"sb.data_mpc.xml"))#Create file for MPC calculation
    cmds.append("cp %s/%s %s/%s" % (localpath,mdbfile,localpath,"sb.data_mpc2.xml"))#Create file for MPC calculation two points of failure
    for cmd in cmds:
        log(cmd)
        pid=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
        out, err = pid.communicate()
        pass
    log("Create configuration pointer")
    db=config.Configuration("oksconfig:%s/%s"%(localpath,mdbfile))
    return db


def startSocket():
    log("[def]startSocket")
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    global classes
    global searchableClasses
    global sysclasses
    global helpers

    try:
        sock.bind((socket.gethostname(),9998))
    except:
        log("Address already in use")
        return 0
    sock.listen(1)
    tid = 0

    simdbfile="sb.sim_data.xml"
    
    permanent_sim_db="/tmp/root/db/simulationdb"
    conn_pool={}
    sess_pool={}
    graph_helper_pool={}
    db=loadDatabaseConfiguration("%s/db/%s" % (tmpdir,0))
    gLH = loadhelpers.LoadHelpers(db)
    classes=gLH.getClasses()
    searchableClasses=gLH.getSearchableClasses()
    sysclasses=gLH.getSysClasses()
    helpers=gLH.getHelpers()
    helpers["probtree"]=ProbTree.ProbTree(db)
    helpers["probtree"].setVerbose(False)
    print(classes)
    print("Sysclasses:", sysclasses)
    graph_helper=GraphHelper(conn_pool,sess_pool,helpers,classes)
    graph_helper.create_graph(db)
    graph_helper.generate_feeding_graph()
    th = treehelper.TreeHelper(conn_pool, sess_pool, helpers, classes)
    
    cont = True
    while cont:
        log("Waiting for a client . . .")
        flush()

        client, address = sock.accept()
        log( "Socket accepting input from: " + client.getpeername()[0])

        bytes_to_read = 1000 # max number of bytes to read at once
        while True:
            try:
                sreq = ""
                while True:
                    s=client.recv(bytes_to_read)
                    # if 0 bytes are received, the client has closed the connection
                    if len(s)==0: break

                    # decode the bytes to a string
                    s=s.decode('utf-8')
                    sreq+=s

                    # a message is complete if 
                    # it ends with a closing bracket
                    # and 
                    # there is no more data to come
                    if s[-1]=="}" and len(s) != bytes_to_read: 
                        break
            except:
                log("Client receive broke")
                print(sys.exc_info())
                continue

            if len(sreq)==0: break
            if len(sreq)>1000:
                log("Request: " + sreq[:1000])
            else:
                log("Request: " + sreq)

            #request dictionary
            dreq = {}
            try:
                dreq = json.loads(sreq)
            except:
                log("Could not decode the request.")

                print(sys.exc_info())
                print("Request:")
                print(sreq)
                continue

            #reply dictionary
            drep = {}
            drep["Trace_exception"]=""
            #time0 to calculate the processing time
            time0 = datetime.datetime.now()
            
            if "Ping" in dreq["cmd"]:
                drep["Pong"]="Server is alive"
                drep["Reply"]="Pong"
                pass
            elif "GetNewToken" in dreq["cmd"]:
                tid=0
                for t in conn_pool:
                    if t>tid: tid=t
                    pass
                tid+=1
                localpath="%s/db/%s" % (tmpdir,tid)
                try:
                    db=loadDatabaseConfiguration(localpath)
                    conn_pool[tid] = db
                    if "dev" in dreq and dreq["dev"]:  
                        graph_helper.create_graph(conn_pool[tid])
                        graph_helper.generate_feeding_graph()
                    drep["Reply"]="OK"
                    drep["TokenId"]=tid
                    log("Token ID : %i" % tid)
                except RuntimeError as emsg:
                    log("Runtime error:")
                    log(emsg)
                    drep["Reply"]="Error"
                    drep["error"]="Error creating configuration pointer"
                    pass
                log("Load simulations db")
                try:
                    sim_db=config.Configuration("oksconfig:%s/%s"%(localpath, simdbfile))
                    drep["Reply"]="OK"
                except RuntimeError as emsg:
                    log("Runtime error loading simulations database:")
                    log(emsg)
                    drep["Reply"]="Error"
                    drep["error"]="Error creating configuration pointer"
                    pass
                log("Getting session object")
                sessionId="Session_"+str(tid)
                objs = db.get_objs("Session")
                sess_pool[tid]=None
                for obj in objs:
                    if obj.UID()==sessionId: 
                        sess_pool[tid]=obj
                        log("Found session in db: %s" % sess_pool[tid])
                        pass
                    pass
                if not sess_pool[tid]:
                    log("Creating new session: %s" % sessionId)
                    helpers["Session"].new_session(db,sessionId)
                    sess_pool[tid] = db.get_obj("Session",sessionId)
                    pass
                pass
            elif "Shutdown" in dreq["cmd"]:
                log( "Server exit requested")
                drep["Reply"]="Server exit requested"
                cont=False
                pass
            else:
                if not "TokenId" in dreq:
                    drep["error"]="TokenId not provided"
                    log("TokenId not provided")
                    dreq["cmd"]=""
                    pass
                try:
                    tid=int(dreq["TokenId"])
                except:
                    drep["error"]="Cannot decode TokenId"
                    log("Cannot decode TokenId")
                    dreq["cmd"]=""
                    pass
                #Session tokenid
                log("TokenId: %i " % tid)
                class_name="SystemBase"

                if "type" in dreq: class_name=str(dreq["type"])

                if tid not in conn_pool:
                    log("TokenId not found in server")
                    drep["Reply"]="Error"
                    drep["error"]="TokenId not found in server"
                    pass

                elif "CheckToken" in dreq["cmd"]:
                    if tid in conn_pool:
                        drep["Reply"]="Valid"
                        pass
                    else:
                        drep["Reply"]="Invalid"
                        pass
                    pass
                elif "GetCurrentStateFull" in dreq["cmd"]:
                    objs = conn_pool[tid].get_objs("SystemBase")
                    for obj in objs:
                        drep[obj.UID()] = {"State": obj.get_string("state"),
                                           "Switch":obj.get_string("switch")}
                        drep[obj.UID()]["powers"]={}
                        for child in obj["powers"]:
                            drep[obj.UID()]["powers"][child.UID()]=child.UID()
                            pass
                        pass
                    pass
                elif "GetObjectChildrenTree" in dreq["cmd"]:
                    try:
                        drep["Reply"]="OK"
                        uid=str(dreq["uid"])
                        level=3
                        parameters=None
                        if "level" in dreq: level=int(dreq["level"])
                        if "paremeters" in dreq: parameters=json.loads(str(dreq["parameters"]))
                        obj=graph_helper.get_object(conn_pool[tid],uid)
                        class_name=obj.class_name()
                        pt=helpers["probtree"]
                        pt.setDb(conn_pool[tid])
                        pt.setProbs(parameters) #why is this here? parameters is always null
                        tree={}
                        tree=pt.getChildrenTreeDraw(class_name,uid,level)
                        tree["text"]={"name":uid,"desc":class_name}
                        tree["HTMLclass"]="tree-first"
                        drep["Result"]=tree
                        pass
                    except Exception as inst:
                        log("Error in GetObjectChildrenTree")
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetAttrObjectParentsTree" in dreq["cmd"]:
                    try:
                        drep["Reply"]="OK"
                        uid=str(dreq["uid"])
                        level=3
                        parameters=None
                        info=False
                        if "level" in dreq: level=int(dreq["level"])
                        if "attr" in dreq: attr=dreq["attr"]
                        if "line" in dreq: line=dreq["line"]
                        if "info" in dreq and "yes" in dreq["info"]: info=True
                        if "paremeters" in dreq: parameters=json.loads(str(dreq["parameters"]))
                        obj=graph_helper.get_object(conn_pool[tid],uid)
                        class_name=obj.class_name()
                        pt=helpers["probtree"]
                        pt.setDb(conn_pool[tid])
                        pt.setProbs(parameters) #why is this here? parameters is always null
                        tree={}
                        if line=="parents":
                            if attr=="all": tree=pt.getTreeDraw(class_name,uid,level,info)
                            else: tree=pt.getAttrTreeDraw(class_name,uid,level,info,attr)
                            tree["text"]={"name":uid,"desc":class_name}
                            if info: tree["text"]["title"]=obj.get_string("description")
                            tree["HTMLclass"]="tree-first"
                            drep["Result"]=tree
                            pass
                        elif line=="children":
                            tree=pt.getChildrenTreeDraw(class_name,uid,level,info)
                            tree["text"]={"name":uid,"desc":class_name}
                            if info: tree["text"]["title"]=obj.get_string("description")
                            tree["HTMLclass"]="tree-first"
                            drep["Result"]=tree
                        else:
                            print ("ERROR in GetAttrObjectParentsTree: Parameter -line- not defined")
                            pass
                        pass
                    except Exception as inst:
                        log("Error in GetAttrObjectParentsTree")
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetCurrentState" in dreq["cmd"]:
                    elementsjson=json.loads(str(dreq["json"]))
                    for each in classes:
                        objs = conn_pool[tid].get_objs(each)
                        for obj in objs:
                            if obj.UID() in elementsjson:
                                if "description" in helpers[each].get_schema() and "switch" in helpers[each].get_schema() and "state" in helpers[each].get_schema() and "subsystem" in helpers[each].get_schema():
                                    if obj.class_name() in ["DigitalInput", "Action"] :
                                        desc ="%s@%s" % (obj.UID(),obj.class_name())
                                        desc+=" crate: %s," % obj.get_string("crate")
                                        desc+=" slot: %i," % obj.get_u32("slot")
                                        desc+=" chan: %i" % obj.get_u32("channel")
                                        log(desc)
                                        drep[obj.UID()] = {"State": obj.get_string("state"),
                                                       "Switch":obj.get_string("switch"),
                                                       "Description":desc,
                                                       "subsystem":obj.get_string("subsystem"),
                                                       "class_":obj.class_name()}
                                        pass
                                    elif obj.class_name() in ["Rack", "Group"] :
                                        drep[obj.UID()] = {"State": obj.get_string("state"),
                                                       "Switch":obj.get_string("switch"),
                                                       "Description":obj.get_string("description"),
                                                       "subsystem":obj.get_string("subsystem"),
                                                       "class_":obj.class_name()}
                                        pass
                                    else:
                                        drep[obj.UID()] = {"State": obj.get_string("state"),
                                                       "Switch":obj.get_string("switch"),
                                                       "Description":obj.get_string("description"),
                                                       "subsystem":obj.get_string("subsystem")}
                                        pass
                                    pass
                                elif "switch" in helpers[each].get_schema() and "state" in helpers[each].get_schema():
                                    drep[obj.UID()] = {"State": obj.get_string("state"),
                                                       "Switch":obj.get_string("switch")}
                                if "subsystem" in helpers[each].get_schema():
                                    if obj.UID() in drep: drep[obj.UID()]["subsystem"]=obj.get_string("subsystem")
                                    pass
                                pass
                            pass
                        pass
                    drep["Reply"]="OK"
                    pass
                elif "SaveCurrentState" in dreq["cmd"]:
                    try:
                        db.commit();
                        fname=str(dreq["fname"])
                        statepath="/data"
                        localpath="/tmp/%s/db/%s" % (os.getlogin(),tid)
                        cmds=[]
                        folname = re.sub(r'\W+', '', fname)
                        log("Folder name %s => %s" % (fname,folname))
                        cmds.append("mkdir %s/%s" % (statepath,folname))
                        cmds.append("cp %s/sb.data.xml %s/%s/." % (localpath,statepath,folname))
                        cmds.append("cp %s/sb.schema.xml %s/%s/." % (localpath,statepath,folname))
                        for cmd in cmds:
                            log(cmd)
                            pid=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
                            out, err = pid.communicate()
                            print(out,err)
                            pass
                        fw = open("/data/%s/name.txt" % folname, "w+")
                        fw.write(fname)
                        fw.close()
                        log("Commited")
                        drep["Reply"]="Current state/configuration saved with folder name: " + folname
                    except:
                        log("Error in SaveCurrentState")
                        drep["Reply"]="Error"
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        pass
                    pass

                elif "GetStateList" in dreq["cmd"]:
                    try:
                        statepath="/data"
                        states={}
                        for x in os.listdir(statepath):
                            fname="%s/%s/name.txt" % (statepath,x)
                            if not os.path.exists(fname): continue
                            log( "Test %s" % fname)
                            states[x]=open(fname).readlines()[0].strip()
                            pass
                        drep["Reply"]="OK"
                        drep["States"]=states
                    except:
                        log("Error in GetStateList")
                        drep["Reply"]="Error"
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                    pass

                elif "GetSavedState" in dreq["cmd"]:
                    try:
                        log( "Getting saved state")
                        statepath="/data/"
                        statename=str(dreq["statename"])
                        sname = re.sub(r'\W+', '', statename)
                        statepath+=sname
                        log( sname)
                        localpath="/tmp/%s/db/%s" % (os.getlogin(),tid)
                        cmds=[]
                        cmds.append("rm %s/sb.data.xml" % (localpath))
                        cmds.append("rm %s/sb.schema.xml" % (localpath))
                        cmds.append("\cp -rf %s/sb.data.xml %s/." % (statepath,localpath))
                        cmds.append("\cp -rf %s/sb.schema.xml %s/." % (statepath,localpath))
                        for cmd in cmds:
                            log(cmd)
                            pid=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
                            out, err = pid.communicate()
                            print(out,err)
                        pass
                        db=config.Configuration("oksconfig:%s/%s"%(localpath,dbfile))
                        conn_pool[tid] = db
                        log( "Copied")
                        drep["Reply"]="OK"
                        drep["Tokenid"]=tid
                    except:
                        log("Error in GetSavedState")
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        drep["Reply"]="Error"
                    pass
                elif "FindObject" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        objs = conn_pool[tid].get_objs(class_name)
                        for obj in objs:
                            if dreq["uid"] in obj.UID():
                                obj_list[obj.UID()]=obj.UID();
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        log("Error in FindObject")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        pass
                    pass
                elif "GetAllObjectsOfAClass" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        class_name=str(dreq["class_name"])
                        objs = conn_pool[tid].get_objs(class_name)
                        for obj in objs:
                            obj_list[obj.UID()]=obj.UID()
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        log("GetAllObjectsOfAClass")
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetSummaryGroupsAffected" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        class_name="Group"
                        objs = conn_pool[tid].get_objs(class_name)
                        for obj in objs:
                            if obj.get_string("SummaryGroup")=="yes" and obj.get_string("state")=="off":
                                obj_list[obj.UID()]=obj.UID()
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("GetSummaryGroupsAffected")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetAllObjects" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        objs = conn_pool[tid].get_objs(class_name)
                        for obj in objs:
                            obj_list[obj.UID()]=obj.UID();
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetAllObjects")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetMPCCalculationAndStatus" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        localpath="%s/db/%s" % (tmpdir,tid)
                        db_mpc=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc.xml"))
                        db_mpc2=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc2.xml"))
                        objs=db_mpc.get_objs("MPCCalculation")
                        for obj in objs:
                            obj_list[obj.UID()]=helpers["MPCCalculation"].get_status_multiple_points_of_failure([db_mpc,db_mpc2],obj.UID());
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetAllObjects")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "AdvancedSearchObjectFullTable" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        attribute = str(dreq["attribute"])
                        stype = str(dreq["type"])
                        objs = conn_pool[tid].get_objs(stype)
                        obj_list = {obj.UID(): helpers[obj.class_name()].serialize(obj) for obj in objs}
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                        print(len(obj_list))
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in AdvancedSearchObjectFullTable")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "AdvancedSearchObject" in dreq["cmd"]:
                    try:
                        obj_list = {}
                        attribute = str(dreq["attribute"])
                        stype = str(dreq["type"])
                        for each in classes:
                            if stype == "All" or each == stype:
                                at=conn_pool[tid].attributes(each,True)
                                at.update(conn_pool[tid].relations(each,True))
                                objs = conn_pool[tid].get_objs(each)
                                if attribute in at:
                                    if at[attribute]['multivalue'] is False:
                                        for obj in objs:
                                            if dreq["string"].lower() in str(obj.__getitem__(attribute)).lower():
                                                obj_list[obj.UID()]=each
                                                pass
                                            pass
                                        pass
                                    else:
                                        for obj in objs:
                                            for subobj in obj.__getitem__(attribute):
                                                if dreq["string"].lower() in subobj.UID().lower():
                                                    obj_list[obj.UID()]=each
                                                    pass
                                                pass
                                            pass
                                        pass
                                    pass
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in AdvancedSearchObject")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "SearchObject" in dreq["cmd"]:
                    try:
                        criteria=dreq["uid"]
                        #print datetime.datetime.utcnow()
                        obj_list = th.searchObjectNoClass(tid,criteria,["description","otherIds"])
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in SearchObject")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetJSONObjectsClassAndLocation" in dreq["cmd"]:
                    elementsjson=json.loads(str(dreq["json"]))
                    obj_list={}
                    try:
                        for element in elementsjson:
                            o=graph_helper.get_object(conn_pool[tid], element)
                            obj_list[o.UID()]=[o.class_name(), o["location"]]
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetJSONObjectsClassAndLocation")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetLogJSON" in dreq["cmd"]:
                    elementsjson=json.loads(str(dreq["json"]))
                    obj_list={}
                    try:
                        for element in elementsjson:
                            obj=graph_helper.get_object(conn_pool[tid], element)
                            obj_list[obj.UID()]=helpers[obj.class_name()].get_log(obj)
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetLogJSON")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetLog" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    try:
                        obj=graph_helper.get_object(conn_pool[tid], uid)
                        drep["Reply"]="OK"
                        drep["Result"]=helpers[obj.class_name()].get_log(obj)
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetLogJSON")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetFullTableSearch" in dreq["cmd"]:
                    try:
                        print(datetime.datetime.utcnow())
                        obj_list=th.getFullTableFromFile(tid)
                        print(datetime.datetime.utcnow())
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetFullTableSearch")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetElementsDescription" in dreq["cmd"]:
                    try:
                        elementsjson=json.loads(str(dreq["json"]))
                        obj_list = {}
                        objs = conn_pool[tid].get_objs("SystemBase")
                        for obj in objs:
                            if obj.UID() in elementsjson:
                                obj_list[obj.UID()]=obj.__getitem__("description")
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetElementsDescription")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetElementsSubsystems" in dreq["cmd"]:
                    try:
                        elementsjson=json.loads(str(dreq["json"]))
                        obj_list = {}
                        objs = conn_pool[tid].get_objs("SystemBase")
                        for obj in objs:
                            if obj.UID() in elementsjson:
                                obj_list[obj.UID()]=obj.__getitem__("subsystem")
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetElementsSubsystems")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetNoClassObjectDescription" in dreq["cmd"]:
                    try:
                        uid=str(dreq["uid"])
                        obj = graph_helper.get_object(conn_pool[tid],uid)
                        result=obj["description"]
                        drep["Reply"]="OK"
                        drep["Result"]= result
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetNoClassObjectDescription")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetNoClassObject" in dreq["cmd"]:
                    #try:
                    uid=str(dreq["uid"])
                    obj = graph_helper.get_object(conn_pool[tid],uid)
                    if not obj:
                        drep["Trace_exception"]+="Error when %s, object %s not found.\n" % (dreq["cmd"],uid)
                        drep["Reply"]="Error"
                    else: 
                        drep["Reply"]="OK"
                        drep["Class"]=obj.class_name()
                        drep["UID"]= uid
                        drep.update(serialize(obj.class_name(),obj))
                    #except Exception as inst:
                    #    drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                    #    log("Error in GetNoClassObject")
                    #    print(inst)
                    #    print(sys.exc_info()[0])
                    #    drep["Reply"]="Error"
                    #    pass
                    pass
                elif "isSystem" in dreq["cmd"]:
                    try:
                        uid=str(dreq["uid"])
                        obj = graph_helper.get_object(conn_pool[tid],uid)
                        class_name=obj.class_name()
                        attributes=helpers[class_name].attrs
                        drep["Reply"]="OK"
                        drep.update(serialize(obj.class_name(),obj))
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in isSystem")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetComputationAll" in dreq["cmd"]:
                    print("GetComputationAll")
                    try:
                        #get commandshistory
                        sessionId="Session_"+str(tid)
                        session_obj=helpers["Session"].get_computation_list(sess_pool[tid])
                        drep["Computation"]=session_obj
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetComputationAll")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetComputation" in dreq["cmd"]:
                    print("GetComputation")
                    try:
                        #get object
                        uid=str(dreq["uid"])
                        obj=graph_helper.get_object(conn_pool[tid], uid)
                        drep["Object"]=serialize(obj.class_name(),obj)
                        #get occurrences
                        occurrences={}
                        occurrences=helpers["Session"].get_last_occurrence(sess_pool[tid], uid)
                        drep["Occurrences"]=occurrences

                        #get commandshistory
                        sessionId="Session_"+str(tid)
                        session_obj=helpers["Session"].get_commands_list(sess_pool[tid])
                        drep["Commands"]=session_obj
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetComputation")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetCommandsHistory" in dreq["cmd"]:
                    try:
                        sessionId="Session_"+str(tid)
                        obj=conn_pool[tid].get_obj("Session",sessionId)
                        drep["Reply"]="OK"
                        drep.update(serialize("Session",obj))
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetCommandsHistory")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetSimulations" in dreq["cmd"]:
                    objs = conn_pool[tid].get_objs("Simulations")
                    print("GetSimulations:")
                    print(objs)
                    obj_list={}
                    for obj in objs:
                        obj_list[obj.UID()]={}
                        obj_list[obj.UID()]["name"]=obj["name"]
                        obj_list[obj.UID()]["comment"]=obj["comment"]
                        obj_list[obj.UID()]["date"]=obj["date"]
                        pass
                    drep["Result"]=obj_list
                    drep["Reply"]="OK"
                    pass
                elif "RunSimulationFromDB" in dreq["cmd"]:
                    print("RunSimulationFromDB")
                    uid=str(dreq["simulation"])
                    ctime= int(dreq["ctime"]) if "ctime" in dreq else 0
                    obj = graph_helper.get_object(conn_pool[tid],uid)
                    print(obj)
                    for o in obj["commands"]:
                        drep=graph_helper.change_switch(tid,o, None, obj["duration"])
                        pass
                    drep["Reply"]="OK"
                    pass
                elif "GetSimulationJSON" in dreq["cmd"]:
                    #get  commands history
                    sessionId="Session_"+str(tid)
                    obj_session=conn_pool[tid].get_obj("Session",sessionId)
                    drep["Reply"]="OK"
                    obj_session_ser=serialize("Session",obj_session)
                    commands=obj_session_ser["commands"]
                    #get affected systems and alarms
                    affected_=th.GetAffectedSystemsWithLocation(tid)
                    drep["Result"]={"commands":commands, "affected":affected_}
                    drep["Reply"]="OK"
                    pass
                elif "SaveSimulation" in dreq["cmd"]:
                    #get  commands history
                    sessionId="Session_"+str(tid)
                    print("SaveSimulation...")
                    simulationname=str(dreq["simulationname"])
                    comment=str(dreq["comment"])
                    print("simulationname:"+simulationname)
                    print("comment:"+comment)
                    helpers["Simulations"].new_simulation(conn_pool[tid],simulationname)
                    objsim=conn_pool[tid].get_obj("Simulations",simulationname)
                    drep["Reply"]="OK"
                    obj_session=conn_pool[tid].get_obj("Session",sessionId)
                    obj_session_ser=serialize("Session",obj_session)
                    commands=obj_session_ser["commands"]
                    print("commands:")
                    print(commands)
                    helpers["Simulations"].addSimulation(objsim, commands, comment, str(time1))
                    conn_pool[tid].commit()
                    drep["Reply"]="OK"
                    pass
                elif "CheckSimulationJSON" in dreq["cmd"]:
                    received_simulation_s=str(dreq["simulation"])[1:]
                    print(received_simulation_s)
                    received_simulation_o=json.loads(received_simulation_s)
                    print(received_simulation_o["simulation"])
                    #reproduce simulation commands
                    for sim_action in received_simulation_o:
                        if sim_action=="":continue
                        print("sim_action", sim_action[0])
                        if len(sim_action)>0:
                            graph_helper.change_switch(tid,str(sim_action[0].replace("[","").split(",")[0]),{})
                            pass
                        pass
                    print("Simulation done")
                    drep["Reply"]="OK"
                    pass
                elif "GetActiveActions" in dreq["cmd"]:
                    print("GetActiveActions")
                    try:
                        obj_list={}
                        objs = conn_pool[tid].get_objs("Action")
                        for obj in objs:
                            if obj["state"]=="off":
                                print("Active action! ", obj.UID())
                                obj_list[obj.UID()]="state"
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetActiveActions")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetActiveInputs" in dreq["cmd"]:
                    print("GetActiveInputs")
                    try:
                        obj_list={}
                        objs = conn_pool[tid].get_objs("DigitalInput")
                        for obj in objs:
                            if obj["state"]=="off":
                                print("Active input! ", obj.UID())
                                obj_list[obj.UID()]="state"
                                pass
                            pass
                        drep["Reply"]="OK"
                        drep["Result"]=obj_list
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetActiveInputs")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetMPCCalculationDetails" in dreq["cmd"]:
                    #try:
                    uid=str(dreq["uid"])
                    localpath="%s/db/%s" % (tmpdir,tid)
                    db_mpc=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc.xml"))
                    db_mpc2=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc2.xml"))#For two ponits of failure
                    if db_mpc.test_object("MPCCalculation",uid):
                        obj=db_mpc.get_obj("MPCCalculation",uid)
                        obj_dict=serialize(obj.class_name(),obj)
                        status=helpers["MPCCalculation"].get_status_multiple_points_of_failure([db_mpc,db_mpc2],uid)
                        obj_dict["status"]=status
                        obj2=db_mpc2.get_obj("MPCCalculation",uid)
                        obj_dict["result2"]=obj2["result"]
                        obj_dict["progress2"]=obj2["progress"]
                        drep["Reply"]="OK"
                        #drep["Result"]=obj
                        drep["Class"]=obj.class_name()
                        drep.update(obj_dict)
                    #except Exception as inst:
                    #    drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                    #    log("Error in GetMPCCalculationDetails")
                    #    print(inst)
                    #    print(sys.exc_info()[0])
                    #    drep["Reply"]="Error"
                    #    pass
                    pass
                elif "isAValidUID" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    obj = graph_helper.get_object(conn_pool[tid],uid)
                    drep["uid"]=uid
                    if obj: drep["Reply"]="True"
                    else: drep["Reply"]="False"
                elif "ChangeState" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    class_name="SystemBase"
                    drep["Reply"]="OK"
                    #Get the object
                    obj=conn_pool[tid].get_obj(class_name,uid)
                    if obj.get_string("state")=="on":
                        obj.set_enum("state","off")
                        pass
                    else:
                        obj.set_enum("state","on")
                        pass
                    drep[uid] = {"State": obj.get_string("state"),
                                 "Switch":obj.get_string("switch")}
                    pass
                elif "ChangeAttribute" in dreq["cmd"]:
                    allowedChanges=["repowered","reSuppliedAir", "inhibit", "switch"]
                    uid=str(dreq["uid"])
                    attr=str(dreq["attr"])
                    value=str(dreq["value"])
                    if attr in allowedChanges:
                        #Get the object
                        obj=graph_helper.get_object(conn_pool[tid],uid)
                        obj.set_enum(attr,value)
                        ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                        drep["Reply"]="OK"
                        pass
                    else:
                        drep["Reply"]="Error: Attribute [%s] not  in allowedChanges list" % str(dreq["attr"])
                        pass
                    pass
                elif "ChangeBoolAttribute" in dreq["cmd"]:
                    try:
                        allowedChanges=["inhibit"]
                        uid=str(dreq["uid"])
                        attr=str(dreq["attr"])
                        value=str(dreq["value"])
                        boolvalue= True if value=="true" else False
                        if attr in allowedChanges:
                            #Get the object
                            obj=graph_helper.get_object(conn_pool[tid],uid)
                            obj.set_bool(attr,boolvalue)
                            ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                            drep["Reply"]="OK"
                            pass
                        else:
                            drep["Reply"]="Error: Attribute [%s] not in allowedChanges list" % str(dreq["attr"])
                            pass
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in ChangeBoolAttribute")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "ChangeBoolJSONAttribute" in dreq["cmd"]:
                    try:
                        elementsjson=json.loads(str(dreq["json"]))
                        allowedChanges=["repowered","reSuppliedAir", "inhibit", "switch"]
                        attr=str(dreq["attr"])
                        rawvalue=str(dreq["value"])
                        if attr in allowedChanges:
                            if rawvalue =="true" or rawvalue == "false":
                                value= True if rawvalue=="true" else False
                                #Get the object
                                for element in elementsjson:
                                    obj=graph_helper.get_object(conn_pool[tid],element)
                                    obj.set_bool(attr,value)
                                    ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                                    pass
                                pass
                            elif rawvalue =="off" or rawvalue == "on":
                                value=rawvalue
                                #Get the object
                                for element in elementsjson:
                                    obj=graph_helper.get_object(conn_pool[tid],element)
                                    obj.set_enum(attr,value)
                                    ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                                    pass
                                pass
                            else:
                                print("ERROR: value [%s] not reconized for attribute [%s]" % (rawvalue, attr))
                                pass
                            drep["Reply"]="OK"
                            pass
                        else:
                            drep["Reply"]="Error: Attribute [%s] not in allowedChanges list" % str(dreq["attr"])
                            pass
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in ChangeBoolJSONAttribute")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetAlarmsAffectingPage" in dreq["cmd"]:
                    localdebug=True
                    alarmslist={}
                    elementsjson=json.loads(str(dreq["json"]))
                    for element in elementsjson:
                        uid=str(element)
                        if localdebug==True: print("Searching alarms of %s" % uid)
                        if uid.startswith("AL_")== False and uid.startswith("O_")== False:
                            try:
                                obj=graph_helper.get_object(conn_pool[tid],uid)
                                if obj:
                                  class_name=obj.class_name()
                                  schema=helpers[class_name].get_schema()
                                  if "interlockedBy" in schema: 
                                    if len(obj["interlockedBy"]) != 0:
                                        interlockers=obj.get_objs("interlockedBy")
                                        for action in interlockers:
                                            try:
                                                if conn_pool[tid].test_object("Action",action.UID()):
                                                	objAction=conn_pool[tid].get_obj("Action",action.UID())
                                                	delayedActions=objAction.get_objs("delayedactions")
                                                	for delayedAction in delayedActions:
                                               			objDelayedAction=conn_pool[tid].get_obj("DelayedAction",delayedAction.UID())
                                                		alarms=objDelayedAction.get_objs("startedByAlarms")
                                                		for alarm in alarms:
                                                			if not alarm.UID() in alarmslist:
                                                				alarmslist[alarm.UID()]=["interlockedBy"]
                                               					pass
                                                			alarmslist[alarm.UID()].append(uid)
                                                			pass
                                                		pass
                                                	pass
                                            except:
                                                drep["Trace_exception"]+="Error when GetAlarmsAffectingPage -> interlockedBy\n"
                                                print(sys.exc_info())
                                                pass
                                            pass
                                        pass
                                    pass
                                if "digitalInput" in schema:
                                    if localdebug==True: print("-->Searching digitalInputs of %s" % element) 
                                    if len(obj["digitalInput"]) != 0:
                                        digitalinputs=obj.get_objs("digitalInput")
                                        for di in digitalinputs:
                                            try:
                                                di=conn_pool[tid].get_obj("DigitalInput",di.UID())
                                                alarms=di.get_objs("triggers")
                                                for a in alarms:
                                                    if not a.UID() in alarmslist:
                                                        alarmslist[a.UID()]=["digitalInput"]
                                                        pass
                                                    alarmslist[a.UID()].append(uid)
                                                    pass
                                                pass
                                            except:
                                                drep["Trace_exception"]+="Error when GetAlarmsAffectingPage -> digitalInput\n"
                                                print(sys.exc_info())
                                                pass
                                            pass
                                        pass
                                    pass
                                pass
                            except:
                                drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                                print("")
                                print("!! Error getting alarm affecting ", uid)
                                print("")
                                print(sys.exc_info())
                                pass
                            pass
                        pass
                    drep["Result"]=alarmslist

                    drep["Error"]=""
                    pass
                elif "ChangeSwitch" in dreq["cmd"]:
                    try:
                        uid=str(dreq["uid"])
                        ctime=0
                        if "ctime" in dreq: ctime=int(dreq["ctime"])
                        if "json" in dreq:
                            elementsjson=json.loads(str(dreq["json"]))
                            drep=graph_helper.change_switch(tid,uid, elementsjson, ctime)
                            pass
                        else:
                            drep=graph_helper.change_switch(tid,uid, None, ctime)
                            pass
                        pass
                    except Exception as e:
                        print(e)
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        pass
                    pass
                elif "GetTriggeredAlarms" in dreq["cmd"]:
                    obj_list = {}
                    class_name="Alarm"
                    objs = conn_pool[tid].get_objs(class_name)
                    for obj in objs:
                        if obj["state"]=="off":
                            obj_list[obj.UID()]=obj.UID()
                            pass
                        pass
                    drep["Reply"]="OK"
                    drep["Result"]=obj_list
                    pass
                elif "GetInhibitedItems" in dreq["cmd"]:
                    inhibited_classes=["DigitalInput", "Alarm", "Action"]
                    obj_list = {}
                    for cn in inhibited_classes:
                        print(cn)
                        objs = conn_pool[tid].get_objs(cn)
                        for obj in objs:
                            if obj.get_bool("inhibit")==True:
                                obj_list[obj.UID()]=obj.UID()
                                pass
                            pass
                        pass
                    drep["Reply"]="OK"
                    drep["Result"]=obj_list
                    pass
                elif "ToggleAlarm" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    drep["Reply"]="OK"
                    drep=graph_helper.change_switch(tid,uid,{})
                    pass
                elif "UnTriggerAlarm" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    drep["Reply"]="OK"
                    obj=conn_pool[tid].get_obj("Alarm",uid)
                    helpers["Session"].addAction(sess_pool[tid],"["+uid+", off]")
                    obj["switch"]="off"
                    obj["state"]="off" 
                    ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                    helpers["Session"].addComputation(sess_pool[tid],ret)
                    pass
                elif "TriggerAlarm" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    drep["Reply"]="OK"
                    obj=conn_pool[tid].get_obj("Alarm",uid)
                    helpers["Session"].addAction(sess_pool[tid],"["+uid+", on]")
                    obj["switch"]="on"
                    obj["state"]="on"
                    ret,affected=graph_helper.propagate_change_effect(conn_pool[tid],obj)
                    helpers["Session"].addComputation(sess_pool[tid],ret)
                    pass
                elif "RemoveDelayedAction" in dreq["cmd"]:
                    print("removing delayed actions")
                    uid=str(dreq["uid"])
                    dauid=str(dreq["dauid"])
                    drep["Reply"]="OK"
                    class_name="Alarm"
                    obj=conn_pool[tid].get_obj(class_name,uid)
                    print(obj["actions"])
                    for child in obj["actions"]:
                        if child.UID()==dauid:
                            print("deleting", child)
                            obj["actions"].remove(child)
                            print("deleted")
                            pass
                        pass
                    pass
                elif "GetClassSchema" in dreq["cmd"]:
                    class_name=str(dreq["class"])
                    ret=helpers[class_name].get_schema()
                    drep["schema"]=ret
                    drep["Reply"]="OK"
                    pass
                elif "GetAttributeCounts" in dreq["cmd"]:
                    class_name=str(dreq["class"])
                    attribute_name=str(dreq["attribute"])
                    obj_list = {}
                    objs = conn_pool[tid].get_objs(class_name)
                    for obj in objs:
                        counter=0
                        occurrences=obj.get_objs(attribute_name)
                        for occ in occurrences:
                            counter+=1
                            pass
                        obj_list[obj.UID()]=counter
                        pass
                    drep["Result"]=obj_list
                    drep["Reply"]="OK"
                    pass
                elif "GetSchema" in dreq["cmd"]:
                    uid=str(dreq["uid"])
                    obj=graph_helper.get_object(conn_pool[tid],uid)
                    dclass=conn_pool[tid].attributes(obj.class_name(),True)
                    dclass.update(conn_pool[tid].relations(obj.class_name(),True))
                    drep["class"]=dclass
                    drep["Reply"]="OK"
                    pass
                elif "GetSummaryOfObjects" in dreq["cmd"]:
                    try:
                        db=conn_pool[tid]
                        cc=db.classes()
                        ret={}
                        tot=0
                        for c in cc:
                            objs=conn_pool[tid].get_objs(c)
                            cnt=0
                            for obj in objs:
                                if obj.class_name()==c: cnt+=1
                                pass
                            ret[c]=cnt
                            tot+=cnt
                            pass
                        ret["Total"]=tot 
                        drep["Reply"]="OK"
                        drep["Result"]=ret
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetClasses")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetSysClasses" in dreq["cmd"]:
                    drep["Reply"]="OK"
                    drep["SysClasses"]=sysclasses
                    pass
                elif "GetClasses" in dreq["cmd"]:
                    drep["Reply"]="OK"
                    drep["Classes"]=classes
                    pass
                elif "GetClass" in dreq["cmd"]:
                    try:
                        uid=str(dreq["uid"])
                        obj=conn_pool[tid].get_obj("Alarm",uid)
                        drep["Reply"]="OK"
                        drep.update(serialize("Alarm",obj))
                    except:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetClass")
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "GetFullDSU" in dreq["cmd"]:
                    try:
                        drep["DIs"]={}
                        drep["actions"]={}
                        uid=str(dreq["uid"])
                        print("uid")
                        print(uid)
                        obj=conn_pool[tid].get_obj("DSU",uid)
                        DIs=obj.get_objs("digitalInput")
                        for DI in DIs:
                            drep["DIs"][DI.UID()]={"crate":DI.get_string("crate"), "slot":DI.get_u32("slot"),  "channel":DI.get_u32("channel"), "cable":DI.get_string("cable")}
                            pass
                        acts=obj.get_objs("actions")
                        for ac in acts:
                            drep["actions"][ac.UID()]={"crate":ac.get_string("crate"), "slot":ac.get_u32("slot"),"channel":ac.get_u32("channel"),  "cable":ac.get_string("cable")}
                            pass
                        drep["Reply"]="OK"
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s\n" % dreq["cmd"]
                        log("Error in GetFullDSU")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetNumberOfAffectedSystems" in dreq["cmd"]:
                    drep["Reply"]="OK"
                    objs = conn_pool[tid].get_objs("SystemBase")
                    affected_systems = 0
                    for obj in objs:
                        if obj.class_name() == "Group" or obj.class_name() == "DigitalInput" or obj.class_name() not in sysclasses: continue
                        if obj["state"]=="off":
                            if obj["default"]=="off": continue
                            affected_systems+=1
                            pass
                        pass
                    drep["Result"]=affected_systems
                    pass
                elif "GetFormulaPoF" in dreq["cmd"]:
                    try:
                        drep["Reply"]="OK"
                        uid=str(dreq["uid"])
                        s_level=str(dreq["level"])
                        i_level=int(s_level)
                        obj=graph_helper.get_object(conn_pool[tid],uid)
                        if obj is  None:
                            drep["Reply"]="Error. Object not found in database"
                            drep["Error"]="Error. Object not found in database"
                        else:
                            try:
                                class_name=obj.class_name()
                                pt=helpers["probtree"]
                                pt.setDb(conn_pool[tid])
                                #pt.setVerbose(True)
                                tree=pt.getTree(class_name,uid,i_level)
                                PoS_formula=""
                                PoS_value=0
                                print("Printing tree")
                                pt.printTree(tree,2)
                                print("Printing tree end")

                                try:
                                    tree_formula_uids = pt.getFormula(tree)
                                    print("tree_formula_uids--> %s" % str(tree_formula_uids))
                                    drep["tree_formula_uids"]=tree_formula_uids
                                    pass
                                except:
                                    print("!!! Error getting element names: tree_formula_uids")
                                    drep["Trace_exception"]+="Error when %s [getFormula]\n" % dreq["cmd"]
                                    drep["tree_formula_uids"]="Error getting element names: tree_formula_uids"
                                    print(sys.exc_info())
                                    pass

                                #Probability of Survival
                                try:
                                    PoS_formula = pt.getPoSFormula(tree)
                                    print("PoS_formula--> %s" % str(PoS_formula))
                                    drep["PoS_formula"]=PoS_formula
                                    pass
                                except:
                                    drep["Trace_exception"]+="Error when %s [getPoSFormula]\n" % dreq["cmd"]
                                    exc_type, exc_value, exc_traceback = sys.exc_info()
                                    print(sys.exc_info())
                                    traceback.print_tb(exc_traceback)
                                    drep["PoS_formula"]="Error getting formula: PoS_formula"
                                    pass

                                try:
                                    PoS_value = pt.getEvaluatedFormula(PoS_formula)
                                    drep["PoS_value"]=PoS_value
                                    pass
                                except:
                                    drep["Trace_exception"]+="Error when %s [getEvaluatedFormula]\n" % dreq["cmd"]
                                    drep["PoS_value"]="Error getting Probability of Survival: PoS_value"
                                    pass

                                #Probability of Failure
                                try:
                                    #PoF_formula=pt.getPoFFormula(tree)
                                    #drep["PoF_formula"]=PoF_formula
                                    drep["PoF_formula"]="1- %s" % str(PoS_value)
                                    try:
                                        #PoF_value = pt.getEvaluatedFormula(PoF_formula)
                                        drep["PoF_value"]=1-PoS_value
                                        pass
                                    except:
                                        drep["Trace_exception"]+="Error when %s [Probability of Failure formula]\n" % dreq["cmd"]
                                        drep["PoF_value"]="Error getting the result of Probability of Failure formula: PoF_value"
                                        pass
                                    pass
                                except:
                                    drep["Trace_exception"]+="Error when %s [Profability of Failure]\n" % dreq["cmd"]
                                    drep["PoF_formula"]="Error getting the formula of Profability of Failure: PoF_formula"
                                    pass

                                try:
                                    print("Going to get PCA_formulas. Printing tree")
                                    #print(tree)
                                    #print("====")
                                    PCA_formulas=pt.getPCAFormula(tree)
                                    print("Got PCA_formulas")
                                    print("PCA_formulas ---> %s " % str(PCA_formulas))
                                    print("-------")
                                    try:
                                        print("Going to get PCA_values")
                                        PCA_values=pt.getPCAValues(PCA_formulas)
                                        print("PCA_values ---> %s " % str(PCA_values))
                                        drep["PCA_values"]=PCA_values
                                        pass
                                    except:
                                        drep["Trace_exception"]+="Error when %s [Principal Component Analysis: PCA_values]\n" % dreq["cmd"]
                                        drep["PCA_values"]="Error getting Principal Component Analysis: PCA_values"
                                    pass
                                except:
                                    print("Error getting formulas for Principal Component Analysis")
                                    pass

                                drep["Reply"]="OK"
                            except:
                                drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                                print("Exception in GetFormulaPoF: Error getting object?")
                                drep["Reply"]="OK"
                                drep["Result"]="Object not found"
                                drep["Formula"]="Object not found"
                            pass
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetFormulaPoF")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="OK"
                        drep["Result"]="Error calculating formula. Under development"
                        pass
                    pass
                elif "GetReliabilityOfClass" in dreq["cmd"]:
                    try:
                        class_name=str(dreq["class_name"])
                        if class_name in sysclasses:
                            obj_list=get_all_objects_of_class(class_name)
                            pt=helpers["probtree"]
                            pt.setDb(conn_pool[tid])

                            reliability={}
                            for obj in obj_list:
                                tree=pt.getTree(class_name,obj,4)
                                PoS_formula = pt.getPoSFormula(tree)
                                PoS_value = pt.getEvaluatedFormula(PoS_formula)
                                reliability[obj]=[PoS_value,PoS_formula]
                                pass
                            drep["Reply"]="OK"
                            drep["Result"]=reliability
                            pass
                        else:
                            drep["Reply"]="OK"
                            drep["Result"]="Error: Class not supported"
                        pass
                    except:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetReliabilityOfClass")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="OK"
                        drep["Result"]="Error calculating GetReliabilityOfClass"
                        pass
                    pass
                elif "GetPoF" in dreq["cmd"]:
                    print("()  ======= GetPoF")
                    uid=str(dreq["uid"])
                    level=int(dreq["level"])
                    print("uid:",uid)
                    print("level", level)
                    rq_parents = th.get_parents
                    try:
                        drep["Reply"]="OK"
                        drep["Parents"]=pof
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetPoF")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: %s" % str(inst)
                        pass
                    print("() ======= GetPoF. END")
                    pass
                elif "GetIntervenedSystemsWithLocation" in dreq["cmd"]:
                    drep["Reply"]="OK"
                    drep["Result"]=th.GetIntervenedSystemsWithLocation(tid)
                    pass
                elif "GetAffectedSystemsWithLocation" in dreq["cmd"]:
                    drep["Reply"]="OK"
                    drep["Result"]=th.GetAffectedSystemsWithLocation(tid)
                    pass
                elif "GetAffectedSystems" in dreq["cmd"]:
                    log("Command: GetAffectedSystems")
                    try:
                        affected_=th.GetAffectedSystems(tid)
                        if affected_ != None:
                            drep["Reply"]="OK"
                            drep["Result"]=affected_
                            pass
                        else:
                            drep["Reply"]="Error"
                            drep["Result"]="Error"
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetAffectedSystems")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                        pass
                    pass
                elif "GetMinimaxDeployed" in dreq["cmd"]:
                    try:
                        obj_list= conn_pool[tid].get_objs("Minimax")
                        deployed={}
                        for obj in obj_list:
                            if obj["deployed"]=="yes":
                                deployed[obj.UID()]=""#obj.UID()
                                pass
                            pass                        
                        drep["Result"]=deployed
                        drep["Reply"]="OK"
                        pass
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetMinimaxDeployed")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error: "
                    pass
                elif "GetRackInfo" in dreq["cmd"]:
                    try:
                        uid=str(dreq["uid"])
                        #obj=conn_pool[tid].get_obj("Rack",uid)
                        #drep.update(serialize(obj.class_name,obj))
                        obj=graph_helper.get_object(conn_pool[tid],uid)
                        drep["uid"]=uid
                        drep["poweredBy"]=helpers[obj.class_name()].getPowerSource(obj)
                        drep["Reply"]="OK"
                    except Exception as inst:
                        drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                        log("Error in GetRackInfo")
                        print(inst)
                        print(sys.exc_info()[0])
                        drep["Reply"]="Error"
                        pass
                    pass
                elif "SetSwitchNoDB" in dreq["cmd"]:
                    # read several json or single uid
                    elementsjson=json.loads(str(dreq["json"]))
                    turnedoff=[]
                    for element in elementsjson:
                        uid=str(element)
                        #get the object
                        obj=None
                        try:
                            obj=graph_helper.get_object(conn_pool[tid],uid) 
                            pass
                        except:
                            drep["Trace_exception"]+="Error when %s]\n" % dreq["cmd"]
                            print(sys.exc_info())
                            pass
                        #check if the object was retrieved
                        if not obj:
                            drep["Reply"] = "Error"
                            drep["Error"] = "Object not found"
                        else:
                            #change switch wo. updating affected systems
                            if obj["switch"]=="on":
                                log( "Switch OFF uid: %s " % uid)
                                obj.set_enum("switch","off")  # set_enum is part of config
                                # record the switching off using helper
                                action_number=helpers["Session"].addAction(sess_pool[tid],"["+uid+", off]")
                                helpers[obj.class_name()].add_log(obj, "["+str(action_number)+"] Switched OFF by user")
                                turnedoff.append(str(uid))
                                pass
                            if obj["state"]=="on":
                                obj.set_enum("state","off")
                                pass
                            pass
                        pass
                    drep["Result"]=turnedoff
                    drep["Reply"]="OK"
                    pass
                elif "InhibitAllActions" in dreq["cmd"]:
                    print("Inhibiting all actions")
                    obj_list= conn_pool[tid].get_objs("Action")
                    for o in obj_list:
                        o.set_bool("inhibit",True)
                        #print "Inhibiting ", o.UID()
                        pass
                    drep["Reply"]="OK"
                    pass
                    
                elif "FindMPC" in dreq["cmd"]:
                    log("FindMPC")
                    children=json.loads(str(dreq["json"]))
                    inhibited_objects_uids=json.loads(str(dreq["inhibitedObjects"]))
                    is_exhaustive=False if "isExhaustive" in dreq and dreq["isExhaustive"]=="false" else True
                    elapsed_time=int(dreq["elapsedTime"]) if "elapsedTime"  in dreq else 0
                    
                    # get all parents of all alarms that have been triggered
                    log("Children: "%children)
                    localpath="%s/db/%s" % (tmpdir,tid)
                    #Create a new calculation
                    db_mpc=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc.xml"))
                    db_mpc2=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc2.xml"))
                    calculation_uid=helpers["MPCCalculation"].new_calculation([db_mpc,db_mpc2])
                    #Process for one point of failure
                    t = Process(target=helpers["MPCCalculation"].mpccalculation_runner, args=(db_mpc, calculation_uid,graph_helper, children,1,is_exhaustive,elapsed_time,inhibited_objects_uids ))
                    t.start()
                    #Process for two points of failure
                    t2 = Process(target=helpers["MPCCalculation"].mpccalculation_runner, args=(db_mpc2, calculation_uid,graph_helper, children,2,is_exhaustive,elapsed_time,inhibited_objects_uids ))
                    t2.start()
                    log("Replying")
                    drep["Result"]="Processing"#result        
                    drep["Reply"]="OK"
                    pass
                elif "StopMPC" in dreq["cmd"]:
                    log("StopMPC")
                    uid=str(dreq["uid"])
                    if t:  t.terminate(); t.join()
                    if t2: t2.terminate(); t2.join()
                    db_mpc=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc.xml"))
                    db_mpc2=config.Configuration("oksconfig:%s/%s"%(localpath,"sb.data_mpc2.xml"))
                    db_mpc.get_obj("MPCCalculation",uid)["status"]="Stopped"; db_mpc.commit()
                    db_mpc2.get_obj("MPCCalculation",uid)["status"]="Stopped"; db_mpc2.commit()
                    log("MPC calculation %s processes terminated, new status %s"%(uid,helpers["MPCCalculation"].get_status_multiple_points_of_failure([db_mpc,db_mpc2],obj.UID())))
                elif "GetObjsClasses" in dreq["cmd"]:
                    log("GetObjsClasses")
                    uids=json.loads(str(dreq["uids"]))
                    class_per_uid={}
                    for uid in uids:
                            class_per_uid[uid]=graph_helper.get_class_name(uid)
                    log("Replying")
                    drep["Classes"]=class_per_uid        
                    drep["Reply"]="OK"
                    pass
                elif "MatchLogsLogactions" in dreq["cmd"]:
                    # match a list of alarms with a list of actions
                    log("MatchLogsLogactions")

                    json_parsed = json.loads(dreq['json'])
                    logs, logactions  = json_parsed["logs"], json_parsed["logactions"]

                    for log_id in logs:
                        logs[log_id]['not_triggered'] = []
                        logs[log_id]['not_triggered_times']    = []
                        logs[log_id]['logactions']    = []
                        logs[log_id]['logactions_names'] = []
                        logs[log_id]['logactions_times'] = []

                    if logactions:
                        for log_id, log_data in logs.items():
                            alarm_obj = graph_helper.get_object(conn_pool[tid],log_data['name'])

                            # if alarm does not exist in oks, assume there are no actions
                            if not alarm_obj: continue

                            # find log_actions from list of actions
                            delayed_actions = alarm_obj["actions"]
                            for action in delayed_actions:
                                # get potenital matches
                                potential_matches = [ logaction_id for logaction_id, logaction_data in logactions.items() 
                                                     if logaction_data["name"] == action.UID().rsplit('_',1)[0] ]
                                if len(potential_matches) == 0: 
                                    # alarm has action, but was not triggered
                                    logs[log_id]['not_triggered'].append(action.UID())
                                elif len(potential_matches) == 1:
                                    logs[log_id]['logactions'].append(potential_matches[0])
                                    logs[log_id]['logactions_names'].append(logactions[potential_matches[0]]['name'])
                                    logs[log_id]['logactions_times'].append(logactions[potential_matches[0]]['time'])
                                else:
                                    # use "came" to find the closest one
                                    closest_match = min(potential_matches, key=lambda x: 
                                                        abs(int(x.replace('logaction_','')) - int(log_id.replace('log_',''))))
                                    logs[log_id]['logactions'].append(closest_match)
                                    logs[log_id]['logactions_names'].append(logactions[closest_match]['name'])
                                    logs[log_id]['logactions_times'].append(logactions[closest_match]['time'])
                            pass
                    drep["Reply"]="OK"
                    drep["Result"]=json.dumps(logs)

                elif "ReadDisplayEventsLogs" in dreq["cmd"]:
                    # for a list of logs, get events and logs saved in tmp/... for given time range
                    # consider tmp files
                    log("ReadDisplayEventsLogs")
                    int_tid = 666
                    if not "json" in dreq:
                        drep["Reply"]="Error"
                        drep["Error"]="No json in request"
                    else:
                        try:
                            json_parsed = json.loads(dreq['json'])
                        except:
                            json_parsed = {"Error":"Cannot parse json"}
                        if "Error" in json_parsed:
                            drep["Reply"]="Error"
                            drep["Error"]=json_parsed["Error"]
                        elif (not "time_range" in json_parsed) or (not "logs" in json_parsed):
                            drep["Reply"]="Error"
                            drep["Error"]="No time_range or logs in request"
                        else:
                            start_time, end_time = json_parsed["time_range"]
                            start_time  = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M")
                            end_time    = datetime.datetime.strptime(end_time,   "%Y-%m-%d %H:%M")

                            # get newest saved user input from file
                            old_display_NotGrouped, old_display_Events = {}, {} # old = on tmp file
                            if (os.path.exists(f"{tmpdir}/tmp/{int_tid}")) and (len(os.listdir(f"{tmpdir}/tmp/{int_tid}")) > 0):
                                newest_file = sorted(os.listdir("%s/tmp/%s" % (tmpdir,int_tid)))[-1]
                                with open(f"{tmpdir}/tmp/{int_tid}/{newest_file}", "r") as f: 
                                    json_dict = json.load(f)
                                    old_display_NotGrouped = json_dict.get("display_notGrouped", {}) 
                                    old_display_Events     = json_dict.get("display_Events", {})

                                    print("Newest file: %s/tmp/%s/%s" % (tmpdir, int_tid, newest_file))

                            # for all logs in time range, 
                            # check if they have been previously stored in user input
                            # or if they are commited to oks
                            requested_logs = json_parsed["logs"]

                            # new = will be returned
                            new_display_NotGrouped, new_display_Events = {}, {}

                            found_logs = [] 
                            not_in_db = []
                            # first, check user input in tmp file
                            for uid in requested_logs.keys():
                                if uid in old_display_NotGrouped:
                                    new_display_NotGrouped[uid] = deepcopy(old_display_NotGrouped[uid])
                                    found_logs.append(uid)
                                    continue

                                # log part of display_Events ?
                                for event_uid, event in old_display_Events.items():
                                    if uid in event["logs"]:

                                        # print("Reaching the bottleneck")
                                        # print(event)

                                        # new_display_Events[event_uid] = deepcopy(event)
                                        new_display_Events[event_uid] = json.loads(json.dumps(event))
                                        found_logs.append(uid)
                                        continue

                            

                            # second, check if remaining logs in oks for the remaining
                            for uid, rlog in requested_logs.items():
                                if uid in found_logs: continue

                                # get the log object from oks
                                log_obj = graph_helper.get_object(conn_pool[tid],uid)

                                # if log obj does not exist in oks, it is also not linked to an oks event object
                                if not log_obj:
                                    new_display_NotGrouped[uid] = rlog
                                    not_in_db.append(uid)
                                    continue
                                
                                # log obj can be linked to an event
                                event = log_obj["event"]

                                # if log obj is not linked to an event, it is part of display_notGrouped
                                if not event:
                                    new_display_NotGrouped[uid] = rlog
                                    continue

                                # else, get the logs that have the same event
                                logs_with_event = [l.UID() for l in event["logs"]]

                                alarms_with_event = {}
                                for luid in logs_with_event:
                                    _lobj = graph_helper.get_object(conn_pool[tid],luid)
                                    alarms_with_event[luid] = {
                                        "log": luid,
                                        "time": _lobj["time"] if _lobj else "N/A",
                                        "date": _lobj["date"] if _lobj else "N/A",
                                        "name": _lobj["name"] if _lobj else "N/A",
                                    }

                                # sort logs_with_event using the associated alarm_with_event
                                logs_with_event = list(sorted(logs_with_event, key=lambda luid: alarms_with_event[luid]["time"]))
                                
                                event_obj = graph_helper.get_object(conn_pool[tid],event.UID())
                                event_dict = {
                                    "uid" : event_obj.UID(),
                                    'description': event_obj["description"], # serves as title
                                    'short_description': event_obj["short_description"],
                                    'event_type': event_obj["event_type"],
                                    'date' : event_obj["date"],
                                    'time' : event_obj["time"],
                                    "logs" : logs_with_event, 
                                    "alarms" : alarms_with_event,
                                    "person" : event_obj["person"],
                                    "elisa"  : event_obj["elisa"],
                                }
                                new_display_Events[event.UID()] = event_dict
                                pass

                            # print("New display not grouped: %s"%new_display_NotGrouped)
                            # print("New display events:      %s"%new_display_Events)

                            # add actions to the event alarms, actions are stored in requested_logs
                            # iterate over all events
                            for event_uid, event_dict in new_display_Events.items():

                                new_display_Events[event_uid]["logactions"] = {}
                                new_display_Events[event_uid]["logactions_names"] = {}
                                new_display_Events[event_uid]["logactions_times"] = {}
                                new_display_Events[event_uid]["not_triggered"] = {}
                                new_display_Events[event_uid]["not_triggered_times"] = {}

                                logs = event_dict["logs"]
                                for event_log in logs:
                                    if event_log not in requested_logs: continue
                                    new_display_Events[event_uid]["logactions"][event_log]       = deepcopy(requested_logs[event_log]["logactions"])
                                    new_display_Events[event_uid]["logactions_names"][event_log] = deepcopy(requested_logs[event_log]["logactions_names"])
                                    new_display_Events[event_uid]["logactions_times"][event_log] = deepcopy(requested_logs[event_log]["logactions_times"])
                                    new_display_Events[event_uid]["not_triggered"][event_log]    = deepcopy(requested_logs[event_log]["not_triggered"])
                                    new_display_Events[event_uid]["not_triggered_times"][event_log] = deepcopy(requested_logs[event_log]["not_triggered_times"])

                            # add logactions etc to display_notGrouped if not already added
                            for log_uid, log_dict in new_display_NotGrouped.items():
                                if log_uid not in requested_logs: continue
                                keys = ["logactions", "logactions_names", "logactions_times", "not_triggered", "not_triggered_times"]
                                for key in keys:
                                    if key not in new_display_NotGrouped[log_uid]: new_display_NotGrouped[log_uid][key] = deepcopy(requested_logs[log_uid][key])

                            jsonReply = {
                                "display_notGrouped" : new_display_NotGrouped,
                                "display_Events"     : new_display_Events,
                                # "not_in_db" : not_in_db,
                                # "found_logs" : found_logs,
                            }
                            drep["Reply"]="OK"
                            drep["Result"]=json.dumps(jsonReply)
                            pass
                        pass
                    pass
                elif "WriteDisplayEventsLogs" in dreq["cmd"]:
                    # write changes to display events and logs to tmp file
                    log("WriteEventsLogs")
                    int_tid = 666
                    if not "json" in dreq:
                        drep["Reply"]="Error"
                        drep["Error"]="No json in request"
                    else:
                        json_parsed = json.loads(dreq['json'])
                        if (not "display_notGrouped" in json_parsed) or (not "display_events" in json_parsed) or (not "time_range" in json_parsed):
                            drep["Reply"]="Error"
                            drep["Error"]="No display_NotGrouped or display_Events or time_range in request"
                        else:
                            def _inTimeRange(time_str):
                                time = datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
                                return time >= start_time and time <= end_time
                            
                            def _eventsAreTheSame(oks_event, event_dict):
            
                                attributes = ["date", "time", "description", "short_description", "event_type", "person", "elisa"]
                                for attr in attributes:
                                    if str(oks_event[attr]) != str(event_dict[attr]):  return False
                                oks_logs = str(sorted([l.UID() for l in oks_event["logs"]]))
                                new_logs = str(event_dict["logs"])
                                if oks_logs != new_logs: return False
                                return True
                            
                            new_display_NotGrouped, new_display_Events = json_parsed["display_notGrouped"], json_parsed["display_events"]

                            # get the time range
                            start_time, end_time = json_parsed["time_range"]
                            start_time, end_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M"), datetime.datetime.strptime(end_time, "%Y-%m-%d %H:%M")

                            # create tmp/tid directory or get the latest file
                            old_display_NotGrouped, old_display_Events = {}, {}
                            if not os.path.exists("%s/tmp/%s" % (tmpdir, int_tid)): 
                                log("Creating directory %s/tmp/%s" % (tmpdir,int_tid))
                                os.makedirs("%s/tmp/%s" % (tmpdir,int_tid))
                            else:
                                # get the latest file from the directory
                                tmp_files = os.listdir("%s/tmp/%s" % (tmpdir,int_tid))

                                json_dict = {}
                                if len(tmp_files) != 0:
                                    latest_file = sorted(tmp_files)[-1]
                                    with open("%s/tmp/%s/%s" % (tmpdir,int_tid,latest_file), "r") as f:
                                        json_dict = json.load(f)
                    
                                old_display_NotGrouped = json_dict.get("display_notGrouped", {})
                                old_display_Events = json_dict.get("display_Events", {})

                            # print("so far stored: ")
                            # print("Old display notGrouped: ")
                            # for k,v in old_display_NotGrouped.items():
                            #     print(k,v)
                            
                            # print("Old display events: ")
                            # for k,v in old_display_Events.items():
                            #     print(k,v)
                            
                            # check if saved logs need to be updated to keep old changes
                            save_display_NotGrouped = {}
                            removed_log, changed_log, kept_log = [], [], []

                            # start with the old logs
                            for log_uid, log_dict in old_display_NotGrouped.items():

                                # keep all logs outside time range
                                log_datetime = "%s %s"%(log_dict["date"],log_dict["time"])
                                if not _inTimeRange(log_datetime):
                                    save_display_NotGrouped[log_uid] = log_dict
                                    kept_log.append(log_uid)
                                    continue

                                # prev saved logs in the time range are saved if in in new (else they are removed) and
                                # - not in oks
                                # - in oks but linked to an event
                                if log_uid in new_display_NotGrouped: 
                                    log_obj = graph_helper.get_object(conn_pool[tid],log_uid)

                                    # not in oks, save the new
                                    if not log_obj:
                                        save_display_NotGrouped[log_uid] = new_display_NotGrouped[log_uid]
                                        changed_log.append(log_uid)
                                        continue

                                    # in oks but linked to an event
                                    linked_event_uid = log_obj["event"]
                                    if linked_event_uid != "":
                                        save_display_NotGrouped[log_uid] = new_display_NotGrouped[log_uid]
                                        changed_log.append(log_uid)
                                        continue
                                    
                                removed_log.append(log_uid)

                            # continue with the new logs
                            for log_uid, log_dict in new_display_NotGrouped.items():
                                # if log in old, it has already been processed
                                if log_uid in old_display_NotGrouped: continue

                                # new saved logs that were not in the old display are saved if
                                # - not in oks
                                # - in oks but linked to an event
                                log_obj = graph_helper.get_object(conn_pool[tid],log_uid)

                                # not in oks, save the new
                                if not log_obj:
                                    save_display_NotGrouped[log_uid] = deepcopy(log_dict)
                                    changed_log.append(log_uid)
                                    continue

                                # in oks but linked to an event
                                linked_event_uid = log_obj["event"]
                                if linked_event_uid:
                                    save_display_NotGrouped[log_uid] = deepcopy(log_dict)
                                    changed_log.append(log_uid)
                                    continue

                                removed_log.append(log_uid)
                                


                            # print("Changed logs to ...")
                            # for k,v in save_display_NotGrouped.items():
                            #     print(k,v)
                            
                            # print("Kept logs:    %s"%kept_log)
                            # print("Removed logs: %s"%removed_log)
                            # print("Changed logs: %s"%changed_log)


                            # now treat the events
                            save_display_Events = {}
                            removed_events, changed_events, kept_events = [], [], []

                            # start with the old events
                            for event_uid, event_dict in old_display_Events.items():

                                # keep all prev saved events outside time range
                                event_datetime = "%s %s"%(event_dict["date"],event_dict["time"]) 
                                # skip events with no date or time
                                if "None" in event_datetime: continue
                                if not _inTimeRange(event_datetime):
                                    save_display_Events[event_uid] = event_dict
                                    kept_events.append(event_uid)
                                    continue

                                # prev saved events in the time range are saved if they are in new (else they are removed) and
                                # - not in oks
                                # - in oks but different event
                                if event_uid in new_display_Events: 
                                    event_obj = graph_helper.get_object(conn_pool[tid],event_uid)

                                    # not in oks, save the new
                                    if not event_obj:
                                        save_display_Events[event_uid] = new_display_Events[event_uid]
                                        changed_events.append(event_uid)
                                        continue

                                    # in oks but different to the new
                                    if not _eventsAreTheSame(event_obj, new_display_Events[event_uid]):
                                        save_display_Events[event_uid] = new_display_Events[event_uid]
                                        changed_events.append(event_uid)
                                        continue

                                removed_events.append(event_uid)

                            # continue with the new events 
                            for event_uid, event_dict in new_display_Events.items():
                                # if event in old, it has already been processed
                                if event_uid in old_display_Events: continue

                                # new saved events that were not in the old display are saved if
                                # - not in oks
                                # - in oks but different event
                                event_obj = graph_helper.get_object(conn_pool[tid],event_uid)

                                # not in oks, save the new
                                if not event_obj:
                                    save_display_Events[event_uid] = deepcopy(event_dict)
                                    changed_events.append(event_uid)
                                    continue

                                # in oks but different to the new
                                if not _eventsAreTheSame(event_obj, event_dict):
                                    save_display_Events[event_uid] = deepcopy(event_dict)
                                    changed_events.append(event_uid)
                                    continue

                                removed_events.append(event_uid)
                            
                            # print("Changed events to ...")
                            # for k,v in save_display_Events.items():
                            #     print(k,v)
                            
                            # print("Kept events:    %s"%kept_events)
                            # print("Removed events: %s"%removed_events)
                            # print("Changed events: %s"%changed_events)
                                
                            # write the new json to a file
                            jsonDict_tmp = {
                                "display_notGrouped" : save_display_NotGrouped,
                                "display_Events" : save_display_Events,
                            }
                            time_now_str = "%s"%datetime.datetime.now().strftime("%y%m%d_%H%M%S")
                            with open("%s/tmp/%s/%s" % (tmpdir,int_tid,"display_%s.json"%time_now_str), "w") as f: json.dump(jsonDict_tmp, f)   

                            # print("New display not grouped: %s/tmp/%s/%s" % (tmpdir,int_tid,"display_%s.json"%time_now_str))

                            # print("New display not grouped: ")
                            # for k,v in save_display_NotGrouped.items():
                            #     print(k,v)
                            # print("New display events: ")
                            # for k,v in save_display_Events.items():
                            #     print(k,v)

                            drep["Reply"]="OK"

                            pass
                        pass
                    pass
                elif "GetAlarmStats" in dreq["cmd"]:
                    # get the stats: Unknown, Error, Intervention for a list of alarms per accessing the logs
                    # and also the shared events
                    # so far, this ignores the tmp files
                    log("GetAlarmStats")
                    if not "json" in dreq:
                        drep["Reply"]="Error"
                        drep["Error"]="No json in request"
                    else:
                        json_parsed = json.loads(dreq['json'])
                        if (not "alarms" in json_parsed) or (not "date" in json_parsed):
                            drep["Reply"]="Error"
                            drep["Error"]="No alarms or date in request"
                        else:
                            log("GetAlarmStats: %s for %s"%(json_parsed["alarms"], json_parsed["date"]))

                            if "nevents" in json_parsed: 
                                log(f"GetAlarmStats: Only request %s events"%{json_parsed['nevents']} )

                            alarms_uid = json_parsed["alarms"]
                            # check correct uid
                            alarms_found = []
                            for uid in alarms_uid:
                                alarm_object = graph_helper.get_object(conn_pool[tid],uid)
                                if alarm_object:
                                    alarms_found.append(alarm_object)

                            logs_per_alarm = [alarm_obj["logs"] for alarm_obj in alarms_found]

                            # get the event uid per log per alarm
                            events_per_alarm = []
                            for log_alarm in logs_per_alarm:
                                events_per_alarm.append([ _log["event"].UID() if _log["event"] else "None" for _log in log_alarm])

                            # get the shared events
                            shared_events = set(events_per_alarm[0]).intersection(*events_per_alarm)
                            
                            shared_events = list(shared_events)
                            # ignore 'None'
                            shared_events = [event for event in shared_events if event != "None"]

                            reply_shared_events = []
                        
                            # time is defined wrt to date given in request
                            try: reference_date = datetime.datetime.strptime(json_parsed["date"], "%Y-%m-%d")
                            except: reference_date = datetime.datetime.now()
                    
                            stats = {}
                            for period in ["inclusive", "last-year", "last-month"]:
                                stats[period] = {}
                                stats[period]['sum'] = { "U" : 0, "E" : 0, "I" : 0, "N" : 0 , "T" : 0}
                                for day in ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]: 
                                    stats[period][day] = { "U" : 0, "E" : 0, "I" : 0, "N" : 0 ,"T" : 0}
                                for hour in range(24):
                                    stats[period][hour] ={ "U" : 0, "E" : 0, "I" : 0, "N" : 0 , "T" : 0}

                            responsibles = set()

                            for event_uid in shared_events:
                                event = graph_helper.get_object(conn_pool[tid],event_uid)
                                if event:
                                    event_type = event["event_type"]

                                    # encode event_type to int
                                    if event_type == "Unknown": event_type = "U"
                                    elif event_type == "Error": event_type = "E"
                                    elif event_type == "Intervention": event_type = "I"
                                    else: event_type = "N"

                                    event_date = event["date"]
                                    event_time = event["time"]

                                    # get the weekday from the date YYYY-MM-DD
                                    date = datetime.datetime.strptime(event_date, "%Y-%m-%d")
                                    weekday = datetime.datetime.strptime(event_date, "%Y-%m-%d").strftime("%A")
                                    hour = int(event_time.split(":")[0])
                                    #print("Event: %s, %s, %s, %s, %s"%(event_type, event_date, event_time, weekday, hour))

                                    # check date within 1 year of reference_date
                                    days = (reference_date - date).days
                                    if days <= 365:
                                        stats['last-year'][hour][event_type] += 1
                                        stats['last-year'][hour]["T"] += 1

                                        stats['last-year'][weekday][event_type] += 1
                                        stats['last-year'][weekday]["T"] += 1

                                        stats['last-year']['sum'][event_type] += 1
                                        stats['last-year']['sum']["T"] += 1
                                        pass
                                    # check date within 1 month of reference_date
                                    if days <= 30:
                                        stats['last-month'][hour][event_type] += 1
                                        stats['last-month'][hour]["T"] += 1

                                        stats['last-month'][weekday][event_type] += 1
                                        stats['last-month'][weekday]["T"] += 1

                                        stats['last-month']['sum'][event_type] += 1
                                        stats['last-month']['sum']["T"] += 1
                                        pass

                                    # inclusive
                                    stats['inclusive'][hour][event_type] += 1
                                    stats['inclusive'][hour]["T"] += 1

                                    stats['inclusive'][weekday][event_type] += 1
                                    stats['inclusive'][weekday]["T"] += 1

                                    stats['inclusive']['sum'][event_type] += 1
                                    stats['inclusive']['sum']["T"] += 1

                                    # get the logs that have the same event
                                    logs_with_event = [l.UID() for l in event["logs"]]

                                    alarms_with_event = {}
                                    for luid in logs_with_event:
                                        _lobj = graph_helper.get_object(conn_pool[tid],luid)
                                        alarms_with_event[luid] = {
                                            "log": luid,
                                            "time": _lobj["time"] if _lobj else "N/A",
                                            "date": _lobj["date"] if _lobj else "N/A",
                                            "name": _lobj["name"] if _lobj else "N/A"
                                        }

                                    # sort logs_with_event using the associated alarm_with_event
                                    logs_with_event = list(sorted(logs_with_event, key=lambda luid: alarms_with_event[luid]["time"]))

                                    # add event info
                                    reply_shared_events.append({
                                        "uid" : event.UID(),
                                        'description': event["description"],
                                        'short_description': event["short_description"],
                                        'event_type': event["event_type"],
                                        'date' : event["date"],
                                        'time' : event["time"],
                                        "logs" : logs_with_event, 
                                        "alarms" : alarms_with_event,
                                        "person" : event["person"],
                                        "elisa"  : event["elisa"],
                                    })

                                    # get the responsibles
                                    responsibles_event = set()
                                    persons_string = event["person"]

                                    if persons_string:
                                        delimiters = [",", " and ", "&"]
                                        found_delimiter = False
                                        for delimiter in delimiters:
                                            if delimiter in persons_string:
                                                found_delimiter = True
                                                # Split and extend the responsibles set
                                                persons = [name.strip() for name in persons_string.split(delimiter)]
                                                responsibles_event.update(persons)
                                                break # TODO: this ignores situations where there are multiple delimiters eg. "A, B and C"

                                        if not found_delimiter: responsibles_event.add(persons_string.strip())
                                    responsibles.update(responsibles_event)

                                else:
                                    # None only for inclusive for now
                                    stats['inclusive'][hour]["None"] += 1
                                    stats['inclusive'][hour]["T"] += 1

                                    stats['inclusive'][weekday]["None"] += 1
                                    stats['inclusive'][weekday]["T"] += 1

                                    stats['inclusive']['sum']["None"] += 1
                                    stats['inclusive']['sum']["T"] += 1

                            if len(responsibles) > 1: responsibles.discard("Unknown")

                            # avoid loading all shared_events, limit to about 10
                            if "nevents" in json_parsed:
                                try:
                                    nevents = int(json_parsed["nevents"]) 
                                except:
                                    nevents = 20
                                    log("GetAlarmStats: Could not parse nevents, using default 20")

                                if nevents == -1:
                                    reply_shared_events = reply_shared_events
                                    log("GetAlarmStats: Using all shared events")
                                    for event in reply_shared_events:
                                        print(event["date"], event["time"], event["description"])
                                elif len(reply_shared_events) > nevents:
                                    reply_shared_events = reply_shared_events[:nevents]
                            
                            jsonDict = {
                                "alarms" : alarms_uid,
                                "stats" : stats,
                                "events" : reply_shared_events,
                                "responsibles" : list(responsibles)
                            }

                            drep["Reply"]="OK"
                            drep["Result"]=json.dumps(jsonDict)
                            pass
                        pass
                    pass
                elif "CreateSlides" in dreq["cmd"]:
                    log("CreateSlides")
                    if not "json" in dreq:
                        drep["Reply"]="Error"
                        drep["Error"]="No json in request"
                    else:
                        json_parsed = json.loads(dreq['json'])
                        if "display_events" in json_parsed:

                            # codi md
                            codi = CodiHelper(json_parsed["display_events"])
                            codi.write_to_file("/root/server/data/codi.txt")
                            with open("/root/server/data/codi.txt", "r") as f: codi_txt = f.read()

                            # pptx
                            create_slides("/root/server/data/DSS_master_slide.pptx",
                                        "/root/server/data/my_tmp_file.pptx",
                                        json_parsed["display_events"])
                            
                            # Read the pptx as binary and then encode to base64
                            with open("/root/server/data/my_tmp_file.pptx", "rb") as f:
                                binary_data = f.read()
                                base64_data = base64.b64encode(binary_data).decode('utf-8')  # Base64 encode and convert to string
                            
                            drep["Reply"] = "OK"
                            drep["codi"] = codi_txt
                            drep["pptx"] = base64_data  # Store base64 encoded data here
                pass
            time1=datetime.datetime.now()
            drep['dt']="%.1f"%((time1-time0).total_seconds()*1000)
            srep = json.dumps(drep)
            #print("--------------1")
            #print(drep)
            #print("--------------2")
            #print(srep)
            #print("--------------3")
            log("Reply  : %s"%srep[:500])
            log("Length : %i"%len(srep))
            client.send(("%08x"%len(srep)).encode())
            client.send(srep.encode())
            pass
        client.close()
        pass
    pass

if __name__ == "__main__":
    uname=pwd.getpwuid(os.geteuid())[0]
    tmpdir="/tmp/%s" % uname

    import argparse
    parser = argparse.ArgumentParser("Expert System Server")
    parser.add_argument("-o","--output",help="output to file. Default print on screen")
    parser.add_argument("--tmpdir",help="work directory. Default %s" % tmpdir)
    args=parser.parse_args()

    if args.output is not None:
        try:
            errput="%s.err" % args.output[:-4]
            print ("Output dir: %s" %os.path.dirname(args.output))
            if os.path.dirname(args.output): os.system("mkdir -p %s"%os.path.dirname(args.output))
            sys.stdout = open(args.output,'w+')
            sys.stderr = open(errput,'w+')
            output=args.output
            pass
        except:
            print("Could not redirect output")
            pass
        pass
    if args.tmpdir:
        tmpdir=args.tmpdir
        pass
    startSocket()
