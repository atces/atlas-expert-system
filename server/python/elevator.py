#!/usr/bin/env python
# ############################################
# Elevator Helper
# Carlos.Solans@cern.ch
# January 2019
# ############################################

import helper

class ElevatorHelper(helper.Helper):
    pass
    
if __name__ == '__main__':
    helper.Helper().cmdline("Elevator")
    print ("Have a nice day")
