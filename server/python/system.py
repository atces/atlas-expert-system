#!/usr/bin/env python
#############################################
# System Helper
# Carlos.Solans@cern.ch
# June 2017
# August 2018: fix cmdline
#############################################

import helper

class SystemHelper(helper.Helper):
    
    # checkConsistency
    def checkConsistency(self):
        if self.verbose: print ("Check consistency in db")
        objs=self.db.get_objs("System")
        if self.verbose: print ("Objects loaded")
        for obj in objs:
            if self.verbose: print ("Parsing %s"%(obj.UID()))
            for obj2 in obj.get_objs("powers"):
                objpow=self.db.get_obj("System",obj2.UID())
                if self.verbose: print ("Check if obj %s powers objpow %s"%(obj.UID(),objpow.UID()))
                if obj in objpow.get_objs("poweredBy"):
                    print ("%s already in %s.poweredBy"%(obj.UID(),objpow.UID()))
                    pass
                else:
                    print ("%s powers %s"%(obj.UID(),objpow.UID()))
                    vv=objpow["poweredBy"]
                    vv.append(obj)
                    objpow["poweredBy"]=vv
                    pass
            pass
        return

if __name__ == '__main__':

    helper.Helper().cmdline("System")
    print ("Have a nice day")
