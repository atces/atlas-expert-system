#!/usr/bin/env python
#############################################
# MPC Calculations Helper
# Gustavo.Uribe@cern.ch
#############################################

import helper
import datetime
import config
import json
import signal
from collections import OrderedDict

class MPCCalculationHelper(helper.Helper):   

    def addMetaData(self, db, graph_helper, obj, affected,is_exhaustive,elapsedTime):
        affected={x:graph_helper.get_class_name(x) for x in affected}
        obj.set_string("affected",str(affected))
        obj.set_string("elapsedTime",str(elapsedTime) if not elapsedTime==0 else ">120")
        obj.set_string("status","Starting...")
        obj.set_string("type","%s"%("exhaustive" if is_exhaustive else "non-exhaustive"))
        obj.set_string("name",obj.UID())
        db.commit()
        pass   

    def new_calculation(self, dbs):
        calculation_name="Calculation_"+datetime.datetime.now().strftime("%Y%m%d__%H_%M_%S")
        if not isinstance(dbs,list): dbs=[dbs]
        for db in dbs:
            calculation_obj=db.create_obj("MPCCalculation",calculation_name)
            calculation_obj["datetime"]=datetime.datetime.now().strftime("%Y.%m.%d %H:%M:%S")
            db.commit()
        return calculation_name
    pass

    def get_status_multiple_points_of_failure(self,dbs,calculation_uid):
        status=""
        if not isinstance(dbs,list): dbs=[dbs]
        for db in dbs:
            calculation_obj=db.get_obj("MPCCalculation",calculation_uid)
            if "Analyzing common parents" in status: break
            status=calculation_obj["status"]
        return status

    def inhibitObjects(self, db, graph_helper,calculation,inhibited_objects_uids):
        calculation.set_string("inhibitedUIDs",str({x:graph_helper.get_class_name(x) for x in inhibited_objects_uids}))
        for uid in inhibited_objects_uids:
                if graph_helper.get_object(db,uid): graph_helper.get_object(db,uid)["inhibit"]=True
        db.commit()

    def removeInhibits(self, db, graph_helper,calculation,inhibited_objects_uids):
        #calculation_dict=json.loads(calculation.get_string("inhibitedUIDs"))
        #calculation.set_string("inhibitedUIDs",str({x:graph_helper.get_class_name(x) for x in calculation_dict if x not in inhibited_objects_uids }))
        for uid in inhibited_objects_uids:
                if graph_helper.get_object(db,uid): graph_helper.get_object(db,uid)["inhibit"]=False
        db.commit()

    def mpccalculation_runner(self, db_mpc, calculation_uid, graph_helper, children,max_faulty_systems,is_exhaustive=True,elapsed_time=0,inhibited_objects_uids=[]):
        print("Starting thread for %i points of failure...."%max_faulty_systems)
        def term_handler(sig,frame):
            print("Terminating the process")
            raise Exception
        signal.signal(signal.SIGTERM, term_handler)
        calculation_obj=db_mpc.get_obj("MPCCalculation",calculation_uid)
        self.addMetaData(db_mpc, graph_helper, calculation_obj, children,is_exhaustive,elapsed_time)
        self.inhibitObjects(db_mpc, graph_helper, calculation_obj, inhibited_objects_uids)
        print("Simulation for ...%s" % calculation_obj.UID() )
        try:
            result=graph_helper.find_MPC(db_mpc, children,ctime=elapsed_time,faulty_systems=max_faulty_systems,is_exhaustive=is_exhaustive,calculation_obj=calculation_obj)
            self.removeInhibits(db_mpc, graph_helper, calculation_obj, inhibited_objects_uids)
        except Exception as e:
            print ("Process terminated")
            self.removeInhibits(db_mpc, graph_helper, calculation_obj, inhibited_objects_uids)
            db_mpc.commit()
        print("Thread done...%s" % calculation_obj.UID())
    pass
    
    #Update results for a calculation with a number of max faulty systems
    def update_results(self,db,calculation_obj,MPC,graph_helper):
        if len(MPC)>0 and not isinstance(MPC[0],tuple): calculation_obj.set_string("result",json.dumps(OrderedDict([(x,graph_helper.get_class_name(x)) for x in MPC])))
        else: calculation_obj.set_string("result",json.dumps(OrderedDict([(x,graph_helper.get_class_name(x)) for i in range(0,len(MPC)) for x in MPC[i]])))
        db.commit()

    #Update progress
    def update_progress(self,db,calculation_obj,completion_percentage):
        calculation_obj.set_u8("progress",int(completion_percentage))
        db.commit()

    #Update status
    def update_status(self,db,calculation_obj,new_status):
        calculation_obj.set_string("status",new_status)
        db.commit()
