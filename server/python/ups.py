#!/usr/bin/env python
#############################################
# UPS helper
# Carlos.Solans@cern.ch
# June 2019
#############################################

import helper

class UPSHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("UPS")
    print ("Have a nice day")
