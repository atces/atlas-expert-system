#!/usr/bin/env python
# ############################################
# Pneumatic Valve Helper
# Ignacio.Asensi@cern.ch
# Nov 2018
# ############################################

import helper

class PneumaticValveHelper(helper.Helper):
    pass


if __name__ == '__main__':

    helper.Helper().cmdline("PneumaticValve")
    print ("Have a nice day")
