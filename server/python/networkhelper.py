#!/usr/bin/env python
#############################################
# Helper
# Gustavo Uribe <gustavo.uribe@cern.ch>
#############################################

import config

class NetworkHelper():

	# update_is_reachable_in
	# brief Update the relationship isReachableIn. It is the list of subnets where the device can be reached.
	# param peer ConfigObject representing the peer
	# return active_subnets List of subnet that are active and where the device is reachable
	def update_is_reachable_in(self,peer,haspower=True):
		if not haspower: peer.set_objs("isReachableIn",[]); return []
		active_nets={}
		subnets={obj.UID():obj for obj in peer.get_objs("isNetworkPeerIn")}
		subnets.update({obj.UID():obj for obj in peer.get_objs("isReachableThroughASetIn")})
		for subnet in subnets.values():
			active_nets.update(self._get_active_supernets(subnet,{}))
		peer.set_objs("isReachableIn",list(active_nets.values()))
		return list(active_nets.values())
	
	# _get_active_supernets
	# brief Internal recursive method for getting the list of active supernets in the given subnet
	# param subnet Current subnet to update 
	# param active_subnets List of subnet that are active and where the device is reachable (updated recursively and return)
	def _get_active_supernets(self,subnet,active_subnets):
		if subnet.get_string("state")=="on":
			active_subnets[subnet.UID()]=subnet
			for supernet in subnet.get_objs("isPartOfSubnet"):
				self._get_active_supernets(supernet,active_subnets)
		return active_subnets 

	# are_servers_reachable
	# brief check if all servers of some client/peer are reachable
	# param peer ConfigObject representing the peer
	# param active_subnets List of subnet that are active and where the device is reachable
	# return True or False
	def are_servers_reachable(self,peer,active_nets,services):
		if len(active_nets)==0: return True #Device is not described in ATCN graph
		if len(services)>0:has_common_subnet=False   
		active_nets_uid=[x.UID() for x in active_nets]
		#Check if server and client have one subnet in common where are reachable
		for service in services:
			servers=service.get_objs("providers")
			if len(servers)==0: continue #If is a black-box service it is assume that is reachable
			for server in servers: 
				server_nets=server.get_objs("isReachableIn")
				server_nets+=server.get_objs("isReachableThroughASetIn")
				for server_net in server_nets: 
					if server_net.UID() in active_nets_uid: has_common_subnet=True; break
				if has_common_subnet: break
			if not has_common_subnet: return False
			has_common_subnet=False
		return True			

	def are_on_required_services(self,peer,hasnet,haspower,active_nets,ctime):
		services=peer.get_objs("isClientOf")
		if len(services)==0: return True
		if not hasnet or not haspower: return False
		for service in services:
			#DHCP and NTP failure affects only when the peer was previously powered off
			#Currently is not possible to know if the peer was affected previously by the power or something else
			#Therefore, the simulation is not precise in case of recover of network downtime or other previous failures
			if service.get_string("state")=="off":
				if ("DHCP" in service.UID() or "DHCP" in service.get_string("networkServiceTypeOffered")) :
					if peer.get_string("state")=="off": return False 
				elif ("NTP" in service.UID() or "NTP" in service.get_string("networkServiceTypeOffered")) :
					if peer.get_string("state")=="off": return False 
				else:
					return False
		#return True
		return self.are_servers_reachable(peer,active_nets,services)
				 
	
	def hasRequiredNetworkService(self,peer,relation="isClientOf",ctime=0):
		services=peer.get_objs("isClientOf")
		active_nets=self.update_is_reachable_in(peer,True)
		return self.are_on_required_services(peer,True,True,active_nets,ctime)

	# Overloading update_state from helper	
	# update_state
	# brief Update the object state
	# param obj ConfigObject
	# return None
	def update_state(self,obj,ctime=0):
		if obj.get_string("switch")=="off":
		    obj.set_enum("state","off")
		    self.update_is_reachable_in(obj,False)
		    return self.msgr.switchOff()
		verboseobjects=[""]
		if self.verbose and obj.UID() in verboseobjects: print ("%s Processing.... %s" % (self.classname,obj.UID()))
		is_satisfied_per_dependancy={} #True if the dependancy is satisfied
		for relation in self.function_per_dependancy:
			is_satisfied_per_dependancy[relation]=getattr(self,self.function_per_dependancy[relation])(obj,relation,ctime=ctime,**self.additional_attributes_per_dependancy[relation])
			if not is_satisfied_per_dependancy[relation]:
				if relation == 'poweredBy': self.update_is_reachable_in(obj,False)	
				elif relation == 'isNetworkPeerIn': 
					self.update_is_reachable_in(obj,True)	
					if obj.class_name()=="NetworkSwitch" or obj.class_name()=="NetworkRouter": continue
				return self._return_off(obj,is_satisfied_per_dependancy)
		obj.set_enum("state","on")
		return self.msgr.getOn() 

