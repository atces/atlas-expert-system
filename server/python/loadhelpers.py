#!/usr/bin/env python
#############################################
# LoadHelpers
#
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
# Gustavo.Uribe@cern.ch
#############################################

LoadHelperClasses={}

class LoadHelpers:
    
    def __init__(self,db):
        non_abstract_classes=[]
        for clazz in db.classes():#Verify if the class is not abstract
                try:
                        db.create_obj(clazz,"Dummy"+clazz)
                        non_abstract_classes.append(clazz)
                except RuntimeError: continue
        db.abort()
	#1st element: classname
	#2nd element: classname lowercase is the name of the python module including the helper class
	#3rd element: classname of the python helper
        #4th element: means can be switch off and on
        #5th element: class in verbose mode
        self.classes=[[clazz,clazz.lower(),clazz+"Helper",True if "switch" in db.__schema__.data[clazz]['attribute'] else False,False] for clazz in non_abstract_classes]
	#Exceptions (classes with switch but can not be used)
        self.classes.remove(['Group','group','GroupHelper',True,False])
        self.classes.append(['Group','group','GroupHelper',False,False])
	#Exceptions (class name different to the python module name)
        #self.classes.remove(['sys','sys','sysHelper',True,False])
        #self.classes.append(['sys','system','SysHelper',True,False])


        self.helpers=LoadHelperClasses
       	for c in self.classes:
       	    if c[0] in self.helpers: continue
       	    print ("Loading: %s" % c[1])
       	    mod=__import__(c[1])
       	    self.helpers[c[0]] = getattr(mod,c[2])(c[0],db,verbose=c[4])
       	    pass
    
    def getHelpers(self):
        return self.helpers
        pass
        
    def getClasses(self):
        ret=[]
        for c in self.classes: ret.append(c[0])
        return ret

    def getSearchableClasses(self):
        ret=[]
        for c in self.classes: 
            if c[0]!="Session":
                ret.append(c[0])
                pass
            pass
        return ret

    def getSysClasses(self):
        ret=[]
        for c in self.classes: 
            if c[3] is True:
                ret.append(c[0])
                pass
            pass
        return ret    
    pass
    
