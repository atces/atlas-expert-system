#!/usr/bin/env python
#############################################
# PLC helper
# ignacio.asensi@cern.ch
# August 2018
#############################################

import helper
from networkhelper import NetworkHelper as NetHelper

class PLCHelper(NetHelper,helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("PLC")
    print ("Have a nice day")
