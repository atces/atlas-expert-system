#!/usr/bin/env python

if __name__ == '__main__':

    import os
    import sys
    import config
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("database",help="database file")
    parser.add_argument("-u","--uid",help="UID",required=True)
    parser.add_argument("-t","--type",help="Class type",required=True)
    parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
    args=parser.parse_args()

    print ("Load database: %s" % args.database)
    db=config.Configuration("oksconfig:%s"%(args.database))

    print ("Delete object: %s@%s" % (args.uid,args.type))
    obj=db.get_obj(args.type,args.uid)
    if not obj:
        print ("Object not found")
        pass
    else:
        ans=input("Do you wish to continue? [y/n]: ")
        if "y" in ans.lower(): 
            db.destroy_obj(obj)
            print ("Commit changes")
            db.commit("")
            pass
        pass
    print ("Have a nice day")
    pass
