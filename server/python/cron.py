#!/usr/bin/env python
# ############################################
# CRON
# Carlos.Solans@cern.ch
# June 2019
# Added flag for production - January 2022
# ############################################

import os
import sys
import subprocess

basedir=os.path.dirname(os.path.realpath(__file__))
isprod=True if not "-dev" in basedir else False

tasks=[
    {"prod":False,"binary":"python3 %s/thumbnailGen.py"%basedir,"env":"source %s/setup.sh"%basedir},
    {"prod":False,"binary":"python3 %s/updateFinder.py"%basedir,"env":"source %s/setup.sh"%basedir},
    {"prod":False,"binary":"python3 %s/updateMenuTree.py"%basedir},
    {"prod":False,"binary":"python3 %s/updateVersion.py"%basedir,"env":"source %s/setup.sh"%basedir},
    {"prod":False,"binary":"python3 %s/update_svg_diagrams.py"%basedir},
    {"prod":True, "binary":"python3 %s/reminders.py"%basedir,"env":"source %s/setup.sh"%basedir},
]

if isprod: print("Environment is production")
else: print("Environment is development")

for t in tasks:
    if t["prod"]!=isprod: continue
    print("Running: %s"%t["binary"])
    cmd=t["binary"]
    if "env" in t: cmd=t["env"]+";"+cmd
    pid=subprocess.Popen(cmd,shell=True)
    pid.wait()
    pass
    
print("Have a nice day")

