#!/usr/bin/env python
# ############################################
# Lighting Helper
# Carlos.Solans@cern.ch
# July 2018
# August 2018: fix cmdline
# ############################################

import helper

class LightingHelper(helper.Helper):
    pass
    
if __name__ == '__main__':

    helper.Helper().cmdline("Lighting")
    print ("Have a nice day")
