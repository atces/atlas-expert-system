#!/usr/bin/env python
#############################################
# Sniffer Tiroir helper
# Carlos.Solans@cern.ch
# February 2019
#############################################

import helper

class SnifferTiroirHelper(helper.Helper):
    pass

if __name__ == '__main__':
    helper.Helper().cmdline("SnifferTiroir")
    print ("Have a nice day")
