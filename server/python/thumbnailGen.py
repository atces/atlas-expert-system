#!/usr/bin/env python
# Expert system thumbnails photo generator
# Ignacio.Asensi@cern.ch
# March 2017


import os
import sys
import PIL
import argparse
from PIL import Image

size=(128,128)
basedir=os.path.dirname(os.path.realpath(__file__))
defdir = "%s/../../login/photos/" % basedir

parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="input directory",default=defdir)
args=parser.parse_args()

print('Parsing photos in directory: ' + os.path.abspath(args.input))

for root, subdirs, files in os.walk(args.input):  
    for filename in files:
        
        if (not filename.endswith("JPG") and 
            not filename.endswith("JPEG") and 
            not filename.endswith("jpg") and 
            not filename.endswith("jpeg")): continue
        if filename.startswith("thumbnail"): continue
        if os.path.exists(root+"/thumbnail_"+filename): continue
        
        print("converting: %s => thumbnail_%s" % (filename,filename))
            
        file_path = os.path.join(root, filename)
    
        im=PIL.Image.open(file_path)
        if hasattr(im, '_getexif') and im._getexif():
            exif=dict(im._getexif().items())
            if 274 in exif: 
                if exif[274]==3:
                    #print("Rotate 180 deg");
                    im=im.rotate(180,expand=True)
                    pass
                elif exif[274]==6:
                    #print("Rotate 270 deg");
                    im=im.rotate(270,expand=True)
                    pass
                elif exif[274]==8:
                    #print("Rotate 90 deg");
                    im=im.rotate(90,expand=True)
                    pass
                pass
            pass
        im.thumbnail(size, Image.LANCZOS)
        im.save("%s/thumbnail_%s"%(root,filename))
        pass
    pass

print("Have a nice day")
