#!/usr/bin/env python
#############################################
# Rack helper
# Carlos.Solans@cern.ch
# July 2017
# August 2018: fix cmdline
# March 2019: remove AlarmProviderBase
#############################################

import helper

class RackHelper(helper.Helper):
    pass

if __name__ == '__main__':

    helper.Helper().cmdline("Rack")
    print ("Have a nice day")
