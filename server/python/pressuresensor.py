#!/usr/bin/env python
#############################################
# Pressure Sensor helper
# ignacio.asensi@cern.ch
# Aug 2018
# Nov 2018: Added update_state
#############################################

import helper

class PressureSensorHelper(helper.Helper):
    pass

if __name__ == '__main__':
    
    helper.Helper().cmdline("PressureSensor")
    print ("Have a nice day")
