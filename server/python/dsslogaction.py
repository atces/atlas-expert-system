#!/usr/bin/env python
#############################################
# DSSLogAction helper
# Florian.Haslbeck@cern.ch
# May 2024
#############################################

import helper

class DSSLogActionHelper(helper.Helper):
	pass

if __name__ == '__main__':

    helper.Helper().cmdline("DSSLogAction")
    print ("Have a nice day")
    pass
