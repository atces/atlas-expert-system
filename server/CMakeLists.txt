cmake_minimum_required(VERSION 3.20)

project(atces)

cmake_host_system_information(RESULT _HN QUERY HOSTNAME)
string(FIND ${_HN} "-" _HNP REVERSE)
string(SUBSTRING ${_HN} 0 ${_HNP} ATCES_HOST)
message(INFO " ATCES_HOST=${ATCES_HOST}")

add_compile_definitions(DAEMON)
add_compile_definitions(ATCES_HOST="${ATCES_HOST}")

add_executable(atces_watchdog
        src/atces_watchdog.cpp
        src/AtcesCommands.cpp
)

install(CODE "execute_process(COMMAND sh -c \"chcon -u system_u -t initrc_exec_t /root/server/build/atces_watchdog\" )")
install(CODE "execute_process(COMMAND sh -c \"chcon -u system_u -t initrc_exec_t /etc/systemd/system/atcesd.service\" )")
install(FILES share/atcesd.service DESTINATION /etc/systemd/system/ PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
