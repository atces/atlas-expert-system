/**
 * Interface to OKS via simple queries 
 * Carlos.Solans@cern.ch
 * November 2015
 **/

#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include <cstdlib>
#include <iostream>
#include <sstream> 
#include <signal.h>
#include <string>
#include <map>

#include "config/Configuration.h"

#define MAXREAD 1024

using namespace std;

bool m_cont = true;
int m_sockfd = 0;
bool m_debug = false;
int m_clisockfd;
Configuration * g_config = 0;

void handle(int signal){
  cout << "signal = " << signal << endl;
  m_cont=false;
  signal=signal;
  close(m_sockfd);
  //shutdown(m_sockfd,2);
}

string encode(string s){
  string r="START\n"+s+"\nEND";
  return r;
}

int recv(int sockfd, string &s){
  //int recv(int s, void *buf, size_t len, int flags);
  char buf[MAXREAD+1];
  s = "";
  memset(buf,0,MAXREAD+1);
  int status = recv(sockfd,buf,MAXREAD,0);
  if(status>0){
    s=buf;
  }
  return status;
}

bool is_numeric(const std::string& s){
  float number;
  //try{
  istringstream ss(s);
  ss >> number;
  if(ss.fail()) {cout << "Failed to convert" << endl; return false;}
  //}catch(exception ex){
  //cout << ex.what() << endl;
  //return false;
  //}
  return true;
}

//void client(int clisockfd){
void client(){
  int clisockfd=m_clisockfd;
  while(true){
    char buffer[MAXREAD+1];
    bzero(buffer,MAXREAD+1);
    int n = read(clisockfd,buffer,MAXREAD);
    if (n < 0) {cout << "ERROR reading from socket" << endl;break;}
    if(buffer[n-1]=='\n')buffer[n-1]=0;
    string qry(buffer);
    string ans;
    if (m_debug) cout << "Here is the message: \"" << qry << "\"" << endl;
    ans = "I got your message";
    n = write(clisockfd,ans.c_str(),ans.size());
    if (n < 0) cout << "ERROR writing to socket" << endl;
  }
  close(clisockfd);
  if(m_debug) cout << "Closed client connection: " << clisockfd << endl;
}

/*char * basename(char*arg){
   char * base = strrchr(arg,(int)'/');
   if(base++) return base;
   else return arg;
}
*/
int main(int argc, char *argv[]){
  //IF DEBUG
#ifdef DEBUG
  m_debug=true;
#endif

  if(argc<2){
    cout << "Usage: " << basename(argv[0]) << " <db>" << endl;
    return 1;
  }
  
  string filepid = "/var/run/"+string(basename(argv[0]))+".pid";
  string filelog = "/var/log/"+string(basename(argv[0]))+".log";
  
  //IF DAEMON
  bool daemon = false;
#ifdef DAEMON
  daemon = true;
#endif
  if(daemon){
      
    //Create copy
    int pid = fork();
    
    //Close out the standard file descriptors 
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    if (pid > 0){
      FILE * pidfile = fopen(filepid.c_str(), "w");
      if(pidfile!=NULL){
	fprintf(pidfile, "%u\n", pid);
	fflush(pidfile);
	fclose(pidfile);
      }
      return 0;
    }
    
    //Change the file mode mask
    umask(0);            
    //Create a new SID for the child process 
    int sid = setsid();
    if (sid < 0) return 1;
    //Change the current working directory 
    if ((chdir("/")) < 0) return 1;
    
    freopen(filelog.c_str(), "w", stdout);
  }
  
  //Create the configuration pointer
  g_config = new Configuration(argv[1]);

  //Start program
  std::setlocale(LC_ALL, "en-US");
  int sockfd, portno;
  struct sockaddr_in serv_addr;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {cout << "ERROR opening socket" << endl; return 1;}
  m_sockfd = sockfd;

  int reuse_addr=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&reuse_addr, sizeof(reuse_addr));
  #ifdef SO_REUSEPORT
  int reuse_port=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&reuse_port, sizeof(reuse_port));
  #endif
  struct linger onclose;
  onclose.l_onoff = 0;
  onclose.l_linger = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (const void*)&onclose, sizeof(onclose));

  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = atoi((argc>1?argv[1]:"9999"));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  int ret = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
  if(ret<0){cout << "ERROR on binding" << endl; return 1;}
  
  listen(sockfd,5);
  if(m_debug) cout << "listening" << endl;

  signal(SIGKILL,handle);
  signal(SIGTERM,handle);
  signal(SIGINT,handle);

  struct sockaddr_in cli_addr;
  socklen_t clilen;
  while(m_cont){
    int clisockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if(!m_cont) break;
    cout << "Client connection created: " << clisockfd << endl;
    if(clisockfd < 0){ cout << "ERROR on accept" << endl; continue;}
    //Create children connection: Thread that will die on it's own
    m_clisockfd=clisockfd;
    int pid = fork();
    if(pid==0){
      client();
    }
  }
  if(m_debug) cout << "I managed to exit the loop" << endl;
  //if daemon
  if(daemon) unlink(filepid.c_str());
  return 0; 
}
