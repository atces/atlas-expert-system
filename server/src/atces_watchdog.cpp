/***********************************************
 * ATLAS Technical Coordination Expert System
 * Watchdog daemon
 *
 * Carlos.Solans@cern.ch
 * November 2016
 ***********************************************/

#include <sys/types.h> 
#include <sys/stat.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <sstream> 
#include <signal.h>
#include <string>
#include <map>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <netdb.h>

#include "AtcesCommands.h"

#define MAXREAD 1024
#define ATCESWPORT 9999

using namespace std;

bool m_cont = true;
bool m_debug = false;
int  m_sockfd = 0;
int  m_clisockfd =-1;
AtcesCommands * m_commands;

void closeServer(){
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0){ cerr << "ERROR opening socket" << endl; return;}
  struct hostent *server;
  server = gethostbyname("localhost");
  if (server == NULL){ cerr << "ERROR getting server host" << endl; return;}
  struct sockaddr_in serv_addr;
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, 
		(char *)&serv_addr.sin_addr.s_addr,
		server->h_length);
  serv_addr.sin_port = htons(ATCESWPORT);
  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	cerr << "ERROR connecting" << endl;
  string msg("stop");
  int n = write(sockfd,msg.c_str(),msg.length());
  if (n < 0) cerr << "ERROR writing to socket" << endl;
  char buffer[256];
  bzero(buffer,256);
  n = read(sockfd,buffer,255);
  if (n < 0) cerr << "ERROR writing to socket" << endl;
  else{ printf("%s\n",buffer); }
  close(sockfd);
  return;
}

void handle(int signal){
  cout << "signal = " << signal << endl;
  m_cont=false;
  signal=signal;
  //closeServer();
  close(m_sockfd);
  //shutdown(m_sockfd,2);
}

string timestamp(){
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer,80,"%Y-%m-%d %H:%M:%S",timeinfo);
  
  string ret(buffer);
  return ret;
}

string uptime(time_t * t0){
  float fdt = difftime(time(0), *t0);
  int dt = (int)fdt + 0;
  int minutes = dt/60;
  int hours = minutes/60;
  minutes = minutes%60;
  int seconds = dt%60;
  ostringstream os;
  os << setw(2) << setfill('0') << hours << ":" 
	 << setw(2) << setfill('0') << minutes << ":" 
	 << setw(2) << setfill('0') << seconds;
  return os.str();
}

int main(int argc, char *argv[]){

  //IF DEBUG
  #ifdef DEBUG
  m_debug=true;
  #endif

  if(argc<2){
    cout << "Usage: " << basename(argv[0]) << " <command>" << endl
		 << " command start|stop|restart|status|stats" << endl;
    return 1;
  }

  string filepid = "/run/"+string(basename(argv[0]))+".pid";
  string filelog = "/var/log/"+string(basename(argv[0]))+".log";
  string fileerr = "/var/log/"+string(basename(argv[0]))+".err";
  
  int testaccess = access("/var/run/", W_OK);
  if (testaccess != 0){
	filepid = string(basename(argv[0]))+".pid";
	filelog = string(basename(argv[0]))+".log";
	fileerr = string(basename(argv[0]))+".err";
  }

  int pid=-1;
  ifstream ifile( filepid.c_str() );
  if(ifile){
	ifile >> pid;
	ifile.close();
  }

  string command(argv[1]);
  if(command=="stop" || command=="restart"){
	if(pid>0){
	  //closeServer();
	  kill(pid, SIGTERM);
	  kill(pid, SIGKILL);
      if(kill(pid, 0)!=0){
        remove(filepid.c_str());
      }
	}
	if(command=="stop"){return 0;}
  }else if(command=="status"){
	if(pid==-1){cout << "Not running" << endl; }
	else if(kill(pid, 0)==0){ cout << "Running" << endl;}
	return 0;
  }else if(command=="stats"){
	string stats;
	ifstream ifile( filelog.c_str() );
	string line;
	if(ifile){
	  bool bstats=false;
	  while(std::getline(ifile, line)){
		if(line.find("END_STATS")!=string::npos){bstats=false;}
		else if(line.find("STATS")!=string::npos){bstats=true;stats="";}
		if(bstats){stats += line + "\n";}
	  }
	  ifile.close();
	}
	cout << stats << endl;
	return 0;
  }else if(command=="getstatus"||command=="getlogs"||command=="getrlog"){
    m_commands = new AtcesCommands();
    cout << m_commands->Execute(command) << endl;
    delete m_commands;
    return 0;
  }else{
	if(pid!=-1 && kill(pid, 0)==0){
	  cout << "Running" << endl;
	  return 0;
	}
  }


  if(m_debug) cout << basename(argv[0]) << " starting" << endl;
  time_t starttime = time(0);
  
  //IF DAEMON
  bool daemon = false;
  #ifdef DAEMON
  daemon = true;
  #endif
  if(daemon){
      
    //Create copy
    int pid = fork();
    
    //Close out the standard file descriptors 
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    if (pid > 0){
	  //Parent write the child PID to file and exit
      FILE * pidfile = fopen(filepid.c_str(), "w");
      if(pidfile!=NULL){
		fprintf(pidfile, "%u\n", pid);
		fflush(pidfile);
		fclose(pidfile);
      }
      return 0;
    }
	
	//reopen output stream
	freopen(filelog.c_str(), "w", stdout);
	freopen(fileerr.c_str(), "w", stderr);
	cout << basename(argv[0]) << " starting " << timestamp() << endl;
    /*
	//Change the file mode mask
	umask(0);
    //Create a new SID for the child process 
    int sid = setsid();
    if (sid < 0) return 1;
    */
	//Change the current working directory 
    if ((chdir("/tmp")) < 0) return 1;
  }
  
  //Create commander
  m_commands = new AtcesCommands();
  cout << "====== STATS ======" << endl
	   << "Uptime\t" << uptime(&starttime) << endl
	   << m_commands->GetStats()
	   << "==== END_STATS ====" << endl;
  //Initialize the server for the first time
  m_commands->Execute("restart");
  
  //Start program
  cout << "Create new socket" << endl;
  std::setlocale(LC_ALL, "en-US");
  int sockfd, portno;
  struct sockaddr_in serv_addr;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {cout << "Socket error on creation" << endl; return 1;}
  m_sockfd = sockfd;

  int reuse_addr=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&reuse_addr, sizeof(reuse_addr));
  #ifdef SO_REUSEPORT
  int reuse_port=1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&reuse_port, sizeof(reuse_port));
  #endif
  struct linger onclose;
  onclose.l_onoff = 0;
  onclose.l_linger = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (const void*)&onclose, sizeof(onclose));

  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = ATCESWPORT;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  cout << "Bind to port: " << portno << endl;
  int ret = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
  if(ret<0){cout << "Socket error on binding" << endl; return 1;}
  
  cout << "Listen" << endl;
  listen(sockfd,5);
  if(m_debug) cout << "Socket listening" << endl;

  signal(SIGKILL,handle);
  signal(SIGTERM,handle);
  signal(SIGINT,handle);

  struct sockaddr_in cli_addr;
  socklen_t clilen;
  while(m_cont){
    int clisockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if(!m_cont) break;
    cout << "Socket client connected " << timestamp() << " (" << clisockfd << ")" << endl;
    if(clisockfd < 0){ cout << "Socket error on accept" << endl; continue;}
	while(true){
	  char buffer[MAXREAD+1];
	  bzero(buffer,MAXREAD+1);
	  int n = read(clisockfd,buffer,MAXREAD);
	  if (n <= 0) {cout << "Socket error reading from socket" << endl;break;}
	  if(buffer[n-1]=='\n')buffer[n-1]=0;
	  string req(buffer);
	  if (m_debug) cout << "Socket client request: " << req << flush << endl;
	  string rep = m_commands->Execute(req);
	  if (m_debug) cout << "Socket client reply: (" << rep.size() << ")" << rep.substr(0,100) << flush << endl;
	  //Write the size of the message
	  ostringstream os;
	  os << std::hex << setw(8) << setfill('0') << rep.size() << std::dec;
	  write(clisockfd,os.str().c_str(),8);
	  //Wrtie the message in whole
	  n = write(clisockfd,rep.c_str(),rep.size());
	  if (n < 0){cout << "Socket error writing to socket" << endl;break;}
	}
	close(clisockfd);
	cout << "Socket client terminated " << timestamp() << " (" << clisockfd << ")" << endl;
	
	string ts = timestamp();
	cout << "====== STATS " << ts << "======" << endl
		 << "Uptime\t" << uptime(&starttime) << endl
		 << m_commands->GetStats()
		 << "==== END_STATS " << ts << "====" << endl;
  }
  if(m_debug) cout << "Have a nice day" << endl;
  //if daemon
  if(daemon) unlink(filepid.c_str());
  return 0; 
}
