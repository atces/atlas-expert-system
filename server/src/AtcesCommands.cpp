#include "AtcesCommands.h"
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <unistd.h>
#include <ctime>
#include <algorithm>
#include <cstdlib>
#include <sys/wait.h>
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "stdlib.h"
#include "string.h"
#include <dirent.h>
#include <sys/types.h>

struct cpuinfo{
   unsigned long user;    // normal processes executing in user mode
   unsigned long nice;    // niced processes executing in user mode
   unsigned long system;  // processes executing in kernel mode
   unsigned long idle;    // twiddling thumbs
   unsigned long iowait;  // waiting for I/O to complete
   unsigned long irq;     // servicing interrupts
   unsigned long softirq; // servicing softirqs
};

struct cpuload{
  unsigned long total;
  unsigned long user;
};

void cpuload(cpuload* cpuLoad){
  struct cpuinfo cpuInfo1, cpuInfo2;
  FILE* file;
  file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu",&cpuInfo1.user,&cpuInfo1.nice,&cpuInfo1.system,&cpuInfo1.idle);
  fclose(file);
  usleep(10000);
  file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu",&cpuInfo2.user,&cpuInfo2.nice,&cpuInfo2.system,&cpuInfo2.idle);
  fclose(file);
  cpuLoad->total = cpuInfo2.user-cpuInfo1.user + cpuInfo2.nice-cpuInfo1.nice + cpuInfo2.system-cpuInfo1.system + cpuInfo2.idle-cpuInfo1.idle;
  cpuLoad->user = cpuInfo2.user-cpuInfo1.user;
}

using namespace std;

AtcesCommands::AtcesCommands(){
  m_logDir = "/tmp/root/logs";
  string cmdInit("ATCES_HOST=__ATCES_HOST__;"
                 "mkdir -p /root/server/python; rm -rf /root/server/python/install.py;"
                 "curl -s https://$ATCES_HOST.web.cern.ch/server/python/install.py -o /root/server/python/install.py;"
                 "python /root/server/python/install.py -s $ATCES_HOST.web.cern.ch/server/python/ -d /root/server/python;");
  string cmdStart("source /root/server/python/setup.sh; nohup python /root/server/python/server.py -o /tmp/root/logs/server_`date +'%y%m%d%H%M'`.log&");
  string cmdStop("pkill -9 -f server.py; pkill -2 -f server.py; pkill -15 -f server.py");
  
  #ifdef ATCES_HOST
  cmdInit.replace(cmdInit.find("__ATCES_HOST__"),14,ATCES_HOST);
  #else
  cmdInit.replace(cmdInit.find("__ATCES_HOST__"),14,"atlas-expert-system");
  #endif

  m_commands["init"].push_back(cmdInit);
  m_commands["start"].push_back(cmdInit);
  m_commands["start"].push_back(cmdStart);
  m_commands["stop"].push_back(cmdStop);
  m_commands["restart"].push_back(cmdStop);
  m_commands["restart"].push_back(cmdInit);
  m_commands["restart"].push_back(cmdStart);
  
  for(m_commandIt=m_commands.begin();m_commandIt!=m_commands.end();m_commandIt++){
	m_stats[m_commandIt->first]=0;
  }

}

AtcesCommands::~AtcesCommands(){
  m_commands.clear();
}

string AtcesCommands::Execute(string cmdkey){
  cout << "Execute " << cmdkey << endl;

  if     (cmdkey=="getlogs"){return GetLogs();}
  else if(cmdkey=="getrlog"){return GetRLog();}
  else if(cmdkey=="getstats"){return GetStats();}
  else if(cmdkey=="getstatus"){return GetStatus();}

  if(m_stats.find(cmdkey)==m_stats.end()){
	return "Command not found";
  }
  m_stats[cmdkey]+=1;
  string ret;
  for(unsigned int i=0;i<m_commands[cmdkey].size();i++){
    cout << "Subcommand " << i << ": " << m_commands[cmdkey].at(i) << endl;
	
	int rc = system(m_commands[cmdkey].at(i).c_str());
	cout << "Returned: " << rc << endl;
	if(rc==0){ret+="OK ";}
	else{
	  ostringstream os;
	  os << rc << " ";
	  ret+=os.str();
	}
  }
  cout << "Execute " << cmdkey << " finished" << endl;
  return ret;
}

string AtcesCommands::GetStatus(){

  //Get memory info
  struct sysinfo memInfo;
  sysinfo (&memInfo);

  //Get Total RAM
  float totalMem = memInfo.totalram;
  totalMem *= memInfo.mem_unit;
  totalMem /= (float)1E9;

  //Get Used RAM                                                                                                                                                                       
  float usedMem = memInfo.totalram - memInfo.freeram;
  usedMem *= memInfo.mem_unit;
  usedMem /= (float)1E9;

  //Get CPU info                                                                                                                                                                       
  struct cpuload cpuLoad;
  cpuload(&cpuLoad);
  float usedCpu = cpuLoad.user*100/float(cpuLoad.total);

  ostringstream buffer;
  buffer.precision(2);
  buffer << "Total RAM: " << totalMem << " GB, "
         << "Used RAM: "  << usedMem  << " GB, "
         << "Used CPU: "  << usedCpu  << " % ";
  return buffer.str();

}

string AtcesCommands::GetStats(){
  ostringstream ret;
  for(m_commandIt=m_commands.begin();m_commandIt!=m_commands.end();m_commandIt++){
	ret << "#" << m_commandIt->first << "\t" 
		<< m_stats[m_commandIt->first] << endl;
  }
  return ret.str();
}

string AtcesCommands::GetLastLog(){
  DIR *dir = opendir(m_logDir.c_str());
  if (dir == NULL) { return ""; }
  string fname="";
  struct dirent *f;
  unsigned mdate=0;
  while ((f = readdir(dir)) != NULL) {
    string fn(f->d_name);
    if(fn=="." || fn=="..") continue;
    if(fn.find("_")==string::npos || fn.find(".log")==string::npos){continue;}
    string fdate = fn.substr(fn.find("_")+1,fn.find(".log"));
    unsigned idate = atoi(fdate.c_str());
    if(idate>mdate){mdate=idate;fname=fn;}
  }
  closedir(dir);
  return fname;
}

string AtcesCommands::GetLogs(){
  string fname=GetLastLog();
  if(fname==""){return "";}
  cout << "Open logs " << fname << endl;
  ostringstream os;
  os << m_logDir << "/" << fname.c_str();
  ifstream t(os.str().c_str());
  stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}

string AtcesCommands::GetRLog(){
  string fname=GetLastLog();
  if(fname==""){return "";}
  cout << "Open logs " << fname << endl;
  ostringstream os;
  os << m_logDir << "/" << fname.c_str();
  ifstream t(os.str().c_str());
  string line;
  string ret;
  while (std::getline(t, line)){
    ret = line + '\n' + ret;
  }
  return ret;
}
