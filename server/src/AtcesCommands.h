//! Dear emacs this is -*-C++-*-
#ifndef ATCESCOMMANDS_H
#define ATCESCOMMANDS_H

#include <iostream>
#include <string>
#include <map>
#include <vector>

class AtcesCommands{
 public:
  AtcesCommands();
  ~AtcesCommands();
  std::string Execute(std::string);
  std::string GetStatus();
  std::string GetStats();
  std::string GetLogs();
  std::string GetRLog();
 private:
  std::map<std::string,std::vector<std::string> > m_commands;
  std::map<std::string,std::vector<std::string> >::iterator m_commandIt;
  std::map<std::string,int> m_stats;
  std::string m_logDir;
  std::string GetLastLog();
};

#endif //ATCESCOMMANDS
