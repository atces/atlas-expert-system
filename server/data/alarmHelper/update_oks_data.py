# #!/usr/bin/env python
#######################################################
# update the OKS database with the new data from user input
# Florian.Haslbeck@cern.ch
# April 2024
#######################################################

import os
import json
import datetime
import urllib.request

def get_changes_from_file():

    # get stored logs and events
    tmp_files = os.listdir("fromServer/")
    latest_file = sorted(tmp_files)[-1]

    print("Loading latest file: %s" % (latest_file))

    with open("fromServer/%s" % (latest_file), "r") as f: 
        file_content = f.read()
        # remove non-ascii
        file_content.encode('ascii', 'ignore').decode('ascii')
        
        json_dict = json.loads(file_content)

    changed_NotGrouped = json_dict.get("display_notGrouped", {})
    changed_Events     = json_dict.get("display_Events", {})

    return changed_Events, changed_NotGrouped

def update_oks_database(db, changed_Events, changed_NotGrouped,
                        verbose=False, sandbox=False):
    """ update the OKS database with the new data from user input """

    print("Updating OKS database")
    print("db: %s" % (db))

    input("Press Enter to continue...")

    # events
    same_as_in_oks = []
    for i, (uid, event) in enumerate(changed_Events.items()):
        
        print("event: %s - %s %s %s"%(uid, event["date"], event["time"], event["description"]))

        different_to_oks = False
        # get Event object
        # check if DSSEvent already exists, else create it
        try : 
            event_obj = db.get_obj("DSSEvent",uid)
            if verbose: print(f"UPDATING DSSEvent {uid}")
        except: 
            event_obj = db.create_obj("DSSEvent", uid)
            different_to_oks = True
            if verbose: print(f"CREATING DSSEvent {uid}")

        # attributes
        attributes = [
            ("date", event["date"]), 
            ("time", event["time"]), 
            ("description", event["description"].encode('ascii', 'replace').decode()), 
            ("short_description", event["short_description"].encode('ascii', 'replace').decode()),
            ("event_type",  event["event_type"]),

            ("person", event["person"].encode('ascii', 'replace').decode()),
            ("elisa",  event["elisa"].encode('ascii', 'replace').decode()),
            # actions are not stored in the database
            ]

        # check if any attribute has changed
        for attr, value in attributes:
            if value is not None and event_obj[attr] != value:
                event_obj[attr]  = value
                different_to_oks = True

        # logs
        logs_uids = event["logs"]
        # print("logs_uids: %s" % (logs_uids))
        logs = []

        # print("Adding logs to event %s .." % (event_obj))
        for luid in logs_uids:
            # print(".. luid: %s" % (luid))
            # get log object

            try: log_obj = db.get_obj("DSSLog", luid) # this is an object of class db
            except: log_obj = None

            # print("got log_obj: %s" % (log_obj))
    
            # log_obj = db.get_obj("DSSLog", luid)
            if log_obj is not None:
                logs.append(log_obj)
            else:
                print(f"WARNING: DSSLog {luid} does not exist in database ")
                
                if log_obj == None:
                    log_name, log_time, log_date = "None", "None", "None"
                    input("Oddly, log_obj is None. Press Enter to continue...")
                else:
                    # TODO create the log object
                    log_name , log_time, log_date = log_obj["name"], log_obj["time"], log_obj["date"]

                print(f"Creating DSSLog {luid} {log_name} {log_time} {log_date} in oks database.")
                print("verbose: ", verbose)

                log_obj = create_DSSLog_object(db, luid, log_name, time = f"{log_date} {log_time}", verbose=verbose)
                logs.append(log_obj)

                input("Press Enter to continue...")

            # update inverse relation
            log_obj["event"] = event_obj

        event_obj["logs"] = logs
        # print("Event updated %s" % (event_obj))
    
        if not different_to_oks:
            print("Event same as in OKS: %s"%uid)
            same_as_in_oks.append(uid)

        # if i % 20 == 0 and not sandbox:
        db.commit()
            

def get_DSS_alarms_from_web_range(date_str_start, date_str_end):
    """ get all DSS alarms from DSS logbook web interface since a given date 
        input: 
            date as string of format yyyy-mm-dd
        output: 
            iterable object with DSSLog-like tuples (name, time, came)
    """
    
    # get json data since
    assert len(date_str_start.split("-")) == 3, "date_str must be of format yyyy-mm-dd"
    if date_str_end == None: 
        url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_range&since={date_str_start}"
        print(url)
    else:
        url = f"https://solans.web.cern.ch/dss/dbfunctions.php?cmd=get_alarms_range&since={date_str_start}&until={date_str_end}"
    data = urllib.request.urlopen(url).read()
    json_data = json.loads(data)

    dss_logs = json_data["report"]

    return dss_logs

def create_DSSLog_object(db, 
                        uid, 
                        name = None, time = None,
                        verbose=False):
    """ edit DSSLog object in database
        if DSSLog does not exist, create it, else update it
    
        input:
            db:    database object
            uid:   unique id of DSSLog -> came
            name:  name of DSSLog -> name
            time:  time of DSSLog -> time
        output:
            None
    """

    def _get_obj(db, _type, _name):
        try: retr_obj = db.get_obj(_type, _name) # this is an object of class db
        except: retr_obj = None
        return retr_obj

    ret = None

    # check if DSSLog already exists, else create it
    try : 
        obj = db.get_obj("DSSLog",uid)
        if verbose: print(f"UPDATING DSSLog {uid} {time} {name} in oks database.")
    except: 
        obj = db.create_obj("DSSLog", uid)
        if verbose: print(f"CREATING DSSLog {uid} {time} {name} in oks database.")

    # check if DSSLog needs to be updated

    date, time = time.split(" ")
    
    # attributes
    attributes = [("name", name), ("time", time), ("date", date)]
    for attr, value in attributes:
        if value is not None and obj[attr] != value:  obj[attr]  = value

    # relations
    relations  = [("alarm", name)] 
    for rel, value in relations:
        # get object from database
        retr_obj = _get_obj(db, "Alarm", value) # this is an object of class db
        if retr_obj is not None:
            obj[rel] = retr_obj
        else:
            print(f"WARNING: Alarm {value} does not exist in database")
            ret = name
    
    return ret


def update_logs(db, days=30, verbose=False):
    # get all logs up to today and create those that dont exist yet

    special_cases = {
        "AL_INF_WaterLeak_MUN_CSC_Y5823X0"          : None,
        "AL_INF_WaterLeak_Failure_MUN_CSC_Y2924X1"  : None,
        "AL_INF_Power_US15_EXS102_UPS_EndOfAutonomy": "AL_INF_Power_US15_EXS2_UPS_EndOfAutonomy",
        "AL_INF_Power_US15_EXS102_UPSonBattery"     : "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_GAS_MUN_CSC_GasFailure"                 : "AL_GAS_MUN_MMG_GasFailure",
        "AL_TestAlarm"                              : None,
        "AL_INF_Smoke_SR1_Ceiling"                  : "AL_INF_Smoke_SR1",
        "AL_INF_Power_US15_EXS102_UPS_OnBattery"	: "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_INF_Power_US15_EXS102_UPSFailure"	    : "AL_INF_Power_US15_EXS2_UPS_OnBattery",
        "AL_GAS_TRT_ArgonActiveGas_Stop"	        : "AL_GAS_TRT_ArgonActiveGas_Stop",
    }

    before = (datetime.datetime.now() - datetime.timedelta(days=days)).strftime("%Y-%m-%d")

    last_logs = get_DSS_alarms_from_web_range(before, None)

    # check which are missing
    # create DSSLogs
    for i, log in enumerate(last_logs):
        name = log["name"].strip()
        name = name.replace("AL_AL_", "AL_")
        if not name.startswith("AL_"): continue

        if name in special_cases:
            name = special_cases[name]
            if name is None: continue
            else: print("Renaming %s to %s" % (log["name"], name))

        uid = "log_%s"%log["came"]
        time = log["time"]

        log_obj = create_DSSLog_object(db, uid, name, time, verbose=verbose)

        

def fix_relation_alarm_to_log(db, save=False):

    # get all logs
    logs = db.get_objs("DSSLog")

    # get all alarms
    alarms = db.get_objs("Alarm")

    for i, alarm in enumerate(alarms):
        alarm_name = alarm.UID()

        if not alarm_name.startswith("AL_"): continue

        #  for debugging
        # if alarm_name != "AL_BCM_BeamAbortConditions": continue

        print("Alarm :", alarm_name)

        # get all logs for this alarm
        logs_of_alarm = [l for l in logs if l["name"] == alarm_name]

        print("Found %d logs" % len(logs_of_alarm))

        # get all logs from the alarm
        logs_linked_to_alarm = [l.UID() for l in alarm["logs"]]

        # find the logs that are not linked to the alarm
        logs_not_linked = [l for l in logs_of_alarm if l.UID() not in logs_linked_to_alarm]

        print("Logs not linked to alarm")
        for l in logs_not_linked:
            print("No relation from alarm to log", l.UID(), l["date"], l["time"])

            # make the relation
            alarm_logs = alarm["logs"]
            alarm_logs.append(l)
            alarm["logs"] = alarm_logs

            print("Added relation from alarm to log", l.UID(), l["date"], l["time"])

        
        if save:
            if i % 50 == 0 :
                db.commit()

    if save: db.commit()



# # # #
# main 
# # # #

import config
import argparse
from time import sleep

parser = argparse.ArgumentParser()
parser.add_argument("database", help="database file")
parser.add_argument("-v", "--verbose", help="verbose" , action='store_true')
parser.add_argument("-s", "--sandbox", help="sandbox, dont commit" , action='store_true')
parser.add_argument("-d", "--days", help="days to update", type=int, default=30)
args = parser.parse_args()

print("Selected %i days .. sure?"%args.days)
input("Press any key to continue ...")

# load oks database and get classes and helpers
db = config.Configuration("oksconfig:%s" % (args.database))

# update logs
update_logs(db, days=args.days, verbose=args.verbose)

if not args.sandbox:
    print("Committing updated logs to database")
    input("Press Enter to continue...")
    db.commit()

    
    db = config.Configuration("oksconfig:%s" % (args.database))
    sleep(1)

input("Press Enter to continue...")

changed_Events, changed_NotGrouped = get_changes_from_file()

print("# # Updating OKS database # #\n")

print("# # # # # # # # # # # # # # #")
print("Events: ")
print("# # # # # # # # # # # # # # #\n")
for uid, event in changed_Events.items():
    print(f"* * {uid} * *")
    print("Title : %s" % event["description"])
    print("Type  : %s" % event["event_type"])
    print("Extra : %s" % event["short_description"])
    print("Date  : %s" % event["date"])
    print("Time  : %s" % event["time"])
    print("Person: %s" % event["person"])
    print("Elisa : %s" % event["elisa"])

    for i, log in enumerate(event["logs"]):
        if i == 0 : print("Log   : %s" % log)
        else: print("        %s" % log)
    print("\n")

print("# # # # # # # # # # # # # # #")
print("Not grouped logs: ")
print("# # # # # # # # # # # # # # #\n")
for log in changed_NotGrouped:
    print(log)

input("Press Enter to continue...")



update_oks_database(db, changed_Events, changed_NotGrouped, verbose=args.verbose, sandbox=args.sandbox)


fix_relation_alarm_to_log(db, args.sandbox)


if not args.sandbox:
    db.commit()



