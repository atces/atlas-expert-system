

def fix_relation_alarm_to_log(db):

    # get all logs
    logs = db.get_objs("DSSLog")

    # get all alarms
    alarms = db.get_objs("Alarm")

    for i, alarm in enumerate(alarms):
        alarm_name = alarm.UID()

        if not alarm_name.startswith("AL_"): continue

        #  for debugging
        # if alarm_name != "AL_BCM_BeamAbortConditions": continue

        print("Alarm :", alarm_name)

        # get all logs for this alarm
        logs_of_alarm = [l for l in logs if l["name"] == alarm_name]

        print("Found %d logs" % len(logs_of_alarm))

        # get all logs from the alarm
        logs_linked_to_alarm = [l.UID() for l in alarm["logs"]]

        # find the logs that are not linked to the alarm
        logs_not_linked = [l for l in logs_of_alarm if l.UID() not in logs_linked_to_alarm]

        print("Logs not linked to alarm : ", len(logs_not_linked))
        for l in logs_not_linked:
            print("No relation from alarm to log", l.UID(), l["date"], l["time"])

            # make the relation
            alarm_logs = alarm["logs"]
            alarm_logs.append(l)
            alarm["logs"] = alarm_logs

            print("Added relation from alarm to log", l.UID(), l["date"], l["time"])

        
        if i % 50 == 0 :
            db.commit()

    db.commit()




if __name__ == "__main__":

    import config

    db_name = "/eos/user/a/atopes/www/atlas-expert-system/data/sb.data.xml"
    # db_name = "../../../data/sb.data.xml"
    db = config.Configuration("oksconfig:%s" % (db_name))

    
    fix_relation_alarm_to_log(db)

    print("Checked and fixed %s" % db_name)
    print("Dont forget to commit!")
