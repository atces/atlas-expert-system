python action.py sb.data.xml -r --uid O_LAR_Barrel_HV_Y2719A2 --interlocks Y.27-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_Barrel_HV_Y2719A2 --interlocks LAr_HV_SR1_BPR_A -y
python action.py sb.data.xml -e --uid O_LAR_Barrel_HV_Y2719A2 --interlocks LAr_HV_SR2_BPS_C -y
python action.py sb.data.xml -e --uid O_LAR_Barrel_HV_Y2719A2 --interlocks LAr_HV_SR3_BSP_PUR -y
python action.py sb.data.xml -e --uid O_LAR_Barrel_HV_Y2719A2 --interlocks LAr_HV_SR4_ECPS -y

python action.py sb.data.xml -r --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks Y.24-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H01 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H02 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H03 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H04 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H05 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H06 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H07 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2416A2 --interlocks LAr_LVPS_FEC_H08 -y

python action.py sb.data.xml -r --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks Y.25-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H09 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H10 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H11 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H12 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H13 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H14 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H15 -y
python action.py sb.data.xml -e --uid O_LAR_BarrelhC_FELV_Y2516A2 --interlocks LAr_LVPS_FEC_H16 -y

python action.py sb.data.xml -r --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks Y.22-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I01 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I02 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I03 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I04 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I05 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I06 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I07 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2216A2 --interlocks LAr_LVPS_FEC_I08 -y

python action.py sb.data.xml -r --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks Y.23-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I09 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I10 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I11 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I12 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I13 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I14 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I15 -y
python action.py sb.data.xml -e --uid O_LAR_BarreliA_FELV_Y2316A2 --interlocks LAr_LVPS_FEC_I16 -y

python action.py sb.data.xml -r --uid O_LAR_ECA_FELV_Y2616A2 --interlocks Y.26-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A08 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A09 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A10 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A11 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A12 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A13 -y

python action.py sb.data.xml -r --uid O_LAR_ECA_FELV_Y2716A2 --interlocks Y.27-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A01 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A02 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A03 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A04 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A05 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A06 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_FELV_Y2716A2 --interlocks LAr_LVPS_FEC_A07 -y

python action.py sb.data.xml -r --uid O_LAR_ECA_HEC_HV_Y2619A2 --interlocks Y.26-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HEC_HV_Y2619A2 --interlocks LAr_HV_SR1_B_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HEC_HV_Y2619A2 --interlocks LAr_HV_SR2_B_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HEC_HV_Y2619A2 --interlocks LAr_HV_SR3_HEC_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HEC_HV_Y2619A2 --interlocks LAr_HV_SR4_HEC_A -y

python action.py sb.data.xml -r --uid O_LAR_ECA_HV_Y2519A2 --interlocks Y.25-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HV_Y2519A2 --interlocks LAr_HV_SR1_EM_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HV_Y2519A2 --interlocks LAr_HV_SR2_EM_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HV_Y2519A2 --interlocks LAr_HV_SR3_EM_A -y
python action.py sb.data.xml -e --uid O_LAR_ECA_HV_Y2519A2 --interlocks LAr_HV_SR4_EM_A -y

python action.py sb.data.xml -r --uid O_LAR_ECC_FELV_Y2816A2 --interlocks Y.28-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C08 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C09 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C10 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C11 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C12 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2816A2 --interlocks LAr_LVPS_FEC_C13 -y

python action.py sb.data.xml -r --uid O_LAR_ECC_FELV_Y2916A2 --interlocks Y.29-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C01 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C02 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C03 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C04 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C05 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C06 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_FELV_Y2916A2 --interlocks LAr_LVPS_FEC_C07 -y

python action.py sb.data.xml -r --uid O_LAR_ECC_HEC_HV_Y2819A2 --interlocks Y.28-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HEC_HV_Y2819A2 --interlocks LAr_HV_SR1_B_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HEC_HV_Y2819A2 --interlocks LAr_HV_SR2_B_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HEC_HV_Y2819A2 --interlocks LAr_HV_SR3_HEC_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HEC_HV_Y2819A2 --interlocks LAr_HV_SR4_HEC_C -y

python action.py sb.data.xml -r --uid O_LAR_ECC_HV_Y2919A2 --interlocks Y.29-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HV_Y2919A2 --interlocks LAr_HV_SR1_EM_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HV_Y2919A2 --interlocks LAr_HV_SR2_EM_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HV_Y2919A2 --interlocks LAr_HV_SR3_EM_C -y
python action.py sb.data.xml -e --uid O_LAR_ECC_HV_Y2919A2 --interlocks LAr_HV_SR4_EM_C -y

python action.py sb.data.xml -r --uid O_LAR_HEC_FELV_Y2616A2 --interlocks Y.26-16.A2 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A08 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A09 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A10 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A11 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A12 -y
python action.py sb.data.xml -e --uid O_LAR_HEC_FELV_Y2616A2 --interlocks LAr_LVPS_FEC_A13 -y

python action.py sb.data.xml -r --uid O_LAR_Presampler_HV_Y2719A2 --interlocks Y.27-19.A2 -y
python action.py sb.data.xml -e --uid O_LAR_Presampler_HV_Y2719A2 --interlocks LAr_HV_SR1_BPR_A -y
python action.py sb.data.xml -e --uid O_LAR_Presampler_HV_Y2719A2 --interlocks LAr_HV_SR2_BPS_C -y
python action.py sb.data.xml -e --uid O_LAR_Presampler_HV_Y2719A2 --interlocks LAr_HV_SR3_BSP_PUR -y
python action.py sb.data.xml -e --uid O_LAR_Presampler_HV_Y2719A2 --interlocks LAr_HV_SR4_ECPS -y



python rack.py sb.data.xml -r --interlockedby O_LAR_Barrel_HV_Y2719A2 --uid Y.27-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Barrel_HV_Y2719A2 --uid LAr_HV_SR1_BPR_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Barrel_HV_Y2719A2 --uid LAr_HV_SR2_BPS_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Barrel_HV_Y2719A2 --uid LAr_HV_SR3_BSP_PUR -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Barrel_HV_Y2719A2 --uid LAr_HV_SR4_ECPS -y

python rack.py sb.data.xml -r --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid Y.24-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H01 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H02 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H03 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H04 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H05 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H06 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H07 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2416A2 --uid LAr_LVPS_FEC_H08 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid Y.25-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H09 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H10 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H11 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H12 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H13 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H14 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H15 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarrelhC_FELV_Y2516A2 --uid LAr_LVPS_FEC_H16 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid Y.22-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I01 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I02 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I03 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I04 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I05 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I06 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I07 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2216A2 --uid LAr_LVPS_FEC_I08 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid Y.23-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I09 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I10 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I11 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I12 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I13 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I14 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I15 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_BarreliA_FELV_Y2316A2 --uid LAr_LVPS_FEC_I16 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid Y.26-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A08 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A09 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A10 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A11 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A12 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2616A2 --uid LAr_LVPS_FEC_A13 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid Y.27-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A01 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A02 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A03 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A04 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A05 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A06 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_FELV_Y2716A2 --uid LAr_LVPS_FEC_A07 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECA_HEC_HV_Y2619A2 --uid Y.26-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HEC_HV_Y2619A2 --uid LAr_HV_SR1_B_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HEC_HV_Y2619A2 --uid LAr_HV_SR2_B_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HEC_HV_Y2619A2 --uid LAr_HV_SR3_HEC_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HEC_HV_Y2619A2 --uid LAr_HV_SR4_HEC_A -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECA_HV_Y2519A2 --uid Y.25-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HV_Y2519A2 --uid LAr_HV_SR1_EM_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HV_Y2519A2 --uid LAr_HV_SR2_EM_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HV_Y2519A2 --uid LAr_HV_SR3_EM_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECA_HV_Y2519A2 --uid LAr_HV_SR4_EM_A -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid Y.28-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C08 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C09 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C10 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C11 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C12 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2816A2 --uid LAr_LVPS_FEC_C13 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid Y.29-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C01 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C02 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C03 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C04 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C05 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C06 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_FELV_Y2916A2 --uid LAr_LVPS_FEC_C07 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECC_HEC_HV_Y2819A2 --uid Y.28-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HEC_HV_Y2819A2 --uid LAr_HV_SR1_B_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HEC_HV_Y2819A2 --uid LAr_HV_SR2_B_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HEC_HV_Y2819A2 --uid LAr_HV_SR3_HEC_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HEC_HV_Y2819A2 --uid LAr_HV_SR4_HEC_C -y

python rack.py sb.data.xml -r --interlockedby O_LAR_ECC_HV_Y2919A2 --uid Y.29-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HV_Y2919A2 --uid LAr_HV_SR1_EM_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HV_Y2919A2 --uid LAr_HV_SR2_EM_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HV_Y2919A2 --uid LAr_HV_SR3_EM_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_ECC_HV_Y2919A2 --uid LAr_HV_SR4_EM_C -y

python rack.py sb.data.xml -r --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid Y.26-16.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A08 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A09 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A10 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A11 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A12 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_HEC_FELV_Y2616A2 --uid LAr_LVPS_FEC_A13 -y

python rack.py sb.data.xml -r --interlockedby O_LAR_Presampler_HV_Y2719A2 --uid Y.27-19.A2 -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Presampler_HV_Y2719A2 --uid LAr_HV_SR1_BPR_A -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Presampler_HV_Y2719A2 --uid LAr_HV_SR2_BPS_C -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Presampler_HV_Y2719A2 --uid LAr_HV_SR3_BSP_PUR -y
python powersupply.py sb.data.xml -e --interlockedby O_LAR_Presampler_HV_Y2719A2 --uid LAr_HV_SR4_ECPS -y