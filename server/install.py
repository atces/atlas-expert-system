#!/usr/bin/env python3
#######################################################
# Preliminary installer for the expert system watchdog
#
# Carlos.Solans@cern.ch
# May 2017
# April 2020 - migrate to python 3
# July 2022 - replace wget by curl
#######################################################
import os
import sys
import argparse

dest = "/root/server"
host = "https://atlas-expert-system.web.cern.ch/server/"

parser = argparse.ArgumentParser()
parser.add_argument("-t","--type",help="production,development")
args=parser.parse_args()

if args.type and "dev" in args.type: 
    host = "https://atlas-expert-system-dev.web.cern.ch/server/"
    pass

files = ("CMakeLists.txt",
				 "cmt/compile.sh",
         "share/atcesd.service",
         "python/client.py",
         "src/atces_watchdog.cpp",
         "src/AtcesCommands.cpp",
         "src/AtcesCommands.h",
         )

print ("Install dependencies")
print("yum -y install python cmake gcc-c++")
os.system("yum -y install python cmake gcc-c++")

print ("Download sources")
for f in files:
    if not os.path.exists("%s/%s"%(dest,os.path.dirname(f))): 
        os.system("mkdir -p %s/%s" % (dest,os.path.dirname(f)))
        pass
    print("curl %s/%s -o %s/%s" % (host,f,dest,f))
    os.system("rm %s/%s" % (dest,f))
    os.system("curl %s/%s -o %s/%s" % (host,f,dest,f))
    pass

print ("Install service")
#os.system("source %s/cmt/compile.sh" % dest)
os.system("mkdir %s/build" % dest)
os.system("cd %s/build; cmake ..; make VERBOSE=1 install" % dest)
os.system("chcon -v -t bin_t %s/build/atces_watchdog" % dest)
os.system("/bin/cp %s/share/atcesd.service /etc/systemd/system/." % dest)
os.system("systemctl enable atcesd")
os.system("systemctl stop atcesd")
os.system("systemctl start atcesd")
os.system("firewall-cmd --zone=public --add-port=9999/tcp  --permanent")
os.system("firewall-cmd --zone=public --add-port=9998/tcp  --permanent")
os.system("firewall-cmd --reload")

print ("Have a nice day")