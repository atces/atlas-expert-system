# ATLAS Technical Coordination Expert System

## The commands for editing the data schema and instances are:

```
source server/python/setup.sh
oks_schema_editor data/sb.schema.xml &
oks_data_editor data/sb.data.xml &
```

## Adding a new package with npm

1. Copy package.json to a non eos directory
2. Install new package and update package.json:

```npm install <package> --save```

3. Make the release a production one

```npm install --production```

4. Copy the new package.json to the installation directory
5. Copy the node_modules folder to the installation directory

## Installing PIP

In some cases, it is necessary to install additional python modules
that are not part of TDAQ or LCG. PIP can ease the installation of
these packages.

It is recommended to install local version of PIP compatible with
the current version of TDAQ.

1. Setup the environment

```source server/python/setup.sh```

2. Install pip

```python -m pip install --upgrade pip --user```

3. Use PIP to install other packages: eg Pillow

```~/.local/bin/pip install Pillow --user```

## Installing in production server

1. Change to web server directory

```cd /eos/home-a/atopes/atlas-expert-system```

2. Pull from git 

```git pull origin master```

3. Setup the environment

```source server/python/setup.sh```

4. Restart the server

```python client.py -p 9999 atlas-expert-system-cc7 restart ```

